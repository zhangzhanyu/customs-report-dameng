package com.qianjing.common.utils.valid;

import javax.validation.groups.Default;

/**
 * 查询分组校验
 */
public interface QueryGroup extends Default {

}
