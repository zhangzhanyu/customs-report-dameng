package com.qianjing.project.capital.mapper;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要关区不合格Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface CapitalInvalidCustomsMapper {

    CapitalInvalidCustoms selectCapitalInvalidCustomsById(Long id);

    List<CapitalInvalidCustoms> selectCapitalInvalidCustomsList(CapitalInvalidCustoms capitalInvalidCustoms);

    int insertCapitalInvalidCustoms(CapitalInvalidCustoms capitalInvalidCustoms);

    int updateCapitalInvalidCustoms(CapitalInvalidCustoms capitalInvalidCustoms);

    int deleteCapitalInvalidCustomsById(Long id);

    int deleteCapitalInvalidCustomsByIds(Long[] ids);
}
