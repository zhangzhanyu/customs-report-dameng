package com.qianjing.project.capital.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.capital.mapper.CapitalInvalidCustomsMapper;
import com.qianjing.project.capital.domain.CapitalInvalidCustoms;
import com.qianjing.project.capital.service.ICapitalInvalidCustomsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要关区不合格Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalInvalidCustomsServiceImpl implements ICapitalInvalidCustomsService {

    private static final Logger log = LoggerFactory.getLogger(CapitalInvalidCustomsServiceImpl.class);

    private final CapitalInvalidCustomsMapper capitalInvalidCustomsMapper;

    /**
     * 查询资化产品主要关区不合格
     * 
     * @param id 资化产品主要关区不合格主键
     * @return 资化产品主要关区不合格
     */
    @Override
    public CapitalInvalidCustoms selectCapitalInvalidCustomsById(Long id) {
        return capitalInvalidCustomsMapper.selectCapitalInvalidCustomsById(id);
    }

    /**
     * 查询资化产品主要关区不合格列表
     * 
     * @param capitalInvalidCustoms 资化产品主要关区不合格
     * @return 资化产品主要关区不合格
     */
    @Override
    public List<CapitalInvalidCustoms> selectCapitalInvalidCustomsList(CapitalInvalidCustoms capitalInvalidCustoms) {
        return capitalInvalidCustomsMapper.selectCapitalInvalidCustomsList(capitalInvalidCustoms);
    }

    /**
     * 新增资化产品主要关区不合格
     * 
     * @param capitalInvalidCustoms 资化产品主要关区不合格
     * @return 结果
     */
    @Override
    public int insertCapitalInvalidCustoms(CapitalInvalidCustoms capitalInvalidCustoms) {
        capitalInvalidCustoms.setCreateTime(DateUtils.getNowDate());
        return capitalInvalidCustomsMapper.insertCapitalInvalidCustoms(capitalInvalidCustoms);
    }

    /**
     * 修改资化产品主要关区不合格
     * 
     * @param capitalInvalidCustoms 资化产品主要关区不合格
     * @return 结果
     */
    @Override
    public int updateCapitalInvalidCustoms(CapitalInvalidCustoms capitalInvalidCustoms) {
        capitalInvalidCustoms.setUpdateTime(DateUtils.getNowDate());
        return capitalInvalidCustomsMapper.updateCapitalInvalidCustoms(capitalInvalidCustoms);
    }

    /**
     * 批量删除资化产品主要关区不合格
     * 
     * @param ids 需要删除的资化产品主要关区不合格主键
     * @return 结果
     */
    @Override
    public int deleteCapitalInvalidCustomsByIds(Long[] ids) {
        return capitalInvalidCustomsMapper.deleteCapitalInvalidCustomsByIds(ids);
    }

    /**
     * 删除资化产品主要关区不合格信息
     * 
     * @param id 资化产品主要关区不合格主键
     * @return 结果
     */
    @Override
    public int deleteCapitalInvalidCustomsById(Long id) {
        return capitalInvalidCustomsMapper.deleteCapitalInvalidCustomsById(id);
    }

    /**
     * 导入资化产品主要关区不合格数据
     *
     * @param capitalInvalidCustomsList capitalInvalidCustomsList 资化产品主要关区不合格信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importCapitalInvalidCustoms(List<CapitalInvalidCustoms> capitalInvalidCustomsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(capitalInvalidCustomsList) || capitalInvalidCustomsList.size() == 0) {
            throw new ServiceException("导入资化产品主要关区不合格数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (CapitalInvalidCustoms capitalInvalidCustoms : capitalInvalidCustomsList){
            try {
                // 验证是否存在
                if (true) {
                    capitalInvalidCustoms.setCreateBy(operName);
                    this.insertCapitalInvalidCustoms(capitalInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    capitalInvalidCustoms.setUpdateBy(operName);
                    this.updateCapitalInvalidCustoms(capitalInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
