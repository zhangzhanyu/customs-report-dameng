package com.qianjing.project.capital.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.capital.domain.CapitalImportCompany;
import com.qianjing.project.capital.service.ICapitalImportCompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 资化产品主要进口企业Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/capital/company")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalImportCompanyController extends BaseController {

    private final ICapitalImportCompanyService capitalImportCompanyService;

    /**
     * 查询资化产品主要进口企业列表
     */
//    @PreAuthorize("@ss.hasPermi('capital:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(CapitalImportCompany capitalImportCompany) {
        startPage();
        List<CapitalImportCompany> list = capitalImportCompanyService.selectCapitalImportCompanyList(capitalImportCompany);
        return getDataTable(list);
    }

    /**
     * 导出资化产品主要进口企业列表
     */
    @PreAuthorize("@ss.hasPermi('capital:company:export')")
    @Log(title = "资化产品主要进口企业", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CapitalImportCompany capitalImportCompany) {
        List<CapitalImportCompany> list = capitalImportCompanyService.selectCapitalImportCompanyList(capitalImportCompany);
        ExcelUtil<CapitalImportCompany> util = new ExcelUtil<CapitalImportCompany>(CapitalImportCompany.class);
        return util.exportExcel(list, "资化产品主要进口企业数据");
    }

    /**
     * 导入资化产品主要进口企业列表
     */
    @Log(title = "资化产品主要进口企业", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('capital:company:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<CapitalImportCompany> util = new ExcelUtil<CapitalImportCompany>(CapitalImportCompany.class);
        List<CapitalImportCompany> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = capitalImportCompanyService.importCapitalImportCompany(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载资化产品主要进口企业导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<CapitalImportCompany> util = new ExcelUtil<CapitalImportCompany>(CapitalImportCompany.class);
        util.importTemplateExcel(response, "资化产品主要进口企业数据");
    }

    /**
     * 获取资化产品主要进口企业详细信息
     */
    @PreAuthorize("@ss.hasPermi('capital:company:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(capitalImportCompanyService.selectCapitalImportCompanyById(id));
    }

    /**
     * 新增资化产品主要进口企业
     */
    @PreAuthorize("@ss.hasPermi('capital:company:add')")
    @Log(title = "资化产品主要进口企业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CapitalImportCompany capitalImportCompany) {
        return toAjax(capitalImportCompanyService.insertCapitalImportCompany(capitalImportCompany));
    }

    /**
     * 修改资化产品主要进口企业
     */
    @PreAuthorize("@ss.hasPermi('capital:company:edit')")
    @Log(title = "资化产品主要进口企业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CapitalImportCompany capitalImportCompany) {
        return toAjax(capitalImportCompanyService.updateCapitalImportCompany(capitalImportCompany));
    }

    /**
     * 删除资化产品主要进口企业
     */
    @PreAuthorize("@ss.hasPermi('capital:company:remove')")
    @Log(title = "资化产品主要进口企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(capitalImportCompanyService.deleteCapitalImportCompanyByIds(ids));
    }
}
