package com.qianjing.project.capital.mapper;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalCheckCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要查验关区Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface CapitalCheckCustomsMapper {

    CapitalCheckCustoms selectCapitalCheckCustomsById(Long id);

    List<CapitalCheckCustoms> selectCapitalCheckCustomsList(CapitalCheckCustoms capitalCheckCustoms);

    int insertCapitalCheckCustoms(CapitalCheckCustoms capitalCheckCustoms);

    int updateCapitalCheckCustoms(CapitalCheckCustoms capitalCheckCustoms);

    int deleteCapitalCheckCustomsById(Long id);

    int deleteCapitalCheckCustomsByIds(Long[] ids);
}
