package com.qianjing.project.capital.mapper;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要来源国Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface CapitalSourceCountryMapper {

    CapitalSourceCountry selectCapitalSourceCountryById(Long id);

    List<CapitalSourceCountry> selectCapitalSourceCountryList(CapitalSourceCountry capitalSourceCountry);

    int insertCapitalSourceCountry(CapitalSourceCountry capitalSourceCountry);

    int updateCapitalSourceCountry(CapitalSourceCountry capitalSourceCountry);

    int deleteCapitalSourceCountryById(Long id);

    int deleteCapitalSourceCountryByIds(Long[] ids);
}
