package com.qianjing.project.capital.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.capital.domain.CapitalSourceCountry;
import com.qianjing.project.capital.service.ICapitalSourceCountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 资化产品主要来源国Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/capital/country")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalSourceCountryController extends BaseController {

    private final ICapitalSourceCountryService capitalSourceCountryService;

    /**
     * 查询资化产品主要来源国列表
     */
//    @PreAuthorize("@ss.hasPermi('capital:country:list')")
    @GetMapping("/list")
    public TableDataInfo list(CapitalSourceCountry capitalSourceCountry) {
        startPage();
        List<CapitalSourceCountry> list = capitalSourceCountryService.selectCapitalSourceCountryList(capitalSourceCountry);
        return getDataTable(list);
    }

    /**
     * 导出资化产品主要来源国列表
     */
    @PreAuthorize("@ss.hasPermi('capital:country:export')")
    @Log(title = "资化产品主要来源国", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CapitalSourceCountry capitalSourceCountry) {
        List<CapitalSourceCountry> list = capitalSourceCountryService.selectCapitalSourceCountryList(capitalSourceCountry);
        ExcelUtil<CapitalSourceCountry> util = new ExcelUtil<CapitalSourceCountry>(CapitalSourceCountry.class);
        return util.exportExcel(list, "资化产品主要来源国数据");
    }

    /**
     * 导入资化产品主要来源国列表
     */
    @Log(title = "资化产品主要来源国", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('capital:country:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<CapitalSourceCountry> util = new ExcelUtil<CapitalSourceCountry>(CapitalSourceCountry.class);
        List<CapitalSourceCountry> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = capitalSourceCountryService.importCapitalSourceCountry(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载资化产品主要来源国导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<CapitalSourceCountry> util = new ExcelUtil<CapitalSourceCountry>(CapitalSourceCountry.class);
        util.importTemplateExcel(response, "资化产品主要来源国数据");
    }

    /**
     * 获取资化产品主要来源国详细信息
     */
    @PreAuthorize("@ss.hasPermi('capital:country:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(capitalSourceCountryService.selectCapitalSourceCountryById(id));
    }

    /**
     * 新增资化产品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('capital:country:add')")
    @Log(title = "资化产品主要来源国", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CapitalSourceCountry capitalSourceCountry) {
        return toAjax(capitalSourceCountryService.insertCapitalSourceCountry(capitalSourceCountry));
    }

    /**
     * 修改资化产品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('capital:country:edit')")
    @Log(title = "资化产品主要来源国", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CapitalSourceCountry capitalSourceCountry) {
        return toAjax(capitalSourceCountryService.updateCapitalSourceCountry(capitalSourceCountry));
    }

    /**
     * 删除资化产品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('capital:country:remove')")
    @Log(title = "资化产品主要来源国", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(capitalSourceCountryService.deleteCapitalSourceCountryByIds(ids));
    }
}
