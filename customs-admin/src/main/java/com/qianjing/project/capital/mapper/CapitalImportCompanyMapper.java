package com.qianjing.project.capital.mapper;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalImportCompany;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要进口企业Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface CapitalImportCompanyMapper {

    CapitalImportCompany selectCapitalImportCompanyById(Long id);

    List<CapitalImportCompany> selectCapitalImportCompanyList(CapitalImportCompany capitalImportCompany);

    int insertCapitalImportCompany(CapitalImportCompany capitalImportCompany);

    int updateCapitalImportCompany(CapitalImportCompany capitalImportCompany);

    int deleteCapitalImportCompanyById(Long id);

    int deleteCapitalImportCompanyByIds(Long[] ids);
}
