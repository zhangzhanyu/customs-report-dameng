package com.qianjing.project.capital.service;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalImportCompany;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要进口企业Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ICapitalImportCompanyService 
{
    /**
     * 查询资化产品主要进口企业
     * 
     * @param id 资化产品主要进口企业主键
     * @return 资化产品主要进口企业
     */
    public CapitalImportCompany selectCapitalImportCompanyById(Long id);

    /**
     * 查询资化产品主要进口企业列表
     * 
     * @param capitalImportCompany 资化产品主要进口企业
     * @return 资化产品主要进口企业集合
     */
    public List<CapitalImportCompany> selectCapitalImportCompanyList(CapitalImportCompany capitalImportCompany);

    /**
     * 新增资化产品主要进口企业
     * 
     * @param capitalImportCompany 资化产品主要进口企业
     * @return 结果
     */
    public int insertCapitalImportCompany(CapitalImportCompany capitalImportCompany);

    /**
     * 修改资化产品主要进口企业
     * 
     * @param capitalImportCompany 资化产品主要进口企业
     * @return 结果
     */
    public int updateCapitalImportCompany(CapitalImportCompany capitalImportCompany);

    /**
     * 批量删除资化产品主要进口企业
     * 
     * @param ids 需要删除的资化产品主要进口企业主键集合
     * @return 结果
     */
    public int deleteCapitalImportCompanyByIds(Long[] ids);

    /**
     * 删除资化产品主要进口企业信息
     * 
     * @param id 资化产品主要进口企业主键
     * @return 结果
     */
    public int deleteCapitalImportCompanyById(Long id);

    /**
     * 导入资化产品主要进口企业信息
     *
     * @param capitalImportCompanyList 资化产品主要进口企业信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importCapitalImportCompany(List<CapitalImportCompany> capitalImportCompanyList, Boolean isUpdateSupport, String operName);
}
