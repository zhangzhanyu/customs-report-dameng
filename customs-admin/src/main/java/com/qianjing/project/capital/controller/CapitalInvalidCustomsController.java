package com.qianjing.project.capital.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.capital.domain.CapitalInvalidCustoms;
import com.qianjing.project.capital.service.ICapitalInvalidCustomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 资化产品主要关区不合格Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/capital/invalidCustoms")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalInvalidCustomsController extends BaseController {

    private final ICapitalInvalidCustomsService capitalInvalidCustomsService;

    /**
     * 查询资化产品主要关区不合格列表
     */
//    @PreAuthorize("@ss.hasPermi('capital:invalidCustoms:list')")
    @GetMapping("/list")
    public TableDataInfo list(CapitalInvalidCustoms capitalInvalidCustoms) {
        startPage();
        List<CapitalInvalidCustoms> list = capitalInvalidCustomsService.selectCapitalInvalidCustomsList(capitalInvalidCustoms);
        return getDataTable(list);
    }

    /**
     * 导出资化产品主要关区不合格列表
     */
    @PreAuthorize("@ss.hasPermi('capital:invalidCustoms:export')")
    @Log(title = "资化产品主要关区不合格", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CapitalInvalidCustoms capitalInvalidCustoms) {
        List<CapitalInvalidCustoms> list = capitalInvalidCustomsService.selectCapitalInvalidCustomsList(capitalInvalidCustoms);
        ExcelUtil<CapitalInvalidCustoms> util = new ExcelUtil<CapitalInvalidCustoms>(CapitalInvalidCustoms.class);
        return util.exportExcel(list, "资化产品主要关区不合格数据");
    }

    /**
     * 导入资化产品主要关区不合格列表
     */
    @Log(title = "资化产品主要关区不合格", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('capital:invalidCustoms:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<CapitalInvalidCustoms> util = new ExcelUtil<CapitalInvalidCustoms>(CapitalInvalidCustoms.class);
        List<CapitalInvalidCustoms> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = capitalInvalidCustomsService.importCapitalInvalidCustoms(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载资化产品主要关区不合格导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<CapitalInvalidCustoms> util = new ExcelUtil<CapitalInvalidCustoms>(CapitalInvalidCustoms.class);
        util.importTemplateExcel(response, "资化产品主要关区不合格数据");
    }

    /**
     * 获取资化产品主要关区不合格详细信息
     */
    @PreAuthorize("@ss.hasPermi('capital:invalidCustoms:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(capitalInvalidCustomsService.selectCapitalInvalidCustomsById(id));
    }

    /**
     * 新增资化产品主要关区不合格
     */
    @PreAuthorize("@ss.hasPermi('capital:invalidCustoms:add')")
    @Log(title = "资化产品主要关区不合格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CapitalInvalidCustoms capitalInvalidCustoms) {
        return toAjax(capitalInvalidCustomsService.insertCapitalInvalidCustoms(capitalInvalidCustoms));
    }

    /**
     * 修改资化产品主要关区不合格
     */
    @PreAuthorize("@ss.hasPermi('capital:invalidCustoms:edit')")
    @Log(title = "资化产品主要关区不合格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CapitalInvalidCustoms capitalInvalidCustoms) {
        return toAjax(capitalInvalidCustomsService.updateCapitalInvalidCustoms(capitalInvalidCustoms));
    }

    /**
     * 删除资化产品主要关区不合格
     */
    @PreAuthorize("@ss.hasPermi('capital:invalidCustoms:remove')")
    @Log(title = "资化产品主要关区不合格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(capitalInvalidCustomsService.deleteCapitalInvalidCustomsByIds(ids));
    }
}
