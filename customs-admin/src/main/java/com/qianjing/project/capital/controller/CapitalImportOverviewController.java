package com.qianjing.project.capital.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.capital.domain.CapitalImportOverview;
import com.qianjing.project.capital.service.ICapitalImportOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 资化产品进口概况Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/capital/importOverview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalImportOverviewController extends BaseController {

    private final ICapitalImportOverviewService capitalImportOverviewService;

    /**
     * 查询资化产品进口概况列表
     */
//    @PreAuthorize("@ss.hasPermi('capital:importOverview:list')")
    @GetMapping("/list")
    public TableDataInfo list(CapitalImportOverview capitalImportOverview) {
        startPage();
        List<CapitalImportOverview> list = capitalImportOverviewService.selectCapitalImportOverviewList(capitalImportOverview);
        return getDataTable(list);
    }

    /**
     * 导出资化产品进口概况列表
     */
    @PreAuthorize("@ss.hasPermi('capital:importOverview:export')")
    @Log(title = "资化产品进口概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CapitalImportOverview capitalImportOverview) {
        List<CapitalImportOverview> list = capitalImportOverviewService.selectCapitalImportOverviewList(capitalImportOverview);
        ExcelUtil<CapitalImportOverview> util = new ExcelUtil<CapitalImportOverview>(CapitalImportOverview.class);
        return util.exportExcel(list, "资化产品进口概况数据");
    }

    /**
     * 导入资化产品进口概况列表
     */
    @Log(title = "资化产品进口概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('capital:importOverview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<CapitalImportOverview> util = new ExcelUtil<CapitalImportOverview>(CapitalImportOverview.class);
        List<CapitalImportOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = capitalImportOverviewService.importCapitalImportOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载资化产品进口概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<CapitalImportOverview> util = new ExcelUtil<CapitalImportOverview>(CapitalImportOverview.class);
        util.importTemplateExcel(response, "资化产品进口概况数据");
    }

    /**
     * 获取资化产品进口概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('capital:importOverview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(capitalImportOverviewService.selectCapitalImportOverviewById(id));
    }

    /**
     * 新增资化产品进口概况
     */
    @PreAuthorize("@ss.hasPermi('capital:importOverview:add')")
    @Log(title = "资化产品进口概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CapitalImportOverview capitalImportOverview) {
        return toAjax(capitalImportOverviewService.insertCapitalImportOverview(capitalImportOverview));
    }

    /**
     * 修改资化产品进口概况
     */
    @PreAuthorize("@ss.hasPermi('capital:importOverview:edit')")
    @Log(title = "资化产品进口概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CapitalImportOverview capitalImportOverview) {
        return toAjax(capitalImportOverviewService.updateCapitalImportOverview(capitalImportOverview));
    }

    /**
     * 删除资化产品进口概况
     */
    @PreAuthorize("@ss.hasPermi('capital:importOverview:remove')")
    @Log(title = "资化产品进口概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(capitalImportOverviewService.deleteCapitalImportOverviewByIds(ids));
    }
}
