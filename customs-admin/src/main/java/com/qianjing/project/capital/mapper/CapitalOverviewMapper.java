package com.qianjing.project.capital.mapper;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要数量Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface CapitalOverviewMapper {

    CapitalOverview selectCapitalOverviewById(Long id);

    List<CapitalOverview> selectCapitalOverviewList(CapitalOverview capitalOverview);

    int insertCapitalOverview(CapitalOverview capitalOverview);

    int updateCapitalOverview(CapitalOverview capitalOverview);

    int deleteCapitalOverviewById(Long id);

    int deleteCapitalOverviewByIds(Long[] ids);
}
