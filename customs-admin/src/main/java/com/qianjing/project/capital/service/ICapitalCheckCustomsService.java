package com.qianjing.project.capital.service;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalCheckCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要查验关区Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ICapitalCheckCustomsService 
{
    /**
     * 查询资化产品主要查验关区
     * 
     * @param id 资化产品主要查验关区主键
     * @return 资化产品主要查验关区
     */
    public CapitalCheckCustoms selectCapitalCheckCustomsById(Long id);

    /**
     * 查询资化产品主要查验关区列表
     * 
     * @param capitalCheckCustoms 资化产品主要查验关区
     * @return 资化产品主要查验关区集合
     */
    public List<CapitalCheckCustoms> selectCapitalCheckCustomsList(CapitalCheckCustoms capitalCheckCustoms);

    /**
     * 新增资化产品主要查验关区
     * 
     * @param capitalCheckCustoms 资化产品主要查验关区
     * @return 结果
     */
    public int insertCapitalCheckCustoms(CapitalCheckCustoms capitalCheckCustoms);

    /**
     * 修改资化产品主要查验关区
     * 
     * @param capitalCheckCustoms 资化产品主要查验关区
     * @return 结果
     */
    public int updateCapitalCheckCustoms(CapitalCheckCustoms capitalCheckCustoms);

    /**
     * 批量删除资化产品主要查验关区
     * 
     * @param ids 需要删除的资化产品主要查验关区主键集合
     * @return 结果
     */
    public int deleteCapitalCheckCustomsByIds(Long[] ids);

    /**
     * 删除资化产品主要查验关区信息
     * 
     * @param id 资化产品主要查验关区主键
     * @return 结果
     */
    public int deleteCapitalCheckCustomsById(Long id);

    /**
     * 导入资化产品主要查验关区信息
     *
     * @param capitalCheckCustomsList 资化产品主要查验关区信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importCapitalCheckCustoms(List<CapitalCheckCustoms> capitalCheckCustomsList, Boolean isUpdateSupport, String operName);
}
