package com.qianjing.project.capital.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.capital.domain.CapitalCheckCustoms;
import com.qianjing.project.capital.service.ICapitalCheckCustomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 资化产品主要查验关区Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/capital/checkCustoms")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalCheckCustomsController extends BaseController {

    private final ICapitalCheckCustomsService capitalCheckCustomsService;

    /**
     * 查询资化产品主要查验关区列表
     */
//    @PreAuthorize("@ss.hasPermi('capital:checkCustoms:list')")
    @GetMapping("/list")
    public TableDataInfo list(CapitalCheckCustoms capitalCheckCustoms) {
        startPage();
        List<CapitalCheckCustoms> list = capitalCheckCustomsService.selectCapitalCheckCustomsList(capitalCheckCustoms);
        return getDataTable(list);
    }

    /**
     * 导出资化产品主要查验关区列表
     */
    @PreAuthorize("@ss.hasPermi('capital:checkCustoms:export')")
    @Log(title = "资化产品主要查验关区", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CapitalCheckCustoms capitalCheckCustoms) {
        List<CapitalCheckCustoms> list = capitalCheckCustomsService.selectCapitalCheckCustomsList(capitalCheckCustoms);
        ExcelUtil<CapitalCheckCustoms> util = new ExcelUtil<CapitalCheckCustoms>(CapitalCheckCustoms.class);
        return util.exportExcel(list, "资化产品主要查验关区数据");
    }

    /**
     * 导入资化产品主要查验关区列表
     */
    @Log(title = "资化产品主要查验关区", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('capital:checkCustoms:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<CapitalCheckCustoms> util = new ExcelUtil<CapitalCheckCustoms>(CapitalCheckCustoms.class);
        List<CapitalCheckCustoms> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = capitalCheckCustomsService.importCapitalCheckCustoms(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载资化产品主要查验关区导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<CapitalCheckCustoms> util = new ExcelUtil<CapitalCheckCustoms>(CapitalCheckCustoms.class);
        util.importTemplateExcel(response, "资化产品主要查验关区数据");
    }

    /**
     * 获取资化产品主要查验关区详细信息
     */
    @PreAuthorize("@ss.hasPermi('capital:checkCustoms:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(capitalCheckCustomsService.selectCapitalCheckCustomsById(id));
    }

    /**
     * 新增资化产品主要查验关区
     */
    @PreAuthorize("@ss.hasPermi('capital:checkCustoms:add')")
    @Log(title = "资化产品主要查验关区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CapitalCheckCustoms capitalCheckCustoms) {
        return toAjax(capitalCheckCustomsService.insertCapitalCheckCustoms(capitalCheckCustoms));
    }

    /**
     * 修改资化产品主要查验关区
     */
    @PreAuthorize("@ss.hasPermi('capital:checkCustoms:edit')")
    @Log(title = "资化产品主要查验关区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CapitalCheckCustoms capitalCheckCustoms) {
        return toAjax(capitalCheckCustomsService.updateCapitalCheckCustoms(capitalCheckCustoms));
    }

    /**
     * 删除资化产品主要查验关区
     */
    @PreAuthorize("@ss.hasPermi('capital:checkCustoms:remove')")
    @Log(title = "资化产品主要查验关区", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(capitalCheckCustomsService.deleteCapitalCheckCustomsByIds(ids));
    }
}
