package com.qianjing.project.capital.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.capital.mapper.CapitalCheckCustomsMapper;
import com.qianjing.project.capital.domain.CapitalCheckCustoms;
import com.qianjing.project.capital.service.ICapitalCheckCustomsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要查验关区Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalCheckCustomsServiceImpl implements ICapitalCheckCustomsService {

    private static final Logger log = LoggerFactory.getLogger(CapitalCheckCustomsServiceImpl.class);

    private final CapitalCheckCustomsMapper capitalCheckCustomsMapper;

    /**
     * 查询资化产品主要查验关区
     * 
     * @param id 资化产品主要查验关区主键
     * @return 资化产品主要查验关区
     */
    @Override
    public CapitalCheckCustoms selectCapitalCheckCustomsById(Long id) {
        return capitalCheckCustomsMapper.selectCapitalCheckCustomsById(id);
    }

    /**
     * 查询资化产品主要查验关区列表
     * 
     * @param capitalCheckCustoms 资化产品主要查验关区
     * @return 资化产品主要查验关区
     */
    @Override
    public List<CapitalCheckCustoms> selectCapitalCheckCustomsList(CapitalCheckCustoms capitalCheckCustoms) {
        return capitalCheckCustomsMapper.selectCapitalCheckCustomsList(capitalCheckCustoms);
    }

    /**
     * 新增资化产品主要查验关区
     * 
     * @param capitalCheckCustoms 资化产品主要查验关区
     * @return 结果
     */
    @Override
    public int insertCapitalCheckCustoms(CapitalCheckCustoms capitalCheckCustoms) {
        capitalCheckCustoms.setCreateTime(DateUtils.getNowDate());
        return capitalCheckCustomsMapper.insertCapitalCheckCustoms(capitalCheckCustoms);
    }

    /**
     * 修改资化产品主要查验关区
     * 
     * @param capitalCheckCustoms 资化产品主要查验关区
     * @return 结果
     */
    @Override
    public int updateCapitalCheckCustoms(CapitalCheckCustoms capitalCheckCustoms) {
        capitalCheckCustoms.setUpdateTime(DateUtils.getNowDate());
        return capitalCheckCustomsMapper.updateCapitalCheckCustoms(capitalCheckCustoms);
    }

    /**
     * 批量删除资化产品主要查验关区
     * 
     * @param ids 需要删除的资化产品主要查验关区主键
     * @return 结果
     */
    @Override
    public int deleteCapitalCheckCustomsByIds(Long[] ids) {
        return capitalCheckCustomsMapper.deleteCapitalCheckCustomsByIds(ids);
    }

    /**
     * 删除资化产品主要查验关区信息
     * 
     * @param id 资化产品主要查验关区主键
     * @return 结果
     */
    @Override
    public int deleteCapitalCheckCustomsById(Long id) {
        return capitalCheckCustomsMapper.deleteCapitalCheckCustomsById(id);
    }

    /**
     * 导入资化产品主要查验关区数据
     *
     * @param capitalCheckCustomsList capitalCheckCustomsList 资化产品主要查验关区信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importCapitalCheckCustoms(List<CapitalCheckCustoms> capitalCheckCustomsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(capitalCheckCustomsList) || capitalCheckCustomsList.size() == 0) {
            throw new ServiceException("导入资化产品主要查验关区数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (CapitalCheckCustoms capitalCheckCustoms : capitalCheckCustomsList){
            try {
                // 验证是否存在
                if (true) {
                    capitalCheckCustoms.setCreateBy(operName);
                    this.insertCapitalCheckCustoms(capitalCheckCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    capitalCheckCustoms.setUpdateBy(operName);
                    this.updateCapitalCheckCustoms(capitalCheckCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
