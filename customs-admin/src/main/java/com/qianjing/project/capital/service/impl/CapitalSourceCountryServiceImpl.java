package com.qianjing.project.capital.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.capital.mapper.CapitalSourceCountryMapper;
import com.qianjing.project.capital.domain.CapitalSourceCountry;
import com.qianjing.project.capital.service.ICapitalSourceCountryService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要来源国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalSourceCountryServiceImpl implements ICapitalSourceCountryService {

    private static final Logger log = LoggerFactory.getLogger(CapitalSourceCountryServiceImpl.class);

    private final CapitalSourceCountryMapper capitalSourceCountryMapper;

    /**
     * 查询资化产品主要来源国
     * 
     * @param id 资化产品主要来源国主键
     * @return 资化产品主要来源国
     */
    @Override
    public CapitalSourceCountry selectCapitalSourceCountryById(Long id) {
        return capitalSourceCountryMapper.selectCapitalSourceCountryById(id);
    }

    /**
     * 查询资化产品主要来源国列表
     * 
     * @param capitalSourceCountry 资化产品主要来源国
     * @return 资化产品主要来源国
     */
    @Override
    public List<CapitalSourceCountry> selectCapitalSourceCountryList(CapitalSourceCountry capitalSourceCountry) {
        return capitalSourceCountryMapper.selectCapitalSourceCountryList(capitalSourceCountry);
    }

    /**
     * 新增资化产品主要来源国
     * 
     * @param capitalSourceCountry 资化产品主要来源国
     * @return 结果
     */
    @Override
    public int insertCapitalSourceCountry(CapitalSourceCountry capitalSourceCountry) {
        capitalSourceCountry.setCreateTime(DateUtils.getNowDate());
        return capitalSourceCountryMapper.insertCapitalSourceCountry(capitalSourceCountry);
    }

    /**
     * 修改资化产品主要来源国
     * 
     * @param capitalSourceCountry 资化产品主要来源国
     * @return 结果
     */
    @Override
    public int updateCapitalSourceCountry(CapitalSourceCountry capitalSourceCountry) {
        capitalSourceCountry.setUpdateTime(DateUtils.getNowDate());
        return capitalSourceCountryMapper.updateCapitalSourceCountry(capitalSourceCountry);
    }

    /**
     * 批量删除资化产品主要来源国
     * 
     * @param ids 需要删除的资化产品主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteCapitalSourceCountryByIds(Long[] ids) {
        return capitalSourceCountryMapper.deleteCapitalSourceCountryByIds(ids);
    }

    /**
     * 删除资化产品主要来源国信息
     * 
     * @param id 资化产品主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteCapitalSourceCountryById(Long id) {
        return capitalSourceCountryMapper.deleteCapitalSourceCountryById(id);
    }

    /**
     * 导入资化产品主要来源国数据
     *
     * @param capitalSourceCountryList capitalSourceCountryList 资化产品主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importCapitalSourceCountry(List<CapitalSourceCountry> capitalSourceCountryList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(capitalSourceCountryList) || capitalSourceCountryList.size() == 0) {
            throw new ServiceException("导入资化产品主要来源国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (CapitalSourceCountry capitalSourceCountry : capitalSourceCountryList){
            try {
                // 验证是否存在
                if (true) {
                    capitalSourceCountry.setCreateBy(operName);
                    this.insertCapitalSourceCountry(capitalSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    capitalSourceCountry.setUpdateBy(operName);
                    this.updateCapitalSourceCountry(capitalSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
