package com.qianjing.project.capital.mapper;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalImportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品进口概况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface CapitalImportOverviewMapper {

    CapitalImportOverview selectCapitalImportOverviewById(Long id);

    List<CapitalImportOverview> selectCapitalImportOverviewList(CapitalImportOverview capitalImportOverview);

    int insertCapitalImportOverview(CapitalImportOverview capitalImportOverview);

    int updateCapitalImportOverview(CapitalImportOverview capitalImportOverview);

    int deleteCapitalImportOverviewById(Long id);

    int deleteCapitalImportOverviewByIds(Long[] ids);
}
