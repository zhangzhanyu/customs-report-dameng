package com.qianjing.project.capital.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.capital.domain.CapitalOverview;
import com.qianjing.project.capital.service.ICapitalOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 资化产品主要数量Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/capital/overview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalOverviewController extends BaseController {

    private final ICapitalOverviewService capitalOverviewService;

    /**
     * 查询资化产品主要数量列表
     */
//    @PreAuthorize("@ss.hasPermi('capital:overview:list')")
    @GetMapping("/list")
    public TableDataInfo list(CapitalOverview capitalOverview) {
        startPage();
        List<CapitalOverview> list = capitalOverviewService.selectCapitalOverviewList(capitalOverview);
        return getDataTable(list);
    }

    /**
     * 导出资化产品主要数量列表
     */
    @PreAuthorize("@ss.hasPermi('capital:overview:export')")
    @Log(title = "资化产品主要数量", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CapitalOverview capitalOverview) {
        List<CapitalOverview> list = capitalOverviewService.selectCapitalOverviewList(capitalOverview);
        ExcelUtil<CapitalOverview> util = new ExcelUtil<CapitalOverview>(CapitalOverview.class);
        return util.exportExcel(list, "资化产品主要数量数据");
    }

    /**
     * 导入资化产品主要数量列表
     */
    @Log(title = "资化产品主要数量", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('capital:overview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<CapitalOverview> util = new ExcelUtil<CapitalOverview>(CapitalOverview.class);
        List<CapitalOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = capitalOverviewService.importCapitalOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载资化产品主要数量导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<CapitalOverview> util = new ExcelUtil<CapitalOverview>(CapitalOverview.class);
        util.importTemplateExcel(response, "资化产品主要数量数据");
    }

    /**
     * 获取资化产品主要数量详细信息
     */
    @PreAuthorize("@ss.hasPermi('capital:overview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(capitalOverviewService.selectCapitalOverviewById(id));
    }

    /**
     * 新增资化产品主要数量
     */
    @PreAuthorize("@ss.hasPermi('capital:overview:add')")
    @Log(title = "资化产品主要数量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CapitalOverview capitalOverview) {
        return toAjax(capitalOverviewService.insertCapitalOverview(capitalOverview));
    }

    /**
     * 修改资化产品主要数量
     */
    @PreAuthorize("@ss.hasPermi('capital:overview:edit')")
    @Log(title = "资化产品主要数量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CapitalOverview capitalOverview) {
        return toAjax(capitalOverviewService.updateCapitalOverview(capitalOverview));
    }

    /**
     * 删除资化产品主要数量
     */
    @PreAuthorize("@ss.hasPermi('capital:overview:remove')")
    @Log(title = "资化产品主要数量", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(capitalOverviewService.deleteCapitalOverviewByIds(ids));
    }
}
