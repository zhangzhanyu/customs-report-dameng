package com.qianjing.project.capital.service;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要数量Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ICapitalOverviewService 
{
    /**
     * 查询资化产品主要数量
     * 
     * @param id 资化产品主要数量主键
     * @return 资化产品主要数量
     */
    public CapitalOverview selectCapitalOverviewById(Long id);

    /**
     * 查询资化产品主要数量列表
     * 
     * @param capitalOverview 资化产品主要数量
     * @return 资化产品主要数量集合
     */
    public List<CapitalOverview> selectCapitalOverviewList(CapitalOverview capitalOverview);

    /**
     * 新增资化产品主要数量
     * 
     * @param capitalOverview 资化产品主要数量
     * @return 结果
     */
    public int insertCapitalOverview(CapitalOverview capitalOverview);

    /**
     * 修改资化产品主要数量
     * 
     * @param capitalOverview 资化产品主要数量
     * @return 结果
     */
    public int updateCapitalOverview(CapitalOverview capitalOverview);

    /**
     * 批量删除资化产品主要数量
     * 
     * @param ids 需要删除的资化产品主要数量主键集合
     * @return 结果
     */
    public int deleteCapitalOverviewByIds(Long[] ids);

    /**
     * 删除资化产品主要数量信息
     * 
     * @param id 资化产品主要数量主键
     * @return 结果
     */
    public int deleteCapitalOverviewById(Long id);

    /**
     * 导入资化产品主要数量信息
     *
     * @param capitalOverviewList 资化产品主要数量信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importCapitalOverview(List<CapitalOverview> capitalOverviewList, Boolean isUpdateSupport, String operName);
}
