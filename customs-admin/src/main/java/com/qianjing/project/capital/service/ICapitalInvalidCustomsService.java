package com.qianjing.project.capital.service;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要关区不合格Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ICapitalInvalidCustomsService 
{
    /**
     * 查询资化产品主要关区不合格
     * 
     * @param id 资化产品主要关区不合格主键
     * @return 资化产品主要关区不合格
     */
    public CapitalInvalidCustoms selectCapitalInvalidCustomsById(Long id);

    /**
     * 查询资化产品主要关区不合格列表
     * 
     * @param capitalInvalidCustoms 资化产品主要关区不合格
     * @return 资化产品主要关区不合格集合
     */
    public List<CapitalInvalidCustoms> selectCapitalInvalidCustomsList(CapitalInvalidCustoms capitalInvalidCustoms);

    /**
     * 新增资化产品主要关区不合格
     * 
     * @param capitalInvalidCustoms 资化产品主要关区不合格
     * @return 结果
     */
    public int insertCapitalInvalidCustoms(CapitalInvalidCustoms capitalInvalidCustoms);

    /**
     * 修改资化产品主要关区不合格
     * 
     * @param capitalInvalidCustoms 资化产品主要关区不合格
     * @return 结果
     */
    public int updateCapitalInvalidCustoms(CapitalInvalidCustoms capitalInvalidCustoms);

    /**
     * 批量删除资化产品主要关区不合格
     * 
     * @param ids 需要删除的资化产品主要关区不合格主键集合
     * @return 结果
     */
    public int deleteCapitalInvalidCustomsByIds(Long[] ids);

    /**
     * 删除资化产品主要关区不合格信息
     * 
     * @param id 资化产品主要关区不合格主键
     * @return 结果
     */
    public int deleteCapitalInvalidCustomsById(Long id);

    /**
     * 导入资化产品主要关区不合格信息
     *
     * @param capitalInvalidCustomsList 资化产品主要关区不合格信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importCapitalInvalidCustoms(List<CapitalInvalidCustoms> capitalInvalidCustomsList, Boolean isUpdateSupport, String operName);
}
