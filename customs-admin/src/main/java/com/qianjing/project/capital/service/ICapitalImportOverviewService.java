package com.qianjing.project.capital.service;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalImportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品进口概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ICapitalImportOverviewService 
{
    /**
     * 查询资化产品进口概况
     * 
     * @param id 资化产品进口概况主键
     * @return 资化产品进口概况
     */
    public CapitalImportOverview selectCapitalImportOverviewById(Long id);

    /**
     * 查询资化产品进口概况列表
     * 
     * @param capitalImportOverview 资化产品进口概况
     * @return 资化产品进口概况集合
     */
    public List<CapitalImportOverview> selectCapitalImportOverviewList(CapitalImportOverview capitalImportOverview);

    /**
     * 新增资化产品进口概况
     * 
     * @param capitalImportOverview 资化产品进口概况
     * @return 结果
     */
    public int insertCapitalImportOverview(CapitalImportOverview capitalImportOverview);

    /**
     * 修改资化产品进口概况
     * 
     * @param capitalImportOverview 资化产品进口概况
     * @return 结果
     */
    public int updateCapitalImportOverview(CapitalImportOverview capitalImportOverview);

    /**
     * 批量删除资化产品进口概况
     * 
     * @param ids 需要删除的资化产品进口概况主键集合
     * @return 结果
     */
    public int deleteCapitalImportOverviewByIds(Long[] ids);

    /**
     * 删除资化产品进口概况信息
     * 
     * @param id 资化产品进口概况主键
     * @return 结果
     */
    public int deleteCapitalImportOverviewById(Long id);

    /**
     * 导入资化产品进口概况信息
     *
     * @param capitalImportOverviewList 资化产品进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importCapitalImportOverview(List<CapitalImportOverview> capitalImportOverviewList, Boolean isUpdateSupport, String operName);
}
