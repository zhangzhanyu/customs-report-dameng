package com.qianjing.project.capital.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.capital.mapper.CapitalImportCompanyMapper;
import com.qianjing.project.capital.domain.CapitalImportCompany;
import com.qianjing.project.capital.service.ICapitalImportCompanyService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要进口企业Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalImportCompanyServiceImpl implements ICapitalImportCompanyService {

    private static final Logger log = LoggerFactory.getLogger(CapitalImportCompanyServiceImpl.class);

    private final CapitalImportCompanyMapper capitalImportCompanyMapper;

    /**
     * 查询资化产品主要进口企业
     * 
     * @param id 资化产品主要进口企业主键
     * @return 资化产品主要进口企业
     */
    @Override
    public CapitalImportCompany selectCapitalImportCompanyById(Long id) {
        return capitalImportCompanyMapper.selectCapitalImportCompanyById(id);
    }

    /**
     * 查询资化产品主要进口企业列表
     * 
     * @param capitalImportCompany 资化产品主要进口企业
     * @return 资化产品主要进口企业
     */
    @Override
    public List<CapitalImportCompany> selectCapitalImportCompanyList(CapitalImportCompany capitalImportCompany) {
        return capitalImportCompanyMapper.selectCapitalImportCompanyList(capitalImportCompany);
    }

    /**
     * 新增资化产品主要进口企业
     * 
     * @param capitalImportCompany 资化产品主要进口企业
     * @return 结果
     */
    @Override
    public int insertCapitalImportCompany(CapitalImportCompany capitalImportCompany) {
        capitalImportCompany.setCreateTime(DateUtils.getNowDate());
        return capitalImportCompanyMapper.insertCapitalImportCompany(capitalImportCompany);
    }

    /**
     * 修改资化产品主要进口企业
     * 
     * @param capitalImportCompany 资化产品主要进口企业
     * @return 结果
     */
    @Override
    public int updateCapitalImportCompany(CapitalImportCompany capitalImportCompany) {
        capitalImportCompany.setUpdateTime(DateUtils.getNowDate());
        return capitalImportCompanyMapper.updateCapitalImportCompany(capitalImportCompany);
    }

    /**
     * 批量删除资化产品主要进口企业
     * 
     * @param ids 需要删除的资化产品主要进口企业主键
     * @return 结果
     */
    @Override
    public int deleteCapitalImportCompanyByIds(Long[] ids) {
        return capitalImportCompanyMapper.deleteCapitalImportCompanyByIds(ids);
    }

    /**
     * 删除资化产品主要进口企业信息
     * 
     * @param id 资化产品主要进口企业主键
     * @return 结果
     */
    @Override
    public int deleteCapitalImportCompanyById(Long id) {
        return capitalImportCompanyMapper.deleteCapitalImportCompanyById(id);
    }

    /**
     * 导入资化产品主要进口企业数据
     *
     * @param capitalImportCompanyList capitalImportCompanyList 资化产品主要进口企业信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importCapitalImportCompany(List<CapitalImportCompany> capitalImportCompanyList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(capitalImportCompanyList) || capitalImportCompanyList.size() == 0) {
            throw new ServiceException("导入资化产品主要进口企业数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (CapitalImportCompany capitalImportCompany : capitalImportCompanyList){
            try {
                // 验证是否存在
                if (true) {
                    capitalImportCompany.setCreateBy(operName);
                    this.insertCapitalImportCompany(capitalImportCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    capitalImportCompany.setUpdateBy(operName);
                    this.updateCapitalImportCompany(capitalImportCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
