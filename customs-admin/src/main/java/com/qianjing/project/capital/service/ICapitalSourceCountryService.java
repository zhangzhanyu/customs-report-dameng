package com.qianjing.project.capital.service;

import java.util.List;
import com.qianjing.project.capital.domain.CapitalSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品主要来源国Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ICapitalSourceCountryService 
{
    /**
     * 查询资化产品主要来源国
     * 
     * @param id 资化产品主要来源国主键
     * @return 资化产品主要来源国
     */
    public CapitalSourceCountry selectCapitalSourceCountryById(Long id);

    /**
     * 查询资化产品主要来源国列表
     * 
     * @param capitalSourceCountry 资化产品主要来源国
     * @return 资化产品主要来源国集合
     */
    public List<CapitalSourceCountry> selectCapitalSourceCountryList(CapitalSourceCountry capitalSourceCountry);

    /**
     * 新增资化产品主要来源国
     * 
     * @param capitalSourceCountry 资化产品主要来源国
     * @return 结果
     */
    public int insertCapitalSourceCountry(CapitalSourceCountry capitalSourceCountry);

    /**
     * 修改资化产品主要来源国
     * 
     * @param capitalSourceCountry 资化产品主要来源国
     * @return 结果
     */
    public int updateCapitalSourceCountry(CapitalSourceCountry capitalSourceCountry);

    /**
     * 批量删除资化产品主要来源国
     * 
     * @param ids 需要删除的资化产品主要来源国主键集合
     * @return 结果
     */
    public int deleteCapitalSourceCountryByIds(Long[] ids);

    /**
     * 删除资化产品主要来源国信息
     * 
     * @param id 资化产品主要来源国主键
     * @return 结果
     */
    public int deleteCapitalSourceCountryById(Long id);

    /**
     * 导入资化产品主要来源国信息
     *
     * @param capitalSourceCountryList 资化产品主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importCapitalSourceCountry(List<CapitalSourceCountry> capitalSourceCountryList, Boolean isUpdateSupport, String operName);
}
