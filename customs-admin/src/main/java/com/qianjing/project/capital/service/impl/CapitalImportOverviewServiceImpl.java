package com.qianjing.project.capital.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.capital.mapper.CapitalImportOverviewMapper;
import com.qianjing.project.capital.domain.CapitalImportOverview;
import com.qianjing.project.capital.service.ICapitalImportOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 资化产品进口概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CapitalImportOverviewServiceImpl implements ICapitalImportOverviewService {

    private static final Logger log = LoggerFactory.getLogger(CapitalImportOverviewServiceImpl.class);

    private final CapitalImportOverviewMapper capitalImportOverviewMapper;

    /**
     * 查询资化产品进口概况
     * 
     * @param id 资化产品进口概况主键
     * @return 资化产品进口概况
     */
    @Override
    public CapitalImportOverview selectCapitalImportOverviewById(Long id) {
        return capitalImportOverviewMapper.selectCapitalImportOverviewById(id);
    }

    /**
     * 查询资化产品进口概况列表
     * 
     * @param capitalImportOverview 资化产品进口概况
     * @return 资化产品进口概况
     */
    @Override
    public List<CapitalImportOverview> selectCapitalImportOverviewList(CapitalImportOverview capitalImportOverview) {
        return capitalImportOverviewMapper.selectCapitalImportOverviewList(capitalImportOverview);
    }

    /**
     * 新增资化产品进口概况
     * 
     * @param capitalImportOverview 资化产品进口概况
     * @return 结果
     */
    @Override
    public int insertCapitalImportOverview(CapitalImportOverview capitalImportOverview) {
        capitalImportOverview.setCreateTime(DateUtils.getNowDate());
        return capitalImportOverviewMapper.insertCapitalImportOverview(capitalImportOverview);
    }

    /**
     * 修改资化产品进口概况
     * 
     * @param capitalImportOverview 资化产品进口概况
     * @return 结果
     */
    @Override
    public int updateCapitalImportOverview(CapitalImportOverview capitalImportOverview) {
        capitalImportOverview.setUpdateTime(DateUtils.getNowDate());
        return capitalImportOverviewMapper.updateCapitalImportOverview(capitalImportOverview);
    }

    /**
     * 批量删除资化产品进口概况
     * 
     * @param ids 需要删除的资化产品进口概况主键
     * @return 结果
     */
    @Override
    public int deleteCapitalImportOverviewByIds(Long[] ids) {
        return capitalImportOverviewMapper.deleteCapitalImportOverviewByIds(ids);
    }

    /**
     * 删除资化产品进口概况信息
     * 
     * @param id 资化产品进口概况主键
     * @return 结果
     */
    @Override
    public int deleteCapitalImportOverviewById(Long id) {
        return capitalImportOverviewMapper.deleteCapitalImportOverviewById(id);
    }

    /**
     * 导入资化产品进口概况数据
     *
     * @param capitalImportOverviewList capitalImportOverviewList 资化产品进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importCapitalImportOverview(List<CapitalImportOverview> capitalImportOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(capitalImportOverviewList) || capitalImportOverviewList.size() == 0) {
            throw new ServiceException("导入资化产品进口概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (CapitalImportOverview capitalImportOverview : capitalImportOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    capitalImportOverview.setCreateBy(operName);
                    this.insertCapitalImportOverview(capitalImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    capitalImportOverview.setUpdateBy(operName);
                    this.updateCapitalImportOverview(capitalImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
