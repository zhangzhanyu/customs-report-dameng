package com.qianjing.project.innovation.mapper;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要来源国Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface InnovationSourceCountryMapper {

    InnovationSourceCountry selectInnovationSourceCountryById(Long id);

    List<InnovationSourceCountry> selectInnovationSourceCountryList(InnovationSourceCountry innovationSourceCountry);

    int insertInnovationSourceCountry(InnovationSourceCountry innovationSourceCountry);

    int updateInnovationSourceCountry(InnovationSourceCountry innovationSourceCountry);

    int deleteInnovationSourceCountryById(Long id);

    int deleteInnovationSourceCountryByIds(Long[] ids);
}
