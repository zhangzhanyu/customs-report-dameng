package com.qianjing.project.innovation.service;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationMeasures;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 便利化措施概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
public interface IInnovationMeasuresService 
{
    /**
     * 查询便利化措施概况
     * 
     * @param id 便利化措施概况主键
     * @return 便利化措施概况
     */
    public InnovationMeasures selectInnovationMeasuresById(Long id);

    /**
     * 查询便利化措施概况列表
     * 
     * @param innovationMeasures 便利化措施概况
     * @return 便利化措施概况集合
     */
    public List<InnovationMeasures> selectInnovationMeasuresList(InnovationMeasures innovationMeasures);

    /**
     * 新增便利化措施概况
     * 
     * @param innovationMeasures 便利化措施概况
     * @return 结果
     */
    public int insertInnovationMeasures(InnovationMeasures innovationMeasures);

    /**
     * 修改便利化措施概况
     * 
     * @param innovationMeasures 便利化措施概况
     * @return 结果
     */
    public int updateInnovationMeasures(InnovationMeasures innovationMeasures);

    /**
     * 批量删除便利化措施概况
     * 
     * @param ids 需要删除的便利化措施概况主键集合
     * @return 结果
     */
    public int deleteInnovationMeasuresByIds(Long[] ids);

    /**
     * 删除便利化措施概况信息
     * 
     * @param id 便利化措施概况主键
     * @return 结果
     */
    public int deleteInnovationMeasuresById(Long id);

    /**
     * 导入便利化措施概况信息
     *
     * @param innovationMeasuresList 便利化措施概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importInnovationMeasures(List<InnovationMeasures> innovationMeasuresList, Boolean isUpdateSupport, String operName);
}
