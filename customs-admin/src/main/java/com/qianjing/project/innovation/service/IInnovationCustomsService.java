package com.qianjing.project.innovation.service;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要开展关区Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IInnovationCustomsService 
{
    /**
     * 查询创新检验监管主要开展关区
     * 
     * @param id 创新检验监管主要开展关区主键
     * @return 创新检验监管主要开展关区
     */
    public InnovationCustoms selectInnovationCustomsById(Long id);

    /**
     * 查询创新检验监管主要开展关区列表
     * 
     * @param innovationCustoms 创新检验监管主要开展关区
     * @return 创新检验监管主要开展关区集合
     */
    public List<InnovationCustoms> selectInnovationCustomsList(InnovationCustoms innovationCustoms);

    /**
     * 新增创新检验监管主要开展关区
     * 
     * @param innovationCustoms 创新检验监管主要开展关区
     * @return 结果
     */
    public int insertInnovationCustoms(InnovationCustoms innovationCustoms);

    /**
     * 修改创新检验监管主要开展关区
     * 
     * @param innovationCustoms 创新检验监管主要开展关区
     * @return 结果
     */
    public int updateInnovationCustoms(InnovationCustoms innovationCustoms);

    /**
     * 批量删除创新检验监管主要开展关区
     * 
     * @param ids 需要删除的创新检验监管主要开展关区主键集合
     * @return 结果
     */
    public int deleteInnovationCustomsByIds(Long[] ids);

    /**
     * 删除创新检验监管主要开展关区信息
     * 
     * @param id 创新检验监管主要开展关区主键
     * @return 结果
     */
    public int deleteInnovationCustomsById(Long id);

    /**
     * 导入创新检验监管主要开展关区信息
     *
     * @param innovationCustomsList 创新检验监管主要开展关区信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importInnovationCustoms(List<InnovationCustoms> innovationCustomsList, Boolean isUpdateSupport, String operName);
}
