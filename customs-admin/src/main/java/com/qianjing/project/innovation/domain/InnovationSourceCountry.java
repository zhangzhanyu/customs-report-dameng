package com.qianjing.project.innovation.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要来源国对象 innovation_source_country
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public class InnovationSourceCountry extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 商品类别（1汽车 2摩托车 3服装） */
    @Excel(name = "商品类别", readConverterExp = "1=汽车,2=摩托车,3=服装")
    private String classification;

    /** 编号 */
    @Excel(name = "编号")
    private Long sortNum;

    /** 国别 */
    @Excel(name = "国别")
    private String item;

    /** 货物批 */
    @Excel(name = "货物批")
    private Long num;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setClassification(String classification){
        this.classification = classification;
    }

    public String getClassification(){
        return classification;
    }
    public void setSortNum(Long sortNum){
        this.sortNum = sortNum;
    }

    public Long getSortNum(){
        return sortNum;
    }
    public void setItem(String item){
        this.item = item;
    }

    public String getItem(){
        return item;
    }
    public void setNum(Long num){
        this.num = num;
    }

    public Long getNum(){
        return num;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("classification", getClassification())
            .append("sortNum", getSortNum())
            .append("item", getItem())
            .append("num", getNum())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
