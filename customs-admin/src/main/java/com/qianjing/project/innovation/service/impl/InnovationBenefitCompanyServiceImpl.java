package com.qianjing.project.innovation.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.innovation.mapper.InnovationBenefitCompanyMapper;
import com.qianjing.project.innovation.domain.InnovationBenefitCompany;
import com.qianjing.project.innovation.service.IInnovationBenefitCompanyService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要受惠企业Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationBenefitCompanyServiceImpl implements IInnovationBenefitCompanyService {

    private static final Logger log = LoggerFactory.getLogger(InnovationBenefitCompanyServiceImpl.class);

    private final InnovationBenefitCompanyMapper innovationBenefitCompanyMapper;

    /**
     * 查询创新检验监管主要受惠企业
     * 
     * @param id 创新检验监管主要受惠企业主键
     * @return 创新检验监管主要受惠企业
     */
    @Override
    public InnovationBenefitCompany selectInnovationBenefitCompanyById(Long id) {
        return innovationBenefitCompanyMapper.selectInnovationBenefitCompanyById(id);
    }

    /**
     * 查询创新检验监管主要受惠企业列表
     * 
     * @param innovationBenefitCompany 创新检验监管主要受惠企业
     * @return 创新检验监管主要受惠企业
     */
    @Override
    public List<InnovationBenefitCompany> selectInnovationBenefitCompanyList(InnovationBenefitCompany innovationBenefitCompany) {
        return innovationBenefitCompanyMapper.selectInnovationBenefitCompanyList(innovationBenefitCompany);
    }

    /**
     * 新增创新检验监管主要受惠企业
     * 
     * @param innovationBenefitCompany 创新检验监管主要受惠企业
     * @return 结果
     */
    @Override
    public int insertInnovationBenefitCompany(InnovationBenefitCompany innovationBenefitCompany) {
        innovationBenefitCompany.setCreateTime(DateUtils.getNowDate());
        return innovationBenefitCompanyMapper.insertInnovationBenefitCompany(innovationBenefitCompany);
    }

    /**
     * 修改创新检验监管主要受惠企业
     * 
     * @param innovationBenefitCompany 创新检验监管主要受惠企业
     * @return 结果
     */
    @Override
    public int updateInnovationBenefitCompany(InnovationBenefitCompany innovationBenefitCompany) {
        innovationBenefitCompany.setUpdateTime(DateUtils.getNowDate());
        return innovationBenefitCompanyMapper.updateInnovationBenefitCompany(innovationBenefitCompany);
    }

    /**
     * 批量删除创新检验监管主要受惠企业
     * 
     * @param ids 需要删除的创新检验监管主要受惠企业主键
     * @return 结果
     */
    @Override
    public int deleteInnovationBenefitCompanyByIds(Long[] ids) {
        return innovationBenefitCompanyMapper.deleteInnovationBenefitCompanyByIds(ids);
    }

    /**
     * 删除创新检验监管主要受惠企业信息
     * 
     * @param id 创新检验监管主要受惠企业主键
     * @return 结果
     */
    @Override
    public int deleteInnovationBenefitCompanyById(Long id) {
        return innovationBenefitCompanyMapper.deleteInnovationBenefitCompanyById(id);
    }

    /**
     * 导入创新检验监管主要受惠企业数据
     *
     * @param innovationBenefitCompanyList innovationBenefitCompanyList 创新检验监管主要受惠企业信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importInnovationBenefitCompany(List<InnovationBenefitCompany> innovationBenefitCompanyList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(innovationBenefitCompanyList) || innovationBenefitCompanyList.size() == 0) {
            throw new ServiceException("导入创新检验监管主要受惠企业数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (InnovationBenefitCompany innovationBenefitCompany : innovationBenefitCompanyList){
            try {
                // 验证是否存在
                if (true) {
                    innovationBenefitCompany.setCreateBy(operName);
                    this.insertInnovationBenefitCompany(innovationBenefitCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    innovationBenefitCompany.setUpdateBy(operName);
                    this.updateInnovationBenefitCompany(innovationBenefitCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
