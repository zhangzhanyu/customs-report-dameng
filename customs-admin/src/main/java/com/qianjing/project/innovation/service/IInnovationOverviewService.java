package com.qianjing.project.innovation.service;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
public interface IInnovationOverviewService 
{
    /**
     * 查询创新检验监管概况
     * 
     * @param id 创新检验监管概况主键
     * @return 创新检验监管概况
     */
    public InnovationOverview selectInnovationOverviewById(Long id);

    /**
     * 查询创新检验监管概况列表
     * 
     * @param innovationOverview 创新检验监管概况
     * @return 创新检验监管概况集合
     */
    public List<InnovationOverview> selectInnovationOverviewList(InnovationOverview innovationOverview);

    /**
     * 新增创新检验监管概况
     * 
     * @param innovationOverview 创新检验监管概况
     * @return 结果
     */
    public int insertInnovationOverview(InnovationOverview innovationOverview);

    /**
     * 修改创新检验监管概况
     * 
     * @param innovationOverview 创新检验监管概况
     * @return 结果
     */
    public int updateInnovationOverview(InnovationOverview innovationOverview);

    /**
     * 批量删除创新检验监管概况
     * 
     * @param ids 需要删除的创新检验监管概况主键集合
     * @return 结果
     */
    public int deleteInnovationOverviewByIds(Long[] ids);

    /**
     * 删除创新检验监管概况信息
     * 
     * @param id 创新检验监管概况主键
     * @return 结果
     */
    public int deleteInnovationOverviewById(Long id);

    /**
     * 导入创新检验监管概况信息
     *
     * @param innovationOverviewList 创新检验监管概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importInnovationOverview(List<InnovationOverview> innovationOverviewList, Boolean isUpdateSupport, String operName);
}
