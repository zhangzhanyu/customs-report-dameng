package com.qianjing.project.innovation.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.innovation.mapper.InnovationSourceCountryMapper;
import com.qianjing.project.innovation.domain.InnovationSourceCountry;
import com.qianjing.project.innovation.service.IInnovationSourceCountryService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要来源国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationSourceCountryServiceImpl implements IInnovationSourceCountryService {

    private static final Logger log = LoggerFactory.getLogger(InnovationSourceCountryServiceImpl.class);

    private final InnovationSourceCountryMapper innovationSourceCountryMapper;

    /**
     * 查询创新检验监管主要来源国
     * 
     * @param id 创新检验监管主要来源国主键
     * @return 创新检验监管主要来源国
     */
    @Override
    public InnovationSourceCountry selectInnovationSourceCountryById(Long id) {
        return innovationSourceCountryMapper.selectInnovationSourceCountryById(id);
    }

    /**
     * 查询创新检验监管主要来源国列表
     * 
     * @param innovationSourceCountry 创新检验监管主要来源国
     * @return 创新检验监管主要来源国
     */
    @Override
    public List<InnovationSourceCountry> selectInnovationSourceCountryList(InnovationSourceCountry innovationSourceCountry) {
        return innovationSourceCountryMapper.selectInnovationSourceCountryList(innovationSourceCountry);
    }

    /**
     * 新增创新检验监管主要来源国
     * 
     * @param innovationSourceCountry 创新检验监管主要来源国
     * @return 结果
     */
    @Override
    public int insertInnovationSourceCountry(InnovationSourceCountry innovationSourceCountry) {
        innovationSourceCountry.setCreateTime(DateUtils.getNowDate());
        return innovationSourceCountryMapper.insertInnovationSourceCountry(innovationSourceCountry);
    }

    /**
     * 修改创新检验监管主要来源国
     * 
     * @param innovationSourceCountry 创新检验监管主要来源国
     * @return 结果
     */
    @Override
    public int updateInnovationSourceCountry(InnovationSourceCountry innovationSourceCountry) {
        innovationSourceCountry.setUpdateTime(DateUtils.getNowDate());
        return innovationSourceCountryMapper.updateInnovationSourceCountry(innovationSourceCountry);
    }

    /**
     * 批量删除创新检验监管主要来源国
     * 
     * @param ids 需要删除的创新检验监管主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteInnovationSourceCountryByIds(Long[] ids) {
        return innovationSourceCountryMapper.deleteInnovationSourceCountryByIds(ids);
    }

    /**
     * 删除创新检验监管主要来源国信息
     * 
     * @param id 创新检验监管主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteInnovationSourceCountryById(Long id) {
        return innovationSourceCountryMapper.deleteInnovationSourceCountryById(id);
    }

    /**
     * 导入创新检验监管主要来源国数据
     *
     * @param innovationSourceCountryList innovationSourceCountryList 创新检验监管主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importInnovationSourceCountry(List<InnovationSourceCountry> innovationSourceCountryList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(innovationSourceCountryList) || innovationSourceCountryList.size() == 0) {
            throw new ServiceException("导入创新检验监管主要来源国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (InnovationSourceCountry innovationSourceCountry : innovationSourceCountryList){
            try {
                // 验证是否存在
                if (true) {
                    innovationSourceCountry.setCreateBy(operName);
                    this.insertInnovationSourceCountry(innovationSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    innovationSourceCountry.setUpdateBy(operName);
                    this.updateInnovationSourceCountry(innovationSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
