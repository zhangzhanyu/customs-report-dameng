package com.qianjing.project.innovation.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管概况对象 innovation_overview
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
public class InnovationOverview extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 商品类别（1汽车 2摩托车 3服装） */
    @Excel(name = "商品类别", readConverterExp = "1=汽车,2=摩托车,3=服装")
    private String classification;

    /** 便利化批次(货物批) */
    @Excel(name = "便利化批次(货物批)")
    private Long innovationEntryBatch;

    /** 节约通关时间(天) */
    @Excel(name = "节约通关时间(天)")
    private Long saveTime;

    /** 节约通关成本 */
    @Excel(name = "节约通关成本")
    private BigDecimal saveCost;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 采信机构数量 */
    @Excel(name = "采信机构数量")
    private Long creditNum;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setClassification(String classification){
        this.classification = classification;
    }

    public String getClassification(){
        return classification;
    }
    public void setInnovationEntryBatch(Long innovationEntryBatch){
        this.innovationEntryBatch = innovationEntryBatch;
    }

    public Long getInnovationEntryBatch(){
        return innovationEntryBatch;
    }
    public void setSaveTime(Long saveTime){
        this.saveTime = saveTime;
    }

    public Long getSaveTime(){
        return saveTime;
    }
    public void setSaveCost(BigDecimal saveCost){
        this.saveCost = saveCost;
    }

    public BigDecimal getSaveCost(){
        return saveCost;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setCreditNum(Long creditNum){
        this.creditNum = creditNum;
    }

    public Long getCreditNum(){
        return creditNum;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("classification", getClassification())
            .append("innovationEntryBatch", getInnovationEntryBatch())
            .append("saveTime", getSaveTime())
            .append("saveCost", getSaveCost())
            .append("delFlag", getDelFlag())
            .append("creditNum", getCreditNum())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
