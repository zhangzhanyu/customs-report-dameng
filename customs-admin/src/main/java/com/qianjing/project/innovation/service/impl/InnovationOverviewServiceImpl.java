package com.qianjing.project.innovation.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.innovation.mapper.InnovationOverviewMapper;
import com.qianjing.project.innovation.domain.InnovationOverview;
import com.qianjing.project.innovation.service.IInnovationOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationOverviewServiceImpl implements IInnovationOverviewService {

    private static final Logger log = LoggerFactory.getLogger(InnovationOverviewServiceImpl.class);

    private final InnovationOverviewMapper innovationOverviewMapper;

    /**
     * 查询创新检验监管概况
     * 
     * @param id 创新检验监管概况主键
     * @return 创新检验监管概况
     */
    @Override
    public InnovationOverview selectInnovationOverviewById(Long id) {
        return innovationOverviewMapper.selectInnovationOverviewById(id);
    }

    /**
     * 查询创新检验监管概况列表
     * 
     * @param innovationOverview 创新检验监管概况
     * @return 创新检验监管概况
     */
    @Override
    public List<InnovationOverview> selectInnovationOverviewList(InnovationOverview innovationOverview) {
        return innovationOverviewMapper.selectInnovationOverviewList(innovationOverview);
    }

    /**
     * 新增创新检验监管概况
     * 
     * @param innovationOverview 创新检验监管概况
     * @return 结果
     */
    @Override
    public int insertInnovationOverview(InnovationOverview innovationOverview) {
        innovationOverview.setCreateTime(DateUtils.getNowDate());
        return innovationOverviewMapper.insertInnovationOverview(innovationOverview);
    }

    /**
     * 修改创新检验监管概况
     * 
     * @param innovationOverview 创新检验监管概况
     * @return 结果
     */
    @Override
    public int updateInnovationOverview(InnovationOverview innovationOverview) {
        innovationOverview.setUpdateTime(DateUtils.getNowDate());
        return innovationOverviewMapper.updateInnovationOverview(innovationOverview);
    }

    /**
     * 批量删除创新检验监管概况
     * 
     * @param ids 需要删除的创新检验监管概况主键
     * @return 结果
     */
    @Override
    public int deleteInnovationOverviewByIds(Long[] ids) {
        return innovationOverviewMapper.deleteInnovationOverviewByIds(ids);
    }

    /**
     * 删除创新检验监管概况信息
     * 
     * @param id 创新检验监管概况主键
     * @return 结果
     */
    @Override
    public int deleteInnovationOverviewById(Long id) {
        return innovationOverviewMapper.deleteInnovationOverviewById(id);
    }

    /**
     * 导入创新检验监管概况数据
     *
     * @param innovationOverviewList innovationOverviewList 创新检验监管概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importInnovationOverview(List<InnovationOverview> innovationOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(innovationOverviewList) || innovationOverviewList.size() == 0) {
            throw new ServiceException("导入创新检验监管概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (InnovationOverview innovationOverview : innovationOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    innovationOverview.setCreateBy(operName);
                    this.insertInnovationOverview(innovationOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    innovationOverview.setUpdateBy(operName);
                    this.updateInnovationOverview(innovationOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
