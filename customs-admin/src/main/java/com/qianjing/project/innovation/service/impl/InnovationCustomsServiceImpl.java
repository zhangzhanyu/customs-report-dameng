package com.qianjing.project.innovation.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.innovation.mapper.InnovationCustomsMapper;
import com.qianjing.project.innovation.domain.InnovationCustoms;
import com.qianjing.project.innovation.service.IInnovationCustomsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要开展关区Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationCustomsServiceImpl implements IInnovationCustomsService {

    private static final Logger log = LoggerFactory.getLogger(InnovationCustomsServiceImpl.class);

    private final InnovationCustomsMapper innovationCustomsMapper;

    /**
     * 查询创新检验监管主要开展关区
     * 
     * @param id 创新检验监管主要开展关区主键
     * @return 创新检验监管主要开展关区
     */
    @Override
    public InnovationCustoms selectInnovationCustomsById(Long id) {
        return innovationCustomsMapper.selectInnovationCustomsById(id);
    }

    /**
     * 查询创新检验监管主要开展关区列表
     * 
     * @param innovationCustoms 创新检验监管主要开展关区
     * @return 创新检验监管主要开展关区
     */
    @Override
    public List<InnovationCustoms> selectInnovationCustomsList(InnovationCustoms innovationCustoms) {
        return innovationCustomsMapper.selectInnovationCustomsList(innovationCustoms);
    }

    /**
     * 新增创新检验监管主要开展关区
     * 
     * @param innovationCustoms 创新检验监管主要开展关区
     * @return 结果
     */
    @Override
    public int insertInnovationCustoms(InnovationCustoms innovationCustoms) {
        innovationCustoms.setCreateTime(DateUtils.getNowDate());
        return innovationCustomsMapper.insertInnovationCustoms(innovationCustoms);
    }

    /**
     * 修改创新检验监管主要开展关区
     * 
     * @param innovationCustoms 创新检验监管主要开展关区
     * @return 结果
     */
    @Override
    public int updateInnovationCustoms(InnovationCustoms innovationCustoms) {
        innovationCustoms.setUpdateTime(DateUtils.getNowDate());
        return innovationCustomsMapper.updateInnovationCustoms(innovationCustoms);
    }

    /**
     * 批量删除创新检验监管主要开展关区
     * 
     * @param ids 需要删除的创新检验监管主要开展关区主键
     * @return 结果
     */
    @Override
    public int deleteInnovationCustomsByIds(Long[] ids) {
        return innovationCustomsMapper.deleteInnovationCustomsByIds(ids);
    }

    /**
     * 删除创新检验监管主要开展关区信息
     * 
     * @param id 创新检验监管主要开展关区主键
     * @return 结果
     */
    @Override
    public int deleteInnovationCustomsById(Long id) {
        return innovationCustomsMapper.deleteInnovationCustomsById(id);
    }

    /**
     * 导入创新检验监管主要开展关区数据
     *
     * @param innovationCustomsList innovationCustomsList 创新检验监管主要开展关区信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importInnovationCustoms(List<InnovationCustoms> innovationCustomsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(innovationCustomsList) || innovationCustomsList.size() == 0) {
            throw new ServiceException("导入创新检验监管主要开展关区数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (InnovationCustoms innovationCustoms : innovationCustomsList){
            try {
                // 验证是否存在
                if (true) {
                    innovationCustoms.setCreateBy(operName);
                    this.insertInnovationCustoms(innovationCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    innovationCustoms.setUpdateBy(operName);
                    this.updateInnovationCustoms(innovationCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
