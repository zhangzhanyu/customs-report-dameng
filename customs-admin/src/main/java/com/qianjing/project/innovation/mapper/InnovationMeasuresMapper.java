package com.qianjing.project.innovation.mapper;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationMeasures;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 便利化措施概况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
public interface InnovationMeasuresMapper {

    InnovationMeasures selectInnovationMeasuresById(Long id);

    List<InnovationMeasures> selectInnovationMeasuresList(InnovationMeasures innovationMeasures);

    int insertInnovationMeasures(InnovationMeasures innovationMeasures);

    int updateInnovationMeasures(InnovationMeasures innovationMeasures);

    int deleteInnovationMeasuresById(Long id);

    int deleteInnovationMeasuresByIds(Long[] ids);
}
