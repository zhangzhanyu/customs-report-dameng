package com.qianjing.project.innovation.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.innovation.domain.InnovationBenefitCompany;
import com.qianjing.project.innovation.service.IInnovationBenefitCompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 创新检验监管主要受惠企业Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/innovation/company")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationBenefitCompanyController extends BaseController {

    private final IInnovationBenefitCompanyService innovationBenefitCompanyService;

    /**
     * 查询创新检验监管主要受惠企业列表
     */
//    @PreAuthorize("@ss.hasPermi('innovation:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(InnovationBenefitCompany innovationBenefitCompany) {
        startPage();
        List<InnovationBenefitCompany> list = innovationBenefitCompanyService.selectInnovationBenefitCompanyList(innovationBenefitCompany);
        return getDataTable(list);
    }

    /**
     * 导出创新检验监管主要受惠企业列表
     */
    @PreAuthorize("@ss.hasPermi('innovation:company:export')")
    @Log(title = "创新检验监管主要受惠企业", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(InnovationBenefitCompany innovationBenefitCompany) {
        List<InnovationBenefitCompany> list = innovationBenefitCompanyService.selectInnovationBenefitCompanyList(innovationBenefitCompany);
        ExcelUtil<InnovationBenefitCompany> util = new ExcelUtil<InnovationBenefitCompany>(InnovationBenefitCompany.class);
        return util.exportExcel(list, "创新检验监管主要受惠企业数据");
    }

    /**
     * 导入创新检验监管主要受惠企业列表
     */
    @Log(title = "创新检验监管主要受惠企业", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('innovation:company:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<InnovationBenefitCompany> util = new ExcelUtil<InnovationBenefitCompany>(InnovationBenefitCompany.class);
        List<InnovationBenefitCompany> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = innovationBenefitCompanyService.importInnovationBenefitCompany(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载创新检验监管主要受惠企业导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<InnovationBenefitCompany> util = new ExcelUtil<InnovationBenefitCompany>(InnovationBenefitCompany.class);
        util.importTemplateExcel(response, "创新检验监管主要受惠企业数据");
    }

    /**
     * 获取创新检验监管主要受惠企业详细信息
     */
    @PreAuthorize("@ss.hasPermi('innovation:company:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(innovationBenefitCompanyService.selectInnovationBenefitCompanyById(id));
    }

    /**
     * 新增创新检验监管主要受惠企业
     */
    @PreAuthorize("@ss.hasPermi('innovation:company:add')")
    @Log(title = "创新检验监管主要受惠企业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InnovationBenefitCompany innovationBenefitCompany) {
        return toAjax(innovationBenefitCompanyService.insertInnovationBenefitCompany(innovationBenefitCompany));
    }

    /**
     * 修改创新检验监管主要受惠企业
     */
    @PreAuthorize("@ss.hasPermi('innovation:company:edit')")
    @Log(title = "创新检验监管主要受惠企业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InnovationBenefitCompany innovationBenefitCompany) {
        return toAjax(innovationBenefitCompanyService.updateInnovationBenefitCompany(innovationBenefitCompany));
    }

    /**
     * 删除创新检验监管主要受惠企业
     */
    @PreAuthorize("@ss.hasPermi('innovation:company:remove')")
    @Log(title = "创新检验监管主要受惠企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(innovationBenefitCompanyService.deleteInnovationBenefitCompanyByIds(ids));
    }
}
