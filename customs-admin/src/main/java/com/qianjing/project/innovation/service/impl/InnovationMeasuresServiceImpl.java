package com.qianjing.project.innovation.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.innovation.mapper.InnovationMeasuresMapper;
import com.qianjing.project.innovation.domain.InnovationMeasures;
import com.qianjing.project.innovation.service.IInnovationMeasuresService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 便利化措施概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationMeasuresServiceImpl implements IInnovationMeasuresService {

    private static final Logger log = LoggerFactory.getLogger(InnovationMeasuresServiceImpl.class);

    private final InnovationMeasuresMapper innovationMeasuresMapper;

    /**
     * 查询便利化措施概况
     * 
     * @param id 便利化措施概况主键
     * @return 便利化措施概况
     */
    @Override
    public InnovationMeasures selectInnovationMeasuresById(Long id) {
        return innovationMeasuresMapper.selectInnovationMeasuresById(id);
    }

    /**
     * 查询便利化措施概况列表
     * 
     * @param innovationMeasures 便利化措施概况
     * @return 便利化措施概况
     */
    @Override
    public List<InnovationMeasures> selectInnovationMeasuresList(InnovationMeasures innovationMeasures) {
        return innovationMeasuresMapper.selectInnovationMeasuresList(innovationMeasures);
    }

    /**
     * 新增便利化措施概况
     * 
     * @param innovationMeasures 便利化措施概况
     * @return 结果
     */
    @Override
    public int insertInnovationMeasures(InnovationMeasures innovationMeasures) {
        innovationMeasures.setCreateTime(DateUtils.getNowDate());
        return innovationMeasuresMapper.insertInnovationMeasures(innovationMeasures);
    }

    /**
     * 修改便利化措施概况
     * 
     * @param innovationMeasures 便利化措施概况
     * @return 结果
     */
    @Override
    public int updateInnovationMeasures(InnovationMeasures innovationMeasures) {
        innovationMeasures.setUpdateTime(DateUtils.getNowDate());
        return innovationMeasuresMapper.updateInnovationMeasures(innovationMeasures);
    }

    /**
     * 批量删除便利化措施概况
     * 
     * @param ids 需要删除的便利化措施概况主键
     * @return 结果
     */
    @Override
    public int deleteInnovationMeasuresByIds(Long[] ids) {
        return innovationMeasuresMapper.deleteInnovationMeasuresByIds(ids);
    }

    /**
     * 删除便利化措施概况信息
     * 
     * @param id 便利化措施概况主键
     * @return 结果
     */
    @Override
    public int deleteInnovationMeasuresById(Long id) {
        return innovationMeasuresMapper.deleteInnovationMeasuresById(id);
    }

    /**
     * 导入便利化措施概况数据
     *
     * @param innovationMeasuresList innovationMeasuresList 便利化措施概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importInnovationMeasures(List<InnovationMeasures> innovationMeasuresList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(innovationMeasuresList) || innovationMeasuresList.size() == 0) {
            throw new ServiceException("导入便利化措施概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (InnovationMeasures innovationMeasures : innovationMeasuresList){
            try {
                // 验证是否存在
                if (true) {
                    innovationMeasures.setCreateBy(operName);
                    this.insertInnovationMeasures(innovationMeasures);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    innovationMeasures.setUpdateBy(operName);
                    this.updateInnovationMeasures(innovationMeasures);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
