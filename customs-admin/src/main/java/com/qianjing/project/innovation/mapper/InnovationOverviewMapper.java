package com.qianjing.project.innovation.mapper;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管概况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
public interface InnovationOverviewMapper {

    InnovationOverview selectInnovationOverviewById(Long id);

    List<InnovationOverview> selectInnovationOverviewList(InnovationOverview innovationOverview);

    int insertInnovationOverview(InnovationOverview innovationOverview);

    int updateInnovationOverview(InnovationOverview innovationOverview);

    int deleteInnovationOverviewById(Long id);

    int deleteInnovationOverviewByIds(Long[] ids);
}
