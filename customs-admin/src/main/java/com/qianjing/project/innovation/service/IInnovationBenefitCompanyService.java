package com.qianjing.project.innovation.service;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationBenefitCompany;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要受惠企业Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IInnovationBenefitCompanyService 
{
    /**
     * 查询创新检验监管主要受惠企业
     * 
     * @param id 创新检验监管主要受惠企业主键
     * @return 创新检验监管主要受惠企业
     */
    public InnovationBenefitCompany selectInnovationBenefitCompanyById(Long id);

    /**
     * 查询创新检验监管主要受惠企业列表
     * 
     * @param innovationBenefitCompany 创新检验监管主要受惠企业
     * @return 创新检验监管主要受惠企业集合
     */
    public List<InnovationBenefitCompany> selectInnovationBenefitCompanyList(InnovationBenefitCompany innovationBenefitCompany);

    /**
     * 新增创新检验监管主要受惠企业
     * 
     * @param innovationBenefitCompany 创新检验监管主要受惠企业
     * @return 结果
     */
    public int insertInnovationBenefitCompany(InnovationBenefitCompany innovationBenefitCompany);

    /**
     * 修改创新检验监管主要受惠企业
     * 
     * @param innovationBenefitCompany 创新检验监管主要受惠企业
     * @return 结果
     */
    public int updateInnovationBenefitCompany(InnovationBenefitCompany innovationBenefitCompany);

    /**
     * 批量删除创新检验监管主要受惠企业
     * 
     * @param ids 需要删除的创新检验监管主要受惠企业主键集合
     * @return 结果
     */
    public int deleteInnovationBenefitCompanyByIds(Long[] ids);

    /**
     * 删除创新检验监管主要受惠企业信息
     * 
     * @param id 创新检验监管主要受惠企业主键
     * @return 结果
     */
    public int deleteInnovationBenefitCompanyById(Long id);

    /**
     * 导入创新检验监管主要受惠企业信息
     *
     * @param innovationBenefitCompanyList 创新检验监管主要受惠企业信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importInnovationBenefitCompany(List<InnovationBenefitCompany> innovationBenefitCompanyList, Boolean isUpdateSupport, String operName);
}
