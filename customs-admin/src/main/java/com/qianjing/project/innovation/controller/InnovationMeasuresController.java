package com.qianjing.project.innovation.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.innovation.domain.InnovationMeasures;
import com.qianjing.project.innovation.service.IInnovationMeasuresService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 便利化措施概况Controller
 *
 * @Created by 张占宇 2023-06-15 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/innovation/measures")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationMeasuresController extends BaseController {

    private final IInnovationMeasuresService innovationMeasuresService;

    /**
     * 查询便利化措施概况列表
     */
//    @PreAuthorize("@ss.hasPermi('innovation:measures:list')")
    @GetMapping("/list")
    public TableDataInfo list(InnovationMeasures innovationMeasures) {
        startPage();
        List<InnovationMeasures> list = innovationMeasuresService.selectInnovationMeasuresList(innovationMeasures);
        return getDataTable(list);
    }

    /**
     * 导出便利化措施概况列表
     */
    @PreAuthorize("@ss.hasPermi('innovation:measures:export')")
    @Log(title = "便利化措施概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(InnovationMeasures innovationMeasures) {
        List<InnovationMeasures> list = innovationMeasuresService.selectInnovationMeasuresList(innovationMeasures);
        ExcelUtil<InnovationMeasures> util = new ExcelUtil<InnovationMeasures>(InnovationMeasures.class);
        return util.exportExcel(list, "便利化措施概况数据");
    }

    /**
     * 导入便利化措施概况列表
     */
    @Log(title = "便利化措施概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('innovation:measures:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<InnovationMeasures> util = new ExcelUtil<InnovationMeasures>(InnovationMeasures.class);
        List<InnovationMeasures> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = innovationMeasuresService.importInnovationMeasures(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载便利化措施概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<InnovationMeasures> util = new ExcelUtil<InnovationMeasures>(InnovationMeasures.class);
        util.importTemplateExcel(response, "便利化措施概况数据");
    }

    /**
     * 获取便利化措施概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('innovation:measures:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(innovationMeasuresService.selectInnovationMeasuresById(id));
    }

    /**
     * 新增便利化措施概况
     */
    @PreAuthorize("@ss.hasPermi('innovation:measures:add')")
    @Log(title = "便利化措施概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InnovationMeasures innovationMeasures) {
        return toAjax(innovationMeasuresService.insertInnovationMeasures(innovationMeasures));
    }

    /**
     * 修改便利化措施概况
     */
    @PreAuthorize("@ss.hasPermi('innovation:measures:edit')")
    @Log(title = "便利化措施概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InnovationMeasures innovationMeasures) {
        return toAjax(innovationMeasuresService.updateInnovationMeasures(innovationMeasures));
    }

    /**
     * 删除便利化措施概况
     */
    @PreAuthorize("@ss.hasPermi('innovation:measures:remove')")
    @Log(title = "便利化措施概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(innovationMeasuresService.deleteInnovationMeasuresByIds(ids));
    }
}
