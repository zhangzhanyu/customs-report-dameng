package com.qianjing.project.innovation.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.innovation.domain.InnovationSourceCountry;
import com.qianjing.project.innovation.service.IInnovationSourceCountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 创新检验监管主要来源国Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/innovation/country")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationSourceCountryController extends BaseController {

    private final IInnovationSourceCountryService innovationSourceCountryService;

    /**
     * 查询创新检验监管主要来源国列表
     */
//    @PreAuthorize("@ss.hasPermi('innovation:country:list')")
    @GetMapping("/list")
    public TableDataInfo list(InnovationSourceCountry innovationSourceCountry) {
        startPage();
        List<InnovationSourceCountry> list = innovationSourceCountryService.selectInnovationSourceCountryList(innovationSourceCountry);
        return getDataTable(list);
    }

    /**
     * 导出创新检验监管主要来源国列表
     */
    @PreAuthorize("@ss.hasPermi('innovation:country:export')")
    @Log(title = "创新检验监管主要来源国", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(InnovationSourceCountry innovationSourceCountry) {
        List<InnovationSourceCountry> list = innovationSourceCountryService.selectInnovationSourceCountryList(innovationSourceCountry);
        ExcelUtil<InnovationSourceCountry> util = new ExcelUtil<InnovationSourceCountry>(InnovationSourceCountry.class);
        return util.exportExcel(list, "创新检验监管主要来源国数据");
    }

    /**
     * 导入创新检验监管主要来源国列表
     */
    @Log(title = "创新检验监管主要来源国", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('innovation:country:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<InnovationSourceCountry> util = new ExcelUtil<InnovationSourceCountry>(InnovationSourceCountry.class);
        List<InnovationSourceCountry> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = innovationSourceCountryService.importInnovationSourceCountry(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载创新检验监管主要来源国导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<InnovationSourceCountry> util = new ExcelUtil<InnovationSourceCountry>(InnovationSourceCountry.class);
        util.importTemplateExcel(response, "创新检验监管主要来源国数据");
    }

    /**
     * 获取创新检验监管主要来源国详细信息
     */
    @PreAuthorize("@ss.hasPermi('innovation:country:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(innovationSourceCountryService.selectInnovationSourceCountryById(id));
    }

    /**
     * 新增创新检验监管主要来源国
     */
    @PreAuthorize("@ss.hasPermi('innovation:country:add')")
    @Log(title = "创新检验监管主要来源国", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InnovationSourceCountry innovationSourceCountry) {
        return toAjax(innovationSourceCountryService.insertInnovationSourceCountry(innovationSourceCountry));
    }

    /**
     * 修改创新检验监管主要来源国
     */
    @PreAuthorize("@ss.hasPermi('innovation:country:edit')")
    @Log(title = "创新检验监管主要来源国", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InnovationSourceCountry innovationSourceCountry) {
        return toAjax(innovationSourceCountryService.updateInnovationSourceCountry(innovationSourceCountry));
    }

    /**
     * 删除创新检验监管主要来源国
     */
    @PreAuthorize("@ss.hasPermi('innovation:country:remove')")
    @Log(title = "创新检验监管主要来源国", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(innovationSourceCountryService.deleteInnovationSourceCountryByIds(ids));
    }
}
