package com.qianjing.project.innovation.mapper;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要开展关区Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface InnovationCustomsMapper {

    InnovationCustoms selectInnovationCustomsById(Long id);

    List<InnovationCustoms> selectInnovationCustomsList(InnovationCustoms innovationCustoms);

    int insertInnovationCustoms(InnovationCustoms innovationCustoms);

    int updateInnovationCustoms(InnovationCustoms innovationCustoms);

    int deleteInnovationCustomsById(Long id);

    int deleteInnovationCustomsByIds(Long[] ids);
}
