package com.qianjing.project.innovation.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.innovation.domain.InnovationOverview;
import com.qianjing.project.innovation.service.IInnovationOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 创新检验监管概况Controller
 *
 * @Created by 张占宇 2023-06-15 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/innovation/overview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationOverviewController extends BaseController {

    private final IInnovationOverviewService innovationOverviewService;

    /**
     * 查询创新检验监管概况列表
     */
//    @PreAuthorize("@ss.hasPermi('innovation:overview:list')")
    @GetMapping("/list")
    public TableDataInfo list(InnovationOverview innovationOverview) {
        startPage();
        List<InnovationOverview> list = innovationOverviewService.selectInnovationOverviewList(innovationOverview);
        return getDataTable(list);
    }

    /**
     * 导出创新检验监管概况列表
     */
    @PreAuthorize("@ss.hasPermi('innovation:overview:export')")
    @Log(title = "创新检验监管概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(InnovationOverview innovationOverview) {
        List<InnovationOverview> list = innovationOverviewService.selectInnovationOverviewList(innovationOverview);
        ExcelUtil<InnovationOverview> util = new ExcelUtil<InnovationOverview>(InnovationOverview.class);
        return util.exportExcel(list, "创新检验监管概况数据");
    }

    /**
     * 导入创新检验监管概况列表
     */
    @Log(title = "创新检验监管概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('innovation:overview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<InnovationOverview> util = new ExcelUtil<InnovationOverview>(InnovationOverview.class);
        List<InnovationOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = innovationOverviewService.importInnovationOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载创新检验监管概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<InnovationOverview> util = new ExcelUtil<InnovationOverview>(InnovationOverview.class);
        util.importTemplateExcel(response, "创新检验监管概况数据");
    }

    /**
     * 获取创新检验监管概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('innovation:overview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(innovationOverviewService.selectInnovationOverviewById(id));
    }

    /**
     * 新增创新检验监管概况
     */
    @PreAuthorize("@ss.hasPermi('innovation:overview:add')")
    @Log(title = "创新检验监管概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InnovationOverview innovationOverview) {
        return toAjax(innovationOverviewService.insertInnovationOverview(innovationOverview));
    }

    /**
     * 修改创新检验监管概况
     */
    @PreAuthorize("@ss.hasPermi('innovation:overview:edit')")
    @Log(title = "创新检验监管概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InnovationOverview innovationOverview) {
        return toAjax(innovationOverviewService.updateInnovationOverview(innovationOverview));
    }

    /**
     * 删除创新检验监管概况
     */
    @PreAuthorize("@ss.hasPermi('innovation:overview:remove')")
    @Log(title = "创新检验监管概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(innovationOverviewService.deleteInnovationOverviewByIds(ids));
    }
}
