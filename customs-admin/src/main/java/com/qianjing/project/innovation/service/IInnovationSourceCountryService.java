package com.qianjing.project.innovation.service;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要来源国Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IInnovationSourceCountryService 
{
    /**
     * 查询创新检验监管主要来源国
     * 
     * @param id 创新检验监管主要来源国主键
     * @return 创新检验监管主要来源国
     */
    public InnovationSourceCountry selectInnovationSourceCountryById(Long id);

    /**
     * 查询创新检验监管主要来源国列表
     * 
     * @param innovationSourceCountry 创新检验监管主要来源国
     * @return 创新检验监管主要来源国集合
     */
    public List<InnovationSourceCountry> selectInnovationSourceCountryList(InnovationSourceCountry innovationSourceCountry);

    /**
     * 新增创新检验监管主要来源国
     * 
     * @param innovationSourceCountry 创新检验监管主要来源国
     * @return 结果
     */
    public int insertInnovationSourceCountry(InnovationSourceCountry innovationSourceCountry);

    /**
     * 修改创新检验监管主要来源国
     * 
     * @param innovationSourceCountry 创新检验监管主要来源国
     * @return 结果
     */
    public int updateInnovationSourceCountry(InnovationSourceCountry innovationSourceCountry);

    /**
     * 批量删除创新检验监管主要来源国
     * 
     * @param ids 需要删除的创新检验监管主要来源国主键集合
     * @return 结果
     */
    public int deleteInnovationSourceCountryByIds(Long[] ids);

    /**
     * 删除创新检验监管主要来源国信息
     * 
     * @param id 创新检验监管主要来源国主键
     * @return 结果
     */
    public int deleteInnovationSourceCountryById(Long id);

    /**
     * 导入创新检验监管主要来源国信息
     *
     * @param innovationSourceCountryList 创新检验监管主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importInnovationSourceCountry(List<InnovationSourceCountry> innovationSourceCountryList, Boolean isUpdateSupport, String operName);
}
