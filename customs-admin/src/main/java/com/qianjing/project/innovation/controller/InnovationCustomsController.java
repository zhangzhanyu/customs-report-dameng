package com.qianjing.project.innovation.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.innovation.domain.InnovationCustoms;
import com.qianjing.project.innovation.service.IInnovationCustomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 创新检验监管主要开展关区Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/innovation/customs")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class InnovationCustomsController extends BaseController {

    private final IInnovationCustomsService innovationCustomsService;

    /**
     * 查询创新检验监管主要开展关区列表
     */
//    @PreAuthorize("@ss.hasPermi('innovation:customs:list')")
    @GetMapping("/list")
    public TableDataInfo list(InnovationCustoms innovationCustoms) {
        startPage();
        List<InnovationCustoms> list = innovationCustomsService.selectInnovationCustomsList(innovationCustoms);
        return getDataTable(list);
    }

    /**
     * 导出创新检验监管主要开展关区列表
     */
    @PreAuthorize("@ss.hasPermi('innovation:customs:export')")
    @Log(title = "创新检验监管主要开展关区", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(InnovationCustoms innovationCustoms) {
        List<InnovationCustoms> list = innovationCustomsService.selectInnovationCustomsList(innovationCustoms);
        ExcelUtil<InnovationCustoms> util = new ExcelUtil<InnovationCustoms>(InnovationCustoms.class);
        return util.exportExcel(list, "创新检验监管主要开展关区数据");
    }

    /**
     * 导入创新检验监管主要开展关区列表
     */
    @Log(title = "创新检验监管主要开展关区", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('innovation:customs:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<InnovationCustoms> util = new ExcelUtil<InnovationCustoms>(InnovationCustoms.class);
        List<InnovationCustoms> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = innovationCustomsService.importInnovationCustoms(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载创新检验监管主要开展关区导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<InnovationCustoms> util = new ExcelUtil<InnovationCustoms>(InnovationCustoms.class);
        util.importTemplateExcel(response, "创新检验监管主要开展关区数据");
    }

    /**
     * 获取创新检验监管主要开展关区详细信息
     */
    @PreAuthorize("@ss.hasPermi('innovation:customs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(innovationCustomsService.selectInnovationCustomsById(id));
    }

    /**
     * 新增创新检验监管主要开展关区
     */
    @PreAuthorize("@ss.hasPermi('innovation:customs:add')")
    @Log(title = "创新检验监管主要开展关区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InnovationCustoms innovationCustoms) {
        return toAjax(innovationCustomsService.insertInnovationCustoms(innovationCustoms));
    }

    /**
     * 修改创新检验监管主要开展关区
     */
    @PreAuthorize("@ss.hasPermi('innovation:customs:edit')")
    @Log(title = "创新检验监管主要开展关区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InnovationCustoms innovationCustoms) {
        return toAjax(innovationCustomsService.updateInnovationCustoms(innovationCustoms));
    }

    /**
     * 删除创新检验监管主要开展关区
     */
    @PreAuthorize("@ss.hasPermi('innovation:customs:remove')")
    @Log(title = "创新检验监管主要开展关区", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(innovationCustomsService.deleteInnovationCustomsByIds(ids));
    }
}
