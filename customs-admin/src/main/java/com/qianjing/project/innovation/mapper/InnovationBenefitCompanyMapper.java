package com.qianjing.project.innovation.mapper;

import java.util.List;
import com.qianjing.project.innovation.domain.InnovationBenefitCompany;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 创新检验监管主要受惠企业Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface InnovationBenefitCompanyMapper {

    InnovationBenefitCompany selectInnovationBenefitCompanyById(Long id);

    List<InnovationBenefitCompany> selectInnovationBenefitCompanyList(InnovationBenefitCompany innovationBenefitCompany);

    int insertInnovationBenefitCompany(InnovationBenefitCompany innovationBenefitCompany);

    int updateInnovationBenefitCompany(InnovationBenefitCompany innovationBenefitCompany);

    int deleteInnovationBenefitCompanyById(Long id);

    int deleteInnovationBenefitCompanyByIds(Long[] ids);
}
