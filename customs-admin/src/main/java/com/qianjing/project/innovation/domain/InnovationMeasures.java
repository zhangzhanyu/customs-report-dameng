package com.qianjing.project.innovation.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 便利化措施概况对象 innovation_measures
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-15 下午3:00    张占宇          V1.0          initialize
 */
public class InnovationMeasures extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 商品类别（1汽车 2摩托车 3服装） */
    @Excel(name = "商品类别", readConverterExp = "1=汽车,2=摩托车,3=服装")
    private String classification;

    /** 进口报关单批 */
    @Excel(name = "进口报关单批")
    private Long importEntryBatch;

    /** 进口金额(美元) */
    @Excel(name = "进口金额(美元)")
    private BigDecimal importUsdPrice;

    /** 便利化报关单批 */
    @Excel(name = "便利化报关单批")
    private Long facilitateEntryBatch;

    /** 便利化金额(美元) */
    @Excel(name = "便利化金额(美元)")
    private BigDecimal facilitateUsdPrice;

    /** 便利化商品占比 */
    @Excel(name = "便利化商品占比")
    private BigDecimal facilitateRatio;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setClassification(String classification){
        this.classification = classification;
    }

    public String getClassification(){
        return classification;
    }
    public void setImportEntryBatch(Long importEntryBatch){
        this.importEntryBatch = importEntryBatch;
    }

    public Long getImportEntryBatch(){
        return importEntryBatch;
    }
    public void setImportUsdPrice(BigDecimal importUsdPrice){
        this.importUsdPrice = importUsdPrice;
    }

    public BigDecimal getImportUsdPrice(){
        return importUsdPrice;
    }
    public void setFacilitateEntryBatch(Long facilitateEntryBatch){
        this.facilitateEntryBatch = facilitateEntryBatch;
    }

    public Long getFacilitateEntryBatch(){
        return facilitateEntryBatch;
    }
    public void setFacilitateUsdPrice(BigDecimal facilitateUsdPrice){
        this.facilitateUsdPrice = facilitateUsdPrice;
    }

    public BigDecimal getFacilitateUsdPrice(){
        return facilitateUsdPrice;
    }
    public void setFacilitateRatio(BigDecimal facilitateRatio){
        this.facilitateRatio = facilitateRatio;
    }

    public BigDecimal getFacilitateRatio(){
        return facilitateRatio;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("classification", getClassification())
                .append("importEntryBatch", getImportEntryBatch())
                .append("importUsdPrice", getImportUsdPrice())
                .append("facilitateEntryBatch", getFacilitateEntryBatch())
                .append("facilitateUsdPrice", getFacilitateUsdPrice())
                .append("facilitateRatio", getFacilitateRatio())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("yearMonth", getYearMonth())
                .toString();
    }
}
