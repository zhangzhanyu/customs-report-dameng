package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalExportGoal;
import com.qianjing.project.chemical.service.IChemicalExportGoalService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品出口目的国Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/goal")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalExportGoalController extends BaseController {

    private final IChemicalExportGoalService chemicalExportGoalService;

    /**
     * 查询危险化学品出口目的国列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:goal:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalExportGoal chemicalExportGoal) {
        startPage();
        List<ChemicalExportGoal> list = chemicalExportGoalService.selectChemicalExportGoalList(chemicalExportGoal);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品出口目的国列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:goal:export')")
    @Log(title = "危险化学品出口目的国", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalExportGoal chemicalExportGoal) {
        List<ChemicalExportGoal> list = chemicalExportGoalService.selectChemicalExportGoalList(chemicalExportGoal);
        ExcelUtil<ChemicalExportGoal> util = new ExcelUtil<ChemicalExportGoal>(ChemicalExportGoal.class);
        return util.exportExcel(list, "危险化学品出口目的国数据");
    }

    /**
     * 导入危险化学品出口目的国列表
     */
    @Log(title = "危险化学品出口目的国", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:goal:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalExportGoal> util = new ExcelUtil<ChemicalExportGoal>(ChemicalExportGoal.class);
        List<ChemicalExportGoal> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalExportGoalService.importChemicalExportGoal(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品出口目的国导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalExportGoal> util = new ExcelUtil<ChemicalExportGoal>(ChemicalExportGoal.class);
        util.importTemplateExcel(response, "危险化学品出口目的国数据");
    }

    /**
     * 获取危险化学品出口目的国详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:goal:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalExportGoalService.selectChemicalExportGoalById(id));
    }

    /**
     * 新增危险化学品出口目的国
     */
    @PreAuthorize("@ss.hasPermi('chemical:goal:add')")
    @Log(title = "危险化学品出口目的国", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalExportGoal chemicalExportGoal) {
        return toAjax(chemicalExportGoalService.insertChemicalExportGoal(chemicalExportGoal));
    }

    /**
     * 修改危险化学品出口目的国
     */
    @PreAuthorize("@ss.hasPermi('chemical:goal:edit')")
    @Log(title = "危险化学品出口目的国", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalExportGoal chemicalExportGoal) {
        return toAjax(chemicalExportGoalService.updateChemicalExportGoal(chemicalExportGoal));
    }

    /**
     * 删除危险化学品出口目的国
     */
    @PreAuthorize("@ss.hasPermi('chemical:goal:remove')")
    @Log(title = "危险化学品出口目的国", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalExportGoalService.deleteChemicalExportGoalByIds(ids));
    }
}
