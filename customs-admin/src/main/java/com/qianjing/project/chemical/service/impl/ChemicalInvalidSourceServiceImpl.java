package com.qianjing.project.chemical.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.chemical.mapper.ChemicalInvalidSourceMapper;
import com.qianjing.project.chemical.domain.ChemicalInvalidSource;
import com.qianjing.project.chemical.service.IChemicalInvalidSourceService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品不合格来源国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalInvalidSourceServiceImpl implements IChemicalInvalidSourceService {

    private static final Logger log = LoggerFactory.getLogger(ChemicalInvalidSourceServiceImpl.class);

    private final ChemicalInvalidSourceMapper chemicalInvalidSourceMapper;

    /**
     * 查询危险化学品不合格来源国
     * 
     * @param id 危险化学品不合格来源国主键
     * @return 危险化学品不合格来源国
     */
    @Override
    public ChemicalInvalidSource selectChemicalInvalidSourceById(Long id) {
        return chemicalInvalidSourceMapper.selectChemicalInvalidSourceById(id);
    }

    /**
     * 查询危险化学品不合格来源国列表
     * 
     * @param chemicalInvalidSource 危险化学品不合格来源国
     * @return 危险化学品不合格来源国
     */
    @Override
    public List<ChemicalInvalidSource> selectChemicalInvalidSourceList(ChemicalInvalidSource chemicalInvalidSource) {
        return chemicalInvalidSourceMapper.selectChemicalInvalidSourceList(chemicalInvalidSource);
    }

    /**
     * 新增危险化学品不合格来源国
     * 
     * @param chemicalInvalidSource 危险化学品不合格来源国
     * @return 结果
     */
    @Override
    public int insertChemicalInvalidSource(ChemicalInvalidSource chemicalInvalidSource) {
        chemicalInvalidSource.setCreateTime(DateUtils.getNowDate());
        return chemicalInvalidSourceMapper.insertChemicalInvalidSource(chemicalInvalidSource);
    }

    /**
     * 修改危险化学品不合格来源国
     * 
     * @param chemicalInvalidSource 危险化学品不合格来源国
     * @return 结果
     */
    @Override
    public int updateChemicalInvalidSource(ChemicalInvalidSource chemicalInvalidSource) {
        chemicalInvalidSource.setUpdateTime(DateUtils.getNowDate());
        return chemicalInvalidSourceMapper.updateChemicalInvalidSource(chemicalInvalidSource);
    }

    /**
     * 批量删除危险化学品不合格来源国
     * 
     * @param ids 需要删除的危险化学品不合格来源国主键
     * @return 结果
     */
    @Override
    public int deleteChemicalInvalidSourceByIds(Long[] ids) {
        return chemicalInvalidSourceMapper.deleteChemicalInvalidSourceByIds(ids);
    }

    /**
     * 删除危险化学品不合格来源国信息
     * 
     * @param id 危险化学品不合格来源国主键
     * @return 结果
     */
    @Override
    public int deleteChemicalInvalidSourceById(Long id) {
        return chemicalInvalidSourceMapper.deleteChemicalInvalidSourceById(id);
    }

    /**
     * 导入危险化学品不合格来源国数据
     *
     * @param chemicalInvalidSourceList chemicalInvalidSourceList 危险化学品不合格来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importChemicalInvalidSource(List<ChemicalInvalidSource> chemicalInvalidSourceList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(chemicalInvalidSourceList) || chemicalInvalidSourceList.size() == 0) {
            throw new ServiceException("导入危险化学品不合格来源国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ChemicalInvalidSource chemicalInvalidSource : chemicalInvalidSourceList){
            try {
                // 验证是否存在
                if (true) {
                    chemicalInvalidSource.setCreateBy(operName);
                    this.insertChemicalInvalidSource(chemicalInvalidSource);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    chemicalInvalidSource.setUpdateBy(operName);
                    this.updateChemicalInvalidSource(chemicalInvalidSource);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
