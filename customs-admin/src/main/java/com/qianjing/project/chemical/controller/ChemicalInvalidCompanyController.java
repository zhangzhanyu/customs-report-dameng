package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalInvalidCompany;
import com.qianjing.project.chemical.service.IChemicalInvalidCompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品不合格企业情况Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/company")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalInvalidCompanyController extends BaseController {

    private final IChemicalInvalidCompanyService chemicalInvalidCompanyService;

    /**
     * 查询危险化学品不合格企业情况列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalInvalidCompany chemicalInvalidCompany) {
        startPage();
        List<ChemicalInvalidCompany> list = chemicalInvalidCompanyService.selectChemicalInvalidCompanyList(chemicalInvalidCompany);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品不合格企业情况列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:company:export')")
    @Log(title = "危险化学品不合格企业情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalInvalidCompany chemicalInvalidCompany) {
        List<ChemicalInvalidCompany> list = chemicalInvalidCompanyService.selectChemicalInvalidCompanyList(chemicalInvalidCompany);
        ExcelUtil<ChemicalInvalidCompany> util = new ExcelUtil<ChemicalInvalidCompany>(ChemicalInvalidCompany.class);
        return util.exportExcel(list, "危险化学品不合格企业情况数据");
    }

    /**
     * 导入危险化学品不合格企业情况列表
     */
    @Log(title = "危险化学品不合格企业情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:company:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalInvalidCompany> util = new ExcelUtil<ChemicalInvalidCompany>(ChemicalInvalidCompany.class);
        List<ChemicalInvalidCompany> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalInvalidCompanyService.importChemicalInvalidCompany(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品不合格企业情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalInvalidCompany> util = new ExcelUtil<ChemicalInvalidCompany>(ChemicalInvalidCompany.class);
        util.importTemplateExcel(response, "危险化学品不合格企业情况数据");
    }

    /**
     * 获取危险化学品不合格企业情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:company:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalInvalidCompanyService.selectChemicalInvalidCompanyById(id));
    }

    /**
     * 新增危险化学品不合格企业情况
     */
    @PreAuthorize("@ss.hasPermi('chemical:company:add')")
    @Log(title = "危险化学品不合格企业情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalInvalidCompany chemicalInvalidCompany) {
        return toAjax(chemicalInvalidCompanyService.insertChemicalInvalidCompany(chemicalInvalidCompany));
    }

    /**
     * 修改危险化学品不合格企业情况
     */
    @PreAuthorize("@ss.hasPermi('chemical:company:edit')")
    @Log(title = "危险化学品不合格企业情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalInvalidCompany chemicalInvalidCompany) {
        return toAjax(chemicalInvalidCompanyService.updateChemicalInvalidCompany(chemicalInvalidCompany));
    }

    /**
     * 删除危险化学品不合格企业情况
     */
    @PreAuthorize("@ss.hasPermi('chemical:company:remove')")
    @Log(title = "危险化学品不合格企业情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalInvalidCompanyService.deleteChemicalInvalidCompanyByIds(ids));
    }
}
