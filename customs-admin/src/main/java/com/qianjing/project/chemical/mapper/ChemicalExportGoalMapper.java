package com.qianjing.project.chemical.mapper;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalExportGoal;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品出口目的国Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface ChemicalExportGoalMapper {

    ChemicalExportGoal selectChemicalExportGoalById(Long id);

    List<ChemicalExportGoal> selectChemicalExportGoalList(ChemicalExportGoal chemicalExportGoal);

    int insertChemicalExportGoal(ChemicalExportGoal chemicalExportGoal);

    int updateChemicalExportGoal(ChemicalExportGoal chemicalExportGoal);

    int deleteChemicalExportGoalById(Long id);

    int deleteChemicalExportGoalByIds(Long[] ids);
}
