package com.qianjing.project.chemical.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.chemical.mapper.ChemicalExportGoalMapper;
import com.qianjing.project.chemical.domain.ChemicalExportGoal;
import com.qianjing.project.chemical.service.IChemicalExportGoalService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品出口目的国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalExportGoalServiceImpl implements IChemicalExportGoalService {

    private static final Logger log = LoggerFactory.getLogger(ChemicalExportGoalServiceImpl.class);

    private final ChemicalExportGoalMapper chemicalExportGoalMapper;

    /**
     * 查询危险化学品出口目的国
     * 
     * @param id 危险化学品出口目的国主键
     * @return 危险化学品出口目的国
     */
    @Override
    public ChemicalExportGoal selectChemicalExportGoalById(Long id) {
        return chemicalExportGoalMapper.selectChemicalExportGoalById(id);
    }

    /**
     * 查询危险化学品出口目的国列表
     * 
     * @param chemicalExportGoal 危险化学品出口目的国
     * @return 危险化学品出口目的国
     */
    @Override
    public List<ChemicalExportGoal> selectChemicalExportGoalList(ChemicalExportGoal chemicalExportGoal) {
        return chemicalExportGoalMapper.selectChemicalExportGoalList(chemicalExportGoal);
    }

    /**
     * 新增危险化学品出口目的国
     * 
     * @param chemicalExportGoal 危险化学品出口目的国
     * @return 结果
     */
    @Override
    public int insertChemicalExportGoal(ChemicalExportGoal chemicalExportGoal) {
        chemicalExportGoal.setCreateTime(DateUtils.getNowDate());
        return chemicalExportGoalMapper.insertChemicalExportGoal(chemicalExportGoal);
    }

    /**
     * 修改危险化学品出口目的国
     * 
     * @param chemicalExportGoal 危险化学品出口目的国
     * @return 结果
     */
    @Override
    public int updateChemicalExportGoal(ChemicalExportGoal chemicalExportGoal) {
        chemicalExportGoal.setUpdateTime(DateUtils.getNowDate());
        return chemicalExportGoalMapper.updateChemicalExportGoal(chemicalExportGoal);
    }

    /**
     * 批量删除危险化学品出口目的国
     * 
     * @param ids 需要删除的危险化学品出口目的国主键
     * @return 结果
     */
    @Override
    public int deleteChemicalExportGoalByIds(Long[] ids) {
        return chemicalExportGoalMapper.deleteChemicalExportGoalByIds(ids);
    }

    /**
     * 删除危险化学品出口目的国信息
     * 
     * @param id 危险化学品出口目的国主键
     * @return 结果
     */
    @Override
    public int deleteChemicalExportGoalById(Long id) {
        return chemicalExportGoalMapper.deleteChemicalExportGoalById(id);
    }

    /**
     * 导入危险化学品出口目的国数据
     *
     * @param chemicalExportGoalList chemicalExportGoalList 危险化学品出口目的国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importChemicalExportGoal(List<ChemicalExportGoal> chemicalExportGoalList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(chemicalExportGoalList) || chemicalExportGoalList.size() == 0) {
            throw new ServiceException("导入危险化学品出口目的国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ChemicalExportGoal chemicalExportGoal : chemicalExportGoalList){
            try {
                // 验证是否存在
                if (true) {
                    chemicalExportGoal.setCreateBy(operName);
                    this.insertChemicalExportGoal(chemicalExportGoal);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    chemicalExportGoal.setUpdateBy(operName);
                    this.updateChemicalExportGoal(chemicalExportGoal);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
