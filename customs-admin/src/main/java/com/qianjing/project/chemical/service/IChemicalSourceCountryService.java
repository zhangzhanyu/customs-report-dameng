package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品主要来源国Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalSourceCountryService 
{
    /**
     * 查询危险化学品主要来源国
     * 
     * @param id 危险化学品主要来源国主键
     * @return 危险化学品主要来源国
     */
    public ChemicalSourceCountry selectChemicalSourceCountryById(Long id);

    /**
     * 查询危险化学品主要来源国列表
     * 
     * @param chemicalSourceCountry 危险化学品主要来源国
     * @return 危险化学品主要来源国集合
     */
    public List<ChemicalSourceCountry> selectChemicalSourceCountryList(ChemicalSourceCountry chemicalSourceCountry);

    /**
     * 新增危险化学品主要来源国
     * 
     * @param chemicalSourceCountry 危险化学品主要来源国
     * @return 结果
     */
    public int insertChemicalSourceCountry(ChemicalSourceCountry chemicalSourceCountry);

    /**
     * 修改危险化学品主要来源国
     * 
     * @param chemicalSourceCountry 危险化学品主要来源国
     * @return 结果
     */
    public int updateChemicalSourceCountry(ChemicalSourceCountry chemicalSourceCountry);

    /**
     * 批量删除危险化学品主要来源国
     * 
     * @param ids 需要删除的危险化学品主要来源国主键集合
     * @return 结果
     */
    public int deleteChemicalSourceCountryByIds(Long[] ids);

    /**
     * 删除危险化学品主要来源国信息
     * 
     * @param id 危险化学品主要来源国主键
     * @return 结果
     */
    public int deleteChemicalSourceCountryById(Long id);

    /**
     * 导入危险化学品主要来源国信息
     *
     * @param chemicalSourceCountryList 危险化学品主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalSourceCountry(List<ChemicalSourceCountry> chemicalSourceCountryList, Boolean isUpdateSupport, String operName);
}
