package com.qianjing.project.chemical.mapper;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品不合格关区情况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface ChemicalInvalidCustomsMapper {

    ChemicalInvalidCustoms selectChemicalInvalidCustomsById(Long id);

    List<ChemicalInvalidCustoms> selectChemicalInvalidCustomsList(ChemicalInvalidCustoms chemicalInvalidCustoms);

    int insertChemicalInvalidCustoms(ChemicalInvalidCustoms chemicalInvalidCustoms);

    int updateChemicalInvalidCustoms(ChemicalInvalidCustoms chemicalInvalidCustoms);

    int deleteChemicalInvalidCustomsById(Long id);

    int deleteChemicalInvalidCustomsByIds(Long[] ids);
}
