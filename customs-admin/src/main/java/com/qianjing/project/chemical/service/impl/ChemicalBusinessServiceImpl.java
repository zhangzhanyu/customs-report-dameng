package com.qianjing.project.chemical.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.chemical.mapper.ChemicalBusinessMapper;
import com.qianjing.project.chemical.domain.ChemicalBusiness;
import com.qianjing.project.chemical.service.IChemicalBusinessService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品口岸及目的地业务量Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalBusinessServiceImpl implements IChemicalBusinessService {

    private static final Logger log = LoggerFactory.getLogger(ChemicalBusinessServiceImpl.class);

    private final ChemicalBusinessMapper chemicalBusinessMapper;

    /**
     * 查询危险化学品口岸及目的地业务量
     * 
     * @param id 危险化学品口岸及目的地业务量主键
     * @return 危险化学品口岸及目的地业务量
     */
    @Override
    public ChemicalBusiness selectChemicalBusinessById(Long id) {
        return chemicalBusinessMapper.selectChemicalBusinessById(id);
    }

    /**
     * 查询危险化学品口岸及目的地业务量列表
     * 
     * @param chemicalBusiness 危险化学品口岸及目的地业务量
     * @return 危险化学品口岸及目的地业务量
     */
    @Override
    public List<ChemicalBusiness> selectChemicalBusinessList(ChemicalBusiness chemicalBusiness) {
        return chemicalBusinessMapper.selectChemicalBusinessList(chemicalBusiness);
    }

    /**
     * 新增危险化学品口岸及目的地业务量
     * 
     * @param chemicalBusiness 危险化学品口岸及目的地业务量
     * @return 结果
     */
    @Override
    public int insertChemicalBusiness(ChemicalBusiness chemicalBusiness) {
        chemicalBusiness.setCreateTime(DateUtils.getNowDate());
        return chemicalBusinessMapper.insertChemicalBusiness(chemicalBusiness);
    }

    /**
     * 修改危险化学品口岸及目的地业务量
     * 
     * @param chemicalBusiness 危险化学品口岸及目的地业务量
     * @return 结果
     */
    @Override
    public int updateChemicalBusiness(ChemicalBusiness chemicalBusiness) {
        chemicalBusiness.setUpdateTime(DateUtils.getNowDate());
        return chemicalBusinessMapper.updateChemicalBusiness(chemicalBusiness);
    }

    /**
     * 批量删除危险化学品口岸及目的地业务量
     * 
     * @param ids 需要删除的危险化学品口岸及目的地业务量主键
     * @return 结果
     */
    @Override
    public int deleteChemicalBusinessByIds(Long[] ids) {
        return chemicalBusinessMapper.deleteChemicalBusinessByIds(ids);
    }

    /**
     * 删除危险化学品口岸及目的地业务量信息
     * 
     * @param id 危险化学品口岸及目的地业务量主键
     * @return 结果
     */
    @Override
    public int deleteChemicalBusinessById(Long id) {
        return chemicalBusinessMapper.deleteChemicalBusinessById(id);
    }

    /**
     * 导入危险化学品口岸及目的地业务量数据
     *
     * @param chemicalBusinessList chemicalBusinessList 危险化学品口岸及目的地业务量信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importChemicalBusiness(List<ChemicalBusiness> chemicalBusinessList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(chemicalBusinessList) || chemicalBusinessList.size() == 0) {
            throw new ServiceException("导入危险化学品口岸及目的地业务量数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ChemicalBusiness chemicalBusiness : chemicalBusinessList){
            try {
                // 验证是否存在
                if (true) {
                    chemicalBusiness.setCreateBy(operName);
                    this.insertChemicalBusiness(chemicalBusiness);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    chemicalBusiness.setUpdateBy(operName);
                    this.updateChemicalBusiness(chemicalBusiness);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
