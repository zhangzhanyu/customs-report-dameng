package com.qianjing.project.chemical.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.chemical.mapper.ChemicalImportOverviewMapper;
import com.qianjing.project.chemical.domain.ChemicalImportOverview;
import com.qianjing.project.chemical.service.IChemicalImportOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品进口概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalImportOverviewServiceImpl implements IChemicalImportOverviewService {

    private static final Logger log = LoggerFactory.getLogger(ChemicalImportOverviewServiceImpl.class);

    private final ChemicalImportOverviewMapper chemicalImportOverviewMapper;

    /**
     * 查询危险化学品进口概况
     * 
     * @param id 危险化学品进口概况主键
     * @return 危险化学品进口概况
     */
    @Override
    public ChemicalImportOverview selectChemicalImportOverviewById(Long id) {
        return chemicalImportOverviewMapper.selectChemicalImportOverviewById(id);
    }

    /**
     * 查询危险化学品进口概况列表
     * 
     * @param chemicalImportOverview 危险化学品进口概况
     * @return 危险化学品进口概况
     */
    @Override
    public List<ChemicalImportOverview> selectChemicalImportOverviewList(ChemicalImportOverview chemicalImportOverview) {
        return chemicalImportOverviewMapper.selectChemicalImportOverviewList(chemicalImportOverview);
    }

    /**
     * 新增危险化学品进口概况
     * 
     * @param chemicalImportOverview 危险化学品进口概况
     * @return 结果
     */
    @Override
    public int insertChemicalImportOverview(ChemicalImportOverview chemicalImportOverview) {
        chemicalImportOverview.setCreateTime(DateUtils.getNowDate());
        return chemicalImportOverviewMapper.insertChemicalImportOverview(chemicalImportOverview);
    }

    /**
     * 修改危险化学品进口概况
     * 
     * @param chemicalImportOverview 危险化学品进口概况
     * @return 结果
     */
    @Override
    public int updateChemicalImportOverview(ChemicalImportOverview chemicalImportOverview) {
        chemicalImportOverview.setUpdateTime(DateUtils.getNowDate());
        return chemicalImportOverviewMapper.updateChemicalImportOverview(chemicalImportOverview);
    }

    /**
     * 批量删除危险化学品进口概况
     * 
     * @param ids 需要删除的危险化学品进口概况主键
     * @return 结果
     */
    @Override
    public int deleteChemicalImportOverviewByIds(Long[] ids) {
        return chemicalImportOverviewMapper.deleteChemicalImportOverviewByIds(ids);
    }

    /**
     * 删除危险化学品进口概况信息
     * 
     * @param id 危险化学品进口概况主键
     * @return 结果
     */
    @Override
    public int deleteChemicalImportOverviewById(Long id) {
        return chemicalImportOverviewMapper.deleteChemicalImportOverviewById(id);
    }

    /**
     * 导入危险化学品进口概况数据
     *
     * @param chemicalImportOverviewList chemicalImportOverviewList 危险化学品进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importChemicalImportOverview(List<ChemicalImportOverview> chemicalImportOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(chemicalImportOverviewList) || chemicalImportOverviewList.size() == 0) {
            throw new ServiceException("导入危险化学品进口概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ChemicalImportOverview chemicalImportOverview : chemicalImportOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    chemicalImportOverview.setCreateBy(operName);
                    this.insertChemicalImportOverview(chemicalImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    chemicalImportOverview.setUpdateBy(operName);
                    this.updateChemicalImportOverview(chemicalImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
