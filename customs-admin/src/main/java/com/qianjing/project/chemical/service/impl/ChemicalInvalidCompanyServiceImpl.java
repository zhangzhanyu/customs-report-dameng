package com.qianjing.project.chemical.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.chemical.mapper.ChemicalInvalidCompanyMapper;
import com.qianjing.project.chemical.domain.ChemicalInvalidCompany;
import com.qianjing.project.chemical.service.IChemicalInvalidCompanyService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品不合格企业情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalInvalidCompanyServiceImpl implements IChemicalInvalidCompanyService {

    private static final Logger log = LoggerFactory.getLogger(ChemicalInvalidCompanyServiceImpl.class);

    private final ChemicalInvalidCompanyMapper chemicalInvalidCompanyMapper;

    /**
     * 查询危险化学品不合格企业情况
     * 
     * @param id 危险化学品不合格企业情况主键
     * @return 危险化学品不合格企业情况
     */
    @Override
    public ChemicalInvalidCompany selectChemicalInvalidCompanyById(Long id) {
        return chemicalInvalidCompanyMapper.selectChemicalInvalidCompanyById(id);
    }

    /**
     * 查询危险化学品不合格企业情况列表
     * 
     * @param chemicalInvalidCompany 危险化学品不合格企业情况
     * @return 危险化学品不合格企业情况
     */
    @Override
    public List<ChemicalInvalidCompany> selectChemicalInvalidCompanyList(ChemicalInvalidCompany chemicalInvalidCompany) {
        return chemicalInvalidCompanyMapper.selectChemicalInvalidCompanyList(chemicalInvalidCompany);
    }

    /**
     * 新增危险化学品不合格企业情况
     * 
     * @param chemicalInvalidCompany 危险化学品不合格企业情况
     * @return 结果
     */
    @Override
    public int insertChemicalInvalidCompany(ChemicalInvalidCompany chemicalInvalidCompany) {
        chemicalInvalidCompany.setCreateTime(DateUtils.getNowDate());
        return chemicalInvalidCompanyMapper.insertChemicalInvalidCompany(chemicalInvalidCompany);
    }

    /**
     * 修改危险化学品不合格企业情况
     * 
     * @param chemicalInvalidCompany 危险化学品不合格企业情况
     * @return 结果
     */
    @Override
    public int updateChemicalInvalidCompany(ChemicalInvalidCompany chemicalInvalidCompany) {
        chemicalInvalidCompany.setUpdateTime(DateUtils.getNowDate());
        return chemicalInvalidCompanyMapper.updateChemicalInvalidCompany(chemicalInvalidCompany);
    }

    /**
     * 批量删除危险化学品不合格企业情况
     * 
     * @param ids 需要删除的危险化学品不合格企业情况主键
     * @return 结果
     */
    @Override
    public int deleteChemicalInvalidCompanyByIds(Long[] ids) {
        return chemicalInvalidCompanyMapper.deleteChemicalInvalidCompanyByIds(ids);
    }

    /**
     * 删除危险化学品不合格企业情况信息
     * 
     * @param id 危险化学品不合格企业情况主键
     * @return 结果
     */
    @Override
    public int deleteChemicalInvalidCompanyById(Long id) {
        return chemicalInvalidCompanyMapper.deleteChemicalInvalidCompanyById(id);
    }

    /**
     * 导入危险化学品不合格企业情况数据
     *
     * @param chemicalInvalidCompanyList chemicalInvalidCompanyList 危险化学品不合格企业情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importChemicalInvalidCompany(List<ChemicalInvalidCompany> chemicalInvalidCompanyList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(chemicalInvalidCompanyList) || chemicalInvalidCompanyList.size() == 0) {
            throw new ServiceException("导入危险化学品不合格企业情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ChemicalInvalidCompany chemicalInvalidCompany : chemicalInvalidCompanyList){
            try {
                // 验证是否存在
                if (true) {
                    chemicalInvalidCompany.setCreateBy(operName);
                    this.insertChemicalInvalidCompany(chemicalInvalidCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    chemicalInvalidCompany.setUpdateBy(operName);
                    this.updateChemicalInvalidCompany(chemicalInvalidCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
