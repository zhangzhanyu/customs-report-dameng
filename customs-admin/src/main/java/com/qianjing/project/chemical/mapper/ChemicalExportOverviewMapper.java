package com.qianjing.project.chemical.mapper;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalExportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品出口概况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface ChemicalExportOverviewMapper {

    ChemicalExportOverview selectChemicalExportOverviewById(Long id);

    List<ChemicalExportOverview> selectChemicalExportOverviewList(ChemicalExportOverview chemicalExportOverview);

    int insertChemicalExportOverview(ChemicalExportOverview chemicalExportOverview);

    int updateChemicalExportOverview(ChemicalExportOverview chemicalExportOverview);

    int deleteChemicalExportOverviewById(Long id);

    int deleteChemicalExportOverviewByIds(Long[] ids);
}
