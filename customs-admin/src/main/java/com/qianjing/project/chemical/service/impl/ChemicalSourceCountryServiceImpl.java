package com.qianjing.project.chemical.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.chemical.mapper.ChemicalSourceCountryMapper;
import com.qianjing.project.chemical.domain.ChemicalSourceCountry;
import com.qianjing.project.chemical.service.IChemicalSourceCountryService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品主要来源国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalSourceCountryServiceImpl implements IChemicalSourceCountryService {

    private static final Logger log = LoggerFactory.getLogger(ChemicalSourceCountryServiceImpl.class);

    private final ChemicalSourceCountryMapper chemicalSourceCountryMapper;

    /**
     * 查询危险化学品主要来源国
     * 
     * @param id 危险化学品主要来源国主键
     * @return 危险化学品主要来源国
     */
    @Override
    public ChemicalSourceCountry selectChemicalSourceCountryById(Long id) {
        return chemicalSourceCountryMapper.selectChemicalSourceCountryById(id);
    }

    /**
     * 查询危险化学品主要来源国列表
     * 
     * @param chemicalSourceCountry 危险化学品主要来源国
     * @return 危险化学品主要来源国
     */
    @Override
    public List<ChemicalSourceCountry> selectChemicalSourceCountryList(ChemicalSourceCountry chemicalSourceCountry) {
        return chemicalSourceCountryMapper.selectChemicalSourceCountryList(chemicalSourceCountry);
    }

    /**
     * 新增危险化学品主要来源国
     * 
     * @param chemicalSourceCountry 危险化学品主要来源国
     * @return 结果
     */
    @Override
    public int insertChemicalSourceCountry(ChemicalSourceCountry chemicalSourceCountry) {
        chemicalSourceCountry.setCreateTime(DateUtils.getNowDate());
        return chemicalSourceCountryMapper.insertChemicalSourceCountry(chemicalSourceCountry);
    }

    /**
     * 修改危险化学品主要来源国
     * 
     * @param chemicalSourceCountry 危险化学品主要来源国
     * @return 结果
     */
    @Override
    public int updateChemicalSourceCountry(ChemicalSourceCountry chemicalSourceCountry) {
        chemicalSourceCountry.setUpdateTime(DateUtils.getNowDate());
        return chemicalSourceCountryMapper.updateChemicalSourceCountry(chemicalSourceCountry);
    }

    /**
     * 批量删除危险化学品主要来源国
     * 
     * @param ids 需要删除的危险化学品主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteChemicalSourceCountryByIds(Long[] ids) {
        return chemicalSourceCountryMapper.deleteChemicalSourceCountryByIds(ids);
    }

    /**
     * 删除危险化学品主要来源国信息
     * 
     * @param id 危险化学品主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteChemicalSourceCountryById(Long id) {
        return chemicalSourceCountryMapper.deleteChemicalSourceCountryById(id);
    }

    /**
     * 导入危险化学品主要来源国数据
     *
     * @param chemicalSourceCountryList chemicalSourceCountryList 危险化学品主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importChemicalSourceCountry(List<ChemicalSourceCountry> chemicalSourceCountryList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(chemicalSourceCountryList) || chemicalSourceCountryList.size() == 0) {
            throw new ServiceException("导入危险化学品主要来源国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ChemicalSourceCountry chemicalSourceCountry : chemicalSourceCountryList){
            try {
                // 验证是否存在
                if (true) {
                    chemicalSourceCountry.setCreateBy(operName);
                    this.insertChemicalSourceCountry(chemicalSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    chemicalSourceCountry.setUpdateBy(operName);
                    this.updateChemicalSourceCountry(chemicalSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
