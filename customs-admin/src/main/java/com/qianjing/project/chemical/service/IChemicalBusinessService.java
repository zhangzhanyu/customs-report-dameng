package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalBusiness;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品口岸及目的地业务量Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalBusinessService 
{
    /**
     * 查询危险化学品口岸及目的地业务量
     * 
     * @param id 危险化学品口岸及目的地业务量主键
     * @return 危险化学品口岸及目的地业务量
     */
    public ChemicalBusiness selectChemicalBusinessById(Long id);

    /**
     * 查询危险化学品口岸及目的地业务量列表
     * 
     * @param chemicalBusiness 危险化学品口岸及目的地业务量
     * @return 危险化学品口岸及目的地业务量集合
     */
    public List<ChemicalBusiness> selectChemicalBusinessList(ChemicalBusiness chemicalBusiness);

    /**
     * 新增危险化学品口岸及目的地业务量
     * 
     * @param chemicalBusiness 危险化学品口岸及目的地业务量
     * @return 结果
     */
    public int insertChemicalBusiness(ChemicalBusiness chemicalBusiness);

    /**
     * 修改危险化学品口岸及目的地业务量
     * 
     * @param chemicalBusiness 危险化学品口岸及目的地业务量
     * @return 结果
     */
    public int updateChemicalBusiness(ChemicalBusiness chemicalBusiness);

    /**
     * 批量删除危险化学品口岸及目的地业务量
     * 
     * @param ids 需要删除的危险化学品口岸及目的地业务量主键集合
     * @return 结果
     */
    public int deleteChemicalBusinessByIds(Long[] ids);

    /**
     * 删除危险化学品口岸及目的地业务量信息
     * 
     * @param id 危险化学品口岸及目的地业务量主键
     * @return 结果
     */
    public int deleteChemicalBusinessById(Long id);

    /**
     * 导入危险化学品口岸及目的地业务量信息
     *
     * @param chemicalBusinessList 危险化学品口岸及目的地业务量信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalBusiness(List<ChemicalBusiness> chemicalBusinessList, Boolean isUpdateSupport, String operName);
}
