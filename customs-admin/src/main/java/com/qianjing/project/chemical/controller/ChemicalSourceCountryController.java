package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalSourceCountry;
import com.qianjing.project.chemical.service.IChemicalSourceCountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品主要来源国Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/country")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalSourceCountryController extends BaseController {

    private final IChemicalSourceCountryService chemicalSourceCountryService;

    /**
     * 查询危险化学品主要来源国列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:country:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalSourceCountry chemicalSourceCountry) {
        startPage();
        List<ChemicalSourceCountry> list = chemicalSourceCountryService.selectChemicalSourceCountryList(chemicalSourceCountry);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品主要来源国列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:country:export')")
    @Log(title = "危险化学品主要来源国", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalSourceCountry chemicalSourceCountry) {
        List<ChemicalSourceCountry> list = chemicalSourceCountryService.selectChemicalSourceCountryList(chemicalSourceCountry);
        ExcelUtil<ChemicalSourceCountry> util = new ExcelUtil<ChemicalSourceCountry>(ChemicalSourceCountry.class);
        return util.exportExcel(list, "危险化学品主要来源国数据");
    }

    /**
     * 导入危险化学品主要来源国列表
     */
    @Log(title = "危险化学品主要来源国", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:country:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalSourceCountry> util = new ExcelUtil<ChemicalSourceCountry>(ChemicalSourceCountry.class);
        List<ChemicalSourceCountry> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalSourceCountryService.importChemicalSourceCountry(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品主要来源国导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalSourceCountry> util = new ExcelUtil<ChemicalSourceCountry>(ChemicalSourceCountry.class);
        util.importTemplateExcel(response, "危险化学品主要来源国数据");
    }

    /**
     * 获取危险化学品主要来源国详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:country:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalSourceCountryService.selectChemicalSourceCountryById(id));
    }

    /**
     * 新增危险化学品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('chemical:country:add')")
    @Log(title = "危险化学品主要来源国", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalSourceCountry chemicalSourceCountry) {
        return toAjax(chemicalSourceCountryService.insertChemicalSourceCountry(chemicalSourceCountry));
    }

    /**
     * 修改危险化学品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('chemical:country:edit')")
    @Log(title = "危险化学品主要来源国", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalSourceCountry chemicalSourceCountry) {
        return toAjax(chemicalSourceCountryService.updateChemicalSourceCountry(chemicalSourceCountry));
    }

    /**
     * 删除危险化学品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('chemical:country:remove')")
    @Log(title = "危险化学品主要来源国", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalSourceCountryService.deleteChemicalSourceCountryByIds(ids));
    }
}
