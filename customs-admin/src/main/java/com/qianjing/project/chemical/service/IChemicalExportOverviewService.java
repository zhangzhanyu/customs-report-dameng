package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalExportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品出口概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalExportOverviewService 
{
    /**
     * 查询危险化学品出口概况
     * 
     * @param id 危险化学品出口概况主键
     * @return 危险化学品出口概况
     */
    public ChemicalExportOverview selectChemicalExportOverviewById(Long id);

    /**
     * 查询危险化学品出口概况列表
     * 
     * @param chemicalExportOverview 危险化学品出口概况
     * @return 危险化学品出口概况集合
     */
    public List<ChemicalExportOverview> selectChemicalExportOverviewList(ChemicalExportOverview chemicalExportOverview);

    /**
     * 新增危险化学品出口概况
     * 
     * @param chemicalExportOverview 危险化学品出口概况
     * @return 结果
     */
    public int insertChemicalExportOverview(ChemicalExportOverview chemicalExportOverview);

    /**
     * 修改危险化学品出口概况
     * 
     * @param chemicalExportOverview 危险化学品出口概况
     * @return 结果
     */
    public int updateChemicalExportOverview(ChemicalExportOverview chemicalExportOverview);

    /**
     * 批量删除危险化学品出口概况
     * 
     * @param ids 需要删除的危险化学品出口概况主键集合
     * @return 结果
     */
    public int deleteChemicalExportOverviewByIds(Long[] ids);

    /**
     * 删除危险化学品出口概况信息
     * 
     * @param id 危险化学品出口概况主键
     * @return 结果
     */
    public int deleteChemicalExportOverviewById(Long id);

    /**
     * 导入危险化学品出口概况信息
     *
     * @param chemicalExportOverviewList 危险化学品出口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalExportOverview(List<ChemicalExportOverview> chemicalExportOverviewList, Boolean isUpdateSupport, String operName);
}
