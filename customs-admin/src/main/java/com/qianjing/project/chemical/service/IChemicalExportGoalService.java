package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalExportGoal;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品出口目的国Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalExportGoalService 
{
    /**
     * 查询危险化学品出口目的国
     * 
     * @param id 危险化学品出口目的国主键
     * @return 危险化学品出口目的国
     */
    public ChemicalExportGoal selectChemicalExportGoalById(Long id);

    /**
     * 查询危险化学品出口目的国列表
     * 
     * @param chemicalExportGoal 危险化学品出口目的国
     * @return 危险化学品出口目的国集合
     */
    public List<ChemicalExportGoal> selectChemicalExportGoalList(ChemicalExportGoal chemicalExportGoal);

    /**
     * 新增危险化学品出口目的国
     * 
     * @param chemicalExportGoal 危险化学品出口目的国
     * @return 结果
     */
    public int insertChemicalExportGoal(ChemicalExportGoal chemicalExportGoal);

    /**
     * 修改危险化学品出口目的国
     * 
     * @param chemicalExportGoal 危险化学品出口目的国
     * @return 结果
     */
    public int updateChemicalExportGoal(ChemicalExportGoal chemicalExportGoal);

    /**
     * 批量删除危险化学品出口目的国
     * 
     * @param ids 需要删除的危险化学品出口目的国主键集合
     * @return 结果
     */
    public int deleteChemicalExportGoalByIds(Long[] ids);

    /**
     * 删除危险化学品出口目的国信息
     * 
     * @param id 危险化学品出口目的国主键
     * @return 结果
     */
    public int deleteChemicalExportGoalById(Long id);

    /**
     * 导入危险化学品出口目的国信息
     *
     * @param chemicalExportGoalList 危险化学品出口目的国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalExportGoal(List<ChemicalExportGoal> chemicalExportGoalList, Boolean isUpdateSupport, String operName);
}
