package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalInvalidCustoms;
import com.qianjing.project.chemical.service.IChemicalInvalidCustomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品不合格关区情况Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/customs")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalInvalidCustomsController extends BaseController {

    private final IChemicalInvalidCustomsService chemicalInvalidCustomsService;

    /**
     * 查询危险化学品不合格关区情况列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:customs:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalInvalidCustoms chemicalInvalidCustoms) {
        startPage();
        List<ChemicalInvalidCustoms> list = chemicalInvalidCustomsService.selectChemicalInvalidCustomsList(chemicalInvalidCustoms);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品不合格关区情况列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:customs:export')")
    @Log(title = "危险化学品不合格关区情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalInvalidCustoms chemicalInvalidCustoms) {
        List<ChemicalInvalidCustoms> list = chemicalInvalidCustomsService.selectChemicalInvalidCustomsList(chemicalInvalidCustoms);
        ExcelUtil<ChemicalInvalidCustoms> util = new ExcelUtil<ChemicalInvalidCustoms>(ChemicalInvalidCustoms.class);
        return util.exportExcel(list, "危险化学品不合格关区情况数据");
    }

    /**
     * 导入危险化学品不合格关区情况列表
     */
    @Log(title = "危险化学品不合格关区情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:customs:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalInvalidCustoms> util = new ExcelUtil<ChemicalInvalidCustoms>(ChemicalInvalidCustoms.class);
        List<ChemicalInvalidCustoms> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalInvalidCustomsService.importChemicalInvalidCustoms(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品不合格关区情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalInvalidCustoms> util = new ExcelUtil<ChemicalInvalidCustoms>(ChemicalInvalidCustoms.class);
        util.importTemplateExcel(response, "危险化学品不合格关区情况数据");
    }

    /**
     * 获取危险化学品不合格关区情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:customs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalInvalidCustomsService.selectChemicalInvalidCustomsById(id));
    }

    /**
     * 新增危险化学品不合格关区情况
     */
    @PreAuthorize("@ss.hasPermi('chemical:customs:add')")
    @Log(title = "危险化学品不合格关区情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalInvalidCustoms chemicalInvalidCustoms) {
        return toAjax(chemicalInvalidCustomsService.insertChemicalInvalidCustoms(chemicalInvalidCustoms));
    }

    /**
     * 修改危险化学品不合格关区情况
     */
    @PreAuthorize("@ss.hasPermi('chemical:customs:edit')")
    @Log(title = "危险化学品不合格关区情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalInvalidCustoms chemicalInvalidCustoms) {
        return toAjax(chemicalInvalidCustomsService.updateChemicalInvalidCustoms(chemicalInvalidCustoms));
    }

    /**
     * 删除危险化学品不合格关区情况
     */
    @PreAuthorize("@ss.hasPermi('chemical:customs:remove')")
    @Log(title = "危险化学品不合格关区情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalInvalidCustomsService.deleteChemicalInvalidCustomsByIds(ids));
    }
}
