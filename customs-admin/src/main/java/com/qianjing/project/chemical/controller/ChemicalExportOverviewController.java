package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalExportOverview;
import com.qianjing.project.chemical.service.IChemicalExportOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品出口概况Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/exportOverview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalExportOverviewController extends BaseController {

    private final IChemicalExportOverviewService chemicalExportOverviewService;

    /**
     * 查询危险化学品出口概况列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:exportOverview:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalExportOverview chemicalExportOverview) {
        startPage();
        List<ChemicalExportOverview> list = chemicalExportOverviewService.selectChemicalExportOverviewList(chemicalExportOverview);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品出口概况列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:exportOverview:export')")
    @Log(title = "危险化学品出口概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalExportOverview chemicalExportOverview) {
        List<ChemicalExportOverview> list = chemicalExportOverviewService.selectChemicalExportOverviewList(chemicalExportOverview);
        ExcelUtil<ChemicalExportOverview> util = new ExcelUtil<ChemicalExportOverview>(ChemicalExportOverview.class);
        return util.exportExcel(list, "危险化学品出口概况数据");
    }

    /**
     * 导入危险化学品出口概况列表
     */
    @Log(title = "危险化学品出口概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:exportOverview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalExportOverview> util = new ExcelUtil<ChemicalExportOverview>(ChemicalExportOverview.class);
        List<ChemicalExportOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalExportOverviewService.importChemicalExportOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品出口概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalExportOverview> util = new ExcelUtil<ChemicalExportOverview>(ChemicalExportOverview.class);
        util.importTemplateExcel(response, "危险化学品出口概况数据");
    }

    /**
     * 获取危险化学品出口概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:exportOverview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalExportOverviewService.selectChemicalExportOverviewById(id));
    }

    /**
     * 新增危险化学品出口概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:exportOverview:add')")
    @Log(title = "危险化学品出口概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalExportOverview chemicalExportOverview) {
        return toAjax(chemicalExportOverviewService.insertChemicalExportOverview(chemicalExportOverview));
    }

    /**
     * 修改危险化学品出口概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:exportOverview:edit')")
    @Log(title = "危险化学品出口概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalExportOverview chemicalExportOverview) {
        return toAjax(chemicalExportOverviewService.updateChemicalExportOverview(chemicalExportOverview));
    }

    /**
     * 删除危险化学品出口概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:exportOverview:remove')")
    @Log(title = "危险化学品出口概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalExportOverviewService.deleteChemicalExportOverviewByIds(ids));
    }
}
