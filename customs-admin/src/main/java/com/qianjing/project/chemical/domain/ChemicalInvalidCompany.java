package com.qianjing.project.chemical.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品不合格企业情况对象 chemical_invalid_company
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public class ChemicalInvalidCompany extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private Long sortNum;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String name;

    /** 查验批次 */
    @Excel(name = "查验批次")
    private Long checkBatch;

    /** 查验货物批 */
    @Excel(name = "查验货物批")
    private Long checkGoodsBatch;

    /** 货值(万美元) */
    @Excel(name = "货值(万美元)")
    private BigDecimal usdPrice;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setSortNum(Long sortNum){
        this.sortNum = sortNum;
    }

    public Long getSortNum(){
        return sortNum;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
    public void setCheckBatch(Long checkBatch){
        this.checkBatch = checkBatch;
    }

    public Long getCheckBatch(){
        return checkBatch;
    }
    public void setCheckGoodsBatch(Long checkGoodsBatch){
        this.checkGoodsBatch = checkGoodsBatch;
    }

    public Long getCheckGoodsBatch(){
        return checkGoodsBatch;
    }
    public void setUsdPrice(BigDecimal usdPrice){
        this.usdPrice = usdPrice;
    }

    public BigDecimal getUsdPrice(){
        return usdPrice;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sortNum", getSortNum())
            .append("name", getName())
            .append("checkBatch", getCheckBatch())
            .append("checkGoodsBatch", getCheckGoodsBatch())
            .append("usdPrice", getUsdPrice())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
