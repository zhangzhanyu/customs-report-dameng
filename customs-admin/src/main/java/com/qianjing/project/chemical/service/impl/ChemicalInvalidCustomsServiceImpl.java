package com.qianjing.project.chemical.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.chemical.mapper.ChemicalInvalidCustomsMapper;
import com.qianjing.project.chemical.domain.ChemicalInvalidCustoms;
import com.qianjing.project.chemical.service.IChemicalInvalidCustomsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品不合格关区情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalInvalidCustomsServiceImpl implements IChemicalInvalidCustomsService {

    private static final Logger log = LoggerFactory.getLogger(ChemicalInvalidCustomsServiceImpl.class);

    private final ChemicalInvalidCustomsMapper chemicalInvalidCustomsMapper;

    /**
     * 查询危险化学品不合格关区情况
     * 
     * @param id 危险化学品不合格关区情况主键
     * @return 危险化学品不合格关区情况
     */
    @Override
    public ChemicalInvalidCustoms selectChemicalInvalidCustomsById(Long id) {
        return chemicalInvalidCustomsMapper.selectChemicalInvalidCustomsById(id);
    }

    /**
     * 查询危险化学品不合格关区情况列表
     * 
     * @param chemicalInvalidCustoms 危险化学品不合格关区情况
     * @return 危险化学品不合格关区情况
     */
    @Override
    public List<ChemicalInvalidCustoms> selectChemicalInvalidCustomsList(ChemicalInvalidCustoms chemicalInvalidCustoms) {
        return chemicalInvalidCustomsMapper.selectChemicalInvalidCustomsList(chemicalInvalidCustoms);
    }

    /**
     * 新增危险化学品不合格关区情况
     * 
     * @param chemicalInvalidCustoms 危险化学品不合格关区情况
     * @return 结果
     */
    @Override
    public int insertChemicalInvalidCustoms(ChemicalInvalidCustoms chemicalInvalidCustoms) {
        chemicalInvalidCustoms.setCreateTime(DateUtils.getNowDate());
        return chemicalInvalidCustomsMapper.insertChemicalInvalidCustoms(chemicalInvalidCustoms);
    }

    /**
     * 修改危险化学品不合格关区情况
     * 
     * @param chemicalInvalidCustoms 危险化学品不合格关区情况
     * @return 结果
     */
    @Override
    public int updateChemicalInvalidCustoms(ChemicalInvalidCustoms chemicalInvalidCustoms) {
        chemicalInvalidCustoms.setUpdateTime(DateUtils.getNowDate());
        return chemicalInvalidCustomsMapper.updateChemicalInvalidCustoms(chemicalInvalidCustoms);
    }

    /**
     * 批量删除危险化学品不合格关区情况
     * 
     * @param ids 需要删除的危险化学品不合格关区情况主键
     * @return 结果
     */
    @Override
    public int deleteChemicalInvalidCustomsByIds(Long[] ids) {
        return chemicalInvalidCustomsMapper.deleteChemicalInvalidCustomsByIds(ids);
    }

    /**
     * 删除危险化学品不合格关区情况信息
     * 
     * @param id 危险化学品不合格关区情况主键
     * @return 结果
     */
    @Override
    public int deleteChemicalInvalidCustomsById(Long id) {
        return chemicalInvalidCustomsMapper.deleteChemicalInvalidCustomsById(id);
    }

    /**
     * 导入危险化学品不合格关区情况数据
     *
     * @param chemicalInvalidCustomsList chemicalInvalidCustomsList 危险化学品不合格关区情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importChemicalInvalidCustoms(List<ChemicalInvalidCustoms> chemicalInvalidCustomsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(chemicalInvalidCustomsList) || chemicalInvalidCustomsList.size() == 0) {
            throw new ServiceException("导入危险化学品不合格关区情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ChemicalInvalidCustoms chemicalInvalidCustoms : chemicalInvalidCustomsList){
            try {
                // 验证是否存在
                if (true) {
                    chemicalInvalidCustoms.setCreateBy(operName);
                    this.insertChemicalInvalidCustoms(chemicalInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    chemicalInvalidCustoms.setUpdateBy(operName);
                    this.updateChemicalInvalidCustoms(chemicalInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
