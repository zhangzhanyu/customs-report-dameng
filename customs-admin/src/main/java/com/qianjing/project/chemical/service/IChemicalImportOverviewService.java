package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalImportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品进口概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalImportOverviewService 
{
    /**
     * 查询危险化学品进口概况
     * 
     * @param id 危险化学品进口概况主键
     * @return 危险化学品进口概况
     */
    public ChemicalImportOverview selectChemicalImportOverviewById(Long id);

    /**
     * 查询危险化学品进口概况列表
     * 
     * @param chemicalImportOverview 危险化学品进口概况
     * @return 危险化学品进口概况集合
     */
    public List<ChemicalImportOverview> selectChemicalImportOverviewList(ChemicalImportOverview chemicalImportOverview);

    /**
     * 新增危险化学品进口概况
     * 
     * @param chemicalImportOverview 危险化学品进口概况
     * @return 结果
     */
    public int insertChemicalImportOverview(ChemicalImportOverview chemicalImportOverview);

    /**
     * 修改危险化学品进口概况
     * 
     * @param chemicalImportOverview 危险化学品进口概况
     * @return 结果
     */
    public int updateChemicalImportOverview(ChemicalImportOverview chemicalImportOverview);

    /**
     * 批量删除危险化学品进口概况
     * 
     * @param ids 需要删除的危险化学品进口概况主键集合
     * @return 结果
     */
    public int deleteChemicalImportOverviewByIds(Long[] ids);

    /**
     * 删除危险化学品进口概况信息
     * 
     * @param id 危险化学品进口概况主键
     * @return 结果
     */
    public int deleteChemicalImportOverviewById(Long id);

    /**
     * 导入危险化学品进口概况信息
     *
     * @param chemicalImportOverviewList 危险化学品进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalImportOverview(List<ChemicalImportOverview> chemicalImportOverviewList, Boolean isUpdateSupport, String operName);
}
