package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalOverview;
import com.qianjing.project.chemical.service.IChemicalOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品概况Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/overview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalOverviewController extends BaseController {

    private final IChemicalOverviewService chemicalOverviewService;

    /**
     * 查询危险化学品概况列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:overview:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalOverview chemicalOverview) {
        startPage();
        List<ChemicalOverview> list = chemicalOverviewService.selectChemicalOverviewList(chemicalOverview);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品概况列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:overview:export')")
    @Log(title = "危险化学品概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalOverview chemicalOverview) {
        List<ChemicalOverview> list = chemicalOverviewService.selectChemicalOverviewList(chemicalOverview);
        ExcelUtil<ChemicalOverview> util = new ExcelUtil<ChemicalOverview>(ChemicalOverview.class);
        return util.exportExcel(list, "危险化学品概况数据");
    }

    /**
     * 导入危险化学品概况列表
     */
    @Log(title = "危险化学品概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:overview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalOverview> util = new ExcelUtil<ChemicalOverview>(ChemicalOverview.class);
        List<ChemicalOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalOverviewService.importChemicalOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalOverview> util = new ExcelUtil<ChemicalOverview>(ChemicalOverview.class);
        util.importTemplateExcel(response, "危险化学品概况数据");
    }

    /**
     * 获取危险化学品概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:overview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalOverviewService.selectChemicalOverviewById(id));
    }

    /**
     * 新增危险化学品概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:overview:add')")
    @Log(title = "危险化学品概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalOverview chemicalOverview) {
        return toAjax(chemicalOverviewService.insertChemicalOverview(chemicalOverview));
    }

    /**
     * 修改危险化学品概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:overview:edit')")
    @Log(title = "危险化学品概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalOverview chemicalOverview) {
        return toAjax(chemicalOverviewService.updateChemicalOverview(chemicalOverview));
    }

    /**
     * 删除危险化学品概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:overview:remove')")
    @Log(title = "危险化学品概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalOverviewService.deleteChemicalOverviewByIds(ids));
    }
}
