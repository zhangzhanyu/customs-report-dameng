package com.qianjing.project.chemical.mapper;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalBusiness;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品口岸及目的地业务量Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface ChemicalBusinessMapper {

    ChemicalBusiness selectChemicalBusinessById(Long id);

    List<ChemicalBusiness> selectChemicalBusinessList(ChemicalBusiness chemicalBusiness);

    int insertChemicalBusiness(ChemicalBusiness chemicalBusiness);

    int updateChemicalBusiness(ChemicalBusiness chemicalBusiness);

    int deleteChemicalBusinessById(Long id);

    int deleteChemicalBusinessByIds(Long[] ids);
}
