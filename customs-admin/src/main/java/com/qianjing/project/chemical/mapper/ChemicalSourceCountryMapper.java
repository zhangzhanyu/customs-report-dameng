package com.qianjing.project.chemical.mapper;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品主要来源国Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface ChemicalSourceCountryMapper {

    ChemicalSourceCountry selectChemicalSourceCountryById(Long id);

    List<ChemicalSourceCountry> selectChemicalSourceCountryList(ChemicalSourceCountry chemicalSourceCountry);

    int insertChemicalSourceCountry(ChemicalSourceCountry chemicalSourceCountry);

    int updateChemicalSourceCountry(ChemicalSourceCountry chemicalSourceCountry);

    int deleteChemicalSourceCountryById(Long id);

    int deleteChemicalSourceCountryByIds(Long[] ids);
}
