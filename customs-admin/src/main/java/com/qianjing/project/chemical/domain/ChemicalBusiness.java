package com.qianjing.project.chemical.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品口岸及目的地业务量对象 chemical_business
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public class ChemicalBusiness extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private Long sortNum;

    /** 查验环节（B前置 M口岸 M目的地 A事后） */
    @Excel(name = "查验环节", readConverterExp = "B=前置,M=口岸,M=目的地,A=事后")
    private String checkState;

    /** 查验海关 */
    @Excel(name = "查验海关")
    private String checkCutoms;

    /** 查验批次 */
    @Excel(name = "查验批次")
    private Long checkBatch;

    /** 查验货物批 */
    @Excel(name = "查验货物批")
    private Long checkGoodsBatch;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setSortNum(Long sortNum){
        this.sortNum = sortNum;
    }

    public Long getSortNum(){
        return sortNum;
    }
    public void setCheckState(String checkState){
        this.checkState = checkState;
    }

    public String getCheckState(){
        return checkState;
    }
    public void setCheckCutoms(String checkCutoms){
        this.checkCutoms = checkCutoms;
    }

    public String getCheckCutoms(){
        return checkCutoms;
    }
    public void setCheckBatch(Long checkBatch){
        this.checkBatch = checkBatch;
    }

    public Long getCheckBatch(){
        return checkBatch;
    }
    public void setCheckGoodsBatch(Long checkGoodsBatch){
        this.checkGoodsBatch = checkGoodsBatch;
    }

    public Long getCheckGoodsBatch(){
        return checkGoodsBatch;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sortNum", getSortNum())
            .append("checkState", getCheckState())
            .append("checkCutoms", getCheckCutoms())
            .append("checkBatch", getCheckBatch())
            .append("checkGoodsBatch", getCheckGoodsBatch())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
