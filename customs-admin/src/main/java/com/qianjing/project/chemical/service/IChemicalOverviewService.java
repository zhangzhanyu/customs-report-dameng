package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalOverviewService 
{
    /**
     * 查询危险化学品概况
     * 
     * @param id 危险化学品概况主键
     * @return 危险化学品概况
     */
    public ChemicalOverview selectChemicalOverviewById(Long id);

    /**
     * 查询危险化学品概况列表
     * 
     * @param chemicalOverview 危险化学品概况
     * @return 危险化学品概况集合
     */
    public List<ChemicalOverview> selectChemicalOverviewList(ChemicalOverview chemicalOverview);

    /**
     * 新增危险化学品概况
     * 
     * @param chemicalOverview 危险化学品概况
     * @return 结果
     */
    public int insertChemicalOverview(ChemicalOverview chemicalOverview);

    /**
     * 修改危险化学品概况
     * 
     * @param chemicalOverview 危险化学品概况
     * @return 结果
     */
    public int updateChemicalOverview(ChemicalOverview chemicalOverview);

    /**
     * 批量删除危险化学品概况
     * 
     * @param ids 需要删除的危险化学品概况主键集合
     * @return 结果
     */
    public int deleteChemicalOverviewByIds(Long[] ids);

    /**
     * 删除危险化学品概况信息
     * 
     * @param id 危险化学品概况主键
     * @return 结果
     */
    public int deleteChemicalOverviewById(Long id);

    /**
     * 导入危险化学品概况信息
     *
     * @param chemicalOverviewList 危险化学品概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalOverview(List<ChemicalOverview> chemicalOverviewList, Boolean isUpdateSupport, String operName);
}
