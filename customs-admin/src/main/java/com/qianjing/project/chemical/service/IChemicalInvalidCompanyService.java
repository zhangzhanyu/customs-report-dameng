package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalInvalidCompany;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品不合格企业情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalInvalidCompanyService 
{
    /**
     * 查询危险化学品不合格企业情况
     * 
     * @param id 危险化学品不合格企业情况主键
     * @return 危险化学品不合格企业情况
     */
    public ChemicalInvalidCompany selectChemicalInvalidCompanyById(Long id);

    /**
     * 查询危险化学品不合格企业情况列表
     * 
     * @param chemicalInvalidCompany 危险化学品不合格企业情况
     * @return 危险化学品不合格企业情况集合
     */
    public List<ChemicalInvalidCompany> selectChemicalInvalidCompanyList(ChemicalInvalidCompany chemicalInvalidCompany);

    /**
     * 新增危险化学品不合格企业情况
     * 
     * @param chemicalInvalidCompany 危险化学品不合格企业情况
     * @return 结果
     */
    public int insertChemicalInvalidCompany(ChemicalInvalidCompany chemicalInvalidCompany);

    /**
     * 修改危险化学品不合格企业情况
     * 
     * @param chemicalInvalidCompany 危险化学品不合格企业情况
     * @return 结果
     */
    public int updateChemicalInvalidCompany(ChemicalInvalidCompany chemicalInvalidCompany);

    /**
     * 批量删除危险化学品不合格企业情况
     * 
     * @param ids 需要删除的危险化学品不合格企业情况主键集合
     * @return 结果
     */
    public int deleteChemicalInvalidCompanyByIds(Long[] ids);

    /**
     * 删除危险化学品不合格企业情况信息
     * 
     * @param id 危险化学品不合格企业情况主键
     * @return 结果
     */
    public int deleteChemicalInvalidCompanyById(Long id);

    /**
     * 导入危险化学品不合格企业情况信息
     *
     * @param chemicalInvalidCompanyList 危险化学品不合格企业情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalInvalidCompany(List<ChemicalInvalidCompany> chemicalInvalidCompanyList, Boolean isUpdateSupport, String operName);
}
