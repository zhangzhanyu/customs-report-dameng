package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalBusiness;
import com.qianjing.project.chemical.service.IChemicalBusinessService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品口岸及目的地业务量Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/business")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalBusinessController extends BaseController {

    private final IChemicalBusinessService chemicalBusinessService;

    /**
     * 查询危险化学品口岸及目的地业务量列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:business:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalBusiness chemicalBusiness) {
        startPage();
        List<ChemicalBusiness> list = chemicalBusinessService.selectChemicalBusinessList(chemicalBusiness);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品口岸及目的地业务量列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:business:export')")
    @Log(title = "危险化学品口岸及目的地业务量", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalBusiness chemicalBusiness) {
        List<ChemicalBusiness> list = chemicalBusinessService.selectChemicalBusinessList(chemicalBusiness);
        ExcelUtil<ChemicalBusiness> util = new ExcelUtil<ChemicalBusiness>(ChemicalBusiness.class);
        return util.exportExcel(list, "危险化学品口岸及目的地业务量数据");
    }

    /**
     * 导入危险化学品口岸及目的地业务量列表
     */
    @Log(title = "危险化学品口岸及目的地业务量", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:business:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalBusiness> util = new ExcelUtil<ChemicalBusiness>(ChemicalBusiness.class);
        List<ChemicalBusiness> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalBusinessService.importChemicalBusiness(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品口岸及目的地业务量导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalBusiness> util = new ExcelUtil<ChemicalBusiness>(ChemicalBusiness.class);
        util.importTemplateExcel(response, "危险化学品口岸及目的地业务量数据");
    }

    /**
     * 获取危险化学品口岸及目的地业务量详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:business:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalBusinessService.selectChemicalBusinessById(id));
    }

    /**
     * 新增危险化学品口岸及目的地业务量
     */
    @PreAuthorize("@ss.hasPermi('chemical:business:add')")
    @Log(title = "危险化学品口岸及目的地业务量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalBusiness chemicalBusiness) {
        return toAjax(chemicalBusinessService.insertChemicalBusiness(chemicalBusiness));
    }

    /**
     * 修改危险化学品口岸及目的地业务量
     */
    @PreAuthorize("@ss.hasPermi('chemical:business:edit')")
    @Log(title = "危险化学品口岸及目的地业务量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalBusiness chemicalBusiness) {
        return toAjax(chemicalBusinessService.updateChemicalBusiness(chemicalBusiness));
    }

    /**
     * 删除危险化学品口岸及目的地业务量
     */
    @PreAuthorize("@ss.hasPermi('chemical:business:remove')")
    @Log(title = "危险化学品口岸及目的地业务量", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalBusinessService.deleteChemicalBusinessByIds(ids));
    }
}
