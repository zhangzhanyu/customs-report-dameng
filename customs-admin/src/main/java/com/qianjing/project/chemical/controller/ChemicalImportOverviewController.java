package com.qianjing.project.chemical.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.chemical.domain.ChemicalImportOverview;
import com.qianjing.project.chemical.service.IChemicalImportOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品进口概况Controller
 *
 * @Created by 张占宇 2023-06-13 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/chemical/importOverview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChemicalImportOverviewController extends BaseController {

    private final IChemicalImportOverviewService chemicalImportOverviewService;

    /**
     * 查询危险化学品进口概况列表
     */
//    @PreAuthorize("@ss.hasPermi('chemical:importOverview:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChemicalImportOverview chemicalImportOverview) {
        startPage();
        List<ChemicalImportOverview> list = chemicalImportOverviewService.selectChemicalImportOverviewList(chemicalImportOverview);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品进口概况列表
     */
    @PreAuthorize("@ss.hasPermi('chemical:importOverview:export')")
    @Log(title = "危险化学品进口概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ChemicalImportOverview chemicalImportOverview) {
        List<ChemicalImportOverview> list = chemicalImportOverviewService.selectChemicalImportOverviewList(chemicalImportOverview);
        ExcelUtil<ChemicalImportOverview> util = new ExcelUtil<ChemicalImportOverview>(ChemicalImportOverview.class);
        return util.exportExcel(list, "危险化学品进口概况数据");
    }

    /**
     * 导入危险化学品进口概况列表
     */
    @Log(title = "危险化学品进口概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('chemical:importOverview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ChemicalImportOverview> util = new ExcelUtil<ChemicalImportOverview>(ChemicalImportOverview.class);
        List<ChemicalImportOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = chemicalImportOverviewService.importChemicalImportOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品进口概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ChemicalImportOverview> util = new ExcelUtil<ChemicalImportOverview>(ChemicalImportOverview.class);
        util.importTemplateExcel(response, "危险化学品进口概况数据");
    }

    /**
     * 获取危险化学品进口概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('chemical:importOverview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(chemicalImportOverviewService.selectChemicalImportOverviewById(id));
    }

    /**
     * 新增危险化学品进口概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:importOverview:add')")
    @Log(title = "危险化学品进口概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChemicalImportOverview chemicalImportOverview) {
        return toAjax(chemicalImportOverviewService.insertChemicalImportOverview(chemicalImportOverview));
    }

    /**
     * 修改危险化学品进口概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:importOverview:edit')")
    @Log(title = "危险化学品进口概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChemicalImportOverview chemicalImportOverview) {
        return toAjax(chemicalImportOverviewService.updateChemicalImportOverview(chemicalImportOverview));
    }

    /**
     * 删除危险化学品进口概况
     */
    @PreAuthorize("@ss.hasPermi('chemical:importOverview:remove')")
    @Log(title = "危险化学品进口概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(chemicalImportOverviewService.deleteChemicalImportOverviewByIds(ids));
    }
}
