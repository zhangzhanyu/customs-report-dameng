package com.qianjing.project.chemical.service;

import java.util.List;
import com.qianjing.project.chemical.domain.ChemicalInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品不合格关区情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public interface IChemicalInvalidCustomsService 
{
    /**
     * 查询危险化学品不合格关区情况
     * 
     * @param id 危险化学品不合格关区情况主键
     * @return 危险化学品不合格关区情况
     */
    public ChemicalInvalidCustoms selectChemicalInvalidCustomsById(Long id);

    /**
     * 查询危险化学品不合格关区情况列表
     * 
     * @param chemicalInvalidCustoms 危险化学品不合格关区情况
     * @return 危险化学品不合格关区情况集合
     */
    public List<ChemicalInvalidCustoms> selectChemicalInvalidCustomsList(ChemicalInvalidCustoms chemicalInvalidCustoms);

    /**
     * 新增危险化学品不合格关区情况
     * 
     * @param chemicalInvalidCustoms 危险化学品不合格关区情况
     * @return 结果
     */
    public int insertChemicalInvalidCustoms(ChemicalInvalidCustoms chemicalInvalidCustoms);

    /**
     * 修改危险化学品不合格关区情况
     * 
     * @param chemicalInvalidCustoms 危险化学品不合格关区情况
     * @return 结果
     */
    public int updateChemicalInvalidCustoms(ChemicalInvalidCustoms chemicalInvalidCustoms);

    /**
     * 批量删除危险化学品不合格关区情况
     * 
     * @param ids 需要删除的危险化学品不合格关区情况主键集合
     * @return 结果
     */
    public int deleteChemicalInvalidCustomsByIds(Long[] ids);

    /**
     * 删除危险化学品不合格关区情况信息
     * 
     * @param id 危险化学品不合格关区情况主键
     * @return 结果
     */
    public int deleteChemicalInvalidCustomsById(Long id);

    /**
     * 导入危险化学品不合格关区情况信息
     *
     * @param chemicalInvalidCustomsList 危险化学品不合格关区情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importChemicalInvalidCustoms(List<ChemicalInvalidCustoms> chemicalInvalidCustomsList, Boolean isUpdateSupport, String operName);
}
