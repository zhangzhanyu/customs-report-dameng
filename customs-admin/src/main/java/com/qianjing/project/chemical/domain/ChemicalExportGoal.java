package com.qianjing.project.chemical.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 危险化学品出口目的国对象 chemical_export_goal
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-13 下午3:00    张占宇          V1.0          initialize
 */
public class ChemicalExportGoal extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 目的国/地区 */
    @Excel(name = "目的国/地区")
    private String goalCountry;

    /** 批次 */
    @Excel(name = "批次")
    private Long batch;

    /** 重量(吨) */
    @Excel(name = "重量(吨)")
    private BigDecimal weight;

    /** 货值(万美元) */
    @Excel(name = "货值(万美元)")
    private BigDecimal usdPrice;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setGoalCountry(String goalCountry){
        this.goalCountry = goalCountry;
    }

    public String getGoalCountry(){
        return goalCountry;
    }
    public void setBatch(Long batch){
        this.batch = batch;
    }

    public Long getBatch(){
        return batch;
    }
    public void setWeight(BigDecimal weight){
        this.weight = weight;
    }

    public BigDecimal getWeight(){
        return weight;
    }
    public void setUsdPrice(BigDecimal usdPrice){
        this.usdPrice = usdPrice;
    }

    public BigDecimal getUsdPrice(){
        return usdPrice;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goalCountry", getGoalCountry())
            .append("batch", getBatch())
            .append("weight", getWeight())
            .append("usdPrice", getUsdPrice())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
