package com.qianjing.project.appraisal.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.appraisal.mapper.AppraisalGoodsImportMapper;
import com.qianjing.project.appraisal.domain.AppraisalGoodsImport;
import com.qianjing.project.appraisal.service.IAppraisalGoodsImportService;

/**
 * 主要进口商品重量鉴定情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@Service
public class AppraisalGoodsImportServiceImpl implements IAppraisalGoodsImportService 
{
    private static final Logger log = LoggerFactory.getLogger(AppraisalGoodsImportServiceImpl.class);

    @Autowired
    private AppraisalGoodsImportMapper appraisalGoodsImportMapper;

    /**
     * 查询主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImportId 主要进口商品重量鉴定情况统计主键
     * @return 主要进口商品重量鉴定情况统计
     */
    @Override
    public AppraisalGoodsImport selectAppraisalGoodsImportByAppraisalGoodsImportId(Long appraisalGoodsImportId)
    {
        return appraisalGoodsImportMapper.selectAppraisalGoodsImportByAppraisalGoodsImportId(appraisalGoodsImportId);
    }

    /**
     * 查询主要进口商品重量鉴定情况统计列表
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 主要进口商品重量鉴定情况统计
     */
    @Override
    public List<AppraisalGoodsImport> selectAppraisalGoodsImportList(AppraisalGoodsImport appraisalGoodsImport)
    {
        return appraisalGoodsImportMapper.selectAppraisalGoodsImportList(appraisalGoodsImport);
    }

    /**
     * 新增主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 结果
     */
    @Override
    public int insertAppraisalGoodsImport(AppraisalGoodsImport appraisalGoodsImport)
    {
        appraisalGoodsImport.setCreateTime(DateUtils.getNowDate());
        return appraisalGoodsImportMapper.insertAppraisalGoodsImport(appraisalGoodsImport);
    }

    /**
     * 修改主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 结果
     */
    @Override
    public int updateAppraisalGoodsImport(AppraisalGoodsImport appraisalGoodsImport)
    {
        appraisalGoodsImport.setUpdateTime(DateUtils.getNowDate());
        return appraisalGoodsImportMapper.updateAppraisalGoodsImport(appraisalGoodsImport);
    }

    /**
     * 批量删除主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImportIds 需要删除的主要进口商品重量鉴定情况统计主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalGoodsImportByAppraisalGoodsImportIds(Long[] appraisalGoodsImportIds)
    {
        return appraisalGoodsImportMapper.deleteAppraisalGoodsImportByAppraisalGoodsImportIds(appraisalGoodsImportIds);
    }

    /**
     * 删除主要进口商品重量鉴定情况统计信息
     * 
     * @param appraisalGoodsImportId 主要进口商品重量鉴定情况统计主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalGoodsImportByAppraisalGoodsImportId(Long appraisalGoodsImportId)
    {
        return appraisalGoodsImportMapper.deleteAppraisalGoodsImportByAppraisalGoodsImportId(appraisalGoodsImportId);
    }

    /**
     * 导入主要进口商品重量鉴定情况统计数据
     *
     * @param appraisalGoodsImportList appraisalGoodsImportList 主要进口商品重量鉴定情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importAppraisalGoodsImport(List<AppraisalGoodsImport> appraisalGoodsImportList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(appraisalGoodsImportList) || appraisalGoodsImportList.size() == 0) {
            throw new ServiceException("导入主要进口商品重量鉴定情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (AppraisalGoodsImport appraisalGoodsImport : appraisalGoodsImportList){
            try {
                // 验证是否存在
                if (true) {
                    appraisalGoodsImport.setCreateBy(operName);
                    this.insertAppraisalGoodsImport(appraisalGoodsImport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    appraisalGoodsImport.setUpdateBy(operName);
                    this.updateAppraisalGoodsImport(appraisalGoodsImport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
