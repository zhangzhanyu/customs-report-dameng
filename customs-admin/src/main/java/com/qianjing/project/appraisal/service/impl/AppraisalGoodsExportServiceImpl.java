package com.qianjing.project.appraisal.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.appraisal.mapper.AppraisalGoodsExportMapper;
import com.qianjing.project.appraisal.domain.AppraisalGoodsExport;
import com.qianjing.project.appraisal.service.IAppraisalGoodsExportService;

/**
 * 主要出口商品重量鉴定情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@Service
public class AppraisalGoodsExportServiceImpl implements IAppraisalGoodsExportService 
{
    private static final Logger log = LoggerFactory.getLogger(AppraisalGoodsExportServiceImpl.class);

    @Autowired
    private AppraisalGoodsExportMapper appraisalGoodsExportMapper;

    /**
     * 查询主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExportId 主要出口商品重量鉴定情况统计主键
     * @return 主要出口商品重量鉴定情况统计
     */
    @Override
    public AppraisalGoodsExport selectAppraisalGoodsExportByAppraisalGoodsExportId(Long appraisalGoodsExportId)
    {
        return appraisalGoodsExportMapper.selectAppraisalGoodsExportByAppraisalGoodsExportId(appraisalGoodsExportId);
    }

    /**
     * 查询主要出口商品重量鉴定情况统计列表
     * 
     * @param appraisalGoodsExport 主要出口商品重量鉴定情况统计
     * @return 主要出口商品重量鉴定情况统计
     */
    @Override
    public List<AppraisalGoodsExport> selectAppraisalGoodsExportList(AppraisalGoodsExport appraisalGoodsExport)
    {
        return appraisalGoodsExportMapper.selectAppraisalGoodsExportList(appraisalGoodsExport);
    }

    /**
     * 新增主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExport 主要出口商品重量鉴定情况统计
     * @return 结果
     */
    @Override
    public int insertAppraisalGoodsExport(AppraisalGoodsExport appraisalGoodsExport)
    {
        appraisalGoodsExport.setCreateTime(DateUtils.getNowDate());
        return appraisalGoodsExportMapper.insertAppraisalGoodsExport(appraisalGoodsExport);
    }

    /**
     * 修改主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExport 主要出口商品重量鉴定情况统计
     * @return 结果
     */
    @Override
    public int updateAppraisalGoodsExport(AppraisalGoodsExport appraisalGoodsExport)
    {
        appraisalGoodsExport.setUpdateTime(DateUtils.getNowDate());
        return appraisalGoodsExportMapper.updateAppraisalGoodsExport(appraisalGoodsExport);
    }

    /**
     * 批量删除主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExportIds 需要删除的主要出口商品重量鉴定情况统计主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalGoodsExportByAppraisalGoodsExportIds(Long[] appraisalGoodsExportIds)
    {
        return appraisalGoodsExportMapper.deleteAppraisalGoodsExportByAppraisalGoodsExportIds(appraisalGoodsExportIds);
    }

    /**
     * 删除主要出口商品重量鉴定情况统计信息
     * 
     * @param appraisalGoodsExportId 主要出口商品重量鉴定情况统计主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalGoodsExportByAppraisalGoodsExportId(Long appraisalGoodsExportId)
    {
        return appraisalGoodsExportMapper.deleteAppraisalGoodsExportByAppraisalGoodsExportId(appraisalGoodsExportId);
    }

    /**
     * 导入主要出口商品重量鉴定情况统计数据
     *
     * @param appraisalGoodsExportList appraisalGoodsExportList 主要出口商品重量鉴定情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importAppraisalGoodsExport(List<AppraisalGoodsExport> appraisalGoodsExportList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(appraisalGoodsExportList) || appraisalGoodsExportList.size() == 0) {
            throw new ServiceException("导入主要出口商品重量鉴定情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (AppraisalGoodsExport appraisalGoodsExport : appraisalGoodsExportList){
            try {
                // 验证是否存在
                if (true) {
                    appraisalGoodsExport.setCreateBy(operName);
                    this.insertAppraisalGoodsExport(appraisalGoodsExport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    appraisalGoodsExport.setUpdateBy(operName);
                    this.updateAppraisalGoodsExport(appraisalGoodsExport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
