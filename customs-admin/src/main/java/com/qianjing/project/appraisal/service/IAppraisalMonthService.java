package com.qianjing.project.appraisal.service;

import java.util.List;
import com.qianjing.project.appraisal.domain.AppraisalMonth;

/**
 * 进口货物固体废物属性鉴别情况汇总月报Service接口
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public interface IAppraisalMonthService 
{
    /**
     * 查询进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonthId 进口货物固体废物属性鉴别情况汇总月报主键
     * @return 进口货物固体废物属性鉴别情况汇总月报
     */
    public AppraisalMonth selectAppraisalMonthByAppraisalMonthId(Long appraisalMonthId);

    /**
     * 查询进口货物固体废物属性鉴别情况汇总月报列表
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 进口货物固体废物属性鉴别情况汇总月报集合
     */
    public List<AppraisalMonth> selectAppraisalMonthList(AppraisalMonth appraisalMonth);

    /**
     * 新增进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 结果
     */
    public int insertAppraisalMonth(AppraisalMonth appraisalMonth);

    /**
     * 修改进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 结果
     */
    public int updateAppraisalMonth(AppraisalMonth appraisalMonth);

    /**
     * 批量删除进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonthIds 需要删除的进口货物固体废物属性鉴别情况汇总月报主键集合
     * @return 结果
     */
    public int deleteAppraisalMonthByAppraisalMonthIds(Long[] appraisalMonthIds);

    /**
     * 删除进口货物固体废物属性鉴别情况汇总月报信息
     * 
     * @param appraisalMonthId 进口货物固体废物属性鉴别情况汇总月报主键
     * @return 结果
     */
    public int deleteAppraisalMonthByAppraisalMonthId(Long appraisalMonthId);

    /**
     * 导入进口货物固体废物属性鉴别情况汇总月报信息
     *
     * @param appraisalMonthList 进口货物固体废物属性鉴别情况汇总月报信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importAppraisalMonth(List<AppraisalMonth> appraisalMonthList, Boolean isUpdateSupport, String operName);
}
