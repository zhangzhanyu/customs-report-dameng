package com.qianjing.project.appraisal.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.appraisal.domain.AppraisalDetail;
import com.qianjing.project.appraisal.service.IAppraisalDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 上海地区检验鉴定机构清单Controller
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@RestController
@RequestMapping("/appraisal/detail")
public class AppraisalDetailController extends BaseController
{
    @Autowired
    private IAppraisalDetailService appraisalDetailService;

    /**
     * 查询上海地区检验鉴定机构清单列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppraisalDetail appraisalDetail)
    {
        startPage();
        List<AppraisalDetail> list = appraisalDetailService.selectAppraisalDetailList(appraisalDetail);
        return getDataTable(list);
    }

    /**
     * 导出上海地区检验鉴定机构清单列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:detail:export')")
    @Log(title = "上海地区检验鉴定机构清单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AppraisalDetail appraisalDetail)
    {
        List<AppraisalDetail> list = appraisalDetailService.selectAppraisalDetailList(appraisalDetail);
        ExcelUtil<AppraisalDetail> util = new ExcelUtil<AppraisalDetail>(AppraisalDetail.class);
        return util.exportExcel(list, "上海地区检验鉴定机构清单数据");
    }

    /**
     * 导入上海地区检验鉴定机构清单列表
     */
    @Log(title = "上海地区检验鉴定机构清单", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('appraisal:detail:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<AppraisalDetail> util = new ExcelUtil<AppraisalDetail>(AppraisalDetail.class);
        List<AppraisalDetail> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = appraisalDetailService.importAppraisalDetail(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载上海地区检验鉴定机构清单导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<AppraisalDetail> util = new ExcelUtil<AppraisalDetail>(AppraisalDetail.class);
        util.importTemplateExcel(response, "上海地区检验鉴定机构清单数据");
    }

    /**
     * 获取上海地区检验鉴定机构清单详细信息
     */
    @PreAuthorize("@ss.hasPermi('appraisal:detail:query')")
    @GetMapping(value = "/{hgAppraisalId}")
    public AjaxResult getInfo(@PathVariable("hgAppraisalId") Long hgAppraisalId)
    {
        return AjaxResult.success(appraisalDetailService.selectAppraisalDetailByHgAppraisalId(hgAppraisalId));
    }

    /**
     * 新增上海地区检验鉴定机构清单
     */
    @PreAuthorize("@ss.hasPermi('appraisal:detail:add')")
    @Log(title = "上海地区检验鉴定机构清单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppraisalDetail appraisalDetail)
    {
        return toAjax(appraisalDetailService.insertAppraisalDetail(appraisalDetail));
    }

    /**
     * 修改上海地区检验鉴定机构清单
     */
    @PreAuthorize("@ss.hasPermi('appraisal:detail:edit')")
    @Log(title = "上海地区检验鉴定机构清单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppraisalDetail appraisalDetail)
    {
        return toAjax(appraisalDetailService.updateAppraisalDetail(appraisalDetail));
    }

    /**
     * 删除上海地区检验鉴定机构清单
     */
    @PreAuthorize("@ss.hasPermi('appraisal:detail:remove')")
    @Log(title = "上海地区检验鉴定机构清单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{hgAppraisalIds}")
    public AjaxResult remove(@PathVariable Long[] hgAppraisalIds)
    {
        return toAjax(appraisalDetailService.deleteAppraisalDetailByHgAppraisalIds(hgAppraisalIds));
    }
}
