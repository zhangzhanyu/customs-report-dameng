package com.qianjing.project.appraisal.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.appraisal.domain.AppraisalGoodsExport;
import com.qianjing.project.appraisal.service.IAppraisalGoodsExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 主要出口商品重量鉴定情况统计Controller
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@RestController
@RequestMapping("/appraisal/goodsExport")
public class AppraisalGoodsExportController extends BaseController
{
    @Autowired
    private IAppraisalGoodsExportService appraisalGoodsExportService;

    /**
     * 查询主要出口商品重量鉴定情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsExport:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppraisalGoodsExport appraisalGoodsExport)
    {
        startPage();
        List<AppraisalGoodsExport> list = appraisalGoodsExportService.selectAppraisalGoodsExportList(appraisalGoodsExport);
        return getDataTable(list);
    }

    /**
     * 导出主要出口商品重量鉴定情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsExport:export')")
    @Log(title = "主要出口商品重量鉴定情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AppraisalGoodsExport appraisalGoodsExport)
    {
        List<AppraisalGoodsExport> list = appraisalGoodsExportService.selectAppraisalGoodsExportList(appraisalGoodsExport);
        ExcelUtil<AppraisalGoodsExport> util = new ExcelUtil<AppraisalGoodsExport>(AppraisalGoodsExport.class);
        return util.exportExcel(list, "主要出口商品重量鉴定情况统计数据");
    }

    /**
     * 导入主要出口商品重量鉴定情况统计列表
     */
    @Log(title = "主要出口商品重量鉴定情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('appraisal:goodsExport:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<AppraisalGoodsExport> util = new ExcelUtil<AppraisalGoodsExport>(AppraisalGoodsExport.class);
        List<AppraisalGoodsExport> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = appraisalGoodsExportService.importAppraisalGoodsExport(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载主要出口商品重量鉴定情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<AppraisalGoodsExport> util = new ExcelUtil<AppraisalGoodsExport>(AppraisalGoodsExport.class);
        util.importTemplateExcel(response, "主要出口商品重量鉴定情况统计数据");
    }

    /**
     * 获取主要出口商品重量鉴定情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsExport:query')")
    @GetMapping(value = "/{appraisalGoodsExportId}")
    public AjaxResult getInfo(@PathVariable("appraisalGoodsExportId") Long appraisalGoodsExportId)
    {
        return AjaxResult.success(appraisalGoodsExportService.selectAppraisalGoodsExportByAppraisalGoodsExportId(appraisalGoodsExportId));
    }

    /**
     * 新增主要出口商品重量鉴定情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsExport:add')")
    @Log(title = "主要出口商品重量鉴定情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppraisalGoodsExport appraisalGoodsExport)
    {
        return toAjax(appraisalGoodsExportService.insertAppraisalGoodsExport(appraisalGoodsExport));
    }

    /**
     * 修改主要出口商品重量鉴定情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsExport:edit')")
    @Log(title = "主要出口商品重量鉴定情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppraisalGoodsExport appraisalGoodsExport)
    {
        return toAjax(appraisalGoodsExportService.updateAppraisalGoodsExport(appraisalGoodsExport));
    }

    /**
     * 删除主要出口商品重量鉴定情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsExport:remove')")
    @Log(title = "主要出口商品重量鉴定情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appraisalGoodsExportIds}")
    public AjaxResult remove(@PathVariable Long[] appraisalGoodsExportIds)
    {
        return toAjax(appraisalGoodsExportService.deleteAppraisalGoodsExportByAppraisalGoodsExportIds(appraisalGoodsExportIds));
    }
}
