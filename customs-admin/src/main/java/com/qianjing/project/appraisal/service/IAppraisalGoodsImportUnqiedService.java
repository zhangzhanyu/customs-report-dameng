package com.qianjing.project.appraisal.service;

import java.util.List;
import com.qianjing.project.appraisal.domain.AppraisalGoodsImportUnqied;

/**
 * 进口商品重量鉴定不合格情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public interface IAppraisalGoodsImportUnqiedService 
{
    /**
     * 查询进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalUnqiedId 进口商品重量鉴定不合格情况统计主键
     * @return 进口商品重量鉴定不合格情况统计
     */
    public AppraisalGoodsImportUnqied selectAppraisalGoodsImportUnqiedByAppraisalUnqiedId(Long appraisalUnqiedId);

    /**
     * 查询进口商品重量鉴定不合格情况统计列表
     * 
     * @param appraisalGoodsImportUnqied 进口商品重量鉴定不合格情况统计
     * @return 进口商品重量鉴定不合格情况统计集合
     */
    public List<AppraisalGoodsImportUnqied> selectAppraisalGoodsImportUnqiedList(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied);

    /**
     * 新增进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalGoodsImportUnqied 进口商品重量鉴定不合格情况统计
     * @return 结果
     */
    public int insertAppraisalGoodsImportUnqied(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied);

    /**
     * 修改进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalGoodsImportUnqied 进口商品重量鉴定不合格情况统计
     * @return 结果
     */
    public int updateAppraisalGoodsImportUnqied(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied);

    /**
     * 批量删除进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalUnqiedIds 需要删除的进口商品重量鉴定不合格情况统计主键集合
     * @return 结果
     */
    public int deleteAppraisalGoodsImportUnqiedByAppraisalUnqiedIds(Long[] appraisalUnqiedIds);

    /**
     * 删除进口商品重量鉴定不合格情况统计信息
     * 
     * @param appraisalUnqiedId 进口商品重量鉴定不合格情况统计主键
     * @return 结果
     */
    public int deleteAppraisalGoodsImportUnqiedByAppraisalUnqiedId(Long appraisalUnqiedId);

    /**
     * 导入进口商品重量鉴定不合格情况统计信息
     *
     * @param appraisalGoodsImportUnqiedList 进口商品重量鉴定不合格情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importAppraisalGoodsImportUnqied(List<AppraisalGoodsImportUnqied> appraisalGoodsImportUnqiedList, Boolean isUpdateSupport, String operName);
}
