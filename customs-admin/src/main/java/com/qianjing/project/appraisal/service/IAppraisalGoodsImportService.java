package com.qianjing.project.appraisal.service;

import java.util.List;
import com.qianjing.project.appraisal.domain.AppraisalGoodsImport;

/**
 * 主要进口商品重量鉴定情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public interface IAppraisalGoodsImportService 
{
    /**
     * 查询主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImportId 主要进口商品重量鉴定情况统计主键
     * @return 主要进口商品重量鉴定情况统计
     */
    public AppraisalGoodsImport selectAppraisalGoodsImportByAppraisalGoodsImportId(Long appraisalGoodsImportId);

    /**
     * 查询主要进口商品重量鉴定情况统计列表
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 主要进口商品重量鉴定情况统计集合
     */
    public List<AppraisalGoodsImport> selectAppraisalGoodsImportList(AppraisalGoodsImport appraisalGoodsImport);

    /**
     * 新增主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 结果
     */
    public int insertAppraisalGoodsImport(AppraisalGoodsImport appraisalGoodsImport);

    /**
     * 修改主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 结果
     */
    public int updateAppraisalGoodsImport(AppraisalGoodsImport appraisalGoodsImport);

    /**
     * 批量删除主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImportIds 需要删除的主要进口商品重量鉴定情况统计主键集合
     * @return 结果
     */
    public int deleteAppraisalGoodsImportByAppraisalGoodsImportIds(Long[] appraisalGoodsImportIds);

    /**
     * 删除主要进口商品重量鉴定情况统计信息
     * 
     * @param appraisalGoodsImportId 主要进口商品重量鉴定情况统计主键
     * @return 结果
     */
    public int deleteAppraisalGoodsImportByAppraisalGoodsImportId(Long appraisalGoodsImportId);

    /**
     * 导入主要进口商品重量鉴定情况统计信息
     *
     * @param appraisalGoodsImportList 主要进口商品重量鉴定情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importAppraisalGoodsImport(List<AppraisalGoodsImport> appraisalGoodsImportList, Boolean isUpdateSupport, String operName);
}
