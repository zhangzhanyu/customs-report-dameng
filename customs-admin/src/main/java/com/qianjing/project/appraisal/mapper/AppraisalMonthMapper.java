package com.qianjing.project.appraisal.mapper;

import java.util.List;
import com.qianjing.project.appraisal.domain.AppraisalMonth;

/**
 * 进口货物固体废物属性鉴别情况汇总月报Mapper接口
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public interface AppraisalMonthMapper 
{
    /**
     * 查询进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonthId 进口货物固体废物属性鉴别情况汇总月报主键
     * @return 进口货物固体废物属性鉴别情况汇总月报
     */
    public AppraisalMonth selectAppraisalMonthByAppraisalMonthId(Long appraisalMonthId);

    /**
     * 查询进口货物固体废物属性鉴别情况汇总月报列表
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 进口货物固体废物属性鉴别情况汇总月报集合
     */
    public List<AppraisalMonth> selectAppraisalMonthList(AppraisalMonth appraisalMonth);

    /**
     * 新增进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 结果
     */
    public int insertAppraisalMonth(AppraisalMonth appraisalMonth);

    /**
     * 修改进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 结果
     */
    public int updateAppraisalMonth(AppraisalMonth appraisalMonth);

    /**
     * 删除进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonthId 进口货物固体废物属性鉴别情况汇总月报主键
     * @return 结果
     */
    public int deleteAppraisalMonthByAppraisalMonthId(Long appraisalMonthId);

    /**
     * 批量删除进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonthIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAppraisalMonthByAppraisalMonthIds(Long[] appraisalMonthIds);
}
