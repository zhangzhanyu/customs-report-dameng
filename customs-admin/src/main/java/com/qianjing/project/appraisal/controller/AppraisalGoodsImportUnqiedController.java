package com.qianjing.project.appraisal.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.appraisal.domain.AppraisalGoodsImportUnqied;
import com.qianjing.project.appraisal.service.IAppraisalGoodsImportUnqiedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口商品重量鉴定不合格情况统计Controller
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@RestController
@RequestMapping("/appraisal/unqied")
public class AppraisalGoodsImportUnqiedController extends BaseController
{
    @Autowired
    private IAppraisalGoodsImportUnqiedService appraisalGoodsImportUnqiedService;

    /**
     * 查询进口商品重量鉴定不合格情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:unqied:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied)
    {
        startPage();
        List<AppraisalGoodsImportUnqied> list = appraisalGoodsImportUnqiedService.selectAppraisalGoodsImportUnqiedList(appraisalGoodsImportUnqied);
        return getDataTable(list);
    }

    /**
     * 导出进口商品重量鉴定不合格情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:unqied:export')")
    @Log(title = "进口商品重量鉴定不合格情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied)
    {
        List<AppraisalGoodsImportUnqied> list = appraisalGoodsImportUnqiedService.selectAppraisalGoodsImportUnqiedList(appraisalGoodsImportUnqied);
        ExcelUtil<AppraisalGoodsImportUnqied> util = new ExcelUtil<AppraisalGoodsImportUnqied>(AppraisalGoodsImportUnqied.class);
        return util.exportExcel(list, "进口商品重量鉴定不合格情况统计数据");
    }

    /**
     * 导入进口商品重量鉴定不合格情况统计列表
     */
    @Log(title = "进口商品重量鉴定不合格情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('appraisal:unqied:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<AppraisalGoodsImportUnqied> util = new ExcelUtil<AppraisalGoodsImportUnqied>(AppraisalGoodsImportUnqied.class);
        List<AppraisalGoodsImportUnqied> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = appraisalGoodsImportUnqiedService.importAppraisalGoodsImportUnqied(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口商品重量鉴定不合格情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<AppraisalGoodsImportUnqied> util = new ExcelUtil<AppraisalGoodsImportUnqied>(AppraisalGoodsImportUnqied.class);
        util.importTemplateExcel(response, "进口商品重量鉴定不合格情况统计数据");
    }

    /**
     * 获取进口商品重量鉴定不合格情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('appraisal:unqied:query')")
    @GetMapping(value = "/{appraisalUnqiedId}")
    public AjaxResult getInfo(@PathVariable("appraisalUnqiedId") Long appraisalUnqiedId)
    {
        return AjaxResult.success(appraisalGoodsImportUnqiedService.selectAppraisalGoodsImportUnqiedByAppraisalUnqiedId(appraisalUnqiedId));
    }

    /**
     * 新增进口商品重量鉴定不合格情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:unqied:add')")
    @Log(title = "进口商品重量鉴定不合格情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppraisalGoodsImportUnqied appraisalGoodsImportUnqied)
    {
        return toAjax(appraisalGoodsImportUnqiedService.insertAppraisalGoodsImportUnqied(appraisalGoodsImportUnqied));
    }

    /**
     * 修改进口商品重量鉴定不合格情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:unqied:edit')")
    @Log(title = "进口商品重量鉴定不合格情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppraisalGoodsImportUnqied appraisalGoodsImportUnqied)
    {
        return toAjax(appraisalGoodsImportUnqiedService.updateAppraisalGoodsImportUnqied(appraisalGoodsImportUnqied));
    }

    /**
     * 删除进口商品重量鉴定不合格情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:unqied:remove')")
    @Log(title = "进口商品重量鉴定不合格情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appraisalUnqiedIds}")
    public AjaxResult remove(@PathVariable Long[] appraisalUnqiedIds)
    {
        return toAjax(appraisalGoodsImportUnqiedService.deleteAppraisalGoodsImportUnqiedByAppraisalUnqiedIds(appraisalUnqiedIds));
    }
}
