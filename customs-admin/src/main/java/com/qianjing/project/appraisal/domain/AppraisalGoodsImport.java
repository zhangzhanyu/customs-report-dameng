package com.qianjing.project.appraisal.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 主要进口商品重量鉴定情况统计对象 appraisal_goods_import
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public class AppraisalGoodsImport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long appraisalGoodsImportId;

    /** 商品种类 */
    @Excel(name = "商品种类")
    private String goodsCategory;

    /** 鉴重方式 */
    @Excel(name = "鉴重方式")
    private String appraisalMode;

    /** 批次(批)(未申请重量鉴定) */
    @Excel(name = "批次(批)(未申请重量鉴定)")
    private String notAppliedBatch;

    /** 重量(吨)(未申请重量鉴定) */
    @Excel(name = "重量(吨)(未申请重量鉴定)")
    private String notAppliedWeight;

    /** 商品总值(万美元)(未申请重量鉴定) */
    @Excel(name = "商品总值(万美元)(未申请重量鉴定)")
    private String notAppliedTotalValue;

    /** 主要进口国家和地区(未申请重量鉴定) */
    @Excel(name = "主要进口国家和地区(未申请重量鉴定)")
    private String notAppliedRegion;

    /** 鉴重批次(批)(海关实施重量鉴定) */
    @Excel(name = "鉴重批次(批)(海关实施重量鉴定)")
    private String appraisalBatch;

    /** 鉴定重量(吨)(海关实施重量鉴定) */
    @Excel(name = "鉴定重量(吨)(海关实施重量鉴定)")
    private String appraisalWeight;

    /** 商品总值(万美元)(海关实施重量鉴定) */
    @Excel(name = "商品总值(万美元)(海关实施重量鉴定)")
    private String appraisalTotalValue;

    /** 主要进口国家和地区(海关实施重量鉴定) */
    @Excel(name = "主要进口国家和地区(海关实施重量鉴定)")
    private String appraisalRegion;

    /** 总批次(总体情况)(短重情况) */
    @Excel(name = "总批次(总体情况)(短重情况)")
    private String generalTotalBatch;

    /** 总重量(吨)(总体情况)(短重情况) */
    @Excel(name = "总重量(吨)(总体情况)(短重情况)")
    private String generalTotalWeight;

    /** 总金额(美元)(总体情况)(短重情况) */
    @Excel(name = "总金额(美元)(总体情况)(短重情况)")
    private String generalTotalAmount;

    /** 批次(短重超过0.3%情况)(短重情况) */
    @Excel(name = "批次(短重超过0.3%情况)(短重情况)")
    private String shortWeightBatch;

    /** 重量(短重超过0.3%情况)(短重情况) */
    @Excel(name = "重量(短重超过0.3%情况)(短重情况)")
    private String shortWeightWeight;

    /** 总金额(短重超过0.3%情况)(短重情况) */
    @Excel(name = "总金额(短重超过0.3%情况)(短重情况)")
    private String shortWeightAmount;

    /** 批次(溢重情况) */
    @Excel(name = "批次(溢重情况)")
    private String excessWeightBatch;

    /** 重量(吨)(溢重情况) */
    @Excel(name = "重量(吨)(溢重情况)")
    private String excessWeight;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setAppraisalGoodsImportId(Long appraisalGoodsImportId) 
    {
        this.appraisalGoodsImportId = appraisalGoodsImportId;
    }

    public Long getAppraisalGoodsImportId() 
    {
        return appraisalGoodsImportId;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setAppraisalMode(String appraisalMode) 
    {
        this.appraisalMode = appraisalMode;
    }

    public String getAppraisalMode() 
    {
        return appraisalMode;
    }
    public void setNotAppliedBatch(String notAppliedBatch) 
    {
        this.notAppliedBatch = notAppliedBatch;
    }

    public String getNotAppliedBatch() 
    {
        return notAppliedBatch;
    }
    public void setNotAppliedWeight(String notAppliedWeight) 
    {
        this.notAppliedWeight = notAppliedWeight;
    }

    public String getNotAppliedWeight() 
    {
        return notAppliedWeight;
    }
    public void setNotAppliedTotalValue(String notAppliedTotalValue) 
    {
        this.notAppliedTotalValue = notAppliedTotalValue;
    }

    public String getNotAppliedTotalValue() 
    {
        return notAppliedTotalValue;
    }
    public void setNotAppliedRegion(String notAppliedRegion) 
    {
        this.notAppliedRegion = notAppliedRegion;
    }

    public String getNotAppliedRegion() 
    {
        return notAppliedRegion;
    }
    public void setAppraisalBatch(String appraisalBatch) 
    {
        this.appraisalBatch = appraisalBatch;
    }

    public String getAppraisalBatch() 
    {
        return appraisalBatch;
    }
    public void setAppraisalWeight(String appraisalWeight) 
    {
        this.appraisalWeight = appraisalWeight;
    }

    public String getAppraisalWeight() 
    {
        return appraisalWeight;
    }
    public void setAppraisalTotalValue(String appraisalTotalValue) 
    {
        this.appraisalTotalValue = appraisalTotalValue;
    }

    public String getAppraisalTotalValue() 
    {
        return appraisalTotalValue;
    }
    public void setAppraisalRegion(String appraisalRegion) 
    {
        this.appraisalRegion = appraisalRegion;
    }

    public String getAppraisalRegion() 
    {
        return appraisalRegion;
    }
    public void setGeneralTotalBatch(String generalTotalBatch) 
    {
        this.generalTotalBatch = generalTotalBatch;
    }

    public String getGeneralTotalBatch() 
    {
        return generalTotalBatch;
    }
    public void setGeneralTotalWeight(String generalTotalWeight) 
    {
        this.generalTotalWeight = generalTotalWeight;
    }

    public String getGeneralTotalWeight() 
    {
        return generalTotalWeight;
    }
    public void setGeneralTotalAmount(String generalTotalAmount) 
    {
        this.generalTotalAmount = generalTotalAmount;
    }

    public String getGeneralTotalAmount() 
    {
        return generalTotalAmount;
    }
    public void setShortWeightBatch(String shortWeightBatch) 
    {
        this.shortWeightBatch = shortWeightBatch;
    }

    public String getShortWeightBatch() 
    {
        return shortWeightBatch;
    }
    public void setShortWeightWeight(String shortWeightWeight) 
    {
        this.shortWeightWeight = shortWeightWeight;
    }

    public String getShortWeightWeight() 
    {
        return shortWeightWeight;
    }
    public void setShortWeightAmount(String shortWeightAmount) 
    {
        this.shortWeightAmount = shortWeightAmount;
    }

    public String getShortWeightAmount() 
    {
        return shortWeightAmount;
    }
    public void setExcessWeightBatch(String excessWeightBatch) 
    {
        this.excessWeightBatch = excessWeightBatch;
    }

    public String getExcessWeightBatch() 
    {
        return excessWeightBatch;
    }
    public void setExcessWeight(String excessWeight) 
    {
        this.excessWeight = excessWeight;
    }

    public String getExcessWeight() 
    {
        return excessWeight;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appraisalGoodsImportId", getAppraisalGoodsImportId())
            .append("goodsCategory", getGoodsCategory())
            .append("appraisalMode", getAppraisalMode())
            .append("notAppliedBatch", getNotAppliedBatch())
            .append("notAppliedWeight", getNotAppliedWeight())
            .append("notAppliedTotalValue", getNotAppliedTotalValue())
            .append("notAppliedRegion", getNotAppliedRegion())
            .append("appraisalBatch", getAppraisalBatch())
            .append("appraisalWeight", getAppraisalWeight())
            .append("appraisalTotalValue", getAppraisalTotalValue())
            .append("appraisalRegion", getAppraisalRegion())
            .append("generalTotalBatch", getGeneralTotalBatch())
            .append("generalTotalWeight", getGeneralTotalWeight())
            .append("generalTotalAmount", getGeneralTotalAmount())
            .append("shortWeightBatch", getShortWeightBatch())
            .append("shortWeightWeight", getShortWeightWeight())
            .append("shortWeightAmount", getShortWeightAmount())
            .append("excessWeightBatch", getExcessWeightBatch())
            .append("excessWeight", getExcessWeight())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
