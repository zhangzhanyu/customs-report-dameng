package com.qianjing.project.appraisal.service;

import java.util.List;
import com.qianjing.project.appraisal.domain.AppraisalDetail;

/**
 * 上海地区检验鉴定机构清单Service接口
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public interface IAppraisalDetailService 
{
    /**
     * 查询上海地区检验鉴定机构清单
     * 
     * @param hgAppraisalId 上海地区检验鉴定机构清单主键
     * @return 上海地区检验鉴定机构清单
     */
    public AppraisalDetail selectAppraisalDetailByHgAppraisalId(Long hgAppraisalId);

    /**
     * 查询上海地区检验鉴定机构清单列表
     * 
     * @param appraisalDetail 上海地区检验鉴定机构清单
     * @return 上海地区检验鉴定机构清单集合
     */
    public List<AppraisalDetail> selectAppraisalDetailList(AppraisalDetail appraisalDetail);

    /**
     * 新增上海地区检验鉴定机构清单
     * 
     * @param appraisalDetail 上海地区检验鉴定机构清单
     * @return 结果
     */
    public int insertAppraisalDetail(AppraisalDetail appraisalDetail);

    /**
     * 修改上海地区检验鉴定机构清单
     * 
     * @param appraisalDetail 上海地区检验鉴定机构清单
     * @return 结果
     */
    public int updateAppraisalDetail(AppraisalDetail appraisalDetail);

    /**
     * 批量删除上海地区检验鉴定机构清单
     * 
     * @param hgAppraisalIds 需要删除的上海地区检验鉴定机构清单主键集合
     * @return 结果
     */
    public int deleteAppraisalDetailByHgAppraisalIds(Long[] hgAppraisalIds);

    /**
     * 删除上海地区检验鉴定机构清单信息
     * 
     * @param hgAppraisalId 上海地区检验鉴定机构清单主键
     * @return 结果
     */
    public int deleteAppraisalDetailByHgAppraisalId(Long hgAppraisalId);

    /**
     * 导入上海地区检验鉴定机构清单信息
     *
     * @param appraisalDetailList 上海地区检验鉴定机构清单信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importAppraisalDetail(List<AppraisalDetail> appraisalDetailList, Boolean isUpdateSupport, String operName);
}
