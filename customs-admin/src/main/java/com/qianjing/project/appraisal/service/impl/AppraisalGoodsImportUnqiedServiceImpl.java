package com.qianjing.project.appraisal.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.appraisal.mapper.AppraisalGoodsImportUnqiedMapper;
import com.qianjing.project.appraisal.domain.AppraisalGoodsImportUnqied;
import com.qianjing.project.appraisal.service.IAppraisalGoodsImportUnqiedService;

/**
 * 进口商品重量鉴定不合格情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@Service
public class AppraisalGoodsImportUnqiedServiceImpl implements IAppraisalGoodsImportUnqiedService 
{
    private static final Logger log = LoggerFactory.getLogger(AppraisalGoodsImportUnqiedServiceImpl.class);

    @Autowired
    private AppraisalGoodsImportUnqiedMapper appraisalGoodsImportUnqiedMapper;

    /**
     * 查询进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalUnqiedId 进口商品重量鉴定不合格情况统计主键
     * @return 进口商品重量鉴定不合格情况统计
     */
    @Override
    public AppraisalGoodsImportUnqied selectAppraisalGoodsImportUnqiedByAppraisalUnqiedId(Long appraisalUnqiedId)
    {
        return appraisalGoodsImportUnqiedMapper.selectAppraisalGoodsImportUnqiedByAppraisalUnqiedId(appraisalUnqiedId);
    }

    /**
     * 查询进口商品重量鉴定不合格情况统计列表
     * 
     * @param appraisalGoodsImportUnqied 进口商品重量鉴定不合格情况统计
     * @return 进口商品重量鉴定不合格情况统计
     */
    @Override
    public List<AppraisalGoodsImportUnqied> selectAppraisalGoodsImportUnqiedList(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied)
    {
        return appraisalGoodsImportUnqiedMapper.selectAppraisalGoodsImportUnqiedList(appraisalGoodsImportUnqied);
    }

    /**
     * 新增进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalGoodsImportUnqied 进口商品重量鉴定不合格情况统计
     * @return 结果
     */
    @Override
    public int insertAppraisalGoodsImportUnqied(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied)
    {
        appraisalGoodsImportUnqied.setCreateTime(DateUtils.getNowDate());
        return appraisalGoodsImportUnqiedMapper.insertAppraisalGoodsImportUnqied(appraisalGoodsImportUnqied);
    }

    /**
     * 修改进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalGoodsImportUnqied 进口商品重量鉴定不合格情况统计
     * @return 结果
     */
    @Override
    public int updateAppraisalGoodsImportUnqied(AppraisalGoodsImportUnqied appraisalGoodsImportUnqied)
    {
        appraisalGoodsImportUnqied.setUpdateTime(DateUtils.getNowDate());
        return appraisalGoodsImportUnqiedMapper.updateAppraisalGoodsImportUnqied(appraisalGoodsImportUnqied);
    }

    /**
     * 批量删除进口商品重量鉴定不合格情况统计
     * 
     * @param appraisalUnqiedIds 需要删除的进口商品重量鉴定不合格情况统计主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalGoodsImportUnqiedByAppraisalUnqiedIds(Long[] appraisalUnqiedIds)
    {
        return appraisalGoodsImportUnqiedMapper.deleteAppraisalGoodsImportUnqiedByAppraisalUnqiedIds(appraisalUnqiedIds);
    }

    /**
     * 删除进口商品重量鉴定不合格情况统计信息
     * 
     * @param appraisalUnqiedId 进口商品重量鉴定不合格情况统计主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalGoodsImportUnqiedByAppraisalUnqiedId(Long appraisalUnqiedId)
    {
        return appraisalGoodsImportUnqiedMapper.deleteAppraisalGoodsImportUnqiedByAppraisalUnqiedId(appraisalUnqiedId);
    }

    /**
     * 导入进口商品重量鉴定不合格情况统计数据
     *
     * @param appraisalGoodsImportUnqiedList appraisalGoodsImportUnqiedList 进口商品重量鉴定不合格情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importAppraisalGoodsImportUnqied(List<AppraisalGoodsImportUnqied> appraisalGoodsImportUnqiedList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(appraisalGoodsImportUnqiedList) || appraisalGoodsImportUnqiedList.size() == 0) {
            throw new ServiceException("导入进口商品重量鉴定不合格情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (AppraisalGoodsImportUnqied appraisalGoodsImportUnqied : appraisalGoodsImportUnqiedList){
            try {
                // 验证是否存在
                if (true) {
                    appraisalGoodsImportUnqied.setCreateBy(operName);
                    this.insertAppraisalGoodsImportUnqied(appraisalGoodsImportUnqied);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    appraisalGoodsImportUnqied.setUpdateBy(operName);
                    this.updateAppraisalGoodsImportUnqied(appraisalGoodsImportUnqied);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
