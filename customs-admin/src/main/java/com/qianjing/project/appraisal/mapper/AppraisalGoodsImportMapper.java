package com.qianjing.project.appraisal.mapper;

import java.util.List;
import com.qianjing.project.appraisal.domain.AppraisalGoodsImport;

/**
 * 主要进口商品重量鉴定情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public interface AppraisalGoodsImportMapper 
{
    /**
     * 查询主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImportId 主要进口商品重量鉴定情况统计主键
     * @return 主要进口商品重量鉴定情况统计
     */
    public AppraisalGoodsImport selectAppraisalGoodsImportByAppraisalGoodsImportId(Long appraisalGoodsImportId);

    /**
     * 查询主要进口商品重量鉴定情况统计列表
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 主要进口商品重量鉴定情况统计集合
     */
    public List<AppraisalGoodsImport> selectAppraisalGoodsImportList(AppraisalGoodsImport appraisalGoodsImport);

    /**
     * 新增主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 结果
     */
    public int insertAppraisalGoodsImport(AppraisalGoodsImport appraisalGoodsImport);

    /**
     * 修改主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImport 主要进口商品重量鉴定情况统计
     * @return 结果
     */
    public int updateAppraisalGoodsImport(AppraisalGoodsImport appraisalGoodsImport);

    /**
     * 删除主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImportId 主要进口商品重量鉴定情况统计主键
     * @return 结果
     */
    public int deleteAppraisalGoodsImportByAppraisalGoodsImportId(Long appraisalGoodsImportId);

    /**
     * 批量删除主要进口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsImportIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAppraisalGoodsImportByAppraisalGoodsImportIds(Long[] appraisalGoodsImportIds);
}
