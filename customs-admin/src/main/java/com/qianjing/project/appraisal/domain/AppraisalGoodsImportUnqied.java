package com.qianjing.project.appraisal.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口商品重量鉴定不合格情况统计对象 appraisal_goods_import_unqied
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public class AppraisalGoodsImportUnqied extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long appraisalUnqiedId;

    /** 报检号 */
    @Excel(name = "报检号")
    private String inspectionCode;

    /** 发货人 */
    @Excel(name = "发货人")
    private String consignor;

    /** 收货人 */
    @Excel(name = "收货人")
    private String consignee;

    /** 货物名称 */
    @Excel(name = "货物名称")
    private String gName;

    /** 鉴重方式 */
    @Excel(name = "鉴重方式")
    private String appraisalMode;

    /** 标箱数/艘 */
    @Excel(name = "标箱数/艘")
    private String boxShip;

    /** 原产地 */
    @Excel(name = "原产地")
    private String originCountry;

    /** 装运港 */
    @Excel(name = "装运港")
    private String loadingPort;

    /** 船名/航次 */
    @Excel(name = "船名/航次")
    private String shipName;

    /** 卸毕日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "卸毕日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date unloadingDate;

    /** 货值(美元) */
    @Excel(name = "货值(美元)")
    private String usdPrice;

    /** 申报重量(吨) */
    @Excel(name = "申报重量(吨)")
    private String declaredWeight;

    /** 最终检验重量(吨) */
    @Excel(name = "最终检验重量(吨)")
    private String finalWeight;

    /** 短少重量 */
    @Excel(name = "短少重量")
    private String shortWeight;

    /** 短重率 */
    @Excel(name = "短重率")
    private String shortWeightRate;

    /** 短重金额 */
    @Excel(name = "短重金额")
    private String shortWeightAmount;

    /** 处理情况 */
    @Excel(name = "处理情况")
    private String treatment;

    /** 实施检验机构名称 */
    @Excel(name = "实施检验机构名称")
    private String inspectionName;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setAppraisalUnqiedId(Long appraisalUnqiedId) 
    {
        this.appraisalUnqiedId = appraisalUnqiedId;
    }

    public Long getAppraisalUnqiedId() 
    {
        return appraisalUnqiedId;
    }
    public void setInspectionCode(String inspectionCode) 
    {
        this.inspectionCode = inspectionCode;
    }

    public String getInspectionCode() 
    {
        return inspectionCode;
    }
    public void setConsignor(String consignor) 
    {
        this.consignor = consignor;
    }

    public String getConsignor() 
    {
        return consignor;
    }
    public void setConsignee(String consignee) 
    {
        this.consignee = consignee;
    }

    public String getConsignee() 
    {
        return consignee;
    }
    public void setgName(String gName) 
    {
        this.gName = gName;
    }

    public String getgName() 
    {
        return gName;
    }
    public void setAppraisalMode(String appraisalMode) 
    {
        this.appraisalMode = appraisalMode;
    }

    public String getAppraisalMode() 
    {
        return appraisalMode;
    }
    public void setBoxShip(String boxShip) 
    {
        this.boxShip = boxShip;
    }

    public String getBoxShip() 
    {
        return boxShip;
    }
    public void setOriginCountry(String originCountry) 
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry() 
    {
        return originCountry;
    }
    public void setLoadingPort(String loadingPort) 
    {
        this.loadingPort = loadingPort;
    }

    public String getLoadingPort() 
    {
        return loadingPort;
    }
    public void setShipName(String shipName) 
    {
        this.shipName = shipName;
    }

    public String getShipName() 
    {
        return shipName;
    }
    public void setUnloadingDate(Date unloadingDate) 
    {
        this.unloadingDate = unloadingDate;
    }

    public Date getUnloadingDate() 
    {
        return unloadingDate;
    }
    public void setUsdPrice(String usdPrice) 
    {
        this.usdPrice = usdPrice;
    }

    public String getUsdPrice() 
    {
        return usdPrice;
    }
    public void setDeclaredWeight(String declaredWeight) 
    {
        this.declaredWeight = declaredWeight;
    }

    public String getDeclaredWeight() 
    {
        return declaredWeight;
    }
    public void setFinalWeight(String finalWeight) 
    {
        this.finalWeight = finalWeight;
    }

    public String getFinalWeight() 
    {
        return finalWeight;
    }
    public void setShortWeight(String shortWeight) 
    {
        this.shortWeight = shortWeight;
    }

    public String getShortWeight() 
    {
        return shortWeight;
    }
    public void setShortWeightRate(String shortWeightRate) 
    {
        this.shortWeightRate = shortWeightRate;
    }

    public String getShortWeightRate() 
    {
        return shortWeightRate;
    }
    public void setShortWeightAmount(String shortWeightAmount) 
    {
        this.shortWeightAmount = shortWeightAmount;
    }

    public String getShortWeightAmount() 
    {
        return shortWeightAmount;
    }
    public void setTreatment(String treatment) 
    {
        this.treatment = treatment;
    }

    public String getTreatment() 
    {
        return treatment;
    }
    public void setInspectionName(String inspectionName) 
    {
        this.inspectionName = inspectionName;
    }

    public String getInspectionName() 
    {
        return inspectionName;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appraisalUnqiedId", getAppraisalUnqiedId())
            .append("inspectionCode", getInspectionCode())
            .append("consignor", getConsignor())
            .append("consignee", getConsignee())
            .append("gName", getgName())
            .append("appraisalMode", getAppraisalMode())
            .append("boxShip", getBoxShip())
            .append("originCountry", getOriginCountry())
            .append("loadingPort", getLoadingPort())
            .append("shipName", getShipName())
            .append("unloadingDate", getUnloadingDate())
            .append("usdPrice", getUsdPrice())
            .append("declaredWeight", getDeclaredWeight())
            .append("finalWeight", getFinalWeight())
            .append("shortWeight", getShortWeight())
            .append("shortWeightRate", getShortWeightRate())
            .append("shortWeightAmount", getShortWeightAmount())
            .append("treatment", getTreatment())
            .append("inspectionName", getInspectionName())
            .append("remarks", getRemarks())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
