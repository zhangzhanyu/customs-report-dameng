package com.qianjing.project.appraisal.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 主要出口商品重量鉴定情况统计对象 appraisal_goods_export
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public class AppraisalGoodsExport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long appraisalGoodsExportId;

    /** 商品种类 */
    @Excel(name = "商品种类")
    private String goodsCategory;

    /** 鉴重方式 */
    @Excel(name = "鉴重方式")
    private String appraisalMode;

    /** 鉴重批次(批) */
    @Excel(name = "鉴重批次(批)")
    private String appraisalBatch;

    /** 鉴定重量(吨) */
    @Excel(name = "鉴定重量(吨)")
    private String appraisalWeight;

    /** 商品总值(美元) */
    @Excel(name = "商品总值(美元)")
    private String totalValue;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setAppraisalGoodsExportId(Long appraisalGoodsExportId) 
    {
        this.appraisalGoodsExportId = appraisalGoodsExportId;
    }

    public Long getAppraisalGoodsExportId() 
    {
        return appraisalGoodsExportId;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setAppraisalMode(String appraisalMode) 
    {
        this.appraisalMode = appraisalMode;
    }

    public String getAppraisalMode() 
    {
        return appraisalMode;
    }
    public void setAppraisalBatch(String appraisalBatch) 
    {
        this.appraisalBatch = appraisalBatch;
    }

    public String getAppraisalBatch() 
    {
        return appraisalBatch;
    }
    public void setAppraisalWeight(String appraisalWeight) 
    {
        this.appraisalWeight = appraisalWeight;
    }

    public String getAppraisalWeight() 
    {
        return appraisalWeight;
    }
    public void setTotalValue(String totalValue) 
    {
        this.totalValue = totalValue;
    }

    public String getTotalValue() 
    {
        return totalValue;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appraisalGoodsExportId", getAppraisalGoodsExportId())
            .append("goodsCategory", getGoodsCategory())
            .append("appraisalMode", getAppraisalMode())
            .append("appraisalBatch", getAppraisalBatch())
            .append("appraisalWeight", getAppraisalWeight())
            .append("totalValue", getTotalValue())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
