package com.qianjing.project.appraisal.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口货物固体废物属性鉴别情况汇总月报对象 appraisal_month
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public class AppraisalMonth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long appraisalMonthId;

    /** 委托部门 */
    @Excel(name = "委托部门")
    private String entrustDepartment;

    /** 实验室名称 */
    @Excel(name = "实验室名称")
    private String laboratoryName;

    /** 报关单号 */
    @Excel(name = "报关单号")
    private String entryId;

    /** 商品序号 */
    @Excel(name = "商品序号")
    private String goodsSerialNumber;

    /** HS编码 */
    @Excel(name = "HS编码")
    private String hsCode;

    /** 申报商品名称 */
    @Excel(name = "申报商品名称")
    private String declareGName;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String goodsCategory;

    /** 发货人 */
    @Excel(name = "发货人")
    private String consignor;

    /** 原产国、地区 */
    @Excel(name = "原产国、地区")
    private String originCountry;

    /** 收货人 */
    @Excel(name = "收货人")
    private String consignee;

    /** 集装箱数量 */
    @Excel(name = "集装箱数量")
    private String containerNumber;

    /** 重量(吨) */
    @Excel(name = "重量(吨)")
    private String weight;

    /** 货值(美元) */
    @Excel(name = "货值(美元)")
    private String usdPrice;

    /** 委托日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "委托日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date entrustDate;

    /** 鉴别方式(来样/现场抽样/现场检) */
    @Excel(name = "鉴别方式(来样/现场抽样/现场检)")
    private String appraisalMode;

    /** 鉴别结论 */
    @Excel(name = "鉴别结论")
    private String appraisalConclusion;

    /** 标准或依据 */
    @Excel(name = "标准或依据")
    private String standardOrBasis;

    /** 固废详情描述 */
    @Excel(name = "固废详情描述")
    private String solidWasteDescription;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setAppraisalMonthId(Long appraisalMonthId) 
    {
        this.appraisalMonthId = appraisalMonthId;
    }

    public Long getAppraisalMonthId() 
    {
        return appraisalMonthId;
    }
    public void setEntrustDepartment(String entrustDepartment) 
    {
        this.entrustDepartment = entrustDepartment;
    }

    public String getEntrustDepartment() 
    {
        return entrustDepartment;
    }
    public void setLaboratoryName(String laboratoryName) 
    {
        this.laboratoryName = laboratoryName;
    }

    public String getLaboratoryName() 
    {
        return laboratoryName;
    }
    public void setEntryId(String entryId) 
    {
        this.entryId = entryId;
    }

    public String getEntryId() 
    {
        return entryId;
    }
    public void setGoodsSerialNumber(String goodsSerialNumber) 
    {
        this.goodsSerialNumber = goodsSerialNumber;
    }

    public String getGoodsSerialNumber() 
    {
        return goodsSerialNumber;
    }
    public void setHsCode(String hsCode) 
    {
        this.hsCode = hsCode;
    }

    public String getHsCode() 
    {
        return hsCode;
    }
    public void setDeclareGName(String declareGName) 
    {
        this.declareGName = declareGName;
    }

    public String getDeclareGName() 
    {
        return declareGName;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setConsignor(String consignor) 
    {
        this.consignor = consignor;
    }

    public String getConsignor() 
    {
        return consignor;
    }
    public void setOriginCountry(String originCountry) 
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry() 
    {
        return originCountry;
    }
    public void setConsignee(String consignee) 
    {
        this.consignee = consignee;
    }

    public String getConsignee() 
    {
        return consignee;
    }
    public void setContainerNumber(String containerNumber) 
    {
        this.containerNumber = containerNumber;
    }

    public String getContainerNumber() 
    {
        return containerNumber;
    }
    public void setWeight(String weight) 
    {
        this.weight = weight;
    }

    public String getWeight() 
    {
        return weight;
    }
    public void setUsdPrice(String usdPrice) 
    {
        this.usdPrice = usdPrice;
    }

    public String getUsdPrice() 
    {
        return usdPrice;
    }
    public void setEntrustDate(Date entrustDate) 
    {
        this.entrustDate = entrustDate;
    }

    public Date getEntrustDate() 
    {
        return entrustDate;
    }
    public void setAppraisalMode(String appraisalMode) 
    {
        this.appraisalMode = appraisalMode;
    }

    public String getAppraisalMode() 
    {
        return appraisalMode;
    }
    public void setAppraisalConclusion(String appraisalConclusion) 
    {
        this.appraisalConclusion = appraisalConclusion;
    }

    public String getAppraisalConclusion() 
    {
        return appraisalConclusion;
    }
    public void setStandardOrBasis(String standardOrBasis) 
    {
        this.standardOrBasis = standardOrBasis;
    }

    public String getStandardOrBasis() 
    {
        return standardOrBasis;
    }
    public void setSolidWasteDescription(String solidWasteDescription) 
    {
        this.solidWasteDescription = solidWasteDescription;
    }

    public String getSolidWasteDescription() 
    {
        return solidWasteDescription;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appraisalMonthId", getAppraisalMonthId())
            .append("entrustDepartment", getEntrustDepartment())
            .append("laboratoryName", getLaboratoryName())
            .append("entryId", getEntryId())
            .append("goodsSerialNumber", getGoodsSerialNumber())
            .append("hsCode", getHsCode())
            .append("declareGName", getDeclareGName())
            .append("goodsCategory", getGoodsCategory())
            .append("consignor", getConsignor())
            .append("originCountry", getOriginCountry())
            .append("consignee", getConsignee())
            .append("containerNumber", getContainerNumber())
            .append("weight", getWeight())
            .append("usdPrice", getUsdPrice())
            .append("entrustDate", getEntrustDate())
            .append("appraisalMode", getAppraisalMode())
            .append("appraisalConclusion", getAppraisalConclusion())
            .append("standardOrBasis", getStandardOrBasis())
            .append("solidWasteDescription", getSolidWasteDescription())
            .append("remarks", getRemarks())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
