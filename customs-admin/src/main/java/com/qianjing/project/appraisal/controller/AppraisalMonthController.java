package com.qianjing.project.appraisal.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.appraisal.domain.AppraisalMonth;
import com.qianjing.project.appraisal.service.IAppraisalMonthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口货物固体废物属性鉴别情况汇总月报Controller
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@RestController
@RequestMapping("/appraisal/month")
public class AppraisalMonthController extends BaseController
{
    @Autowired
    private IAppraisalMonthService appraisalMonthService;

    /**
     * 查询进口货物固体废物属性鉴别情况汇总月报列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:month:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppraisalMonth appraisalMonth)
    {
        startPage();
        List<AppraisalMonth> list = appraisalMonthService.selectAppraisalMonthList(appraisalMonth);
        return getDataTable(list);
    }

    /**
     * 导出进口货物固体废物属性鉴别情况汇总月报列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:month:export')")
    @Log(title = "进口货物固体废物属性鉴别情况汇总月报", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AppraisalMonth appraisalMonth)
    {
        List<AppraisalMonth> list = appraisalMonthService.selectAppraisalMonthList(appraisalMonth);
        ExcelUtil<AppraisalMonth> util = new ExcelUtil<AppraisalMonth>(AppraisalMonth.class);
        return util.exportExcel(list, "进口货物固体废物属性鉴别情况汇总月报数据");
    }

    /**
     * 导入进口货物固体废物属性鉴别情况汇总月报列表
     */
    @Log(title = "进口货物固体废物属性鉴别情况汇总月报", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('appraisal:month:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<AppraisalMonth> util = new ExcelUtil<AppraisalMonth>(AppraisalMonth.class);
        List<AppraisalMonth> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = appraisalMonthService.importAppraisalMonth(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口货物固体废物属性鉴别情况汇总月报导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<AppraisalMonth> util = new ExcelUtil<AppraisalMonth>(AppraisalMonth.class);
        util.importTemplateExcel(response, "进口货物固体废物属性鉴别情况汇总月报数据");
    }

    /**
     * 获取进口货物固体废物属性鉴别情况汇总月报详细信息
     */
    @PreAuthorize("@ss.hasPermi('appraisal:month:query')")
    @GetMapping(value = "/{appraisalMonthId}")
    public AjaxResult getInfo(@PathVariable("appraisalMonthId") Long appraisalMonthId)
    {
        return AjaxResult.success(appraisalMonthService.selectAppraisalMonthByAppraisalMonthId(appraisalMonthId));
    }

    /**
     * 新增进口货物固体废物属性鉴别情况汇总月报
     */
    @PreAuthorize("@ss.hasPermi('appraisal:month:add')")
    @Log(title = "进口货物固体废物属性鉴别情况汇总月报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppraisalMonth appraisalMonth)
    {
        return toAjax(appraisalMonthService.insertAppraisalMonth(appraisalMonth));
    }

    /**
     * 修改进口货物固体废物属性鉴别情况汇总月报
     */
    @PreAuthorize("@ss.hasPermi('appraisal:month:edit')")
    @Log(title = "进口货物固体废物属性鉴别情况汇总月报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppraisalMonth appraisalMonth)
    {
        return toAjax(appraisalMonthService.updateAppraisalMonth(appraisalMonth));
    }

    /**
     * 删除进口货物固体废物属性鉴别情况汇总月报
     */
    @PreAuthorize("@ss.hasPermi('appraisal:month:remove')")
    @Log(title = "进口货物固体废物属性鉴别情况汇总月报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appraisalMonthIds}")
    public AjaxResult remove(@PathVariable Long[] appraisalMonthIds)
    {
        return toAjax(appraisalMonthService.deleteAppraisalMonthByAppraisalMonthIds(appraisalMonthIds));
    }
}
