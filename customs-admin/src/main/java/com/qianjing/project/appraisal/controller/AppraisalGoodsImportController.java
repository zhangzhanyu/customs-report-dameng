package com.qianjing.project.appraisal.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.appraisal.domain.AppraisalGoodsImport;
import com.qianjing.project.appraisal.service.IAppraisalGoodsImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 主要进口商品重量鉴定情况统计Controller
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@RestController
@RequestMapping("/appraisal/goodsImport")
public class AppraisalGoodsImportController extends BaseController
{
    @Autowired
    private IAppraisalGoodsImportService appraisalGoodsImportService;

    /**
     * 查询主要进口商品重量鉴定情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsImport:list')")
    @GetMapping("/list")
    public TableDataInfo list(AppraisalGoodsImport appraisalGoodsImport)
    {
        startPage();
        List<AppraisalGoodsImport> list = appraisalGoodsImportService.selectAppraisalGoodsImportList(appraisalGoodsImport);
        return getDataTable(list);
    }

    /**
     * 导出主要进口商品重量鉴定情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsImport:export')")
    @Log(title = "主要进口商品重量鉴定情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AppraisalGoodsImport appraisalGoodsImport)
    {
        List<AppraisalGoodsImport> list = appraisalGoodsImportService.selectAppraisalGoodsImportList(appraisalGoodsImport);
        ExcelUtil<AppraisalGoodsImport> util = new ExcelUtil<AppraisalGoodsImport>(AppraisalGoodsImport.class);
        return util.exportExcel(list, "主要进口商品重量鉴定情况统计数据");
    }

    /**
     * 导入主要进口商品重量鉴定情况统计列表
     */
    @Log(title = "主要进口商品重量鉴定情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('appraisal:goodsImport:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<AppraisalGoodsImport> util = new ExcelUtil<AppraisalGoodsImport>(AppraisalGoodsImport.class);
        List<AppraisalGoodsImport> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = appraisalGoodsImportService.importAppraisalGoodsImport(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载主要进口商品重量鉴定情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<AppraisalGoodsImport> util = new ExcelUtil<AppraisalGoodsImport>(AppraisalGoodsImport.class);
        util.importTemplateExcel(response, "主要进口商品重量鉴定情况统计数据");
    }

    /**
     * 获取主要进口商品重量鉴定情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsImport:query')")
    @GetMapping(value = "/{appraisalGoodsImportId}")
    public AjaxResult getInfo(@PathVariable("appraisalGoodsImportId") Long appraisalGoodsImportId)
    {
        return AjaxResult.success(appraisalGoodsImportService.selectAppraisalGoodsImportByAppraisalGoodsImportId(appraisalGoodsImportId));
    }

    /**
     * 新增主要进口商品重量鉴定情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsImport:add')")
    @Log(title = "主要进口商品重量鉴定情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppraisalGoodsImport appraisalGoodsImport)
    {
        return toAjax(appraisalGoodsImportService.insertAppraisalGoodsImport(appraisalGoodsImport));
    }

    /**
     * 修改主要进口商品重量鉴定情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsImport:edit')")
    @Log(title = "主要进口商品重量鉴定情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppraisalGoodsImport appraisalGoodsImport)
    {
        return toAjax(appraisalGoodsImportService.updateAppraisalGoodsImport(appraisalGoodsImport));
    }

    /**
     * 删除主要进口商品重量鉴定情况统计
     */
    @PreAuthorize("@ss.hasPermi('appraisal:goodsImport:remove')")
    @Log(title = "主要进口商品重量鉴定情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appraisalGoodsImportIds}")
    public AjaxResult remove(@PathVariable Long[] appraisalGoodsImportIds)
    {
        return toAjax(appraisalGoodsImportService.deleteAppraisalGoodsImportByAppraisalGoodsImportIds(appraisalGoodsImportIds));
    }
}
