package com.qianjing.project.appraisal.service;

import java.util.List;
import com.qianjing.project.appraisal.domain.AppraisalGoodsExport;

/**
 * 主要出口商品重量鉴定情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public interface IAppraisalGoodsExportService 
{
    /**
     * 查询主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExportId 主要出口商品重量鉴定情况统计主键
     * @return 主要出口商品重量鉴定情况统计
     */
    public AppraisalGoodsExport selectAppraisalGoodsExportByAppraisalGoodsExportId(Long appraisalGoodsExportId);

    /**
     * 查询主要出口商品重量鉴定情况统计列表
     * 
     * @param appraisalGoodsExport 主要出口商品重量鉴定情况统计
     * @return 主要出口商品重量鉴定情况统计集合
     */
    public List<AppraisalGoodsExport> selectAppraisalGoodsExportList(AppraisalGoodsExport appraisalGoodsExport);

    /**
     * 新增主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExport 主要出口商品重量鉴定情况统计
     * @return 结果
     */
    public int insertAppraisalGoodsExport(AppraisalGoodsExport appraisalGoodsExport);

    /**
     * 修改主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExport 主要出口商品重量鉴定情况统计
     * @return 结果
     */
    public int updateAppraisalGoodsExport(AppraisalGoodsExport appraisalGoodsExport);

    /**
     * 批量删除主要出口商品重量鉴定情况统计
     * 
     * @param appraisalGoodsExportIds 需要删除的主要出口商品重量鉴定情况统计主键集合
     * @return 结果
     */
    public int deleteAppraisalGoodsExportByAppraisalGoodsExportIds(Long[] appraisalGoodsExportIds);

    /**
     * 删除主要出口商品重量鉴定情况统计信息
     * 
     * @param appraisalGoodsExportId 主要出口商品重量鉴定情况统计主键
     * @return 结果
     */
    public int deleteAppraisalGoodsExportByAppraisalGoodsExportId(Long appraisalGoodsExportId);

    /**
     * 导入主要出口商品重量鉴定情况统计信息
     *
     * @param appraisalGoodsExportList 主要出口商品重量鉴定情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importAppraisalGoodsExport(List<AppraisalGoodsExport> appraisalGoodsExportList, Boolean isUpdateSupport, String operName);
}
