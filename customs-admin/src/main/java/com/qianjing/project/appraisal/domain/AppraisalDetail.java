package com.qianjing.project.appraisal.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 上海地区检验鉴定机构清单对象 appraisal_detail
 * 
 * @author qianjing
 * @date 2022-07-21
 */
public class AppraisalDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long hgAppraisalId;

    /** 档案序号 */
    @Excel(name = "档案序号")
    private String daxh;

    /** 关区 */
    @Excel(name = "关区")
    private String gq;

    /** 证书许可号 */
    @Excel(name = "证书许可号")
    private String zsxkz;

    /** 许可日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "许可日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date xkrq;

    /** 机构名称 */
    @Excel(name = "机构名称")
    private String jgmc;

    /** 品牌 */
    @Excel(name = "品牌")
    private String jgdz;

    /** 是否有CMA */
    @Excel(name = "是否有CMA")
    private String cmaFlag;

    /** 联系人 */
    @Excel(name = "联系人")
    private String lxr;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    /** 传真 */
    @Excel(name = "传真")
    private String cz;

    /** 邮编 */
    @Excel(name = "邮编")
    private String yb;

    /** EMAIL */
    @Excel(name = "EMAIL")
    private String email;

    /** 办公地点 */
    @Excel(name = "办公地点")
    private String bgdd;

    /** 存续情况 */
    @Excel(name = "存续情况")
    private String cxqk;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setHgAppraisalId(Long hgAppraisalId) 
    {
        this.hgAppraisalId = hgAppraisalId;
    }

    public Long getHgAppraisalId() 
    {
        return hgAppraisalId;
    }
    public void setDaxh(String daxh) 
    {
        this.daxh = daxh;
    }

    public String getDaxh() 
    {
        return daxh;
    }
    public void setGq(String gq) 
    {
        this.gq = gq;
    }

    public String getGq() 
    {
        return gq;
    }
    public void setZsxkz(String zsxkz) 
    {
        this.zsxkz = zsxkz;
    }

    public String getZsxkz() 
    {
        return zsxkz;
    }
    public void setXkrq(Date xkrq) 
    {
        this.xkrq = xkrq;
    }

    public Date getXkrq() 
    {
        return xkrq;
    }
    public void setJgmc(String jgmc) 
    {
        this.jgmc = jgmc;
    }

    public String getJgmc() 
    {
        return jgmc;
    }
    public void setJgdz(String jgdz) 
    {
        this.jgdz = jgdz;
    }

    public String getJgdz() 
    {
        return jgdz;
    }
    public void setCmaFlag(String cmaFlag) 
    {
        this.cmaFlag = cmaFlag;
    }

    public String getCmaFlag() 
    {
        return cmaFlag;
    }
    public void setLxr(String lxr) 
    {
        this.lxr = lxr;
    }

    public String getLxr() 
    {
        return lxr;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }
    public void setCz(String cz) 
    {
        this.cz = cz;
    }

    public String getCz() 
    {
        return cz;
    }
    public void setYb(String yb) 
    {
        this.yb = yb;
    }

    public String getYb() 
    {
        return yb;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setBgdd(String bgdd) 
    {
        this.bgdd = bgdd;
    }

    public String getBgdd() 
    {
        return bgdd;
    }
    public void setCxqk(String cxqk) 
    {
        this.cxqk = cxqk;
    }

    public String getCxqk() 
    {
        return cxqk;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("hgAppraisalId", getHgAppraisalId())
            .append("daxh", getDaxh())
            .append("gq", getGq())
            .append("zsxkz", getZsxkz())
            .append("xkrq", getXkrq())
            .append("jgmc", getJgmc())
            .append("jgdz", getJgdz())
            .append("cmaFlag", getCmaFlag())
            .append("lxr", getLxr())
            .append("lxdh", getLxdh())
            .append("cz", getCz())
            .append("yb", getYb())
            .append("email", getEmail())
            .append("bgdd", getBgdd())
            .append("cxqk", getCxqk())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
