package com.qianjing.project.appraisal.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.appraisal.mapper.AppraisalMonthMapper;
import com.qianjing.project.appraisal.domain.AppraisalMonth;
import com.qianjing.project.appraisal.service.IAppraisalMonthService;

/**
 * 进口货物固体废物属性鉴别情况汇总月报Service业务层处理
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@Service
public class AppraisalMonthServiceImpl implements IAppraisalMonthService 
{
    private static final Logger log = LoggerFactory.getLogger(AppraisalMonthServiceImpl.class);

    @Autowired
    private AppraisalMonthMapper appraisalMonthMapper;

    /**
     * 查询进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonthId 进口货物固体废物属性鉴别情况汇总月报主键
     * @return 进口货物固体废物属性鉴别情况汇总月报
     */
    @Override
    public AppraisalMonth selectAppraisalMonthByAppraisalMonthId(Long appraisalMonthId)
    {
        return appraisalMonthMapper.selectAppraisalMonthByAppraisalMonthId(appraisalMonthId);
    }

    /**
     * 查询进口货物固体废物属性鉴别情况汇总月报列表
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 进口货物固体废物属性鉴别情况汇总月报
     */
    @Override
    public List<AppraisalMonth> selectAppraisalMonthList(AppraisalMonth appraisalMonth)
    {
        return appraisalMonthMapper.selectAppraisalMonthList(appraisalMonth);
    }

    /**
     * 新增进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 结果
     */
    @Override
    public int insertAppraisalMonth(AppraisalMonth appraisalMonth)
    {
        appraisalMonth.setCreateTime(DateUtils.getNowDate());
        return appraisalMonthMapper.insertAppraisalMonth(appraisalMonth);
    }

    /**
     * 修改进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonth 进口货物固体废物属性鉴别情况汇总月报
     * @return 结果
     */
    @Override
    public int updateAppraisalMonth(AppraisalMonth appraisalMonth)
    {
        appraisalMonth.setUpdateTime(DateUtils.getNowDate());
        return appraisalMonthMapper.updateAppraisalMonth(appraisalMonth);
    }

    /**
     * 批量删除进口货物固体废物属性鉴别情况汇总月报
     * 
     * @param appraisalMonthIds 需要删除的进口货物固体废物属性鉴别情况汇总月报主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalMonthByAppraisalMonthIds(Long[] appraisalMonthIds)
    {
        return appraisalMonthMapper.deleteAppraisalMonthByAppraisalMonthIds(appraisalMonthIds);
    }

    /**
     * 删除进口货物固体废物属性鉴别情况汇总月报信息
     * 
     * @param appraisalMonthId 进口货物固体废物属性鉴别情况汇总月报主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalMonthByAppraisalMonthId(Long appraisalMonthId)
    {
        return appraisalMonthMapper.deleteAppraisalMonthByAppraisalMonthId(appraisalMonthId);
    }

    /**
     * 导入进口货物固体废物属性鉴别情况汇总月报数据
     *
     * @param appraisalMonthList appraisalMonthList 进口货物固体废物属性鉴别情况汇总月报信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importAppraisalMonth(List<AppraisalMonth> appraisalMonthList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(appraisalMonthList) || appraisalMonthList.size() == 0) {
            throw new ServiceException("导入进口货物固体废物属性鉴别情况汇总月报数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (AppraisalMonth appraisalMonth : appraisalMonthList){
            try {
                // 验证是否存在
                if (true) {
                    appraisalMonth.setCreateBy(operName);
                    this.insertAppraisalMonth(appraisalMonth);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    appraisalMonth.setUpdateBy(operName);
                    this.updateAppraisalMonth(appraisalMonth);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
