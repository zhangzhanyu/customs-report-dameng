package com.qianjing.project.appraisal.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.appraisal.mapper.AppraisalDetailMapper;
import com.qianjing.project.appraisal.domain.AppraisalDetail;
import com.qianjing.project.appraisal.service.IAppraisalDetailService;

/**
 * 上海地区检验鉴定机构清单Service业务层处理
 * 
 * @author qianjing
 * @date 2022-07-21
 */
@Service
public class AppraisalDetailServiceImpl implements IAppraisalDetailService 
{
    private static final Logger log = LoggerFactory.getLogger(AppraisalDetailServiceImpl.class);

    @Autowired
    private AppraisalDetailMapper appraisalDetailMapper;

    /**
     * 查询上海地区检验鉴定机构清单
     * 
     * @param hgAppraisalId 上海地区检验鉴定机构清单主键
     * @return 上海地区检验鉴定机构清单
     */
    @Override
    public AppraisalDetail selectAppraisalDetailByHgAppraisalId(Long hgAppraisalId)
    {
        return appraisalDetailMapper.selectAppraisalDetailByHgAppraisalId(hgAppraisalId);
    }

    /**
     * 查询上海地区检验鉴定机构清单列表
     * 
     * @param appraisalDetail 上海地区检验鉴定机构清单
     * @return 上海地区检验鉴定机构清单
     */
    @Override
    public List<AppraisalDetail> selectAppraisalDetailList(AppraisalDetail appraisalDetail)
    {
        return appraisalDetailMapper.selectAppraisalDetailList(appraisalDetail);
    }

    /**
     * 新增上海地区检验鉴定机构清单
     * 
     * @param appraisalDetail 上海地区检验鉴定机构清单
     * @return 结果
     */
    @Override
    public int insertAppraisalDetail(AppraisalDetail appraisalDetail)
    {
        appraisalDetail.setCreateTime(DateUtils.getNowDate());
        return appraisalDetailMapper.insertAppraisalDetail(appraisalDetail);
    }

    /**
     * 修改上海地区检验鉴定机构清单
     * 
     * @param appraisalDetail 上海地区检验鉴定机构清单
     * @return 结果
     */
    @Override
    public int updateAppraisalDetail(AppraisalDetail appraisalDetail)
    {
        appraisalDetail.setUpdateTime(DateUtils.getNowDate());
        return appraisalDetailMapper.updateAppraisalDetail(appraisalDetail);
    }

    /**
     * 批量删除上海地区检验鉴定机构清单
     * 
     * @param hgAppraisalIds 需要删除的上海地区检验鉴定机构清单主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalDetailByHgAppraisalIds(Long[] hgAppraisalIds)
    {
        return appraisalDetailMapper.deleteAppraisalDetailByHgAppraisalIds(hgAppraisalIds);
    }

    /**
     * 删除上海地区检验鉴定机构清单信息
     * 
     * @param hgAppraisalId 上海地区检验鉴定机构清单主键
     * @return 结果
     */
    @Override
    public int deleteAppraisalDetailByHgAppraisalId(Long hgAppraisalId)
    {
        return appraisalDetailMapper.deleteAppraisalDetailByHgAppraisalId(hgAppraisalId);
    }

    /**
     * 导入上海地区检验鉴定机构清单数据
     *
     * @param appraisalDetailList appraisalDetailList 上海地区检验鉴定机构清单信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importAppraisalDetail(List<AppraisalDetail> appraisalDetailList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(appraisalDetailList) || appraisalDetailList.size() == 0) {
            throw new ServiceException("导入上海地区检验鉴定机构清单数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (AppraisalDetail appraisalDetail : appraisalDetailList){
            try {
                // 验证是否存在
                if (true) {
                    appraisalDetail.setCreateBy(operName);
                    this.insertAppraisalDetail(appraisalDetail);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    appraisalDetail.setUpdateBy(operName);
                    this.updateAppraisalDetail(appraisalDetail);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
