package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.SusionHygienic;
import com.qianjing.project.oneDept.service.ISusionHygienicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口一次性使用卫生用品检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/susionHygienic")
public class SusionHygienicController extends BaseController
{
    @Autowired
    private ISusionHygienicService susionHygienicService;

    /**
     * 查询进口一次性使用卫生用品检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionHygienic:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionHygienic susionHygienic)
    {
        startPage();
        List<SusionHygienic> list = susionHygienicService.selectSusionHygienicList(susionHygienic);
        return getDataTable(list);
    }

    /**
     * 导出进口一次性使用卫生用品检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionHygienic:export')")
    @Log(title = "进口一次性使用卫生用品检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionHygienic susionHygienic)
    {
        List<SusionHygienic> list = susionHygienicService.selectSusionHygienicList(susionHygienic);
        ExcelUtil<SusionHygienic> util = new ExcelUtil<SusionHygienic>(SusionHygienic.class);
        return util.exportExcel(list, "进口一次性使用卫生用品检验监管情况统计数据");
    }

    /**
     * 导入进口一次性使用卫生用品检验监管情况统计列表
     */
    @Log(title = "进口一次性使用卫生用品检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:susionHygienic:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionHygienic> util = new ExcelUtil<SusionHygienic>(SusionHygienic.class);
        List<SusionHygienic> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionHygienicService.importSusionHygienic(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口一次性使用卫生用品检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionHygienic> util = new ExcelUtil<SusionHygienic>(SusionHygienic.class);
        util.importTemplateExcel(response, "进口一次性使用卫生用品检验监管情况统计数据");
    }

    /**
     * 获取进口一次性使用卫生用品检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionHygienic:query')")
    @GetMapping(value = "/{susionHygienicId}")
    public AjaxResult getInfo(@PathVariable("susionHygienicId") Long susionHygienicId)
    {
        return AjaxResult.success(susionHygienicService.selectSusionHygienicBySusionHygienicId(susionHygienicId));
    }

    /**
     * 新增进口一次性使用卫生用品检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionHygienic:add')")
    @Log(title = "进口一次性使用卫生用品检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionHygienic susionHygienic)
    {
        return toAjax(susionHygienicService.insertSusionHygienic(susionHygienic));
    }

    /**
     * 修改进口一次性使用卫生用品检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionHygienic:edit')")
    @Log(title = "进口一次性使用卫生用品检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionHygienic susionHygienic)
    {
        return toAjax(susionHygienicService.updateSusionHygienic(susionHygienic));
    }

    /**
     * 删除进口一次性使用卫生用品检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionHygienic:remove')")
    @Log(title = "进口一次性使用卫生用品检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionHygienicIds}")
    public AjaxResult remove(@PathVariable Long[] susionHygienicIds)
    {
        return toAjax(susionHygienicService.deleteSusionHygienicBySusionHygienicIds(susionHygienicIds));
    }
}
