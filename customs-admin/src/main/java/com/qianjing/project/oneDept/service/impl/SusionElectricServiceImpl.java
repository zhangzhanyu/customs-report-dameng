package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.SusionElectricMapper;
import com.qianjing.project.oneDept.domain.SusionElectric;
import com.qianjing.project.oneDept.service.ISusionElectricService;

/**
 * 进口旧机电检验监管情况Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class SusionElectricServiceImpl implements ISusionElectricService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionElectricServiceImpl.class);

    @Autowired
    private SusionElectricMapper susionElectricMapper;

    /**
     * 查询进口旧机电检验监管情况
     * 
     * @param susionElectricId 进口旧机电检验监管情况主键
     * @return 进口旧机电检验监管情况
     */
    @Override
    public SusionElectric selectSusionElectricBySusionElectricId(Long susionElectricId)
    {
        return susionElectricMapper.selectSusionElectricBySusionElectricId(susionElectricId);
    }

    /**
     * 查询进口旧机电检验监管情况列表
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 进口旧机电检验监管情况
     */
    @Override
    public List<SusionElectric> selectSusionElectricList(SusionElectric susionElectric)
    {
        return susionElectricMapper.selectSusionElectricList(susionElectric);
    }

    /**
     * 新增进口旧机电检验监管情况
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 结果
     */
    @Override
    public int insertSusionElectric(SusionElectric susionElectric)
    {
        return susionElectricMapper.insertSusionElectric(susionElectric);
    }

    /**
     * 修改进口旧机电检验监管情况
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 结果
     */
    @Override
    public int updateSusionElectric(SusionElectric susionElectric)
    {
        return susionElectricMapper.updateSusionElectric(susionElectric);
    }

    /**
     * 批量删除进口旧机电检验监管情况
     * 
     * @param susionElectricIds 需要删除的进口旧机电检验监管情况主键
     * @return 结果
     */
    @Override
    public int deleteSusionElectricBySusionElectricIds(Long[] susionElectricIds)
    {
        return susionElectricMapper.deleteSusionElectricBySusionElectricIds(susionElectricIds);
    }

    /**
     * 删除进口旧机电检验监管情况信息
     * 
     * @param susionElectricId 进口旧机电检验监管情况主键
     * @return 结果
     */
    @Override
    public int deleteSusionElectricBySusionElectricId(Long susionElectricId)
    {
        return susionElectricMapper.deleteSusionElectricBySusionElectricId(susionElectricId);
    }

    /**
     * 导入进口旧机电检验监管情况数据
     *
     * @param susionElectricList susionElectricList 进口旧机电检验监管情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionElectric(List<SusionElectric> susionElectricList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionElectricList) || susionElectricList.size() == 0) {
            throw new ServiceException("导入进口旧机电检验监管情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionElectric susionElectric : susionElectricList){
            try {
                // 验证是否存在
                if (true) {
                    susionElectric.setCreateBy(operName);
                    this.insertSusionElectric(susionElectric);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionElectric.setUpdateBy(operName);
                    this.updateSusionElectric(susionElectric);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
