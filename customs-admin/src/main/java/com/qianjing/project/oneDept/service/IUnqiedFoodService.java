package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedFood;

/**
 * 进口食品接触产品不合格统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface IUnqiedFoodService 
{
    /**
     * 查询进口食品接触产品不合格统计
     * 
     * @param unqiedFoodId 进口食品接触产品不合格统计主键
     * @return 进口食品接触产品不合格统计
     */
    public UnqiedFood selectUnqiedFoodByUnqiedFoodId(Long unqiedFoodId);

    /**
     * 查询进口食品接触产品不合格统计列表
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 进口食品接触产品不合格统计集合
     */
    public List<UnqiedFood> selectUnqiedFoodList(UnqiedFood unqiedFood);

    /**
     * 新增进口食品接触产品不合格统计
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 结果
     */
    public int insertUnqiedFood(UnqiedFood unqiedFood);

    /**
     * 修改进口食品接触产品不合格统计
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 结果
     */
    public int updateUnqiedFood(UnqiedFood unqiedFood);

    /**
     * 批量删除进口食品接触产品不合格统计
     * 
     * @param unqiedFoodIds 需要删除的进口食品接触产品不合格统计主键集合
     * @return 结果
     */
    public int deleteUnqiedFoodByUnqiedFoodIds(Long[] unqiedFoodIds);

    /**
     * 删除进口食品接触产品不合格统计信息
     * 
     * @param unqiedFoodId 进口食品接触产品不合格统计主键
     * @return 结果
     */
    public int deleteUnqiedFoodByUnqiedFoodId(Long unqiedFoodId);

    /**
     * 导入进口食品接触产品不合格统计信息
     *
     * @param unqiedFoodList 进口食品接触产品不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUnqiedFood(List<UnqiedFood> unqiedFoodList, Boolean isUpdateSupport, String operName);
}
