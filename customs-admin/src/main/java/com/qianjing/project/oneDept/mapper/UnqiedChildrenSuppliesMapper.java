package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedChildrenSupplies;

/**
 * 进口儿童用品不合格统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface UnqiedChildrenSuppliesMapper 
{
    /**
     * 查询进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSuppliesId 进口儿童用品不合格统计主键
     * @return 进口儿童用品不合格统计
     */
    public UnqiedChildrenSupplies selectUnqiedChildrenSuppliesByUnqiedChildrenSuppliesId(Long unqiedChildrenSuppliesId);

    /**
     * 查询进口儿童用品不合格统计列表
     * 
     * @param unqiedChildrenSupplies 进口儿童用品不合格统计
     * @return 进口儿童用品不合格统计集合
     */
    public List<UnqiedChildrenSupplies> selectUnqiedChildrenSuppliesList(UnqiedChildrenSupplies unqiedChildrenSupplies);

    /**
     * 新增进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSupplies 进口儿童用品不合格统计
     * @return 结果
     */
    public int insertUnqiedChildrenSupplies(UnqiedChildrenSupplies unqiedChildrenSupplies);

    /**
     * 修改进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSupplies 进口儿童用品不合格统计
     * @return 结果
     */
    public int updateUnqiedChildrenSupplies(UnqiedChildrenSupplies unqiedChildrenSupplies);

    /**
     * 删除进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSuppliesId 进口儿童用品不合格统计主键
     * @return 结果
     */
    public int deleteUnqiedChildrenSuppliesByUnqiedChildrenSuppliesId(Long unqiedChildrenSuppliesId);

    /**
     * 批量删除进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSuppliesIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUnqiedChildrenSuppliesByUnqiedChildrenSuppliesIds(Long[] unqiedChildrenSuppliesIds);
}
