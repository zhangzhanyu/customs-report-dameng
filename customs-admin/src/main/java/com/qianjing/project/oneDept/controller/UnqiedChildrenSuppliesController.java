package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.UnqiedChildrenSupplies;
import com.qianjing.project.oneDept.service.IUnqiedChildrenSuppliesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口儿童用品不合格统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/unqiedSupplies")
public class UnqiedChildrenSuppliesController extends BaseController
{
    @Autowired
    private IUnqiedChildrenSuppliesService unqiedChildrenSuppliesService;

    /**
     * 查询进口儿童用品不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedSupplies:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnqiedChildrenSupplies unqiedChildrenSupplies)
    {
        startPage();
        List<UnqiedChildrenSupplies> list = unqiedChildrenSuppliesService.selectUnqiedChildrenSuppliesList(unqiedChildrenSupplies);
        return getDataTable(list);
    }

    /**
     * 导出进口儿童用品不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedSupplies:export')")
    @Log(title = "进口儿童用品不合格统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UnqiedChildrenSupplies unqiedChildrenSupplies)
    {
        List<UnqiedChildrenSupplies> list = unqiedChildrenSuppliesService.selectUnqiedChildrenSuppliesList(unqiedChildrenSupplies);
        ExcelUtil<UnqiedChildrenSupplies> util = new ExcelUtil<UnqiedChildrenSupplies>(UnqiedChildrenSupplies.class);
        return util.exportExcel(list, "进口儿童用品不合格统计数据");
    }

    /**
     * 导入进口儿童用品不合格统计列表
     */
    @Log(title = "进口儿童用品不合格统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedSupplies:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<UnqiedChildrenSupplies> util = new ExcelUtil<UnqiedChildrenSupplies>(UnqiedChildrenSupplies.class);
        List<UnqiedChildrenSupplies> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = unqiedChildrenSuppliesService.importUnqiedChildrenSupplies(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口儿童用品不合格统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<UnqiedChildrenSupplies> util = new ExcelUtil<UnqiedChildrenSupplies>(UnqiedChildrenSupplies.class);
        util.importTemplateExcel(response, "进口儿童用品不合格统计数据");
    }

    /**
     * 获取进口儿童用品不合格统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedSupplies:query')")
    @GetMapping(value = "/{unqiedChildrenSuppliesId}")
    public AjaxResult getInfo(@PathVariable("unqiedChildrenSuppliesId") Long unqiedChildrenSuppliesId)
    {
        return AjaxResult.success(unqiedChildrenSuppliesService.selectUnqiedChildrenSuppliesByUnqiedChildrenSuppliesId(unqiedChildrenSuppliesId));
    }

    /**
     * 新增进口儿童用品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedSupplies:add')")
    @Log(title = "进口儿童用品不合格统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UnqiedChildrenSupplies unqiedChildrenSupplies)
    {
        return toAjax(unqiedChildrenSuppliesService.insertUnqiedChildrenSupplies(unqiedChildrenSupplies));
    }

    /**
     * 修改进口儿童用品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedSupplies:edit')")
    @Log(title = "进口儿童用品不合格统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UnqiedChildrenSupplies unqiedChildrenSupplies)
    {
        return toAjax(unqiedChildrenSuppliesService.updateUnqiedChildrenSupplies(unqiedChildrenSupplies));
    }

    /**
     * 删除进口儿童用品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedSupplies:remove')")
    @Log(title = "进口儿童用品不合格统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unqiedChildrenSuppliesIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedChildrenSuppliesIds)
    {
        return toAjax(unqiedChildrenSuppliesService.deleteUnqiedChildrenSuppliesByUnqiedChildrenSuppliesIds(unqiedChildrenSuppliesIds));
    }
}
