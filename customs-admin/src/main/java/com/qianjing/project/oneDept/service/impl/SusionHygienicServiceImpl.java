package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.SusionHygienicMapper;
import com.qianjing.project.oneDept.domain.SusionHygienic;
import com.qianjing.project.oneDept.service.ISusionHygienicService;

/**
 * 进口一次性使用卫生用品检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class SusionHygienicServiceImpl implements ISusionHygienicService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionHygienicServiceImpl.class);

    @Autowired
    private SusionHygienicMapper susionHygienicMapper;

    /**
     * 查询进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienicId 进口一次性使用卫生用品检验监管情况统计主键
     * @return 进口一次性使用卫生用品检验监管情况统计
     */
    @Override
    public SusionHygienic selectSusionHygienicBySusionHygienicId(Long susionHygienicId)
    {
        return susionHygienicMapper.selectSusionHygienicBySusionHygienicId(susionHygienicId);
    }

    /**
     * 查询进口一次性使用卫生用品检验监管情况统计列表
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 进口一次性使用卫生用品检验监管情况统计
     */
    @Override
    public List<SusionHygienic> selectSusionHygienicList(SusionHygienic susionHygienic)
    {
        return susionHygienicMapper.selectSusionHygienicList(susionHygienic);
    }

    /**
     * 新增进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionHygienic(SusionHygienic susionHygienic)
    {
        return susionHygienicMapper.insertSusionHygienic(susionHygienic);
    }

    /**
     * 修改进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionHygienic(SusionHygienic susionHygienic)
    {
        return susionHygienicMapper.updateSusionHygienic(susionHygienic);
    }

    /**
     * 批量删除进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienicIds 需要删除的进口一次性使用卫生用品检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionHygienicBySusionHygienicIds(Long[] susionHygienicIds)
    {
        return susionHygienicMapper.deleteSusionHygienicBySusionHygienicIds(susionHygienicIds);
    }

    /**
     * 删除进口一次性使用卫生用品检验监管情况统计信息
     * 
     * @param susionHygienicId 进口一次性使用卫生用品检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionHygienicBySusionHygienicId(Long susionHygienicId)
    {
        return susionHygienicMapper.deleteSusionHygienicBySusionHygienicId(susionHygienicId);
    }

    /**
     * 导入进口一次性使用卫生用品检验监管情况统计数据
     *
     * @param susionHygienicList susionHygienicList 进口一次性使用卫生用品检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionHygienic(List<SusionHygienic> susionHygienicList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionHygienicList) || susionHygienicList.size() == 0) {
            throw new ServiceException("导入进口一次性使用卫生用品检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionHygienic susionHygienic : susionHygienicList){
            try {
                // 验证是否存在
                if (true) {
                    susionHygienic.setCreateBy(operName);
                    this.insertSusionHygienic(susionHygienic);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionHygienic.setUpdateBy(operName);
                    this.updateSusionHygienic(susionHygienic);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
