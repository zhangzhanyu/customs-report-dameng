package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionChildrenSupplies;

/**
 * 进口儿童用品监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface SusionChildrenSuppliesMapper 
{
    /**
     * 查询进口儿童用品监管情况统计
     * 
     * @param susionChildrenSuppliesId 进口儿童用品监管情况统计主键
     * @return 进口儿童用品监管情况统计
     */
    public SusionChildrenSupplies selectSusionChildrenSuppliesBySusionChildrenSuppliesId(Long susionChildrenSuppliesId);

    /**
     * 查询进口儿童用品监管情况统计列表
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 进口儿童用品监管情况统计集合
     */
    public List<SusionChildrenSupplies> selectSusionChildrenSuppliesList(SusionChildrenSupplies susionChildrenSupplies);

    /**
     * 新增进口儿童用品监管情况统计
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 结果
     */
    public int insertSusionChildrenSupplies(SusionChildrenSupplies susionChildrenSupplies);

    /**
     * 修改进口儿童用品监管情况统计
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 结果
     */
    public int updateSusionChildrenSupplies(SusionChildrenSupplies susionChildrenSupplies);

    /**
     * 删除进口儿童用品监管情况统计
     * 
     * @param susionChildrenSuppliesId 进口儿童用品监管情况统计主键
     * @return 结果
     */
    public int deleteSusionChildrenSuppliesBySusionChildrenSuppliesId(Long susionChildrenSuppliesId);

    /**
     * 批量删除进口儿童用品监管情况统计
     * 
     * @param susionChildrenSuppliesIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionChildrenSuppliesBySusionChildrenSuppliesIds(Long[] susionChildrenSuppliesIds);
}
