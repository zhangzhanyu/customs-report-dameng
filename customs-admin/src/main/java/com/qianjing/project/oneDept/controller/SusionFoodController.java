package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.SusionFood;
import com.qianjing.project.oneDept.service.ISusionFoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口食品接触产品检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/susionFood")
public class SusionFoodController extends BaseController
{
    @Autowired
    private ISusionFoodService susionFoodService;

    /**
     * 查询进口食品接触产品检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionFood:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionFood susionFood)
    {
        startPage();
        List<SusionFood> list = susionFoodService.selectSusionFoodList(susionFood);
        return getDataTable(list);
    }

    /**
     * 导出进口食品接触产品检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionFood:export')")
    @Log(title = "进口食品接触产品检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionFood susionFood)
    {
        List<SusionFood> list = susionFoodService.selectSusionFoodList(susionFood);
        ExcelUtil<SusionFood> util = new ExcelUtil<SusionFood>(SusionFood.class);
        return util.exportExcel(list, "进口食品接触产品检验监管情况统计数据");
    }

    /**
     * 导入进口食品接触产品检验监管情况统计列表
     */
    @Log(title = "进口食品接触产品检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:susionFood:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionFood> util = new ExcelUtil<SusionFood>(SusionFood.class);
        List<SusionFood> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionFoodService.importSusionFood(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口食品接触产品检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionFood> util = new ExcelUtil<SusionFood>(SusionFood.class);
        util.importTemplateExcel(response, "进口食品接触产品检验监管情况统计数据");
    }

    /**
     * 获取进口食品接触产品检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionFood:query')")
    @GetMapping(value = "/{susionFoodId}")
    public AjaxResult getInfo(@PathVariable("susionFoodId") Long susionFoodId)
    {
        return AjaxResult.success(susionFoodService.selectSusionFoodBySusionFoodId(susionFoodId));
    }

    /**
     * 新增进口食品接触产品检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionFood:add')")
    @Log(title = "进口食品接触产品检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionFood susionFood)
    {
        return toAjax(susionFoodService.insertSusionFood(susionFood));
    }

    /**
     * 修改进口食品接触产品检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionFood:edit')")
    @Log(title = "进口食品接触产品检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionFood susionFood)
    {
        return toAjax(susionFoodService.updateSusionFood(susionFood));
    }

    /**
     * 删除进口食品接触产品检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionFood:remove')")
    @Log(title = "进口食品接触产品检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionFoodIds}")
    public AjaxResult remove(@PathVariable Long[] susionFoodIds)
    {
        return toAjax(susionFoodService.deleteSusionFoodBySusionFoodIds(susionFoodIds));
    }
}
