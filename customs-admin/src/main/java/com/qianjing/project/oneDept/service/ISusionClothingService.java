package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionClothing;

/**
 * 进口服装检验监管情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface ISusionClothingService 
{
    /**
     * 查询进口服装检验监管情况统计
     * 
     * @param susionClothingId 进口服装检验监管情况统计主键
     * @return 进口服装检验监管情况统计
     */
    public SusionClothing selectSusionClothingBySusionClothingId(Long susionClothingId);

    /**
     * 查询进口服装检验监管情况统计列表
     * 
     * @param susionClothing 进口服装检验监管情况统计
     * @return 进口服装检验监管情况统计集合
     */
    public List<SusionClothing> selectSusionClothingList(SusionClothing susionClothing);

    /**
     * 新增进口服装检验监管情况统计
     * 
     * @param susionClothing 进口服装检验监管情况统计
     * @return 结果
     */
    public int insertSusionClothing(SusionClothing susionClothing);

    /**
     * 修改进口服装检验监管情况统计
     * 
     * @param susionClothing 进口服装检验监管情况统计
     * @return 结果
     */
    public int updateSusionClothing(SusionClothing susionClothing);

    /**
     * 批量删除进口服装检验监管情况统计
     * 
     * @param susionClothingIds 需要删除的进口服装检验监管情况统计主键集合
     * @return 结果
     */
    public int deleteSusionClothingBySusionClothingIds(Long[] susionClothingIds);

    /**
     * 删除进口服装检验监管情况统计信息
     * 
     * @param susionClothingId 进口服装检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionClothingBySusionClothingId(Long susionClothingId);

    /**
     * 导入进口服装检验监管情况统计信息
     *
     * @param susionClothingList 进口服装检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionClothing(List<SusionClothing> susionClothingList, Boolean isUpdateSupport, String operName);
}
