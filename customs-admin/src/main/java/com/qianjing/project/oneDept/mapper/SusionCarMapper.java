package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionCar;

/**
 * 进口汽车和摩托车检验监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface SusionCarMapper 
{
    /**
     * 查询进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCarId 进口汽车和摩托车检验监管情况统计主键
     * @return 进口汽车和摩托车检验监管情况统计
     */
    public SusionCar selectSusionCarBySusionCarId(Long susionCarId);

    /**
     * 查询进口汽车和摩托车检验监管情况统计列表
     * 
     * @param susionCar 进口汽车和摩托车检验监管情况统计
     * @return 进口汽车和摩托车检验监管情况统计集合
     */
    public List<SusionCar> selectSusionCarList(SusionCar susionCar);

    /**
     * 新增进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCar 进口汽车和摩托车检验监管情况统计
     * @return 结果
     */
    public int insertSusionCar(SusionCar susionCar);

    /**
     * 修改进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCar 进口汽车和摩托车检验监管情况统计
     * @return 结果
     */
    public int updateSusionCar(SusionCar susionCar);

    /**
     * 删除进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCarId 进口汽车和摩托车检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionCarBySusionCarId(Long susionCarId);

    /**
     * 批量删除进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCarIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionCarBySusionCarIds(Long[] susionCarIds);
}
