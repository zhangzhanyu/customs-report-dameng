package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionElectric;

/**
 * 进口旧机电检验监管情况Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface ISusionElectricService 
{
    /**
     * 查询进口旧机电检验监管情况
     * 
     * @param susionElectricId 进口旧机电检验监管情况主键
     * @return 进口旧机电检验监管情况
     */
    public SusionElectric selectSusionElectricBySusionElectricId(Long susionElectricId);

    /**
     * 查询进口旧机电检验监管情况列表
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 进口旧机电检验监管情况集合
     */
    public List<SusionElectric> selectSusionElectricList(SusionElectric susionElectric);

    /**
     * 新增进口旧机电检验监管情况
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 结果
     */
    public int insertSusionElectric(SusionElectric susionElectric);

    /**
     * 修改进口旧机电检验监管情况
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 结果
     */
    public int updateSusionElectric(SusionElectric susionElectric);

    /**
     * 批量删除进口旧机电检验监管情况
     * 
     * @param susionElectricIds 需要删除的进口旧机电检验监管情况主键集合
     * @return 结果
     */
    public int deleteSusionElectricBySusionElectricIds(Long[] susionElectricIds);

    /**
     * 删除进口旧机电检验监管情况信息
     * 
     * @param susionElectricId 进口旧机电检验监管情况主键
     * @return 结果
     */
    public int deleteSusionElectricBySusionElectricId(Long susionElectricId);

    /**
     * 导入进口旧机电检验监管情况信息
     *
     * @param susionElectricList 进口旧机电检验监管情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionElectric(List<SusionElectric> susionElectricList, Boolean isUpdateSupport, String operName);
}
