package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionHygienic;

/**
 * 进口一次性使用卫生用品检验监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface SusionHygienicMapper 
{
    /**
     * 查询进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienicId 进口一次性使用卫生用品检验监管情况统计主键
     * @return 进口一次性使用卫生用品检验监管情况统计
     */
    public SusionHygienic selectSusionHygienicBySusionHygienicId(Long susionHygienicId);

    /**
     * 查询进口一次性使用卫生用品检验监管情况统计列表
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 进口一次性使用卫生用品检验监管情况统计集合
     */
    public List<SusionHygienic> selectSusionHygienicList(SusionHygienic susionHygienic);

    /**
     * 新增进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 结果
     */
    public int insertSusionHygienic(SusionHygienic susionHygienic);

    /**
     * 修改进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 结果
     */
    public int updateSusionHygienic(SusionHygienic susionHygienic);

    /**
     * 删除进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienicId 进口一次性使用卫生用品检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionHygienicBySusionHygienicId(Long susionHygienicId);

    /**
     * 批量删除进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienicIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionHygienicBySusionHygienicIds(Long[] susionHygienicIds);
}
