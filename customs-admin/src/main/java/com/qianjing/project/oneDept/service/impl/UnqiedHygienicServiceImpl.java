package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.UnqiedHygienicMapper;
import com.qianjing.project.oneDept.domain.UnqiedHygienic;
import com.qianjing.project.oneDept.service.IUnqiedHygienicService;

/**
 * 进口一次性使用卫生用品不合格统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class UnqiedHygienicServiceImpl implements IUnqiedHygienicService 
{
    private static final Logger log = LoggerFactory.getLogger(UnqiedHygienicServiceImpl.class);

    @Autowired
    private UnqiedHygienicMapper unqiedHygienicMapper;

    /**
     * 查询进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienicId 进口一次性使用卫生用品不合格统计主键
     * @return 进口一次性使用卫生用品不合格统计
     */
    @Override
    public UnqiedHygienic selectUnqiedHygienicByUnqiedHygienicId(Long unqiedHygienicId)
    {
        return unqiedHygienicMapper.selectUnqiedHygienicByUnqiedHygienicId(unqiedHygienicId);
    }

    /**
     * 查询进口一次性使用卫生用品不合格统计列表
     * 
     * @param unqiedHygienic 进口一次性使用卫生用品不合格统计
     * @return 进口一次性使用卫生用品不合格统计
     */
    @Override
    public List<UnqiedHygienic> selectUnqiedHygienicList(UnqiedHygienic unqiedHygienic)
    {
        return unqiedHygienicMapper.selectUnqiedHygienicList(unqiedHygienic);
    }

    /**
     * 新增进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienic 进口一次性使用卫生用品不合格统计
     * @return 结果
     */
    @Override
    public int insertUnqiedHygienic(UnqiedHygienic unqiedHygienic)
    {
        unqiedHygienic.setCreateTime(DateUtils.getNowDate());
        return unqiedHygienicMapper.insertUnqiedHygienic(unqiedHygienic);
    }

    /**
     * 修改进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienic 进口一次性使用卫生用品不合格统计
     * @return 结果
     */
    @Override
    public int updateUnqiedHygienic(UnqiedHygienic unqiedHygienic)
    {
        unqiedHygienic.setUpdateTime(DateUtils.getNowDate());
        return unqiedHygienicMapper.updateUnqiedHygienic(unqiedHygienic);
    }

    /**
     * 批量删除进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienicIds 需要删除的进口一次性使用卫生用品不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedHygienicByUnqiedHygienicIds(Long[] unqiedHygienicIds)
    {
        return unqiedHygienicMapper.deleteUnqiedHygienicByUnqiedHygienicIds(unqiedHygienicIds);
    }

    /**
     * 删除进口一次性使用卫生用品不合格统计信息
     * 
     * @param unqiedHygienicId 进口一次性使用卫生用品不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedHygienicByUnqiedHygienicId(Long unqiedHygienicId)
    {
        return unqiedHygienicMapper.deleteUnqiedHygienicByUnqiedHygienicId(unqiedHygienicId);
    }

    /**
     * 导入进口一次性使用卫生用品不合格统计数据
     *
     * @param unqiedHygienicList unqiedHygienicList 进口一次性使用卫生用品不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUnqiedHygienic(List<UnqiedHygienic> unqiedHygienicList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(unqiedHygienicList) || unqiedHygienicList.size() == 0) {
            throw new ServiceException("导入进口一次性使用卫生用品不合格统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UnqiedHygienic unqiedHygienic : unqiedHygienicList){
            try {
                // 验证是否存在
                if (true) {
                    unqiedHygienic.setCreateBy(operName);
                    this.insertUnqiedHygienic(unqiedHygienic);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    unqiedHygienic.setUpdateBy(operName);
                    this.updateUnqiedHygienic(unqiedHygienic);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
