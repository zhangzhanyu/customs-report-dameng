package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.UnqiedChildrenSuppliesMapper;
import com.qianjing.project.oneDept.domain.UnqiedChildrenSupplies;
import com.qianjing.project.oneDept.service.IUnqiedChildrenSuppliesService;

/**
 * 进口儿童用品不合格统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class UnqiedChildrenSuppliesServiceImpl implements IUnqiedChildrenSuppliesService 
{
    private static final Logger log = LoggerFactory.getLogger(UnqiedChildrenSuppliesServiceImpl.class);

    @Autowired
    private UnqiedChildrenSuppliesMapper unqiedChildrenSuppliesMapper;

    /**
     * 查询进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSuppliesId 进口儿童用品不合格统计主键
     * @return 进口儿童用品不合格统计
     */
    @Override
    public UnqiedChildrenSupplies selectUnqiedChildrenSuppliesByUnqiedChildrenSuppliesId(Long unqiedChildrenSuppliesId)
    {
        return unqiedChildrenSuppliesMapper.selectUnqiedChildrenSuppliesByUnqiedChildrenSuppliesId(unqiedChildrenSuppliesId);
    }

    /**
     * 查询进口儿童用品不合格统计列表
     * 
     * @param unqiedChildrenSupplies 进口儿童用品不合格统计
     * @return 进口儿童用品不合格统计
     */
    @Override
    public List<UnqiedChildrenSupplies> selectUnqiedChildrenSuppliesList(UnqiedChildrenSupplies unqiedChildrenSupplies)
    {
        return unqiedChildrenSuppliesMapper.selectUnqiedChildrenSuppliesList(unqiedChildrenSupplies);
    }

    /**
     * 新增进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSupplies 进口儿童用品不合格统计
     * @return 结果
     */
    @Override
    public int insertUnqiedChildrenSupplies(UnqiedChildrenSupplies unqiedChildrenSupplies)
    {
        unqiedChildrenSupplies.setCreateTime(DateUtils.getNowDate());
        return unqiedChildrenSuppliesMapper.insertUnqiedChildrenSupplies(unqiedChildrenSupplies);
    }

    /**
     * 修改进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSupplies 进口儿童用品不合格统计
     * @return 结果
     */
    @Override
    public int updateUnqiedChildrenSupplies(UnqiedChildrenSupplies unqiedChildrenSupplies)
    {
        unqiedChildrenSupplies.setUpdateTime(DateUtils.getNowDate());
        return unqiedChildrenSuppliesMapper.updateUnqiedChildrenSupplies(unqiedChildrenSupplies);
    }

    /**
     * 批量删除进口儿童用品不合格统计
     * 
     * @param unqiedChildrenSuppliesIds 需要删除的进口儿童用品不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedChildrenSuppliesByUnqiedChildrenSuppliesIds(Long[] unqiedChildrenSuppliesIds)
    {
        return unqiedChildrenSuppliesMapper.deleteUnqiedChildrenSuppliesByUnqiedChildrenSuppliesIds(unqiedChildrenSuppliesIds);
    }

    /**
     * 删除进口儿童用品不合格统计信息
     * 
     * @param unqiedChildrenSuppliesId 进口儿童用品不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedChildrenSuppliesByUnqiedChildrenSuppliesId(Long unqiedChildrenSuppliesId)
    {
        return unqiedChildrenSuppliesMapper.deleteUnqiedChildrenSuppliesByUnqiedChildrenSuppliesId(unqiedChildrenSuppliesId);
    }

    /**
     * 导入进口儿童用品不合格统计数据
     *
     * @param unqiedChildrenSuppliesList unqiedChildrenSuppliesList 进口儿童用品不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUnqiedChildrenSupplies(List<UnqiedChildrenSupplies> unqiedChildrenSuppliesList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(unqiedChildrenSuppliesList) || unqiedChildrenSuppliesList.size() == 0) {
            throw new ServiceException("导入进口儿童用品不合格统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UnqiedChildrenSupplies unqiedChildrenSupplies : unqiedChildrenSuppliesList){
            try {
                // 验证是否存在
                if (true) {
                    unqiedChildrenSupplies.setCreateBy(operName);
                    this.insertUnqiedChildrenSupplies(unqiedChildrenSupplies);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    unqiedChildrenSupplies.setUpdateBy(operName);
                    this.updateUnqiedChildrenSupplies(unqiedChildrenSupplies);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
