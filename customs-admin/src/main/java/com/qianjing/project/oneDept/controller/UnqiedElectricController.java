package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.UnqiedElectric;
import com.qianjing.project.oneDept.service.IUnqiedElectricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口旧机电检验监管不合格情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/unqiedElectric")
public class UnqiedElectricController extends BaseController
{
    @Autowired
    private IUnqiedElectricService unqiedElectricService;

    /**
     * 查询进口旧机电检验监管不合格情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedElectric:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnqiedElectric unqiedElectric)
    {
        startPage();
        List<UnqiedElectric> list = unqiedElectricService.selectUnqiedElectricList(unqiedElectric);
        return getDataTable(list);
    }

    /**
     * 导出进口旧机电检验监管不合格情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedElectric:export')")
    @Log(title = "进口旧机电检验监管不合格情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UnqiedElectric unqiedElectric)
    {
        List<UnqiedElectric> list = unqiedElectricService.selectUnqiedElectricList(unqiedElectric);
        ExcelUtil<UnqiedElectric> util = new ExcelUtil<UnqiedElectric>(UnqiedElectric.class);
        return util.exportExcel(list, "进口旧机电检验监管不合格情况统计数据");
    }

    /**
     * 导入进口旧机电检验监管不合格情况统计列表
     */
    @Log(title = "进口旧机电检验监管不合格情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedElectric:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<UnqiedElectric> util = new ExcelUtil<UnqiedElectric>(UnqiedElectric.class);
        List<UnqiedElectric> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = unqiedElectricService.importUnqiedElectric(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口旧机电检验监管不合格情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<UnqiedElectric> util = new ExcelUtil<UnqiedElectric>(UnqiedElectric.class);
        util.importTemplateExcel(response, "进口旧机电检验监管不合格情况统计数据");
    }

    /**
     * 获取进口旧机电检验监管不合格情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedElectric:query')")
    @GetMapping(value = "/{unqiedElectricId}")
    public AjaxResult getInfo(@PathVariable("unqiedElectricId") Long unqiedElectricId)
    {
        return AjaxResult.success(unqiedElectricService.selectUnqiedElectricByUnqiedElectricId(unqiedElectricId));
    }

    /**
     * 新增进口旧机电检验监管不合格情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedElectric:add')")
    @Log(title = "进口旧机电检验监管不合格情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UnqiedElectric unqiedElectric)
    {
        return toAjax(unqiedElectricService.insertUnqiedElectric(unqiedElectric));
    }

    /**
     * 修改进口旧机电检验监管不合格情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedElectric:edit')")
    @Log(title = "进口旧机电检验监管不合格情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UnqiedElectric unqiedElectric)
    {
        return toAjax(unqiedElectricService.updateUnqiedElectric(unqiedElectric));
    }

    /**
     * 删除进口旧机电检验监管不合格情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedElectric:remove')")
    @Log(title = "进口旧机电检验监管不合格情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unqiedElectricIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedElectricIds)
    {
        return toAjax(unqiedElectricService.deleteUnqiedElectricByUnqiedElectricIds(unqiedElectricIds));
    }
}
