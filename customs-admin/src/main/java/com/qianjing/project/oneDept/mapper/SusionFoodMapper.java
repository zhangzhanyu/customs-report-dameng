package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionFood;

/**
 * 进口食品接触产品检验监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface SusionFoodMapper 
{
    /**
     * 查询进口食品接触产品检验监管情况统计
     * 
     * @param susionFoodId 进口食品接触产品检验监管情况统计主键
     * @return 进口食品接触产品检验监管情况统计
     */
    public SusionFood selectSusionFoodBySusionFoodId(Long susionFoodId);

    /**
     * 查询进口食品接触产品检验监管情况统计列表
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 进口食品接触产品检验监管情况统计集合
     */
    public List<SusionFood> selectSusionFoodList(SusionFood susionFood);

    /**
     * 新增进口食品接触产品检验监管情况统计
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 结果
     */
    public int insertSusionFood(SusionFood susionFood);

    /**
     * 修改进口食品接触产品检验监管情况统计
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 结果
     */
    public int updateSusionFood(SusionFood susionFood);

    /**
     * 删除进口食品接触产品检验监管情况统计
     * 
     * @param susionFoodId 进口食品接触产品检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionFoodBySusionFoodId(Long susionFoodId);

    /**
     * 批量删除进口食品接触产品检验监管情况统计
     * 
     * @param susionFoodIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionFoodBySusionFoodIds(Long[] susionFoodIds);
}
