package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.UnqiedHygienic;
import com.qianjing.project.oneDept.service.IUnqiedHygienicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口一次性使用卫生用品不合格统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/unqiedHygienic")
public class UnqiedHygienicController extends BaseController
{
    @Autowired
    private IUnqiedHygienicService unqiedHygienicService;

    /**
     * 查询进口一次性使用卫生用品不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedHygienic:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnqiedHygienic unqiedHygienic)
    {
        startPage();
        List<UnqiedHygienic> list = unqiedHygienicService.selectUnqiedHygienicList(unqiedHygienic);
        return getDataTable(list);
    }

    /**
     * 导出进口一次性使用卫生用品不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedHygienic:export')")
    @Log(title = "进口一次性使用卫生用品不合格统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UnqiedHygienic unqiedHygienic)
    {
        List<UnqiedHygienic> list = unqiedHygienicService.selectUnqiedHygienicList(unqiedHygienic);
        ExcelUtil<UnqiedHygienic> util = new ExcelUtil<UnqiedHygienic>(UnqiedHygienic.class);
        return util.exportExcel(list, "进口一次性使用卫生用品不合格统计数据");
    }

    /**
     * 导入进口一次性使用卫生用品不合格统计列表
     */
    @Log(title = "进口一次性使用卫生用品不合格统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedHygienic:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<UnqiedHygienic> util = new ExcelUtil<UnqiedHygienic>(UnqiedHygienic.class);
        List<UnqiedHygienic> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = unqiedHygienicService.importUnqiedHygienic(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口一次性使用卫生用品不合格统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<UnqiedHygienic> util = new ExcelUtil<UnqiedHygienic>(UnqiedHygienic.class);
        util.importTemplateExcel(response, "进口一次性使用卫生用品不合格统计数据");
    }

    /**
     * 获取进口一次性使用卫生用品不合格统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedHygienic:query')")
    @GetMapping(value = "/{unqiedHygienicId}")
    public AjaxResult getInfo(@PathVariable("unqiedHygienicId") Long unqiedHygienicId)
    {
        return AjaxResult.success(unqiedHygienicService.selectUnqiedHygienicByUnqiedHygienicId(unqiedHygienicId));
    }

    /**
     * 新增进口一次性使用卫生用品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedHygienic:add')")
    @Log(title = "进口一次性使用卫生用品不合格统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UnqiedHygienic unqiedHygienic)
    {
        return toAjax(unqiedHygienicService.insertUnqiedHygienic(unqiedHygienic));
    }

    /**
     * 修改进口一次性使用卫生用品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedHygienic:edit')")
    @Log(title = "进口一次性使用卫生用品不合格统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UnqiedHygienic unqiedHygienic)
    {
        return toAjax(unqiedHygienicService.updateUnqiedHygienic(unqiedHygienic));
    }

    /**
     * 删除进口一次性使用卫生用品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedHygienic:remove')")
    @Log(title = "进口一次性使用卫生用品不合格统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unqiedHygienicIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedHygienicIds)
    {
        return toAjax(unqiedHygienicService.deleteUnqiedHygienicByUnqiedHygienicIds(unqiedHygienicIds));
    }
}
