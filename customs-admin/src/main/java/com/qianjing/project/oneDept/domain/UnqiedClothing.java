package com.qianjing.project.oneDept.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口服装不合格统计对象 unqied_clothing
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public class UnqiedClothing extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long unqiedClothingId;

    /** 直属海关 */
    @Excel(name = "直属海关")
    private String customs;

    /** 报关单号 */
    @Excel(name = "报关单号")
    private String customsDeclareCode;

    /** 检验检疫编号 */
    @Excel(name = "检验检疫编号")
    private String quarantineCode;

    /** 经营单位/收货人 */
    @Excel(name = "经营单位/收货人")
    private String consignee;

    /** 商品名称(系统申报) */
    @Excel(name = "商品名称(系统申报)")
    private String goodsName;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 型号 */
    @Excel(name = "型号")
    private String model;

    /** 品质证书 */
    @Excel(name = "品质证书")
    private String qualityCertificate;

    /** 重量证书 */
    @Excel(name = "重量证书")
    private String weightCertificate;

    /** 不合格数量 */
    @Excel(name = "不合格数量")
    private String unqiedQuantity;

    /** 不合格金额 */
    @Excel(name = "不合格金额")
    private String unqiedAmount;

    /** 不合格项目 */
    @Excel(name = "不合格项目")
    private String unqiedItem;

    /** 不合格描述 */
    @Excel(name = "不合格描述")
    private String unqiedDescribe;

    /** 不合格处置 */
    @Excel(name = "不合格处置")
    private String unqiedDeal;

    /** 判断依据名称 */
    @Excel(name = "判断依据名称")
    private String judgeBasis;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setUnqiedClothingId(Long unqiedClothingId) 
    {
        this.unqiedClothingId = unqiedClothingId;
    }

    public Long getUnqiedClothingId() 
    {
        return unqiedClothingId;
    }
    public void setCustoms(String customs) 
    {
        this.customs = customs;
    }

    public String getCustoms() 
    {
        return customs;
    }
    public void setCustomsDeclareCode(String customsDeclareCode) 
    {
        this.customsDeclareCode = customsDeclareCode;
    }

    public String getCustomsDeclareCode() 
    {
        return customsDeclareCode;
    }
    public void setQuarantineCode(String quarantineCode) 
    {
        this.quarantineCode = quarantineCode;
    }

    public String getQuarantineCode() 
    {
        return quarantineCode;
    }
    public void setConsignee(String consignee) 
    {
        this.consignee = consignee;
    }

    public String getConsignee() 
    {
        return consignee;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setModel(String model) 
    {
        this.model = model;
    }

    public String getModel() 
    {
        return model;
    }
    public void setQualityCertificate(String qualityCertificate) 
    {
        this.qualityCertificate = qualityCertificate;
    }

    public String getQualityCertificate() 
    {
        return qualityCertificate;
    }
    public void setWeightCertificate(String weightCertificate) 
    {
        this.weightCertificate = weightCertificate;
    }

    public String getWeightCertificate() 
    {
        return weightCertificate;
    }
    public void setUnqiedQuantity(String unqiedQuantity) 
    {
        this.unqiedQuantity = unqiedQuantity;
    }

    public String getUnqiedQuantity() 
    {
        return unqiedQuantity;
    }
    public void setUnqiedAmount(String unqiedAmount) 
    {
        this.unqiedAmount = unqiedAmount;
    }

    public String getUnqiedAmount() 
    {
        return unqiedAmount;
    }
    public void setUnqiedItem(String unqiedItem) 
    {
        this.unqiedItem = unqiedItem;
    }

    public String getUnqiedItem() 
    {
        return unqiedItem;
    }
    public void setUnqiedDescribe(String unqiedDescribe) 
    {
        this.unqiedDescribe = unqiedDescribe;
    }

    public String getUnqiedDescribe() 
    {
        return unqiedDescribe;
    }
    public void setUnqiedDeal(String unqiedDeal) 
    {
        this.unqiedDeal = unqiedDeal;
    }

    public String getUnqiedDeal() 
    {
        return unqiedDeal;
    }
    public void setJudgeBasis(String judgeBasis) 
    {
        this.judgeBasis = judgeBasis;
    }

    public String getJudgeBasis() 
    {
        return judgeBasis;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("unqiedClothingId", getUnqiedClothingId())
            .append("customs", getCustoms())
            .append("customsDeclareCode", getCustomsDeclareCode())
            .append("quarantineCode", getQuarantineCode())
            .append("consignee", getConsignee())
            .append("goodsName", getGoodsName())
            .append("brand", getBrand())
            .append("model", getModel())
            .append("qualityCertificate", getQualityCertificate())
            .append("weightCertificate", getWeightCertificate())
            .append("unqiedQuantity", getUnqiedQuantity())
            .append("unqiedAmount", getUnqiedAmount())
            .append("unqiedItem", getUnqiedItem())
            .append("unqiedDescribe", getUnqiedDescribe())
            .append("unqiedDeal", getUnqiedDeal())
            .append("judgeBasis", getJudgeBasis())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
