package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedCar;

/**
 * 进口汽车和摩托车不合格统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface IUnqiedCarService 
{
    /**
     * 查询进口汽车和摩托车不合格统计
     * 
     * @param unqiedCarId 进口汽车和摩托车不合格统计主键
     * @return 进口汽车和摩托车不合格统计
     */
    public UnqiedCar selectUnqiedCarByUnqiedCarId(Long unqiedCarId);

    /**
     * 查询进口汽车和摩托车不合格统计列表
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 进口汽车和摩托车不合格统计集合
     */
    public List<UnqiedCar> selectUnqiedCarList(UnqiedCar unqiedCar);

    /**
     * 新增进口汽车和摩托车不合格统计
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 结果
     */
    public int insertUnqiedCar(UnqiedCar unqiedCar);

    /**
     * 修改进口汽车和摩托车不合格统计
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 结果
     */
    public int updateUnqiedCar(UnqiedCar unqiedCar);

    /**
     * 批量删除进口汽车和摩托车不合格统计
     * 
     * @param unqiedCarIds 需要删除的进口汽车和摩托车不合格统计主键集合
     * @return 结果
     */
    public int deleteUnqiedCarByUnqiedCarIds(Long[] unqiedCarIds);

    /**
     * 删除进口汽车和摩托车不合格统计信息
     * 
     * @param unqiedCarId 进口汽车和摩托车不合格统计主键
     * @return 结果
     */
    public int deleteUnqiedCarByUnqiedCarId(Long unqiedCarId);

    /**
     * 导入进口汽车和摩托车不合格统计信息
     *
     * @param unqiedCarList 进口汽车和摩托车不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUnqiedCar(List<UnqiedCar> unqiedCarList, Boolean isUpdateSupport, String operName);
}
