package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.UnqiedElectricMapper;
import com.qianjing.project.oneDept.domain.UnqiedElectric;
import com.qianjing.project.oneDept.service.IUnqiedElectricService;

/**
 * 进口旧机电检验监管不合格情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class UnqiedElectricServiceImpl implements IUnqiedElectricService 
{
    private static final Logger log = LoggerFactory.getLogger(UnqiedElectricServiceImpl.class);

    @Autowired
    private UnqiedElectricMapper unqiedElectricMapper;

    /**
     * 查询进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectricId 进口旧机电检验监管不合格情况统计主键
     * @return 进口旧机电检验监管不合格情况统计
     */
    @Override
    public UnqiedElectric selectUnqiedElectricByUnqiedElectricId(Long unqiedElectricId)
    {
        return unqiedElectricMapper.selectUnqiedElectricByUnqiedElectricId(unqiedElectricId);
    }

    /**
     * 查询进口旧机电检验监管不合格情况统计列表
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 进口旧机电检验监管不合格情况统计
     */
    @Override
    public List<UnqiedElectric> selectUnqiedElectricList(UnqiedElectric unqiedElectric)
    {
        return unqiedElectricMapper.selectUnqiedElectricList(unqiedElectric);
    }

    /**
     * 新增进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 结果
     */
    @Override
    public int insertUnqiedElectric(UnqiedElectric unqiedElectric)
    {
        unqiedElectric.setCreateTime(DateUtils.getNowDate());
        return unqiedElectricMapper.insertUnqiedElectric(unqiedElectric);
    }

    /**
     * 修改进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 结果
     */
    @Override
    public int updateUnqiedElectric(UnqiedElectric unqiedElectric)
    {
        unqiedElectric.setUpdateTime(DateUtils.getNowDate());
        return unqiedElectricMapper.updateUnqiedElectric(unqiedElectric);
    }

    /**
     * 批量删除进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectricIds 需要删除的进口旧机电检验监管不合格情况统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedElectricByUnqiedElectricIds(Long[] unqiedElectricIds)
    {
        return unqiedElectricMapper.deleteUnqiedElectricByUnqiedElectricIds(unqiedElectricIds);
    }

    /**
     * 删除进口旧机电检验监管不合格情况统计信息
     * 
     * @param unqiedElectricId 进口旧机电检验监管不合格情况统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedElectricByUnqiedElectricId(Long unqiedElectricId)
    {
        return unqiedElectricMapper.deleteUnqiedElectricByUnqiedElectricId(unqiedElectricId);
    }

    /**
     * 导入进口旧机电检验监管不合格情况统计数据
     *
     * @param unqiedElectricList unqiedElectricList 进口旧机电检验监管不合格情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUnqiedElectric(List<UnqiedElectric> unqiedElectricList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(unqiedElectricList) || unqiedElectricList.size() == 0) {
            throw new ServiceException("导入进口旧机电检验监管不合格情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UnqiedElectric unqiedElectric : unqiedElectricList){
            try {
                // 验证是否存在
                if (true) {
                    unqiedElectric.setCreateBy(operName);
                    this.insertUnqiedElectric(unqiedElectric);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    unqiedElectric.setUpdateBy(operName);
                    this.updateUnqiedElectric(unqiedElectric);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
