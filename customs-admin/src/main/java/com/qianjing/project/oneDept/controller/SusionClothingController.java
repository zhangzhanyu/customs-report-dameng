package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.SusionClothing;
import com.qianjing.project.oneDept.service.ISusionClothingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口服装检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/susionClothing")
public class SusionClothingController extends BaseController
{
    @Autowired
    private ISusionClothingService susionClothingService;

    /**
     * 查询进口服装检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionClothing:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionClothing susionClothing)
    {
        startPage();
        List<SusionClothing> list = susionClothingService.selectSusionClothingList(susionClothing);
        return getDataTable(list);
    }

    /**
     * 导出进口服装检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionClothing:export')")
    @Log(title = "进口服装检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionClothing susionClothing)
    {
        List<SusionClothing> list = susionClothingService.selectSusionClothingList(susionClothing);
        ExcelUtil<SusionClothing> util = new ExcelUtil<SusionClothing>(SusionClothing.class);
        return util.exportExcel(list, "进口服装检验监管情况统计数据");
    }

    /**
     * 导入进口服装检验监管情况统计列表
     */
    @Log(title = "进口服装检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:susionClothing:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionClothing> util = new ExcelUtil<SusionClothing>(SusionClothing.class);
        List<SusionClothing> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionClothingService.importSusionClothing(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口服装检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionClothing> util = new ExcelUtil<SusionClothing>(SusionClothing.class);
        util.importTemplateExcel(response, "进口服装检验监管情况统计数据");
    }

    /**
     * 获取进口服装检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionClothing:query')")
    @GetMapping(value = "/{susionClothingId}")
    public AjaxResult getInfo(@PathVariable("susionClothingId") Long susionClothingId)
    {
        return AjaxResult.success(susionClothingService.selectSusionClothingBySusionClothingId(susionClothingId));
    }

    /**
     * 新增进口服装检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionClothing:add')")
    @Log(title = "进口服装检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionClothing susionClothing)
    {
        return toAjax(susionClothingService.insertSusionClothing(susionClothing));
    }

    /**
     * 修改进口服装检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionClothing:edit')")
    @Log(title = "进口服装检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionClothing susionClothing)
    {
        return toAjax(susionClothingService.updateSusionClothing(susionClothing));
    }

    /**
     * 删除进口服装检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionClothing:remove')")
    @Log(title = "进口服装检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionClothingIds}")
    public AjaxResult remove(@PathVariable Long[] susionClothingIds)
    {
        return toAjax(susionClothingService.deleteSusionClothingBySusionClothingIds(susionClothingIds));
    }
}
