package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.UnqiedClothingMapper;
import com.qianjing.project.oneDept.domain.UnqiedClothing;
import com.qianjing.project.oneDept.service.IUnqiedClothingService;

/**
 * 进口服装不合格统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class UnqiedClothingServiceImpl implements IUnqiedClothingService 
{
    private static final Logger log = LoggerFactory.getLogger(UnqiedClothingServiceImpl.class);

    @Autowired
    private UnqiedClothingMapper unqiedClothingMapper;

    /**
     * 查询进口服装不合格统计
     * 
     * @param unqiedClothingId 进口服装不合格统计主键
     * @return 进口服装不合格统计
     */
    @Override
    public UnqiedClothing selectUnqiedClothingByUnqiedClothingId(Long unqiedClothingId)
    {
        return unqiedClothingMapper.selectUnqiedClothingByUnqiedClothingId(unqiedClothingId);
    }

    /**
     * 查询进口服装不合格统计列表
     * 
     * @param unqiedClothing 进口服装不合格统计
     * @return 进口服装不合格统计
     */
    @Override
    public List<UnqiedClothing> selectUnqiedClothingList(UnqiedClothing unqiedClothing)
    {
        return unqiedClothingMapper.selectUnqiedClothingList(unqiedClothing);
    }

    /**
     * 新增进口服装不合格统计
     * 
     * @param unqiedClothing 进口服装不合格统计
     * @return 结果
     */
    @Override
    public int insertUnqiedClothing(UnqiedClothing unqiedClothing)
    {
        unqiedClothing.setCreateTime(DateUtils.getNowDate());
        return unqiedClothingMapper.insertUnqiedClothing(unqiedClothing);
    }

    /**
     * 修改进口服装不合格统计
     * 
     * @param unqiedClothing 进口服装不合格统计
     * @return 结果
     */
    @Override
    public int updateUnqiedClothing(UnqiedClothing unqiedClothing)
    {
        unqiedClothing.setUpdateTime(DateUtils.getNowDate());
        return unqiedClothingMapper.updateUnqiedClothing(unqiedClothing);
    }

    /**
     * 批量删除进口服装不合格统计
     * 
     * @param unqiedClothingIds 需要删除的进口服装不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedClothingByUnqiedClothingIds(Long[] unqiedClothingIds)
    {
        return unqiedClothingMapper.deleteUnqiedClothingByUnqiedClothingIds(unqiedClothingIds);
    }

    /**
     * 删除进口服装不合格统计信息
     * 
     * @param unqiedClothingId 进口服装不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedClothingByUnqiedClothingId(Long unqiedClothingId)
    {
        return unqiedClothingMapper.deleteUnqiedClothingByUnqiedClothingId(unqiedClothingId);
    }

    /**
     * 导入进口服装不合格统计数据
     *
     * @param unqiedClothingList unqiedClothingList 进口服装不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUnqiedClothing(List<UnqiedClothing> unqiedClothingList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(unqiedClothingList) || unqiedClothingList.size() == 0) {
            throw new ServiceException("导入进口服装不合格统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UnqiedClothing unqiedClothing : unqiedClothingList){
            try {
                // 验证是否存在
                if (true) {
                    unqiedClothing.setCreateBy(operName);
                    this.insertUnqiedClothing(unqiedClothing);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    unqiedClothing.setUpdateBy(operName);
                    this.updateUnqiedClothing(unqiedClothing);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
