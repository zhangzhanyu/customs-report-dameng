package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.SusionCarMapper;
import com.qianjing.project.oneDept.domain.SusionCar;
import com.qianjing.project.oneDept.service.ISusionCarService;

/**
 * 进口汽车和摩托车检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class SusionCarServiceImpl implements ISusionCarService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionCarServiceImpl.class);

    @Autowired
    private SusionCarMapper susionCarMapper;

    /**
     * 查询进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCarId 进口汽车和摩托车检验监管情况统计主键
     * @return 进口汽车和摩托车检验监管情况统计
     */
    @Override
    public SusionCar selectSusionCarBySusionCarId(Long susionCarId)
    {
        return susionCarMapper.selectSusionCarBySusionCarId(susionCarId);
    }

    /**
     * 查询进口汽车和摩托车检验监管情况统计列表
     * 
     * @param susionCar 进口汽车和摩托车检验监管情况统计
     * @return 进口汽车和摩托车检验监管情况统计
     */
    @Override
    public List<SusionCar> selectSusionCarList(SusionCar susionCar)
    {
        return susionCarMapper.selectSusionCarList(susionCar);
    }

    /**
     * 新增进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCar 进口汽车和摩托车检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionCar(SusionCar susionCar)
    {
        return susionCarMapper.insertSusionCar(susionCar);
    }

    /**
     * 修改进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCar 进口汽车和摩托车检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionCar(SusionCar susionCar)
    {
        return susionCarMapper.updateSusionCar(susionCar);
    }

    /**
     * 批量删除进口汽车和摩托车检验监管情况统计
     * 
     * @param susionCarIds 需要删除的进口汽车和摩托车检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionCarBySusionCarIds(Long[] susionCarIds)
    {
        return susionCarMapper.deleteSusionCarBySusionCarIds(susionCarIds);
    }

    /**
     * 删除进口汽车和摩托车检验监管情况统计信息
     * 
     * @param susionCarId 进口汽车和摩托车检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionCarBySusionCarId(Long susionCarId)
    {
        return susionCarMapper.deleteSusionCarBySusionCarId(susionCarId);
    }

    /**
     * 导入进口汽车和摩托车检验监管情况统计数据
     *
     * @param susionCarList susionCarList 进口汽车和摩托车检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionCar(List<SusionCar> susionCarList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionCarList) || susionCarList.size() == 0) {
            throw new ServiceException("导入进口汽车和摩托车检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionCar susionCar : susionCarList){
            try {
                // 验证是否存在
                if (true) {
                    susionCar.setCreateBy(operName);
                    this.insertSusionCar(susionCar);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionCar.setUpdateBy(operName);
                    this.updateSusionCar(susionCar);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
