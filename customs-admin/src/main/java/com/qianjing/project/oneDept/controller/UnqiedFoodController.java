package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.UnqiedFood;
import com.qianjing.project.oneDept.service.IUnqiedFoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口食品接触产品不合格统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/unqiedFood")
public class UnqiedFoodController extends BaseController
{
    @Autowired
    private IUnqiedFoodService unqiedFoodService;

    /**
     * 查询进口食品接触产品不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedFood:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnqiedFood unqiedFood)
    {
        startPage();
        List<UnqiedFood> list = unqiedFoodService.selectUnqiedFoodList(unqiedFood);
        return getDataTable(list);
    }

    /**
     * 导出进口食品接触产品不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedFood:export')")
    @Log(title = "进口食品接触产品不合格统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UnqiedFood unqiedFood)
    {
        List<UnqiedFood> list = unqiedFoodService.selectUnqiedFoodList(unqiedFood);
        ExcelUtil<UnqiedFood> util = new ExcelUtil<UnqiedFood>(UnqiedFood.class);
        return util.exportExcel(list, "进口食品接触产品不合格统计数据");
    }

    /**
     * 导入进口食品接触产品不合格统计列表
     */
    @Log(title = "进口食品接触产品不合格统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedFood:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<UnqiedFood> util = new ExcelUtil<UnqiedFood>(UnqiedFood.class);
        List<UnqiedFood> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = unqiedFoodService.importUnqiedFood(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口食品接触产品不合格统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<UnqiedFood> util = new ExcelUtil<UnqiedFood>(UnqiedFood.class);
        util.importTemplateExcel(response, "进口食品接触产品不合格统计数据");
    }

    /**
     * 获取进口食品接触产品不合格统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedFood:query')")
    @GetMapping(value = "/{unqiedFoodId}")
    public AjaxResult getInfo(@PathVariable("unqiedFoodId") Long unqiedFoodId)
    {
        return AjaxResult.success(unqiedFoodService.selectUnqiedFoodByUnqiedFoodId(unqiedFoodId));
    }

    /**
     * 新增进口食品接触产品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedFood:add')")
    @Log(title = "进口食品接触产品不合格统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UnqiedFood unqiedFood)
    {
        return toAjax(unqiedFoodService.insertUnqiedFood(unqiedFood));
    }

    /**
     * 修改进口食品接触产品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedFood:edit')")
    @Log(title = "进口食品接触产品不合格统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UnqiedFood unqiedFood)
    {
        return toAjax(unqiedFoodService.updateUnqiedFood(unqiedFood));
    }

    /**
     * 删除进口食品接触产品不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedFood:remove')")
    @Log(title = "进口食品接触产品不合格统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unqiedFoodIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedFoodIds)
    {
        return toAjax(unqiedFoodService.deleteUnqiedFoodByUnqiedFoodIds(unqiedFoodIds));
    }
}
