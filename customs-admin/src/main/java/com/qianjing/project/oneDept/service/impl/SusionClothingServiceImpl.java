package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.SusionClothingMapper;
import com.qianjing.project.oneDept.domain.SusionClothing;
import com.qianjing.project.oneDept.service.ISusionClothingService;

/**
 * 进口服装检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class SusionClothingServiceImpl implements ISusionClothingService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionClothingServiceImpl.class);

    @Autowired
    private SusionClothingMapper susionClothingMapper;

    /**
     * 查询进口服装检验监管情况统计
     * 
     * @param susionClothingId 进口服装检验监管情况统计主键
     * @return 进口服装检验监管情况统计
     */
    @Override
    public SusionClothing selectSusionClothingBySusionClothingId(Long susionClothingId)
    {
        return susionClothingMapper.selectSusionClothingBySusionClothingId(susionClothingId);
    }

    /**
     * 查询进口服装检验监管情况统计列表
     * 
     * @param susionClothing 进口服装检验监管情况统计
     * @return 进口服装检验监管情况统计
     */
    @Override
    public List<SusionClothing> selectSusionClothingList(SusionClothing susionClothing)
    {
        return susionClothingMapper.selectSusionClothingList(susionClothing);
    }

    /**
     * 新增进口服装检验监管情况统计
     * 
     * @param susionClothing 进口服装检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionClothing(SusionClothing susionClothing)
    {
        return susionClothingMapper.insertSusionClothing(susionClothing);
    }

    /**
     * 修改进口服装检验监管情况统计
     * 
     * @param susionClothing 进口服装检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionClothing(SusionClothing susionClothing)
    {
        return susionClothingMapper.updateSusionClothing(susionClothing);
    }

    /**
     * 批量删除进口服装检验监管情况统计
     * 
     * @param susionClothingIds 需要删除的进口服装检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionClothingBySusionClothingIds(Long[] susionClothingIds)
    {
        return susionClothingMapper.deleteSusionClothingBySusionClothingIds(susionClothingIds);
    }

    /**
     * 删除进口服装检验监管情况统计信息
     * 
     * @param susionClothingId 进口服装检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionClothingBySusionClothingId(Long susionClothingId)
    {
        return susionClothingMapper.deleteSusionClothingBySusionClothingId(susionClothingId);
    }

    /**
     * 导入进口服装检验监管情况统计数据
     *
     * @param susionClothingList susionClothingList 进口服装检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionClothing(List<SusionClothing> susionClothingList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionClothingList) || susionClothingList.size() == 0) {
            throw new ServiceException("导入进口服装检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionClothing susionClothing : susionClothingList){
            try {
                // 验证是否存在
                if (true) {
                    susionClothing.setCreateBy(operName);
                    this.insertSusionClothing(susionClothing);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionClothing.setUpdateBy(operName);
                    this.updateSusionClothing(susionClothing);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
