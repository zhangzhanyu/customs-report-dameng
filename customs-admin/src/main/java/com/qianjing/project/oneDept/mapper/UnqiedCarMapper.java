package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedCar;

/**
 * 进口汽车和摩托车不合格统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface UnqiedCarMapper 
{
    /**
     * 查询进口汽车和摩托车不合格统计
     * 
     * @param unqiedCarId 进口汽车和摩托车不合格统计主键
     * @return 进口汽车和摩托车不合格统计
     */
    public UnqiedCar selectUnqiedCarByUnqiedCarId(Long unqiedCarId);

    /**
     * 查询进口汽车和摩托车不合格统计列表
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 进口汽车和摩托车不合格统计集合
     */
    public List<UnqiedCar> selectUnqiedCarList(UnqiedCar unqiedCar);

    /**
     * 新增进口汽车和摩托车不合格统计
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 结果
     */
    public int insertUnqiedCar(UnqiedCar unqiedCar);

    /**
     * 修改进口汽车和摩托车不合格统计
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 结果
     */
    public int updateUnqiedCar(UnqiedCar unqiedCar);

    /**
     * 删除进口汽车和摩托车不合格统计
     * 
     * @param unqiedCarId 进口汽车和摩托车不合格统计主键
     * @return 结果
     */
    public int deleteUnqiedCarByUnqiedCarId(Long unqiedCarId);

    /**
     * 批量删除进口汽车和摩托车不合格统计
     * 
     * @param unqiedCarIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUnqiedCarByUnqiedCarIds(Long[] unqiedCarIds);
}
