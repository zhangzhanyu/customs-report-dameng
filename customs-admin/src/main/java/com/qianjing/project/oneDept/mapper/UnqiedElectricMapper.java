package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedElectric;

/**
 * 进口旧机电检验监管不合格情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface UnqiedElectricMapper 
{
    /**
     * 查询进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectricId 进口旧机电检验监管不合格情况统计主键
     * @return 进口旧机电检验监管不合格情况统计
     */
    public UnqiedElectric selectUnqiedElectricByUnqiedElectricId(Long unqiedElectricId);

    /**
     * 查询进口旧机电检验监管不合格情况统计列表
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 进口旧机电检验监管不合格情况统计集合
     */
    public List<UnqiedElectric> selectUnqiedElectricList(UnqiedElectric unqiedElectric);

    /**
     * 新增进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 结果
     */
    public int insertUnqiedElectric(UnqiedElectric unqiedElectric);

    /**
     * 修改进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 结果
     */
    public int updateUnqiedElectric(UnqiedElectric unqiedElectric);

    /**
     * 删除进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectricId 进口旧机电检验监管不合格情况统计主键
     * @return 结果
     */
    public int deleteUnqiedElectricByUnqiedElectricId(Long unqiedElectricId);

    /**
     * 批量删除进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectricIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUnqiedElectricByUnqiedElectricIds(Long[] unqiedElectricIds);
}
