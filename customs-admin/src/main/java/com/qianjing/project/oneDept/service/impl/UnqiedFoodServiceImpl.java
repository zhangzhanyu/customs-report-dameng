package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.UnqiedFoodMapper;
import com.qianjing.project.oneDept.domain.UnqiedFood;
import com.qianjing.project.oneDept.service.IUnqiedFoodService;

/**
 * 进口食品接触产品不合格统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class UnqiedFoodServiceImpl implements IUnqiedFoodService 
{
    private static final Logger log = LoggerFactory.getLogger(UnqiedFoodServiceImpl.class);

    @Autowired
    private UnqiedFoodMapper unqiedFoodMapper;

    /**
     * 查询进口食品接触产品不合格统计
     * 
     * @param unqiedFoodId 进口食品接触产品不合格统计主键
     * @return 进口食品接触产品不合格统计
     */
    @Override
    public UnqiedFood selectUnqiedFoodByUnqiedFoodId(Long unqiedFoodId)
    {
        return unqiedFoodMapper.selectUnqiedFoodByUnqiedFoodId(unqiedFoodId);
    }

    /**
     * 查询进口食品接触产品不合格统计列表
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 进口食品接触产品不合格统计
     */
    @Override
    public List<UnqiedFood> selectUnqiedFoodList(UnqiedFood unqiedFood)
    {
        return unqiedFoodMapper.selectUnqiedFoodList(unqiedFood);
    }

    /**
     * 新增进口食品接触产品不合格统计
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 结果
     */
    @Override
    public int insertUnqiedFood(UnqiedFood unqiedFood)
    {
        unqiedFood.setCreateTime(DateUtils.getNowDate());
        return unqiedFoodMapper.insertUnqiedFood(unqiedFood);
    }

    /**
     * 修改进口食品接触产品不合格统计
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 结果
     */
    @Override
    public int updateUnqiedFood(UnqiedFood unqiedFood)
    {
        unqiedFood.setUpdateTime(DateUtils.getNowDate());
        return unqiedFoodMapper.updateUnqiedFood(unqiedFood);
    }

    /**
     * 批量删除进口食品接触产品不合格统计
     * 
     * @param unqiedFoodIds 需要删除的进口食品接触产品不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedFoodByUnqiedFoodIds(Long[] unqiedFoodIds)
    {
        return unqiedFoodMapper.deleteUnqiedFoodByUnqiedFoodIds(unqiedFoodIds);
    }

    /**
     * 删除进口食品接触产品不合格统计信息
     * 
     * @param unqiedFoodId 进口食品接触产品不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedFoodByUnqiedFoodId(Long unqiedFoodId)
    {
        return unqiedFoodMapper.deleteUnqiedFoodByUnqiedFoodId(unqiedFoodId);
    }

    /**
     * 导入进口食品接触产品不合格统计数据
     *
     * @param unqiedFoodList unqiedFoodList 进口食品接触产品不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUnqiedFood(List<UnqiedFood> unqiedFoodList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(unqiedFoodList) || unqiedFoodList.size() == 0) {
            throw new ServiceException("导入进口食品接触产品不合格统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UnqiedFood unqiedFood : unqiedFoodList){
            try {
                // 验证是否存在
                if (true) {
                    unqiedFood.setCreateBy(operName);
                    this.insertUnqiedFood(unqiedFood);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    unqiedFood.setUpdateBy(operName);
                    this.updateUnqiedFood(unqiedFood);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
