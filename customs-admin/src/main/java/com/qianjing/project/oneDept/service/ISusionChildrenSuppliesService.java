package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionChildrenSupplies;

/**
 * 进口儿童用品监管情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface ISusionChildrenSuppliesService 
{
    /**
     * 查询进口儿童用品监管情况统计
     * 
     * @param susionChildrenSuppliesId 进口儿童用品监管情况统计主键
     * @return 进口儿童用品监管情况统计
     */
    public SusionChildrenSupplies selectSusionChildrenSuppliesBySusionChildrenSuppliesId(Long susionChildrenSuppliesId);

    /**
     * 查询进口儿童用品监管情况统计列表
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 进口儿童用品监管情况统计集合
     */
    public List<SusionChildrenSupplies> selectSusionChildrenSuppliesList(SusionChildrenSupplies susionChildrenSupplies);

    /**
     * 新增进口儿童用品监管情况统计
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 结果
     */
    public int insertSusionChildrenSupplies(SusionChildrenSupplies susionChildrenSupplies);

    /**
     * 修改进口儿童用品监管情况统计
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 结果
     */
    public int updateSusionChildrenSupplies(SusionChildrenSupplies susionChildrenSupplies);

    /**
     * 批量删除进口儿童用品监管情况统计
     * 
     * @param susionChildrenSuppliesIds 需要删除的进口儿童用品监管情况统计主键集合
     * @return 结果
     */
    public int deleteSusionChildrenSuppliesBySusionChildrenSuppliesIds(Long[] susionChildrenSuppliesIds);

    /**
     * 删除进口儿童用品监管情况统计信息
     * 
     * @param susionChildrenSuppliesId 进口儿童用品监管情况统计主键
     * @return 结果
     */
    public int deleteSusionChildrenSuppliesBySusionChildrenSuppliesId(Long susionChildrenSuppliesId);

    /**
     * 导入进口儿童用品监管情况统计信息
     *
     * @param susionChildrenSuppliesList 进口儿童用品监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionChildrenSupplies(List<SusionChildrenSupplies> susionChildrenSuppliesList, Boolean isUpdateSupport, String operName);
}
