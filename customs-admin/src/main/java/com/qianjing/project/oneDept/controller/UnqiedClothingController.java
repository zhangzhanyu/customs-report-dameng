package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.UnqiedClothing;
import com.qianjing.project.oneDept.service.IUnqiedClothingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口服装不合格统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/unqiedClothing")
public class UnqiedClothingController extends BaseController
{
    @Autowired
    private IUnqiedClothingService unqiedClothingService;

    /**
     * 查询进口服装不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedClothing:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnqiedClothing unqiedClothing)
    {
        startPage();
        List<UnqiedClothing> list = unqiedClothingService.selectUnqiedClothingList(unqiedClothing);
        return getDataTable(list);
    }

    /**
     * 导出进口服装不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedClothing:export')")
    @Log(title = "进口服装不合格统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UnqiedClothing unqiedClothing)
    {
        List<UnqiedClothing> list = unqiedClothingService.selectUnqiedClothingList(unqiedClothing);
        ExcelUtil<UnqiedClothing> util = new ExcelUtil<UnqiedClothing>(UnqiedClothing.class);
        return util.exportExcel(list, "进口服装不合格统计数据");
    }

    /**
     * 导入进口服装不合格统计列表
     */
    @Log(title = "进口服装不合格统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedClothing:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<UnqiedClothing> util = new ExcelUtil<UnqiedClothing>(UnqiedClothing.class);
        List<UnqiedClothing> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = unqiedClothingService.importUnqiedClothing(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口服装不合格统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<UnqiedClothing> util = new ExcelUtil<UnqiedClothing>(UnqiedClothing.class);
        util.importTemplateExcel(response, "进口服装不合格统计数据");
    }

    /**
     * 获取进口服装不合格统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedClothing:query')")
    @GetMapping(value = "/{unqiedClothingId}")
    public AjaxResult getInfo(@PathVariable("unqiedClothingId") Long unqiedClothingId)
    {
        return AjaxResult.success(unqiedClothingService.selectUnqiedClothingByUnqiedClothingId(unqiedClothingId));
    }

    /**
     * 新增进口服装不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedClothing:add')")
    @Log(title = "进口服装不合格统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UnqiedClothing unqiedClothing)
    {
        return toAjax(unqiedClothingService.insertUnqiedClothing(unqiedClothing));
    }

    /**
     * 修改进口服装不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedClothing:edit')")
    @Log(title = "进口服装不合格统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UnqiedClothing unqiedClothing)
    {
        return toAjax(unqiedClothingService.updateUnqiedClothing(unqiedClothing));
    }

    /**
     * 删除进口服装不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedClothing:remove')")
    @Log(title = "进口服装不合格统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unqiedClothingIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedClothingIds)
    {
        return toAjax(unqiedClothingService.deleteUnqiedClothingByUnqiedClothingIds(unqiedClothingIds));
    }
}
