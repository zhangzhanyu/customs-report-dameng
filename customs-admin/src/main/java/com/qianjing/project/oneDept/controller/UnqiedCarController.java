package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.UnqiedCar;
import com.qianjing.project.oneDept.service.IUnqiedCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口汽车和摩托车不合格统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/unqiedCar")
public class UnqiedCarController extends BaseController
{
    @Autowired
    private IUnqiedCarService unqiedCarService;

    /**
     * 查询进口汽车和摩托车不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedCar:list')")
    @GetMapping("/list")
    public TableDataInfo list(UnqiedCar unqiedCar)
    {
        startPage();
        List<UnqiedCar> list = unqiedCarService.selectUnqiedCarList(unqiedCar);
        return getDataTable(list);
    }

    /**
     * 导出进口汽车和摩托车不合格统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedCar:export')")
    @Log(title = "进口汽车和摩托车不合格统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UnqiedCar unqiedCar)
    {
        List<UnqiedCar> list = unqiedCarService.selectUnqiedCarList(unqiedCar);
        ExcelUtil<UnqiedCar> util = new ExcelUtil<UnqiedCar>(UnqiedCar.class);
        return util.exportExcel(list, "进口汽车和摩托车不合格统计数据");
    }

    /**
     * 导入进口汽车和摩托车不合格统计列表
     */
    @Log(title = "进口汽车和摩托车不合格统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedCar:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<UnqiedCar> util = new ExcelUtil<UnqiedCar>(UnqiedCar.class);
        List<UnqiedCar> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = unqiedCarService.importUnqiedCar(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口汽车和摩托车不合格统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<UnqiedCar> util = new ExcelUtil<UnqiedCar>(UnqiedCar.class);
        util.importTemplateExcel(response, "进口汽车和摩托车不合格统计数据");
    }

    /**
     * 获取进口汽车和摩托车不合格统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedCar:query')")
    @GetMapping(value = "/{unqiedCarId}")
    public AjaxResult getInfo(@PathVariable("unqiedCarId") Long unqiedCarId)
    {
        return AjaxResult.success(unqiedCarService.selectUnqiedCarByUnqiedCarId(unqiedCarId));
    }

    /**
     * 新增进口汽车和摩托车不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedCar:add')")
    @Log(title = "进口汽车和摩托车不合格统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UnqiedCar unqiedCar)
    {
        return toAjax(unqiedCarService.insertUnqiedCar(unqiedCar));
    }

    /**
     * 修改进口汽车和摩托车不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedCar:edit')")
    @Log(title = "进口汽车和摩托车不合格统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UnqiedCar unqiedCar)
    {
        return toAjax(unqiedCarService.updateUnqiedCar(unqiedCar));
    }

    /**
     * 删除进口汽车和摩托车不合格统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:unqiedCar:remove')")
    @Log(title = "进口汽车和摩托车不合格统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unqiedCarIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedCarIds)
    {
        return toAjax(unqiedCarService.deleteUnqiedCarByUnqiedCarIds(unqiedCarIds));
    }
}
