package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.SusionChildrenSupplies;
import com.qianjing.project.oneDept.service.ISusionChildrenSuppliesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口儿童用品监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/susionSupplies")
public class SusionChildrenSuppliesController extends BaseController
{
    @Autowired
    private ISusionChildrenSuppliesService susionChildrenSuppliesService;

    /**
     * 查询进口儿童用品监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionSupplies:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionChildrenSupplies susionChildrenSupplies)
    {
        startPage();
        List<SusionChildrenSupplies> list = susionChildrenSuppliesService.selectSusionChildrenSuppliesList(susionChildrenSupplies);
        return getDataTable(list);
    }

    /**
     * 导出进口儿童用品监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionSupplies:export')")
    @Log(title = "进口儿童用品监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionChildrenSupplies susionChildrenSupplies)
    {
        List<SusionChildrenSupplies> list = susionChildrenSuppliesService.selectSusionChildrenSuppliesList(susionChildrenSupplies);
        ExcelUtil<SusionChildrenSupplies> util = new ExcelUtil<SusionChildrenSupplies>(SusionChildrenSupplies.class);
        return util.exportExcel(list, "进口儿童用品监管情况统计数据");
    }

    /**
     * 导入进口儿童用品监管情况统计列表
     */
    @Log(title = "进口儿童用品监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:susionSupplies:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionChildrenSupplies> util = new ExcelUtil<SusionChildrenSupplies>(SusionChildrenSupplies.class);
        List<SusionChildrenSupplies> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionChildrenSuppliesService.importSusionChildrenSupplies(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口儿童用品监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionChildrenSupplies> util = new ExcelUtil<SusionChildrenSupplies>(SusionChildrenSupplies.class);
        util.importTemplateExcel(response, "进口儿童用品监管情况统计数据");
    }

    /**
     * 获取进口儿童用品监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionSupplies:query')")
    @GetMapping(value = "/{susionChildrenSuppliesId}")
    public AjaxResult getInfo(@PathVariable("susionChildrenSuppliesId") Long susionChildrenSuppliesId)
    {
        return AjaxResult.success(susionChildrenSuppliesService.selectSusionChildrenSuppliesBySusionChildrenSuppliesId(susionChildrenSuppliesId));
    }

    /**
     * 新增进口儿童用品监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionSupplies:add')")
    @Log(title = "进口儿童用品监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionChildrenSupplies susionChildrenSupplies)
    {
        return toAjax(susionChildrenSuppliesService.insertSusionChildrenSupplies(susionChildrenSupplies));
    }

    /**
     * 修改进口儿童用品监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionSupplies:edit')")
    @Log(title = "进口儿童用品监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionChildrenSupplies susionChildrenSupplies)
    {
        return toAjax(susionChildrenSuppliesService.updateSusionChildrenSupplies(susionChildrenSupplies));
    }

    /**
     * 删除进口儿童用品监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionSupplies:remove')")
    @Log(title = "进口儿童用品监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionChildrenSuppliesIds}")
    public AjaxResult remove(@PathVariable Long[] susionChildrenSuppliesIds)
    {
        return toAjax(susionChildrenSuppliesService.deleteSusionChildrenSuppliesBySusionChildrenSuppliesIds(susionChildrenSuppliesIds));
    }
}
