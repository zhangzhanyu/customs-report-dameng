package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedHygienic;

/**
 * 进口一次性使用卫生用品不合格统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface UnqiedHygienicMapper 
{
    /**
     * 查询进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienicId 进口一次性使用卫生用品不合格统计主键
     * @return 进口一次性使用卫生用品不合格统计
     */
    public UnqiedHygienic selectUnqiedHygienicByUnqiedHygienicId(Long unqiedHygienicId);

    /**
     * 查询进口一次性使用卫生用品不合格统计列表
     * 
     * @param unqiedHygienic 进口一次性使用卫生用品不合格统计
     * @return 进口一次性使用卫生用品不合格统计集合
     */
    public List<UnqiedHygienic> selectUnqiedHygienicList(UnqiedHygienic unqiedHygienic);

    /**
     * 新增进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienic 进口一次性使用卫生用品不合格统计
     * @return 结果
     */
    public int insertUnqiedHygienic(UnqiedHygienic unqiedHygienic);

    /**
     * 修改进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienic 进口一次性使用卫生用品不合格统计
     * @return 结果
     */
    public int updateUnqiedHygienic(UnqiedHygienic unqiedHygienic);

    /**
     * 删除进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienicId 进口一次性使用卫生用品不合格统计主键
     * @return 结果
     */
    public int deleteUnqiedHygienicByUnqiedHygienicId(Long unqiedHygienicId);

    /**
     * 批量删除进口一次性使用卫生用品不合格统计
     * 
     * @param unqiedHygienicIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUnqiedHygienicByUnqiedHygienicIds(Long[] unqiedHygienicIds);
}
