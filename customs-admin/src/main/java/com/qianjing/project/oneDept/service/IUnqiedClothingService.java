package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedClothing;

/**
 * 进口服装不合格统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface IUnqiedClothingService 
{
    /**
     * 查询进口服装不合格统计
     * 
     * @param unqiedClothingId 进口服装不合格统计主键
     * @return 进口服装不合格统计
     */
    public UnqiedClothing selectUnqiedClothingByUnqiedClothingId(Long unqiedClothingId);

    /**
     * 查询进口服装不合格统计列表
     * 
     * @param unqiedClothing 进口服装不合格统计
     * @return 进口服装不合格统计集合
     */
    public List<UnqiedClothing> selectUnqiedClothingList(UnqiedClothing unqiedClothing);

    /**
     * 新增进口服装不合格统计
     * 
     * @param unqiedClothing 进口服装不合格统计
     * @return 结果
     */
    public int insertUnqiedClothing(UnqiedClothing unqiedClothing);

    /**
     * 修改进口服装不合格统计
     * 
     * @param unqiedClothing 进口服装不合格统计
     * @return 结果
     */
    public int updateUnqiedClothing(UnqiedClothing unqiedClothing);

    /**
     * 批量删除进口服装不合格统计
     * 
     * @param unqiedClothingIds 需要删除的进口服装不合格统计主键集合
     * @return 结果
     */
    public int deleteUnqiedClothingByUnqiedClothingIds(Long[] unqiedClothingIds);

    /**
     * 删除进口服装不合格统计信息
     * 
     * @param unqiedClothingId 进口服装不合格统计主键
     * @return 结果
     */
    public int deleteUnqiedClothingByUnqiedClothingId(Long unqiedClothingId);

    /**
     * 导入进口服装不合格统计信息
     *
     * @param unqiedClothingList 进口服装不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUnqiedClothing(List<UnqiedClothing> unqiedClothingList, Boolean isUpdateSupport, String operName);
}
