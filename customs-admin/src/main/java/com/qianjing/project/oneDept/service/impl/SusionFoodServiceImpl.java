package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.SusionFoodMapper;
import com.qianjing.project.oneDept.domain.SusionFood;
import com.qianjing.project.oneDept.service.ISusionFoodService;

/**
 * 进口食品接触产品检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class SusionFoodServiceImpl implements ISusionFoodService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionFoodServiceImpl.class);

    @Autowired
    private SusionFoodMapper susionFoodMapper;

    /**
     * 查询进口食品接触产品检验监管情况统计
     * 
     * @param susionFoodId 进口食品接触产品检验监管情况统计主键
     * @return 进口食品接触产品检验监管情况统计
     */
    @Override
    public SusionFood selectSusionFoodBySusionFoodId(Long susionFoodId)
    {
        return susionFoodMapper.selectSusionFoodBySusionFoodId(susionFoodId);
    }

    /**
     * 查询进口食品接触产品检验监管情况统计列表
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 进口食品接触产品检验监管情况统计
     */
    @Override
    public List<SusionFood> selectSusionFoodList(SusionFood susionFood)
    {
        return susionFoodMapper.selectSusionFoodList(susionFood);
    }

    /**
     * 新增进口食品接触产品检验监管情况统计
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionFood(SusionFood susionFood)
    {
        return susionFoodMapper.insertSusionFood(susionFood);
    }

    /**
     * 修改进口食品接触产品检验监管情况统计
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionFood(SusionFood susionFood)
    {
        return susionFoodMapper.updateSusionFood(susionFood);
    }

    /**
     * 批量删除进口食品接触产品检验监管情况统计
     * 
     * @param susionFoodIds 需要删除的进口食品接触产品检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionFoodBySusionFoodIds(Long[] susionFoodIds)
    {
        return susionFoodMapper.deleteSusionFoodBySusionFoodIds(susionFoodIds);
    }

    /**
     * 删除进口食品接触产品检验监管情况统计信息
     * 
     * @param susionFoodId 进口食品接触产品检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionFoodBySusionFoodId(Long susionFoodId)
    {
        return susionFoodMapper.deleteSusionFoodBySusionFoodId(susionFoodId);
    }

    /**
     * 导入进口食品接触产品检验监管情况统计数据
     *
     * @param susionFoodList susionFoodList 进口食品接触产品检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionFood(List<SusionFood> susionFoodList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionFoodList) || susionFoodList.size() == 0) {
            throw new ServiceException("导入进口食品接触产品检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionFood susionFood : susionFoodList){
            try {
                // 验证是否存在
                if (true) {
                    susionFood.setCreateBy(operName);
                    this.insertSusionFood(susionFood);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionFood.setUpdateBy(operName);
                    this.updateSusionFood(susionFood);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
