package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionHygienic;

/**
 * 进口一次性使用卫生用品检验监管情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface ISusionHygienicService 
{
    /**
     * 查询进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienicId 进口一次性使用卫生用品检验监管情况统计主键
     * @return 进口一次性使用卫生用品检验监管情况统计
     */
    public SusionHygienic selectSusionHygienicBySusionHygienicId(Long susionHygienicId);

    /**
     * 查询进口一次性使用卫生用品检验监管情况统计列表
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 进口一次性使用卫生用品检验监管情况统计集合
     */
    public List<SusionHygienic> selectSusionHygienicList(SusionHygienic susionHygienic);

    /**
     * 新增进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 结果
     */
    public int insertSusionHygienic(SusionHygienic susionHygienic);

    /**
     * 修改进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienic 进口一次性使用卫生用品检验监管情况统计
     * @return 结果
     */
    public int updateSusionHygienic(SusionHygienic susionHygienic);

    /**
     * 批量删除进口一次性使用卫生用品检验监管情况统计
     * 
     * @param susionHygienicIds 需要删除的进口一次性使用卫生用品检验监管情况统计主键集合
     * @return 结果
     */
    public int deleteSusionHygienicBySusionHygienicIds(Long[] susionHygienicIds);

    /**
     * 删除进口一次性使用卫生用品检验监管情况统计信息
     * 
     * @param susionHygienicId 进口一次性使用卫生用品检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionHygienicBySusionHygienicId(Long susionHygienicId);

    /**
     * 导入进口一次性使用卫生用品检验监管情况统计信息
     *
     * @param susionHygienicList 进口一次性使用卫生用品检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionHygienic(List<SusionHygienic> susionHygienicList, Boolean isUpdateSupport, String operName);
}
