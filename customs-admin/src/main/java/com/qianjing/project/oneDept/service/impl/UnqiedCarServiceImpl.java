package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.UnqiedCarMapper;
import com.qianjing.project.oneDept.domain.UnqiedCar;
import com.qianjing.project.oneDept.service.IUnqiedCarService;

/**
 * 进口汽车和摩托车不合格统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class UnqiedCarServiceImpl implements IUnqiedCarService 
{
    private static final Logger log = LoggerFactory.getLogger(UnqiedCarServiceImpl.class);

    @Autowired
    private UnqiedCarMapper unqiedCarMapper;

    /**
     * 查询进口汽车和摩托车不合格统计
     * 
     * @param unqiedCarId 进口汽车和摩托车不合格统计主键
     * @return 进口汽车和摩托车不合格统计
     */
    @Override
    public UnqiedCar selectUnqiedCarByUnqiedCarId(Long unqiedCarId)
    {
        return unqiedCarMapper.selectUnqiedCarByUnqiedCarId(unqiedCarId);
    }

    /**
     * 查询进口汽车和摩托车不合格统计列表
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 进口汽车和摩托车不合格统计
     */
    @Override
    public List<UnqiedCar> selectUnqiedCarList(UnqiedCar unqiedCar)
    {
        return unqiedCarMapper.selectUnqiedCarList(unqiedCar);
    }

    /**
     * 新增进口汽车和摩托车不合格统计
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 结果
     */
    @Override
    public int insertUnqiedCar(UnqiedCar unqiedCar)
    {
        unqiedCar.setCreateTime(DateUtils.getNowDate());
        return unqiedCarMapper.insertUnqiedCar(unqiedCar);
    }

    /**
     * 修改进口汽车和摩托车不合格统计
     * 
     * @param unqiedCar 进口汽车和摩托车不合格统计
     * @return 结果
     */
    @Override
    public int updateUnqiedCar(UnqiedCar unqiedCar)
    {
        unqiedCar.setUpdateTime(DateUtils.getNowDate());
        return unqiedCarMapper.updateUnqiedCar(unqiedCar);
    }

    /**
     * 批量删除进口汽车和摩托车不合格统计
     * 
     * @param unqiedCarIds 需要删除的进口汽车和摩托车不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedCarByUnqiedCarIds(Long[] unqiedCarIds)
    {
        return unqiedCarMapper.deleteUnqiedCarByUnqiedCarIds(unqiedCarIds);
    }

    /**
     * 删除进口汽车和摩托车不合格统计信息
     * 
     * @param unqiedCarId 进口汽车和摩托车不合格统计主键
     * @return 结果
     */
    @Override
    public int deleteUnqiedCarByUnqiedCarId(Long unqiedCarId)
    {
        return unqiedCarMapper.deleteUnqiedCarByUnqiedCarId(unqiedCarId);
    }

    /**
     * 导入进口汽车和摩托车不合格统计数据
     *
     * @param unqiedCarList unqiedCarList 进口汽车和摩托车不合格统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUnqiedCar(List<UnqiedCar> unqiedCarList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(unqiedCarList) || unqiedCarList.size() == 0) {
            throw new ServiceException("导入进口汽车和摩托车不合格统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (UnqiedCar unqiedCar : unqiedCarList){
            try {
                // 验证是否存在
                if (true) {
                    unqiedCar.setCreateBy(operName);
                    this.insertUnqiedCar(unqiedCar);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    unqiedCar.setUpdateBy(operName);
                    this.updateUnqiedCar(unqiedCar);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
