package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionElectric;

/**
 * 进口旧机电检验监管情况Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface SusionElectricMapper 
{
    /**
     * 查询进口旧机电检验监管情况
     * 
     * @param susionElectricId 进口旧机电检验监管情况主键
     * @return 进口旧机电检验监管情况
     */
    public SusionElectric selectSusionElectricBySusionElectricId(Long susionElectricId);

    /**
     * 查询进口旧机电检验监管情况列表
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 进口旧机电检验监管情况集合
     */
    public List<SusionElectric> selectSusionElectricList(SusionElectric susionElectric);

    /**
     * 新增进口旧机电检验监管情况
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 结果
     */
    public int insertSusionElectric(SusionElectric susionElectric);

    /**
     * 修改进口旧机电检验监管情况
     * 
     * @param susionElectric 进口旧机电检验监管情况
     * @return 结果
     */
    public int updateSusionElectric(SusionElectric susionElectric);

    /**
     * 删除进口旧机电检验监管情况
     * 
     * @param susionElectricId 进口旧机电检验监管情况主键
     * @return 结果
     */
    public int deleteSusionElectricBySusionElectricId(Long susionElectricId);

    /**
     * 批量删除进口旧机电检验监管情况
     * 
     * @param susionElectricIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionElectricBySusionElectricIds(Long[] susionElectricIds);
}
