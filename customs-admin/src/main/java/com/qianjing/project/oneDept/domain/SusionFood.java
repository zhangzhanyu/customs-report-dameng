package com.qianjing.project.oneDept.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口食品接触产品检验监管情况统计对象 susion_food
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public class SusionFood extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long susionFoodId;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String goodsCategory;

    /** 进口批次(进口总体情况) */
    @Excel(name = "进口批次(进口总体情况)")
    private String totalBatch;

    /** 进口数量(进口总体情况) */
    @Excel(name = "进口数量(进口总体情况)")
    private String totalQuantity;

    /** 进口金额(进口总体情况) */
    @Excel(name = "进口金额(进口总体情况)")
    private String totalAmount;

    /** 批次(涉检布控查验情况) */
    @Excel(name = "批次(涉检布控查验情况)")
    private String involveBatch;

    /** 数量(涉检布控查验情况) */
    @Excel(name = "数量(涉检布控查验情况)")
    private String involveQuantity;

    /** 金额(涉检布控查验情况) */
    @Excel(name = "金额(涉检布控查验情况)")
    private String involveAmount;

    /** 不合格批次(涉检布控查验情况) */
    @Excel(name = "不合格批次(涉检布控查验情况)")
    private String involveInvolveBatch;

    /** 不合格数量(涉检布控查验情况) */
    @Excel(name = "不合格数量(涉检布控查验情况)")
    private String involveInvolveQuantity;

    /** 不合格金额(涉检布控查验情况) */
    @Excel(name = "不合格金额(涉检布控查验情况)")
    private String involveInvolveAmount;

    /** 批次(布控送检情况) */
    @Excel(name = "批次(布控送检情况)")
    private String submitBatch;

    /** 数量(布控送检情况) */
    @Excel(name = "数量(布控送检情况)")
    private String submitQuantity;

    /** 金额(布控送检情况) */
    @Excel(name = "金额(布控送检情况)")
    private String submitAmount;

    /** 不合格批次(布控送检情况) */
    @Excel(name = "不合格批次(布控送检情况)")
    private String submitInvolveBatch;

    /** 不合格数量(布控送检情况) */
    @Excel(name = "不合格数量(布控送检情况)")
    private String submitInvolveQuantity;

    /** 不合格金额(布控送检情况) */
    @Excel(name = "不合格金额(布控送检情况)")
    private String submitInvolveAmount;

    public void setSusionFoodId(Long susionFoodId) 
    {
        this.susionFoodId = susionFoodId;
    }

    public Long getSusionFoodId() 
    {
        return susionFoodId;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setTotalBatch(String totalBatch) 
    {
        this.totalBatch = totalBatch;
    }

    public String getTotalBatch() 
    {
        return totalBatch;
    }
    public void setTotalQuantity(String totalQuantity) 
    {
        this.totalQuantity = totalQuantity;
    }

    public String getTotalQuantity() 
    {
        return totalQuantity;
    }
    public void setTotalAmount(String totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() 
    {
        return totalAmount;
    }
    public void setInvolveBatch(String involveBatch) 
    {
        this.involveBatch = involveBatch;
    }

    public String getInvolveBatch() 
    {
        return involveBatch;
    }
    public void setInvolveQuantity(String involveQuantity) 
    {
        this.involveQuantity = involveQuantity;
    }

    public String getInvolveQuantity() 
    {
        return involveQuantity;
    }
    public void setInvolveAmount(String involveAmount) 
    {
        this.involveAmount = involveAmount;
    }

    public String getInvolveAmount() 
    {
        return involveAmount;
    }
    public void setInvolveInvolveBatch(String involveInvolveBatch) 
    {
        this.involveInvolveBatch = involveInvolveBatch;
    }

    public String getInvolveInvolveBatch() 
    {
        return involveInvolveBatch;
    }
    public void setInvolveInvolveQuantity(String involveInvolveQuantity) 
    {
        this.involveInvolveQuantity = involveInvolveQuantity;
    }

    public String getInvolveInvolveQuantity() 
    {
        return involveInvolveQuantity;
    }
    public void setInvolveInvolveAmount(String involveInvolveAmount) 
    {
        this.involveInvolveAmount = involveInvolveAmount;
    }

    public String getInvolveInvolveAmount() 
    {
        return involveInvolveAmount;
    }
    public void setSubmitBatch(String submitBatch) 
    {
        this.submitBatch = submitBatch;
    }

    public String getSubmitBatch() 
    {
        return submitBatch;
    }
    public void setSubmitQuantity(String submitQuantity) 
    {
        this.submitQuantity = submitQuantity;
    }

    public String getSubmitQuantity() 
    {
        return submitQuantity;
    }
    public void setSubmitAmount(String submitAmount) 
    {
        this.submitAmount = submitAmount;
    }

    public String getSubmitAmount() 
    {
        return submitAmount;
    }
    public void setSubmitInvolveBatch(String submitInvolveBatch) 
    {
        this.submitInvolveBatch = submitInvolveBatch;
    }

    public String getSubmitInvolveBatch() 
    {
        return submitInvolveBatch;
    }
    public void setSubmitInvolveQuantity(String submitInvolveQuantity) 
    {
        this.submitInvolveQuantity = submitInvolveQuantity;
    }

    public String getSubmitInvolveQuantity() 
    {
        return submitInvolveQuantity;
    }
    public void setSubmitInvolveAmount(String submitInvolveAmount) 
    {
        this.submitInvolveAmount = submitInvolveAmount;
    }

    public String getSubmitInvolveAmount() 
    {
        return submitInvolveAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("susionFoodId", getSusionFoodId())
            .append("goodsCategory", getGoodsCategory())
            .append("totalBatch", getTotalBatch())
            .append("totalQuantity", getTotalQuantity())
            .append("totalAmount", getTotalAmount())
            .append("involveBatch", getInvolveBatch())
            .append("involveQuantity", getInvolveQuantity())
            .append("involveAmount", getInvolveAmount())
            .append("involveInvolveBatch", getInvolveInvolveBatch())
            .append("involveInvolveQuantity", getInvolveInvolveQuantity())
            .append("involveInvolveAmount", getInvolveInvolveAmount())
            .append("submitBatch", getSubmitBatch())
            .append("submitQuantity", getSubmitQuantity())
            .append("submitAmount", getSubmitAmount())
            .append("submitInvolveBatch", getSubmitInvolveBatch())
            .append("submitInvolveQuantity", getSubmitInvolveQuantity())
            .append("submitInvolveAmount", getSubmitInvolveAmount())
            .toString();
    }
}
