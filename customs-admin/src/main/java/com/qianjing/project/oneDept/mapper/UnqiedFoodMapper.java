package com.qianjing.project.oneDept.mapper;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedFood;

/**
 * 进口食品接触产品不合格统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface UnqiedFoodMapper 
{
    /**
     * 查询进口食品接触产品不合格统计
     * 
     * @param unqiedFoodId 进口食品接触产品不合格统计主键
     * @return 进口食品接触产品不合格统计
     */
    public UnqiedFood selectUnqiedFoodByUnqiedFoodId(Long unqiedFoodId);

    /**
     * 查询进口食品接触产品不合格统计列表
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 进口食品接触产品不合格统计集合
     */
    public List<UnqiedFood> selectUnqiedFoodList(UnqiedFood unqiedFood);

    /**
     * 新增进口食品接触产品不合格统计
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 结果
     */
    public int insertUnqiedFood(UnqiedFood unqiedFood);

    /**
     * 修改进口食品接触产品不合格统计
     * 
     * @param unqiedFood 进口食品接触产品不合格统计
     * @return 结果
     */
    public int updateUnqiedFood(UnqiedFood unqiedFood);

    /**
     * 删除进口食品接触产品不合格统计
     * 
     * @param unqiedFoodId 进口食品接触产品不合格统计主键
     * @return 结果
     */
    public int deleteUnqiedFoodByUnqiedFoodId(Long unqiedFoodId);

    /**
     * 批量删除进口食品接触产品不合格统计
     * 
     * @param unqiedFoodIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUnqiedFoodByUnqiedFoodIds(Long[] unqiedFoodIds);
}
