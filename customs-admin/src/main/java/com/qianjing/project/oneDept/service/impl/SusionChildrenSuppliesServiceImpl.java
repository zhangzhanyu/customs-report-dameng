package com.qianjing.project.oneDept.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.oneDept.mapper.SusionChildrenSuppliesMapper;
import com.qianjing.project.oneDept.domain.SusionChildrenSupplies;
import com.qianjing.project.oneDept.service.ISusionChildrenSuppliesService;

/**
 * 进口儿童用品监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@Service
public class SusionChildrenSuppliesServiceImpl implements ISusionChildrenSuppliesService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionChildrenSuppliesServiceImpl.class);

    @Autowired
    private SusionChildrenSuppliesMapper susionChildrenSuppliesMapper;

    /**
     * 查询进口儿童用品监管情况统计
     * 
     * @param susionChildrenSuppliesId 进口儿童用品监管情况统计主键
     * @return 进口儿童用品监管情况统计
     */
    @Override
    public SusionChildrenSupplies selectSusionChildrenSuppliesBySusionChildrenSuppliesId(Long susionChildrenSuppliesId)
    {
        return susionChildrenSuppliesMapper.selectSusionChildrenSuppliesBySusionChildrenSuppliesId(susionChildrenSuppliesId);
    }

    /**
     * 查询进口儿童用品监管情况统计列表
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 进口儿童用品监管情况统计
     */
    @Override
    public List<SusionChildrenSupplies> selectSusionChildrenSuppliesList(SusionChildrenSupplies susionChildrenSupplies)
    {
        return susionChildrenSuppliesMapper.selectSusionChildrenSuppliesList(susionChildrenSupplies);
    }

    /**
     * 新增进口儿童用品监管情况统计
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionChildrenSupplies(SusionChildrenSupplies susionChildrenSupplies)
    {
        return susionChildrenSuppliesMapper.insertSusionChildrenSupplies(susionChildrenSupplies);
    }

    /**
     * 修改进口儿童用品监管情况统计
     * 
     * @param susionChildrenSupplies 进口儿童用品监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionChildrenSupplies(SusionChildrenSupplies susionChildrenSupplies)
    {
        return susionChildrenSuppliesMapper.updateSusionChildrenSupplies(susionChildrenSupplies);
    }

    /**
     * 批量删除进口儿童用品监管情况统计
     * 
     * @param susionChildrenSuppliesIds 需要删除的进口儿童用品监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionChildrenSuppliesBySusionChildrenSuppliesIds(Long[] susionChildrenSuppliesIds)
    {
        return susionChildrenSuppliesMapper.deleteSusionChildrenSuppliesBySusionChildrenSuppliesIds(susionChildrenSuppliesIds);
    }

    /**
     * 删除进口儿童用品监管情况统计信息
     * 
     * @param susionChildrenSuppliesId 进口儿童用品监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionChildrenSuppliesBySusionChildrenSuppliesId(Long susionChildrenSuppliesId)
    {
        return susionChildrenSuppliesMapper.deleteSusionChildrenSuppliesBySusionChildrenSuppliesId(susionChildrenSuppliesId);
    }

    /**
     * 导入进口儿童用品监管情况统计数据
     *
     * @param susionChildrenSuppliesList susionChildrenSuppliesList 进口儿童用品监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionChildrenSupplies(List<SusionChildrenSupplies> susionChildrenSuppliesList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionChildrenSuppliesList) || susionChildrenSuppliesList.size() == 0) {
            throw new ServiceException("导入进口儿童用品监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionChildrenSupplies susionChildrenSupplies : susionChildrenSuppliesList){
            try {
                // 验证是否存在
                if (true) {
                    susionChildrenSupplies.setCreateBy(operName);
                    this.insertSusionChildrenSupplies(susionChildrenSupplies);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionChildrenSupplies.setUpdateBy(operName);
                    this.updateSusionChildrenSupplies(susionChildrenSupplies);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
