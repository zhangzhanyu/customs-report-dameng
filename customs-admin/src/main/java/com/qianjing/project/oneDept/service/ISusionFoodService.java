package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.SusionFood;

/**
 * 进口食品接触产品检验监管情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface ISusionFoodService 
{
    /**
     * 查询进口食品接触产品检验监管情况统计
     * 
     * @param susionFoodId 进口食品接触产品检验监管情况统计主键
     * @return 进口食品接触产品检验监管情况统计
     */
    public SusionFood selectSusionFoodBySusionFoodId(Long susionFoodId);

    /**
     * 查询进口食品接触产品检验监管情况统计列表
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 进口食品接触产品检验监管情况统计集合
     */
    public List<SusionFood> selectSusionFoodList(SusionFood susionFood);

    /**
     * 新增进口食品接触产品检验监管情况统计
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 结果
     */
    public int insertSusionFood(SusionFood susionFood);

    /**
     * 修改进口食品接触产品检验监管情况统计
     * 
     * @param susionFood 进口食品接触产品检验监管情况统计
     * @return 结果
     */
    public int updateSusionFood(SusionFood susionFood);

    /**
     * 批量删除进口食品接触产品检验监管情况统计
     * 
     * @param susionFoodIds 需要删除的进口食品接触产品检验监管情况统计主键集合
     * @return 结果
     */
    public int deleteSusionFoodBySusionFoodIds(Long[] susionFoodIds);

    /**
     * 删除进口食品接触产品检验监管情况统计信息
     * 
     * @param susionFoodId 进口食品接触产品检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionFoodBySusionFoodId(Long susionFoodId);

    /**
     * 导入进口食品接触产品检验监管情况统计信息
     *
     * @param susionFoodList 进口食品接触产品检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionFood(List<SusionFood> susionFoodList, Boolean isUpdateSupport, String operName);
}
