package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.SusionCar;
import com.qianjing.project.oneDept.service.ISusionCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口汽车和摩托车检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/susionCar")
public class SusionCarController extends BaseController
{
    @Autowired
    private ISusionCarService susionCarService;

    /**
     * 查询进口汽车和摩托车检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionCar:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionCar susionCar)
    {
        startPage();
        List<SusionCar> list = susionCarService.selectSusionCarList(susionCar);
        return getDataTable(list);
    }

    /**
     * 导出进口汽车和摩托车检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionCar:export')")
    @Log(title = "进口汽车和摩托车检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionCar susionCar)
    {
        List<SusionCar> list = susionCarService.selectSusionCarList(susionCar);
        ExcelUtil<SusionCar> util = new ExcelUtil<SusionCar>(SusionCar.class);
        return util.exportExcel(list, "进口汽车和摩托车检验监管情况统计数据");
    }

    /**
     * 导入进口汽车和摩托车检验监管情况统计列表
     */
    @Log(title = "进口汽车和摩托车检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:susionCar:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionCar> util = new ExcelUtil<SusionCar>(SusionCar.class);
        List<SusionCar> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionCarService.importSusionCar(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口汽车和摩托车检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionCar> util = new ExcelUtil<SusionCar>(SusionCar.class);
        util.importTemplateExcel(response, "进口汽车和摩托车检验监管情况统计数据");
    }

    /**
     * 获取进口汽车和摩托车检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionCar:query')")
    @GetMapping(value = "/{susionCarId}")
    public AjaxResult getInfo(@PathVariable("susionCarId") Long susionCarId)
    {
        return AjaxResult.success(susionCarService.selectSusionCarBySusionCarId(susionCarId));
    }

    /**
     * 新增进口汽车和摩托车检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionCar:add')")
    @Log(title = "进口汽车和摩托车检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionCar susionCar)
    {
        return toAjax(susionCarService.insertSusionCar(susionCar));
    }

    /**
     * 修改进口汽车和摩托车检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionCar:edit')")
    @Log(title = "进口汽车和摩托车检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionCar susionCar)
    {
        return toAjax(susionCarService.updateSusionCar(susionCar));
    }

    /**
     * 删除进口汽车和摩托车检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionCar:remove')")
    @Log(title = "进口汽车和摩托车检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionCarIds}")
    public AjaxResult remove(@PathVariable Long[] susionCarIds)
    {
        return toAjax(susionCarService.deleteSusionCarBySusionCarIds(susionCarIds));
    }
}
