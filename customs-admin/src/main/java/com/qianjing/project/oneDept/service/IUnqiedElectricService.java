package com.qianjing.project.oneDept.service;

import java.util.List;
import com.qianjing.project.oneDept.domain.UnqiedElectric;

/**
 * 进口旧机电检验监管不合格情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-11
 */
public interface IUnqiedElectricService 
{
    /**
     * 查询进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectricId 进口旧机电检验监管不合格情况统计主键
     * @return 进口旧机电检验监管不合格情况统计
     */
    public UnqiedElectric selectUnqiedElectricByUnqiedElectricId(Long unqiedElectricId);

    /**
     * 查询进口旧机电检验监管不合格情况统计列表
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 进口旧机电检验监管不合格情况统计集合
     */
    public List<UnqiedElectric> selectUnqiedElectricList(UnqiedElectric unqiedElectric);

    /**
     * 新增进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 结果
     */
    public int insertUnqiedElectric(UnqiedElectric unqiedElectric);

    /**
     * 修改进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectric 进口旧机电检验监管不合格情况统计
     * @return 结果
     */
    public int updateUnqiedElectric(UnqiedElectric unqiedElectric);

    /**
     * 批量删除进口旧机电检验监管不合格情况统计
     * 
     * @param unqiedElectricIds 需要删除的进口旧机电检验监管不合格情况统计主键集合
     * @return 结果
     */
    public int deleteUnqiedElectricByUnqiedElectricIds(Long[] unqiedElectricIds);

    /**
     * 删除进口旧机电检验监管不合格情况统计信息
     * 
     * @param unqiedElectricId 进口旧机电检验监管不合格情况统计主键
     * @return 结果
     */
    public int deleteUnqiedElectricByUnqiedElectricId(Long unqiedElectricId);

    /**
     * 导入进口旧机电检验监管不合格情况统计信息
     *
     * @param unqiedElectricList 进口旧机电检验监管不合格情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUnqiedElectric(List<UnqiedElectric> unqiedElectricList, Boolean isUpdateSupport, String operName);
}
