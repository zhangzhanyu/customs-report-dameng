package com.qianjing.project.oneDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.oneDept.domain.SusionElectric;
import com.qianjing.project.oneDept.service.ISusionElectricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口旧机电检验监管情况Controller
 * 
 * @author qianjing
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/oneDept/susionElectric")
public class SusionElectricController extends BaseController
{
    @Autowired
    private ISusionElectricService susionElectricService;

    /**
     * 查询进口旧机电检验监管情况列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionElectric:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionElectric susionElectric)
    {
        startPage();
        List<SusionElectric> list = susionElectricService.selectSusionElectricList(susionElectric);
        return getDataTable(list);
    }

    /**
     * 导出进口旧机电检验监管情况列表
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionElectric:export')")
    @Log(title = "进口旧机电检验监管情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionElectric susionElectric)
    {
        List<SusionElectric> list = susionElectricService.selectSusionElectricList(susionElectric);
        ExcelUtil<SusionElectric> util = new ExcelUtil<SusionElectric>(SusionElectric.class);
        return util.exportExcel(list, "进口旧机电检验监管情况数据");
    }

    /**
     * 导入进口旧机电检验监管情况列表
     */
    @Log(title = "进口旧机电检验监管情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oneDept:susionElectric:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionElectric> util = new ExcelUtil<SusionElectric>(SusionElectric.class);
        List<SusionElectric> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionElectricService.importSusionElectric(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口旧机电检验监管情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionElectric> util = new ExcelUtil<SusionElectric>(SusionElectric.class);
        util.importTemplateExcel(response, "进口旧机电检验监管情况数据");
    }

    /**
     * 获取进口旧机电检验监管情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionElectric:query')")
    @GetMapping(value = "/{susionElectricId}")
    public AjaxResult getInfo(@PathVariable("susionElectricId") Long susionElectricId)
    {
        return AjaxResult.success(susionElectricService.selectSusionElectricBySusionElectricId(susionElectricId));
    }

    /**
     * 新增进口旧机电检验监管情况
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionElectric:add')")
    @Log(title = "进口旧机电检验监管情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionElectric susionElectric)
    {
        return toAjax(susionElectricService.insertSusionElectric(susionElectric));
    }

    /**
     * 修改进口旧机电检验监管情况
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionElectric:edit')")
    @Log(title = "进口旧机电检验监管情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionElectric susionElectric)
    {
        return toAjax(susionElectricService.updateSusionElectric(susionElectric));
    }

    /**
     * 删除进口旧机电检验监管情况
     */
    @PreAuthorize("@ss.hasPermi('oneDept:susionElectric:remove')")
    @Log(title = "进口旧机电检验监管情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionElectricIds}")
    public AjaxResult remove(@PathVariable Long[] susionElectricIds)
    {
        return toAjax(susionElectricService.deleteSusionElectricBySusionElectricIds(susionElectricIds));
    }
}
