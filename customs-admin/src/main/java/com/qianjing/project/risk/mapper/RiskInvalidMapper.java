package com.qianjing.project.risk.mapper;

import java.util.List;
import com.qianjing.project.risk.domain.RiskInvalid;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 检验不合格情况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface RiskInvalidMapper {

    RiskInvalid selectRiskInvalidById(Long id);

    List<RiskInvalid> selectRiskInvalidList(RiskInvalid riskInvalid);

    int insertRiskInvalid(RiskInvalid riskInvalid);

    int updateRiskInvalid(RiskInvalid riskInvalid);

    int deleteRiskInvalidById(Long id);

    int deleteRiskInvalidByIds(Long[] ids);
}
