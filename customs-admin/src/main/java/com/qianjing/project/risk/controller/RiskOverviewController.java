package com.qianjing.project.risk.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.risk.domain.RiskOverview;
import com.qianjing.project.risk.service.IRiskOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 风险预警监管体系概况Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/risk/overview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskOverviewController extends BaseController {

    private final IRiskOverviewService riskOverviewService;

    /**
     * 查询风险预警监管体系概况列表
     */
//    @PreAuthorize("@ss.hasPermi('risk:overview:list')")
    @GetMapping("/list")
    public TableDataInfo list(RiskOverview riskOverview) {
        startPage();
        List<RiskOverview> list = riskOverviewService.selectRiskOverviewList(riskOverview);
        return getDataTable(list);
    }

    /**
     * 导出风险预警监管体系概况列表
     */
    @PreAuthorize("@ss.hasPermi('risk:overview:export')")
    @Log(title = "风险预警监管体系概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RiskOverview riskOverview) {
        List<RiskOverview> list = riskOverviewService.selectRiskOverviewList(riskOverview);
        ExcelUtil<RiskOverview> util = new ExcelUtil<RiskOverview>(RiskOverview.class);
        return util.exportExcel(list, "风险预警监管体系概况数据");
    }

    /**
     * 导入风险预警监管体系概况列表
     */
    @Log(title = "风险预警监管体系概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('risk:overview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<RiskOverview> util = new ExcelUtil<RiskOverview>(RiskOverview.class);
        List<RiskOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = riskOverviewService.importRiskOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载风险预警监管体系概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<RiskOverview> util = new ExcelUtil<RiskOverview>(RiskOverview.class);
        util.importTemplateExcel(response, "风险预警监管体系概况数据");
    }

    /**
     * 获取风险预警监管体系概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('risk:overview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(riskOverviewService.selectRiskOverviewById(id));
    }

    /**
     * 新增风险预警监管体系概况
     */
    @PreAuthorize("@ss.hasPermi('risk:overview:add')")
    @Log(title = "风险预警监管体系概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RiskOverview riskOverview) {
        return toAjax(riskOverviewService.insertRiskOverview(riskOverview));
    }

    /**
     * 修改风险预警监管体系概况
     */
    @PreAuthorize("@ss.hasPermi('risk:overview:edit')")
    @Log(title = "风险预警监管体系概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RiskOverview riskOverview) {
        return toAjax(riskOverviewService.updateRiskOverview(riskOverview));
    }

    /**
     * 删除风险预警监管体系概况
     */
    @PreAuthorize("@ss.hasPermi('risk:overview:remove')")
    @Log(title = "风险预警监管体系概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(riskOverviewService.deleteRiskOverviewByIds(ids));
    }
}
