package com.qianjing.project.risk.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.risk.mapper.RiskRandomCheckMapper;
import com.qianjing.project.risk.domain.RiskRandomCheck;
import com.qianjing.project.risk.service.IRiskRandomCheckService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 抽查检验情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskRandomCheckServiceImpl implements IRiskRandomCheckService {

    private static final Logger log = LoggerFactory.getLogger(RiskRandomCheckServiceImpl.class);

    private final RiskRandomCheckMapper riskRandomCheckMapper;

    /**
     * 查询抽查检验情况
     * 
     * @param id 抽查检验情况主键
     * @return 抽查检验情况
     */
    @Override
    public RiskRandomCheck selectRiskRandomCheckById(Long id) {
        return riskRandomCheckMapper.selectRiskRandomCheckById(id);
    }

    /**
     * 查询抽查检验情况列表
     * 
     * @param riskRandomCheck 抽查检验情况
     * @return 抽查检验情况
     */
    @Override
    public List<RiskRandomCheck> selectRiskRandomCheckList(RiskRandomCheck riskRandomCheck) {
        return riskRandomCheckMapper.selectRiskRandomCheckList(riskRandomCheck);
    }

    /**
     * 新增抽查检验情况
     * 
     * @param riskRandomCheck 抽查检验情况
     * @return 结果
     */
    @Override
    public int insertRiskRandomCheck(RiskRandomCheck riskRandomCheck) {
        riskRandomCheck.setCreateTime(DateUtils.getNowDate());
        return riskRandomCheckMapper.insertRiskRandomCheck(riskRandomCheck);
    }

    /**
     * 修改抽查检验情况
     * 
     * @param riskRandomCheck 抽查检验情况
     * @return 结果
     */
    @Override
    public int updateRiskRandomCheck(RiskRandomCheck riskRandomCheck) {
        riskRandomCheck.setUpdateTime(DateUtils.getNowDate());
        return riskRandomCheckMapper.updateRiskRandomCheck(riskRandomCheck);
    }

    /**
     * 批量删除抽查检验情况
     * 
     * @param ids 需要删除的抽查检验情况主键
     * @return 结果
     */
    @Override
    public int deleteRiskRandomCheckByIds(Long[] ids) {
        return riskRandomCheckMapper.deleteRiskRandomCheckByIds(ids);
    }

    /**
     * 删除抽查检验情况信息
     * 
     * @param id 抽查检验情况主键
     * @return 结果
     */
    @Override
    public int deleteRiskRandomCheckById(Long id) {
        return riskRandomCheckMapper.deleteRiskRandomCheckById(id);
    }

    /**
     * 导入抽查检验情况数据
     *
     * @param riskRandomCheckList riskRandomCheckList 抽查检验情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importRiskRandomCheck(List<RiskRandomCheck> riskRandomCheckList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(riskRandomCheckList) || riskRandomCheckList.size() == 0) {
            throw new ServiceException("导入抽查检验情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (RiskRandomCheck riskRandomCheck : riskRandomCheckList){
            try {
                // 验证是否存在
                if (true) {
                    riskRandomCheck.setCreateBy(operName);
                    this.insertRiskRandomCheck(riskRandomCheck);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    riskRandomCheck.setUpdateBy(operName);
                    this.updateRiskRandomCheck(riskRandomCheck);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
