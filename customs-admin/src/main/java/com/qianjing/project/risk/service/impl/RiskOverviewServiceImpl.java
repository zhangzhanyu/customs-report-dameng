package com.qianjing.project.risk.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.risk.mapper.RiskOverviewMapper;
import com.qianjing.project.risk.domain.RiskOverview;
import com.qianjing.project.risk.service.IRiskOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 风险预警监管体系概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskOverviewServiceImpl implements IRiskOverviewService {

    private static final Logger log = LoggerFactory.getLogger(RiskOverviewServiceImpl.class);

    private final RiskOverviewMapper riskOverviewMapper;

    /**
     * 查询风险预警监管体系概况
     * 
     * @param id 风险预警监管体系概况主键
     * @return 风险预警监管体系概况
     */
    @Override
    public RiskOverview selectRiskOverviewById(Long id) {
        return riskOverviewMapper.selectRiskOverviewById(id);
    }

    /**
     * 查询风险预警监管体系概况列表
     * 
     * @param riskOverview 风险预警监管体系概况
     * @return 风险预警监管体系概况
     */
    @Override
    public List<RiskOverview> selectRiskOverviewList(RiskOverview riskOverview) {
        return riskOverviewMapper.selectRiskOverviewList(riskOverview);
    }

    /**
     * 新增风险预警监管体系概况
     * 
     * @param riskOverview 风险预警监管体系概况
     * @return 结果
     */
    @Override
    public int insertRiskOverview(RiskOverview riskOverview) {
        riskOverview.setCreateTime(DateUtils.getNowDate());
        return riskOverviewMapper.insertRiskOverview(riskOverview);
    }

    /**
     * 修改风险预警监管体系概况
     * 
     * @param riskOverview 风险预警监管体系概况
     * @return 结果
     */
    @Override
    public int updateRiskOverview(RiskOverview riskOverview) {
        riskOverview.setUpdateTime(DateUtils.getNowDate());
        return riskOverviewMapper.updateRiskOverview(riskOverview);
    }

    /**
     * 批量删除风险预警监管体系概况
     * 
     * @param ids 需要删除的风险预警监管体系概况主键
     * @return 结果
     */
    @Override
    public int deleteRiskOverviewByIds(Long[] ids) {
        return riskOverviewMapper.deleteRiskOverviewByIds(ids);
    }

    /**
     * 删除风险预警监管体系概况信息
     * 
     * @param id 风险预警监管体系概况主键
     * @return 结果
     */
    @Override
    public int deleteRiskOverviewById(Long id) {
        return riskOverviewMapper.deleteRiskOverviewById(id);
    }

    /**
     * 导入风险预警监管体系概况数据
     *
     * @param riskOverviewList riskOverviewList 风险预警监管体系概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importRiskOverview(List<RiskOverview> riskOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(riskOverviewList) || riskOverviewList.size() == 0) {
            throw new ServiceException("导入风险预警监管体系概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (RiskOverview riskOverview : riskOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    riskOverview.setCreateBy(operName);
                    this.insertRiskOverview(riskOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    riskOverview.setUpdateBy(operName);
                    this.updateRiskOverview(riskOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
