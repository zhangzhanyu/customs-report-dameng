package com.qianjing.project.risk.mapper;

import java.util.List;
import com.qianjing.project.risk.domain.RiskOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 风险预警监管体系概况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface RiskOverviewMapper {

    RiskOverview selectRiskOverviewById(Long id);

    List<RiskOverview> selectRiskOverviewList(RiskOverview riskOverview);

    int insertRiskOverview(RiskOverview riskOverview);

    int updateRiskOverview(RiskOverview riskOverview);

    int deleteRiskOverviewById(Long id);

    int deleteRiskOverviewByIds(Long[] ids);
}
