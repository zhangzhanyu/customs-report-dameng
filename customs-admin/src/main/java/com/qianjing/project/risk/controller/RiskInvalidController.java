package com.qianjing.project.risk.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.risk.domain.RiskInvalid;
import com.qianjing.project.risk.service.IRiskInvalidService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 检验不合格情况Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/risk/invalid")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskInvalidController extends BaseController {

    private final IRiskInvalidService riskInvalidService;

    /**
     * 查询检验不合格情况列表
     */
//    @PreAuthorize("@ss.hasPermi('risk:invalid:list')")
    @GetMapping("/list")
    public TableDataInfo list(RiskInvalid riskInvalid) {
        startPage();
        List<RiskInvalid> list = riskInvalidService.selectRiskInvalidList(riskInvalid);
        return getDataTable(list);
    }

    /**
     * 导出检验不合格情况列表
     */
    @PreAuthorize("@ss.hasPermi('risk:invalid:export')")
    @Log(title = "检验不合格情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RiskInvalid riskInvalid) {
        List<RiskInvalid> list = riskInvalidService.selectRiskInvalidList(riskInvalid);
        ExcelUtil<RiskInvalid> util = new ExcelUtil<RiskInvalid>(RiskInvalid.class);
        return util.exportExcel(list, "检验不合格情况数据");
    }

    /**
     * 导入检验不合格情况列表
     */
    @Log(title = "检验不合格情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('risk:invalid:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<RiskInvalid> util = new ExcelUtil<RiskInvalid>(RiskInvalid.class);
        List<RiskInvalid> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = riskInvalidService.importRiskInvalid(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载检验不合格情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<RiskInvalid> util = new ExcelUtil<RiskInvalid>(RiskInvalid.class);
        util.importTemplateExcel(response, "检验不合格情况数据");
    }

    /**
     * 获取检验不合格情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('risk:invalid:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(riskInvalidService.selectRiskInvalidById(id));
    }

    /**
     * 新增检验不合格情况
     */
    @PreAuthorize("@ss.hasPermi('risk:invalid:add')")
    @Log(title = "检验不合格情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RiskInvalid riskInvalid) {
        return toAjax(riskInvalidService.insertRiskInvalid(riskInvalid));
    }

    /**
     * 修改检验不合格情况
     */
    @PreAuthorize("@ss.hasPermi('risk:invalid:edit')")
    @Log(title = "检验不合格情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RiskInvalid riskInvalid) {
        return toAjax(riskInvalidService.updateRiskInvalid(riskInvalid));
    }

    /**
     * 删除检验不合格情况
     */
    @PreAuthorize("@ss.hasPermi('risk:invalid:remove')")
    @Log(title = "检验不合格情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(riskInvalidService.deleteRiskInvalidByIds(ids));
    }
}
