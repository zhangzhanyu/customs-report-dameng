package com.qianjing.project.risk.service;

import java.util.List;
import com.qianjing.project.risk.domain.RiskReport;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 评估报告舆情监测报告清单Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IRiskReportService 
{
    /**
     * 查询评估报告舆情监测报告清单
     * 
     * @param id 评估报告舆情监测报告清单主键
     * @return 评估报告舆情监测报告清单
     */
    public RiskReport selectRiskReportById(Long id);

    /**
     * 查询评估报告舆情监测报告清单列表
     * 
     * @param riskReport 评估报告舆情监测报告清单
     * @return 评估报告舆情监测报告清单集合
     */
    public List<RiskReport> selectRiskReportList(RiskReport riskReport);

    /**
     * 新增评估报告舆情监测报告清单
     * 
     * @param riskReport 评估报告舆情监测报告清单
     * @return 结果
     */
    public int insertRiskReport(RiskReport riskReport);

    /**
     * 修改评估报告舆情监测报告清单
     * 
     * @param riskReport 评估报告舆情监测报告清单
     * @return 结果
     */
    public int updateRiskReport(RiskReport riskReport);

    /**
     * 批量删除评估报告舆情监测报告清单
     * 
     * @param ids 需要删除的评估报告舆情监测报告清单主键集合
     * @return 结果
     */
    public int deleteRiskReportByIds(Long[] ids);

    /**
     * 删除评估报告舆情监测报告清单信息
     * 
     * @param id 评估报告舆情监测报告清单主键
     * @return 结果
     */
    public int deleteRiskReportById(Long id);

    /**
     * 导入评估报告舆情监测报告清单信息
     *
     * @param riskReportList 评估报告舆情监测报告清单信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importRiskReport(List<RiskReport> riskReportList, Boolean isUpdateSupport, String operName);
}
