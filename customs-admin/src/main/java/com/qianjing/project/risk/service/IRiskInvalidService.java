package com.qianjing.project.risk.service;

import java.util.List;
import com.qianjing.project.risk.domain.RiskInvalid;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 检验不合格情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IRiskInvalidService 
{
    /**
     * 查询检验不合格情况
     * 
     * @param id 检验不合格情况主键
     * @return 检验不合格情况
     */
    public RiskInvalid selectRiskInvalidById(Long id);

    /**
     * 查询检验不合格情况列表
     * 
     * @param riskInvalid 检验不合格情况
     * @return 检验不合格情况集合
     */
    public List<RiskInvalid> selectRiskInvalidList(RiskInvalid riskInvalid);

    /**
     * 新增检验不合格情况
     * 
     * @param riskInvalid 检验不合格情况
     * @return 结果
     */
    public int insertRiskInvalid(RiskInvalid riskInvalid);

    /**
     * 修改检验不合格情况
     * 
     * @param riskInvalid 检验不合格情况
     * @return 结果
     */
    public int updateRiskInvalid(RiskInvalid riskInvalid);

    /**
     * 批量删除检验不合格情况
     * 
     * @param ids 需要删除的检验不合格情况主键集合
     * @return 结果
     */
    public int deleteRiskInvalidByIds(Long[] ids);

    /**
     * 删除检验不合格情况信息
     * 
     * @param id 检验不合格情况主键
     * @return 结果
     */
    public int deleteRiskInvalidById(Long id);

    /**
     * 导入检验不合格情况信息
     *
     * @param riskInvalidList 检验不合格情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importRiskInvalid(List<RiskInvalid> riskInvalidList, Boolean isUpdateSupport, String operName);
}
