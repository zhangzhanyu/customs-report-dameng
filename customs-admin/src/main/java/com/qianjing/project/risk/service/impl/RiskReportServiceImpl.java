package com.qianjing.project.risk.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.risk.mapper.RiskReportMapper;
import com.qianjing.project.risk.domain.RiskReport;
import com.qianjing.project.risk.service.IRiskReportService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 评估报告舆情监测报告清单Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskReportServiceImpl implements IRiskReportService {

    private static final Logger log = LoggerFactory.getLogger(RiskReportServiceImpl.class);

    private final RiskReportMapper riskReportMapper;

    /**
     * 查询评估报告舆情监测报告清单
     * 
     * @param id 评估报告舆情监测报告清单主键
     * @return 评估报告舆情监测报告清单
     */
    @Override
    public RiskReport selectRiskReportById(Long id) {
        return riskReportMapper.selectRiskReportById(id);
    }

    /**
     * 查询评估报告舆情监测报告清单列表
     * 
     * @param riskReport 评估报告舆情监测报告清单
     * @return 评估报告舆情监测报告清单
     */
    @Override
    public List<RiskReport> selectRiskReportList(RiskReport riskReport) {
        return riskReportMapper.selectRiskReportList(riskReport);
    }

    /**
     * 新增评估报告舆情监测报告清单
     * 
     * @param riskReport 评估报告舆情监测报告清单
     * @return 结果
     */
    @Override
    public int insertRiskReport(RiskReport riskReport) {
        riskReport.setCreateTime(DateUtils.getNowDate());
        return riskReportMapper.insertRiskReport(riskReport);
    }

    /**
     * 修改评估报告舆情监测报告清单
     * 
     * @param riskReport 评估报告舆情监测报告清单
     * @return 结果
     */
    @Override
    public int updateRiskReport(RiskReport riskReport) {
        riskReport.setUpdateTime(DateUtils.getNowDate());
        return riskReportMapper.updateRiskReport(riskReport);
    }

    /**
     * 批量删除评估报告舆情监测报告清单
     * 
     * @param ids 需要删除的评估报告舆情监测报告清单主键
     * @return 结果
     */
    @Override
    public int deleteRiskReportByIds(Long[] ids) {
        return riskReportMapper.deleteRiskReportByIds(ids);
    }

    /**
     * 删除评估报告舆情监测报告清单信息
     * 
     * @param id 评估报告舆情监测报告清单主键
     * @return 结果
     */
    @Override
    public int deleteRiskReportById(Long id) {
        return riskReportMapper.deleteRiskReportById(id);
    }

    /**
     * 导入评估报告舆情监测报告清单数据
     *
     * @param riskReportList riskReportList 评估报告舆情监测报告清单信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importRiskReport(List<RiskReport> riskReportList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(riskReportList) || riskReportList.size() == 0) {
            throw new ServiceException("导入评估报告舆情监测报告清单数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (RiskReport riskReport : riskReportList){
            try {
                // 验证是否存在
                if (true) {
                    riskReport.setCreateBy(operName);
                    this.insertRiskReport(riskReport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    riskReport.setUpdateBy(operName);
                    this.updateRiskReport(riskReport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
