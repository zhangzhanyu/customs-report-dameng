package com.qianjing.project.risk.mapper;

import java.util.List;
import com.qianjing.project.risk.domain.RiskReport;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 评估报告舆情监测报告清单Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface RiskReportMapper {

    RiskReport selectRiskReportById(Long id);

    List<RiskReport> selectRiskReportList(RiskReport riskReport);

    int insertRiskReport(RiskReport riskReport);

    int updateRiskReport(RiskReport riskReport);

    int deleteRiskReportById(Long id);

    int deleteRiskReportByIds(Long[] ids);
}
