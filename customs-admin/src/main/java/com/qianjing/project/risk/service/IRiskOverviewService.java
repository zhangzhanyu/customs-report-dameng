package com.qianjing.project.risk.service;

import java.util.List;
import com.qianjing.project.risk.domain.RiskOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 风险预警监管体系概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IRiskOverviewService 
{
    /**
     * 查询风险预警监管体系概况
     * 
     * @param id 风险预警监管体系概况主键
     * @return 风险预警监管体系概况
     */
    public RiskOverview selectRiskOverviewById(Long id);

    /**
     * 查询风险预警监管体系概况列表
     * 
     * @param riskOverview 风险预警监管体系概况
     * @return 风险预警监管体系概况集合
     */
    public List<RiskOverview> selectRiskOverviewList(RiskOverview riskOverview);

    /**
     * 新增风险预警监管体系概况
     * 
     * @param riskOverview 风险预警监管体系概况
     * @return 结果
     */
    public int insertRiskOverview(RiskOverview riskOverview);

    /**
     * 修改风险预警监管体系概况
     * 
     * @param riskOverview 风险预警监管体系概况
     * @return 结果
     */
    public int updateRiskOverview(RiskOverview riskOverview);

    /**
     * 批量删除风险预警监管体系概况
     * 
     * @param ids 需要删除的风险预警监管体系概况主键集合
     * @return 结果
     */
    public int deleteRiskOverviewByIds(Long[] ids);

    /**
     * 删除风险预警监管体系概况信息
     * 
     * @param id 风险预警监管体系概况主键
     * @return 结果
     */
    public int deleteRiskOverviewById(Long id);

    /**
     * 导入风险预警监管体系概况信息
     *
     * @param riskOverviewList 风险预警监管体系概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importRiskOverview(List<RiskOverview> riskOverviewList, Boolean isUpdateSupport, String operName);
}
