package com.qianjing.project.risk.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 抽查检验情况对象 risk_random_check
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public class RiskRandomCheck extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String classification;

    /** 抽查批次 */
    @Excel(name = "抽查批次")
    private Long randomCheckBatch;

    /** 不合格批次 */
    @Excel(name = "不合格批次")
    private Long invalidBatch;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setClassification(String classification){
        this.classification = classification;
    }

    public String getClassification(){
        return classification;
    }
    public void setRandomCheckBatch(Long randomCheckBatch){
        this.randomCheckBatch = randomCheckBatch;
    }

    public Long getRandomCheckBatch(){
        return randomCheckBatch;
    }
    public void setInvalidBatch(Long invalidBatch){
        this.invalidBatch = invalidBatch;
    }

    public Long getInvalidBatch(){
        return invalidBatch;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("classification", getClassification())
            .append("randomCheckBatch", getRandomCheckBatch())
            .append("invalidBatch", getInvalidBatch())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
