package com.qianjing.project.risk.service;

import java.util.List;
import com.qianjing.project.risk.domain.RiskRandomCheck;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 抽查检验情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IRiskRandomCheckService 
{
    /**
     * 查询抽查检验情况
     * 
     * @param id 抽查检验情况主键
     * @return 抽查检验情况
     */
    public RiskRandomCheck selectRiskRandomCheckById(Long id);

    /**
     * 查询抽查检验情况列表
     * 
     * @param riskRandomCheck 抽查检验情况
     * @return 抽查检验情况集合
     */
    public List<RiskRandomCheck> selectRiskRandomCheckList(RiskRandomCheck riskRandomCheck);

    /**
     * 新增抽查检验情况
     * 
     * @param riskRandomCheck 抽查检验情况
     * @return 结果
     */
    public int insertRiskRandomCheck(RiskRandomCheck riskRandomCheck);

    /**
     * 修改抽查检验情况
     * 
     * @param riskRandomCheck 抽查检验情况
     * @return 结果
     */
    public int updateRiskRandomCheck(RiskRandomCheck riskRandomCheck);

    /**
     * 批量删除抽查检验情况
     * 
     * @param ids 需要删除的抽查检验情况主键集合
     * @return 结果
     */
    public int deleteRiskRandomCheckByIds(Long[] ids);

    /**
     * 删除抽查检验情况信息
     * 
     * @param id 抽查检验情况主键
     * @return 结果
     */
    public int deleteRiskRandomCheckById(Long id);

    /**
     * 导入抽查检验情况信息
     *
     * @param riskRandomCheckList 抽查检验情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importRiskRandomCheck(List<RiskRandomCheck> riskRandomCheckList, Boolean isUpdateSupport, String operName);
}
