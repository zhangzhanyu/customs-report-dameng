package com.qianjing.project.risk.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.risk.domain.RiskReport;
import com.qianjing.project.risk.service.IRiskReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 评估报告舆情监测报告清单Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/risk/report")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskReportController extends BaseController {

    private final IRiskReportService riskReportService;

    /**
     * 查询评估报告舆情监测报告清单列表
     */
//    @PreAuthorize("@ss.hasPermi('risk:report:list')")
    @GetMapping("/list")
    public TableDataInfo list(RiskReport riskReport) {
        startPage();
        List<RiskReport> list = riskReportService.selectRiskReportList(riskReport);
        return getDataTable(list);
    }

    /**
     * 导出评估报告舆情监测报告清单列表
     */
    @PreAuthorize("@ss.hasPermi('risk:report:export')")
    @Log(title = "评估报告舆情监测报告清单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RiskReport riskReport) {
        List<RiskReport> list = riskReportService.selectRiskReportList(riskReport);
        ExcelUtil<RiskReport> util = new ExcelUtil<RiskReport>(RiskReport.class);
        return util.exportExcel(list, "评估报告舆情监测报告清单数据");
    }

    /**
     * 导入评估报告舆情监测报告清单列表
     */
    @Log(title = "评估报告舆情监测报告清单", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('risk:report:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<RiskReport> util = new ExcelUtil<RiskReport>(RiskReport.class);
        List<RiskReport> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = riskReportService.importRiskReport(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载评估报告舆情监测报告清单导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<RiskReport> util = new ExcelUtil<RiskReport>(RiskReport.class);
        util.importTemplateExcel(response, "评估报告舆情监测报告清单数据");
    }

    /**
     * 获取评估报告舆情监测报告清单详细信息
     */
    @PreAuthorize("@ss.hasPermi('risk:report:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(riskReportService.selectRiskReportById(id));
    }

    /**
     * 新增评估报告舆情监测报告清单
     */
    @PreAuthorize("@ss.hasPermi('risk:report:add')")
    @Log(title = "评估报告舆情监测报告清单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RiskReport riskReport) {
        return toAjax(riskReportService.insertRiskReport(riskReport));
    }

    /**
     * 修改评估报告舆情监测报告清单
     */
    @PreAuthorize("@ss.hasPermi('risk:report:edit')")
    @Log(title = "评估报告舆情监测报告清单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RiskReport riskReport) {
        return toAjax(riskReportService.updateRiskReport(riskReport));
    }

    /**
     * 删除评估报告舆情监测报告清单
     */
    @PreAuthorize("@ss.hasPermi('risk:report:remove')")
    @Log(title = "评估报告舆情监测报告清单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(riskReportService.deleteRiskReportByIds(ids));
    }
}
