package com.qianjing.project.risk.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 检验不合格情况对象 risk_invalid
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public class RiskInvalid extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 不合格类型 */
    @Excel(name = "不合格类型")
    private String invalidType;

    /** 报关单批 */
    @Excel(name = "报关单批")
    private Long entryBatch;

    /** 货物批 */
    @Excel(name = "货物批")
    private Long goodsBatch;

    /** 金额(美元) */
    @Excel(name = "金额(美元)")
    private Long usdPrice;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setInvalidType(String invalidType){
        this.invalidType = invalidType;
    }

    public String getInvalidType(){
        return invalidType;
    }
    public void setEntryBatch(Long entryBatch){
        this.entryBatch = entryBatch;
    }

    public Long getEntryBatch(){
        return entryBatch;
    }
    public void setGoodsBatch(Long goodsBatch){
        this.goodsBatch = goodsBatch;
    }

    public Long getGoodsBatch(){
        return goodsBatch;
    }
    public void setUsdPrice(Long usdPrice){
        this.usdPrice = usdPrice;
    }

    public Long getUsdPrice(){
        return usdPrice;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("invalidType", getInvalidType())
            .append("entryBatch", getEntryBatch())
            .append("goodsBatch", getGoodsBatch())
            .append("usdPrice", getUsdPrice())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
