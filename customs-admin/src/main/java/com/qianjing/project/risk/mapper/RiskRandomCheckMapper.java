package com.qianjing.project.risk.mapper;

import java.util.List;
import com.qianjing.project.risk.domain.RiskRandomCheck;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 抽查检验情况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface RiskRandomCheckMapper {

    RiskRandomCheck selectRiskRandomCheckById(Long id);

    List<RiskRandomCheck> selectRiskRandomCheckList(RiskRandomCheck riskRandomCheck);

    int insertRiskRandomCheck(RiskRandomCheck riskRandomCheck);

    int updateRiskRandomCheck(RiskRandomCheck riskRandomCheck);

    int deleteRiskRandomCheckById(Long id);

    int deleteRiskRandomCheckByIds(Long[] ids);
}
