package com.qianjing.project.risk.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.risk.domain.RiskRandomCheck;
import com.qianjing.project.risk.service.IRiskRandomCheckService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 抽查检验情况Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/risk/check")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskRandomCheckController extends BaseController {

    private final IRiskRandomCheckService riskRandomCheckService;

    /**
     * 查询抽查检验情况列表
     */
//    @PreAuthorize("@ss.hasPermi('risk:check:list')")
    @GetMapping("/list")
    public TableDataInfo list(RiskRandomCheck riskRandomCheck) {
        startPage();
        List<RiskRandomCheck> list = riskRandomCheckService.selectRiskRandomCheckList(riskRandomCheck);
        return getDataTable(list);
    }

    /**
     * 导出抽查检验情况列表
     */
    @PreAuthorize("@ss.hasPermi('risk:check:export')")
    @Log(title = "抽查检验情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RiskRandomCheck riskRandomCheck) {
        List<RiskRandomCheck> list = riskRandomCheckService.selectRiskRandomCheckList(riskRandomCheck);
        ExcelUtil<RiskRandomCheck> util = new ExcelUtil<RiskRandomCheck>(RiskRandomCheck.class);
        return util.exportExcel(list, "抽查检验情况数据");
    }

    /**
     * 导入抽查检验情况列表
     */
    @Log(title = "抽查检验情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('risk:check:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<RiskRandomCheck> util = new ExcelUtil<RiskRandomCheck>(RiskRandomCheck.class);
        List<RiskRandomCheck> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = riskRandomCheckService.importRiskRandomCheck(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载抽查检验情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<RiskRandomCheck> util = new ExcelUtil<RiskRandomCheck>(RiskRandomCheck.class);
        util.importTemplateExcel(response, "抽查检验情况数据");
    }

    /**
     * 获取抽查检验情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('risk:check:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(riskRandomCheckService.selectRiskRandomCheckById(id));
    }

    /**
     * 新增抽查检验情况
     */
    @PreAuthorize("@ss.hasPermi('risk:check:add')")
    @Log(title = "抽查检验情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RiskRandomCheck riskRandomCheck) {
        return toAjax(riskRandomCheckService.insertRiskRandomCheck(riskRandomCheck));
    }

    /**
     * 修改抽查检验情况
     */
    @PreAuthorize("@ss.hasPermi('risk:check:edit')")
    @Log(title = "抽查检验情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RiskRandomCheck riskRandomCheck) {
        return toAjax(riskRandomCheckService.updateRiskRandomCheck(riskRandomCheck));
    }

    /**
     * 删除抽查检验情况
     */
    @PreAuthorize("@ss.hasPermi('risk:check:remove')")
    @Log(title = "抽查检验情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(riskRandomCheckService.deleteRiskRandomCheckByIds(ids));
    }
}
