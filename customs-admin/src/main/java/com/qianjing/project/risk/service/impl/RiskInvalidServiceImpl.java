package com.qianjing.project.risk.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.risk.mapper.RiskInvalidMapper;
import com.qianjing.project.risk.domain.RiskInvalid;
import com.qianjing.project.risk.service.IRiskInvalidService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 检验不合格情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RiskInvalidServiceImpl implements IRiskInvalidService {

    private static final Logger log = LoggerFactory.getLogger(RiskInvalidServiceImpl.class);

    private final RiskInvalidMapper riskInvalidMapper;

    /**
     * 查询检验不合格情况
     * 
     * @param id 检验不合格情况主键
     * @return 检验不合格情况
     */
    @Override
    public RiskInvalid selectRiskInvalidById(Long id) {
        return riskInvalidMapper.selectRiskInvalidById(id);
    }

    /**
     * 查询检验不合格情况列表
     * 
     * @param riskInvalid 检验不合格情况
     * @return 检验不合格情况
     */
    @Override
    public List<RiskInvalid> selectRiskInvalidList(RiskInvalid riskInvalid) {
        return riskInvalidMapper.selectRiskInvalidList(riskInvalid);
    }

    /**
     * 新增检验不合格情况
     * 
     * @param riskInvalid 检验不合格情况
     * @return 结果
     */
    @Override
    public int insertRiskInvalid(RiskInvalid riskInvalid) {
        riskInvalid.setCreateTime(DateUtils.getNowDate());
        return riskInvalidMapper.insertRiskInvalid(riskInvalid);
    }

    /**
     * 修改检验不合格情况
     * 
     * @param riskInvalid 检验不合格情况
     * @return 结果
     */
    @Override
    public int updateRiskInvalid(RiskInvalid riskInvalid) {
        riskInvalid.setUpdateTime(DateUtils.getNowDate());
        return riskInvalidMapper.updateRiskInvalid(riskInvalid);
    }

    /**
     * 批量删除检验不合格情况
     * 
     * @param ids 需要删除的检验不合格情况主键
     * @return 结果
     */
    @Override
    public int deleteRiskInvalidByIds(Long[] ids) {
        return riskInvalidMapper.deleteRiskInvalidByIds(ids);
    }

    /**
     * 删除检验不合格情况信息
     * 
     * @param id 检验不合格情况主键
     * @return 结果
     */
    @Override
    public int deleteRiskInvalidById(Long id) {
        return riskInvalidMapper.deleteRiskInvalidById(id);
    }

    /**
     * 导入检验不合格情况数据
     *
     * @param riskInvalidList riskInvalidList 检验不合格情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importRiskInvalid(List<RiskInvalid> riskInvalidList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(riskInvalidList) || riskInvalidList.size() == 0) {
            throw new ServiceException("导入检验不合格情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (RiskInvalid riskInvalid : riskInvalidList){
            try {
                // 验证是否存在
                if (true) {
                    riskInvalid.setCreateBy(operName);
                    this.insertRiskInvalid(riskInvalid);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    riskInvalid.setUpdateBy(operName);
                    this.updateRiskInvalid(riskInvalid);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
