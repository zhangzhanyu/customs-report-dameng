package com.qianjing.project.twoDept.mapper;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionIron;

/**
 * 进口铁矿检验监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-16
 */
public interface SusionIronMapper 
{
    /**
     * 查询进口铁矿检验监管情况统计
     * 
     * @param susionIronId 进口铁矿检验监管情况统计主键
     * @return 进口铁矿检验监管情况统计
     */
    public SusionIron selectSusionIronBySusionIronId(Long susionIronId);

    /**
     * 查询进口铁矿检验监管情况统计列表
     * 
     * @param susionIron 进口铁矿检验监管情况统计
     * @return 进口铁矿检验监管情况统计集合
     */
    public List<SusionIron> selectSusionIronList(SusionIron susionIron);

    /**
     * 新增进口铁矿检验监管情况统计
     * 
     * @param susionIron 进口铁矿检验监管情况统计
     * @return 结果
     */
    public int insertSusionIron(SusionIron susionIron);

    /**
     * 修改进口铁矿检验监管情况统计
     * 
     * @param susionIron 进口铁矿检验监管情况统计
     * @return 结果
     */
    public int updateSusionIron(SusionIron susionIron);

    /**
     * 删除进口铁矿检验监管情况统计
     * 
     * @param susionIronId 进口铁矿检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionIronBySusionIronId(Long susionIronId);

    /**
     * 批量删除进口铁矿检验监管情况统计
     * 
     * @param susionIronIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionIronBySusionIronIds(Long[] susionIronIds);
}
