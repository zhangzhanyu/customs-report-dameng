package com.qianjing.project.twoDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.twoDept.domain.SusionRawMetal;
import com.qianjing.project.twoDept.service.ISusionRawMetalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口再生原料检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/twoDept/susionMetal")
public class SusionRawMetalController extends BaseController
{
    @Autowired
    private ISusionRawMetalService susionRawMetalService;

    /**
     * 查询进口再生原料检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMetal:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionRawMetal susionRawMetal)
    {
        startPage();
        List<SusionRawMetal> list = susionRawMetalService.selectSusionRawMetalList(susionRawMetal);
        return getDataTable(list);
    }

    /**
     * 导出进口再生原料检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMetal:export')")
    @Log(title = "进口再生原料检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionRawMetal susionRawMetal)
    {
        List<SusionRawMetal> list = susionRawMetalService.selectSusionRawMetalList(susionRawMetal);
        ExcelUtil<SusionRawMetal> util = new ExcelUtil<SusionRawMetal>(SusionRawMetal.class);
        return util.exportExcel(list, "进口再生原料检验监管情况统计数据");
    }

    /**
     * 导入进口再生原料检验监管情况统计列表
     */
    @Log(title = "进口再生原料检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('twoDept:susionMetal:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionRawMetal> util = new ExcelUtil<SusionRawMetal>(SusionRawMetal.class);
        List<SusionRawMetal> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionRawMetalService.importSusionRawMetal(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口再生原料检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionRawMetal> util = new ExcelUtil<SusionRawMetal>(SusionRawMetal.class);
        util.importTemplateExcel(response, "进口再生原料检验监管情况统计数据");
    }

    /**
     * 获取进口再生原料检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMetal:query')")
    @GetMapping(value = "/{susionRawMetalId}")
    public AjaxResult getInfo(@PathVariable("susionRawMetalId") Long susionRawMetalId)
    {
        return AjaxResult.success(susionRawMetalService.selectSusionRawMetalBySusionRawMetalId(susionRawMetalId));
    }

    /**
     * 新增进口再生原料检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMetal:add')")
    @Log(title = "进口再生原料检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionRawMetal susionRawMetal)
    {
        return toAjax(susionRawMetalService.insertSusionRawMetal(susionRawMetal));
    }

    /**
     * 修改进口再生原料检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMetal:edit')")
    @Log(title = "进口再生原料检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionRawMetal susionRawMetal)
    {
        return toAjax(susionRawMetalService.updateSusionRawMetal(susionRawMetal));
    }

    /**
     * 删除进口再生原料检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMetal:remove')")
    @Log(title = "进口再生原料检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionRawMetalIds}")
    public AjaxResult remove(@PathVariable Long[] susionRawMetalIds)
    {
        return toAjax(susionRawMetalService.deleteSusionRawMetalBySusionRawMetalIds(susionRawMetalIds));
    }
}
