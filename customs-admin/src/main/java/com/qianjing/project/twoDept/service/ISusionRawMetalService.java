package com.qianjing.project.twoDept.service;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionRawMetal;

/**
 * 进口再生原料检验监管情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-19
 */
public interface ISusionRawMetalService 
{
    /**
     * 查询进口再生原料检验监管情况统计
     * 
     * @param susionRawMetalId 进口再生原料检验监管情况统计主键
     * @return 进口再生原料检验监管情况统计
     */
    public SusionRawMetal selectSusionRawMetalBySusionRawMetalId(Long susionRawMetalId);

    /**
     * 查询进口再生原料检验监管情况统计列表
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 进口再生原料检验监管情况统计集合
     */
    public List<SusionRawMetal> selectSusionRawMetalList(SusionRawMetal susionRawMetal);

    /**
     * 新增进口再生原料检验监管情况统计
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 结果
     */
    public int insertSusionRawMetal(SusionRawMetal susionRawMetal);

    /**
     * 修改进口再生原料检验监管情况统计
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 结果
     */
    public int updateSusionRawMetal(SusionRawMetal susionRawMetal);

    /**
     * 批量删除进口再生原料检验监管情况统计
     * 
     * @param susionRawMetalIds 需要删除的进口再生原料检验监管情况统计主键集合
     * @return 结果
     */
    public int deleteSusionRawMetalBySusionRawMetalIds(Long[] susionRawMetalIds);

    /**
     * 删除进口再生原料检验监管情况统计信息
     * 
     * @param susionRawMetalId 进口再生原料检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionRawMetalBySusionRawMetalId(Long susionRawMetalId);

    /**
     * 导入进口再生原料检验监管情况统计信息
     *
     * @param susionRawMetalList 进口再生原料检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionRawMetal(List<SusionRawMetal> susionRawMetalList, Boolean isUpdateSupport, String operName);
}
