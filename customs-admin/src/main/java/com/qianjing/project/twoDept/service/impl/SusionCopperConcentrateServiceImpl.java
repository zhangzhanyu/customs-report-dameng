package com.qianjing.project.twoDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.twoDept.mapper.SusionCopperConcentrateMapper;
import com.qianjing.project.twoDept.domain.SusionCopperConcentrate;
import com.qianjing.project.twoDept.service.ISusionCopperConcentrateService;

/**
 * 进口铜精矿检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-12
 */
@Service
public class SusionCopperConcentrateServiceImpl implements ISusionCopperConcentrateService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionCopperConcentrateServiceImpl.class);

    @Autowired
    private SusionCopperConcentrateMapper susionCopperConcentrateMapper;

    /**
     * 查询进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrateId 进口铜精矿检验监管情况统计主键
     * @return 进口铜精矿检验监管情况统计
     */
    @Override
    public SusionCopperConcentrate selectSusionCopperConcentrateBySusionCopperConcentrateId(Long susionCopperConcentrateId)
    {
        return susionCopperConcentrateMapper.selectSusionCopperConcentrateBySusionCopperConcentrateId(susionCopperConcentrateId);
    }

    /**
     * 查询进口铜精矿检验监管情况统计列表
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 进口铜精矿检验监管情况统计
     */
    @Override
    public List<SusionCopperConcentrate> selectSusionCopperConcentrateList(SusionCopperConcentrate susionCopperConcentrate)
    {
        return susionCopperConcentrateMapper.selectSusionCopperConcentrateList(susionCopperConcentrate);
    }

    /**
     * 新增进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionCopperConcentrate(SusionCopperConcentrate susionCopperConcentrate)
    {
        susionCopperConcentrate.setCreateTime(DateUtils.getNowDate());
        return susionCopperConcentrateMapper.insertSusionCopperConcentrate(susionCopperConcentrate);
    }

    /**
     * 修改进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionCopperConcentrate(SusionCopperConcentrate susionCopperConcentrate)
    {
        susionCopperConcentrate.setUpdateTime(DateUtils.getNowDate());
        return susionCopperConcentrateMapper.updateSusionCopperConcentrate(susionCopperConcentrate);
    }

    /**
     * 批量删除进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrateIds 需要删除的进口铜精矿检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionCopperConcentrateBySusionCopperConcentrateIds(Long[] susionCopperConcentrateIds)
    {
        return susionCopperConcentrateMapper.deleteSusionCopperConcentrateBySusionCopperConcentrateIds(susionCopperConcentrateIds);
    }

    /**
     * 删除进口铜精矿检验监管情况统计信息
     * 
     * @param susionCopperConcentrateId 进口铜精矿检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionCopperConcentrateBySusionCopperConcentrateId(Long susionCopperConcentrateId)
    {
        return susionCopperConcentrateMapper.deleteSusionCopperConcentrateBySusionCopperConcentrateId(susionCopperConcentrateId);
    }

    /**
     * 导入进口铜精矿检验监管情况统计数据
     *
     * @param susionCopperConcentrateList susionCopperConcentrateList 进口铜精矿检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionCopperConcentrate(List<SusionCopperConcentrate> susionCopperConcentrateList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionCopperConcentrateList) || susionCopperConcentrateList.size() == 0) {
            throw new ServiceException("导入进口铜精矿检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionCopperConcentrate susionCopperConcentrate : susionCopperConcentrateList){
            try {
                // 验证是否存在
                if (true) {
                    susionCopperConcentrate.setCreateBy(operName);
                    this.insertSusionCopperConcentrate(susionCopperConcentrate);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionCopperConcentrate.setUpdateBy(operName);
                    this.updateSusionCopperConcentrate(susionCopperConcentrate);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
