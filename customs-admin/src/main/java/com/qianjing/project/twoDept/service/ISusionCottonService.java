package com.qianjing.project.twoDept.service;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionCotton;

/**
 * 进口棉花检验情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-12
 */
public interface ISusionCottonService 
{
    /**
     * 查询进口棉花检验情况统计
     * 
     * @param cottonId 进口棉花检验情况统计主键
     * @return 进口棉花检验情况统计
     */
    public SusionCotton selectSusionCottonByCottonId(Long cottonId);

    /**
     * 查询进口棉花检验情况统计列表
     * 
     * @param susionCotton 进口棉花检验情况统计
     * @return 进口棉花检验情况统计集合
     */
    public List<SusionCotton> selectSusionCottonList(SusionCotton susionCotton);

    /**
     * 新增进口棉花检验情况统计
     * 
     * @param susionCotton 进口棉花检验情况统计
     * @return 结果
     */
    public int insertSusionCotton(SusionCotton susionCotton);

    /**
     * 修改进口棉花检验情况统计
     * 
     * @param susionCotton 进口棉花检验情况统计
     * @return 结果
     */
    public int updateSusionCotton(SusionCotton susionCotton);

    /**
     * 批量删除进口棉花检验情况统计
     * 
     * @param cottonIds 需要删除的进口棉花检验情况统计主键集合
     * @return 结果
     */
    public int deleteSusionCottonByCottonIds(Long[] cottonIds);

    /**
     * 删除进口棉花检验情况统计信息
     * 
     * @param cottonId 进口棉花检验情况统计主键
     * @return 结果
     */
    public int deleteSusionCottonByCottonId(Long cottonId);

    /**
     * 导入进口棉花检验情况统计信息
     *
     * @param susionCottonList 进口棉花检验情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionCotton(List<SusionCotton> susionCottonList, Boolean isUpdateSupport, String operName);
}
