package com.qianjing.project.twoDept.mapper;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionRawMetal;

/**
 * 进口再生原料检验监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-19
 */
public interface SusionRawMetalMapper 
{
    /**
     * 查询进口再生原料检验监管情况统计
     * 
     * @param susionRawMetalId 进口再生原料检验监管情况统计主键
     * @return 进口再生原料检验监管情况统计
     */
    public SusionRawMetal selectSusionRawMetalBySusionRawMetalId(Long susionRawMetalId);

    /**
     * 查询进口再生原料检验监管情况统计列表
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 进口再生原料检验监管情况统计集合
     */
    public List<SusionRawMetal> selectSusionRawMetalList(SusionRawMetal susionRawMetal);

    /**
     * 新增进口再生原料检验监管情况统计
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 结果
     */
    public int insertSusionRawMetal(SusionRawMetal susionRawMetal);

    /**
     * 修改进口再生原料检验监管情况统计
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 结果
     */
    public int updateSusionRawMetal(SusionRawMetal susionRawMetal);

    /**
     * 删除进口再生原料检验监管情况统计
     * 
     * @param susionRawMetalId 进口再生原料检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionRawMetalBySusionRawMetalId(Long susionRawMetalId);

    /**
     * 批量删除进口再生原料检验监管情况统计
     * 
     * @param susionRawMetalIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionRawMetalBySusionRawMetalIds(Long[] susionRawMetalIds);
}
