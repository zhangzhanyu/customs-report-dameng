package com.qianjing.project.twoDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.twoDept.mapper.SusionMineMapper;
import com.qianjing.project.twoDept.domain.SusionMine;
import com.qianjing.project.twoDept.service.ISusionMineService;

/**
 * 进口矿产品“先放后检”情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-16
 */
@Service
public class SusionMineServiceImpl implements ISusionMineService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionMineServiceImpl.class);

    @Autowired
    private SusionMineMapper susionMineMapper;

    /**
     * 查询进口矿产品“先放后检”情况统计
     * 
     * @param susionMineId 进口矿产品“先放后检”情况统计主键
     * @return 进口矿产品“先放后检”情况统计
     */
    @Override
    public SusionMine selectSusionMineBySusionMineId(Long susionMineId)
    {
        return susionMineMapper.selectSusionMineBySusionMineId(susionMineId);
    }

    /**
     * 查询进口矿产品“先放后检”情况统计列表
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 进口矿产品“先放后检”情况统计
     */
    @Override
    public List<SusionMine> selectSusionMineList(SusionMine susionMine)
    {
        return susionMineMapper.selectSusionMineList(susionMine);
    }

    /**
     * 新增进口矿产品“先放后检”情况统计
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 结果
     */
    @Override
    public int insertSusionMine(SusionMine susionMine)
    {
        susionMine.setCreateTime(DateUtils.getNowDate());
        return susionMineMapper.insertSusionMine(susionMine);
    }

    /**
     * 修改进口矿产品“先放后检”情况统计
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 结果
     */
    @Override
    public int updateSusionMine(SusionMine susionMine)
    {
        susionMine.setUpdateTime(DateUtils.getNowDate());
        return susionMineMapper.updateSusionMine(susionMine);
    }

    /**
     * 批量删除进口矿产品“先放后检”情况统计
     * 
     * @param susionMineIds 需要删除的进口矿产品“先放后检”情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionMineBySusionMineIds(Long[] susionMineIds)
    {
        return susionMineMapper.deleteSusionMineBySusionMineIds(susionMineIds);
    }

    /**
     * 删除进口矿产品“先放后检”情况统计信息
     * 
     * @param susionMineId 进口矿产品“先放后检”情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionMineBySusionMineId(Long susionMineId)
    {
        return susionMineMapper.deleteSusionMineBySusionMineId(susionMineId);
    }

    /**
     * 导入进口矿产品“先放后检”情况统计数据
     *
     * @param susionMineList susionMineList 进口矿产品“先放后检”情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionMine(List<SusionMine> susionMineList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionMineList) || susionMineList.size() == 0) {
            throw new ServiceException("导入进口矿产品“先放后检”情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionMine susionMine : susionMineList){
            try {
                // 验证是否存在
                if (true) {
                    susionMine.setCreateBy(operName);
                    this.insertSusionMine(susionMine);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionMine.setUpdateBy(operName);
                    this.updateSusionMine(susionMine);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
