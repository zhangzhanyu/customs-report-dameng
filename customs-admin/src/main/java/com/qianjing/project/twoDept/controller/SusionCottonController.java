package com.qianjing.project.twoDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.twoDept.domain.SusionCotton;
import com.qianjing.project.twoDept.service.ISusionCottonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口棉花检验情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-12
 */
@RestController
@RequestMapping("/twoDept/susionCotton")
public class SusionCottonController extends BaseController
{
    @Autowired
    private ISusionCottonService susionCottonService;

    /**
     * 查询进口棉花检验情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionCotton:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionCotton susionCotton)
    {
        startPage();
        List<SusionCotton> list = susionCottonService.selectSusionCottonList(susionCotton);
        return getDataTable(list);
    }

    /**
     * 导出进口棉花检验情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionCotton:export')")
    @Log(title = "进口棉花检验情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionCotton susionCotton)
    {
        List<SusionCotton> list = susionCottonService.selectSusionCottonList(susionCotton);
        ExcelUtil<SusionCotton> util = new ExcelUtil<SusionCotton>(SusionCotton.class);
        return util.exportExcel(list, "进口棉花检验情况统计数据");
    }

    /**
     * 导入进口棉花检验情况统计列表
     */
    @Log(title = "进口棉花检验情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('twoDept:susionCotton:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionCotton> util = new ExcelUtil<SusionCotton>(SusionCotton.class);
        List<SusionCotton> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionCottonService.importSusionCotton(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口棉花检验情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionCotton> util = new ExcelUtil<SusionCotton>(SusionCotton.class);
        util.importTemplateExcel(response, "进口棉花检验情况统计数据");
    }

    /**
     * 获取进口棉花检验情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionCotton:query')")
    @GetMapping(value = "/{cottonId}")
    public AjaxResult getInfo(@PathVariable("cottonId") Long cottonId)
    {
        return AjaxResult.success(susionCottonService.selectSusionCottonByCottonId(cottonId));
    }

    /**
     * 新增进口棉花检验情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionCotton:add')")
    @Log(title = "进口棉花检验情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionCotton susionCotton)
    {
        return toAjax(susionCottonService.insertSusionCotton(susionCotton));
    }

    /**
     * 修改进口棉花检验情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionCotton:edit')")
    @Log(title = "进口棉花检验情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionCotton susionCotton)
    {
        return toAjax(susionCottonService.updateSusionCotton(susionCotton));
    }

    /**
     * 删除进口棉花检验情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionCotton:remove')")
    @Log(title = "进口棉花检验情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cottonIds}")
    public AjaxResult remove(@PathVariable Long[] cottonIds)
    {
        return toAjax(susionCottonService.deleteSusionCottonByCottonIds(cottonIds));
    }
}
