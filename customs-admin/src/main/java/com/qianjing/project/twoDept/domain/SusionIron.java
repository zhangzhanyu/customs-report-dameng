package com.qianjing.project.twoDept.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口铁矿检验监管情况统计对象 susion_iron
 * 
 * @author qianjing
 * @date 2022-05-16
 */
public class SusionIron extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long susionIronId;

    /** 项目 */
    @Excel(name = "项目")
    private String goodsCategory;

    /** 批次 */
    @Excel(name = "批次")
    private String totalBatch;

    /** 重量(万吨) */
    @Excel(name = "重量(万吨)")
    private String totalWeight;

    /** 金额(万美元) */
    @Excel(name = "金额(万美元)")
    private String totalAmount;

    /** 批次(实施固废排查的铁矿) */
    @Excel(name = "批次(实施固废排查的铁矿)")
    private String solidWasteBatch;

    /** 重量(万吨)(实施固废排查的铁矿) */
    @Excel(name = "重量(万吨)(实施固废排查的铁矿)")
    private String solidWasteWeight;

    /** 金额(万美元)(实施固废排查的铁矿) */
    @Excel(name = "金额(万美元)(实施固废排查的铁矿)")
    private String solidWasteAmount;

    /** 批次(铁矿固废排查中确定为固废) */
    @Excel(name = "批次(铁矿固废排查中确定为固废)")
    private String defineBatch;

    /** 重量(铁矿固废排查中确定为固废) */
    @Excel(name = "重量(铁矿固废排查中确定为固废)")
    private String defineWeight;

    /** 金额(铁矿固废排查中确定为固废) */
    @Excel(name = "金额(铁矿固废排查中确定为固废)")
    private String defineAmount;

    /** 批次(放射性检测不合格) */
    @Excel(name = "批次(放射性检测不合格)")
    private String radiationBatch;

    /** 重量(放射性检测不合格) */
    @Excel(name = "重量(放射性检测不合格)")
    private String radiationWeight;

    /** 金额(放射性检测不合格) */
    @Excel(name = "金额(放射性检测不合格)")
    private String radiationAmount;

    /** 批次(总检出固体废物) */
    @Excel(name = "批次(总检出固体废物)")
    private String wasteBatch;

    /** 重量(总检出固体废物) */
    @Excel(name = "重量(总检出固体废物)")
    private String wasteWeight;

    /** 金额(总检出固体废物) */
    @Excel(name = "金额(总检出固体废物)")
    private String wasteAmount;

    /** 批次(外来夹杂物检疫不合格) */
    @Excel(name = "批次(外来夹杂物检疫不合格)")
    private String sundriesBatch;

    /** 重量(外来夹杂物检疫不合格) */
    @Excel(name = "重量(外来夹杂物检疫不合格)")
    private String sundriesWeight;

    /** 金额(外来夹杂物检疫不合格) */
    @Excel(name = "金额(外来夹杂物检疫不合格)")
    private String sundriesAmount;

    /** 批次(品质项目与合同不符) */
    @Excel(name = "批次(品质项目与合同不符)")
    private String contractBatch;

    /** 重量(品质项目与合同不符) */
    @Excel(name = "重量(品质项目与合同不符)")
    private String contractWeight;

    /** 金额(品质项目与合同不符) */
    @Excel(name = "金额(品质项目与合同不符)")
    private String contractAmount;

    /** 不合格处置方式 */
    @Excel(name = "不合格处置方式")
    private String dealMode;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setSusionIronId(Long susionIronId) 
    {
        this.susionIronId = susionIronId;
    }

    public Long getSusionIronId() 
    {
        return susionIronId;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setTotalBatch(String totalBatch) 
    {
        this.totalBatch = totalBatch;
    }

    public String getTotalBatch() 
    {
        return totalBatch;
    }
    public void setTotalWeight(String totalWeight) 
    {
        this.totalWeight = totalWeight;
    }

    public String getTotalWeight() 
    {
        return totalWeight;
    }
    public void setTotalAmount(String totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() 
    {
        return totalAmount;
    }
    public void setSolidWasteBatch(String solidWasteBatch) 
    {
        this.solidWasteBatch = solidWasteBatch;
    }

    public String getSolidWasteBatch() 
    {
        return solidWasteBatch;
    }
    public void setSolidWasteWeight(String solidWasteWeight) 
    {
        this.solidWasteWeight = solidWasteWeight;
    }

    public String getSolidWasteWeight() 
    {
        return solidWasteWeight;
    }
    public void setSolidWasteAmount(String solidWasteAmount) 
    {
        this.solidWasteAmount = solidWasteAmount;
    }

    public String getSolidWasteAmount() 
    {
        return solidWasteAmount;
    }
    public void setDefineBatch(String defineBatch) 
    {
        this.defineBatch = defineBatch;
    }

    public String getDefineBatch() 
    {
        return defineBatch;
    }
    public void setDefineWeight(String defineWeight) 
    {
        this.defineWeight = defineWeight;
    }

    public String getDefineWeight() 
    {
        return defineWeight;
    }
    public void setDefineAmount(String defineAmount) 
    {
        this.defineAmount = defineAmount;
    }

    public String getDefineAmount() 
    {
        return defineAmount;
    }
    public void setRadiationBatch(String radiationBatch) 
    {
        this.radiationBatch = radiationBatch;
    }

    public String getRadiationBatch() 
    {
        return radiationBatch;
    }
    public void setRadiationWeight(String radiationWeight) 
    {
        this.radiationWeight = radiationWeight;
    }

    public String getRadiationWeight() 
    {
        return radiationWeight;
    }
    public void setRadiationAmount(String radiationAmount) 
    {
        this.radiationAmount = radiationAmount;
    }

    public String getRadiationAmount() 
    {
        return radiationAmount;
    }
    public void setWasteBatch(String wasteBatch) 
    {
        this.wasteBatch = wasteBatch;
    }

    public String getWasteBatch() 
    {
        return wasteBatch;
    }
    public void setWasteWeight(String wasteWeight) 
    {
        this.wasteWeight = wasteWeight;
    }

    public String getWasteWeight() 
    {
        return wasteWeight;
    }
    public void setWasteAmount(String wasteAmount) 
    {
        this.wasteAmount = wasteAmount;
    }

    public String getWasteAmount() 
    {
        return wasteAmount;
    }
    public void setSundriesBatch(String sundriesBatch) 
    {
        this.sundriesBatch = sundriesBatch;
    }

    public String getSundriesBatch() 
    {
        return sundriesBatch;
    }
    public void setSundriesWeight(String sundriesWeight) 
    {
        this.sundriesWeight = sundriesWeight;
    }

    public String getSundriesWeight() 
    {
        return sundriesWeight;
    }
    public void setSundriesAmount(String sundriesAmount) 
    {
        this.sundriesAmount = sundriesAmount;
    }

    public String getSundriesAmount() 
    {
        return sundriesAmount;
    }
    public void setContractBatch(String contractBatch) 
    {
        this.contractBatch = contractBatch;
    }

    public String getContractBatch() 
    {
        return contractBatch;
    }
    public void setContractWeight(String contractWeight) 
    {
        this.contractWeight = contractWeight;
    }

    public String getContractWeight() 
    {
        return contractWeight;
    }
    public void setContractAmount(String contractAmount) 
    {
        this.contractAmount = contractAmount;
    }

    public String getContractAmount() 
    {
        return contractAmount;
    }
    public void setDealMode(String dealMode) 
    {
        this.dealMode = dealMode;
    }

    public String getDealMode() 
    {
        return dealMode;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("susionIronId", getSusionIronId())
            .append("goodsCategory", getGoodsCategory())
            .append("totalBatch", getTotalBatch())
            .append("totalWeight", getTotalWeight())
            .append("totalAmount", getTotalAmount())
            .append("solidWasteBatch", getSolidWasteBatch())
            .append("solidWasteWeight", getSolidWasteWeight())
            .append("solidWasteAmount", getSolidWasteAmount())
            .append("defineBatch", getDefineBatch())
            .append("defineWeight", getDefineWeight())
            .append("defineAmount", getDefineAmount())
            .append("radiationBatch", getRadiationBatch())
            .append("radiationWeight", getRadiationWeight())
            .append("radiationAmount", getRadiationAmount())
            .append("wasteBatch", getWasteBatch())
            .append("wasteWeight", getWasteWeight())
            .append("wasteAmount", getWasteAmount())
            .append("sundriesBatch", getSundriesBatch())
            .append("sundriesWeight", getSundriesWeight())
            .append("sundriesAmount", getSundriesAmount())
            .append("contractBatch", getContractBatch())
            .append("contractWeight", getContractWeight())
            .append("contractAmount", getContractAmount())
            .append("dealMode", getDealMode())
            .append("remarks", getRemarks())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
