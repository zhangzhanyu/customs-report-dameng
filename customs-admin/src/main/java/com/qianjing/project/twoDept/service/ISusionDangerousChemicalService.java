package com.qianjing.project.twoDept.service;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;

/**
 * 危险化学品及其包装检验监管情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-12
 */
public interface ISusionDangerousChemicalService 
{
    /**
     * 查询危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemicalId 危险化学品及其包装检验监管情况统计主键
     * @return 危险化学品及其包装检验监管情况统计
     */
    public SusionDangerousChemical selectSusionDangerousChemicalBySusionDangerousChemicalId(Long susionDangerousChemicalId);

    /**
     * 查询危险化学品及其包装检验监管情况统计列表
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 危险化学品及其包装检验监管情况统计集合
     */
    public List<SusionDangerousChemical> selectSusionDangerousChemicalList(SusionDangerousChemical susionDangerousChemical);

    /**
     * 新增危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 结果
     */
    public int insertSusionDangerousChemical(SusionDangerousChemical susionDangerousChemical);

    /**
     * 修改危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 结果
     */
    public int updateSusionDangerousChemical(SusionDangerousChemical susionDangerousChemical);

    /**
     * 批量删除危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemicalIds 需要删除的危险化学品及其包装检验监管情况统计主键集合
     * @return 结果
     */
    public int deleteSusionDangerousChemicalBySusionDangerousChemicalIds(Long[] susionDangerousChemicalIds);

    /**
     * 删除危险化学品及其包装检验监管情况统计信息
     * 
     * @param susionDangerousChemicalId 危险化学品及其包装检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionDangerousChemicalBySusionDangerousChemicalId(Long susionDangerousChemicalId);

    /**
     * 导入危险化学品及其包装检验监管情况统计信息
     *
     * @param susionDangerousChemicalList 危险化学品及其包装检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionDangerousChemical(List<SusionDangerousChemical> susionDangerousChemicalList, Boolean isUpdateSupport, String operName);

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    public SusionDangerousChemical checkEntryIdGnoUnique(String entryId, Long gNo);

    /**
     * 导入危险化学品及其包装检验监管情况统计信息
     *
     * @param susionDangerousChemicalList 批量数据
     * @return 结果
     */
    public int batchInsertSusionDangerousChemicalList(List<SusionDangerousChemical> susionDangerousChemicalList);
}
