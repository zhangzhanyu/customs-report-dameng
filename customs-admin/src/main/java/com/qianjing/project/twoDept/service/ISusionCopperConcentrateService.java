package com.qianjing.project.twoDept.service;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionCopperConcentrate;

/**
 * 进口铜精矿检验监管情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-12
 */
public interface ISusionCopperConcentrateService 
{
    /**
     * 查询进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrateId 进口铜精矿检验监管情况统计主键
     * @return 进口铜精矿检验监管情况统计
     */
    public SusionCopperConcentrate selectSusionCopperConcentrateBySusionCopperConcentrateId(Long susionCopperConcentrateId);

    /**
     * 查询进口铜精矿检验监管情况统计列表
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 进口铜精矿检验监管情况统计集合
     */
    public List<SusionCopperConcentrate> selectSusionCopperConcentrateList(SusionCopperConcentrate susionCopperConcentrate);

    /**
     * 新增进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 结果
     */
    public int insertSusionCopperConcentrate(SusionCopperConcentrate susionCopperConcentrate);

    /**
     * 修改进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 结果
     */
    public int updateSusionCopperConcentrate(SusionCopperConcentrate susionCopperConcentrate);

    /**
     * 批量删除进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrateIds 需要删除的进口铜精矿检验监管情况统计主键集合
     * @return 结果
     */
    public int deleteSusionCopperConcentrateBySusionCopperConcentrateIds(Long[] susionCopperConcentrateIds);

    /**
     * 删除进口铜精矿检验监管情况统计信息
     * 
     * @param susionCopperConcentrateId 进口铜精矿检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionCopperConcentrateBySusionCopperConcentrateId(Long susionCopperConcentrateId);

    /**
     * 导入进口铜精矿检验监管情况统计信息
     *
     * @param susionCopperConcentrateList 进口铜精矿检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionCopperConcentrate(List<SusionCopperConcentrate> susionCopperConcentrateList, Boolean isUpdateSupport, String operName);
}
