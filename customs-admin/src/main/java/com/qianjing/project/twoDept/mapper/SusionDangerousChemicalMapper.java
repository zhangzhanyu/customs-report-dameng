package com.qianjing.project.twoDept.mapper;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;
import org.apache.ibatis.annotations.Param;

/**
 * 危险化学品及其包装检验监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-12
 */
public interface SusionDangerousChemicalMapper 
{
    /**
     * 查询危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemicalId 危险化学品及其包装检验监管情况统计主键
     * @return 危险化学品及其包装检验监管情况统计
     */
    public SusionDangerousChemical selectSusionDangerousChemicalBySusionDangerousChemicalId(Long susionDangerousChemicalId);

    /**
     * 查询危险化学品及其包装检验监管情况统计列表
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 危险化学品及其包装检验监管情况统计集合
     */
    public List<SusionDangerousChemical> selectSusionDangerousChemicalList(SusionDangerousChemical susionDangerousChemical);

    /**
     * 新增危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 结果
     */
    public int insertSusionDangerousChemical(SusionDangerousChemical susionDangerousChemical);

    /**
     * 修改危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 结果
     */
    public int updateSusionDangerousChemical(SusionDangerousChemical susionDangerousChemical);

    /**
     * 删除危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemicalId 危险化学品及其包装检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionDangerousChemicalBySusionDangerousChemicalId(Long susionDangerousChemicalId);

    /**
     * 批量删除危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemicalIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionDangerousChemicalBySusionDangerousChemicalIds(Long[] susionDangerousChemicalIds);

    public SusionDangerousChemical checkEntryIdGnoUnique(@Param("entryId") String entryId, @Param("gNo") Long gNo);

    public int batchInsertSusionDangerousChemical(List<SusionDangerousChemical> susionDangerousChemicalList);
}
