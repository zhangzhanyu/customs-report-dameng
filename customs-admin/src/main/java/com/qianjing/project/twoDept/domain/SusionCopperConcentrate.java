package com.qianjing.project.twoDept.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口铜精矿检验监管情况统计对象 susion_copper_concentrate
 * 
 * @author qianjing
 * @date 2022-05-19
 */
public class SusionCopperConcentrate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 监管情况编号 */
    private Long susionCopperConcentrateId;

    /** 报关单号 */
    @Excel(name = "报关单号")
    private String customsCode;

    /** 运输方式 */
    @Excel(name = "运输方式")
    private String transportType;

    /** 原产国 */
    @Excel(name = "原产国")
    private String originCountry;

    /** 入境口岸 */
    @Excel(name = "入境口岸")
    private String entryPort;

    /** 收货人 */
    @Excel(name = "收货人")
    private String consignee;

    /** 重量 */
    @Excel(name = "重量")
    private String weight;

    /** 货值 */
    @Excel(name = "货值")
    private String cargoValue;

    /** 放射性检测结果 */
    @Excel(name = "放射性检测结果")
    private String radiationResult;

    /** 外来夹杂物情况 */
    @Excel(name = "外来夹杂物情况")
    private String sundriesSituation;

    /** 固废排查情况 */
    @Excel(name = "固废排查情况")
    private String wasteSituation;

    /** 铅 */
    @Excel(name = "铅")
    private String pb;

    /** 砷 */
    @Excel(name = "砷")
    private String s;

    /** 氟 */
    @Excel(name = "氟")
    private String f;

    /** 镉 */
    @Excel(name = "镉")
    private String cd;

    /** 汞 */
    @Excel(name = "汞")
    private String hg;

    /** 铜 */
    @Excel(name = "铜")
    private String cu;

    /** 结果判定 */
    @Excel(name = "结果判定")
    private String resultJudge;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setSusionCopperConcentrateId(Long susionCopperConcentrateId) 
    {
        this.susionCopperConcentrateId = susionCopperConcentrateId;
    }

    public Long getSusionCopperConcentrateId() 
    {
        return susionCopperConcentrateId;
    }
    public void setCustomsCode(String customsCode) 
    {
        this.customsCode = customsCode;
    }

    public String getCustomsCode() 
    {
        return customsCode;
    }
    public void setTransportType(String transportType) 
    {
        this.transportType = transportType;
    }

    public String getTransportType() 
    {
        return transportType;
    }
    public void setOriginCountry(String originCountry) 
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry() 
    {
        return originCountry;
    }
    public void setEntryPort(String entryPort) 
    {
        this.entryPort = entryPort;
    }

    public String getEntryPort() 
    {
        return entryPort;
    }
    public void setConsignee(String consignee) 
    {
        this.consignee = consignee;
    }

    public String getConsignee() 
    {
        return consignee;
    }
    public void setWeight(String weight) 
    {
        this.weight = weight;
    }

    public String getWeight() 
    {
        return weight;
    }
    public void setCargoValue(String cargoValue) 
    {
        this.cargoValue = cargoValue;
    }

    public String getCargoValue() 
    {
        return cargoValue;
    }
    public void setRadiationResult(String radiationResult) 
    {
        this.radiationResult = radiationResult;
    }

    public String getRadiationResult() 
    {
        return radiationResult;
    }
    public void setSundriesSituation(String sundriesSituation) 
    {
        this.sundriesSituation = sundriesSituation;
    }

    public String getSundriesSituation() 
    {
        return sundriesSituation;
    }
    public void setWasteSituation(String wasteSituation) 
    {
        this.wasteSituation = wasteSituation;
    }

    public String getWasteSituation() 
    {
        return wasteSituation;
    }
    public void setPb(String pb) 
    {
        this.pb = pb;
    }

    public String getPb() 
    {
        return pb;
    }
    public void setS(String s) 
    {
        this.s = s;
    }

    public String getS() 
    {
        return s;
    }
    public void setF(String f) 
    {
        this.f = f;
    }

    public String getF() 
    {
        return f;
    }
    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setHg(String hg) 
    {
        this.hg = hg;
    }

    public String getHg() 
    {
        return hg;
    }
    public void setCu(String cu) 
    {
        this.cu = cu;
    }

    public String getCu() 
    {
        return cu;
    }
    public void setResultJudge(String resultJudge) 
    {
        this.resultJudge = resultJudge;
    }

    public String getResultJudge() 
    {
        return resultJudge;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("susionCopperConcentrateId", getSusionCopperConcentrateId())
            .append("customsCode", getCustomsCode())
            .append("transportType", getTransportType())
            .append("originCountry", getOriginCountry())
            .append("entryPort", getEntryPort())
            .append("consignee", getConsignee())
            .append("weight", getWeight())
            .append("cargoValue", getCargoValue())
            .append("radiationResult", getRadiationResult())
            .append("sundriesSituation", getSundriesSituation())
            .append("wasteSituation", getWasteSituation())
            .append("pb", getPb())
            .append("s", getS())
            .append("f", getF())
            .append("cd", getCd())
            .append("hg", getHg())
            .append("cu", getCu())
            .append("resultJudge", getResultJudge())
            .append("remarks", getRemarks())
            .toString();
    }
}
