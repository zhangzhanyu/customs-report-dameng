package com.qianjing.project.twoDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.twoDept.domain.SusionMine;
import com.qianjing.project.twoDept.service.ISusionMineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口矿产品“先放后检”情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-16
 */
@RestController
@RequestMapping("/twoDept/susionMine")
public class SusionMineController extends BaseController
{
    @Autowired
    private ISusionMineService susionMineService;

    /**
     * 查询进口矿产品“先放后检”情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMine:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionMine susionMine)
    {
        startPage();
        List<SusionMine> list = susionMineService.selectSusionMineList(susionMine);
        return getDataTable(list);
    }

    /**
     * 导出进口矿产品“先放后检”情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMine:export')")
    @Log(title = "进口矿产品“先放后检”情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionMine susionMine)
    {
        List<SusionMine> list = susionMineService.selectSusionMineList(susionMine);
        ExcelUtil<SusionMine> util = new ExcelUtil<SusionMine>(SusionMine.class);
        return util.exportExcel(list, "进口矿产品“先放后检”情况统计数据");
    }

    /**
     * 导入进口矿产品“先放后检”情况统计列表
     */
    @Log(title = "进口矿产品“先放后检”情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('twoDept:susionMine:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionMine> util = new ExcelUtil<SusionMine>(SusionMine.class);
        List<SusionMine> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionMineService.importSusionMine(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口矿产品“先放后检”情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionMine> util = new ExcelUtil<SusionMine>(SusionMine.class);
        util.importTemplateExcel(response, "进口矿产品“先放后检”情况统计数据");
    }

    /**
     * 获取进口矿产品“先放后检”情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMine:query')")
    @GetMapping(value = "/{susionMineId}")
    public AjaxResult getInfo(@PathVariable("susionMineId") Long susionMineId)
    {
        return AjaxResult.success(susionMineService.selectSusionMineBySusionMineId(susionMineId));
    }

    /**
     * 新增进口矿产品“先放后检”情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMine:add')")
    @Log(title = "进口矿产品“先放后检”情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionMine susionMine)
    {
        return toAjax(susionMineService.insertSusionMine(susionMine));
    }

    /**
     * 修改进口矿产品“先放后检”情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMine:edit')")
    @Log(title = "进口矿产品“先放后检”情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionMine susionMine)
    {
        return toAjax(susionMineService.updateSusionMine(susionMine));
    }

    /**
     * 删除进口矿产品“先放后检”情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionMine:remove')")
    @Log(title = "进口矿产品“先放后检”情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionMineIds}")
    public AjaxResult remove(@PathVariable Long[] susionMineIds)
    {
        return toAjax(susionMineService.deleteSusionMineBySusionMineIds(susionMineIds));
    }
}
