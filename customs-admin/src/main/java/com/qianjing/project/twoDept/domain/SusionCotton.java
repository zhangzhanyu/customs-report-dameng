package com.qianjing.project.twoDept.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口棉花检验情况统计对象 susion_cotton
 * 
 * @author qianjing
 * @date 2022-05-12
 */
public class SusionCotton extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long cottonId;

    /** 直属关 */
    @Excel(name = "直属关")
    private String customs;

    /** 报关日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报关日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date declareTime;

    /** 境外供货企业 */
    @Excel(name = "境外供货企业")
    private String suppliers;

    /** 国内收货人 */
    @Excel(name = "国内收货人")
    private Long consignee;

    /** 原产地 */
    @Excel(name = "原产地")
    private String ycd;

    /** 发票重量 */
    @Excel(name = "发票重量")
    private String fpzl;

    /** 总货值 */
    @Excel(name = "总货值")
    private String zhz;

    /** 品质证书 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "品质证书", width = 30, dateFormat = "yyyy-MM-dd")
    private Date pzzs;

    /** 重量证书 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "重量证书", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zlzs;

    /** 未申请品质证书原因 */
    @Excel(name = "未申请品质证书原因")
    private String yy;

    /** 成交方式 */
    @Excel(name = "成交方式")
    private String cjfs;

    /** 是否完成进口棉花质量监控管理系统录入 */
    private String delFlag;

    public void setCottonId(Long cottonId) 
    {
        this.cottonId = cottonId;
    }

    public Long getCottonId() 
    {
        return cottonId;
    }
    public void setCustoms(String customs) 
    {
        this.customs = customs;
    }

    public String getCustoms() 
    {
        return customs;
    }
    public void setDeclareTime(Date declareTime) 
    {
        this.declareTime = declareTime;
    }

    public Date getDeclareTime() 
    {
        return declareTime;
    }
    public void setSuppliers(String suppliers) 
    {
        this.suppliers = suppliers;
    }

    public String getSuppliers() 
    {
        return suppliers;
    }
    public void setConsignee(Long consignee) 
    {
        this.consignee = consignee;
    }

    public Long getConsignee() 
    {
        return consignee;
    }
    public void setYcd(String ycd) 
    {
        this.ycd = ycd;
    }

    public String getYcd() 
    {
        return ycd;
    }
    public void setFpzl(String fpzl) 
    {
        this.fpzl = fpzl;
    }

    public String getFpzl() 
    {
        return fpzl;
    }
    public void setZhz(String zhz) 
    {
        this.zhz = zhz;
    }

    public String getZhz() 
    {
        return zhz;
    }
    public void setPzzs(Date pzzs) 
    {
        this.pzzs = pzzs;
    }

    public Date getPzzs() 
    {
        return pzzs;
    }
    public void setZlzs(Date zlzs) 
    {
        this.zlzs = zlzs;
    }

    public Date getZlzs() 
    {
        return zlzs;
    }
    public void setYy(String yy) 
    {
        this.yy = yy;
    }

    public String getYy() 
    {
        return yy;
    }
    public void setCjfs(String cjfs) 
    {
        this.cjfs = cjfs;
    }

    public String getCjfs() 
    {
        return cjfs;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cottonId", getCottonId())
            .append("customs", getCustoms())
            .append("declareTime", getDeclareTime())
            .append("suppliers", getSuppliers())
            .append("consignee", getConsignee())
            .append("ycd", getYcd())
            .append("fpzl", getFpzl())
            .append("zhz", getZhz())
            .append("pzzs", getPzzs())
            .append("zlzs", getZlzs())
            .append("yy", getYy())
            .append("cjfs", getCjfs())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
