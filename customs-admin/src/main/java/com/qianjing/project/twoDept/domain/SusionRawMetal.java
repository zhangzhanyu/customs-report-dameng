package com.qianjing.project.twoDept.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 进口再生原料检验监管情况统计对象 susion_raw_metal
 * 
 * @author qianjing
 * @date 2022-05-19
 */
public class SusionRawMetal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long susionRawMetalId;

    /** 商品种类 */
    @Excel(name = "商品种类")
    private String goodsCategory;

    /** 批次 */
    @Excel(name = "批次")
    private String totalBatch;

    /** 重量(万吨) */
    @Excel(name = "重量(万吨)")
    private String totalWeight;

    /** 金额(万美元) */
    @Excel(name = "金额(万美元)")
    private String totalAmount;

    /** 批次(不合格检验情况) */
    @Excel(name = "批次(不合格检验情况)")
    private String unqiedBatch;

    /** 重量(万吨)(不合格检验情况) */
    @Excel(name = "重量(万吨)(不合格检验情况)")
    private String unqiedWeight;

    /** 金额(万美元)(不合格检验情况) */
    @Excel(name = "金额(万美元)(不合格检验情况)")
    private String unqiedAmount;

    /** 不合格情况具体描述 */
    @Excel(name = "不合格情况具体描述")
    private String detailDescription;

    /** 批次(固废鉴别送检总量) */
    @Excel(name = "批次(固废鉴别送检总量)")
    private String totalWasteBatch;

    /** 重量(固废鉴别送检总量) */
    @Excel(name = "重量(固废鉴别送检总量)")
    private String totalWasteWeight;

    /** 金额(固废鉴别送检总量) */
    @Excel(name = "金额(固废鉴别送检总量)")
    private String totalWasteAmount;

    /** 删除标志(0未删 1已删) */
    private String delFlag;

    /** 批次(鉴别为固体废物) */
    @Excel(name = "批次(鉴别为固体废物)")
    private String wasteBatch;

    /** 重量(鉴别为固体废物) */
    @Excel(name = "重量(鉴别为固体废物)")
    private String wasteWeight;

    /** 金额(鉴别为固体废物) */
    @Excel(name = "金额(鉴别为固体废物)")
    private String wasteAmount;

    /** 执行过程中存在的问题 */
    @Excel(name = "执行过程中存在的问题")
    private String problems;

    /** 相关意见和建议 */
    @Excel(name = "相关意见和建议")
    private String proposal;

    public void setSusionRawMetalId(Long susionRawMetalId) 
    {
        this.susionRawMetalId = susionRawMetalId;
    }

    public Long getSusionRawMetalId() 
    {
        return susionRawMetalId;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setTotalBatch(String totalBatch) 
    {
        this.totalBatch = totalBatch;
    }

    public String getTotalBatch() 
    {
        return totalBatch;
    }
    public void setTotalWeight(String totalWeight) 
    {
        this.totalWeight = totalWeight;
    }

    public String getTotalWeight() 
    {
        return totalWeight;
    }
    public void setTotalAmount(String totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() 
    {
        return totalAmount;
    }
    public void setUnqiedBatch(String unqiedBatch) 
    {
        this.unqiedBatch = unqiedBatch;
    }

    public String getUnqiedBatch() 
    {
        return unqiedBatch;
    }
    public void setUnqiedWeight(String unqiedWeight) 
    {
        this.unqiedWeight = unqiedWeight;
    }

    public String getUnqiedWeight() 
    {
        return unqiedWeight;
    }
    public void setUnqiedAmount(String unqiedAmount) 
    {
        this.unqiedAmount = unqiedAmount;
    }

    public String getUnqiedAmount() 
    {
        return unqiedAmount;
    }
    public void setDetailDescription(String detailDescription) 
    {
        this.detailDescription = detailDescription;
    }

    public String getDetailDescription() 
    {
        return detailDescription;
    }
    public void setTotalWasteBatch(String totalWasteBatch) 
    {
        this.totalWasteBatch = totalWasteBatch;
    }

    public String getTotalWasteBatch() 
    {
        return totalWasteBatch;
    }
    public void setTotalWasteWeight(String totalWasteWeight) 
    {
        this.totalWasteWeight = totalWasteWeight;
    }

    public String getTotalWasteWeight() 
    {
        return totalWasteWeight;
    }
    public void setTotalWasteAmount(String totalWasteAmount) 
    {
        this.totalWasteAmount = totalWasteAmount;
    }

    public String getTotalWasteAmount() 
    {
        return totalWasteAmount;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setWasteBatch(String wasteBatch) 
    {
        this.wasteBatch = wasteBatch;
    }

    public String getWasteBatch() 
    {
        return wasteBatch;
    }
    public void setWasteWeight(String wasteWeight) 
    {
        this.wasteWeight = wasteWeight;
    }

    public String getWasteWeight() 
    {
        return wasteWeight;
    }
    public void setWasteAmount(String wasteAmount) 
    {
        this.wasteAmount = wasteAmount;
    }

    public String getWasteAmount() 
    {
        return wasteAmount;
    }
    public void setProblems(String problems) 
    {
        this.problems = problems;
    }

    public String getProblems() 
    {
        return problems;
    }
    public void setProposal(String proposal) 
    {
        this.proposal = proposal;
    }

    public String getProposal() 
    {
        return proposal;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("susionRawMetalId", getSusionRawMetalId())
            .append("goodsCategory", getGoodsCategory())
            .append("totalBatch", getTotalBatch())
            .append("totalWeight", getTotalWeight())
            .append("totalAmount", getTotalAmount())
            .append("unqiedBatch", getUnqiedBatch())
            .append("unqiedWeight", getUnqiedWeight())
            .append("unqiedAmount", getUnqiedAmount())
            .append("detailDescription", getDetailDescription())
            .append("createTime", getCreateTime())
            .append("totalWasteBatch", getTotalWasteBatch())
            .append("updateTime", getUpdateTime())
            .append("totalWasteWeight", getTotalWasteWeight())
            .append("createBy", getCreateBy())
            .append("totalWasteAmount", getTotalWasteAmount())
            .append("updateBy", getUpdateBy())
            .append("delFlag", getDelFlag())
            .append("wasteBatch", getWasteBatch())
            .append("wasteWeight", getWasteWeight())
            .append("wasteAmount", getWasteAmount())
            .append("problems", getProblems())
            .append("proposal", getProposal())
            .toString();
    }
}
