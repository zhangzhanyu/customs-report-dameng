package com.qianjing.project.twoDept.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 危险化学品及其包装检验监管情况统计对象 susion_dangerous_chemical
 *
 * @author qianjing
 * @date 2022-07-21
 */
public class SusionDangerousChemical extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long susionDangerousChemicalId;

    /** 进出口 */
    @Excel(name = "进出口")
    private String iEFlag;

    /** 报关单号 */
    @Excel(name = "报关单号")
    private String entryId;

    /** 项号 */
    @Excel(name = "项号")
    private Long gNo;

    /** 货物属性代码 */
    @Excel(name = "货物属性代码")
    private String productCharCode;

    /** HS编码 */
    @Excel(name = "HS编码")
    private String codeTs;

    /** CIQ编码 */
    @Excel(name = "CIQ编码")
    private String iqCode;

    /** 运输方式 */
    @Excel(name = "运输方式")
    private String trafMode;

    /** 启用国别/地区 */
    @Excel(name = "启用国别/地区")
    private String originCountry;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String gName;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String gModel;

    /** 对应《危险化学品目录》名称 */
    @Excel(name = "对应《危险化学品目录》名称")
    private String correspondingName;

    /** UN编号 */
    @Excel(name = "UN编号")
    private String ungid;

    /** 第一(法定)数量 */
    @Excel(name = "第一(法定)数量")
    private Double qty1;

    /** 第一(法定)计量单位 */
    @Excel(name = "第一(法定)计量单位")
    private String unit1;

    /** 备注 */
    @Excel(name = "备注", readConverterExp = "1=非危险品,2=不合格")
    private String remarks;

    /** 货值(美元) */
    @Excel(name = "货值(美元)")
    private Double usdPrice;

    /** 货值(人民币) */
    @Excel(name = "货值(人民币)")
    private Double rmbPrice;

    /** 商品项查验结果 */
    @Excel(name = "商品项查验结果", readConverterExp = "0=异常,1=无异常,2=无法确认")
    private String checkResult;

    /** 不合格类别 */
    @Excel(name = "不合格类别", readConverterExp = "1=标签,2=安全技术说明书,3=包装,4=品质,5=数量,6=重量,7=内容物特性与申报信息不符,8=其他")
    private String nonconformityCategory;

    /** 处置 */
    @Excel(name = "处置", readConverterExp = "1=技术整改,2=退运,3=使用救助包装,4=降级,5=无害化处理,6=其他")
    private String disposal;

    /** 放行时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "放行时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date releaseTime;

    public void setSusionDangerousChemicalId(Long susionDangerousChemicalId)
    {
        this.susionDangerousChemicalId = susionDangerousChemicalId;
    }

    public Long getSusionDangerousChemicalId()
    {
        return susionDangerousChemicalId;
    }
    public void setiEFlag(String iEFlag)
    {
        this.iEFlag = iEFlag;
    }

    public String getiEFlag()
    {
        return iEFlag;
    }
    public void setEntryId(String entryId)
    {
        this.entryId = entryId;
    }

    public String getEntryId()
    {
        return entryId;
    }
    public void setgNo(Long gNo)
    {
        this.gNo = gNo;
    }

    public Long getgNo()
    {
        return gNo;
    }
    public void setProductCharCode(String productCharCode)
    {
        this.productCharCode = productCharCode;
    }

    public String getProductCharCode()
    {
        return productCharCode;
    }
    public void setCodeTs(String codeTs)
    {
        this.codeTs = codeTs;
    }

    public String getCodeTs()
    {
        return codeTs;
    }
    public void setIqCode(String iqCode)
    {
        this.iqCode = iqCode;
    }

    public String getIqCode()
    {
        return iqCode;
    }
    public void setTrafMode(String trafMode)
    {
        this.trafMode = trafMode;
    }

    public String getTrafMode()
    {
        return trafMode;
    }
    public void setOriginCountry(String originCountry)
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry()
    {
        return originCountry;
    }
    public void setgName(String gName)
    {
        this.gName = gName;
    }

    public String getgName()
    {
        return gName;
    }
    public void setgModel(String gModel)
    {
        this.gModel = gModel;
    }

    public String getgModel()
    {
        return gModel;
    }
    public void setCorrespondingName(String correspondingName)
    {
        this.correspondingName = correspondingName;
    }

    public String getCorrespondingName()
    {
        return correspondingName;
    }
    public void setUngid(String ungid)
    {
        this.ungid = ungid;
    }

    public String getUngid()
    {
        return ungid;
    }
    public void setQty1(Double qty1)
    {
        this.qty1 = qty1;
    }

    public Double getQty1()
    {
        return qty1;
    }
    public void setUnit1(String unit1)
    {
        this.unit1 = unit1;
    }

    public String getUnit1()
    {
        return unit1;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setUsdPrice(Double usdPrice)
    {
        this.usdPrice = usdPrice;
    }

    public Double getUsdPrice()
    {
        return usdPrice;
    }
    public void setRmbPrice(Double rmbPrice)
    {
        this.rmbPrice = rmbPrice;
    }

    public Double getRmbPrice()
    {
        return rmbPrice;
    }
    public void setCheckResult(String checkResult)
    {
        this.checkResult = checkResult;
    }

    public String getCheckResult()
    {
        return checkResult;
    }
    public void setNonconformityCategory(String nonconformityCategory)
    {
        this.nonconformityCategory = nonconformityCategory;
    }

    public String getNonconformityCategory()
    {
        return nonconformityCategory;
    }
    public void setDisposal(String disposal)
    {
        this.disposal = disposal;
    }

    public String getDisposal()
    {
        return disposal;
    }
    public void setReleaseTime(Date releaseTime)
    {
        this.releaseTime = releaseTime;
    }

    public Date getReleaseTime()
    {
        return releaseTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("susionDangerousChemicalId", getSusionDangerousChemicalId())
                .append("iEFlag", getiEFlag())
                .append("entryId", getEntryId())
                .append("gNo", getgNo())
                .append("productCharCode", getProductCharCode())
                .append("codeTs", getCodeTs())
                .append("iqCode", getIqCode())
                .append("trafMode", getTrafMode())
                .append("originCountry", getOriginCountry())
                .append("gName", getgName())
                .append("gModel", getgModel())
                .append("correspondingName", getCorrespondingName())
                .append("ungid", getUngid())
                .append("qty1", getQty1())
                .append("unit1", getUnit1())
                .append("remarks", getRemarks())
                .append("usdPrice", getUsdPrice())
                .append("rmbPrice", getRmbPrice())
                .append("checkResult", getCheckResult())
                .append("nonconformityCategory", getNonconformityCategory())
                .append("disposal", getDisposal())
                .append("releaseTime", getReleaseTime())
                .toString();
    }
}
