package com.qianjing.project.twoDept.mapper;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionCopperConcentrate;

/**
 * 进口铜精矿检验监管情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-12
 */
public interface SusionCopperConcentrateMapper 
{
    /**
     * 查询进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrateId 进口铜精矿检验监管情况统计主键
     * @return 进口铜精矿检验监管情况统计
     */
    public SusionCopperConcentrate selectSusionCopperConcentrateBySusionCopperConcentrateId(Long susionCopperConcentrateId);

    /**
     * 查询进口铜精矿检验监管情况统计列表
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 进口铜精矿检验监管情况统计集合
     */
    public List<SusionCopperConcentrate> selectSusionCopperConcentrateList(SusionCopperConcentrate susionCopperConcentrate);

    /**
     * 新增进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 结果
     */
    public int insertSusionCopperConcentrate(SusionCopperConcentrate susionCopperConcentrate);

    /**
     * 修改进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrate 进口铜精矿检验监管情况统计
     * @return 结果
     */
    public int updateSusionCopperConcentrate(SusionCopperConcentrate susionCopperConcentrate);

    /**
     * 删除进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrateId 进口铜精矿检验监管情况统计主键
     * @return 结果
     */
    public int deleteSusionCopperConcentrateBySusionCopperConcentrateId(Long susionCopperConcentrateId);

    /**
     * 批量删除进口铜精矿检验监管情况统计
     * 
     * @param susionCopperConcentrateIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionCopperConcentrateBySusionCopperConcentrateIds(Long[] susionCopperConcentrateIds);
}
