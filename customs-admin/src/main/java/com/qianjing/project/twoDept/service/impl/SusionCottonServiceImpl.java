package com.qianjing.project.twoDept.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.twoDept.mapper.SusionCottonMapper;
import com.qianjing.project.twoDept.domain.SusionCotton;
import com.qianjing.project.twoDept.service.ISusionCottonService;

/**
 * 进口棉花检验情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-12
 */
@Service
public class SusionCottonServiceImpl implements ISusionCottonService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionCottonServiceImpl.class);

    @Autowired
    private SusionCottonMapper susionCottonMapper;

    /**
     * 查询进口棉花检验情况统计
     * 
     * @param cottonId 进口棉花检验情况统计主键
     * @return 进口棉花检验情况统计
     */
    @Override
    public SusionCotton selectSusionCottonByCottonId(Long cottonId)
    {
        return susionCottonMapper.selectSusionCottonByCottonId(cottonId);
    }

    /**
     * 查询进口棉花检验情况统计列表
     * 
     * @param susionCotton 进口棉花检验情况统计
     * @return 进口棉花检验情况统计
     */
    @Override
    public List<SusionCotton> selectSusionCottonList(SusionCotton susionCotton)
    {
        return susionCottonMapper.selectSusionCottonList(susionCotton);
    }

    /**
     * 新增进口棉花检验情况统计
     * 
     * @param susionCotton 进口棉花检验情况统计
     * @return 结果
     */
    @Override
    public int insertSusionCotton(SusionCotton susionCotton)
    {
        return susionCottonMapper.insertSusionCotton(susionCotton);
    }

    /**
     * 修改进口棉花检验情况统计
     * 
     * @param susionCotton 进口棉花检验情况统计
     * @return 结果
     */
    @Override
    public int updateSusionCotton(SusionCotton susionCotton)
    {
        return susionCottonMapper.updateSusionCotton(susionCotton);
    }

    /**
     * 批量删除进口棉花检验情况统计
     * 
     * @param cottonIds 需要删除的进口棉花检验情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionCottonByCottonIds(Long[] cottonIds)
    {
        return susionCottonMapper.deleteSusionCottonByCottonIds(cottonIds);
    }

    /**
     * 删除进口棉花检验情况统计信息
     * 
     * @param cottonId 进口棉花检验情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionCottonByCottonId(Long cottonId)
    {
        return susionCottonMapper.deleteSusionCottonByCottonId(cottonId);
    }

    /**
     * 导入进口棉花检验情况统计数据
     *
     * @param susionCottonList susionCottonList 进口棉花检验情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionCotton(List<SusionCotton> susionCottonList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionCottonList) || susionCottonList.size() == 0) {
            throw new ServiceException("导入进口棉花检验情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionCotton susionCotton : susionCottonList){
            try {
                // 验证是否存在
                if (true) {
                    susionCotton.setCreateBy(operName);
                    this.insertSusionCotton(susionCotton);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionCotton.setUpdateBy(operName);
                    this.updateSusionCotton(susionCotton);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
