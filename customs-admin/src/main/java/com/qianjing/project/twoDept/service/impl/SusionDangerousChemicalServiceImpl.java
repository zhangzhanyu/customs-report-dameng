package com.qianjing.project.twoDept.service.impl;

import java.util.List;

import com.qianjing.common.constant.Constants;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.twoDept.mapper.SusionDangerousChemicalMapper;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;
import com.qianjing.project.twoDept.service.ISusionDangerousChemicalService;

/**
 * 危险化学品及其包装检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-12
 */
@Service
public class SusionDangerousChemicalServiceImpl implements ISusionDangerousChemicalService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionDangerousChemicalServiceImpl.class);

    @Autowired
    private SusionDangerousChemicalMapper susionDangerousChemicalMapper;

    /**
     * 查询危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemicalId 危险化学品及其包装检验监管情况统计主键
     * @return 危险化学品及其包装检验监管情况统计
     */
    @Override
    public SusionDangerousChemical selectSusionDangerousChemicalBySusionDangerousChemicalId(Long susionDangerousChemicalId)
    {
        return susionDangerousChemicalMapper.selectSusionDangerousChemicalBySusionDangerousChemicalId(susionDangerousChemicalId);
    }

    /**
     * 查询危险化学品及其包装检验监管情况统计列表
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 危险化学品及其包装检验监管情况统计
     */
    @Override
    public List<SusionDangerousChemical> selectSusionDangerousChemicalList(SusionDangerousChemical susionDangerousChemical)
    {
        return susionDangerousChemicalMapper.selectSusionDangerousChemicalList(susionDangerousChemical);
    }

    /**
     * 新增危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionDangerousChemical(SusionDangerousChemical susionDangerousChemical)
    {
        susionDangerousChemical.setCreateTime(DateUtils.getNowDate());
        return susionDangerousChemicalMapper.insertSusionDangerousChemical(susionDangerousChemical);
    }

    /**
     * 修改危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemical 危险化学品及其包装检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionDangerousChemical(SusionDangerousChemical susionDangerousChemical)
    {
        susionDangerousChemical.setUpdateTime(DateUtils.getNowDate());
        return susionDangerousChemicalMapper.updateSusionDangerousChemical(susionDangerousChemical);
    }

    /**
     * 批量删除危险化学品及其包装检验监管情况统计
     * 
     * @param susionDangerousChemicalIds 需要删除的危险化学品及其包装检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionDangerousChemicalBySusionDangerousChemicalIds(Long[] susionDangerousChemicalIds)
    {
        return susionDangerousChemicalMapper.deleteSusionDangerousChemicalBySusionDangerousChemicalIds(susionDangerousChemicalIds);
    }

    /**
     * 删除危险化学品及其包装检验监管情况统计信息
     * 
     * @param susionDangerousChemicalId 危险化学品及其包装检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionDangerousChemicalBySusionDangerousChemicalId(Long susionDangerousChemicalId)
    {
        return susionDangerousChemicalMapper.deleteSusionDangerousChemicalBySusionDangerousChemicalId(susionDangerousChemicalId);
    }

    /**
     * 导入危险化学品及其包装检验监管情况统计数据
     *
     * @param susionDangerousChemicalList susionDangerousChemicalList 危险化学品及其包装检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionDangerousChemical(List<SusionDangerousChemical> susionDangerousChemicalList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionDangerousChemicalList) || susionDangerousChemicalList.size() == 0) {
            throw new ServiceException("导入危险化学品及其包装检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionDangerousChemical susionDangerousChemical : susionDangerousChemicalList){
            try {
                // 验证是否存在
                if (true) {
                    susionDangerousChemical.setCreateBy(operName);
                    this.insertSusionDangerousChemical(susionDangerousChemical);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionDangerousChemical.setUpdateBy(operName);
                    this.updateSusionDangerousChemical(susionDangerousChemical);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    @Override
    public SusionDangerousChemical checkEntryIdGnoUnique(String entryId, Long gNo) {
        return susionDangerousChemicalMapper.checkEntryIdGnoUnique(entryId, gNo);
    }

    /**
     * 导入危险化学品及其包装检验监管情况统计信息
     *
     * @param susionDangerousChemicalList 批量数据
     * @return 结果
     */
    @Override
    public int batchInsertSusionDangerousChemicalList(List<SusionDangerousChemical> susionDangerousChemicalList) {
        return susionDangerousChemicalMapper.batchInsertSusionDangerousChemical(susionDangerousChemicalList);
    }


}
