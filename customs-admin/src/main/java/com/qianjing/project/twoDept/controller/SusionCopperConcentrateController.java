package com.qianjing.project.twoDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.twoDept.domain.SusionCopperConcentrate;
import com.qianjing.project.twoDept.service.ISusionCopperConcentrateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口铜精矿检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-12
 */
@RestController
@RequestMapping("/twoDept/susionConcentrate")
public class SusionCopperConcentrateController extends BaseController
{
    @Autowired
    private ISusionCopperConcentrateService susionCopperConcentrateService;

    /**
     * 查询进口铜精矿检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionConcentrate:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionCopperConcentrate susionCopperConcentrate)
    {
        startPage();
        List<SusionCopperConcentrate> list = susionCopperConcentrateService.selectSusionCopperConcentrateList(susionCopperConcentrate);
        return getDataTable(list);
    }

    /**
     * 导出进口铜精矿检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionConcentrate:export')")
    @Log(title = "进口铜精矿检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionCopperConcentrate susionCopperConcentrate)
    {
        List<SusionCopperConcentrate> list = susionCopperConcentrateService.selectSusionCopperConcentrateList(susionCopperConcentrate);
        ExcelUtil<SusionCopperConcentrate> util = new ExcelUtil<SusionCopperConcentrate>(SusionCopperConcentrate.class);
        return util.exportExcel(list, "进口铜精矿检验监管情况统计数据");
    }

    /**
     * 导入进口铜精矿检验监管情况统计列表
     */
    @Log(title = "进口铜精矿检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('twoDept:susionConcentrate:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionCopperConcentrate> util = new ExcelUtil<SusionCopperConcentrate>(SusionCopperConcentrate.class);
        List<SusionCopperConcentrate> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionCopperConcentrateService.importSusionCopperConcentrate(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口铜精矿检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionCopperConcentrate> util = new ExcelUtil<SusionCopperConcentrate>(SusionCopperConcentrate.class);
        util.importTemplateExcel(response, "进口铜精矿检验监管情况统计数据");
    }

    /**
     * 获取进口铜精矿检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionConcentrate:query')")
    @GetMapping(value = "/{susionCopperConcentrateId}")
    public AjaxResult getInfo(@PathVariable("susionCopperConcentrateId") Long susionCopperConcentrateId)
    {
        return AjaxResult.success(susionCopperConcentrateService.selectSusionCopperConcentrateBySusionCopperConcentrateId(susionCopperConcentrateId));
    }

    /**
     * 新增进口铜精矿检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionConcentrate:add')")
    @Log(title = "进口铜精矿检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionCopperConcentrate susionCopperConcentrate)
    {
        return toAjax(susionCopperConcentrateService.insertSusionCopperConcentrate(susionCopperConcentrate));
    }

    /**
     * 修改进口铜精矿检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionConcentrate:edit')")
    @Log(title = "进口铜精矿检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionCopperConcentrate susionCopperConcentrate)
    {
        return toAjax(susionCopperConcentrateService.updateSusionCopperConcentrate(susionCopperConcentrate));
    }

    /**
     * 删除进口铜精矿检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionConcentrate:remove')")
    @Log(title = "进口铜精矿检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionCopperConcentrateIds}")
    public AjaxResult remove(@PathVariable Long[] susionCopperConcentrateIds)
    {
        return toAjax(susionCopperConcentrateService.deleteSusionCopperConcentrateBySusionCopperConcentrateIds(susionCopperConcentrateIds));
    }
}
