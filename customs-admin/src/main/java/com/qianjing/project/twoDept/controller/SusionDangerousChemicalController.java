package com.qianjing.project.twoDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;
import com.qianjing.project.twoDept.service.ISusionDangerousChemicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 危险化学品及其包装检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/twoDept/susionChemical")
public class SusionDangerousChemicalController extends BaseController
{
    @Autowired
    private ISusionDangerousChemicalService susionDangerousChemicalService;

    /**
     * 查询危险化学品及其包装检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionChemical:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionDangerousChemical susionDangerousChemical)
    {
        startPage();
        List<SusionDangerousChemical> list = susionDangerousChemicalService.selectSusionDangerousChemicalList(susionDangerousChemical);
        return getDataTable(list);
    }

    /**
     * 导出危险化学品及其包装检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionChemical:export')")
    @Log(title = "危险化学品及其包装检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionDangerousChemical susionDangerousChemical)
    {
        List<SusionDangerousChemical> list = susionDangerousChemicalService.selectSusionDangerousChemicalList(susionDangerousChemical);
        ExcelUtil<SusionDangerousChemical> util = new ExcelUtil<SusionDangerousChemical>(SusionDangerousChemical.class);
        return util.exportExcel(list, "危险化学品及其包装检验监管情况统计数据");
    }

    /**
     * 导入危险化学品及其包装检验监管情况统计列表
     */
    @Log(title = "危险化学品及其包装检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('twoDept:susionChemical:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionDangerousChemical> util = new ExcelUtil<SusionDangerousChemical>(SusionDangerousChemical.class);
        List<SusionDangerousChemical> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionDangerousChemicalService.importSusionDangerousChemical(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载危险化学品及其包装检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionDangerousChemical> util = new ExcelUtil<SusionDangerousChemical>(SusionDangerousChemical.class);
        util.importTemplateExcel(response, "危险化学品及其包装检验监管情况统计数据");
    }

    /**
     * 获取危险化学品及其包装检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionChemical:query')")
    @GetMapping(value = "/{susionDangerousChemicalId}")
    public AjaxResult getInfo(@PathVariable("susionDangerousChemicalId") Long susionDangerousChemicalId)
    {
        return AjaxResult.success(susionDangerousChemicalService.selectSusionDangerousChemicalBySusionDangerousChemicalId(susionDangerousChemicalId));
    }

    /**
     * 新增危险化学品及其包装检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionChemical:add')")
    @Log(title = "危险化学品及其包装检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionDangerousChemical susionDangerousChemical)
    {
        return toAjax(susionDangerousChemicalService.insertSusionDangerousChemical(susionDangerousChemical));
    }

    /**
     * 修改危险化学品及其包装检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionChemical:edit')")
    @Log(title = "危险化学品及其包装检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionDangerousChemical susionDangerousChemical)
    {
        return toAjax(susionDangerousChemicalService.updateSusionDangerousChemical(susionDangerousChemical));
    }

    /**
     * 删除危险化学品及其包装检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionChemical:remove')")
    @Log(title = "危险化学品及其包装检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionDangerousChemicalIds}")
    public AjaxResult remove(@PathVariable Long[] susionDangerousChemicalIds)
    {
        return toAjax(susionDangerousChemicalService.deleteSusionDangerousChemicalBySusionDangerousChemicalIds(susionDangerousChemicalIds));
    }
}
