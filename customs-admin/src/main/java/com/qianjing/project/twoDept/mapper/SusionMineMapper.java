package com.qianjing.project.twoDept.mapper;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionMine;

/**
 * 进口矿产品“先放后检”情况统计Mapper接口
 * 
 * @author qianjing
 * @date 2022-05-16
 */
public interface SusionMineMapper 
{
    /**
     * 查询进口矿产品“先放后检”情况统计
     * 
     * @param susionMineId 进口矿产品“先放后检”情况统计主键
     * @return 进口矿产品“先放后检”情况统计
     */
    public SusionMine selectSusionMineBySusionMineId(Long susionMineId);

    /**
     * 查询进口矿产品“先放后检”情况统计列表
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 进口矿产品“先放后检”情况统计集合
     */
    public List<SusionMine> selectSusionMineList(SusionMine susionMine);

    /**
     * 新增进口矿产品“先放后检”情况统计
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 结果
     */
    public int insertSusionMine(SusionMine susionMine);

    /**
     * 修改进口矿产品“先放后检”情况统计
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 结果
     */
    public int updateSusionMine(SusionMine susionMine);

    /**
     * 删除进口矿产品“先放后检”情况统计
     * 
     * @param susionMineId 进口矿产品“先放后检”情况统计主键
     * @return 结果
     */
    public int deleteSusionMineBySusionMineId(Long susionMineId);

    /**
     * 批量删除进口矿产品“先放后检”情况统计
     * 
     * @param susionMineIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSusionMineBySusionMineIds(Long[] susionMineIds);
}
