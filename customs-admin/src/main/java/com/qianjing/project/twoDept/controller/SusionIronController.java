package com.qianjing.project.twoDept.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.twoDept.domain.SusionIron;
import com.qianjing.project.twoDept.service.ISusionIronService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 进口铁矿检验监管情况统计Controller
 * 
 * @author qianjing
 * @date 2022-05-16
 */
@RestController
@RequestMapping("/twoDept/susionIron")
public class SusionIronController extends BaseController
{
    @Autowired
    private ISusionIronService susionIronService;

    /**
     * 查询进口铁矿检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionIron:list')")
    @GetMapping("/list")
    public TableDataInfo list(SusionIron susionIron)
    {
        startPage();
        List<SusionIron> list = susionIronService.selectSusionIronList(susionIron);
        return getDataTable(list);
    }

    /**
     * 导出进口铁矿检验监管情况统计列表
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionIron:export')")
    @Log(title = "进口铁矿检验监管情况统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SusionIron susionIron)
    {
        List<SusionIron> list = susionIronService.selectSusionIronList(susionIron);
        ExcelUtil<SusionIron> util = new ExcelUtil<SusionIron>(SusionIron.class);
        return util.exportExcel(list, "进口铁矿检验监管情况统计数据");
    }

    /**
     * 导入进口铁矿检验监管情况统计列表
     */
    @Log(title = "进口铁矿检验监管情况统计", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('twoDept:susionIron:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SusionIron> util = new ExcelUtil<SusionIron>(SusionIron.class);
        List<SusionIron> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = susionIronService.importSusionIron(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载进口铁矿检验监管情况统计导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SusionIron> util = new ExcelUtil<SusionIron>(SusionIron.class);
        util.importTemplateExcel(response, "进口铁矿检验监管情况统计数据");
    }

    /**
     * 获取进口铁矿检验监管情况统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionIron:query')")
    @GetMapping(value = "/{susionIronId}")
    public AjaxResult getInfo(@PathVariable("susionIronId") Long susionIronId)
    {
        return AjaxResult.success(susionIronService.selectSusionIronBySusionIronId(susionIronId));
    }

    /**
     * 新增进口铁矿检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionIron:add')")
    @Log(title = "进口铁矿检验监管情况统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SusionIron susionIron)
    {
        return toAjax(susionIronService.insertSusionIron(susionIron));
    }

    /**
     * 修改进口铁矿检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionIron:edit')")
    @Log(title = "进口铁矿检验监管情况统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SusionIron susionIron)
    {
        return toAjax(susionIronService.updateSusionIron(susionIron));
    }

    /**
     * 删除进口铁矿检验监管情况统计
     */
    @PreAuthorize("@ss.hasPermi('twoDept:susionIron:remove')")
    @Log(title = "进口铁矿检验监管情况统计", businessType = BusinessType.DELETE)
	@DeleteMapping("/{susionIronIds}")
    public AjaxResult remove(@PathVariable Long[] susionIronIds)
    {
        return toAjax(susionIronService.deleteSusionIronBySusionIronIds(susionIronIds));
    }
}
