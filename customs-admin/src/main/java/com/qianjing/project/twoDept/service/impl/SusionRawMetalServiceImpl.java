package com.qianjing.project.twoDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.twoDept.mapper.SusionRawMetalMapper;
import com.qianjing.project.twoDept.domain.SusionRawMetal;
import com.qianjing.project.twoDept.service.ISusionRawMetalService;

/**
 * 进口再生原料检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-19
 */
@Service
public class SusionRawMetalServiceImpl implements ISusionRawMetalService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionRawMetalServiceImpl.class);

    @Autowired
    private SusionRawMetalMapper susionRawMetalMapper;

    /**
     * 查询进口再生原料检验监管情况统计
     * 
     * @param susionRawMetalId 进口再生原料检验监管情况统计主键
     * @return 进口再生原料检验监管情况统计
     */
    @Override
    public SusionRawMetal selectSusionRawMetalBySusionRawMetalId(Long susionRawMetalId)
    {
        return susionRawMetalMapper.selectSusionRawMetalBySusionRawMetalId(susionRawMetalId);
    }

    /**
     * 查询进口再生原料检验监管情况统计列表
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 进口再生原料检验监管情况统计
     */
    @Override
    public List<SusionRawMetal> selectSusionRawMetalList(SusionRawMetal susionRawMetal)
    {
        return susionRawMetalMapper.selectSusionRawMetalList(susionRawMetal);
    }

    /**
     * 新增进口再生原料检验监管情况统计
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionRawMetal(SusionRawMetal susionRawMetal)
    {
        susionRawMetal.setCreateTime(DateUtils.getNowDate());
        return susionRawMetalMapper.insertSusionRawMetal(susionRawMetal);
    }

    /**
     * 修改进口再生原料检验监管情况统计
     * 
     * @param susionRawMetal 进口再生原料检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionRawMetal(SusionRawMetal susionRawMetal)
    {
        susionRawMetal.setUpdateTime(DateUtils.getNowDate());
        return susionRawMetalMapper.updateSusionRawMetal(susionRawMetal);
    }

    /**
     * 批量删除进口再生原料检验监管情况统计
     * 
     * @param susionRawMetalIds 需要删除的进口再生原料检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionRawMetalBySusionRawMetalIds(Long[] susionRawMetalIds)
    {
        return susionRawMetalMapper.deleteSusionRawMetalBySusionRawMetalIds(susionRawMetalIds);
    }

    /**
     * 删除进口再生原料检验监管情况统计信息
     * 
     * @param susionRawMetalId 进口再生原料检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionRawMetalBySusionRawMetalId(Long susionRawMetalId)
    {
        return susionRawMetalMapper.deleteSusionRawMetalBySusionRawMetalId(susionRawMetalId);
    }

    /**
     * 导入进口再生原料检验监管情况统计数据
     *
     * @param susionRawMetalList susionRawMetalList 进口再生原料检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionRawMetal(List<SusionRawMetal> susionRawMetalList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionRawMetalList) || susionRawMetalList.size() == 0) {
            throw new ServiceException("导入进口再生原料检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionRawMetal susionRawMetal : susionRawMetalList){
            try {
                // 验证是否存在
                if (true) {
                    susionRawMetal.setCreateBy(operName);
                    this.insertSusionRawMetal(susionRawMetal);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionRawMetal.setUpdateBy(operName);
                    this.updateSusionRawMetal(susionRawMetal);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
