package com.qianjing.project.twoDept.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.twoDept.mapper.SusionIronMapper;
import com.qianjing.project.twoDept.domain.SusionIron;
import com.qianjing.project.twoDept.service.ISusionIronService;

/**
 * 进口铁矿检验监管情况统计Service业务层处理
 * 
 * @author qianjing
 * @date 2022-05-16
 */
@Service
public class SusionIronServiceImpl implements ISusionIronService 
{
    private static final Logger log = LoggerFactory.getLogger(SusionIronServiceImpl.class);

    @Autowired
    private SusionIronMapper susionIronMapper;

    /**
     * 查询进口铁矿检验监管情况统计
     * 
     * @param susionIronId 进口铁矿检验监管情况统计主键
     * @return 进口铁矿检验监管情况统计
     */
    @Override
    public SusionIron selectSusionIronBySusionIronId(Long susionIronId)
    {
        return susionIronMapper.selectSusionIronBySusionIronId(susionIronId);
    }

    /**
     * 查询进口铁矿检验监管情况统计列表
     * 
     * @param susionIron 进口铁矿检验监管情况统计
     * @return 进口铁矿检验监管情况统计
     */
    @Override
    public List<SusionIron> selectSusionIronList(SusionIron susionIron)
    {
        return susionIronMapper.selectSusionIronList(susionIron);
    }

    /**
     * 新增进口铁矿检验监管情况统计
     * 
     * @param susionIron 进口铁矿检验监管情况统计
     * @return 结果
     */
    @Override
    public int insertSusionIron(SusionIron susionIron)
    {
        susionIron.setCreateTime(DateUtils.getNowDate());
        return susionIronMapper.insertSusionIron(susionIron);
    }

    /**
     * 修改进口铁矿检验监管情况统计
     * 
     * @param susionIron 进口铁矿检验监管情况统计
     * @return 结果
     */
    @Override
    public int updateSusionIron(SusionIron susionIron)
    {
        susionIron.setUpdateTime(DateUtils.getNowDate());
        return susionIronMapper.updateSusionIron(susionIron);
    }

    /**
     * 批量删除进口铁矿检验监管情况统计
     * 
     * @param susionIronIds 需要删除的进口铁矿检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionIronBySusionIronIds(Long[] susionIronIds)
    {
        return susionIronMapper.deleteSusionIronBySusionIronIds(susionIronIds);
    }

    /**
     * 删除进口铁矿检验监管情况统计信息
     * 
     * @param susionIronId 进口铁矿检验监管情况统计主键
     * @return 结果
     */
    @Override
    public int deleteSusionIronBySusionIronId(Long susionIronId)
    {
        return susionIronMapper.deleteSusionIronBySusionIronId(susionIronId);
    }

    /**
     * 导入进口铁矿检验监管情况统计数据
     *
     * @param susionIronList susionIronList 进口铁矿检验监管情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSusionIron(List<SusionIron> susionIronList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(susionIronList) || susionIronList.size() == 0) {
            throw new ServiceException("导入进口铁矿检验监管情况统计数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SusionIron susionIron : susionIronList){
            try {
                // 验证是否存在
                if (true) {
                    susionIron.setCreateBy(operName);
                    this.insertSusionIron(susionIron);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    susionIron.setUpdateBy(operName);
                    this.updateSusionIron(susionIron);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
