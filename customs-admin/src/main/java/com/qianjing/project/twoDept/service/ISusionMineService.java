package com.qianjing.project.twoDept.service;

import java.util.List;
import com.qianjing.project.twoDept.domain.SusionMine;

/**
 * 进口矿产品“先放后检”情况统计Service接口
 * 
 * @author qianjing
 * @date 2022-05-16
 */
public interface ISusionMineService 
{
    /**
     * 查询进口矿产品“先放后检”情况统计
     * 
     * @param susionMineId 进口矿产品“先放后检”情况统计主键
     * @return 进口矿产品“先放后检”情况统计
     */
    public SusionMine selectSusionMineBySusionMineId(Long susionMineId);

    /**
     * 查询进口矿产品“先放后检”情况统计列表
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 进口矿产品“先放后检”情况统计集合
     */
    public List<SusionMine> selectSusionMineList(SusionMine susionMine);

    /**
     * 新增进口矿产品“先放后检”情况统计
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 结果
     */
    public int insertSusionMine(SusionMine susionMine);

    /**
     * 修改进口矿产品“先放后检”情况统计
     * 
     * @param susionMine 进口矿产品“先放后检”情况统计
     * @return 结果
     */
    public int updateSusionMine(SusionMine susionMine);

    /**
     * 批量删除进口矿产品“先放后检”情况统计
     * 
     * @param susionMineIds 需要删除的进口矿产品“先放后检”情况统计主键集合
     * @return 结果
     */
    public int deleteSusionMineBySusionMineIds(Long[] susionMineIds);

    /**
     * 删除进口矿产品“先放后检”情况统计信息
     * 
     * @param susionMineId 进口矿产品“先放后检”情况统计主键
     * @return 结果
     */
    public int deleteSusionMineBySusionMineId(Long susionMineId);

    /**
     * 导入进口矿产品“先放后检”情况统计信息
     *
     * @param susionMineList 进口矿产品“先放后检”情况统计信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSusionMine(List<SusionMine> susionMineList, Boolean isUpdateSupport, String operName);
}
