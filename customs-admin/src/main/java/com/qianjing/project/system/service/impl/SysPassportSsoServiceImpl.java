package com.qianjing.project.system.service.impl;
import cn.gov.customs.casp.sdk.h4a.sso.IPassportSSO;
import cn.gov.customs.casp.sdk.h4a.sso.passport.Ticket;
import com.qianjing.framework.security.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class SysPassportSsoServiceImpl implements IPassportSSO {

    @Autowired
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Override
    public void Login(Ticket ticket, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String s) {
        //根据项目的实际情况，自行实现单点登陆后系统的处理
    }

    @Override
    public void LogOut(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        //根据项目的实际情况，自行实现单点登陆后系统的处理
    }
}

