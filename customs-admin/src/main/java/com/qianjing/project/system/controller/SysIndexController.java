package com.qianjing.project.system.controller;

import com.qianjing.framework.config.QianJingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页
 *
 * @author qianjing
 */
@RestController
public class SysIndexController
{
    /** 系统基础配置 */
    @Autowired
    private QianJingConfig qianjingConfig;

    /**
     * 访问首页，提示语
     */
    @RequestMapping("/")
    public String index()
    {
        return "对不起，您即将要访问的页面，出现网络波动或后端服务异常...请清理浏览器缓存后重试！";
    }
}
