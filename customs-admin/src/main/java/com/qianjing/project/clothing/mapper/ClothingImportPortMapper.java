package com.qianjing.project.clothing.mapper;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingImportPort;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装主要进口口岸Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ClothingImportPortMapper {

    ClothingImportPort selectClothingImportPortById(Long id);

    List<ClothingImportPort> selectClothingImportPortList(ClothingImportPort clothingImportPort);

    int insertClothingImportPort(ClothingImportPort clothingImportPort);

    int updateClothingImportPort(ClothingImportPort clothingImportPort);

    int deleteClothingImportPortById(Long id);

    int deleteClothingImportPortByIds(Long[] ids);
}
