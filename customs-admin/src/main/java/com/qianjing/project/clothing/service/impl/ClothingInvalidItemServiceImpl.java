package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingInvalidItemMapper;
import com.qianjing.project.clothing.domain.ClothingInvalidItem;
import com.qianjing.project.clothing.service.IClothingInvalidItemService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格项目情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidItemServiceImpl implements IClothingInvalidItemService {

    private static final Logger log = LoggerFactory.getLogger(ClothingInvalidItemServiceImpl.class);

    private final ClothingInvalidItemMapper clothingInvalidItemMapper;

    /**
     * 查询服装不合格项目情况
     * 
     * @param id 服装不合格项目情况主键
     * @return 服装不合格项目情况
     */
    @Override
    public ClothingInvalidItem selectClothingInvalidItemById(Long id) {
        return clothingInvalidItemMapper.selectClothingInvalidItemById(id);
    }

    /**
     * 查询服装不合格项目情况列表
     * 
     * @param clothingInvalidItem 服装不合格项目情况
     * @return 服装不合格项目情况
     */
    @Override
    public List<ClothingInvalidItem> selectClothingInvalidItemList(ClothingInvalidItem clothingInvalidItem) {
        return clothingInvalidItemMapper.selectClothingInvalidItemList(clothingInvalidItem);
    }

    /**
     * 新增服装不合格项目情况
     * 
     * @param clothingInvalidItem 服装不合格项目情况
     * @return 结果
     */
    @Override
    public int insertClothingInvalidItem(ClothingInvalidItem clothingInvalidItem) {
        clothingInvalidItem.setCreateTime(DateUtils.getNowDate());
        return clothingInvalidItemMapper.insertClothingInvalidItem(clothingInvalidItem);
    }

    /**
     * 修改服装不合格项目情况
     * 
     * @param clothingInvalidItem 服装不合格项目情况
     * @return 结果
     */
    @Override
    public int updateClothingInvalidItem(ClothingInvalidItem clothingInvalidItem) {
        clothingInvalidItem.setUpdateTime(DateUtils.getNowDate());
        return clothingInvalidItemMapper.updateClothingInvalidItem(clothingInvalidItem);
    }

    /**
     * 批量删除服装不合格项目情况
     * 
     * @param ids 需要删除的服装不合格项目情况主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidItemByIds(Long[] ids) {
        return clothingInvalidItemMapper.deleteClothingInvalidItemByIds(ids);
    }

    /**
     * 删除服装不合格项目情况信息
     * 
     * @param id 服装不合格项目情况主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidItemById(Long id) {
        return clothingInvalidItemMapper.deleteClothingInvalidItemById(id);
    }

    /**
     * 导入服装不合格项目情况数据
     *
     * @param clothingInvalidItemList clothingInvalidItemList 服装不合格项目情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingInvalidItem(List<ClothingInvalidItem> clothingInvalidItemList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingInvalidItemList) || clothingInvalidItemList.size() == 0) {
            throw new ServiceException("导入服装不合格项目情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingInvalidItem clothingInvalidItem : clothingInvalidItemList){
            try {
                // 验证是否存在
                if (true) {
                    clothingInvalidItem.setCreateBy(operName);
                    this.insertClothingInvalidItem(clothingInvalidItem);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingInvalidItem.setUpdateBy(operName);
                    this.updateClothingInvalidItem(clothingInvalidItem);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
