package com.qianjing.project.clothing.service;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingInvalidSource;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格来源国Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IClothingInvalidSourceService 
{
    /**
     * 查询服装不合格来源国
     * 
     * @param id 服装不合格来源国主键
     * @return 服装不合格来源国
     */
    public ClothingInvalidSource selectClothingInvalidSourceById(Long id);

    /**
     * 查询服装不合格来源国列表
     * 
     * @param clothingInvalidSource 服装不合格来源国
     * @return 服装不合格来源国集合
     */
    public List<ClothingInvalidSource> selectClothingInvalidSourceList(ClothingInvalidSource clothingInvalidSource);

    /**
     * 新增服装不合格来源国
     * 
     * @param clothingInvalidSource 服装不合格来源国
     * @return 结果
     */
    public int insertClothingInvalidSource(ClothingInvalidSource clothingInvalidSource);

    /**
     * 修改服装不合格来源国
     * 
     * @param clothingInvalidSource 服装不合格来源国
     * @return 结果
     */
    public int updateClothingInvalidSource(ClothingInvalidSource clothingInvalidSource);

    /**
     * 批量删除服装不合格来源国
     * 
     * @param ids 需要删除的服装不合格来源国主键集合
     * @return 结果
     */
    public int deleteClothingInvalidSourceByIds(Long[] ids);

    /**
     * 删除服装不合格来源国信息
     * 
     * @param id 服装不合格来源国主键
     * @return 结果
     */
    public int deleteClothingInvalidSourceById(Long id);

    /**
     * 导入服装不合格来源国信息
     *
     * @param clothingInvalidSourceList 服装不合格来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importClothingInvalidSource(List<ClothingInvalidSource> clothingInvalidSourceList, Boolean isUpdateSupport, String operName);
}
