package com.qianjing.project.clothing.mapper;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装概况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ClothingOverviewMapper {

    ClothingOverview selectClothingOverviewById(Long id);

    List<ClothingOverview> selectClothingOverviewList(ClothingOverview clothingOverview);

    int insertClothingOverview(ClothingOverview clothingOverview);

    int updateClothingOverview(ClothingOverview clothingOverview);

    int deleteClothingOverviewById(Long id);

    int deleteClothingOverviewByIds(Long[] ids);
}
