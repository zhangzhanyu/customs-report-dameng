package com.qianjing.project.clothing.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.clothing.domain.ClothingInvalidCustoms;
import com.qianjing.project.clothing.service.IClothingInvalidCustomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 服装不合格关区情况Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/clothing/customs")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidCustomsController extends BaseController {

    private final IClothingInvalidCustomsService clothingInvalidCustomsService;

    /**
     * 查询服装不合格关区情况列表
     */
//    @PreAuthorize("@ss.hasPermi('clothing:customs:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClothingInvalidCustoms clothingInvalidCustoms) {
        startPage();
        List<ClothingInvalidCustoms> list = clothingInvalidCustomsService.selectClothingInvalidCustomsList(clothingInvalidCustoms);
        return getDataTable(list);
    }

    /**
     * 导出服装不合格关区情况列表
     */
    @PreAuthorize("@ss.hasPermi('clothing:customs:export')")
    @Log(title = "服装不合格关区情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClothingInvalidCustoms clothingInvalidCustoms) {
        List<ClothingInvalidCustoms> list = clothingInvalidCustomsService.selectClothingInvalidCustomsList(clothingInvalidCustoms);
        ExcelUtil<ClothingInvalidCustoms> util = new ExcelUtil<ClothingInvalidCustoms>(ClothingInvalidCustoms.class);
        return util.exportExcel(list, "服装不合格关区情况数据");
    }

    /**
     * 导入服装不合格关区情况列表
     */
    @Log(title = "服装不合格关区情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('clothing:customs:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ClothingInvalidCustoms> util = new ExcelUtil<ClothingInvalidCustoms>(ClothingInvalidCustoms.class);
        List<ClothingInvalidCustoms> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = clothingInvalidCustomsService.importClothingInvalidCustoms(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载服装不合格关区情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ClothingInvalidCustoms> util = new ExcelUtil<ClothingInvalidCustoms>(ClothingInvalidCustoms.class);
        util.importTemplateExcel(response, "服装不合格关区情况数据");
    }

    /**
     * 获取服装不合格关区情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('clothing:customs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(clothingInvalidCustomsService.selectClothingInvalidCustomsById(id));
    }

    /**
     * 新增服装不合格关区情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:customs:add')")
    @Log(title = "服装不合格关区情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClothingInvalidCustoms clothingInvalidCustoms) {
        return toAjax(clothingInvalidCustomsService.insertClothingInvalidCustoms(clothingInvalidCustoms));
    }

    /**
     * 修改服装不合格关区情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:customs:edit')")
    @Log(title = "服装不合格关区情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClothingInvalidCustoms clothingInvalidCustoms) {
        return toAjax(clothingInvalidCustomsService.updateClothingInvalidCustoms(clothingInvalidCustoms));
    }

    /**
     * 删除服装不合格关区情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:customs:remove')")
    @Log(title = "服装不合格关区情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(clothingInvalidCustomsService.deleteClothingInvalidCustomsByIds(ids));
    }
}
