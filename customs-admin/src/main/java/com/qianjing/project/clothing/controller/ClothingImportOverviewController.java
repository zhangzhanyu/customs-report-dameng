package com.qianjing.project.clothing.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.clothing.domain.ClothingImportOverview;
import com.qianjing.project.clothing.service.IClothingImportOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 服装进口概况Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/clothing/importOverview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingImportOverviewController extends BaseController {

    private final IClothingImportOverviewService clothingImportOverviewService;

    /**
     * 查询服装进口概况列表
     */
//    @PreAuthorize("@ss.hasPermi('clothing:importOverview:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClothingImportOverview clothingImportOverview) {
        startPage();
        List<ClothingImportOverview> list = clothingImportOverviewService.selectClothingImportOverviewList(clothingImportOverview);
        return getDataTable(list);
    }

    /**
     * 导出服装进口概况列表
     */
    @PreAuthorize("@ss.hasPermi('clothing:importOverview:export')")
    @Log(title = "服装进口概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClothingImportOverview clothingImportOverview) {
        List<ClothingImportOverview> list = clothingImportOverviewService.selectClothingImportOverviewList(clothingImportOverview);
        ExcelUtil<ClothingImportOverview> util = new ExcelUtil<ClothingImportOverview>(ClothingImportOverview.class);
        return util.exportExcel(list, "服装进口概况数据");
    }

    /**
     * 导入服装进口概况列表
     */
    @Log(title = "服装进口概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('clothing:importOverview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ClothingImportOverview> util = new ExcelUtil<ClothingImportOverview>(ClothingImportOverview.class);
        List<ClothingImportOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = clothingImportOverviewService.importClothingImportOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载服装进口概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ClothingImportOverview> util = new ExcelUtil<ClothingImportOverview>(ClothingImportOverview.class);
        util.importTemplateExcel(response, "服装进口概况数据");
    }

    /**
     * 获取服装进口概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('clothing:importOverview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(clothingImportOverviewService.selectClothingImportOverviewById(id));
    }

    /**
     * 新增服装进口概况
     */
    @PreAuthorize("@ss.hasPermi('clothing:importOverview:add')")
    @Log(title = "服装进口概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClothingImportOverview clothingImportOverview) {
        return toAjax(clothingImportOverviewService.insertClothingImportOverview(clothingImportOverview));
    }

    /**
     * 修改服装进口概况
     */
    @PreAuthorize("@ss.hasPermi('clothing:importOverview:edit')")
    @Log(title = "服装进口概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClothingImportOverview clothingImportOverview) {
        return toAjax(clothingImportOverviewService.updateClothingImportOverview(clothingImportOverview));
    }

    /**
     * 删除服装进口概况
     */
    @PreAuthorize("@ss.hasPermi('clothing:importOverview:remove')")
    @Log(title = "服装进口概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(clothingImportOverviewService.deleteClothingImportOverviewByIds(ids));
    }
}
