package com.qianjing.project.clothing.mapper;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingImportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装进口概况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface ClothingImportOverviewMapper {

    ClothingImportOverview selectClothingImportOverviewById(Long id);

    List<ClothingImportOverview> selectClothingImportOverviewList(ClothingImportOverview clothingImportOverview);

    int insertClothingImportOverview(ClothingImportOverview clothingImportOverview);

    int updateClothingImportOverview(ClothingImportOverview clothingImportOverview);

    int deleteClothingImportOverviewById(Long id);

    int deleteClothingImportOverviewByIds(Long[] ids);
}
