package com.qianjing.project.clothing.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.clothing.domain.ClothingInvalidCompany;
import com.qianjing.project.clothing.service.IClothingInvalidCompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 服装不合格企业情况Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/clothing/company")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidCompanyController extends BaseController {

    private final IClothingInvalidCompanyService clothingInvalidCompanyService;

    /**
     * 查询服装不合格企业情况列表
     */
//    @PreAuthorize("@ss.hasPermi('clothing:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClothingInvalidCompany clothingInvalidCompany) {
        startPage();
        List<ClothingInvalidCompany> list = clothingInvalidCompanyService.selectClothingInvalidCompanyList(clothingInvalidCompany);
        return getDataTable(list);
    }

    /**
     * 导出服装不合格企业情况列表
     */
    @PreAuthorize("@ss.hasPermi('clothing:company:export')")
    @Log(title = "服装不合格企业情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClothingInvalidCompany clothingInvalidCompany) {
        List<ClothingInvalidCompany> list = clothingInvalidCompanyService.selectClothingInvalidCompanyList(clothingInvalidCompany);
        ExcelUtil<ClothingInvalidCompany> util = new ExcelUtil<ClothingInvalidCompany>(ClothingInvalidCompany.class);
        return util.exportExcel(list, "服装不合格企业情况数据");
    }

    /**
     * 导入服装不合格企业情况列表
     */
    @Log(title = "服装不合格企业情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('clothing:company:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ClothingInvalidCompany> util = new ExcelUtil<ClothingInvalidCompany>(ClothingInvalidCompany.class);
        List<ClothingInvalidCompany> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = clothingInvalidCompanyService.importClothingInvalidCompany(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载服装不合格企业情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ClothingInvalidCompany> util = new ExcelUtil<ClothingInvalidCompany>(ClothingInvalidCompany.class);
        util.importTemplateExcel(response, "服装不合格企业情况数据");
    }

    /**
     * 获取服装不合格企业情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('clothing:company:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(clothingInvalidCompanyService.selectClothingInvalidCompanyById(id));
    }

    /**
     * 新增服装不合格企业情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:company:add')")
    @Log(title = "服装不合格企业情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClothingInvalidCompany clothingInvalidCompany) {
        return toAjax(clothingInvalidCompanyService.insertClothingInvalidCompany(clothingInvalidCompany));
    }

    /**
     * 修改服装不合格企业情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:company:edit')")
    @Log(title = "服装不合格企业情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClothingInvalidCompany clothingInvalidCompany) {
        return toAjax(clothingInvalidCompanyService.updateClothingInvalidCompany(clothingInvalidCompany));
    }

    /**
     * 删除服装不合格企业情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:company:remove')")
    @Log(title = "服装不合格企业情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(clothingInvalidCompanyService.deleteClothingInvalidCompanyByIds(ids));
    }
}
