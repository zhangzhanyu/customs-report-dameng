package com.qianjing.project.clothing.service;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IClothingOverviewService 
{
    /**
     * 查询服装概况
     * 
     * @param id 服装概况主键
     * @return 服装概况
     */
    public ClothingOverview selectClothingOverviewById(Long id);

    /**
     * 查询服装概况列表
     * 
     * @param clothingOverview 服装概况
     * @return 服装概况集合
     */
    public List<ClothingOverview> selectClothingOverviewList(ClothingOverview clothingOverview);

    /**
     * 新增服装概况
     * 
     * @param clothingOverview 服装概况
     * @return 结果
     */
    public int insertClothingOverview(ClothingOverview clothingOverview);

    /**
     * 修改服装概况
     * 
     * @param clothingOverview 服装概况
     * @return 结果
     */
    public int updateClothingOverview(ClothingOverview clothingOverview);

    /**
     * 批量删除服装概况
     * 
     * @param ids 需要删除的服装概况主键集合
     * @return 结果
     */
    public int deleteClothingOverviewByIds(Long[] ids);

    /**
     * 删除服装概况信息
     * 
     * @param id 服装概况主键
     * @return 结果
     */
    public int deleteClothingOverviewById(Long id);

    /**
     * 导入服装概况信息
     *
     * @param clothingOverviewList 服装概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importClothingOverview(List<ClothingOverview> clothingOverviewList, Boolean isUpdateSupport, String operName);
}
