package com.qianjing.project.clothing.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.clothing.domain.ClothingImportPort;
import com.qianjing.project.clothing.service.IClothingImportPortService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 服装主要进口口岸Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/clothing/port")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingImportPortController extends BaseController {

    private final IClothingImportPortService clothingImportPortService;

    /**
     * 查询服装主要进口口岸列表
     */
//    @PreAuthorize("@ss.hasPermi('clothing:port:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClothingImportPort clothingImportPort) {
        startPage();
        List<ClothingImportPort> list = clothingImportPortService.selectClothingImportPortList(clothingImportPort);
        return getDataTable(list);
    }

    /**
     * 导出服装主要进口口岸列表
     */
    @PreAuthorize("@ss.hasPermi('clothing:port:export')")
    @Log(title = "服装主要进口口岸", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClothingImportPort clothingImportPort) {
        List<ClothingImportPort> list = clothingImportPortService.selectClothingImportPortList(clothingImportPort);
        ExcelUtil<ClothingImportPort> util = new ExcelUtil<ClothingImportPort>(ClothingImportPort.class);
        return util.exportExcel(list, "服装主要进口口岸数据");
    }

    /**
     * 导入服装主要进口口岸列表
     */
    @Log(title = "服装主要进口口岸", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('clothing:port:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ClothingImportPort> util = new ExcelUtil<ClothingImportPort>(ClothingImportPort.class);
        List<ClothingImportPort> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = clothingImportPortService.importClothingImportPort(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载服装主要进口口岸导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ClothingImportPort> util = new ExcelUtil<ClothingImportPort>(ClothingImportPort.class);
        util.importTemplateExcel(response, "服装主要进口口岸数据");
    }

    /**
     * 获取服装主要进口口岸详细信息
     */
    @PreAuthorize("@ss.hasPermi('clothing:port:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(clothingImportPortService.selectClothingImportPortById(id));
    }

    /**
     * 新增服装主要进口口岸
     */
    @PreAuthorize("@ss.hasPermi('clothing:port:add')")
    @Log(title = "服装主要进口口岸", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClothingImportPort clothingImportPort) {
        return toAjax(clothingImportPortService.insertClothingImportPort(clothingImportPort));
    }

    /**
     * 修改服装主要进口口岸
     */
    @PreAuthorize("@ss.hasPermi('clothing:port:edit')")
    @Log(title = "服装主要进口口岸", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClothingImportPort clothingImportPort) {
        return toAjax(clothingImportPortService.updateClothingImportPort(clothingImportPort));
    }

    /**
     * 删除服装主要进口口岸
     */
    @PreAuthorize("@ss.hasPermi('clothing:port:remove')")
    @Log(title = "服装主要进口口岸", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(clothingImportPortService.deleteClothingImportPortByIds(ids));
    }
}
