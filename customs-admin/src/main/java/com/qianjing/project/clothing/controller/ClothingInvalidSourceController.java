package com.qianjing.project.clothing.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.clothing.domain.ClothingInvalidSource;
import com.qianjing.project.clothing.service.IClothingInvalidSourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 服装不合格来源国Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/clothing/source")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidSourceController extends BaseController {

    private final IClothingInvalidSourceService clothingInvalidSourceService;

    /**
     * 查询服装不合格来源国列表
     */
//    @PreAuthorize("@ss.hasPermi('clothing:source:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClothingInvalidSource clothingInvalidSource) {
        startPage();
        List<ClothingInvalidSource> list = clothingInvalidSourceService.selectClothingInvalidSourceList(clothingInvalidSource);
        return getDataTable(list);
    }

    /**
     * 导出服装不合格来源国列表
     */
    @PreAuthorize("@ss.hasPermi('clothing:source:export')")
    @Log(title = "服装不合格来源国", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClothingInvalidSource clothingInvalidSource) {
        List<ClothingInvalidSource> list = clothingInvalidSourceService.selectClothingInvalidSourceList(clothingInvalidSource);
        ExcelUtil<ClothingInvalidSource> util = new ExcelUtil<ClothingInvalidSource>(ClothingInvalidSource.class);
        return util.exportExcel(list, "服装不合格来源国数据");
    }

    /**
     * 导入服装不合格来源国列表
     */
    @Log(title = "服装不合格来源国", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('clothing:source:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ClothingInvalidSource> util = new ExcelUtil<ClothingInvalidSource>(ClothingInvalidSource.class);
        List<ClothingInvalidSource> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = clothingInvalidSourceService.importClothingInvalidSource(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载服装不合格来源国导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ClothingInvalidSource> util = new ExcelUtil<ClothingInvalidSource>(ClothingInvalidSource.class);
        util.importTemplateExcel(response, "服装不合格来源国数据");
    }

    /**
     * 获取服装不合格来源国详细信息
     */
    @PreAuthorize("@ss.hasPermi('clothing:source:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(clothingInvalidSourceService.selectClothingInvalidSourceById(id));
    }

    /**
     * 新增服装不合格来源国
     */
    @PreAuthorize("@ss.hasPermi('clothing:source:add')")
    @Log(title = "服装不合格来源国", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClothingInvalidSource clothingInvalidSource) {
        return toAjax(clothingInvalidSourceService.insertClothingInvalidSource(clothingInvalidSource));
    }

    /**
     * 修改服装不合格来源国
     */
    @PreAuthorize("@ss.hasPermi('clothing:source:edit')")
    @Log(title = "服装不合格来源国", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClothingInvalidSource clothingInvalidSource) {
        return toAjax(clothingInvalidSourceService.updateClothingInvalidSource(clothingInvalidSource));
    }

    /**
     * 删除服装不合格来源国
     */
    @PreAuthorize("@ss.hasPermi('clothing:source:remove')")
    @Log(title = "服装不合格来源国", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(clothingInvalidSourceService.deleteClothingInvalidSourceByIds(ids));
    }
}
