package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingSourceCountryMapper;
import com.qianjing.project.clothing.domain.ClothingSourceCountry;
import com.qianjing.project.clothing.service.IClothingSourceCountryService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装主要来源国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingSourceCountryServiceImpl implements IClothingSourceCountryService {

    private static final Logger log = LoggerFactory.getLogger(ClothingSourceCountryServiceImpl.class);

    private final ClothingSourceCountryMapper clothingSourceCountryMapper;

    /**
     * 查询服装主要来源国
     * 
     * @param id 服装主要来源国主键
     * @return 服装主要来源国
     */
    @Override
    public ClothingSourceCountry selectClothingSourceCountryById(Long id) {
        return clothingSourceCountryMapper.selectClothingSourceCountryById(id);
    }

    /**
     * 查询服装主要来源国列表
     * 
     * @param clothingSourceCountry 服装主要来源国
     * @return 服装主要来源国
     */
    @Override
    public List<ClothingSourceCountry> selectClothingSourceCountryList(ClothingSourceCountry clothingSourceCountry) {
        return clothingSourceCountryMapper.selectClothingSourceCountryList(clothingSourceCountry);
    }

    /**
     * 新增服装主要来源国
     * 
     * @param clothingSourceCountry 服装主要来源国
     * @return 结果
     */
    @Override
    public int insertClothingSourceCountry(ClothingSourceCountry clothingSourceCountry) {
        clothingSourceCountry.setCreateTime(DateUtils.getNowDate());
        return clothingSourceCountryMapper.insertClothingSourceCountry(clothingSourceCountry);
    }

    /**
     * 修改服装主要来源国
     * 
     * @param clothingSourceCountry 服装主要来源国
     * @return 结果
     */
    @Override
    public int updateClothingSourceCountry(ClothingSourceCountry clothingSourceCountry) {
        clothingSourceCountry.setUpdateTime(DateUtils.getNowDate());
        return clothingSourceCountryMapper.updateClothingSourceCountry(clothingSourceCountry);
    }

    /**
     * 批量删除服装主要来源国
     * 
     * @param ids 需要删除的服装主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteClothingSourceCountryByIds(Long[] ids) {
        return clothingSourceCountryMapper.deleteClothingSourceCountryByIds(ids);
    }

    /**
     * 删除服装主要来源国信息
     * 
     * @param id 服装主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteClothingSourceCountryById(Long id) {
        return clothingSourceCountryMapper.deleteClothingSourceCountryById(id);
    }

    /**
     * 导入服装主要来源国数据
     *
     * @param clothingSourceCountryList clothingSourceCountryList 服装主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingSourceCountry(List<ClothingSourceCountry> clothingSourceCountryList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingSourceCountryList) || clothingSourceCountryList.size() == 0) {
            throw new ServiceException("导入服装主要来源国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingSourceCountry clothingSourceCountry : clothingSourceCountryList){
            try {
                // 验证是否存在
                if (true) {
                    clothingSourceCountry.setCreateBy(operName);
                    this.insertClothingSourceCountry(clothingSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingSourceCountry.setUpdateBy(operName);
                    this.updateClothingSourceCountry(clothingSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
