package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingInvalidCustomsMapper;
import com.qianjing.project.clothing.domain.ClothingInvalidCustoms;
import com.qianjing.project.clothing.service.IClothingInvalidCustomsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格关区情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidCustomsServiceImpl implements IClothingInvalidCustomsService {

    private static final Logger log = LoggerFactory.getLogger(ClothingInvalidCustomsServiceImpl.class);

    private final ClothingInvalidCustomsMapper clothingInvalidCustomsMapper;

    /**
     * 查询服装不合格关区情况
     * 
     * @param id 服装不合格关区情况主键
     * @return 服装不合格关区情况
     */
    @Override
    public ClothingInvalidCustoms selectClothingInvalidCustomsById(Long id) {
        return clothingInvalidCustomsMapper.selectClothingInvalidCustomsById(id);
    }

    /**
     * 查询服装不合格关区情况列表
     * 
     * @param clothingInvalidCustoms 服装不合格关区情况
     * @return 服装不合格关区情况
     */
    @Override
    public List<ClothingInvalidCustoms> selectClothingInvalidCustomsList(ClothingInvalidCustoms clothingInvalidCustoms) {
        return clothingInvalidCustomsMapper.selectClothingInvalidCustomsList(clothingInvalidCustoms);
    }

    /**
     * 新增服装不合格关区情况
     * 
     * @param clothingInvalidCustoms 服装不合格关区情况
     * @return 结果
     */
    @Override
    public int insertClothingInvalidCustoms(ClothingInvalidCustoms clothingInvalidCustoms) {
        clothingInvalidCustoms.setCreateTime(DateUtils.getNowDate());
        return clothingInvalidCustomsMapper.insertClothingInvalidCustoms(clothingInvalidCustoms);
    }

    /**
     * 修改服装不合格关区情况
     * 
     * @param clothingInvalidCustoms 服装不合格关区情况
     * @return 结果
     */
    @Override
    public int updateClothingInvalidCustoms(ClothingInvalidCustoms clothingInvalidCustoms) {
        clothingInvalidCustoms.setUpdateTime(DateUtils.getNowDate());
        return clothingInvalidCustomsMapper.updateClothingInvalidCustoms(clothingInvalidCustoms);
    }

    /**
     * 批量删除服装不合格关区情况
     * 
     * @param ids 需要删除的服装不合格关区情况主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidCustomsByIds(Long[] ids) {
        return clothingInvalidCustomsMapper.deleteClothingInvalidCustomsByIds(ids);
    }

    /**
     * 删除服装不合格关区情况信息
     * 
     * @param id 服装不合格关区情况主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidCustomsById(Long id) {
        return clothingInvalidCustomsMapper.deleteClothingInvalidCustomsById(id);
    }

    /**
     * 导入服装不合格关区情况数据
     *
     * @param clothingInvalidCustomsList clothingInvalidCustomsList 服装不合格关区情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingInvalidCustoms(List<ClothingInvalidCustoms> clothingInvalidCustomsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingInvalidCustomsList) || clothingInvalidCustomsList.size() == 0) {
            throw new ServiceException("导入服装不合格关区情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingInvalidCustoms clothingInvalidCustoms : clothingInvalidCustomsList){
            try {
                // 验证是否存在
                if (true) {
                    clothingInvalidCustoms.setCreateBy(operName);
                    this.insertClothingInvalidCustoms(clothingInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingInvalidCustoms.setUpdateBy(operName);
                    this.updateClothingInvalidCustoms(clothingInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
