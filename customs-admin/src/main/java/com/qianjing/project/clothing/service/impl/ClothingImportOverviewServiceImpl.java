package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingImportOverviewMapper;
import com.qianjing.project.clothing.domain.ClothingImportOverview;
import com.qianjing.project.clothing.service.IClothingImportOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装进口概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingImportOverviewServiceImpl implements IClothingImportOverviewService {

    private static final Logger log = LoggerFactory.getLogger(ClothingImportOverviewServiceImpl.class);

    private final ClothingImportOverviewMapper clothingImportOverviewMapper;

    /**
     * 查询服装进口概况
     * 
     * @param id 服装进口概况主键
     * @return 服装进口概况
     */
    @Override
    public ClothingImportOverview selectClothingImportOverviewById(Long id) {
        return clothingImportOverviewMapper.selectClothingImportOverviewById(id);
    }

    /**
     * 查询服装进口概况列表
     * 
     * @param clothingImportOverview 服装进口概况
     * @return 服装进口概况
     */
    @Override
    public List<ClothingImportOverview> selectClothingImportOverviewList(ClothingImportOverview clothingImportOverview) {
        return clothingImportOverviewMapper.selectClothingImportOverviewList(clothingImportOverview);
    }

    /**
     * 新增服装进口概况
     * 
     * @param clothingImportOverview 服装进口概况
     * @return 结果
     */
    @Override
    public int insertClothingImportOverview(ClothingImportOverview clothingImportOverview) {
        clothingImportOverview.setCreateTime(DateUtils.getNowDate());
        return clothingImportOverviewMapper.insertClothingImportOverview(clothingImportOverview);
    }

    /**
     * 修改服装进口概况
     * 
     * @param clothingImportOverview 服装进口概况
     * @return 结果
     */
    @Override
    public int updateClothingImportOverview(ClothingImportOverview clothingImportOverview) {
        clothingImportOverview.setUpdateTime(DateUtils.getNowDate());
        return clothingImportOverviewMapper.updateClothingImportOverview(clothingImportOverview);
    }

    /**
     * 批量删除服装进口概况
     * 
     * @param ids 需要删除的服装进口概况主键
     * @return 结果
     */
    @Override
    public int deleteClothingImportOverviewByIds(Long[] ids) {
        return clothingImportOverviewMapper.deleteClothingImportOverviewByIds(ids);
    }

    /**
     * 删除服装进口概况信息
     * 
     * @param id 服装进口概况主键
     * @return 结果
     */
    @Override
    public int deleteClothingImportOverviewById(Long id) {
        return clothingImportOverviewMapper.deleteClothingImportOverviewById(id);
    }

    /**
     * 导入服装进口概况数据
     *
     * @param clothingImportOverviewList clothingImportOverviewList 服装进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingImportOverview(List<ClothingImportOverview> clothingImportOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingImportOverviewList) || clothingImportOverviewList.size() == 0) {
            throw new ServiceException("导入服装进口概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingImportOverview clothingImportOverview : clothingImportOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    clothingImportOverview.setCreateBy(operName);
                    this.insertClothingImportOverview(clothingImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingImportOverview.setUpdateBy(operName);
                    this.updateClothingImportOverview(clothingImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
