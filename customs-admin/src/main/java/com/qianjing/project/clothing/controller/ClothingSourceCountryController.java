package com.qianjing.project.clothing.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.clothing.domain.ClothingSourceCountry;
import com.qianjing.project.clothing.service.IClothingSourceCountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 服装主要来源国Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/clothing/country")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingSourceCountryController extends BaseController {

    private final IClothingSourceCountryService clothingSourceCountryService;

    /**
     * 查询服装主要来源国列表
     */
//    @PreAuthorize("@ss.hasPermi('clothing:country:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClothingSourceCountry clothingSourceCountry) {
        startPage();
        List<ClothingSourceCountry> list = clothingSourceCountryService.selectClothingSourceCountryList(clothingSourceCountry);
        return getDataTable(list);
    }

    /**
     * 导出服装主要来源国列表
     */
    @PreAuthorize("@ss.hasPermi('clothing:country:export')")
    @Log(title = "服装主要来源国", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClothingSourceCountry clothingSourceCountry) {
        List<ClothingSourceCountry> list = clothingSourceCountryService.selectClothingSourceCountryList(clothingSourceCountry);
        ExcelUtil<ClothingSourceCountry> util = new ExcelUtil<ClothingSourceCountry>(ClothingSourceCountry.class);
        return util.exportExcel(list, "服装主要来源国数据");
    }

    /**
     * 导入服装主要来源国列表
     */
    @Log(title = "服装主要来源国", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('clothing:country:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ClothingSourceCountry> util = new ExcelUtil<ClothingSourceCountry>(ClothingSourceCountry.class);
        List<ClothingSourceCountry> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = clothingSourceCountryService.importClothingSourceCountry(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载服装主要来源国导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ClothingSourceCountry> util = new ExcelUtil<ClothingSourceCountry>(ClothingSourceCountry.class);
        util.importTemplateExcel(response, "服装主要来源国数据");
    }

    /**
     * 获取服装主要来源国详细信息
     */
    @PreAuthorize("@ss.hasPermi('clothing:country:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(clothingSourceCountryService.selectClothingSourceCountryById(id));
    }

    /**
     * 新增服装主要来源国
     */
    @PreAuthorize("@ss.hasPermi('clothing:country:add')")
    @Log(title = "服装主要来源国", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClothingSourceCountry clothingSourceCountry) {
        return toAjax(clothingSourceCountryService.insertClothingSourceCountry(clothingSourceCountry));
    }

    /**
     * 修改服装主要来源国
     */
    @PreAuthorize("@ss.hasPermi('clothing:country:edit')")
    @Log(title = "服装主要来源国", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClothingSourceCountry clothingSourceCountry) {
        return toAjax(clothingSourceCountryService.updateClothingSourceCountry(clothingSourceCountry));
    }

    /**
     * 删除服装主要来源国
     */
    @PreAuthorize("@ss.hasPermi('clothing:country:remove')")
    @Log(title = "服装主要来源国", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(clothingSourceCountryService.deleteClothingSourceCountryByIds(ids));
    }
}
