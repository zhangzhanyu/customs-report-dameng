package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingInvalidSourceMapper;
import com.qianjing.project.clothing.domain.ClothingInvalidSource;
import com.qianjing.project.clothing.service.IClothingInvalidSourceService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格来源国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidSourceServiceImpl implements IClothingInvalidSourceService {

    private static final Logger log = LoggerFactory.getLogger(ClothingInvalidSourceServiceImpl.class);

    private final ClothingInvalidSourceMapper clothingInvalidSourceMapper;

    /**
     * 查询服装不合格来源国
     * 
     * @param id 服装不合格来源国主键
     * @return 服装不合格来源国
     */
    @Override
    public ClothingInvalidSource selectClothingInvalidSourceById(Long id) {
        return clothingInvalidSourceMapper.selectClothingInvalidSourceById(id);
    }

    /**
     * 查询服装不合格来源国列表
     * 
     * @param clothingInvalidSource 服装不合格来源国
     * @return 服装不合格来源国
     */
    @Override
    public List<ClothingInvalidSource> selectClothingInvalidSourceList(ClothingInvalidSource clothingInvalidSource) {
        return clothingInvalidSourceMapper.selectClothingInvalidSourceList(clothingInvalidSource);
    }

    /**
     * 新增服装不合格来源国
     * 
     * @param clothingInvalidSource 服装不合格来源国
     * @return 结果
     */
    @Override
    public int insertClothingInvalidSource(ClothingInvalidSource clothingInvalidSource) {
        clothingInvalidSource.setCreateTime(DateUtils.getNowDate());
        return clothingInvalidSourceMapper.insertClothingInvalidSource(clothingInvalidSource);
    }

    /**
     * 修改服装不合格来源国
     * 
     * @param clothingInvalidSource 服装不合格来源国
     * @return 结果
     */
    @Override
    public int updateClothingInvalidSource(ClothingInvalidSource clothingInvalidSource) {
        clothingInvalidSource.setUpdateTime(DateUtils.getNowDate());
        return clothingInvalidSourceMapper.updateClothingInvalidSource(clothingInvalidSource);
    }

    /**
     * 批量删除服装不合格来源国
     * 
     * @param ids 需要删除的服装不合格来源国主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidSourceByIds(Long[] ids) {
        return clothingInvalidSourceMapper.deleteClothingInvalidSourceByIds(ids);
    }

    /**
     * 删除服装不合格来源国信息
     * 
     * @param id 服装不合格来源国主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidSourceById(Long id) {
        return clothingInvalidSourceMapper.deleteClothingInvalidSourceById(id);
    }

    /**
     * 导入服装不合格来源国数据
     *
     * @param clothingInvalidSourceList clothingInvalidSourceList 服装不合格来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingInvalidSource(List<ClothingInvalidSource> clothingInvalidSourceList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingInvalidSourceList) || clothingInvalidSourceList.size() == 0) {
            throw new ServiceException("导入服装不合格来源国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingInvalidSource clothingInvalidSource : clothingInvalidSourceList){
            try {
                // 验证是否存在
                if (true) {
                    clothingInvalidSource.setCreateBy(operName);
                    this.insertClothingInvalidSource(clothingInvalidSource);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingInvalidSource.setUpdateBy(operName);
                    this.updateClothingInvalidSource(clothingInvalidSource);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
