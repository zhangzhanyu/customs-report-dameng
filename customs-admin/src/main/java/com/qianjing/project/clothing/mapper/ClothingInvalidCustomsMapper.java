package com.qianjing.project.clothing.mapper;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格关区情况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ClothingInvalidCustomsMapper {

    ClothingInvalidCustoms selectClothingInvalidCustomsById(Long id);

    List<ClothingInvalidCustoms> selectClothingInvalidCustomsList(ClothingInvalidCustoms clothingInvalidCustoms);

    int insertClothingInvalidCustoms(ClothingInvalidCustoms clothingInvalidCustoms);

    int updateClothingInvalidCustoms(ClothingInvalidCustoms clothingInvalidCustoms);

    int deleteClothingInvalidCustomsById(Long id);

    int deleteClothingInvalidCustomsByIds(Long[] ids);
}
