package com.qianjing.project.clothing.mapper;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingInvalidSource;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格来源国Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ClothingInvalidSourceMapper {

    ClothingInvalidSource selectClothingInvalidSourceById(Long id);

    List<ClothingInvalidSource> selectClothingInvalidSourceList(ClothingInvalidSource clothingInvalidSource);

    int insertClothingInvalidSource(ClothingInvalidSource clothingInvalidSource);

    int updateClothingInvalidSource(ClothingInvalidSource clothingInvalidSource);

    int deleteClothingInvalidSourceById(Long id);

    int deleteClothingInvalidSourceByIds(Long[] ids);
}
