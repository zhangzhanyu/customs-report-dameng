package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingImportPortMapper;
import com.qianjing.project.clothing.domain.ClothingImportPort;
import com.qianjing.project.clothing.service.IClothingImportPortService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装主要进口口岸Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingImportPortServiceImpl implements IClothingImportPortService {

    private static final Logger log = LoggerFactory.getLogger(ClothingImportPortServiceImpl.class);

    private final ClothingImportPortMapper clothingImportPortMapper;

    /**
     * 查询服装主要进口口岸
     * 
     * @param id 服装主要进口口岸主键
     * @return 服装主要进口口岸
     */
    @Override
    public ClothingImportPort selectClothingImportPortById(Long id) {
        return clothingImportPortMapper.selectClothingImportPortById(id);
    }

    /**
     * 查询服装主要进口口岸列表
     * 
     * @param clothingImportPort 服装主要进口口岸
     * @return 服装主要进口口岸
     */
    @Override
    public List<ClothingImportPort> selectClothingImportPortList(ClothingImportPort clothingImportPort) {
        return clothingImportPortMapper.selectClothingImportPortList(clothingImportPort);
    }

    /**
     * 新增服装主要进口口岸
     * 
     * @param clothingImportPort 服装主要进口口岸
     * @return 结果
     */
    @Override
    public int insertClothingImportPort(ClothingImportPort clothingImportPort) {
        clothingImportPort.setCreateTime(DateUtils.getNowDate());
        return clothingImportPortMapper.insertClothingImportPort(clothingImportPort);
    }

    /**
     * 修改服装主要进口口岸
     * 
     * @param clothingImportPort 服装主要进口口岸
     * @return 结果
     */
    @Override
    public int updateClothingImportPort(ClothingImportPort clothingImportPort) {
        clothingImportPort.setUpdateTime(DateUtils.getNowDate());
        return clothingImportPortMapper.updateClothingImportPort(clothingImportPort);
    }

    /**
     * 批量删除服装主要进口口岸
     * 
     * @param ids 需要删除的服装主要进口口岸主键
     * @return 结果
     */
    @Override
    public int deleteClothingImportPortByIds(Long[] ids) {
        return clothingImportPortMapper.deleteClothingImportPortByIds(ids);
    }

    /**
     * 删除服装主要进口口岸信息
     * 
     * @param id 服装主要进口口岸主键
     * @return 结果
     */
    @Override
    public int deleteClothingImportPortById(Long id) {
        return clothingImportPortMapper.deleteClothingImportPortById(id);
    }

    /**
     * 导入服装主要进口口岸数据
     *
     * @param clothingImportPortList clothingImportPortList 服装主要进口口岸信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingImportPort(List<ClothingImportPort> clothingImportPortList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingImportPortList) || clothingImportPortList.size() == 0) {
            throw new ServiceException("导入服装主要进口口岸数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingImportPort clothingImportPort : clothingImportPortList){
            try {
                // 验证是否存在
                if (true) {
                    clothingImportPort.setCreateBy(operName);
                    this.insertClothingImportPort(clothingImportPort);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingImportPort.setUpdateBy(operName);
                    this.updateClothingImportPort(clothingImportPort);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
