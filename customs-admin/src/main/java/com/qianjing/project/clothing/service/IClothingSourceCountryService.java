package com.qianjing.project.clothing.service;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装主要来源国Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IClothingSourceCountryService 
{
    /**
     * 查询服装主要来源国
     * 
     * @param id 服装主要来源国主键
     * @return 服装主要来源国
     */
    public ClothingSourceCountry selectClothingSourceCountryById(Long id);

    /**
     * 查询服装主要来源国列表
     * 
     * @param clothingSourceCountry 服装主要来源国
     * @return 服装主要来源国集合
     */
    public List<ClothingSourceCountry> selectClothingSourceCountryList(ClothingSourceCountry clothingSourceCountry);

    /**
     * 新增服装主要来源国
     * 
     * @param clothingSourceCountry 服装主要来源国
     * @return 结果
     */
    public int insertClothingSourceCountry(ClothingSourceCountry clothingSourceCountry);

    /**
     * 修改服装主要来源国
     * 
     * @param clothingSourceCountry 服装主要来源国
     * @return 结果
     */
    public int updateClothingSourceCountry(ClothingSourceCountry clothingSourceCountry);

    /**
     * 批量删除服装主要来源国
     * 
     * @param ids 需要删除的服装主要来源国主键集合
     * @return 结果
     */
    public int deleteClothingSourceCountryByIds(Long[] ids);

    /**
     * 删除服装主要来源国信息
     * 
     * @param id 服装主要来源国主键
     * @return 结果
     */
    public int deleteClothingSourceCountryById(Long id);

    /**
     * 导入服装主要来源国信息
     *
     * @param clothingSourceCountryList 服装主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importClothingSourceCountry(List<ClothingSourceCountry> clothingSourceCountryList, Boolean isUpdateSupport, String operName);
}
