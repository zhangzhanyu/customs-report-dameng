package com.qianjing.project.clothing.service;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格关区情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IClothingInvalidCustomsService 
{
    /**
     * 查询服装不合格关区情况
     * 
     * @param id 服装不合格关区情况主键
     * @return 服装不合格关区情况
     */
    public ClothingInvalidCustoms selectClothingInvalidCustomsById(Long id);

    /**
     * 查询服装不合格关区情况列表
     * 
     * @param clothingInvalidCustoms 服装不合格关区情况
     * @return 服装不合格关区情况集合
     */
    public List<ClothingInvalidCustoms> selectClothingInvalidCustomsList(ClothingInvalidCustoms clothingInvalidCustoms);

    /**
     * 新增服装不合格关区情况
     * 
     * @param clothingInvalidCustoms 服装不合格关区情况
     * @return 结果
     */
    public int insertClothingInvalidCustoms(ClothingInvalidCustoms clothingInvalidCustoms);

    /**
     * 修改服装不合格关区情况
     * 
     * @param clothingInvalidCustoms 服装不合格关区情况
     * @return 结果
     */
    public int updateClothingInvalidCustoms(ClothingInvalidCustoms clothingInvalidCustoms);

    /**
     * 批量删除服装不合格关区情况
     * 
     * @param ids 需要删除的服装不合格关区情况主键集合
     * @return 结果
     */
    public int deleteClothingInvalidCustomsByIds(Long[] ids);

    /**
     * 删除服装不合格关区情况信息
     * 
     * @param id 服装不合格关区情况主键
     * @return 结果
     */
    public int deleteClothingInvalidCustomsById(Long id);

    /**
     * 导入服装不合格关区情况信息
     *
     * @param clothingInvalidCustomsList 服装不合格关区情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importClothingInvalidCustoms(List<ClothingInvalidCustoms> clothingInvalidCustomsList, Boolean isUpdateSupport, String operName);
}
