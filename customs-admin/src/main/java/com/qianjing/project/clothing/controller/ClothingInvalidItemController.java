package com.qianjing.project.clothing.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.clothing.domain.ClothingInvalidItem;
import com.qianjing.project.clothing.service.IClothingInvalidItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 服装不合格项目情况Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/clothing/item")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidItemController extends BaseController {

    private final IClothingInvalidItemService clothingInvalidItemService;

    /**
     * 查询服装不合格项目情况列表
     */
//    @PreAuthorize("@ss.hasPermi('clothing:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClothingInvalidItem clothingInvalidItem) {
        startPage();
        List<ClothingInvalidItem> list = clothingInvalidItemService.selectClothingInvalidItemList(clothingInvalidItem);
        return getDataTable(list);
    }

    /**
     * 导出服装不合格项目情况列表
     */
    @PreAuthorize("@ss.hasPermi('clothing:item:export')")
    @Log(title = "服装不合格项目情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ClothingInvalidItem clothingInvalidItem) {
        List<ClothingInvalidItem> list = clothingInvalidItemService.selectClothingInvalidItemList(clothingInvalidItem);
        ExcelUtil<ClothingInvalidItem> util = new ExcelUtil<ClothingInvalidItem>(ClothingInvalidItem.class);
        return util.exportExcel(list, "服装不合格项目情况数据");
    }

    /**
     * 导入服装不合格项目情况列表
     */
    @Log(title = "服装不合格项目情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('clothing:item:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ClothingInvalidItem> util = new ExcelUtil<ClothingInvalidItem>(ClothingInvalidItem.class);
        List<ClothingInvalidItem> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = clothingInvalidItemService.importClothingInvalidItem(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载服装不合格项目情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ClothingInvalidItem> util = new ExcelUtil<ClothingInvalidItem>(ClothingInvalidItem.class);
        util.importTemplateExcel(response, "服装不合格项目情况数据");
    }

    /**
     * 获取服装不合格项目情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('clothing:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(clothingInvalidItemService.selectClothingInvalidItemById(id));
    }

    /**
     * 新增服装不合格项目情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:item:add')")
    @Log(title = "服装不合格项目情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClothingInvalidItem clothingInvalidItem) {
        return toAjax(clothingInvalidItemService.insertClothingInvalidItem(clothingInvalidItem));
    }

    /**
     * 修改服装不合格项目情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:item:edit')")
    @Log(title = "服装不合格项目情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClothingInvalidItem clothingInvalidItem) {
        return toAjax(clothingInvalidItemService.updateClothingInvalidItem(clothingInvalidItem));
    }

    /**
     * 删除服装不合格项目情况
     */
    @PreAuthorize("@ss.hasPermi('clothing:item:remove')")
    @Log(title = "服装不合格项目情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(clothingInvalidItemService.deleteClothingInvalidItemByIds(ids));
    }
}
