package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingInvalidCompanyMapper;
import com.qianjing.project.clothing.domain.ClothingInvalidCompany;
import com.qianjing.project.clothing.service.IClothingInvalidCompanyService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格企业情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingInvalidCompanyServiceImpl implements IClothingInvalidCompanyService {

    private static final Logger log = LoggerFactory.getLogger(ClothingInvalidCompanyServiceImpl.class);

    private final ClothingInvalidCompanyMapper clothingInvalidCompanyMapper;

    /**
     * 查询服装不合格企业情况
     * 
     * @param id 服装不合格企业情况主键
     * @return 服装不合格企业情况
     */
    @Override
    public ClothingInvalidCompany selectClothingInvalidCompanyById(Long id) {
        return clothingInvalidCompanyMapper.selectClothingInvalidCompanyById(id);
    }

    /**
     * 查询服装不合格企业情况列表
     * 
     * @param clothingInvalidCompany 服装不合格企业情况
     * @return 服装不合格企业情况
     */
    @Override
    public List<ClothingInvalidCompany> selectClothingInvalidCompanyList(ClothingInvalidCompany clothingInvalidCompany) {
        return clothingInvalidCompanyMapper.selectClothingInvalidCompanyList(clothingInvalidCompany);
    }

    /**
     * 新增服装不合格企业情况
     * 
     * @param clothingInvalidCompany 服装不合格企业情况
     * @return 结果
     */
    @Override
    public int insertClothingInvalidCompany(ClothingInvalidCompany clothingInvalidCompany) {
        clothingInvalidCompany.setCreateTime(DateUtils.getNowDate());
        return clothingInvalidCompanyMapper.insertClothingInvalidCompany(clothingInvalidCompany);
    }

    /**
     * 修改服装不合格企业情况
     * 
     * @param clothingInvalidCompany 服装不合格企业情况
     * @return 结果
     */
    @Override
    public int updateClothingInvalidCompany(ClothingInvalidCompany clothingInvalidCompany) {
        clothingInvalidCompany.setUpdateTime(DateUtils.getNowDate());
        return clothingInvalidCompanyMapper.updateClothingInvalidCompany(clothingInvalidCompany);
    }

    /**
     * 批量删除服装不合格企业情况
     * 
     * @param ids 需要删除的服装不合格企业情况主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidCompanyByIds(Long[] ids) {
        return clothingInvalidCompanyMapper.deleteClothingInvalidCompanyByIds(ids);
    }

    /**
     * 删除服装不合格企业情况信息
     * 
     * @param id 服装不合格企业情况主键
     * @return 结果
     */
    @Override
    public int deleteClothingInvalidCompanyById(Long id) {
        return clothingInvalidCompanyMapper.deleteClothingInvalidCompanyById(id);
    }

    /**
     * 导入服装不合格企业情况数据
     *
     * @param clothingInvalidCompanyList clothingInvalidCompanyList 服装不合格企业情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingInvalidCompany(List<ClothingInvalidCompany> clothingInvalidCompanyList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingInvalidCompanyList) || clothingInvalidCompanyList.size() == 0) {
            throw new ServiceException("导入服装不合格企业情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingInvalidCompany clothingInvalidCompany : clothingInvalidCompanyList){
            try {
                // 验证是否存在
                if (true) {
                    clothingInvalidCompany.setCreateBy(operName);
                    this.insertClothingInvalidCompany(clothingInvalidCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingInvalidCompany.setUpdateBy(operName);
                    this.updateClothingInvalidCompany(clothingInvalidCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
