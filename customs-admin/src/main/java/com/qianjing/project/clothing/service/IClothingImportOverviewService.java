package com.qianjing.project.clothing.service;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingImportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装进口概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IClothingImportOverviewService 
{
    /**
     * 查询服装进口概况
     * 
     * @param id 服装进口概况主键
     * @return 服装进口概况
     */
    public ClothingImportOverview selectClothingImportOverviewById(Long id);

    /**
     * 查询服装进口概况列表
     * 
     * @param clothingImportOverview 服装进口概况
     * @return 服装进口概况集合
     */
    public List<ClothingImportOverview> selectClothingImportOverviewList(ClothingImportOverview clothingImportOverview);

    /**
     * 新增服装进口概况
     * 
     * @param clothingImportOverview 服装进口概况
     * @return 结果
     */
    public int insertClothingImportOverview(ClothingImportOverview clothingImportOverview);

    /**
     * 修改服装进口概况
     * 
     * @param clothingImportOverview 服装进口概况
     * @return 结果
     */
    public int updateClothingImportOverview(ClothingImportOverview clothingImportOverview);

    /**
     * 批量删除服装进口概况
     * 
     * @param ids 需要删除的服装进口概况主键集合
     * @return 结果
     */
    public int deleteClothingImportOverviewByIds(Long[] ids);

    /**
     * 删除服装进口概况信息
     * 
     * @param id 服装进口概况主键
     * @return 结果
     */
    public int deleteClothingImportOverviewById(Long id);

    /**
     * 导入服装进口概况信息
     *
     * @param clothingImportOverviewList 服装进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importClothingImportOverview(List<ClothingImportOverview> clothingImportOverviewList, Boolean isUpdateSupport, String operName);
}
