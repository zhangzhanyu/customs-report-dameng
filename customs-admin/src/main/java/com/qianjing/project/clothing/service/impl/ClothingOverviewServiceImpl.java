package com.qianjing.project.clothing.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.clothing.mapper.ClothingOverviewMapper;
import com.qianjing.project.clothing.domain.ClothingOverview;
import com.qianjing.project.clothing.service.IClothingOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClothingOverviewServiceImpl implements IClothingOverviewService {

    private static final Logger log = LoggerFactory.getLogger(ClothingOverviewServiceImpl.class);

    private final ClothingOverviewMapper clothingOverviewMapper;

    /**
     * 查询服装概况
     * 
     * @param id 服装概况主键
     * @return 服装概况
     */
    @Override
    public ClothingOverview selectClothingOverviewById(Long id) {
        return clothingOverviewMapper.selectClothingOverviewById(id);
    }

    /**
     * 查询服装概况列表
     * 
     * @param clothingOverview 服装概况
     * @return 服装概况
     */
    @Override
    public List<ClothingOverview> selectClothingOverviewList(ClothingOverview clothingOverview) {
        return clothingOverviewMapper.selectClothingOverviewList(clothingOverview);
    }

    /**
     * 新增服装概况
     * 
     * @param clothingOverview 服装概况
     * @return 结果
     */
    @Override
    public int insertClothingOverview(ClothingOverview clothingOverview) {
        clothingOverview.setCreateTime(DateUtils.getNowDate());
        return clothingOverviewMapper.insertClothingOverview(clothingOverview);
    }

    /**
     * 修改服装概况
     * 
     * @param clothingOverview 服装概况
     * @return 结果
     */
    @Override
    public int updateClothingOverview(ClothingOverview clothingOverview) {
        clothingOverview.setUpdateTime(DateUtils.getNowDate());
        return clothingOverviewMapper.updateClothingOverview(clothingOverview);
    }

    /**
     * 批量删除服装概况
     * 
     * @param ids 需要删除的服装概况主键
     * @return 结果
     */
    @Override
    public int deleteClothingOverviewByIds(Long[] ids) {
        return clothingOverviewMapper.deleteClothingOverviewByIds(ids);
    }

    /**
     * 删除服装概况信息
     * 
     * @param id 服装概况主键
     * @return 结果
     */
    @Override
    public int deleteClothingOverviewById(Long id) {
        return clothingOverviewMapper.deleteClothingOverviewById(id);
    }

    /**
     * 导入服装概况数据
     *
     * @param clothingOverviewList clothingOverviewList 服装概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importClothingOverview(List<ClothingOverview> clothingOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(clothingOverviewList) || clothingOverviewList.size() == 0) {
            throw new ServiceException("导入服装概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ClothingOverview clothingOverview : clothingOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    clothingOverview.setCreateBy(operName);
                    this.insertClothingOverview(clothingOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    clothingOverview.setUpdateBy(operName);
                    this.updateClothingOverview(clothingOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
