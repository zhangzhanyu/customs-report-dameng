package com.qianjing.project.clothing.service;

import java.util.List;
import com.qianjing.project.clothing.domain.ClothingInvalidItem;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格项目情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IClothingInvalidItemService 
{
    /**
     * 查询服装不合格项目情况
     * 
     * @param id 服装不合格项目情况主键
     * @return 服装不合格项目情况
     */
    public ClothingInvalidItem selectClothingInvalidItemById(Long id);

    /**
     * 查询服装不合格项目情况列表
     * 
     * @param clothingInvalidItem 服装不合格项目情况
     * @return 服装不合格项目情况集合
     */
    public List<ClothingInvalidItem> selectClothingInvalidItemList(ClothingInvalidItem clothingInvalidItem);

    /**
     * 新增服装不合格项目情况
     * 
     * @param clothingInvalidItem 服装不合格项目情况
     * @return 结果
     */
    public int insertClothingInvalidItem(ClothingInvalidItem clothingInvalidItem);

    /**
     * 修改服装不合格项目情况
     * 
     * @param clothingInvalidItem 服装不合格项目情况
     * @return 结果
     */
    public int updateClothingInvalidItem(ClothingInvalidItem clothingInvalidItem);

    /**
     * 批量删除服装不合格项目情况
     * 
     * @param ids 需要删除的服装不合格项目情况主键集合
     * @return 结果
     */
    public int deleteClothingInvalidItemByIds(Long[] ids);

    /**
     * 删除服装不合格项目情况信息
     * 
     * @param id 服装不合格项目情况主键
     * @return 结果
     */
    public int deleteClothingInvalidItemById(Long id);

    /**
     * 导入服装不合格项目情况信息
     *
     * @param clothingInvalidItemList 服装不合格项目情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importClothingInvalidItem(List<ClothingInvalidItem> clothingInvalidItemList, Boolean isUpdateSupport, String operName);
}
