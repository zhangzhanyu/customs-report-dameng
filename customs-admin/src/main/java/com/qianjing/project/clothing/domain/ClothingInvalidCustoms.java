package com.qianjing.project.clothing.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 服装不合格关区情况对象 clothing_invalid_customs
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public class ClothingInvalidCustoms extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private Long sortNum;

    /** 关区 */
    @Excel(name = "关区")
    private String checkCustoms;

    /** 报关批 */
    @Excel(name = "报关批")
    private Long checkBatch;

    /** 货物批 */
    @Excel(name = "货物批")
    private Long checkGoodsBatch;

    /** 金额(美元) */
    @Excel(name = "金额(美元)")
    private BigDecimal usdPrice;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setSortNum(Long sortNum){
        this.sortNum = sortNum;
    }

    public Long getSortNum(){
        return sortNum;
    }
    public void setCheckCustoms(String checkCustoms){
        this.checkCustoms = checkCustoms;
    }

    public String getCheckCustoms(){
        return checkCustoms;
    }
    public void setCheckBatch(Long checkBatch){
        this.checkBatch = checkBatch;
    }

    public Long getCheckBatch(){
        return checkBatch;
    }
    public void setCheckGoodsBatch(Long checkGoodsBatch){
        this.checkGoodsBatch = checkGoodsBatch;
    }

    public Long getCheckGoodsBatch(){
        return checkGoodsBatch;
    }
    public void setUsdPrice(BigDecimal usdPrice){
        this.usdPrice = usdPrice;
    }

    public BigDecimal getUsdPrice(){
        return usdPrice;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sortNum", getSortNum())
            .append("checkCustoms", getCheckCustoms())
            .append("checkBatch", getCheckBatch())
            .append("checkGoodsBatch", getCheckGoodsBatch())
            .append("usdPrice", getUsdPrice())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
