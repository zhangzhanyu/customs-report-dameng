package com.qianjing.project.electric.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.electric.mapper.ElectricSourceCountryMapper;
import com.qianjing.project.electric.domain.ElectricSourceCountry;
import com.qianjing.project.electric.service.IElectricSourceCountryService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要来源国Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricSourceCountryServiceImpl implements IElectricSourceCountryService {

    private static final Logger log = LoggerFactory.getLogger(ElectricSourceCountryServiceImpl.class);

    private final ElectricSourceCountryMapper electricSourceCountryMapper;

    /**
     * 查询机电消费品主要来源国
     * 
     * @param id 机电消费品主要来源国主键
     * @return 机电消费品主要来源国
     */
    @Override
    public ElectricSourceCountry selectElectricSourceCountryById(Long id) {
        return electricSourceCountryMapper.selectElectricSourceCountryById(id);
    }

    /**
     * 查询机电消费品主要来源国列表
     * 
     * @param electricSourceCountry 机电消费品主要来源国
     * @return 机电消费品主要来源国
     */
    @Override
    public List<ElectricSourceCountry> selectElectricSourceCountryList(ElectricSourceCountry electricSourceCountry) {
        return electricSourceCountryMapper.selectElectricSourceCountryList(electricSourceCountry);
    }

    /**
     * 新增机电消费品主要来源国
     * 
     * @param electricSourceCountry 机电消费品主要来源国
     * @return 结果
     */
    @Override
    public int insertElectricSourceCountry(ElectricSourceCountry electricSourceCountry) {
        electricSourceCountry.setCreateTime(DateUtils.getNowDate());
        return electricSourceCountryMapper.insertElectricSourceCountry(electricSourceCountry);
    }

    /**
     * 修改机电消费品主要来源国
     * 
     * @param electricSourceCountry 机电消费品主要来源国
     * @return 结果
     */
    @Override
    public int updateElectricSourceCountry(ElectricSourceCountry electricSourceCountry) {
        electricSourceCountry.setUpdateTime(DateUtils.getNowDate());
        return electricSourceCountryMapper.updateElectricSourceCountry(electricSourceCountry);
    }

    /**
     * 批量删除机电消费品主要来源国
     * 
     * @param ids 需要删除的机电消费品主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteElectricSourceCountryByIds(Long[] ids) {
        return electricSourceCountryMapper.deleteElectricSourceCountryByIds(ids);
    }

    /**
     * 删除机电消费品主要来源国信息
     * 
     * @param id 机电消费品主要来源国主键
     * @return 结果
     */
    @Override
    public int deleteElectricSourceCountryById(Long id) {
        return electricSourceCountryMapper.deleteElectricSourceCountryById(id);
    }

    /**
     * 导入机电消费品主要来源国数据
     *
     * @param electricSourceCountryList electricSourceCountryList 机电消费品主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importElectricSourceCountry(List<ElectricSourceCountry> electricSourceCountryList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(electricSourceCountryList) || electricSourceCountryList.size() == 0) {
            throw new ServiceException("导入机电消费品主要来源国数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ElectricSourceCountry electricSourceCountry : electricSourceCountryList){
            try {
                // 验证是否存在
                if (true) {
                    electricSourceCountry.setCreateBy(operName);
                    this.insertElectricSourceCountry(electricSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    electricSourceCountry.setUpdateBy(operName);
                    this.updateElectricSourceCountry(electricSourceCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
