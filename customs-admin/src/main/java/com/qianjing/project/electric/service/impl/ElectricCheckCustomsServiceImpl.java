package com.qianjing.project.electric.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.electric.mapper.ElectricCheckCustomsMapper;
import com.qianjing.project.electric.domain.ElectricCheckCustoms;
import com.qianjing.project.electric.service.IElectricCheckCustomsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要查验关区Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricCheckCustomsServiceImpl implements IElectricCheckCustomsService {

    private static final Logger log = LoggerFactory.getLogger(ElectricCheckCustomsServiceImpl.class);

    private final ElectricCheckCustomsMapper electricCheckCustomsMapper;

    /**
     * 查询机电消费品主要查验关区
     * 
     * @param id 机电消费品主要查验关区主键
     * @return 机电消费品主要查验关区
     */
    @Override
    public ElectricCheckCustoms selectElectricCheckCustomsById(Long id) {
        return electricCheckCustomsMapper.selectElectricCheckCustomsById(id);
    }

    /**
     * 查询机电消费品主要查验关区列表
     * 
     * @param electricCheckCustoms 机电消费品主要查验关区
     * @return 机电消费品主要查验关区
     */
    @Override
    public List<ElectricCheckCustoms> selectElectricCheckCustomsList(ElectricCheckCustoms electricCheckCustoms) {
        return electricCheckCustomsMapper.selectElectricCheckCustomsList(electricCheckCustoms);
    }

    /**
     * 新增机电消费品主要查验关区
     * 
     * @param electricCheckCustoms 机电消费品主要查验关区
     * @return 结果
     */
    @Override
    public int insertElectricCheckCustoms(ElectricCheckCustoms electricCheckCustoms) {
        electricCheckCustoms.setCreateTime(DateUtils.getNowDate());
        return electricCheckCustomsMapper.insertElectricCheckCustoms(electricCheckCustoms);
    }

    /**
     * 修改机电消费品主要查验关区
     * 
     * @param electricCheckCustoms 机电消费品主要查验关区
     * @return 结果
     */
    @Override
    public int updateElectricCheckCustoms(ElectricCheckCustoms electricCheckCustoms) {
        electricCheckCustoms.setUpdateTime(DateUtils.getNowDate());
        return electricCheckCustomsMapper.updateElectricCheckCustoms(electricCheckCustoms);
    }

    /**
     * 批量删除机电消费品主要查验关区
     * 
     * @param ids 需要删除的机电消费品主要查验关区主键
     * @return 结果
     */
    @Override
    public int deleteElectricCheckCustomsByIds(Long[] ids) {
        return electricCheckCustomsMapper.deleteElectricCheckCustomsByIds(ids);
    }

    /**
     * 删除机电消费品主要查验关区信息
     * 
     * @param id 机电消费品主要查验关区主键
     * @return 结果
     */
    @Override
    public int deleteElectricCheckCustomsById(Long id) {
        return electricCheckCustomsMapper.deleteElectricCheckCustomsById(id);
    }

    /**
     * 导入机电消费品主要查验关区数据
     *
     * @param electricCheckCustomsList electricCheckCustomsList 机电消费品主要查验关区信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importElectricCheckCustoms(List<ElectricCheckCustoms> electricCheckCustomsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(electricCheckCustomsList) || electricCheckCustomsList.size() == 0) {
            throw new ServiceException("导入机电消费品主要查验关区数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ElectricCheckCustoms electricCheckCustoms : electricCheckCustomsList){
            try {
                // 验证是否存在
                if (true) {
                    electricCheckCustoms.setCreateBy(operName);
                    this.insertElectricCheckCustoms(electricCheckCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    electricCheckCustoms.setUpdateBy(operName);
                    this.updateElectricCheckCustoms(electricCheckCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
