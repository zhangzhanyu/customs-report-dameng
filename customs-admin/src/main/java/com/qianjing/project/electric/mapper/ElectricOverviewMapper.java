package com.qianjing.project.electric.mapper;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要数量Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ElectricOverviewMapper {

    ElectricOverview selectElectricOverviewById(Long id);

    List<ElectricOverview> selectElectricOverviewList(ElectricOverview electricOverview);

    int insertElectricOverview(ElectricOverview electricOverview);

    int updateElectricOverview(ElectricOverview electricOverview);

    int deleteElectricOverviewById(Long id);

    int deleteElectricOverviewByIds(Long[] ids);
}
