package com.qianjing.project.electric.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.electric.mapper.ElectricOverviewMapper;
import com.qianjing.project.electric.domain.ElectricOverview;
import com.qianjing.project.electric.service.IElectricOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要数量Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricOverviewServiceImpl implements IElectricOverviewService {

    private static final Logger log = LoggerFactory.getLogger(ElectricOverviewServiceImpl.class);

    private final ElectricOverviewMapper electricOverviewMapper;

    /**
     * 查询机电消费品主要数量
     * 
     * @param id 机电消费品主要数量主键
     * @return 机电消费品主要数量
     */
    @Override
    public ElectricOverview selectElectricOverviewById(Long id) {
        return electricOverviewMapper.selectElectricOverviewById(id);
    }

    /**
     * 查询机电消费品主要数量列表
     * 
     * @param electricOverview 机电消费品主要数量
     * @return 机电消费品主要数量
     */
    @Override
    public List<ElectricOverview> selectElectricOverviewList(ElectricOverview electricOverview) {
        return electricOverviewMapper.selectElectricOverviewList(electricOverview);
    }

    /**
     * 新增机电消费品主要数量
     * 
     * @param electricOverview 机电消费品主要数量
     * @return 结果
     */
    @Override
    public int insertElectricOverview(ElectricOverview electricOverview) {
        electricOverview.setCreateTime(DateUtils.getNowDate());
        return electricOverviewMapper.insertElectricOverview(electricOverview);
    }

    /**
     * 修改机电消费品主要数量
     * 
     * @param electricOverview 机电消费品主要数量
     * @return 结果
     */
    @Override
    public int updateElectricOverview(ElectricOverview electricOverview) {
        electricOverview.setUpdateTime(DateUtils.getNowDate());
        return electricOverviewMapper.updateElectricOverview(electricOverview);
    }

    /**
     * 批量删除机电消费品主要数量
     * 
     * @param ids 需要删除的机电消费品主要数量主键
     * @return 结果
     */
    @Override
    public int deleteElectricOverviewByIds(Long[] ids) {
        return electricOverviewMapper.deleteElectricOverviewByIds(ids);
    }

    /**
     * 删除机电消费品主要数量信息
     * 
     * @param id 机电消费品主要数量主键
     * @return 结果
     */
    @Override
    public int deleteElectricOverviewById(Long id) {
        return electricOverviewMapper.deleteElectricOverviewById(id);
    }

    /**
     * 导入机电消费品主要数量数据
     *
     * @param electricOverviewList electricOverviewList 机电消费品主要数量信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importElectricOverview(List<ElectricOverview> electricOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(electricOverviewList) || electricOverviewList.size() == 0) {
            throw new ServiceException("导入机电消费品主要数量数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ElectricOverview electricOverview : electricOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    electricOverview.setCreateBy(operName);
                    this.insertElectricOverview(electricOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    electricOverview.setUpdateBy(operName);
                    this.updateElectricOverview(electricOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
