package com.qianjing.project.electric.mapper;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要关区不合格Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ElectricInvalidCustomsMapper {

    ElectricInvalidCustoms selectElectricInvalidCustomsById(Long id);

    List<ElectricInvalidCustoms> selectElectricInvalidCustomsList(ElectricInvalidCustoms electricInvalidCustoms);

    int insertElectricInvalidCustoms(ElectricInvalidCustoms electricInvalidCustoms);

    int updateElectricInvalidCustoms(ElectricInvalidCustoms electricInvalidCustoms);

    int deleteElectricInvalidCustomsById(Long id);

    int deleteElectricInvalidCustomsByIds(Long[] ids);
}
