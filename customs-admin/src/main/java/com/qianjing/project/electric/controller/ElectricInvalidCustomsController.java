package com.qianjing.project.electric.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.electric.domain.ElectricInvalidCustoms;
import com.qianjing.project.electric.service.IElectricInvalidCustomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 机电消费品主要关区不合格Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/electric/invalidCustoms")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricInvalidCustomsController extends BaseController {

    private final IElectricInvalidCustomsService electricInvalidCustomsService;

    /**
     * 查询机电消费品主要关区不合格列表
     */
//    @PreAuthorize("@ss.hasPermi('electric:invalidCustoms:list')")
    @GetMapping("/list")
    public TableDataInfo list(ElectricInvalidCustoms electricInvalidCustoms) {
        startPage();
        List<ElectricInvalidCustoms> list = electricInvalidCustomsService.selectElectricInvalidCustomsList(electricInvalidCustoms);
        return getDataTable(list);
    }

    /**
     * 导出机电消费品主要关区不合格列表
     */
    @PreAuthorize("@ss.hasPermi('electric:invalidCustoms:export')")
    @Log(title = "机电消费品主要关区不合格", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ElectricInvalidCustoms electricInvalidCustoms) {
        List<ElectricInvalidCustoms> list = electricInvalidCustomsService.selectElectricInvalidCustomsList(electricInvalidCustoms);
        ExcelUtil<ElectricInvalidCustoms> util = new ExcelUtil<ElectricInvalidCustoms>(ElectricInvalidCustoms.class);
        return util.exportExcel(list, "机电消费品主要关区不合格数据");
    }

    /**
     * 导入机电消费品主要关区不合格列表
     */
    @Log(title = "机电消费品主要关区不合格", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('electric:invalidCustoms:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ElectricInvalidCustoms> util = new ExcelUtil<ElectricInvalidCustoms>(ElectricInvalidCustoms.class);
        List<ElectricInvalidCustoms> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = electricInvalidCustomsService.importElectricInvalidCustoms(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载机电消费品主要关区不合格导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ElectricInvalidCustoms> util = new ExcelUtil<ElectricInvalidCustoms>(ElectricInvalidCustoms.class);
        util.importTemplateExcel(response, "机电消费品主要关区不合格数据");
    }

    /**
     * 获取机电消费品主要关区不合格详细信息
     */
    @PreAuthorize("@ss.hasPermi('electric:invalidCustoms:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(electricInvalidCustomsService.selectElectricInvalidCustomsById(id));
    }

    /**
     * 新增机电消费品主要关区不合格
     */
    @PreAuthorize("@ss.hasPermi('electric:invalidCustoms:add')")
    @Log(title = "机电消费品主要关区不合格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ElectricInvalidCustoms electricInvalidCustoms) {
        return toAjax(electricInvalidCustomsService.insertElectricInvalidCustoms(electricInvalidCustoms));
    }

    /**
     * 修改机电消费品主要关区不合格
     */
    @PreAuthorize("@ss.hasPermi('electric:invalidCustoms:edit')")
    @Log(title = "机电消费品主要关区不合格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ElectricInvalidCustoms electricInvalidCustoms) {
        return toAjax(electricInvalidCustomsService.updateElectricInvalidCustoms(electricInvalidCustoms));
    }

    /**
     * 删除机电消费品主要关区不合格
     */
    @PreAuthorize("@ss.hasPermi('electric:invalidCustoms:remove')")
    @Log(title = "机电消费品主要关区不合格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(electricInvalidCustomsService.deleteElectricInvalidCustomsByIds(ids));
    }
}
