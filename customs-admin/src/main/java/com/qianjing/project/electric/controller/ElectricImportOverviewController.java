package com.qianjing.project.electric.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.electric.domain.ElectricImportOverview;
import com.qianjing.project.electric.service.IElectricImportOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 机电消费品进口概况Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/electric/importOverview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricImportOverviewController extends BaseController {

    private final IElectricImportOverviewService electricImportOverviewService;

    /**
     * 查询机电消费品进口概况列表
     */
//    @PreAuthorize("@ss.hasPermi('electric:importOverview:list')")
    @GetMapping("/list")
    public TableDataInfo list(ElectricImportOverview electricImportOverview) {
        startPage();
        List<ElectricImportOverview> list = electricImportOverviewService.selectElectricImportOverviewList(electricImportOverview);
        return getDataTable(list);
    }

    /**
     * 导出机电消费品进口概况列表
     */
    @PreAuthorize("@ss.hasPermi('electric:importOverview:export')")
    @Log(title = "机电消费品进口概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ElectricImportOverview electricImportOverview) {
        List<ElectricImportOverview> list = electricImportOverviewService.selectElectricImportOverviewList(electricImportOverview);
        ExcelUtil<ElectricImportOverview> util = new ExcelUtil<ElectricImportOverview>(ElectricImportOverview.class);
        return util.exportExcel(list, "机电消费品进口概况数据");
    }

    /**
     * 导入机电消费品进口概况列表
     */
    @Log(title = "机电消费品进口概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('electric:importOverview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ElectricImportOverview> util = new ExcelUtil<ElectricImportOverview>(ElectricImportOverview.class);
        List<ElectricImportOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = electricImportOverviewService.importElectricImportOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载机电消费品进口概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ElectricImportOverview> util = new ExcelUtil<ElectricImportOverview>(ElectricImportOverview.class);
        util.importTemplateExcel(response, "机电消费品进口概况数据");
    }

    /**
     * 获取机电消费品进口概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('electric:importOverview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(electricImportOverviewService.selectElectricImportOverviewById(id));
    }

    /**
     * 新增机电消费品进口概况
     */
    @PreAuthorize("@ss.hasPermi('electric:importOverview:add')")
    @Log(title = "机电消费品进口概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ElectricImportOverview electricImportOverview) {
        return toAjax(electricImportOverviewService.insertElectricImportOverview(electricImportOverview));
    }

    /**
     * 修改机电消费品进口概况
     */
    @PreAuthorize("@ss.hasPermi('electric:importOverview:edit')")
    @Log(title = "机电消费品进口概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ElectricImportOverview electricImportOverview) {
        return toAjax(electricImportOverviewService.updateElectricImportOverview(electricImportOverview));
    }

    /**
     * 删除机电消费品进口概况
     */
    @PreAuthorize("@ss.hasPermi('electric:importOverview:remove')")
    @Log(title = "机电消费品进口概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(electricImportOverviewService.deleteElectricImportOverviewByIds(ids));
    }
}
