package com.qianjing.project.electric.mapper;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricImportCompany;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要进口企业Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface ElectricImportCompanyMapper {

    ElectricImportCompany selectElectricImportCompanyById(Long id);

    List<ElectricImportCompany> selectElectricImportCompanyList(ElectricImportCompany electricImportCompany);

    int insertElectricImportCompany(ElectricImportCompany electricImportCompany);

    int updateElectricImportCompany(ElectricImportCompany electricImportCompany);

    int deleteElectricImportCompanyById(Long id);

    int deleteElectricImportCompanyByIds(Long[] ids);
}
