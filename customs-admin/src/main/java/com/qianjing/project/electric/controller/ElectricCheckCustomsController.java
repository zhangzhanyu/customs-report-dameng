package com.qianjing.project.electric.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.electric.domain.ElectricCheckCustoms;
import com.qianjing.project.electric.service.IElectricCheckCustomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 机电消费品主要查验关区Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/electric/checkCustoms")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricCheckCustomsController extends BaseController {

    private final IElectricCheckCustomsService electricCheckCustomsService;

    /**
     * 查询机电消费品主要查验关区列表
     */
//    @PreAuthorize("@ss.hasPermi('electric:checkCustoms:list')")
    @GetMapping("/list")
    public TableDataInfo list(ElectricCheckCustoms electricCheckCustoms) {
        startPage();
        List<ElectricCheckCustoms> list = electricCheckCustomsService.selectElectricCheckCustomsList(electricCheckCustoms);
        return getDataTable(list);
    }

    /**
     * 导出机电消费品主要查验关区列表
     */
    @PreAuthorize("@ss.hasPermi('electric:checkCustoms:export')")
    @Log(title = "机电消费品主要查验关区", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ElectricCheckCustoms electricCheckCustoms) {
        List<ElectricCheckCustoms> list = electricCheckCustomsService.selectElectricCheckCustomsList(electricCheckCustoms);
        ExcelUtil<ElectricCheckCustoms> util = new ExcelUtil<ElectricCheckCustoms>(ElectricCheckCustoms.class);
        return util.exportExcel(list, "机电消费品主要查验关区数据");
    }

    /**
     * 导入机电消费品主要查验关区列表
     */
    @Log(title = "机电消费品主要查验关区", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('electric:checkCustoms:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ElectricCheckCustoms> util = new ExcelUtil<ElectricCheckCustoms>(ElectricCheckCustoms.class);
        List<ElectricCheckCustoms> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = electricCheckCustomsService.importElectricCheckCustoms(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载机电消费品主要查验关区导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ElectricCheckCustoms> util = new ExcelUtil<ElectricCheckCustoms>(ElectricCheckCustoms.class);
        util.importTemplateExcel(response, "机电消费品主要查验关区数据");
    }

    /**
     * 获取机电消费品主要查验关区详细信息
     */
    @PreAuthorize("@ss.hasPermi('electric:checkCustoms:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(electricCheckCustomsService.selectElectricCheckCustomsById(id));
    }

    /**
     * 新增机电消费品主要查验关区
     */
    @PreAuthorize("@ss.hasPermi('electric:checkCustoms:add')")
    @Log(title = "机电消费品主要查验关区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ElectricCheckCustoms electricCheckCustoms) {
        return toAjax(electricCheckCustomsService.insertElectricCheckCustoms(electricCheckCustoms));
    }

    /**
     * 修改机电消费品主要查验关区
     */
    @PreAuthorize("@ss.hasPermi('electric:checkCustoms:edit')")
    @Log(title = "机电消费品主要查验关区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ElectricCheckCustoms electricCheckCustoms) {
        return toAjax(electricCheckCustomsService.updateElectricCheckCustoms(electricCheckCustoms));
    }

    /**
     * 删除机电消费品主要查验关区
     */
    @PreAuthorize("@ss.hasPermi('electric:checkCustoms:remove')")
    @Log(title = "机电消费品主要查验关区", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(electricCheckCustomsService.deleteElectricCheckCustomsByIds(ids));
    }
}
