package com.qianjing.project.electric.service;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricCheckCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要查验关区Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IElectricCheckCustomsService 
{
    /**
     * 查询机电消费品主要查验关区
     * 
     * @param id 机电消费品主要查验关区主键
     * @return 机电消费品主要查验关区
     */
    public ElectricCheckCustoms selectElectricCheckCustomsById(Long id);

    /**
     * 查询机电消费品主要查验关区列表
     * 
     * @param electricCheckCustoms 机电消费品主要查验关区
     * @return 机电消费品主要查验关区集合
     */
    public List<ElectricCheckCustoms> selectElectricCheckCustomsList(ElectricCheckCustoms electricCheckCustoms);

    /**
     * 新增机电消费品主要查验关区
     * 
     * @param electricCheckCustoms 机电消费品主要查验关区
     * @return 结果
     */
    public int insertElectricCheckCustoms(ElectricCheckCustoms electricCheckCustoms);

    /**
     * 修改机电消费品主要查验关区
     * 
     * @param electricCheckCustoms 机电消费品主要查验关区
     * @return 结果
     */
    public int updateElectricCheckCustoms(ElectricCheckCustoms electricCheckCustoms);

    /**
     * 批量删除机电消费品主要查验关区
     * 
     * @param ids 需要删除的机电消费品主要查验关区主键集合
     * @return 结果
     */
    public int deleteElectricCheckCustomsByIds(Long[] ids);

    /**
     * 删除机电消费品主要查验关区信息
     * 
     * @param id 机电消费品主要查验关区主键
     * @return 结果
     */
    public int deleteElectricCheckCustomsById(Long id);

    /**
     * 导入机电消费品主要查验关区信息
     *
     * @param electricCheckCustomsList 机电消费品主要查验关区信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importElectricCheckCustoms(List<ElectricCheckCustoms> electricCheckCustomsList, Boolean isUpdateSupport, String operName);
}
