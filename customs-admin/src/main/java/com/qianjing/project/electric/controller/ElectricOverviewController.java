package com.qianjing.project.electric.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.electric.domain.ElectricOverview;
import com.qianjing.project.electric.service.IElectricOverviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 机电消费品主要数量Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/electric/overview")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricOverviewController extends BaseController {

    private final IElectricOverviewService electricOverviewService;

    /**
     * 查询机电消费品主要数量列表
     */
//    @PreAuthorize("@ss.hasPermi('electric:overview:list')")
    @GetMapping("/list")
    public TableDataInfo list(ElectricOverview electricOverview) {
        startPage();
        List<ElectricOverview> list = electricOverviewService.selectElectricOverviewList(electricOverview);
        return getDataTable(list);
    }

    /**
     * 导出机电消费品主要数量列表
     */
    @PreAuthorize("@ss.hasPermi('electric:overview:export')")
    @Log(title = "机电消费品主要数量", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ElectricOverview electricOverview) {
        List<ElectricOverview> list = electricOverviewService.selectElectricOverviewList(electricOverview);
        ExcelUtil<ElectricOverview> util = new ExcelUtil<ElectricOverview>(ElectricOverview.class);
        return util.exportExcel(list, "机电消费品主要数量数据");
    }

    /**
     * 导入机电消费品主要数量列表
     */
    @Log(title = "机电消费品主要数量", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('electric:overview:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ElectricOverview> util = new ExcelUtil<ElectricOverview>(ElectricOverview.class);
        List<ElectricOverview> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = electricOverviewService.importElectricOverview(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载机电消费品主要数量导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ElectricOverview> util = new ExcelUtil<ElectricOverview>(ElectricOverview.class);
        util.importTemplateExcel(response, "机电消费品主要数量数据");
    }

    /**
     * 获取机电消费品主要数量详细信息
     */
    @PreAuthorize("@ss.hasPermi('electric:overview:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(electricOverviewService.selectElectricOverviewById(id));
    }

    /**
     * 新增机电消费品主要数量
     */
    @PreAuthorize("@ss.hasPermi('electric:overview:add')")
    @Log(title = "机电消费品主要数量", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ElectricOverview electricOverview) {
        return toAjax(electricOverviewService.insertElectricOverview(electricOverview));
    }

    /**
     * 修改机电消费品主要数量
     */
    @PreAuthorize("@ss.hasPermi('electric:overview:edit')")
    @Log(title = "机电消费品主要数量", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ElectricOverview electricOverview) {
        return toAjax(electricOverviewService.updateElectricOverview(electricOverview));
    }

    /**
     * 删除机电消费品主要数量
     */
    @PreAuthorize("@ss.hasPermi('electric:overview:remove')")
    @Log(title = "机电消费品主要数量", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(electricOverviewService.deleteElectricOverviewByIds(ids));
    }
}
