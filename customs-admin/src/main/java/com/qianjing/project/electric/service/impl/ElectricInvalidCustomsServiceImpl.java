package com.qianjing.project.electric.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.electric.mapper.ElectricInvalidCustomsMapper;
import com.qianjing.project.electric.domain.ElectricInvalidCustoms;
import com.qianjing.project.electric.service.IElectricInvalidCustomsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要关区不合格Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricInvalidCustomsServiceImpl implements IElectricInvalidCustomsService {

    private static final Logger log = LoggerFactory.getLogger(ElectricInvalidCustomsServiceImpl.class);

    private final ElectricInvalidCustomsMapper electricInvalidCustomsMapper;

    /**
     * 查询机电消费品主要关区不合格
     * 
     * @param id 机电消费品主要关区不合格主键
     * @return 机电消费品主要关区不合格
     */
    @Override
    public ElectricInvalidCustoms selectElectricInvalidCustomsById(Long id) {
        return electricInvalidCustomsMapper.selectElectricInvalidCustomsById(id);
    }

    /**
     * 查询机电消费品主要关区不合格列表
     * 
     * @param electricInvalidCustoms 机电消费品主要关区不合格
     * @return 机电消费品主要关区不合格
     */
    @Override
    public List<ElectricInvalidCustoms> selectElectricInvalidCustomsList(ElectricInvalidCustoms electricInvalidCustoms) {
        return electricInvalidCustomsMapper.selectElectricInvalidCustomsList(electricInvalidCustoms);
    }

    /**
     * 新增机电消费品主要关区不合格
     * 
     * @param electricInvalidCustoms 机电消费品主要关区不合格
     * @return 结果
     */
    @Override
    public int insertElectricInvalidCustoms(ElectricInvalidCustoms electricInvalidCustoms) {
        electricInvalidCustoms.setCreateTime(DateUtils.getNowDate());
        return electricInvalidCustomsMapper.insertElectricInvalidCustoms(electricInvalidCustoms);
    }

    /**
     * 修改机电消费品主要关区不合格
     * 
     * @param electricInvalidCustoms 机电消费品主要关区不合格
     * @return 结果
     */
    @Override
    public int updateElectricInvalidCustoms(ElectricInvalidCustoms electricInvalidCustoms) {
        electricInvalidCustoms.setUpdateTime(DateUtils.getNowDate());
        return electricInvalidCustomsMapper.updateElectricInvalidCustoms(electricInvalidCustoms);
    }

    /**
     * 批量删除机电消费品主要关区不合格
     * 
     * @param ids 需要删除的机电消费品主要关区不合格主键
     * @return 结果
     */
    @Override
    public int deleteElectricInvalidCustomsByIds(Long[] ids) {
        return electricInvalidCustomsMapper.deleteElectricInvalidCustomsByIds(ids);
    }

    /**
     * 删除机电消费品主要关区不合格信息
     * 
     * @param id 机电消费品主要关区不合格主键
     * @return 结果
     */
    @Override
    public int deleteElectricInvalidCustomsById(Long id) {
        return electricInvalidCustomsMapper.deleteElectricInvalidCustomsById(id);
    }

    /**
     * 导入机电消费品主要关区不合格数据
     *
     * @param electricInvalidCustomsList electricInvalidCustomsList 机电消费品主要关区不合格信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importElectricInvalidCustoms(List<ElectricInvalidCustoms> electricInvalidCustomsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(electricInvalidCustomsList) || electricInvalidCustomsList.size() == 0) {
            throw new ServiceException("导入机电消费品主要关区不合格数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ElectricInvalidCustoms electricInvalidCustoms : electricInvalidCustomsList){
            try {
                // 验证是否存在
                if (true) {
                    electricInvalidCustoms.setCreateBy(operName);
                    this.insertElectricInvalidCustoms(electricInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    electricInvalidCustoms.setUpdateBy(operName);
                    this.updateElectricInvalidCustoms(electricInvalidCustoms);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
