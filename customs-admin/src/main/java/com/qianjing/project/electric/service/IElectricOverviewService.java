package com.qianjing.project.electric.service;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要数量Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IElectricOverviewService 
{
    /**
     * 查询机电消费品主要数量
     * 
     * @param id 机电消费品主要数量主键
     * @return 机电消费品主要数量
     */
    public ElectricOverview selectElectricOverviewById(Long id);

    /**
     * 查询机电消费品主要数量列表
     * 
     * @param electricOverview 机电消费品主要数量
     * @return 机电消费品主要数量集合
     */
    public List<ElectricOverview> selectElectricOverviewList(ElectricOverview electricOverview);

    /**
     * 新增机电消费品主要数量
     * 
     * @param electricOverview 机电消费品主要数量
     * @return 结果
     */
    public int insertElectricOverview(ElectricOverview electricOverview);

    /**
     * 修改机电消费品主要数量
     * 
     * @param electricOverview 机电消费品主要数量
     * @return 结果
     */
    public int updateElectricOverview(ElectricOverview electricOverview);

    /**
     * 批量删除机电消费品主要数量
     * 
     * @param ids 需要删除的机电消费品主要数量主键集合
     * @return 结果
     */
    public int deleteElectricOverviewByIds(Long[] ids);

    /**
     * 删除机电消费品主要数量信息
     * 
     * @param id 机电消费品主要数量主键
     * @return 结果
     */
    public int deleteElectricOverviewById(Long id);

    /**
     * 导入机电消费品主要数量信息
     *
     * @param electricOverviewList 机电消费品主要数量信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importElectricOverview(List<ElectricOverview> electricOverviewList, Boolean isUpdateSupport, String operName);
}
