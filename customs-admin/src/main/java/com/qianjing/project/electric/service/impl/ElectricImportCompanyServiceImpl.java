package com.qianjing.project.electric.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.electric.mapper.ElectricImportCompanyMapper;
import com.qianjing.project.electric.domain.ElectricImportCompany;
import com.qianjing.project.electric.service.IElectricImportCompanyService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要进口企业Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricImportCompanyServiceImpl implements IElectricImportCompanyService {

    private static final Logger log = LoggerFactory.getLogger(ElectricImportCompanyServiceImpl.class);

    private final ElectricImportCompanyMapper electricImportCompanyMapper;

    /**
     * 查询机电消费品主要进口企业
     * 
     * @param id 机电消费品主要进口企业主键
     * @return 机电消费品主要进口企业
     */
    @Override
    public ElectricImportCompany selectElectricImportCompanyById(Long id) {
        return electricImportCompanyMapper.selectElectricImportCompanyById(id);
    }

    /**
     * 查询机电消费品主要进口企业列表
     * 
     * @param electricImportCompany 机电消费品主要进口企业
     * @return 机电消费品主要进口企业
     */
    @Override
    public List<ElectricImportCompany> selectElectricImportCompanyList(ElectricImportCompany electricImportCompany) {
        return electricImportCompanyMapper.selectElectricImportCompanyList(electricImportCompany);
    }

    /**
     * 新增机电消费品主要进口企业
     * 
     * @param electricImportCompany 机电消费品主要进口企业
     * @return 结果
     */
    @Override
    public int insertElectricImportCompany(ElectricImportCompany electricImportCompany) {
        electricImportCompany.setCreateTime(DateUtils.getNowDate());
        return electricImportCompanyMapper.insertElectricImportCompany(electricImportCompany);
    }

    /**
     * 修改机电消费品主要进口企业
     * 
     * @param electricImportCompany 机电消费品主要进口企业
     * @return 结果
     */
    @Override
    public int updateElectricImportCompany(ElectricImportCompany electricImportCompany) {
        electricImportCompany.setUpdateTime(DateUtils.getNowDate());
        return electricImportCompanyMapper.updateElectricImportCompany(electricImportCompany);
    }

    /**
     * 批量删除机电消费品主要进口企业
     * 
     * @param ids 需要删除的机电消费品主要进口企业主键
     * @return 结果
     */
    @Override
    public int deleteElectricImportCompanyByIds(Long[] ids) {
        return electricImportCompanyMapper.deleteElectricImportCompanyByIds(ids);
    }

    /**
     * 删除机电消费品主要进口企业信息
     * 
     * @param id 机电消费品主要进口企业主键
     * @return 结果
     */
    @Override
    public int deleteElectricImportCompanyById(Long id) {
        return electricImportCompanyMapper.deleteElectricImportCompanyById(id);
    }

    /**
     * 导入机电消费品主要进口企业数据
     *
     * @param electricImportCompanyList electricImportCompanyList 机电消费品主要进口企业信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importElectricImportCompany(List<ElectricImportCompany> electricImportCompanyList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(electricImportCompanyList) || electricImportCompanyList.size() == 0) {
            throw new ServiceException("导入机电消费品主要进口企业数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ElectricImportCompany electricImportCompany : electricImportCompanyList){
            try {
                // 验证是否存在
                if (true) {
                    electricImportCompany.setCreateBy(operName);
                    this.insertElectricImportCompany(electricImportCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    electricImportCompany.setUpdateBy(operName);
                    this.updateElectricImportCompany(electricImportCompany);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
