package com.qianjing.project.electric.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.electric.domain.ElectricImportCompany;
import com.qianjing.project.electric.service.IElectricImportCompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 机电消费品主要进口企业Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/electric/company")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricImportCompanyController extends BaseController {

    private final IElectricImportCompanyService electricImportCompanyService;

    /**
     * 查询机电消费品主要进口企业列表
     */
//    @PreAuthorize("@ss.hasPermi('electric:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(ElectricImportCompany electricImportCompany) {
        startPage();
        List<ElectricImportCompany> list = electricImportCompanyService.selectElectricImportCompanyList(electricImportCompany);
        return getDataTable(list);
    }

    /**
     * 导出机电消费品主要进口企业列表
     */
    @PreAuthorize("@ss.hasPermi('electric:company:export')")
    @Log(title = "机电消费品主要进口企业", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ElectricImportCompany electricImportCompany) {
        List<ElectricImportCompany> list = electricImportCompanyService.selectElectricImportCompanyList(electricImportCompany);
        ExcelUtil<ElectricImportCompany> util = new ExcelUtil<ElectricImportCompany>(ElectricImportCompany.class);
        return util.exportExcel(list, "机电消费品主要进口企业数据");
    }

    /**
     * 导入机电消费品主要进口企业列表
     */
    @Log(title = "机电消费品主要进口企业", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('electric:company:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ElectricImportCompany> util = new ExcelUtil<ElectricImportCompany>(ElectricImportCompany.class);
        List<ElectricImportCompany> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = electricImportCompanyService.importElectricImportCompany(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载机电消费品主要进口企业导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ElectricImportCompany> util = new ExcelUtil<ElectricImportCompany>(ElectricImportCompany.class);
        util.importTemplateExcel(response, "机电消费品主要进口企业数据");
    }

    /**
     * 获取机电消费品主要进口企业详细信息
     */
    @PreAuthorize("@ss.hasPermi('electric:company:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(electricImportCompanyService.selectElectricImportCompanyById(id));
    }

    /**
     * 新增机电消费品主要进口企业
     */
    @PreAuthorize("@ss.hasPermi('electric:company:add')")
    @Log(title = "机电消费品主要进口企业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ElectricImportCompany electricImportCompany) {
        return toAjax(electricImportCompanyService.insertElectricImportCompany(electricImportCompany));
    }

    /**
     * 修改机电消费品主要进口企业
     */
    @PreAuthorize("@ss.hasPermi('electric:company:edit')")
    @Log(title = "机电消费品主要进口企业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ElectricImportCompany electricImportCompany) {
        return toAjax(electricImportCompanyService.updateElectricImportCompany(electricImportCompany));
    }

    /**
     * 删除机电消费品主要进口企业
     */
    @PreAuthorize("@ss.hasPermi('electric:company:remove')")
    @Log(title = "机电消费品主要进口企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(electricImportCompanyService.deleteElectricImportCompanyByIds(ids));
    }
}
