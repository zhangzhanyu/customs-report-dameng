package com.qianjing.project.electric.service;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricInvalidCustoms;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要关区不合格Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IElectricInvalidCustomsService 
{
    /**
     * 查询机电消费品主要关区不合格
     * 
     * @param id 机电消费品主要关区不合格主键
     * @return 机电消费品主要关区不合格
     */
    public ElectricInvalidCustoms selectElectricInvalidCustomsById(Long id);

    /**
     * 查询机电消费品主要关区不合格列表
     * 
     * @param electricInvalidCustoms 机电消费品主要关区不合格
     * @return 机电消费品主要关区不合格集合
     */
    public List<ElectricInvalidCustoms> selectElectricInvalidCustomsList(ElectricInvalidCustoms electricInvalidCustoms);

    /**
     * 新增机电消费品主要关区不合格
     * 
     * @param electricInvalidCustoms 机电消费品主要关区不合格
     * @return 结果
     */
    public int insertElectricInvalidCustoms(ElectricInvalidCustoms electricInvalidCustoms);

    /**
     * 修改机电消费品主要关区不合格
     * 
     * @param electricInvalidCustoms 机电消费品主要关区不合格
     * @return 结果
     */
    public int updateElectricInvalidCustoms(ElectricInvalidCustoms electricInvalidCustoms);

    /**
     * 批量删除机电消费品主要关区不合格
     * 
     * @param ids 需要删除的机电消费品主要关区不合格主键集合
     * @return 结果
     */
    public int deleteElectricInvalidCustomsByIds(Long[] ids);

    /**
     * 删除机电消费品主要关区不合格信息
     * 
     * @param id 机电消费品主要关区不合格主键
     * @return 结果
     */
    public int deleteElectricInvalidCustomsById(Long id);

    /**
     * 导入机电消费品主要关区不合格信息
     *
     * @param electricInvalidCustomsList 机电消费品主要关区不合格信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importElectricInvalidCustoms(List<ElectricInvalidCustoms> electricInvalidCustomsList, Boolean isUpdateSupport, String operName);
}
