package com.qianjing.project.electric.service;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricSourceCountry;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要来源国Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IElectricSourceCountryService 
{
    /**
     * 查询机电消费品主要来源国
     * 
     * @param id 机电消费品主要来源国主键
     * @return 机电消费品主要来源国
     */
    public ElectricSourceCountry selectElectricSourceCountryById(Long id);

    /**
     * 查询机电消费品主要来源国列表
     * 
     * @param electricSourceCountry 机电消费品主要来源国
     * @return 机电消费品主要来源国集合
     */
    public List<ElectricSourceCountry> selectElectricSourceCountryList(ElectricSourceCountry electricSourceCountry);

    /**
     * 新增机电消费品主要来源国
     * 
     * @param electricSourceCountry 机电消费品主要来源国
     * @return 结果
     */
    public int insertElectricSourceCountry(ElectricSourceCountry electricSourceCountry);

    /**
     * 修改机电消费品主要来源国
     * 
     * @param electricSourceCountry 机电消费品主要来源国
     * @return 结果
     */
    public int updateElectricSourceCountry(ElectricSourceCountry electricSourceCountry);

    /**
     * 批量删除机电消费品主要来源国
     * 
     * @param ids 需要删除的机电消费品主要来源国主键集合
     * @return 结果
     */
    public int deleteElectricSourceCountryByIds(Long[] ids);

    /**
     * 删除机电消费品主要来源国信息
     * 
     * @param id 机电消费品主要来源国主键
     * @return 结果
     */
    public int deleteElectricSourceCountryById(Long id);

    /**
     * 导入机电消费品主要来源国信息
     *
     * @param electricSourceCountryList 机电消费品主要来源国信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importElectricSourceCountry(List<ElectricSourceCountry> electricSourceCountryList, Boolean isUpdateSupport, String operName);
}
