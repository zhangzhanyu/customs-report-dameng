package com.qianjing.project.electric.service;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricImportCompany;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要进口企业Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IElectricImportCompanyService 
{
    /**
     * 查询机电消费品主要进口企业
     * 
     * @param id 机电消费品主要进口企业主键
     * @return 机电消费品主要进口企业
     */
    public ElectricImportCompany selectElectricImportCompanyById(Long id);

    /**
     * 查询机电消费品主要进口企业列表
     * 
     * @param electricImportCompany 机电消费品主要进口企业
     * @return 机电消费品主要进口企业集合
     */
    public List<ElectricImportCompany> selectElectricImportCompanyList(ElectricImportCompany electricImportCompany);

    /**
     * 新增机电消费品主要进口企业
     * 
     * @param electricImportCompany 机电消费品主要进口企业
     * @return 结果
     */
    public int insertElectricImportCompany(ElectricImportCompany electricImportCompany);

    /**
     * 修改机电消费品主要进口企业
     * 
     * @param electricImportCompany 机电消费品主要进口企业
     * @return 结果
     */
    public int updateElectricImportCompany(ElectricImportCompany electricImportCompany);

    /**
     * 批量删除机电消费品主要进口企业
     * 
     * @param ids 需要删除的机电消费品主要进口企业主键集合
     * @return 结果
     */
    public int deleteElectricImportCompanyByIds(Long[] ids);

    /**
     * 删除机电消费品主要进口企业信息
     * 
     * @param id 机电消费品主要进口企业主键
     * @return 结果
     */
    public int deleteElectricImportCompanyById(Long id);

    /**
     * 导入机电消费品主要进口企业信息
     *
     * @param electricImportCompanyList 机电消费品主要进口企业信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importElectricImportCompany(List<ElectricImportCompany> electricImportCompanyList, Boolean isUpdateSupport, String operName);
}
