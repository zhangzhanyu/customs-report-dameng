package com.qianjing.project.electric.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品主要来源国对象 electric_source_country
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public class ElectricSourceCountry extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private Long sortNum;

    /** 原产国/名称 */
    @Excel(name = "原产国/名称")
    private String sourceCountry;

    /** 货物批 */
    @Excel(name = "货物批")
    private Long goodsBatch;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setSortNum(Long sortNum){
        this.sortNum = sortNum;
    }

    public Long getSortNum(){
        return sortNum;
    }
    public void setSourceCountry(String sourceCountry){
        this.sourceCountry = sourceCountry;
    }

    public String getSourceCountry(){
        return sourceCountry;
    }
    public void setGoodsBatch(Long goodsBatch){
        this.goodsBatch = goodsBatch;
    }

    public Long getGoodsBatch(){
        return goodsBatch;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sortNum", getSortNum())
            .append("sourceCountry", getSourceCountry())
            .append("goodsBatch", getGoodsBatch())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
