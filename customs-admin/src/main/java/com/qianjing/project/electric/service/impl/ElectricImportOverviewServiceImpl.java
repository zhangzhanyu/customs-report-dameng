package com.qianjing.project.electric.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.electric.mapper.ElectricImportOverviewMapper;
import com.qianjing.project.electric.domain.ElectricImportOverview;
import com.qianjing.project.electric.service.IElectricImportOverviewService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品进口概况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricImportOverviewServiceImpl implements IElectricImportOverviewService {

    private static final Logger log = LoggerFactory.getLogger(ElectricImportOverviewServiceImpl.class);

    private final ElectricImportOverviewMapper electricImportOverviewMapper;

    /**
     * 查询机电消费品进口概况
     * 
     * @param id 机电消费品进口概况主键
     * @return 机电消费品进口概况
     */
    @Override
    public ElectricImportOverview selectElectricImportOverviewById(Long id) {
        return electricImportOverviewMapper.selectElectricImportOverviewById(id);
    }

    /**
     * 查询机电消费品进口概况列表
     * 
     * @param electricImportOverview 机电消费品进口概况
     * @return 机电消费品进口概况
     */
    @Override
    public List<ElectricImportOverview> selectElectricImportOverviewList(ElectricImportOverview electricImportOverview) {
        return electricImportOverviewMapper.selectElectricImportOverviewList(electricImportOverview);
    }

    /**
     * 新增机电消费品进口概况
     * 
     * @param electricImportOverview 机电消费品进口概况
     * @return 结果
     */
    @Override
    public int insertElectricImportOverview(ElectricImportOverview electricImportOverview) {
        electricImportOverview.setCreateTime(DateUtils.getNowDate());
        return electricImportOverviewMapper.insertElectricImportOverview(electricImportOverview);
    }

    /**
     * 修改机电消费品进口概况
     * 
     * @param electricImportOverview 机电消费品进口概况
     * @return 结果
     */
    @Override
    public int updateElectricImportOverview(ElectricImportOverview electricImportOverview) {
        electricImportOverview.setUpdateTime(DateUtils.getNowDate());
        return electricImportOverviewMapper.updateElectricImportOverview(electricImportOverview);
    }

    /**
     * 批量删除机电消费品进口概况
     * 
     * @param ids 需要删除的机电消费品进口概况主键
     * @return 结果
     */
    @Override
    public int deleteElectricImportOverviewByIds(Long[] ids) {
        return electricImportOverviewMapper.deleteElectricImportOverviewByIds(ids);
    }

    /**
     * 删除机电消费品进口概况信息
     * 
     * @param id 机电消费品进口概况主键
     * @return 结果
     */
    @Override
    public int deleteElectricImportOverviewById(Long id) {
        return electricImportOverviewMapper.deleteElectricImportOverviewById(id);
    }

    /**
     * 导入机电消费品进口概况数据
     *
     * @param electricImportOverviewList electricImportOverviewList 机电消费品进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importElectricImportOverview(List<ElectricImportOverview> electricImportOverviewList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(electricImportOverviewList) || electricImportOverviewList.size() == 0) {
            throw new ServiceException("导入机电消费品进口概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ElectricImportOverview electricImportOverview : electricImportOverviewList){
            try {
                // 验证是否存在
                if (true) {
                    electricImportOverview.setCreateBy(operName);
                    this.insertElectricImportOverview(electricImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    electricImportOverview.setUpdateBy(operName);
                    this.updateElectricImportOverview(electricImportOverview);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
