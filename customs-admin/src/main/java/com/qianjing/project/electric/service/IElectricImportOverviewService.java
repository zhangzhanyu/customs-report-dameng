package com.qianjing.project.electric.service;

import java.util.List;
import com.qianjing.project.electric.domain.ElectricImportOverview;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 机电消费品进口概况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IElectricImportOverviewService 
{
    /**
     * 查询机电消费品进口概况
     * 
     * @param id 机电消费品进口概况主键
     * @return 机电消费品进口概况
     */
    public ElectricImportOverview selectElectricImportOverviewById(Long id);

    /**
     * 查询机电消费品进口概况列表
     * 
     * @param electricImportOverview 机电消费品进口概况
     * @return 机电消费品进口概况集合
     */
    public List<ElectricImportOverview> selectElectricImportOverviewList(ElectricImportOverview electricImportOverview);

    /**
     * 新增机电消费品进口概况
     * 
     * @param electricImportOverview 机电消费品进口概况
     * @return 结果
     */
    public int insertElectricImportOverview(ElectricImportOverview electricImportOverview);

    /**
     * 修改机电消费品进口概况
     * 
     * @param electricImportOverview 机电消费品进口概况
     * @return 结果
     */
    public int updateElectricImportOverview(ElectricImportOverview electricImportOverview);

    /**
     * 批量删除机电消费品进口概况
     * 
     * @param ids 需要删除的机电消费品进口概况主键集合
     * @return 结果
     */
    public int deleteElectricImportOverviewByIds(Long[] ids);

    /**
     * 删除机电消费品进口概况信息
     * 
     * @param id 机电消费品进口概况主键
     * @return 结果
     */
    public int deleteElectricImportOverviewById(Long id);

    /**
     * 导入机电消费品进口概况信息
     *
     * @param electricImportOverviewList 机电消费品进口概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importElectricImportOverview(List<ElectricImportOverview> electricImportOverviewList, Boolean isUpdateSupport, String operName);
}
