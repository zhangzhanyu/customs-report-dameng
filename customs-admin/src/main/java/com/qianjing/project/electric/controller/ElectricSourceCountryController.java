package com.qianjing.project.electric.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.electric.domain.ElectricSourceCountry;
import com.qianjing.project.electric.service.IElectricSourceCountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 机电消费品主要来源国Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/electric/country")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElectricSourceCountryController extends BaseController {

    private final IElectricSourceCountryService electricSourceCountryService;

    /**
     * 查询机电消费品主要来源国列表
     */
//    @PreAuthorize("@ss.hasPermi('electric:country:list')")
    @GetMapping("/list")
    public TableDataInfo list(ElectricSourceCountry electricSourceCountry) {
        startPage();
        List<ElectricSourceCountry> list = electricSourceCountryService.selectElectricSourceCountryList(electricSourceCountry);
        return getDataTable(list);
    }

    /**
     * 导出机电消费品主要来源国列表
     */
    @PreAuthorize("@ss.hasPermi('electric:country:export')")
    @Log(title = "机电消费品主要来源国", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ElectricSourceCountry electricSourceCountry) {
        List<ElectricSourceCountry> list = electricSourceCountryService.selectElectricSourceCountryList(electricSourceCountry);
        ExcelUtil<ElectricSourceCountry> util = new ExcelUtil<ElectricSourceCountry>(ElectricSourceCountry.class);
        return util.exportExcel(list, "机电消费品主要来源国数据");
    }

    /**
     * 导入机电消费品主要来源国列表
     */
    @Log(title = "机电消费品主要来源国", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('electric:country:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<ElectricSourceCountry> util = new ExcelUtil<ElectricSourceCountry>(ElectricSourceCountry.class);
        List<ElectricSourceCountry> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = electricSourceCountryService.importElectricSourceCountry(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载机电消费品主要来源国导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<ElectricSourceCountry> util = new ExcelUtil<ElectricSourceCountry>(ElectricSourceCountry.class);
        util.importTemplateExcel(response, "机电消费品主要来源国数据");
    }

    /**
     * 获取机电消费品主要来源国详细信息
     */
    @PreAuthorize("@ss.hasPermi('electric:country:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(electricSourceCountryService.selectElectricSourceCountryById(id));
    }

    /**
     * 新增机电消费品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('electric:country:add')")
    @Log(title = "机电消费品主要来源国", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ElectricSourceCountry electricSourceCountry) {
        return toAjax(electricSourceCountryService.insertElectricSourceCountry(electricSourceCountry));
    }

    /**
     * 修改机电消费品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('electric:country:edit')")
    @Log(title = "机电消费品主要来源国", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ElectricSourceCountry electricSourceCountry) {
        return toAjax(electricSourceCountryService.updateElectricSourceCountry(electricSourceCountry));
    }

    /**
     * 删除机电消费品主要来源国
     */
    @PreAuthorize("@ss.hasPermi('electric:country:remove')")
    @Log(title = "机电消费品主要来源国", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(electricSourceCountryService.deleteElectricSourceCountryByIds(ids));
    }
}
