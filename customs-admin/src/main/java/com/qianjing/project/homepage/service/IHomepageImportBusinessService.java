package com.qianjing.project.homepage.service;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageImportBusiness;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要进口业务数据Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IHomepageImportBusinessService 
{
    /**
     * 查询主要进口业务数据
     * 
     * @param id 主要进口业务数据主键
     * @return 主要进口业务数据
     */
    public HomepageImportBusiness selectHomepageImportBusinessById(Long id);

    /**
     * 查询主要进口业务数据列表
     * 
     * @param homepageImportBusiness 主要进口业务数据
     * @return 主要进口业务数据集合
     */
    public List<HomepageImportBusiness> selectHomepageImportBusinessList(HomepageImportBusiness homepageImportBusiness);

    /**
     * 新增主要进口业务数据
     * 
     * @param homepageImportBusiness 主要进口业务数据
     * @return 结果
     */
    public int insertHomepageImportBusiness(HomepageImportBusiness homepageImportBusiness);

    /**
     * 修改主要进口业务数据
     * 
     * @param homepageImportBusiness 主要进口业务数据
     * @return 结果
     */
    public int updateHomepageImportBusiness(HomepageImportBusiness homepageImportBusiness);

    /**
     * 批量删除主要进口业务数据
     * 
     * @param ids 需要删除的主要进口业务数据主键集合
     * @return 结果
     */
    public int deleteHomepageImportBusinessByIds(Long[] ids);

    /**
     * 删除主要进口业务数据信息
     * 
     * @param id 主要进口业务数据主键
     * @return 结果
     */
    public int deleteHomepageImportBusinessById(Long id);

    /**
     * 导入主要进口业务数据信息
     *
     * @param homepageImportBusinessList 主要进口业务数据信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHomepageImportBusiness(List<HomepageImportBusiness> homepageImportBusinessList, Boolean isUpdateSupport, String operName);
}
