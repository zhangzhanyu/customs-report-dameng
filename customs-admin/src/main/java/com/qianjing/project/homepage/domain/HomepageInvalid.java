package com.qianjing.project.homepage.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 不合格情况对象 homepage_invalid
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public class HomepageInvalid extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String classification;

    /** 不合格报关单批 */
    @Excel(name = "不合格报关单批")
    private Long invalidEntryBatch;

    /** 不合格货物批 */
    @Excel(name = "不合格货物批")
    private Long invalidGoodsBatch;

    /** 不合格金额(万美元) */
    @Excel(name = "不合格金额(万美元)")
    private Long invalidUsdPrice;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setClassification(String classification){
        this.classification = classification;
    }

    public String getClassification(){
        return classification;
    }
    public void setInvalidEntryBatch(Long invalidEntryBatch){
        this.invalidEntryBatch = invalidEntryBatch;
    }

    public Long getInvalidEntryBatch(){
        return invalidEntryBatch;
    }
    public void setInvalidGoodsBatch(Long invalidGoodsBatch){
        this.invalidGoodsBatch = invalidGoodsBatch;
    }

    public Long getInvalidGoodsBatch(){
        return invalidGoodsBatch;
    }
    public void setInvalidUsdPrice(Long invalidUsdPrice){
        this.invalidUsdPrice = invalidUsdPrice;
    }

    public Long getInvalidUsdPrice(){
        return invalidUsdPrice;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("classification", getClassification())
                .append("invalidEntryBatch", getInvalidEntryBatch())
                .append("invalidGoodsBatch", getInvalidGoodsBatch())
                .append("invalidUsdPrice", getInvalidUsdPrice())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("yearMonth", getYearMonth())
                .toString();
    }
}
