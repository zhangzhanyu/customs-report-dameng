package com.qianjing.project.homepage.service;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageFocus;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 重点工作Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IHomepageFocusService 
{
    /**
     * 查询重点工作
     * 
     * @param id 重点工作主键
     * @return 重点工作
     */
    public HomepageFocus selectHomepageFocusById(Long id);

    /**
     * 查询重点工作列表
     * 
     * @param homepageFocus 重点工作
     * @return 重点工作集合
     */
    public List<HomepageFocus> selectHomepageFocusList(HomepageFocus homepageFocus);

    /**
     * 新增重点工作
     * 
     * @param homepageFocus 重点工作
     * @return 结果
     */
    public int insertHomepageFocus(HomepageFocus homepageFocus);

    /**
     * 修改重点工作
     * 
     * @param homepageFocus 重点工作
     * @return 结果
     */
    public int updateHomepageFocus(HomepageFocus homepageFocus);

    /**
     * 批量删除重点工作
     * 
     * @param ids 需要删除的重点工作主键集合
     * @return 结果
     */
    public int deleteHomepageFocusByIds(Long[] ids);

    /**
     * 删除重点工作信息
     * 
     * @param id 重点工作主键
     * @return 结果
     */
    public int deleteHomepageFocusById(Long id);

    /**
     * 导入重点工作信息
     *
     * @param homepageFocusList 重点工作信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHomepageFocus(List<HomepageFocus> homepageFocusList, Boolean isUpdateSupport, String operName);
}
