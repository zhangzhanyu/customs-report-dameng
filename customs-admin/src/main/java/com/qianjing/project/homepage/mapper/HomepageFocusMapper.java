package com.qianjing.project.homepage.mapper;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageFocus;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 重点工作Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface HomepageFocusMapper {

    HomepageFocus selectHomepageFocusById(Long id);

    List<HomepageFocus> selectHomepageFocusList(HomepageFocus homepageFocus);

    int insertHomepageFocus(HomepageFocus homepageFocus);

    int updateHomepageFocus(HomepageFocus homepageFocus);

    int deleteHomepageFocusById(Long id);

    int deleteHomepageFocusByIds(Long[] ids);
}
