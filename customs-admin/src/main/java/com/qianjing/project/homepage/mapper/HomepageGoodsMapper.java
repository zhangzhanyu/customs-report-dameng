package com.qianjing.project.homepage.mapper;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageGoods;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要商品情况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface HomepageGoodsMapper {

    HomepageGoods selectHomepageGoodsById(Long id);

    List<HomepageGoods> selectHomepageGoodsList(HomepageGoods homepageGoods);

    int insertHomepageGoods(HomepageGoods homepageGoods);

    int updateHomepageGoods(HomepageGoods homepageGoods);

    int deleteHomepageGoodsById(Long id);

    int deleteHomepageGoodsByIds(Long[] ids);
}
