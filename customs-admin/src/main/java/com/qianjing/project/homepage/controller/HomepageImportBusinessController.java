package com.qianjing.project.homepage.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.homepage.domain.HomepageImportBusiness;
import com.qianjing.project.homepage.service.IHomepageImportBusinessService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 主要进口业务数据Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/homepage/business")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageImportBusinessController extends BaseController {

    private final IHomepageImportBusinessService homepageImportBusinessService;

    /**
     * 查询主要进口业务数据列表
     */
//    @PreAuthorize("@ss.hasPermi('homepage:business:list')")
    @GetMapping("/list")
    public TableDataInfo list(HomepageImportBusiness homepageImportBusiness) {
        startPage();
        List<HomepageImportBusiness> list = homepageImportBusinessService.selectHomepageImportBusinessList(homepageImportBusiness);
        return getDataTable(list);
    }

    /**
     * 导出主要进口业务数据列表
     */
    @PreAuthorize("@ss.hasPermi('homepage:business:export')")
    @Log(title = "主要进口业务数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HomepageImportBusiness homepageImportBusiness) {
        List<HomepageImportBusiness> list = homepageImportBusinessService.selectHomepageImportBusinessList(homepageImportBusiness);
        ExcelUtil<HomepageImportBusiness> util = new ExcelUtil<HomepageImportBusiness>(HomepageImportBusiness.class);
        return util.exportExcel(list, "主要进口业务数据数据");
    }

    /**
     * 导入主要进口业务数据列表
     */
    @Log(title = "主要进口业务数据", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('homepage:business:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HomepageImportBusiness> util = new ExcelUtil<HomepageImportBusiness>(HomepageImportBusiness.class);
        List<HomepageImportBusiness> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = homepageImportBusinessService.importHomepageImportBusiness(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载主要进口业务数据导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HomepageImportBusiness> util = new ExcelUtil<HomepageImportBusiness>(HomepageImportBusiness.class);
        util.importTemplateExcel(response, "主要进口业务数据数据");
    }

    /**
     * 获取主要进口业务数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('homepage:business:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(homepageImportBusinessService.selectHomepageImportBusinessById(id));
    }

    /**
     * 新增主要进口业务数据
     */
    @PreAuthorize("@ss.hasPermi('homepage:business:add')")
    @Log(title = "主要进口业务数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HomepageImportBusiness homepageImportBusiness) {
        return toAjax(homepageImportBusinessService.insertHomepageImportBusiness(homepageImportBusiness));
    }

    /**
     * 修改主要进口业务数据
     */
    @PreAuthorize("@ss.hasPermi('homepage:business:edit')")
    @Log(title = "主要进口业务数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HomepageImportBusiness homepageImportBusiness) {
        return toAjax(homepageImportBusinessService.updateHomepageImportBusiness(homepageImportBusiness));
    }

    /**
     * 删除主要进口业务数据
     */
    @PreAuthorize("@ss.hasPermi('homepage:business:remove')")
    @Log(title = "主要进口业务数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(homepageImportBusinessService.deleteHomepageImportBusinessByIds(ids));
    }
}
