package com.qianjing.project.homepage.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.homepage.mapper.HomepageImportBusinessMapper;
import com.qianjing.project.homepage.domain.HomepageImportBusiness;
import com.qianjing.project.homepage.service.IHomepageImportBusinessService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要进口业务数据Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageImportBusinessServiceImpl implements IHomepageImportBusinessService {

    private static final Logger log = LoggerFactory.getLogger(HomepageImportBusinessServiceImpl.class);

    private final HomepageImportBusinessMapper homepageImportBusinessMapper;

    /**
     * 查询主要进口业务数据
     * 
     * @param id 主要进口业务数据主键
     * @return 主要进口业务数据
     */
    @Override
    public HomepageImportBusiness selectHomepageImportBusinessById(Long id) {
        return homepageImportBusinessMapper.selectHomepageImportBusinessById(id);
    }

    /**
     * 查询主要进口业务数据列表
     * 
     * @param homepageImportBusiness 主要进口业务数据
     * @return 主要进口业务数据
     */
    @Override
    public List<HomepageImportBusiness> selectHomepageImportBusinessList(HomepageImportBusiness homepageImportBusiness) {
        return homepageImportBusinessMapper.selectHomepageImportBusinessList(homepageImportBusiness);
    }

    /**
     * 新增主要进口业务数据
     * 
     * @param homepageImportBusiness 主要进口业务数据
     * @return 结果
     */
    @Override
    public int insertHomepageImportBusiness(HomepageImportBusiness homepageImportBusiness) {
        homepageImportBusiness.setCreateTime(DateUtils.getNowDate());
        return homepageImportBusinessMapper.insertHomepageImportBusiness(homepageImportBusiness);
    }

    /**
     * 修改主要进口业务数据
     * 
     * @param homepageImportBusiness 主要进口业务数据
     * @return 结果
     */
    @Override
    public int updateHomepageImportBusiness(HomepageImportBusiness homepageImportBusiness) {
        homepageImportBusiness.setUpdateTime(DateUtils.getNowDate());
        return homepageImportBusinessMapper.updateHomepageImportBusiness(homepageImportBusiness);
    }

    /**
     * 批量删除主要进口业务数据
     * 
     * @param ids 需要删除的主要进口业务数据主键
     * @return 结果
     */
    @Override
    public int deleteHomepageImportBusinessByIds(Long[] ids) {
        return homepageImportBusinessMapper.deleteHomepageImportBusinessByIds(ids);
    }

    /**
     * 删除主要进口业务数据信息
     * 
     * @param id 主要进口业务数据主键
     * @return 结果
     */
    @Override
    public int deleteHomepageImportBusinessById(Long id) {
        return homepageImportBusinessMapper.deleteHomepageImportBusinessById(id);
    }

    /**
     * 导入主要进口业务数据数据
     *
     * @param homepageImportBusinessList homepageImportBusinessList 主要进口业务数据信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHomepageImportBusiness(List<HomepageImportBusiness> homepageImportBusinessList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(homepageImportBusinessList) || homepageImportBusinessList.size() == 0) {
            throw new ServiceException("导入主要进口业务数据数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HomepageImportBusiness homepageImportBusiness : homepageImportBusinessList){
            try {
                // 验证是否存在
                if (true) {
                    homepageImportBusiness.setCreateBy(operName);
                    this.insertHomepageImportBusiness(homepageImportBusiness);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    homepageImportBusiness.setUpdateBy(operName);
                    this.updateHomepageImportBusiness(homepageImportBusiness);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
