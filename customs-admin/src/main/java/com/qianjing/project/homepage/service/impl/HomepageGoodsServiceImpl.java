package com.qianjing.project.homepage.service.impl;

import java.text.DecimalFormat;
import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.homepage.mapper.HomepageGoodsMapper;
import com.qianjing.project.homepage.domain.HomepageGoods;
import com.qianjing.project.homepage.service.IHomepageGoodsService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要商品情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageGoodsServiceImpl implements IHomepageGoodsService {

    private static final Logger log = LoggerFactory.getLogger(HomepageGoodsServiceImpl.class);

    private final HomepageGoodsMapper homepageGoodsMapper;

    /**
     * 查询主要商品情况
     * 
     * @param id 主要商品情况主键
     * @return 主要商品情况
     */
    @Override
    public HomepageGoods selectHomepageGoodsById(Long id) {
        return homepageGoodsMapper.selectHomepageGoodsById(id);
    }

    /**
     * 查询主要商品情况列表
     * 
     * @param homepageGoods 主要商品情况
     * @return 主要商品情况
     */
    @Override
    public List<HomepageGoods> selectHomepageGoodsList(HomepageGoods homepageGoods) {
        return homepageGoodsMapper.selectHomepageGoodsList(homepageGoods);
    }

    /**
     * 新增主要商品情况
     * 
     * @param homepageGoods 主要商品情况
     * @return 结果
     */
    @Override
    public int insertHomepageGoods(HomepageGoods homepageGoods) {
        homepageGoods.setCreateTime(DateUtils.getNowDate());
        return homepageGoodsMapper.insertHomepageGoods(homepageGoods);
    }

    /**
     * 修改主要商品情况
     * 
     * @param homepageGoods 主要商品情况
     * @return 结果
     */
    @Override
    public int updateHomepageGoods(HomepageGoods homepageGoods) {
        homepageGoods.setUpdateTime(DateUtils.getNowDate());
        return homepageGoodsMapper.updateHomepageGoods(homepageGoods);
    }

    /**
     * 批量删除主要商品情况
     * 
     * @param ids 需要删除的主要商品情况主键
     * @return 结果
     */
    @Override
    public int deleteHomepageGoodsByIds(Long[] ids) {
        return homepageGoodsMapper.deleteHomepageGoodsByIds(ids);
    }

    /**
     * 删除主要商品情况信息
     * 
     * @param id 主要商品情况主键
     * @return 结果
     */
    @Override
    public int deleteHomepageGoodsById(Long id) {
        return homepageGoodsMapper.deleteHomepageGoodsById(id);
    }

    /**
     * 导入主要商品情况数据
     *
     * @param homepageGoodsList homepageGoodsList 主要商品情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHomepageGoods(List<HomepageGoods> homepageGoodsList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(homepageGoodsList) || homepageGoodsList.size() == 0) {
            throw new ServiceException("导入主要商品情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HomepageGoods homepageGoods : homepageGoodsList){
            try {
                // 验证是否存在
                if (true) {
                    homepageGoods.setCreateBy(operName);
                    this.insertHomepageGoods(homepageGoods);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    homepageGoods.setUpdateBy(operName);
                    this.updateHomepageGoods(homepageGoods);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
