package com.qianjing.project.homepage.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要商品情况对象 homepage_goods
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public class HomepageGoods extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String classification;

    /** 进口报关单批 */
    @Excel(name = "进口报关单批")
    private BigDecimal importEntryBatch;

    /** 进口货物批 */
    @Excel(name = "进口货物批")
    private BigDecimal importGoodsBatch;

    /** 进口金额 */
    @Excel(name = "进口金额")
    private BigDecimal importUsdPrice;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setClassification(String classification){
        this.classification = classification;
    }

    public String getClassification(){
        return classification;
    }
    public void setImportEntryBatch(BigDecimal importEntryBatch){
        this.importEntryBatch = importEntryBatch;
    }

    public BigDecimal getImportEntryBatch(){
        return importEntryBatch;
    }
    public void setImportGoodsBatch(BigDecimal importGoodsBatch){
        this.importGoodsBatch = importGoodsBatch;
    }

    public BigDecimal getImportGoodsBatch(){
        return importGoodsBatch;
    }
    public void setImportUsdPrice(BigDecimal importUsdPrice){
        this.importUsdPrice = importUsdPrice;
    }

    public BigDecimal getImportUsdPrice(){
        return importUsdPrice;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("classification", getClassification())
            .append("importEntryBatch", getImportEntryBatch())
            .append("importGoodsBatch", getImportGoodsBatch())
            .append("importUsdPrice", getImportUsdPrice())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("yearMonth", getYearMonth())
            .toString();
    }
}
