package com.qianjing.project.homepage.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要进口业务数据对象 homepage_import_business
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public class HomepageImportBusiness extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private Long sortNum;

    /** 进口报关单批 */
    @Excel(name = "进口报关单批")
    private Long importEntryBatch;

    /** 进口货物批 */
    @Excel(name = "进口货物批")
    private Long importGoodsBatch;

    /** 进口金额(万美元) */
    @Excel(name = "进口金额(万美元)")
    private BigDecimal importUsdPrice;

    /** 查验报关单批次 */
    @Excel(name = "查验报关单批次")
    private Long checkBatch;

    /** 查验货物批 */
    @Excel(name = "查验货物批")
    private Long checkGoodsBatch;

    /** 查验金额(万美元) */
    @Excel(name = "查验金额(万美元)")
    private BigDecimal checkUsdPrice;

    /** 年月 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "年月", width = 30, dateFormat = "yyyy年MM月")
    private Date yearMonth;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setSortNum(Long sortNum){
        this.sortNum = sortNum;
    }

    public Long getSortNum(){
        return sortNum;
    }
    public void setImportEntryBatch(Long importEntryBatch){
        this.importEntryBatch = importEntryBatch;
    }

    public Long getImportEntryBatch(){
        return importEntryBatch;
    }
    public void setImportGoodsBatch(Long importGoodsBatch){
        this.importGoodsBatch = importGoodsBatch;
    }

    public Long getImportGoodsBatch(){
        return importGoodsBatch;
    }
    public void setImportUsdPrice(BigDecimal importUsdPrice){
        this.importUsdPrice = importUsdPrice;
    }

    public BigDecimal getImportUsdPrice(){
        return importUsdPrice;
    }
    public void setCheckBatch(Long checkBatch){
        this.checkBatch = checkBatch;
    }

    public Long getCheckBatch(){
        return checkBatch;
    }
    public void setCheckGoodsBatch(Long checkGoodsBatch){
        this.checkGoodsBatch = checkGoodsBatch;
    }

    public Long getCheckGoodsBatch(){
        return checkGoodsBatch;
    }
    public void setCheckUsdPrice(BigDecimal checkUsdPrice){
        this.checkUsdPrice = checkUsdPrice;
    }

    public BigDecimal getCheckUsdPrice(){
        return checkUsdPrice;
    }
    public void setYearMonth(Date yearMonth){
        this.yearMonth = yearMonth;
    }

    public Date getYearMonth(){
        return yearMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sortNum", getSortNum())
            .append("importEntryBatch", getImportEntryBatch())
            .append("importGoodsBatch", getImportGoodsBatch())
            .append("importUsdPrice", getImportUsdPrice())
            .append("checkBatch", getCheckBatch())
            .append("checkGoodsBatch", getCheckGoodsBatch())
            .append("checkUsdPrice", getCheckUsdPrice())
            .append("yearMonth", getYearMonth())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
