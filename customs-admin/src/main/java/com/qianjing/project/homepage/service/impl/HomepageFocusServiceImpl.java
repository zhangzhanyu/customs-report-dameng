package com.qianjing.project.homepage.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.homepage.mapper.HomepageFocusMapper;
import com.qianjing.project.homepage.domain.HomepageFocus;
import com.qianjing.project.homepage.service.IHomepageFocusService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 重点工作Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageFocusServiceImpl implements IHomepageFocusService {

    private static final Logger log = LoggerFactory.getLogger(HomepageFocusServiceImpl.class);

    private final HomepageFocusMapper homepageFocusMapper;

    /**
     * 查询重点工作
     * 
     * @param id 重点工作主键
     * @return 重点工作
     */
    @Override
    public HomepageFocus selectHomepageFocusById(Long id) {
        return homepageFocusMapper.selectHomepageFocusById(id);
    }

    /**
     * 查询重点工作列表
     * 
     * @param homepageFocus 重点工作
     * @return 重点工作
     */
    @Override
    public List<HomepageFocus> selectHomepageFocusList(HomepageFocus homepageFocus) {
        return homepageFocusMapper.selectHomepageFocusList(homepageFocus);
    }

    /**
     * 新增重点工作
     * 
     * @param homepageFocus 重点工作
     * @return 结果
     */
    @Override
    public int insertHomepageFocus(HomepageFocus homepageFocus) {
        homepageFocus.setCreateTime(DateUtils.getNowDate());
        return homepageFocusMapper.insertHomepageFocus(homepageFocus);
    }

    /**
     * 修改重点工作
     * 
     * @param homepageFocus 重点工作
     * @return 结果
     */
    @Override
    public int updateHomepageFocus(HomepageFocus homepageFocus) {
        homepageFocus.setUpdateTime(DateUtils.getNowDate());
        return homepageFocusMapper.updateHomepageFocus(homepageFocus);
    }

    /**
     * 批量删除重点工作
     * 
     * @param ids 需要删除的重点工作主键
     * @return 结果
     */
    @Override
    public int deleteHomepageFocusByIds(Long[] ids) {
        return homepageFocusMapper.deleteHomepageFocusByIds(ids);
    }

    /**
     * 删除重点工作信息
     * 
     * @param id 重点工作主键
     * @return 结果
     */
    @Override
    public int deleteHomepageFocusById(Long id) {
        return homepageFocusMapper.deleteHomepageFocusById(id);
    }

    /**
     * 导入重点工作数据
     *
     * @param homepageFocusList homepageFocusList 重点工作信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHomepageFocus(List<HomepageFocus> homepageFocusList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(homepageFocusList) || homepageFocusList.size() == 0) {
            throw new ServiceException("导入重点工作数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HomepageFocus homepageFocus : homepageFocusList){
            try {
                // 验证是否存在
                if (true) {
                    homepageFocus.setCreateBy(operName);
                    this.insertHomepageFocus(homepageFocus);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    homepageFocus.setUpdateBy(operName);
                    this.updateHomepageFocus(homepageFocus);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
