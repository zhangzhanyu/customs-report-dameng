package com.qianjing.project.homepage.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.homepage.domain.HomepageFocus;
import com.qianjing.project.homepage.service.IHomepageFocusService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 重点工作Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/homepage/focus")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageFocusController extends BaseController {

    private final IHomepageFocusService homepageFocusService;

    /**
     * 查询重点工作列表
     */
//    @PreAuthorize("@ss.hasPermi('homepage:focus:list')")
    @GetMapping("/list")
    public TableDataInfo list(HomepageFocus homepageFocus) {
        startPage();
        List<HomepageFocus> list = homepageFocusService.selectHomepageFocusList(homepageFocus);
        return getDataTable(list);
    }

    /**
     * 导出重点工作列表
     */
    @PreAuthorize("@ss.hasPermi('homepage:focus:export')")
    @Log(title = "重点工作", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HomepageFocus homepageFocus) {
        List<HomepageFocus> list = homepageFocusService.selectHomepageFocusList(homepageFocus);
        ExcelUtil<HomepageFocus> util = new ExcelUtil<HomepageFocus>(HomepageFocus.class);
        return util.exportExcel(list, "重点工作数据");
    }

    /**
     * 导入重点工作列表
     */
    @Log(title = "重点工作", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('homepage:focus:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HomepageFocus> util = new ExcelUtil<HomepageFocus>(HomepageFocus.class);
        List<HomepageFocus> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = homepageFocusService.importHomepageFocus(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载重点工作导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HomepageFocus> util = new ExcelUtil<HomepageFocus>(HomepageFocus.class);
        util.importTemplateExcel(response, "重点工作数据");
    }

    /**
     * 获取重点工作详细信息
     */
    @PreAuthorize("@ss.hasPermi('homepage:focus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(homepageFocusService.selectHomepageFocusById(id));
    }

    /**
     * 新增重点工作
     */
    @PreAuthorize("@ss.hasPermi('homepage:focus:add')")
    @Log(title = "重点工作", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HomepageFocus homepageFocus) {
        return toAjax(homepageFocusService.insertHomepageFocus(homepageFocus));
    }

    /**
     * 修改重点工作
     */
    @PreAuthorize("@ss.hasPermi('homepage:focus:edit')")
    @Log(title = "重点工作", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HomepageFocus homepageFocus) {
        return toAjax(homepageFocusService.updateHomepageFocus(homepageFocus));
    }

    /**
     * 删除重点工作
     */
    @PreAuthorize("@ss.hasPermi('homepage:focus:remove')")
    @Log(title = "重点工作", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(homepageFocusService.deleteHomepageFocusByIds(ids));
    }
}
