package com.qianjing.project.homepage.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.homepage.domain.HomepageInvalid;
import com.qianjing.project.homepage.service.IHomepageInvalidService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 不合格情况Controller
 *
 * @Created by 张占宇 2023-06-12 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/homepage/invalid")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageInvalidController extends BaseController {

    private final IHomepageInvalidService homepageInvalidService;

    /**
     * 查询不合格情况列表
     */
//    @PreAuthorize("@ss.hasPermi('homepage:invalid:list')")
    @GetMapping("/list")
    public TableDataInfo list(HomepageInvalid homepageInvalid) {
        startPage();
        List<HomepageInvalid> list = homepageInvalidService.selectHomepageInvalidList(homepageInvalid);
        return getDataTable(list);
    }

    /**
     * 导出不合格情况列表
     */
    @PreAuthorize("@ss.hasPermi('homepage:invalid:export')")
    @Log(title = "不合格情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HomepageInvalid homepageInvalid) {
        List<HomepageInvalid> list = homepageInvalidService.selectHomepageInvalidList(homepageInvalid);
        ExcelUtil<HomepageInvalid> util = new ExcelUtil<HomepageInvalid>(HomepageInvalid.class);
        return util.exportExcel(list, "不合格情况数据");
    }

    /**
     * 导入不合格情况列表
     */
    @Log(title = "不合格情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('homepage:invalid:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HomepageInvalid> util = new ExcelUtil<HomepageInvalid>(HomepageInvalid.class);
        List<HomepageInvalid> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = homepageInvalidService.importHomepageInvalid(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载不合格情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HomepageInvalid> util = new ExcelUtil<HomepageInvalid>(HomepageInvalid.class);
        util.importTemplateExcel(response, "不合格情况数据");
    }

    /**
     * 获取不合格情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('homepage:invalid:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(homepageInvalidService.selectHomepageInvalidById(id));
    }

    /**
     * 新增不合格情况
     */
    @PreAuthorize("@ss.hasPermi('homepage:invalid:add')")
    @Log(title = "不合格情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HomepageInvalid homepageInvalid) {
        return toAjax(homepageInvalidService.insertHomepageInvalid(homepageInvalid));
    }

    /**
     * 修改不合格情况
     */
    @PreAuthorize("@ss.hasPermi('homepage:invalid:edit')")
    @Log(title = "不合格情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HomepageInvalid homepageInvalid) {
        return toAjax(homepageInvalidService.updateHomepageInvalid(homepageInvalid));
    }

    /**
     * 删除不合格情况
     */
    @PreAuthorize("@ss.hasPermi('homepage:invalid:remove')")
    @Log(title = "不合格情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(homepageInvalidService.deleteHomepageInvalidByIds(ids));
    }
}
