package com.qianjing.project.homepage.service;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageInvalid;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 不合格情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface IHomepageInvalidService 
{
    /**
     * 查询不合格情况
     * 
     * @param id 不合格情况主键
     * @return 不合格情况
     */
    public HomepageInvalid selectHomepageInvalidById(Long id);

    /**
     * 查询不合格情况列表
     * 
     * @param homepageInvalid 不合格情况
     * @return 不合格情况集合
     */
    public List<HomepageInvalid> selectHomepageInvalidList(HomepageInvalid homepageInvalid);

    /**
     * 新增不合格情况
     * 
     * @param homepageInvalid 不合格情况
     * @return 结果
     */
    public int insertHomepageInvalid(HomepageInvalid homepageInvalid);

    /**
     * 修改不合格情况
     * 
     * @param homepageInvalid 不合格情况
     * @return 结果
     */
    public int updateHomepageInvalid(HomepageInvalid homepageInvalid);

    /**
     * 批量删除不合格情况
     * 
     * @param ids 需要删除的不合格情况主键集合
     * @return 结果
     */
    public int deleteHomepageInvalidByIds(Long[] ids);

    /**
     * 删除不合格情况信息
     * 
     * @param id 不合格情况主键
     * @return 结果
     */
    public int deleteHomepageInvalidById(Long id);

    /**
     * 导入不合格情况信息
     *
     * @param homepageInvalidList 不合格情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHomepageInvalid(List<HomepageInvalid> homepageInvalidList, Boolean isUpdateSupport, String operName);
}
