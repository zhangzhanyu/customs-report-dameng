package com.qianjing.project.homepage.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.homepage.mapper.HomepageInvalidMapper;
import com.qianjing.project.homepage.domain.HomepageInvalid;
import com.qianjing.project.homepage.service.IHomepageInvalidService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 不合格情况Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageInvalidServiceImpl implements IHomepageInvalidService {

    private static final Logger log = LoggerFactory.getLogger(HomepageInvalidServiceImpl.class);

    private final HomepageInvalidMapper homepageInvalidMapper;

    /**
     * 查询不合格情况
     * 
     * @param id 不合格情况主键
     * @return 不合格情况
     */
    @Override
    public HomepageInvalid selectHomepageInvalidById(Long id) {
        return homepageInvalidMapper.selectHomepageInvalidById(id);
    }

    /**
     * 查询不合格情况列表
     * 
     * @param homepageInvalid 不合格情况
     * @return 不合格情况
     */
    @Override
    public List<HomepageInvalid> selectHomepageInvalidList(HomepageInvalid homepageInvalid) {
        return homepageInvalidMapper.selectHomepageInvalidList(homepageInvalid);
    }

    /**
     * 新增不合格情况
     * 
     * @param homepageInvalid 不合格情况
     * @return 结果
     */
    @Override
    public int insertHomepageInvalid(HomepageInvalid homepageInvalid) {
        homepageInvalid.setCreateTime(DateUtils.getNowDate());
        return homepageInvalidMapper.insertHomepageInvalid(homepageInvalid);
    }

    /**
     * 修改不合格情况
     * 
     * @param homepageInvalid 不合格情况
     * @return 结果
     */
    @Override
    public int updateHomepageInvalid(HomepageInvalid homepageInvalid) {
        homepageInvalid.setUpdateTime(DateUtils.getNowDate());
        return homepageInvalidMapper.updateHomepageInvalid(homepageInvalid);
    }

    /**
     * 批量删除不合格情况
     * 
     * @param ids 需要删除的不合格情况主键
     * @return 结果
     */
    @Override
    public int deleteHomepageInvalidByIds(Long[] ids) {
        return homepageInvalidMapper.deleteHomepageInvalidByIds(ids);
    }

    /**
     * 删除不合格情况信息
     * 
     * @param id 不合格情况主键
     * @return 结果
     */
    @Override
    public int deleteHomepageInvalidById(Long id) {
        return homepageInvalidMapper.deleteHomepageInvalidById(id);
    }

    /**
     * 导入不合格情况数据
     *
     * @param homepageInvalidList homepageInvalidList 不合格情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHomepageInvalid(List<HomepageInvalid> homepageInvalidList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(homepageInvalidList) || homepageInvalidList.size() == 0) {
            throw new ServiceException("导入不合格情况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HomepageInvalid homepageInvalid : homepageInvalidList){
            try {
                // 验证是否存在
                if (true) {
                    homepageInvalid.setCreateBy(operName);
                    this.insertHomepageInvalid(homepageInvalid);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    homepageInvalid.setUpdateBy(operName);
                    this.updateHomepageInvalid(homepageInvalid);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
