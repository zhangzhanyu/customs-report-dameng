package com.qianjing.project.homepage.mapper;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageImportBusiness;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要进口业务数据Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface HomepageImportBusinessMapper {

    HomepageImportBusiness selectHomepageImportBusinessById(Long id);

    List<HomepageImportBusiness> selectHomepageImportBusinessList(HomepageImportBusiness homepageImportBusiness);

    int insertHomepageImportBusiness(HomepageImportBusiness homepageImportBusiness);

    int updateHomepageImportBusiness(HomepageImportBusiness homepageImportBusiness);

    int deleteHomepageImportBusinessById(Long id);

    int deleteHomepageImportBusinessByIds(Long[] ids);
}
