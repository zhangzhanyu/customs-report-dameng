package com.qianjing.project.homepage.mapper;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageInvalid;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 不合格情况Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-12 下午3:00    张占宇          V1.0          initialize
 */
public interface HomepageInvalidMapper {

    HomepageInvalid selectHomepageInvalidById(Long id);

    List<HomepageInvalid> selectHomepageInvalidList(HomepageInvalid homepageInvalid);

    int insertHomepageInvalid(HomepageInvalid homepageInvalid);

    int updateHomepageInvalid(HomepageInvalid homepageInvalid);

    int deleteHomepageInvalidById(Long id);

    int deleteHomepageInvalidByIds(Long[] ids);
}
