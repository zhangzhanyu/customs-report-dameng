package com.qianjing.project.homepage.service;

import java.util.List;
import com.qianjing.project.homepage.domain.HomepageGoods;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 主要商品情况Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-06-14 下午3:00    张占宇          V1.0          initialize
 */
public interface IHomepageGoodsService 
{
    /**
     * 查询主要商品情况
     * 
     * @param id 主要商品情况主键
     * @return 主要商品情况
     */
    public HomepageGoods selectHomepageGoodsById(Long id);

    /**
     * 查询主要商品情况列表
     * 
     * @param homepageGoods 主要商品情况
     * @return 主要商品情况集合
     */
    public List<HomepageGoods> selectHomepageGoodsList(HomepageGoods homepageGoods);

    /**
     * 新增主要商品情况
     * 
     * @param homepageGoods 主要商品情况
     * @return 结果
     */
    public int insertHomepageGoods(HomepageGoods homepageGoods);

    /**
     * 修改主要商品情况
     * 
     * @param homepageGoods 主要商品情况
     * @return 结果
     */
    public int updateHomepageGoods(HomepageGoods homepageGoods);

    /**
     * 批量删除主要商品情况
     * 
     * @param ids 需要删除的主要商品情况主键集合
     * @return 结果
     */
    public int deleteHomepageGoodsByIds(Long[] ids);

    /**
     * 删除主要商品情况信息
     * 
     * @param id 主要商品情况主键
     * @return 结果
     */
    public int deleteHomepageGoodsById(Long id);

    /**
     * 导入主要商品情况信息
     *
     * @param homepageGoodsList 主要商品情况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHomepageGoods(List<HomepageGoods> homepageGoodsList, Boolean isUpdateSupport, String operName);
}
