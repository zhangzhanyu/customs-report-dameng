package com.qianjing.project.homepage.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.homepage.domain.HomepageGoods;
import com.qianjing.project.homepage.service.IHomepageGoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 主要商品情况Controller
 *
 * @Created by 张占宇 2023-06-14 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/homepage/goods")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HomepageGoodsController extends BaseController {

    private final IHomepageGoodsService homepageGoodsService;

    /**
     * 查询主要商品情况列表
     */
//    @PreAuthorize("@ss.hasPermi('homepage:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(HomepageGoods homepageGoods) {
        startPage();
        List<HomepageGoods> list = homepageGoodsService.selectHomepageGoodsList(homepageGoods);
        return getDataTable(list);
    }

    /**
     * 导出主要商品情况列表
     */
    @PreAuthorize("@ss.hasPermi('homepage:goods:export')")
    @Log(title = "主要商品情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HomepageGoods homepageGoods) {
        List<HomepageGoods> list = homepageGoodsService.selectHomepageGoodsList(homepageGoods);
        ExcelUtil<HomepageGoods> util = new ExcelUtil<HomepageGoods>(HomepageGoods.class);
        return util.exportExcel(list, "主要商品情况数据");
    }

    /**
     * 导入主要商品情况列表
     */
    @Log(title = "主要商品情况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('homepage:goods:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HomepageGoods> util = new ExcelUtil<HomepageGoods>(HomepageGoods.class);
        List<HomepageGoods> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = homepageGoodsService.importHomepageGoods(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载主要商品情况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HomepageGoods> util = new ExcelUtil<HomepageGoods>(HomepageGoods.class);
        util.importTemplateExcel(response, "主要商品情况数据");
    }

    /**
     * 获取主要商品情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('homepage:goods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(homepageGoodsService.selectHomepageGoodsById(id));
    }

    /**
     * 新增主要商品情况
     */
    @PreAuthorize("@ss.hasPermi('homepage:goods:add')")
    @Log(title = "主要商品情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HomepageGoods homepageGoods) {
        return toAjax(homepageGoodsService.insertHomepageGoods(homepageGoods));
    }

    /**
     * 修改主要商品情况
     */
    @PreAuthorize("@ss.hasPermi('homepage:goods:edit')")
    @Log(title = "主要商品情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HomepageGoods homepageGoods) {
        return toAjax(homepageGoodsService.updateHomepageGoods(homepageGoods));
    }

    /**
     * 删除主要商品情况
     */
    @PreAuthorize("@ss.hasPermi('homepage:goods:remove')")
    @Log(title = "主要商品情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(homepageGoodsService.deleteHomepageGoodsByIds(ids));
    }
}
