package com.qianjing.project.overview.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.overview.mapper.OverviewGoodsMapper;
import com.qianjing.project.overview.domain.OverviewGoods;
import com.qianjing.project.overview.service.IOverviewGoodsService;

/**
 * 商品检验概况Service业务层处理
 * 
 * @author qianjing
 * @date 2022-06-10
 */
@Service
public class OverviewGoodsServiceImpl implements IOverviewGoodsService 
{
    private static final Logger log = LoggerFactory.getLogger(OverviewGoodsServiceImpl.class);

    @Autowired
    private OverviewGoodsMapper overviewGoodsMapper;

    /**
     * 查询商品检验概况
     * 
     * @param overviewGoodsId 商品检验概况主键
     * @return 商品检验概况
     */
    @Override
    public OverviewGoods selectOverviewGoodsByOverviewGoodsId(Long overviewGoodsId)
    {
        return overviewGoodsMapper.selectOverviewGoodsByOverviewGoodsId(overviewGoodsId);
    }

    /**
     * 查询商品检验概况列表
     * 
     * @param overviewGoods 商品检验概况
     * @return 商品检验概况
     */
    @Override
    public List<OverviewGoods> selectOverviewGoodsList(OverviewGoods overviewGoods)
    {
        return overviewGoodsMapper.selectOverviewGoodsList(overviewGoods);
    }

    /**
     * 新增商品检验概况
     * 
     * @param overviewGoods 商品检验概况
     * @return 结果
     */
    @Override
    public int insertOverviewGoods(OverviewGoods overviewGoods)
    {
        overviewGoods.setCreateTime(DateUtils.getNowDate());
        return overviewGoodsMapper.insertOverviewGoods(overviewGoods);
    }

    /**
     * 修改商品检验概况
     * 
     * @param overviewGoods 商品检验概况
     * @return 结果
     */
    @Override
    public int updateOverviewGoods(OverviewGoods overviewGoods)
    {
        overviewGoods.setUpdateTime(DateUtils.getNowDate());
        return overviewGoodsMapper.updateOverviewGoods(overviewGoods);
    }

    /**
     * 批量删除商品检验概况
     * 
     * @param overviewGoodsIds 需要删除的商品检验概况主键
     * @return 结果
     */
    @Override
    public int deleteOverviewGoodsByOverviewGoodsIds(Long[] overviewGoodsIds)
    {
        return overviewGoodsMapper.deleteOverviewGoodsByOverviewGoodsIds(overviewGoodsIds);
    }

    /**
     * 删除商品检验概况信息
     * 
     * @param overviewGoodsId 商品检验概况主键
     * @return 结果
     */
    @Override
    public int deleteOverviewGoodsByOverviewGoodsId(Long overviewGoodsId)
    {
        return overviewGoodsMapper.deleteOverviewGoodsByOverviewGoodsId(overviewGoodsId);
    }

    /**
     * 导入商品检验概况数据
     *
     * @param overviewGoodsList overviewGoodsList 商品检验概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importOverviewGoods(List<OverviewGoods> overviewGoodsList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(overviewGoodsList) || overviewGoodsList.size() == 0) {
            throw new ServiceException("导入商品检验概况数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (OverviewGoods overviewGoods : overviewGoodsList){
            try {
                // 验证是否存在
                if (true) {
                    overviewGoods.setCreateBy(operName);
                    this.insertOverviewGoods(overviewGoods);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    overviewGoods.setUpdateBy(operName);
                    this.updateOverviewGoods(overviewGoods);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
