package com.qianjing.project.overview.service;

import java.util.List;
import com.qianjing.project.overview.domain.OverviewGoods;

/**
 * 商品检验概况Service接口
 * 
 * @author qianjing
 * @date 2022-06-10
 */
public interface IOverviewGoodsService 
{
    /**
     * 查询商品检验概况
     * 
     * @param overviewGoodsId 商品检验概况主键
     * @return 商品检验概况
     */
    public OverviewGoods selectOverviewGoodsByOverviewGoodsId(Long overviewGoodsId);

    /**
     * 查询商品检验概况列表
     * 
     * @param overviewGoods 商品检验概况
     * @return 商品检验概况集合
     */
    public List<OverviewGoods> selectOverviewGoodsList(OverviewGoods overviewGoods);

    /**
     * 新增商品检验概况
     * 
     * @param overviewGoods 商品检验概况
     * @return 结果
     */
    public int insertOverviewGoods(OverviewGoods overviewGoods);

    /**
     * 修改商品检验概况
     * 
     * @param overviewGoods 商品检验概况
     * @return 结果
     */
    public int updateOverviewGoods(OverviewGoods overviewGoods);

    /**
     * 批量删除商品检验概况
     * 
     * @param overviewGoodsIds 需要删除的商品检验概况主键集合
     * @return 结果
     */
    public int deleteOverviewGoodsByOverviewGoodsIds(Long[] overviewGoodsIds);

    /**
     * 删除商品检验概况信息
     * 
     * @param overviewGoodsId 商品检验概况主键
     * @return 结果
     */
    public int deleteOverviewGoodsByOverviewGoodsId(Long overviewGoodsId);

    /**
     * 导入商品检验概况信息
     *
     * @param overviewGoodsList 商品检验概况信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importOverviewGoods(List<OverviewGoods> overviewGoodsList, Boolean isUpdateSupport, String operName);
}
