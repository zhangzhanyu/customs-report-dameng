package com.qianjing.project.overview.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.overview.domain.OverviewGoods;
import com.qianjing.project.overview.service.IOverviewGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 商品检验概况Controller
 * 
 * @author qianjing
 * @date 2022-06-10
 */
@RestController
@RequestMapping("/overview/goods")
public class OverviewGoodsController extends BaseController
{
    @Autowired
    private IOverviewGoodsService overviewGoodsService;

    /**
     * 查询商品检验概况列表
     */
    @PreAuthorize("@ss.hasPermi('overview:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(OverviewGoods overviewGoods)
    {
        startPage();
        List<OverviewGoods> list = overviewGoodsService.selectOverviewGoodsList(overviewGoods);
        return getDataTable(list);
    }

    /**
     * 导出商品检验概况列表
     */
    @PreAuthorize("@ss.hasPermi('overview:goods:export')")
    @Log(title = "商品检验概况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OverviewGoods overviewGoods)
    {
        List<OverviewGoods> list = overviewGoodsService.selectOverviewGoodsList(overviewGoods);
        ExcelUtil<OverviewGoods> util = new ExcelUtil<OverviewGoods>(OverviewGoods.class);
        return util.exportExcel(list, "商品检验概况数据");
    }

    /**
     * 导入商品检验概况列表
     */
    @Log(title = "商品检验概况", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('overview:goods:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<OverviewGoods> util = new ExcelUtil<OverviewGoods>(OverviewGoods.class);
        List<OverviewGoods> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = overviewGoodsService.importOverviewGoods(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载商品检验概况导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<OverviewGoods> util = new ExcelUtil<OverviewGoods>(OverviewGoods.class);
        util.importTemplateExcel(response, "商品检验概况数据");
    }

    /**
     * 获取商品检验概况详细信息
     */
    @PreAuthorize("@ss.hasPermi('overview:goods:query')")
    @GetMapping(value = "/{overviewGoodsId}")
    public AjaxResult getInfo(@PathVariable("overviewGoodsId") Long overviewGoodsId)
    {
        return AjaxResult.success(overviewGoodsService.selectOverviewGoodsByOverviewGoodsId(overviewGoodsId));
    }

    /**
     * 新增商品检验概况
     */
    @PreAuthorize("@ss.hasPermi('overview:goods:add')")
    @Log(title = "商品检验概况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OverviewGoods overviewGoods)
    {
        return toAjax(overviewGoodsService.insertOverviewGoods(overviewGoods));
    }

    /**
     * 修改商品检验概况
     */
    @PreAuthorize("@ss.hasPermi('overview:goods:edit')")
    @Log(title = "商品检验概况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OverviewGoods overviewGoods)
    {
        return toAjax(overviewGoodsService.updateOverviewGoods(overviewGoods));
    }

    /**
     * 删除商品检验概况
     */
    @PreAuthorize("@ss.hasPermi('overview:goods:remove')")
    @Log(title = "商品检验概况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{overviewGoodsIds}")
    public AjaxResult remove(@PathVariable Long[] overviewGoodsIds)
    {
        return toAjax(overviewGoodsService.deleteOverviewGoodsByOverviewGoodsIds(overviewGoodsIds));
    }
}
