package com.qianjing.project.overview.mapper;

import java.util.List;
import com.qianjing.project.overview.domain.OverviewGoods;

/**
 * 商品检验概况Mapper接口
 * 
 * @author qianjing
 * @date 2022-06-10
 */
public interface OverviewGoodsMapper 
{
    /**
     * 查询商品检验概况
     * 
     * @param overviewGoodsId 商品检验概况主键
     * @return 商品检验概况
     */
    public OverviewGoods selectOverviewGoodsByOverviewGoodsId(Long overviewGoodsId);

    /**
     * 查询商品检验概况列表
     * 
     * @param overviewGoods 商品检验概况
     * @return 商品检验概况集合
     */
    public List<OverviewGoods> selectOverviewGoodsList(OverviewGoods overviewGoods);

    /**
     * 新增商品检验概况
     * 
     * @param overviewGoods 商品检验概况
     * @return 结果
     */
    public int insertOverviewGoods(OverviewGoods overviewGoods);

    /**
     * 修改商品检验概况
     * 
     * @param overviewGoods 商品检验概况
     * @return 结果
     */
    public int updateOverviewGoods(OverviewGoods overviewGoods);

    /**
     * 删除商品检验概况
     * 
     * @param overviewGoodsId 商品检验概况主键
     * @return 结果
     */
    public int deleteOverviewGoodsByOverviewGoodsId(Long overviewGoodsId);

    /**
     * 批量删除商品检验概况
     * 
     * @param overviewGoodsIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOverviewGoodsByOverviewGoodsIds(Long[] overviewGoodsIds);
}
