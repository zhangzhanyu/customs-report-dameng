package com.qianjing.project.overview.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.Date;

/**
 * 商品检验概况对象 overview_goods
 *
 * @author qianjing
 * @date 2022-06-10
 */
public class OverviewGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long overviewGoodsId;

    /** 时间段 */
    @Excel(name = "时间段")
    private String timeRange;

    /** 开始时间 */
    private Date beginTime;

    /** 结束时间 */
    private Date endTime;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String goodsCategory;

    /** 报关单批次(进口总体情况) */
    @Excel(name = "报关单批次(进口总体情况)")
    private String totalBatchCustoms;

    /** 货物批次(进口总体情况) */
    @Excel(name = "货物批次(进口总体情况)")
    private String totalBatchGoods;

    /** 金额(人民币)(进口总体情况) */
    @Excel(name = "金额(人民币)(进口总体情况)")
    private String totalAmountRmb;

    /** 金额(美元)(进口总体情况) */
    @Excel(name = "金额(美元)(进口总体情况)")
    private String totalAmountUsd;

    /** 数量(进口总体情况) */
    @Excel(name = "数量(进口总体情况)")
    private String totalQuantity;

    /** 报关单批次(涉检布控查验情况) */
    @Excel(name = "报关单批次(涉检布控查验情况)")
    private String involveBatchCustoms;

    /** 货物批次(涉检布控查验情况) */
    @Excel(name = "货物批次(涉检布控查验情况)")
    private String involveBatchGoods;

    /** 金额(人民币)(涉检布控查验情况) */
    @Excel(name = "金额(人民币)(涉检布控查验情况)")
    private String involveAmountRmb;

    /** 金额(美元)(涉检布控查验情况) */
    @Excel(name = "金额(美元)(涉检布控查验情况)")
    private String involveAmountUsd;

    /** 数量(涉检布控查验情况) */
    @Excel(name = "数量(涉检布控查验情况)")
    private String involveQuantity;

    /** 报关单批次(布控送检情况) */
    @Excel(name = "报关单批次(布控送检情况)")
    private String submitBatchCustoms;

    /** 货物批次(布控送检情况) */
    @Excel(name = "货物批次(布控送检情况)")
    private String submitBatchGoods;

    /** 金额(人民币)(布控送检情况) */
    @Excel(name = "金额(人民币)(布控送检情况)")
    private String submitAmountRmb;

    /** 金额(美元)(布控送检情况) */
    @Excel(name = "金额(美元)(布控送检情况)")
    private String submitAmountUsd;

    /** 数量(布控送检情况) */
    @Excel(name = "数量(布控送检情况)")
    private String submitQuantity;

    /** 报关单批次(不合格情况) */
    @Excel(name = "报关单批次(不合格情况)")
    private String unqiedBatchCustoms;

    /** 货物批次(不合格情况) */
    @Excel(name = "货物批次(不合格情况)")
    private String unqiedBatchGoods;

    /** 金额(人民币)(不合格情况) */
    @Excel(name = "金额(人民币)(不合格情况)")
    private String unqiedAmountRmb;

    /** 金额(美元)(不合格情况) */
    @Excel(name = "金额(美元)(不合格情况)")
    private String unqiedAmountUsd;

    /** 数量(不合格情况) */
    @Excel(name = "数量(不合格情况)")
    private String unqiedQuantity;

    /** 报关单批次(检测不合格情况) */
    @Excel(name = "报关单批次(检测不合格情况)")
    private String inspectBatchCustoms;

    /** 货物批次(检测不合格情况) */
    @Excel(name = "货物批次(检测不合格情况)")
    private String inspectBatchGoods;

    /** 金额(人民币)(检测不合格情况) */
    @Excel(name = "金额(人民币)(检测不合格情况)")
    private String inspectAmountRmb;

    /** 金额(美元)(检测不合格情况) */
    @Excel(name = "金额(美元)(检测不合格情况)")
    private String inspectAmountUsd;

    /** 报关单批次(检测不合格情况) */
    @Excel(name = "报关单批次(检测不合格情况)")
    private String inspectQuantity;

    /** 审单放行批次(出证情况) */
    @Excel(name = "审单放行批次(出证情况)")
    private String certificateReviewBatch;

    /** 查验出证批次(出证情况) */
    @Excel(name = "查验出证批次(出证情况)")
    private String certificateInspectionBatch;

    /** 其他证书出证批次(出证情况) */
    @Excel(name = "其他证书出证批次(出证情况)")
    private String certificateOtherBatch;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setOverviewGoodsId(Long overviewGoodsId)
    {
        this.overviewGoodsId = overviewGoodsId;
    }

    public Long getOverviewGoodsId()
    {
        return overviewGoodsId;
    }
    public void setTimeRange(String timeRange)
    {
        this.timeRange = timeRange;
    }

    public String getTimeRange()
    {
        return timeRange;
    }
    public Date getBeginTime()
    {
        return beginTime;
    }

    public void setBeginTime(Date beginTime)
    {
        this.beginTime = beginTime;
    }
    public Date getEndTime()
    {
        return endTime;
    }

    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }
    public void setGoodsCategory(String goodsCategory)
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory()
    {
        return goodsCategory;
    }
    public void setTotalBatchCustoms(String totalBatchCustoms)
    {
        this.totalBatchCustoms = totalBatchCustoms;
    }

    public String getTotalBatchCustoms()
    {
        return totalBatchCustoms;
    }
    public void setTotalBatchGoods(String totalBatchGoods)
    {
        this.totalBatchGoods = totalBatchGoods;
    }

    public String getTotalBatchGoods()
    {
        return totalBatchGoods;
    }
    public void setTotalAmountRmb(String totalAmountRmb)
    {
        this.totalAmountRmb = totalAmountRmb;
    }

    public String getTotalAmountRmb()
    {
        return totalAmountRmb;
    }
    public void setTotalAmountUsd(String totalAmountUsd)
    {
        this.totalAmountUsd = totalAmountUsd;
    }

    public String getTotalAmountUsd()
    {
        return totalAmountUsd;
    }
    public void setTotalQuantity(String totalQuantity)
    {
        this.totalQuantity = totalQuantity;
    }

    public String getTotalQuantity()
    {
        return totalQuantity;
    }
    public void setInvolveBatchCustoms(String involveBatchCustoms)
    {
        this.involveBatchCustoms = involveBatchCustoms;
    }

    public String getInvolveBatchCustoms()
    {
        return involveBatchCustoms;
    }
    public void setInvolveBatchGoods(String involveBatchGoods)
    {
        this.involveBatchGoods = involveBatchGoods;
    }

    public String getInvolveBatchGoods()
    {
        return involveBatchGoods;
    }
    public void setInvolveAmountRmb(String involveAmountRmb)
    {
        this.involveAmountRmb = involveAmountRmb;
    }

    public String getInvolveAmountRmb()
    {
        return involveAmountRmb;
    }
    public void setInvolveAmountUsd(String involveAmountUsd)
    {
        this.involveAmountUsd = involveAmountUsd;
    }

    public String getInvolveAmountUsd()
    {
        return involveAmountUsd;
    }
    public void setInvolveQuantity(String involveQuantity)
    {
        this.involveQuantity = involveQuantity;
    }

    public String getInvolveQuantity()
    {
        return involveQuantity;
    }
    public void setSubmitBatchCustoms(String submitBatchCustoms)
    {
        this.submitBatchCustoms = submitBatchCustoms;
    }

    public String getSubmitBatchCustoms()
    {
        return submitBatchCustoms;
    }
    public void setSubmitBatchGoods(String submitBatchGoods)
    {
        this.submitBatchGoods = submitBatchGoods;
    }

    public String getSubmitBatchGoods()
    {
        return submitBatchGoods;
    }
    public void setSubmitAmountRmb(String submitAmountRmb)
    {
        this.submitAmountRmb = submitAmountRmb;
    }

    public String getSubmitAmountRmb()
    {
        return submitAmountRmb;
    }
    public void setSubmitAmountUsd(String submitAmountUsd)
    {
        this.submitAmountUsd = submitAmountUsd;
    }

    public String getSubmitAmountUsd()
    {
        return submitAmountUsd;
    }
    public void setSubmitQuantity(String submitQuantity)
    {
        this.submitQuantity = submitQuantity;
    }

    public String getSubmitQuantity()
    {
        return submitQuantity;
    }
    public void setUnqiedBatchCustoms(String unqiedBatchCustoms)
    {
        this.unqiedBatchCustoms = unqiedBatchCustoms;
    }

    public String getUnqiedBatchCustoms()
    {
        return unqiedBatchCustoms;
    }
    public void setUnqiedBatchGoods(String unqiedBatchGoods)
    {
        this.unqiedBatchGoods = unqiedBatchGoods;
    }

    public String getUnqiedBatchGoods()
    {
        return unqiedBatchGoods;
    }
    public void setUnqiedAmountRmb(String unqiedAmountRmb)
    {
        this.unqiedAmountRmb = unqiedAmountRmb;
    }

    public String getUnqiedAmountRmb()
    {
        return unqiedAmountRmb;
    }
    public void setUnqiedAmountUsd(String unqiedAmountUsd)
    {
        this.unqiedAmountUsd = unqiedAmountUsd;
    }

    public String getUnqiedAmountUsd()
    {
        return unqiedAmountUsd;
    }
    public void setUnqiedQuantity(String unqiedQuantity)
    {
        this.unqiedQuantity = unqiedQuantity;
    }

    public String getUnqiedQuantity()
    {
        return unqiedQuantity;
    }
    public void setInspectBatchCustoms(String inspectBatchCustoms)
    {
        this.inspectBatchCustoms = inspectBatchCustoms;
    }

    public String getInspectBatchCustoms()
    {
        return inspectBatchCustoms;
    }
    public void setInspectBatchGoods(String inspectBatchGoods)
    {
        this.inspectBatchGoods = inspectBatchGoods;
    }

    public String getInspectBatchGoods()
    {
        return inspectBatchGoods;
    }
    public void setInspectAmountRmb(String inspectAmountRmb)
    {
        this.inspectAmountRmb = inspectAmountRmb;
    }

    public String getInspectAmountRmb()
    {
        return inspectAmountRmb;
    }
    public void setInspectAmountUsd(String inspectAmountUsd)
    {
        this.inspectAmountUsd = inspectAmountUsd;
    }

    public String getInspectAmountUsd()
    {
        return inspectAmountUsd;
    }
    public void setInspectQuantity(String inspectQuantity)
    {
        this.inspectQuantity = inspectQuantity;
    }

    public String getInspectQuantity()
    {
        return inspectQuantity;
    }
    public void setCertificateReviewBatch(String certificateReviewBatch)
    {
        this.certificateReviewBatch = certificateReviewBatch;
    }

    public String getCertificateReviewBatch()
    {
        return certificateReviewBatch;
    }
    public void setCertificateInspectionBatch(String certificateInspectionBatch)
    {
        this.certificateInspectionBatch = certificateInspectionBatch;
    }

    public String getCertificateInspectionBatch()
    {
        return certificateInspectionBatch;
    }
    public void setCertificateOtherBatch(String certificateOtherBatch)
    {
        this.certificateOtherBatch = certificateOtherBatch;
    }

    public String getCertificateOtherBatch()
    {
        return certificateOtherBatch;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("overviewGoodsId", getOverviewGoodsId())
            .append("timeRange", getTimeRange())
            .append("goodsCategory", getGoodsCategory())
            .append("totalBatchCustoms", getTotalBatchCustoms())
            .append("totalBatchGoods", getTotalBatchGoods())
            .append("totalAmountRmb", getTotalAmountRmb())
            .append("totalAmountUsd", getTotalAmountUsd())
            .append("totalQuantity", getTotalQuantity())
            .append("involveBatchCustoms", getInvolveBatchCustoms())
            .append("involveBatchGoods", getInvolveBatchGoods())
            .append("involveAmountRmb", getInvolveAmountRmb())
            .append("involveAmountUsd", getInvolveAmountUsd())
            .append("involveQuantity", getInvolveQuantity())
            .append("submitBatchCustoms", getSubmitBatchCustoms())
            .append("submitBatchGoods", getSubmitBatchGoods())
            .append("submitAmountRmb", getSubmitAmountRmb())
            .append("submitAmountUsd", getSubmitAmountUsd())
            .append("submitQuantity", getSubmitQuantity())
            .append("unqiedBatchCustoms", getUnqiedBatchCustoms())
            .append("unqiedBatchGoods", getUnqiedBatchGoods())
            .append("unqiedAmountRmb", getUnqiedAmountRmb())
            .append("unqiedAmountUsd", getUnqiedAmountUsd())
            .append("unqiedQuantity", getUnqiedQuantity())
            .append("inspectBatchCustoms", getInspectBatchCustoms())
            .append("inspectBatchGoods", getInspectBatchGoods())
            .append("inspectAmountRmb", getInspectAmountRmb())
            .append("inspectAmountUsd", getInspectAmountUsd())
            .append("inspectQuantity", getInspectQuantity())
            .append("certificateReviewBatch", getCertificateReviewBatch())
            .append("certificateInspectionBatch", getCertificateInspectionBatch())
            .append("certificateOtherBatch", getCertificateOtherBatch())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
