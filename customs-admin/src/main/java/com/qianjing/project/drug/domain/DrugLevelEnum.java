package com.qianjing.project.drug.domain;

/**
 * @author zzy
 * @version $Id: DrugLevelEnum.java, v 0.1 2021/3/10 11:03 张占宇 Exp $
 */
public enum DrugLevelEnum {

    ONE("一级", "1"), TWO("二级", "2"), THREE("三级", "3"), FOUR("四级", "4");

    private String text;

    private String value;

    DrugLevelEnum(String text, String value) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取对应的中文
     * @param value
     * @return
     */
    public static String returnTextByValue(String value){
        DrugLevelEnum[] values = DrugLevelEnum.values();
        for (DrugLevelEnum projectStatusEnum : values){
            if(projectStatusEnum.getValue().equals(value)){
                return  projectStatusEnum.getText();
            }
        }
        return null;
    }

    /**
     * 获取对应数字
     * @param text
     * @return
     */
    public static String returnValueByText(String text){
        DrugLevelEnum[] values = DrugLevelEnum.values();
        for (DrugLevelEnum smallProgramEnum : values){
            if(smallProgramEnum.getText().equals(text)){
                return smallProgramEnum.getValue();
            }
        }
        return null;
    }

}
