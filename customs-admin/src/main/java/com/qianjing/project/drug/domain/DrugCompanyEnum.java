package com.qianjing.project.drug.domain;

/**
 * @author zzy
 * @version $Id: DrugCompanyEnum.java, v 0.1 2021/3/10 11:03 张占宇 Exp $
 */
public enum DrugCompanyEnum {

    PHILIPS_ANDOVER("Philips Andover", "1"), SIEMENS("Siemens", "2");

    private String text;

    private String value;

    DrugCompanyEnum(String text, String value) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取对应的中文
     * @param value
     * @return
     */
    public static String returnTextByValue(String value){
        DrugCompanyEnum[] values = DrugCompanyEnum.values();
        for (DrugCompanyEnum projectStatusEnum : values){
            if(projectStatusEnum.getValue().equals(value)){
                return  projectStatusEnum.getText();
            }
        }
        return null;
    }

    /**
     * 获取对应数字
     * @param text
     * @return
     */
    public static String returnValueByText(String text){
        DrugCompanyEnum[] values = DrugCompanyEnum.values();
        for (DrugCompanyEnum smallProgramEnum : values){
            if(smallProgramEnum.getText().equals(text)){
                return smallProgramEnum.getValue();
            }
        }
        return null;
    }

}
