package com.qianjing.project.drug.controller;

import com.qianjing.common.constant.UserConstants;
import com.qianjing.common.utils.StringUtils;
import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.common.utils.valid.InsertGroup;
import com.qianjing.common.utils.valid.UpdateGroup;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.drug.domain.DrugSupervision;
import com.qianjing.project.drug.domain.DrugSupervisionDetail;
import com.qianjing.project.drug.service.IDrugSupervisionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Classname DrugSupervisionController
 * @Description 药品监督Controller
 * @Created by zzy 2021年11月17日 下午3:55:54
 * @Version V1.0
 */
@Validated
@RestController
@RequestMapping("/drug/supervision")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DrugSupervisionController extends BaseController {

    private final IDrugSupervisionService drugSupervisionService;

    /**
     * 查询药品监督列表
     * @Param drugSupervision 药品监督
     * @return AjaxResult
     * @Date: 2021年3月18日 下午3:55:54
     */
    @PostMapping("/selectDrugSupervisionList")
    public TableDataInfo selectDrugSupervisionList(DrugSupervision drugSupervision) {
        startPage();
        List<DrugSupervisionDetail> list = drugSupervisionService.selectDrugSupervisionList(drugSupervision);
        return getDataTable(list);
    }

    @GetMapping("/export")
    public AjaxResult export(DrugSupervision drugSupervision) {
        List<DrugSupervisionDetail> list = drugSupervisionService.selectDrugSupervisionList(drugSupervision);
        ExcelUtil<DrugSupervisionDetail> util = new ExcelUtil<>(DrugSupervisionDetail.class);
        return util.exportExcel(list, "药品监督");
    }

    @PostMapping("/importDrugSupervision")
    public AjaxResult importDrugSupervision(MultipartFile file) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<DrugSupervisionDetail> util = new ExcelUtil<>(DrugSupervisionDetail.class);
        List<DrugSupervisionDetail> drugSupervisionDetailList = util.importExcel(file.getInputStream());
        //String operName = getUsername();
        String operName = "当前用户";
        String message = drugSupervisionService.importDrugSupervision(drugSupervisionDetailList,operName);
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    public AjaxResult importTemplate() {
        ExcelUtil<DrugSupervisionDetail> util = new ExcelUtil<>(DrugSupervisionDetail.class);
        return util.importTemplateExcel("药品监督");
    }


    /**
     * 新增药品监督信息
     * @Param drugSupervision 药品监督
     * @return AjaxResult
     * @Date: 2021年3月18日 下午3:55:54
     */
    @Log(title = "药品监督", businessType = BusinessType.INSERT)
    @PostMapping("/insertDrugSupervision")
    public AjaxResult insertDrugSupervision(@Validated({InsertGroup.class}) @RequestBody DrugSupervision drugSupervision) {
        if (StringUtils.isNotEmpty(drugSupervision.getIdNumber()) && UserConstants.NOT_UNIQUE.equals(drugSupervisionService.checkIdNumber(drugSupervision))) {
            return AjaxResult.error("新增药品监督信息'" + drugSupervision.getIdNumber() + "'失败，药品编号已存在");
        }
        return toAjax(drugSupervisionService.insertDrugSupervision(drugSupervision));
    }

    /**
     * 根据药品监督编号获取详细信息
     * @Param drugSupervision 药品监督
     * @return AjaxResult
     * @Date: 2021年3月18日 下午3:55:54
     */
    @PostMapping( "/selectDrugSupervisionById")
    public AjaxResult selectDrugSupervisionById(@NotBlank(message = "请携带必要参数【drugSupervisionId】！") Long drugSupervisionId) {
        return AjaxResult.success(drugSupervisionService.selectDrugSupervisionById(drugSupervisionId));
    }

    /**
     * 修改药品监督信息
     * @Param drugSupervision 药品监督
     * @return AjaxResult
     * @Date: 2021年3月18日 下午3:55:54
     */
    @Log(title = "药品监督", businessType = BusinessType.UPDATE)
    @PostMapping("/updateDrugSupervision")
    public AjaxResult updateDrugSupervision(@Validated({UpdateGroup.class}) @RequestBody DrugSupervision drugSupervision) {
        if (StringUtils.isNotEmpty(drugSupervision.getIdNumber()) && UserConstants.NOT_UNIQUE.equals(drugSupervisionService.checkIdNumber(drugSupervision))) {
            return AjaxResult.error("修改药品监督信息'" + drugSupervision.getIdNumber() + "'失败，药品编号已存在");
        }
        return toAjax(drugSupervisionService.updateDrugSupervision(drugSupervision));
    }

    /**
     * 删除药品监督信息
     * @Param drugSupervisionId 药品监督编号
     * @return AjaxResult
     * @Date: 2021年3月18日 下午3:55:54
     */
    @PostMapping( "/deleteDrugSupervisionByIds")
    public AjaxResult deleteDrugSupervisionByIds(@NotBlank(message = "请携带必要参数【drugSupervisionIds】！") String drugSupervisionIds) {
        return toAjax(drugSupervisionService.deleteDrugSupervisionByIds(drugSupervisionIds));
    }

}
