package com.qianjing.project.drug.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.common.utils.valid.InsertGroup;
import com.qianjing.common.utils.valid.UpdateGroup;
import com.qianjing.framework.web.domain.BaseEntity;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Copyright: Copyright (c) 2021 上海前景网络科技有限公司
 *
 * <p>说明： 药品监督实体类</P>
 * @Version: V1.0
 * @Author: zzy
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2021年3月10日        惊鸿着陆          V1.0          initialize
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DrugSupervision extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    @NotNull(message = "drugSupervisionId不可为空",groups = {UpdateGroup.class})
    private Long drugSupervisionId;

    /** 编号 */
    @NotBlank(message = "idNumber不可为空",groups = {InsertGroup.class})
    private String idNumber;

    /** 源名称(英文) */
    @NotBlank(message = "sourceNameEnglish不可为空",groups = {InsertGroup.class})
    private String sourceNameEnglish;

    /** 源名称(中文) */
    @NotBlank(message = "sourceNameChinese不可为空",groups = {InsertGroup.class})
    private String sourceNameChinese;

    /** 源域名 */
    @NotBlank(message = "sourceDomainName不可为空",groups = {InsertGroup.class})
    private String sourceDomainName;

    /** 来源类型 */
    @NotBlank(message = "sourceType不可为空",groups = {InsertGroup.class})
    private String sourceType;

    /** 源url */
    @NotBlank(message = "sourceUrl不可为空",groups = {InsertGroup.class})
    private String sourceUrl;

    /** 收集日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date collectTime;

    /** 出版日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date publishTime;

    /** 标题(英文) */
    @NotBlank(message = "titleEnglish不可为空",groups = {InsertGroup.class})
    private String titleEnglish;

    /** 标题(中文) */
    @NotBlank(message = "titleChinese不可为空",groups = {InsertGroup.class})
    private String titleChinese;

    /** 产品(英文) */
    @NotBlank(message = "productEnglish不可为空",groups = {InsertGroup.class})
    private String productEnglish;

    /** 产品(中文) */
    @NotBlank(message = "productChinese不可为空",groups = {InsertGroup.class})
    private String productChinese;

    /** 国家(1美国 2中国 3加拿大 4澳大利亚) */
    //@NotBlank(message = "country不可为空",groups = {InsertGroup.class})
    private String country;

    /** 补救方法 */
    @NotBlank(message = "drugSelection不可为空",groups = {InsertGroup.class})
    private String drugSelection;

    /** 产品标签(1医疗器械) */
    private String productSelection;

    /** 产品选择-ID(1代表35 2代表40) */
    private String productSelectionId;

    /** 公司标签(1Philips Andover 2Siemens) */
    //@NotBlank(message = "公司标签companyLabel不可为空",groups = {InsertGroup.class})
    private String companyLabel;

    /** 源标记 */
    private String sourceTag;

    /** 中国标签 */
    private String chinaLabel;

    /** 儿童标签 */
    private String childLabel;

    /** 标签危害 */
    @NotBlank(message = "labelHarm不可为空",groups = {InsertGroup.class})
    private String labelHarm;

    /** 标签情感 */
    private String labelEmotion;

    /** 标签等级(1一级 2二级 3三级 4四级) */
    //@NotBlank(message = "标签等级【labelLevel】不可为空",groups = {InsertGroup.class})
    private String labelLevel;

    /** 召回数量 */
    @NotBlank(message = "召回数量【recallNumber】不可为空",groups = {InsertGroup.class})
    private String recallNumber;

    /** 召回日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date recallDate;

    /** productUPCs */
    private String productUpcs;

    /** 消费者接触 */
    @NotBlank(message = "消费者接触【consumerContact】不可为空",groups = {InsertGroup.class})
    private String consumerContact;

    /** 伤害 */
    private String hurt;

    /** 引用 */
    private String quote;

    /** 危害英文 */
    @NotBlank(message = "危害英文【harmEnglish】不可为空",groups = {InsertGroup.class})
    private String harmEnglish;

    /** 危害中文 */
    @NotBlank(message = "危害中文【harmChinese】不可为空",groups = {InsertGroup.class})
    private String harmChinese;

    /** P1 */
    private String P1;

    /** P2 */
    private String P2;

    /** P3 */
    private String P3;

    /** P4 */
    private String P4;

    /** P5 */
    private String P5;

    /** P6 */
    private String P6;

    /** P7 */
    private String P7;

    /** P8 */
    private String P8;

    /** P9 */
    private String P9;

    /** tagAged */
    @NotBlank(message = "【tagAged】不可为空",groups = {InsertGroup.class})
    private String tagAged;

    /** 关系 */
    private String relationship;

    /** 相似性 */
    private String similarity;

    /** referrsd-number */
    private String referrsdNumber;

    /** 国家召回(1美国 2中国) */
    //@NotBlank(message = "国家召回【nationalRecall】不可为空",groups = {InsertGroup.class})
    private String nationalRecall;

    /** 标记源回收 */
    @NotBlank(message = "标记源回收【tagSourceRecycle】不可为空",groups = {InsertGroup.class})
    private String tagSourceRecycle;

    /** 标记的产品 */
    @NotBlank(message = "标记的产品【tagProduct】不可为空",groups = {InsertGroup.class})
    private String tagProduct;

    /** 标记的分数 */
    @NotBlank(message = "标记的分数【tagScore】不可为空",groups = {InsertGroup.class})
    private String tagScore;

    /** 星期 */
    @NotBlank(message = "星期【weekDay】不可为空",groups = {InsertGroup.class})
    private String weekDay;

    /** 描述(英文) */
    @NotBlank(message = "描述(英文)【describeEnglish】不可为空",groups = {InsertGroup.class})
    @Size(min = 0, max = 500, message = "英文描述长度不能超过500个字符")
    private String describeEnglish;

    /** 描述(中文) */
    @NotBlank(message = "描述(中文)【describeChinese】不可为空",groups = {InsertGroup.class})
    @Size(min = 0, max = 500, message = "中文描述长度不能超过500个字符")
    private String describeChinese;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;


}
