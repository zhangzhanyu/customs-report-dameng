package com.qianjing.project.drug.service;

import java.util.List;
import com.qianjing.project.drug.domain.DrugSupervision;
import com.qianjing.project.drug.domain.DrugSupervisionDetail;
import com.qianjing.project.system.domain.SysUser;

/**
 * Copyright: Copyright (c) 2021 上海前景网络科技有限公司
 *
 * <p>说明： 药品监督Service接口</P>
 * @Version: V1.0
 * @Author: zzy
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2021年3月10日        惊鸿着陆          V1.0          initialize
 */
public interface IDrugSupervisionService {
    /**
     * 查询药品监督
     * 
     * @param drugSupervisionId 药品监督ID
     * @return 药品监督
     */
    DrugSupervisionDetail selectDrugSupervisionById(Long drugSupervisionId);

    /**
     * 查询药品监督列表
     * 
     * @param drugSupervision 药品监督
     * @return 药品监督集合
     */
    List<DrugSupervisionDetail> selectDrugSupervisionList(DrugSupervision drugSupervision);

    /**
     * 校验药品编号是否唯一
     *
     * @param drugSupervision 药品监督
     * @return 结果
     */
    String checkIdNumber(DrugSupervision drugSupervision);

    /**
     * 新增药品监督
     * 
     * @param drugSupervision 药品监督
     * @return 结果
     */
    int insertDrugSupervision(DrugSupervision drugSupervision);

    /**
     * 修改药品监督
     * 
     * @param drugSupervision 药品监督
     * @return 结果
     */
    int updateDrugSupervision(DrugSupervision drugSupervision);

    /**
     * 删除药品监督信息
     *
     * @param drugSupervisionIds 药品监督ID
     * @return 结果
     */
    int deleteDrugSupervisionByIds(String drugSupervisionIds);

    /**
     * 导入药品监督信息
     *
     * @param drugSupervisionDetailList 药品监督列表
     * @param operName 操作用户
     * @return 结果
     */
    String importDrugSupervision(List<DrugSupervisionDetail> drugSupervisionDetailList, String operName);

}
