package com.qianjing.project.drug.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.qianjing.common.constant.Constants;
import com.qianjing.common.core.text.Convert;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.StringUtils;
import com.qianjing.common.utils.bean.BeanUtils;
import com.qianjing.project.drug.domain.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.project.drug.mapper.DrugSupervisionMapper;
import com.qianjing.project.drug.service.IDrugSupervisionService;

/**
 * Copyright: Copyright (c) 2021 上海前景网络科技有限公司
 *
 * <p>说明： 药品监督Service接口</P>
 * @Version: V1.0
 * @Author: zzy
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2021年3月10日        惊鸿着陆          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DrugSupervisionServiceImpl implements IDrugSupervisionService {

    private final DrugSupervisionMapper drugSupervisionMapper;

    /**
     * 查询药品监督
     * @Create by zzy 2021-11-17
     * @Param drugSupervisionId 药品监督ID
     * @return 药品监督
     */
    @Override
    public DrugSupervisionDetail selectDrugSupervisionById(Long drugSupervisionId) {
        return drugSupervisionMapper.selectDrugSupervisionById(drugSupervisionId);
    }

    /**
     * 查询药品监督列表
     *
     * @param drugSupervision 药品监督
     * @return 药品监督
     */
    @Override
    public List<DrugSupervisionDetail> selectDrugSupervisionList(DrugSupervision drugSupervision) {
        List<DrugSupervisionDetail> drugSupervisionDetailList = drugSupervisionMapper.selectDrugSupervisionList(drugSupervision);
        for (DrugSupervisionDetail drugSupervisionDetail:drugSupervisionDetailList) {
            drugSupervisionDetail.setCountryText(DrugCountryEnum.returnTextByValue(drugSupervisionDetail.getCountry()));
            drugSupervisionDetail.setLabelLevelText(DrugLevelEnum.returnTextByValue(drugSupervisionDetail.getLabelLevel()));
            drugSupervisionDetail.setCompanyLabelText(DrugCompanyEnum.returnTextByValue(drugSupervisionDetail.getCompanyLabel()));
        }
        return drugSupervisionDetailList;
    }

    /**
     * 校验药品编号是否唯一
     *
     * @param drugSupervision 药品监督
     * @return 结果
     */
    @Override
    public String checkIdNumber(DrugSupervision drugSupervision) {
        Long drugSupervisionId = StringUtils.isNull(drugSupervision.getDrugSupervisionId()) ? -1L : drugSupervision.getDrugSupervisionId();
        DrugSupervisionDetail info = drugSupervisionMapper.checkIdNumber(drugSupervision.getIdNumber());
        if (StringUtils.isNotNull(info) && info.getDrugSupervisionId().longValue() != drugSupervisionId.longValue()) {
            return Constants.NOT_UNIQUE;
        }
        return Constants.UNIQUE;
    }

    /**
     * 新增药品监督
     *
     * @param drugSupervision 药品监督
     * @return 结果
     */
    @Override
    public int insertDrugSupervision(DrugSupervision drugSupervision) {
        drugSupervision.setDelFlag(Constants.DB_IS_STATUS_NO);
        drugSupervision.setCreateTime(new Date());
        drugSupervision.setUpdateTime(new Date());
        return drugSupervisionMapper.insertDrugSupervision(drugSupervision);
    }

    /**
     * 修改药品监督
     *
     * @param drugSupervision 药品监督
     * @return 结果
     */
    @Override
    public int updateDrugSupervision(DrugSupervision drugSupervision) {
        drugSupervision.setUpdateTime(new Date());
        return drugSupervisionMapper.updateDrugSupervision(drugSupervision);
    }

    /**
     * 删除药品监督信息
     *
     * @param ids 药品监督ID
     * @return 结果
     */
    @Override
    public int deleteDrugSupervisionByIds(String ids) {
        String[] drugSupervisionIds = Convert.toStrArray(ids);
        return drugSupervisionMapper.deleteDrugSupervisionByIds(drugSupervisionIds);
    }

    /**
     * 导入药品监督信息
     *
     * @param drugSupervisionDetailList 药品监督列表
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importDrugSupervision(List<DrugSupervisionDetail> drugSupervisionDetailList, String operName) {
        List<DrugSupervision> batchInsertList = new ArrayList<>();
        List<DrugSupervision> batchUpdateList = new ArrayList<>();
        for (DrugSupervisionDetail drugSupervisionDetail : drugSupervisionDetailList) {
            DrugSupervision drugSupervision = new DrugSupervision();
            BeanUtils.copyBeanProp(drugSupervision, drugSupervisionDetail);
            drugSupervision.setCompanyLabel(DrugCompanyEnum.returnValueByText(drugSupervisionDetail.getCompanyLabelText()));
            drugSupervision.setCountry(DrugCountryEnum.returnValueByText(drugSupervisionDetail.getCountryText()));
            drugSupervision.setLabelLevel(DrugLevelEnum.returnValueByText(drugSupervisionDetail.getLabelLevelText()));
            drugSupervision.setDelFlag(Constants.DB_IS_STATUS_NO);
            drugSupervision.setCreateTime(new Date());
            drugSupervision.setUpdateTime(new Date());
            if (StringUtils.isBlank(drugSupervision.getIdNumber())){
                throw new ServiceException("药品编号字段不能为空！");
            }
            if (drugSupervisionMapper.checkIdNumber(drugSupervision.getIdNumber())!=null) {
                batchUpdateList.add(drugSupervision);
            } else {
                batchInsertList.add(drugSupervision);
            }
        }
        int insertNum = 0;
        int updateNum = 0;
        if (batchInsertList.size()>0) {
            insertNum = drugSupervisionMapper.batchInsertDrugSupervision(batchInsertList);
        }
        if (batchUpdateList.size()>0) {
            updateNum = drugSupervisionMapper.batchUpdateDrugSupervision(batchUpdateList);
        }
        return "新增:"+insertNum+"条;更新:"+updateNum+"条";
    }

}
