package com.qianjing.project.drug.domain;

/**
 * @author zzy
 * @version $Id: DrugCountryEnum.java, v 0.1 2021/3/10 11:03 张占宇 Exp $
 */
public enum DrugCountryEnum {

    US("美国", "1"), CH("中国", "2"), CA("加拿大", "3"), AU("澳大利亚", "4");

    private String text;

    private String value;

    DrugCountryEnum(String text, String value) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取对应的中文
     * @param value
     * @return
     */
    public static String returnTextByValue(String value){
        DrugCountryEnum[] values = DrugCountryEnum.values();
        for (DrugCountryEnum projectStatusEnum : values){
            if(projectStatusEnum.getValue().equals(value)){
                return  projectStatusEnum.getText();
            }
        }
        return null;
    }

    /**
     * 获取对应数字
     * @param text
     * @return
     */
    public static String returnValueByText(String text){
        DrugCountryEnum[] values = DrugCountryEnum.values();
        for (DrugCountryEnum smallProgramEnum : values){
            if(smallProgramEnum.getText().equals(text)){
                return smallProgramEnum.getValue();
            }
        }
        return null;
    }

}
