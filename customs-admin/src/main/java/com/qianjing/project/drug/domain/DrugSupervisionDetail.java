package com.qianjing.project.drug.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.common.utils.valid.InsertGroup;
import com.qianjing.common.utils.valid.UpdateGroup;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author : 张占宇
 * create at:  2021-10-22  11:36
 * @description: 对外暴露 - 药品监督
 */
@Getter
@Setter
public class DrugSupervisionDetail {

    /** 业务主键 */
    private Long drugSupervisionId;

    /** 编号 */
    @Excel(name = "编号")
    private String idNumber;

    /** 源名称(英文) */
    @Excel(name = "源名称(英文)")
    private String sourceNameEnglish;

    /** 源名称(中文) */
    @Excel(name = "源名称(中文)")
    private String sourceNameChinese;

    /** 源域名 */
    @Excel(name = "源域名")
    private String sourceDomainName;

    /** 来源类型 */
    @Excel(name = "来源类型")
    private String sourceType;

    /** 源url */
    @Excel(name = "源url")
    private String sourceUrl;

    /** 收集日期 */
    @Excel(name = "收集日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date collectTime;

    /** 出版日期 */
    @Excel(name = "出版日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date publishTime;

    /** 标题(英文) */
    @Excel(name = "标题(英文)")
    private String titleEnglish;

    /** 标题(中文) */
    @Excel(name = "标题(中文)")
    private String titleChinese;

    /** 产品(英文) */
    @Excel(name = "产品(英文)")
    private String productEnglish;

    /** 产品(中文) */
    @Excel(name = "产品(中文)")
    private String productChinese;

    /** 国家(1-美国 2-中国 3-加拿大 4-澳大利亚) */
    private String country;

    @Excel(name = "国家")
    private String countryText;

    /** 补救方法 */
    @Excel(name = "补救方法")
    private String drugSelection;

    /** 产品标签(1医疗器械) */
    @Excel(name = "产品标签")
    private String productSelection;

    /** 产品选择-ID(1代表35 2代表40) */
    @Excel(name = "产品选择-ID")
    private String productSelectionId;

    /** 公司标签(1Philips Andover 2Siemens) */
    private String companyLabel;

    @Excel(name = "公司标签")
    private String companyLabelText;

    /** 源标记 */
    @Excel(name = "源标记")
    private String sourceTag;

    /** 中国标签 */
    @Excel(name = "中国标签")
    private String chinaLabel;

    /** 儿童标签 */
    @Excel(name = "儿童标签")
    private String childLabel;

    /** 标签危害 */
    @Excel(name = "标签危害")
    private String labelHarm;

    /** 标签情感 */
    @Excel(name = "标签情感")
    private String labelEmotion;

    /** 标签等级(1一级 2二级 3三级 4四级) */
    private String labelLevel;

    @Excel(name = "标签等级")
    private String labelLevelText;

    /** 召回数量 */
    @Excel(name = "召回数量")
    private String recallNumber;

    /** 召回日期 */
    @Excel(name = "召回日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date recallDate;

    /** productUPCs */
    @Excel(name = "productUPCs")
    private String productUpcs;

    /** 消费者接触 */
    @Excel(name = "消费者接触")
    private String consumerContact;

    /** 伤害 */
    @Excel(name = "伤害")
    private String hurt;

    /** 引用 */
    @Excel(name = "引用")
    private String quote;

    /** 危害英文 */
    @Excel(name = "危害英文")
    private String harmEnglish;

    /** 危害中文 */
    @Excel(name = "危害中文")
    private String harmChinese;

    /** P1 */
    @Excel(name = "P1")
    private String P1;

    /** c */
    @Excel(name = "P1")
    private String P2;

    /** P3 */
    @Excel(name = "P3")
    private String P3;

    /** P4 */
    @Excel(name = "P4")
    private String P4;

    /** P5 */
    @Excel(name = "P5")
    private String P5;

    /** P6 */
    @Excel(name = "P6")
    private String P6;

    /** P7 */
    @Excel(name = "P7")
    private String P7;

    /** P8 */
    @Excel(name = "P8")
    private String P8;

    /** P9 */
    @Excel(name = "P9")
    private String P9;

    @Excel(name = "tagAged")
    private String tagAged;

    /** 关系 */
    @Excel(name = "关系")
    private String relationship;

    /** 相似性 */
    @Excel(name = "相似性")
    private String similarity;

    /** referrsd-number */
    @Excel(name = "referrsd-number")
    private String referrsdNumber;

    /** 国家召回(1美国 2中国) */
    @Excel(name = "国家召回")
    private String nationalRecall;

    /** 标记源回收 */
    @Excel(name = "标记源回收")
    private String tagSourceRecycle;

    /** 标记的产品 */
    @Excel(name = "标记的产品")
    private String tagProduct;

    /** 标记的分数 */
    @Excel(name = "标记的分数")
    private String tagScore;

    /** 星期 */
    @Excel(name = "星期")
    private String weekDay;

    /** 描述(英文) */
    @Excel(name = "描述(英文)")
    private String describeEnglish;

    /** 描述(中文) */
    @Excel(name = "描述(中文)")
    private String describeChinese;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;


}
