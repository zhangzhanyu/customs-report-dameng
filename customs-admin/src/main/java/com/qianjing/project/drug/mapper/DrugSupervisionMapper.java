package com.qianjing.project.drug.mapper;

import java.util.List;
import com.qianjing.project.drug.domain.DrugSupervision;
import com.qianjing.project.drug.domain.DrugSupervisionDetail;
import com.qianjing.project.system.domain.SysUserRole;

/**
 * Copyright: Copyright (c) 2021 上海前景网络科技有限公司
 *
 * <p>说明： 药品监督Mapper接口</P>
 * @Version: V1.0
 * @Author: 张占宇
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2021年3月10日        惊鸿着陆          V1.0          initialize
 */
public interface DrugSupervisionMapper {

    DrugSupervisionDetail selectDrugSupervisionById(Long drugSupervisionId);

    List<DrugSupervisionDetail> selectDrugSupervisionList(DrugSupervision drugSupervision);

    DrugSupervisionDetail checkIdNumber(String idNumber);

    int insertDrugSupervision(DrugSupervision drugSupervision);

    int updateDrugSupervision(DrugSupervision drugSupervision);

    int deleteDrugSupervisionByIds(String[] drugSupervisionIds);

    int batchUpdateDrugSupervision(List<DrugSupervision> drugSupervisionList);

    int batchInsertDrugSupervision(List<DrugSupervision> drugSupervisionList);
}
