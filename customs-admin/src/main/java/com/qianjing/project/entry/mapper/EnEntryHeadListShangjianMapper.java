package com.qianjing.project.entry.mapper;

import java.util.List;
import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;
import org.apache.ibatis.annotations.Param;

/**
 * 报关表头表体Mapper接口
 *
 * @author qianjing
 * @date 2023-03-09
 */
public interface EnEntryHeadListShangjianMapper
{

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    public EnEntryHeadListShangjian checkEntryIdGnoUnique(@Param("entryId") String entryId, @Param("gNo") Long gNo);

    /**
     * 查询报关表头表体
     *
     * @param id 报关表头表体主键
     * @return 报关表头表体
     */
    public EnEntryHeadListShangjian selectEnEntryHeadListShangjianById(String id);

    /**
     * 查询报关表头表体列表
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 报关表头表体集合
     */
    public List<EnEntryHeadListShangjian> selectEnEntryHeadListShangjianList(EnEntryHeadListShangjian enEntryHeadListShangjian);

    /**
     * 新增报关表头表体
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    public int insertEnEntryHeadListShangjian(EnEntryHeadListShangjian enEntryHeadListShangjian);

    /**
     * 修改报关表头表体
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    public int updateEnEntryHeadListShangjian(EnEntryHeadListShangjian enEntryHeadListShangjian);

    /**
     * 删除报关表头表体
     *
     * @param id 报关表头表体主键
     * @return 结果
     */
    public int deleteEnEntryHeadListShangjianById(String id);

    /**
     * 批量删除报关表头表体
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEnEntryHeadListShangjianByIds(String[] ids);
}
