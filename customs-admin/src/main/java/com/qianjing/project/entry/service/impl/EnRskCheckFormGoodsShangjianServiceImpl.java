package com.qianjing.project.entry.service.impl;

import java.util.List;
import com.qianjing.common.constant.Constants;
import com.qianjing.common.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.entry.mapper.EnRskCheckFormGoodsShangjianMapper;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.service.IEnRskCheckFormGoodsShangjianService;

/**
 * 查验结果反馈Service业务层处理
 *
 * @author zhangzy
 * @date 2022-09-16
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EnRskCheckFormGoodsShangjianServiceImpl implements IEnRskCheckFormGoodsShangjianService {

    private static final Logger log = LoggerFactory.getLogger(EnRskCheckFormGoodsShangjianServiceImpl.class);

    private final EnRskCheckFormGoodsShangjianMapper enRskCheckFormGoodsShangjianMapper;

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    @Override
    public EnRskCheckFormGoodsShangjian checkEntryIdGnoUnique(String entryId, Long gNo) {
        return enRskCheckFormGoodsShangjianMapper.checkEntryIdGnoUnique(entryId, gNo);
    }

    /**
     * 查询查验结果反馈
     *
     * @param id 查验结果反馈主键
     * @return 查验结果反馈
     */
    @Override
    public EnRskCheckFormGoodsShangjian selectRskCheckFormGoodsShangjianById(String id)
    {
        return enRskCheckFormGoodsShangjianMapper.selectRskCheckFormGoodsShangjianById(id);
    }

    /**
     * 查询查验结果反馈列表
     *
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 查验结果反馈
     */
    @Override
    public List<EnRskCheckFormGoodsShangjian> selectRskCheckFormGoodsShangjianList(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian)
    {
        return enRskCheckFormGoodsShangjianMapper.selectRskCheckFormGoodsShangjianList(enRskCheckFormGoodsShangjian);
    }

    /**
     * 新增查验结果反馈
     *
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    @Override
    public int insertRskCheckFormGoodsShangjian(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian)
    {
        enRskCheckFormGoodsShangjian.setCreateTime(DateUtils.getNowDate());
        return enRskCheckFormGoodsShangjianMapper.insertRskCheckFormGoodsShangjian(enRskCheckFormGoodsShangjian);
    }

    /**
     * 修改查验结果反馈
     *
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    @Override
    public int updateRskCheckFormGoodsShangjian(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian)
    {
        return enRskCheckFormGoodsShangjianMapper.updateRskCheckFormGoodsShangjian(enRskCheckFormGoodsShangjian);
    }

    /**
     * 批量删除查验结果反馈
     *
     * @param ids 需要删除的查验结果反馈主键
     * @return 结果
     */
    @Override
    public int deleteRskCheckFormGoodsShangjianByIds(String[] ids)
    {
        return enRskCheckFormGoodsShangjianMapper.deleteRskCheckFormGoodsShangjianByIds(ids);
    }

    /**
     * 删除查验结果反馈信息
     *
     * @param id 查验结果反馈主键
     * @return 结果
     */
    @Override
    public int deleteRskCheckFormGoodsShangjianById(String id)
    {
        return enRskCheckFormGoodsShangjianMapper.deleteRskCheckFormGoodsShangjianById(id);
    }

    /**
     * 导入查验结果反馈数据
     *
     * @param enRskCheckFormGoodsShangjianList rskCheckFormGoodsShangjianList 查验结果反馈信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importRskCheckFormGoodsShangjian(List<EnRskCheckFormGoodsShangjian> enRskCheckFormGoodsShangjianList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(enRskCheckFormGoodsShangjianList) || enRskCheckFormGoodsShangjianList.size() == 0) {
            throw new ServiceException("导入查验结果反馈数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian : enRskCheckFormGoodsShangjianList){
            try {
                // 验证是否存在
                if (true) {
                    enRskCheckFormGoodsShangjian.setCreateBy(operName);
                    this.insertRskCheckFormGoodsShangjian(enRskCheckFormGoodsShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    enRskCheckFormGoodsShangjian.setUpdateBy(operName);
                    this.updateRskCheckFormGoodsShangjian(enRskCheckFormGoodsShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
