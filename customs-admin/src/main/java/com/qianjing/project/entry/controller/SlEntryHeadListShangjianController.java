package com.qianjing.project.entry.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.entry.domain.SlEntryHeadListShangjian;
import com.qianjing.project.entry.service.ISlEntryHeadListShangjianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 报关表头表体Controller
 * 
 * @author qianjing
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/entry/slaveHeadList")
public class SlEntryHeadListShangjianController extends BaseController {

    @Autowired
    private ISlEntryHeadListShangjianService slEntryHeadListShangjianService;

    /**
     * 查询报关表头表体列表
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveHeadList:list')")
    @GetMapping("/list")
    public TableDataInfo list(SlEntryHeadListShangjian slEntryHeadListShangjian)
    {
        startPage();
        List<SlEntryHeadListShangjian> list = slEntryHeadListShangjianService.selectSlEntryHeadListShangjianList(slEntryHeadListShangjian);
        return getDataTable(list);
    }

    /**
     * 导出报关表头表体列表
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveHeadList:export')")
    @Log(title = "报关表头表体", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SlEntryHeadListShangjian slEntryHeadListShangjian)
    {
        List<SlEntryHeadListShangjian> list = slEntryHeadListShangjianService.selectSlEntryHeadListShangjianList(slEntryHeadListShangjian);
        ExcelUtil<SlEntryHeadListShangjian> util = new ExcelUtil<SlEntryHeadListShangjian>(SlEntryHeadListShangjian.class);
        return util.exportExcel(list, "报关表头表体数据");
    }

    /**
     * 导入报关表头表体列表
     */
    @Log(title = "报关表头表体", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('entry:slaveHeadList:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SlEntryHeadListShangjian> util = new ExcelUtil<SlEntryHeadListShangjian>(SlEntryHeadListShangjian.class);
        List<SlEntryHeadListShangjian> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = slEntryHeadListShangjianService.importSlEntryHeadListShangjian(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载报关表头表体导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SlEntryHeadListShangjian> util = new ExcelUtil<SlEntryHeadListShangjian>(SlEntryHeadListShangjian.class);
        util.importTemplateExcel(response, "报关表头表体数据");
    }

    /**
     * 获取报关表头表体详细信息
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveHeadList:query')")
    @GetMapping(value = "/{entryId}")
    public AjaxResult getInfo(@PathVariable("entryId") String entryId)
    {
        return AjaxResult.success(slEntryHeadListShangjianService.selectSlEntryHeadListShangjianByEntryId(entryId));
    }

    /**
     * 新增报关表头表体
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveHeadList:add')")
    @Log(title = "报关表头表体", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SlEntryHeadListShangjian slEntryHeadListShangjian)
    {
        return toAjax(slEntryHeadListShangjianService.insertSlEntryHeadListShangjian(slEntryHeadListShangjian));
    }

    /**
     * 修改报关表头表体
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveHeadList:edit')")
    @Log(title = "报关表头表体", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SlEntryHeadListShangjian slEntryHeadListShangjian)
    {
        return toAjax(slEntryHeadListShangjianService.updateSlEntryHeadListShangjian(slEntryHeadListShangjian));
    }

    /**
     * 删除报关表头表体
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveHeadList:remove')")
    @Log(title = "报关表头表体", businessType = BusinessType.DELETE)
	@DeleteMapping("/{entryIds}")
    public AjaxResult remove(@PathVariable String[] entryIds)
    {
        return toAjax(slEntryHeadListShangjianService.deleteSlEntryHeadListShangjianByEntryIds(entryIds));
    }
}
