package com.qianjing.project.entry.mapper;

import java.util.List;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;
import org.apache.ibatis.annotations.Param;

/**
 * 查验结果反馈Mapper接口
 * 
 * @author zhangzy
 * @date 2022-09-16
 */
public interface EnRskCheckFormGoodsShangjianMapper
{

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    public EnRskCheckFormGoodsShangjian checkEntryIdGnoUnique(@Param("entryId") String entryId, @Param("gNo") Long gNo);

    /**
     * 查询查验结果反馈
     * 
     * @param id 查验结果反馈主键
     * @return 查验结果反馈
     */
    public EnRskCheckFormGoodsShangjian selectRskCheckFormGoodsShangjianById(String id);

    /**
     * 查询查验结果反馈列表
     * 
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 查验结果反馈集合
     */
    public List<EnRskCheckFormGoodsShangjian> selectRskCheckFormGoodsShangjianList(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    /**
     * 新增查验结果反馈
     * 
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    public int insertRskCheckFormGoodsShangjian(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    /**
     * 修改查验结果反馈
     * 
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    public int updateRskCheckFormGoodsShangjian(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    /**
     * 删除查验结果反馈
     * 
     * @param id 查验结果反馈主键
     * @return 结果
     */
    public int deleteRskCheckFormGoodsShangjianById(String id);

    /**
     * 批量删除查验结果反馈
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRskCheckFormGoodsShangjianByIds(String[] ids);
}
