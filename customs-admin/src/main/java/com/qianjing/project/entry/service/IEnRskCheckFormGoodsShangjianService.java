package com.qianjing.project.entry.service;

import java.util.List;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;

/**
 * 查验结果反馈Service接口
 * 
 * @author zhangzy
 * @date 2022-09-16
 */
public interface IEnRskCheckFormGoodsShangjianService
{

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    public EnRskCheckFormGoodsShangjian checkEntryIdGnoUnique(String entryId, Long gNo);

    /**
     * 查询查验结果反馈
     * 
     * @param id 查验结果反馈主键
     * @return 查验结果反馈
     */
    public EnRskCheckFormGoodsShangjian selectRskCheckFormGoodsShangjianById(String id);

    /**
     * 查询查验结果反馈列表
     * 
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 查验结果反馈集合
     */
    public List<EnRskCheckFormGoodsShangjian> selectRskCheckFormGoodsShangjianList(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    /**
     * 新增查验结果反馈
     * 
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    public int insertRskCheckFormGoodsShangjian(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    /**
     * 修改查验结果反馈
     * 
     * @param enRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    public int updateRskCheckFormGoodsShangjian(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    /**
     * 批量删除查验结果反馈
     * 
     * @param ids 需要删除的查验结果反馈主键集合
     * @return 结果
     */
    public int deleteRskCheckFormGoodsShangjianByIds(String[] ids);

    /**
     * 删除查验结果反馈信息
     * 
     * @param id 查验结果反馈主键
     * @return 结果
     */
    public int deleteRskCheckFormGoodsShangjianById(String id);

    /**
     * 导入查验结果反馈信息
     *
     * @param enRskCheckFormGoodsShangjianList 查验结果反馈信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importRskCheckFormGoodsShangjian(List<EnRskCheckFormGoodsShangjian> enRskCheckFormGoodsShangjianList, Boolean isUpdateSupport, String operName);
}
