package com.qianjing.project.entry.mapper;

import java.util.List;
import com.qianjing.project.entry.domain.SlRskCheckFormGoodsShangjian;

public interface SlRskCheckFormGoodsShangjianMapper {

    SlRskCheckFormGoodsShangjian selectSlRskCheckFormGoodsShangjianById(String id);

    List<SlRskCheckFormGoodsShangjian> selectSlRskCheckFormGoodsShangjianList(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian);

    int insertSlRskCheckFormGoodsShangjian(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian);

    int updateSlRskCheckFormGoodsShangjian(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian);

    int deleteSlRskCheckFormGoodsShangjianById(String id);

    int deleteSlRskCheckFormGoodsShangjianByIds(String[] ids);
}
