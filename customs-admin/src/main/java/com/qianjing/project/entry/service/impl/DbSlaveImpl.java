package com.qianjing.project.entry.service.impl;

import com.qianjing.framework.aspectj.lang.annotation.DataSource;
import com.qianjing.framework.aspectj.lang.enums.DataSourceType;
import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.domain.FieldPoolDO;
import com.qianjing.project.entry.mapper.DbSlaveMapper;
import com.qianjing.project.entry.service.IDbSlaveService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 报关表头表体Service业务层处理
 * 
 * @author qianjing
 * @date 2022-09-16
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DbSlaveImpl implements IDbSlaveService {

    private static final Logger log = LoggerFactory.getLogger(DbSlaveImpl.class);

    private final DbSlaveMapper dbSlaveMapper;

    @Override
    public List<EnEntryHeadListShangjian> selectEntryHeadListShangjianList(EnEntryHeadListShangjian enEntryHeadListShangjian) {
        return dbSlaveMapper.selectEntryHeadListShangjianList(enEntryHeadListShangjian);
    }

    @Override
    public List<EnRskCheckFormGoodsShangjian> selectRskCheckFormGoodsShangjianList(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian){
        return dbSlaveMapper.selectRskCheckFormGoodsShangjianList(enRskCheckFormGoodsShangjian);
    }

    @Override
    public List<FieldPoolDO> selectFieldPoolDtoList(List<DbUnqied> dbUnqiedList) {
        return dbSlaveMapper.selectFieldPoolDtoList(dbUnqiedList);
    }
    @Override
    public List<FieldPoolDO> selectFieldPoolDtoList2(List<String> hsCodeList, List<String> ciqCodeList, List<String> productCharCodeList) {
        return dbSlaveMapper.selectFieldPoolDtoList2(hsCodeList,ciqCodeList,productCharCodeList);
    }

}
