package com.qianjing.project.entry.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 查验结果反馈对象 rsk_check_form_goods_shangjian
 * 
 * @author zhangzy
 * @date 2023-03-21
 */
public class SlRskCheckFormGoodsShangjian extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private String id;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyTime;

    /** 环节类型（B前置 M口岸 M1目的地 A事后） */
    @Excel(name = "环节类型", readConverterExp = "B=前置,M=口岸,M1=目的地,A=事后")
    private String checkState;

    /** 现场工作开始的时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "现场工作开始的时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkStartTime;

    /** 现场工作结束的时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "现场工作结束的时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkEndTime;

    /** 查验要求（0表示未选中 1辐射探测 2预防性检疫处理 3检查病媒/有害生物 4动物临床检查 5检查残留剂 6检查车体/箱体 7检查包装 8核对品名 9检查标签/标识 10核对规格/型号 11核对产终地 12核对数量 13核对重量 14检查侵权 15检查外观品质 16检查夹藏/夹杂 17检查温度 18取样送检） */
    @Excel(name = "查验要求", readConverterExp = "0=表示未选中,1=辐射探测,2=预防性检疫处理,3=检查病媒/有害生物,4=动物临床检查,5=检查残留剂,6=检查车体/箱体,7=检查包装,8=核对品名,9=检查标签/标识,1=0核对规格/型号,1=1核对产终地,1=2核对数量,1=3核对重量,1=4检查侵权,1=5检查外观品质,1=6检查夹藏/夹杂,1=7检查温度,1=8取样送检")
    private String checkRequestTotal;

    /** 查验结果（0未见异常 1发现异常 2无法确定） */
    @Excel(name = "查验结果", readConverterExp = "0=未见异常,1=发现异常,2=无法确定")
    private String checkResultTotal;

    /** 查验处理意见（00建议放行 01待处理 02取样送检 03检疫处理 04技术整改 05隔离检疫 06指定场所存放加工监管 07移后续监管部门 08移综合业务部门 09移法规部门 10移缉私部门） */
    @Excel(name = "查验处理意见", readConverterExp = "0=0建议放行,0=1待处理,0=2取样送检,0=3检疫处理,0=4技术整改,0=5隔离检疫,0=6指定场所存放加工监管,0=7移后续监管部门,0=8移综合业务部门,0=9移法规部门,1=0移缉私部门")
    private String checkProcIdea;

    /** 查验处理结果（01放行 02改单放行 03处罚后放行 04担保放行 05补证放行 06补税放行 07补证补税放行 08隔离检疫后放行 09技术整改后放行 10检疫处理后放） */
    @Excel(name = "查验处理结果", readConverterExp = "0=1放行,0=2改单放行,0=3处罚后放行,0=4担保放行,0=5补证放行,0=6补税放行,0=7补证补税放行,0=8隔离检疫后放行,0=9技术整改后放行,1=0检疫处理后放")
    private String checkProcResult;

    /** 报关单编号 */
    @Excel(name = "报关单编号")
    private String entryId;

    /** 项号 */
    @Excel(name = "项号")
    private Long gNo;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String codeTs;

    /** 是否布控命中（0未命中 1命中） */
    @Excel(name = "是否布控命中", readConverterExp = "0=未命中,1=命中")
    private String isHit;

    /** 商品项查验结果（0异常 1无异常 2无法确认） */
    @Excel(name = "商品项查验结果", readConverterExp = "0=异常,1=无异常,2=无法确认")
    private String checkResult;

    /** 总署整体查验结果（0未见异常 1发现异常 2无该项操作） */
    @Excel(name = "总署整体查验结果", readConverterExp = "0=未见异常,1=发现异常,2=无该项操作")
    private String checkRequestTotalResult;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setModifyTime(Date modifyTime)
    {
        this.modifyTime = modifyTime;
    }

    public Date getModifyTime() 
    {
        return modifyTime;
    }
    public void setCheckState(String checkState) 
    {
        this.checkState = checkState;
    }

    public String getCheckState() 
    {
        return checkState;
    }
    public void setCheckStartTime(Date checkStartTime) 
    {
        this.checkStartTime = checkStartTime;
    }

    public Date getCheckStartTime() 
    {
        return checkStartTime;
    }
    public void setCheckEndTime(Date checkEndTime) 
    {
        this.checkEndTime = checkEndTime;
    }

    public Date getCheckEndTime() 
    {
        return checkEndTime;
    }
    public void setCheckRequestTotal(String checkRequestTotal) 
    {
        this.checkRequestTotal = checkRequestTotal;
    }

    public String getCheckRequestTotal() 
    {
        return checkRequestTotal;
    }
    public void setCheckResultTotal(String checkResultTotal) 
    {
        this.checkResultTotal = checkResultTotal;
    }

    public String getCheckResultTotal() 
    {
        return checkResultTotal;
    }
    public void setCheckProcIdea(String checkProcIdea) 
    {
        this.checkProcIdea = checkProcIdea;
    }

    public String getCheckProcIdea() 
    {
        return checkProcIdea;
    }
    public void setCheckProcResult(String checkProcResult) 
    {
        this.checkProcResult = checkProcResult;
    }

    public String getCheckProcResult() 
    {
        return checkProcResult;
    }
    public void setEntryId(String entryId) 
    {
        this.entryId = entryId;
    }

    public String getEntryId() 
    {
        return entryId;
    }
    public void setgNo(Long gNo) 
    {
        this.gNo = gNo;
    }

    public Long getgNo() 
    {
        return gNo;
    }
    public void setCodeTs(String codeTs) 
    {
        this.codeTs = codeTs;
    }

    public String getCodeTs() 
    {
        return codeTs;
    }
    public void setIsHit(String isHit) 
    {
        this.isHit = isHit;
    }

    public String getIsHit() 
    {
        return isHit;
    }
    public void setCheckResult(String checkResult) 
    {
        this.checkResult = checkResult;
    }

    public String getCheckResult() 
    {
        return checkResult;
    }
    public void setCheckRequestTotalResult(String checkRequestTotalResult) 
    {
        this.checkRequestTotalResult = checkRequestTotalResult;
    }

    public String getCheckRequestTotalResult() 
    {
        return checkRequestTotalResult;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("modifyTime", getModifyTime())
            .append("checkState", getCheckState())
            .append("checkStartTime", getCheckStartTime())
            .append("checkEndTime", getCheckEndTime())
            .append("checkRequestTotal", getCheckRequestTotal())
            .append("checkResultTotal", getCheckResultTotal())
            .append("checkProcIdea", getCheckProcIdea())
            .append("checkProcResult", getCheckProcResult())
            .append("entryId", getEntryId())
            .append("gNo", getgNo())
            .append("codeTs", getCodeTs())
            .append("isHit", getIsHit())
            .append("checkResult", getCheckResult())
            .append("checkRequestTotalResult", getCheckRequestTotalResult())
            .toString();
    }
}
