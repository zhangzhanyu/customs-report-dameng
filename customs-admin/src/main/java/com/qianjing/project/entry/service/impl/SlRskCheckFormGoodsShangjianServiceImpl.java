package com.qianjing.project.entry.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.framework.aspectj.lang.annotation.DataSource;
import com.qianjing.framework.aspectj.lang.enums.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.entry.mapper.SlRskCheckFormGoodsShangjianMapper;
import com.qianjing.project.entry.domain.SlRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.service.ISlRskCheckFormGoodsShangjianService;

/**
 * 查验结果反馈Service业务层处理
 * 
 * @author zhangzy
 * @date 2023-03-21
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class SlRskCheckFormGoodsShangjianServiceImpl implements ISlRskCheckFormGoodsShangjianService 
{
    private static final Logger log = LoggerFactory.getLogger(SlRskCheckFormGoodsShangjianServiceImpl.class);

    @Autowired
    private SlRskCheckFormGoodsShangjianMapper slRskCheckFormGoodsShangjianMapper;

    /**
     * 查询查验结果反馈
     * 
     * @param id 查验结果反馈主键
     * @return 查验结果反馈
     */
    @Override
    public SlRskCheckFormGoodsShangjian selectSlRskCheckFormGoodsShangjianById(String id)
    {
        return slRskCheckFormGoodsShangjianMapper.selectSlRskCheckFormGoodsShangjianById(id);
    }

    /**
     * 查询查验结果反馈列表
     * 
     * @param slRskCheckFormGoodsShangjian 查验结果反馈
     * @return 查验结果反馈
     */
    @Override
    public List<SlRskCheckFormGoodsShangjian> selectSlRskCheckFormGoodsShangjianList(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian)
    {
        return slRskCheckFormGoodsShangjianMapper.selectSlRskCheckFormGoodsShangjianList(slRskCheckFormGoodsShangjian);
    }

    /**
     * 新增查验结果反馈
     * 
     * @param slRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    @Override
    public int insertSlRskCheckFormGoodsShangjian(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian)
    {
        slRskCheckFormGoodsShangjian.setCreateTime(DateUtils.getNowDate());
        return slRskCheckFormGoodsShangjianMapper.insertSlRskCheckFormGoodsShangjian(slRskCheckFormGoodsShangjian);
    }

    /**
     * 修改查验结果反馈
     * 
     * @param slRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    @Override
    public int updateSlRskCheckFormGoodsShangjian(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian)
    {
        return slRskCheckFormGoodsShangjianMapper.updateSlRskCheckFormGoodsShangjian(slRskCheckFormGoodsShangjian);
    }

    /**
     * 批量删除查验结果反馈
     * 
     * @param ids 需要删除的查验结果反馈主键
     * @return 结果
     */
    @Override
    public int deleteSlRskCheckFormGoodsShangjianByIds(String[] ids)
    {
        return slRskCheckFormGoodsShangjianMapper.deleteSlRskCheckFormGoodsShangjianByIds(ids);
    }

    /**
     * 删除查验结果反馈信息
     * 
     * @param id 查验结果反馈主键
     * @return 结果
     */
    @Override
    public int deleteSlRskCheckFormGoodsShangjianById(String id)
    {
        return slRskCheckFormGoodsShangjianMapper.deleteSlRskCheckFormGoodsShangjianById(id);
    }

    /**
     * 导入查验结果反馈数据
     *
     * @param slRskCheckFormGoodsShangjianList slRskCheckFormGoodsShangjianList 查验结果反馈信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSlRskCheckFormGoodsShangjian(List<SlRskCheckFormGoodsShangjian> slRskCheckFormGoodsShangjianList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(slRskCheckFormGoodsShangjianList) || slRskCheckFormGoodsShangjianList.size() == 0) {
            throw new ServiceException("导入查验结果反馈数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian : slRskCheckFormGoodsShangjianList){
            try {
                // 验证是否存在
                if (true) {
                    slRskCheckFormGoodsShangjian.setCreateBy(operName);
                    this.insertSlRskCheckFormGoodsShangjian(slRskCheckFormGoodsShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    slRskCheckFormGoodsShangjian.setUpdateBy(operName);
                    this.updateSlRskCheckFormGoodsShangjian(slRskCheckFormGoodsShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
