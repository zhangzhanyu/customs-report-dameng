package com.qianjing.project.entry.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 查验结果反馈对象 rsk_check_form_goods_shangjian
 *
 * @author zhangzy
 * @date 2022-09-16
 */
public class EnRskCheckFormGoodsShangjian extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private String id;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyTime;

    /** 环节类型（B前置 M口岸 M1目的地 A事后） */
    @Excel(name = "环节类型", readConverterExp = "B=前置,M=口岸,M1=目的地,A=事后")
    private String checkState;

    /** 查验开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "查验开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkStartTime;

    /** 查验结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "查验结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkEndTime;

    /** 查验要求（0表示未选中 1辐射探测 2预防性检疫处理 3检查病媒/有害生物 4动物临床检查 5检查残留剂 6检查车体/箱体 7检查包装 8核对品名 9检查标签/标识 10核对规格/型号 11核对产终地 12核对数量 13核对重量 14检查侵权 15检查外观品质 16检查夹藏/夹杂 17检查温度 18取样送检） */
    @Excel(name = "查验要求", readConverterExp = "0=表示未选中,1=辐射探测,2=预防性检疫处理,3=检查病媒/有害生物,4=动物临床检查,5=检查残留剂,6=检查车体/箱体,7=检查包装,8=核对品名,9=检查标签/标识,10=核对规格/型号,11=核对产终地,12=核对数量,13=核对重量,14=检查侵权,15=检查外观品质,16=检查夹藏/夹杂,17=检查温度,18=取样送检")
    private String checkRequestTotal;

    /** 查验结果  (0未见异常 1发现异常 2无法确定) */
    @Excel(name = "查验结果", readConverterExp = "0=未见异常,1=发现异常,2=无法确定")
    private String checkResultTotal;

    /** 查验处理意见（00建议放行 01待处理 02取样送检 03检疫处理 04技术整改 05隔离检疫 06指定场所存放加工监管 07移后续监管部门 08移综合业务部门 09移法规部门 10移缉私部门） */
    @Excel(name = "查验处理意见", readConverterExp = "00=建议放行,01=待处理,02=取样送检,03=检疫处理,04=技术整改,05=隔离检疫,06=指定场所存放加工监管,07=移后续监管部门,08=移综合业务部门,09=移法规部门,10=移缉私部门")
    private String checkProcIdea;

    /** 查验处理结果（01放行 02改单放行 03处罚后放行 04担保放行 05补证放行 06补税放行 07补证补税放行 08隔离检疫后放行 09技术整改后放行 10检疫处理后放） */
    @Excel(name = "查验处理结果", readConverterExp = "01=放行,02=改单放行,03=处罚后放行,04=担保放行,05=补证放行,06=补税放行,07=补证补税放行,08=隔离检疫后放行,09=技术整改后放行,10=检疫处理后放")
    private String checkProcResult;

    /** 报关单编号 */
    @Excel(name = "报关单编号")
    private String entryId;

    /** 项号 */
    @Excel(name = "项号")
    private Long gNo;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String codeTs;

    /** 是否布控命中（0未命中 1命中） */
    @Excel(name = "是否布控命中", readConverterExp = "0=未命中,1=命中")
    private String isHit;

    /** 商品项查验结果（0异常 1无异常 2无法确认） */
    @Excel(name = "商品项查验结果", readConverterExp = "0=异常,1=无异常,2=无法确认")
    private String checkResult;

    /** 总署整体查验结果（0未见异常 1发现异常 2无该项操作） */
    @Excel(name = "总署整体查验结果", readConverterExp = "0=未见异常,1=发现异常,2=无该项操作")
    private String checkRequestTotalResult;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }

    public Date getCheckStartTime() {
        return checkStartTime;
    }

    public void setCheckStartTime(Date checkStartTime) {
        this.checkStartTime = checkStartTime;
    }

    public Date getCheckEndTime() {
        return checkEndTime;
    }

    public void setCheckEndTime(Date checkEndTime) {
        this.checkEndTime = checkEndTime;
    }

    public String getCheckRequestTotal() {
        return checkRequestTotal;
    }

    public void setCheckRequestTotal(String checkRequestTotal) {
        this.checkRequestTotal = checkRequestTotal;
    }

    public String getCheckResultTotal() {
        return checkResultTotal;
    }

    public void setCheckResultTotal(String checkResultTotal) {
        this.checkResultTotal = checkResultTotal;
    }

    public String getCheckProcIdea() {
        return checkProcIdea;
    }

    public void setCheckProcIdea(String checkProcIdea) {
        this.checkProcIdea = checkProcIdea;
    }

    public String getCheckProcResult() {
        return checkProcResult;
    }

    public void setCheckProcResult(String checkProcResult) {
        this.checkProcResult = checkProcResult;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public Long getgNo() {
        return gNo;
    }

    public void setgNo(Long gNo) {
        this.gNo = gNo;
    }

    public String getCodeTs() {
        return codeTs;
    }

    public void setCodeTs(String codeTs) {
        this.codeTs = codeTs;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getCheckRequestTotalResult() {
        return checkRequestTotalResult;
    }

    public void setCheckRequestTotalResult(String checkRequestTotalResult) {
        this.checkRequestTotalResult = checkRequestTotalResult;
    }
}
