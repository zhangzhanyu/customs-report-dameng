package com.qianjing.project.entry.service.impl;

import java.util.List;

import com.qianjing.framework.aspectj.lang.annotation.DataSource;
import com.qianjing.framework.aspectj.lang.enums.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.entry.mapper.SlEntryHeadListShangjianMapper;
import com.qianjing.project.entry.domain.SlEntryHeadListShangjian;
import com.qianjing.project.entry.service.ISlEntryHeadListShangjianService;

/**
 * 报关表头表体Service业务层处理
 * 
 * @author qianjing
 * @date 2023-03-21
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class SlEntryHeadListShangjianServiceImpl implements ISlEntryHeadListShangjianService {

    private static final Logger log = LoggerFactory.getLogger(SlEntryHeadListShangjianServiceImpl.class);

    @Autowired
    private SlEntryHeadListShangjianMapper slEntryHeadListShangjianMapper;

    /**
     * 查询报关表头表体
     * 
     * @param entryId 报关表头表体主键
     * @return 报关表头表体
     */
    @Override
    public SlEntryHeadListShangjian selectSlEntryHeadListShangjianByEntryId(String entryId)
    {
        return slEntryHeadListShangjianMapper.selectSlEntryHeadListShangjianByEntryId(entryId);
    }

    /**
     * 查询报关表头表体列表
     * 
     * @param slEntryHeadListShangjian 报关表头表体
     * @return 报关表头表体
     */
    @Override
    public List<SlEntryHeadListShangjian> selectSlEntryHeadListShangjianList(SlEntryHeadListShangjian slEntryHeadListShangjian)
    {
        return slEntryHeadListShangjianMapper.selectSlEntryHeadListShangjianList(slEntryHeadListShangjian);
    }

    /**
     * 新增报关表头表体
     * 
     * @param slEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    @Override
    public int insertSlEntryHeadListShangjian(SlEntryHeadListShangjian slEntryHeadListShangjian)
    {
        return slEntryHeadListShangjianMapper.insertSlEntryHeadListShangjian(slEntryHeadListShangjian);
    }

    /**
     * 修改报关表头表体
     * 
     * @param slEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    @Override
    public int updateSlEntryHeadListShangjian(SlEntryHeadListShangjian slEntryHeadListShangjian)
    {
        return slEntryHeadListShangjianMapper.updateSlEntryHeadListShangjian(slEntryHeadListShangjian);
    }

    /**
     * 批量删除报关表头表体
     * 
     * @param entryIds 需要删除的报关表头表体主键
     * @return 结果
     */
    @Override
    public int deleteSlEntryHeadListShangjianByEntryIds(String[] entryIds)
    {
        return slEntryHeadListShangjianMapper.deleteSlEntryHeadListShangjianByEntryIds(entryIds);
    }

    /**
     * 删除报关表头表体信息
     * 
     * @param entryId 报关表头表体主键
     * @return 结果
     */
    @Override
    public int deleteSlEntryHeadListShangjianByEntryId(String entryId)
    {
        return slEntryHeadListShangjianMapper.deleteSlEntryHeadListShangjianByEntryId(entryId);
    }

    /**
     * 导入报关表头表体数据
     *
     * @param slEntryHeadListShangjianList slEntryHeadListShangjianList 报关表头表体信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importSlEntryHeadListShangjian(List<SlEntryHeadListShangjian> slEntryHeadListShangjianList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(slEntryHeadListShangjianList) || slEntryHeadListShangjianList.size() == 0) {
            throw new ServiceException("导入报关表头表体数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (SlEntryHeadListShangjian slEntryHeadListShangjian : slEntryHeadListShangjianList){
            try {
                // 验证是否存在
                if (true) {
                    slEntryHeadListShangjian.setCreateBy(operName);
                    this.insertSlEntryHeadListShangjian(slEntryHeadListShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    slEntryHeadListShangjian.setUpdateBy(operName);
                    this.updateSlEntryHeadListShangjian(slEntryHeadListShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
