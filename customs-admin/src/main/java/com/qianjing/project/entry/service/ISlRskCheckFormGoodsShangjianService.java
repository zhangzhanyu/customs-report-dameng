package com.qianjing.project.entry.service;

import java.util.List;
import com.qianjing.project.entry.domain.SlRskCheckFormGoodsShangjian;

/**
 * 查验结果反馈Service接口
 * 
 * @author zhangzy
 * @date 2023-03-21
 */
public interface ISlRskCheckFormGoodsShangjianService 
{
    /**
     * 查询查验结果反馈
     * 
     * @param id 查验结果反馈主键
     * @return 查验结果反馈
     */
    public SlRskCheckFormGoodsShangjian selectSlRskCheckFormGoodsShangjianById(String id);

    /**
     * 查询查验结果反馈列表
     * 
     * @param slRskCheckFormGoodsShangjian 查验结果反馈
     * @return 查验结果反馈集合
     */
    public List<SlRskCheckFormGoodsShangjian> selectSlRskCheckFormGoodsShangjianList(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian);

    /**
     * 新增查验结果反馈
     * 
     * @param slRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    public int insertSlRskCheckFormGoodsShangjian(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian);

    /**
     * 修改查验结果反馈
     * 
     * @param slRskCheckFormGoodsShangjian 查验结果反馈
     * @return 结果
     */
    public int updateSlRskCheckFormGoodsShangjian(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian);

    /**
     * 批量删除查验结果反馈
     * 
     * @param ids 需要删除的查验结果反馈主键集合
     * @return 结果
     */
    public int deleteSlRskCheckFormGoodsShangjianByIds(String[] ids);

    /**
     * 删除查验结果反馈信息
     * 
     * @param id 查验结果反馈主键
     * @return 结果
     */
    public int deleteSlRskCheckFormGoodsShangjianById(String id);

    /**
     * 导入查验结果反馈信息
     *
     * @param slRskCheckFormGoodsShangjianList 查验结果反馈信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSlRskCheckFormGoodsShangjian(List<SlRskCheckFormGoodsShangjian> slRskCheckFormGoodsShangjianList, Boolean isUpdateSupport, String operName);
}
