package com.qianjing.project.entry.survey;


import com.qianjing.project.entry.domain.FieldPoolDO;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 项目勘察DO\VO转换类
 * @author loujinglong
 * @version $Id: ProjectSurveyConvert.java, v 0.1 2018年7月3日 下午5:51:21 loujinglong Exp $
 */
public class SusionDangerousChemicalConvert {

    private SusionDangerousChemicalConvert() {
    }

    public static SusionDangerousChemical convertDOToVO(FieldPoolDO fieldPoolDO) {
        SusionDangerousChemical susionDangerousChemical = new SusionDangerousChemical();
        if (fieldPoolDO == null) {
            return susionDangerousChemical;
        }
        susionDangerousChemical.setReleaseTime(fieldPoolDO.getRecCreateTime());
        return susionDangerousChemical;
    }

    public static List<SusionDangerousChemical> convertDOListToVOList(List<FieldPoolDO> fieldPoolDOList) {
        List<SusionDangerousChemical> susionDangerousChemicalList = new ArrayList<SusionDangerousChemical>();
        if (CollectionUtils.isEmpty(susionDangerousChemicalList)) {
            return susionDangerousChemicalList;
        }
        return fieldPoolDOList.parallelStream().map(SusionDangerousChemicalConvert::convertDOToVO)
                .collect(Collectors.toList());
    }
}
