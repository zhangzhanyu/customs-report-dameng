package com.qianjing.project.entry.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;
import com.qianjing.project.entry.service.IEnEntryHeadListShangjianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 报关表头表体Controller
 *
 * @author qianjing
 * @date 2023-03-09
 */
@RestController
@RequestMapping("/entry/masterHeadList")
public class EnEntryHeadListShangjianController extends BaseController
{
    @Autowired
    private IEnEntryHeadListShangjianService enEntryHeadListShangjianService;

    /**
     * 查询报关表头表体列表
     */
    @PreAuthorize("@ss.hasPermi('entry:masterHeadList:list')")
    @GetMapping("/list")
    public TableDataInfo list(EnEntryHeadListShangjian enEntryHeadListShangjian)
    {
        startPage();
        List<EnEntryHeadListShangjian> list = enEntryHeadListShangjianService.selectEnEntryHeadListShangjianList(enEntryHeadListShangjian);
        return getDataTable(list);
    }

    /**
     * 导出报关表头表体列表
     */
    @PreAuthorize("@ss.hasPermi('entry:masterHeadList:export')")
    @Log(title = "报关表头表体", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EnEntryHeadListShangjian enEntryHeadListShangjian)
    {
        List<EnEntryHeadListShangjian> list = enEntryHeadListShangjianService.selectEnEntryHeadListShangjianList(enEntryHeadListShangjian);
        ExcelUtil<EnEntryHeadListShangjian> util = new ExcelUtil<EnEntryHeadListShangjian>(EnEntryHeadListShangjian.class);
        return util.exportExcel(list, "报关表头表体数据");
    }

    /**
     * 导入报关表头表体列表
     */
    @Log(title = "报关表头表体", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('entry:masterHeadList:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<EnEntryHeadListShangjian> util = new ExcelUtil<EnEntryHeadListShangjian>(EnEntryHeadListShangjian.class);
        List<EnEntryHeadListShangjian> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = enEntryHeadListShangjianService.importEnEntryHeadListShangjian(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载报关表头表体导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<EnEntryHeadListShangjian> util = new ExcelUtil<EnEntryHeadListShangjian>(EnEntryHeadListShangjian.class);
        util.importTemplateExcel(response, "报关表头表体数据");
    }

    /**
     * 获取报关表头表体详细信息
     */
    @PreAuthorize("@ss.hasPermi('entry:masterHeadList:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(enEntryHeadListShangjianService.selectEnEntryHeadListShangjianById(id));
    }

    /**
     * 新增报关表头表体
     */
    @PreAuthorize("@ss.hasPermi('entry:masterHeadList:add')")
    @Log(title = "报关表头表体", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EnEntryHeadListShangjian enEntryHeadListShangjian)
    {
        return toAjax(enEntryHeadListShangjianService.insertEnEntryHeadListShangjian(enEntryHeadListShangjian));
    }

    /**
     * 修改报关表头表体
     */
    @PreAuthorize("@ss.hasPermi('entry:masterHeadList:edit')")
    @Log(title = "报关表头表体", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EnEntryHeadListShangjian enEntryHeadListShangjian)
    {
        return toAjax(enEntryHeadListShangjianService.updateEnEntryHeadListShangjian(enEntryHeadListShangjian));
    }

    /**
     * 删除报关表头表体
     */
    @PreAuthorize("@ss.hasPermi('entry:masterHeadList:remove')")
    @Log(title = "报关表头表体", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(enEntryHeadListShangjianService.deleteEnEntryHeadListShangjianByIds(ids));
    }
}
