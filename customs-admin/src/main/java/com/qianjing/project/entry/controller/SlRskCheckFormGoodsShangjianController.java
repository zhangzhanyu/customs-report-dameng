package com.qianjing.project.entry.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.entry.domain.SlRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.service.ISlRskCheckFormGoodsShangjianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 查验结果反馈Controller
 * 
 * @author zhangzy
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/entry/slaveCheckForm")
public class SlRskCheckFormGoodsShangjianController extends BaseController
{
    @Autowired
    private ISlRskCheckFormGoodsShangjianService slRskCheckFormGoodsShangjianService;

    /**
     * 查询查验结果反馈列表
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveCheckForm:list')")
    @GetMapping("/list")
    public TableDataInfo list(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian)
    {
        startPage();
        List<SlRskCheckFormGoodsShangjian> list = slRskCheckFormGoodsShangjianService.selectSlRskCheckFormGoodsShangjianList(slRskCheckFormGoodsShangjian);
        return getDataTable(list);
    }

    /**
     * 导出查验结果反馈列表
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveCheckForm:export')")
    @Log(title = "查验结果反馈", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian)
    {
        List<SlRskCheckFormGoodsShangjian> list = slRskCheckFormGoodsShangjianService.selectSlRskCheckFormGoodsShangjianList(slRskCheckFormGoodsShangjian);
        ExcelUtil<SlRskCheckFormGoodsShangjian> util = new ExcelUtil<SlRskCheckFormGoodsShangjian>(SlRskCheckFormGoodsShangjian.class);
        return util.exportExcel(list, "查验结果反馈数据");
    }

    /**
     * 导入查验结果反馈列表
     */
    @Log(title = "查验结果反馈", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('entry:slaveCheckForm:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<SlRskCheckFormGoodsShangjian> util = new ExcelUtil<SlRskCheckFormGoodsShangjian>(SlRskCheckFormGoodsShangjian.class);
        List<SlRskCheckFormGoodsShangjian> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = slRskCheckFormGoodsShangjianService.importSlRskCheckFormGoodsShangjian(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载查验结果反馈导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<SlRskCheckFormGoodsShangjian> util = new ExcelUtil<SlRskCheckFormGoodsShangjian>(SlRskCheckFormGoodsShangjian.class);
        util.importTemplateExcel(response, "查验结果反馈数据");
    }

    /**
     * 获取查验结果反馈详细信息
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveCheckForm:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(slRskCheckFormGoodsShangjianService.selectSlRskCheckFormGoodsShangjianById(id));
    }

    /**
     * 新增查验结果反馈
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveCheckForm:add')")
    @Log(title = "查验结果反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian)
    {
        return toAjax(slRskCheckFormGoodsShangjianService.insertSlRskCheckFormGoodsShangjian(slRskCheckFormGoodsShangjian));
    }

    /**
     * 修改查验结果反馈
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveCheckForm:edit')")
    @Log(title = "查验结果反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SlRskCheckFormGoodsShangjian slRskCheckFormGoodsShangjian)
    {
        return toAjax(slRskCheckFormGoodsShangjianService.updateSlRskCheckFormGoodsShangjian(slRskCheckFormGoodsShangjian));
    }

    /**
     * 删除查验结果反馈
     */
    @PreAuthorize("@ss.hasPermi('entry:slaveCheckForm:remove')")
    @Log(title = "查验结果反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(slRskCheckFormGoodsShangjianService.deleteSlRskCheckFormGoodsShangjianByIds(ids));
    }
}
