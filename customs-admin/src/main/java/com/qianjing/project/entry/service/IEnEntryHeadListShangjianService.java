package com.qianjing.project.entry.service;

import java.util.List;
import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;

/**
 * 报关表头表体Service接口
 *
 * @author qianjing
 * @date 2023-03-09
 */
public interface IEnEntryHeadListShangjianService
{

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    public EnEntryHeadListShangjian checkEntryIdGnoUnique(String entryId, Long gNo);

    /**
     * 查询报关表头表体
     *
     * @param id 报关表头表体主键
     * @return 报关表头表体
     */
    public EnEntryHeadListShangjian selectEnEntryHeadListShangjianById(String id);

    /**
     * 查询报关表头表体列表
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 报关表头表体集合
     */
    public List<EnEntryHeadListShangjian> selectEnEntryHeadListShangjianList(EnEntryHeadListShangjian enEntryHeadListShangjian);

    /**
     * 新增报关表头表体
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    public int insertEnEntryHeadListShangjian(EnEntryHeadListShangjian enEntryHeadListShangjian);

    /**
     * 修改报关表头表体
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    public int updateEnEntryHeadListShangjian(EnEntryHeadListShangjian enEntryHeadListShangjian);

    /**
     * 批量删除报关表头表体
     *
     * @param ids 需要删除的报关表头表体主键集合
     * @return 结果
     */
    public int deleteEnEntryHeadListShangjianByIds(String[] ids);

    /**
     * 删除报关表头表体信息
     *
     * @param id 报关表头表体主键
     * @return 结果
     */
    public int deleteEnEntryHeadListShangjianById(String id);

    /**
     * 导入报关表头表体信息
     *
     * @param enEntryHeadListShangjianList 报关表头表体信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importEnEntryHeadListShangjian(List<EnEntryHeadListShangjian> enEntryHeadListShangjianList, Boolean isUpdateSupport, String operName);
}
