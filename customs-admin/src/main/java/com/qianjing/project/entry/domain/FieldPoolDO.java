package com.qianjing.project.entry.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;

import java.util.Date;

public class FieldPoolDO {

    /**********************************************表头表体字段*****************************************/

    /** 海关编号 */
    @Excel(name = "海关编号")
    private String entryId;

    /** 检验检疫特殊业务标识 */
    @Excel(name = "检验检疫特殊业务标识")
    private String iqSpecialMark;

    /** 进出口标识（I进口 E出口） */
    @Excel(name = "进出口标识", readConverterExp = "I=进口,E=出口")
    private String iEFlag;

    /** 申报地海关 */
    @Excel(name = "申报地海关")
    private String declPort;

    /** 进出口关别 */
    @Excel(name = "进出口关别")
    private String iEPort;

    /** 申报日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申报日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dDate;

    /** 监管方式 */
    @Excel(name = "监管方式")
    private String tradeMode;

    /** 境内收发货人代码 */
    @Excel(name = "境内收发货人代码")
    private String consignCode;

    /** 境内收发货人名称 */
    @Excel(name = "境内收发货人名称")
    private String consignName;

    /** 境外收发货人名称(外文) */
    @Excel(name = "境外收发货人名称(外文)")
    private String frnConsignNameFn;

    /** 境外收发货人地址 */
    @Excel(name = "境外收发货人地址")
    private String frnConsignAddress;

    /** 消费使用单位代码\生产销售单位代码(统一社会信用代码) */
    @Excel(name = "消费使用单位代码\\生产销售单位代码(统一社会信用代码)")
    private String ownerCodeScc;

    /** 消费使用单位代码\生产销售单位代码 */
    @Excel(name = "消费使用单位代码\\生产销售单位代码")
    private String ownerCode;

    /** 消费使用单位名称\生产销售单位名称 */
    @Excel(name = "消费使用单位名称\\生产销售单位名称")
    private String ownerName;

    /** 申报单位代码 */
    @Excel(name = "申报单位代码")
    private String agentCode;

    /** 申报单位名称 */
    @Excel(name = "申报单位名称")
    private String agentName;

    /** 贸易国别(地区) */
    @Excel(name = "贸易国别(地区)")
    private String tradeCountry;

    /** 运输方式代码 */
    @Excel(name = "运输方式代码")
    private String trafMode;

    /** 启运国(地区)/运抵国(地区) */
    @Excel(name = "启运国(地区)/运抵国(地区)")
    private String frnIECountry;

    /** 入境口岸 */
    @Excel(name = "入境口岸")
    private String iqImportPort;

    /** 涉检标识（1涉检 0不涉检） */
    @Excel(name = "涉检标识", readConverterExp = "1=涉检,0=不涉检")
    private String eciqType;

    /** 记录创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recCreateTime;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recLastUpdateTime;

    /** 项号 */
    @Excel(name = "项号")
    private Long gNo;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String codeTs;

    /** 检验检疫编码 */
    @Excel(name = "检验检疫编码")
    private String iqCode;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String gName;

    /** 报检名称 */
    @Excel(name = "报检名称")
    private String iqGName;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String gModel;

    /** 法定第一数量 */
    @Excel(name = "法定第一数量")
    private Double qty1;

    /** 法定第一计量单位 */
    @Excel(name = "法定第一计量单位")
    private String unit1;

    /** 第二数量 */
    @Excel(name = "第二数量")
    private Double qty2;

    /** 第二计量单位 */
    @Excel(name = "第二计量单位")
    private String unit2;

    /** 统计人民币价 */
    @Excel(name = "统计人民币价")
    private Double rmbPrice;

    /** 统计美元价 */
    @Excel(name = "统计美元价")
    private Double usdPrice;

    /** 原产国(地区) */
    @Excel(name = "原产国(地区)")
    private String originCountry;

    /** 境外生产企业名称 */
    @Excel(name = "境外生产企业名称")
    private String frnProducerName;

    /** 货物型号 */
    @Excel(name = "货物型号")
    private String productModel;

    /** 货物品牌 */
    @Excel(name = "货物品牌")
    private String productBrand;

    /** UN编码 */
    @Excel(name = "UN编码")
    private String ungid;

    /** 非危险化学品 */
    @Excel(name = "非危险化学品")
    private String ungFlag;

    /** 危包规格 */
    @Excel(name = "危包规格")
    private String ungModel;

    /** 危包类别 */
    @Excel(name = "危包类别")
    private String ungClassify;

    /** 危险货物名称 */
    @Excel(name = "危险货物名称")
    private String ungGName;

    /** 货物属性代码 */
    @Excel(name = "货物属性代码")
    private String productCharCode;


    /**********************************************查管系统字段*****************************************/

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyTime;

    /** 环节类型（B前置 M口岸 M1目的地 A事后） */
    @Excel(name = "环节类型", readConverterExp = "B=前置,M=口岸,M1=目的地,A=事后")
    private String checkState;

    /** 查验开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "查验开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkStartTime;

    /** 查验结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "查验结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkEndTime;

    /** 查验要求（0表示未选中 1辐射探测 2预防性检疫处理 3检查病媒/有害生物 4动物临床检查 5检查残留剂 6检查车体/箱体 7检查包装 8核对品名 9检查标签/标识 10核对规格/型号 11核对产终地 12核对数量 13核对重量 14检查侵权 15检查外观品质 16检查夹藏/夹杂 17检查温度 18取样送检） */
    @Excel(name = "查验要求", readConverterExp = "0=表示未选中,1=辐射探测,2=预防性检疫处理,3=检查病媒/有害生物,4=动物临床检查,5=检查残留剂,6=检查车体/箱体,7=检查包装,8=核对品名,9=检查标签/标识,10=核对规格/型号,11=核对产终地,12=核对数量,13=核对重量,14=检查侵权,15=检查外观品质,16=检查夹藏/夹杂,17=检查温度,18=取样送检")
    private String checkRequestTotal;

    /** 查验结果  (0未见异常 1发现异常 2无法确定) */
    @Excel(name = "查验结果", readConverterExp = "0=未见异常,1=发现异常,2=无法确定")
    private String checkResultTotal;

    /** 查验处理意见（00建议放行 01待处理 02取样送检 03检疫处理 04技术整改 05隔离检疫 06指定场所存放加工监管 07移后续监管部门 08移综合业务部门 09移法规部门 10移缉私部门） */
    @Excel(name = "查验处理意见", readConverterExp = "00=建议放行,01=待处理,02=取样送检,03=检疫处理,04=技术整改,05=隔离检疫,06=指定场所存放加工监管,07=移后续监管部门,08=移综合业务部门,09=移法规部门,10=移缉私部门")
    private String checkProcIdea;

    /** 查验处理结果（01放行 02改单放行 03处罚后放行 04担保放行 05补证放行 06补税放行 07补证补税放行 08隔离检疫后放行 09技术整改后放行 10检疫处理后放） */
    @Excel(name = "查验处理结果", readConverterExp = "01=放行,02=改单放行,03=处罚后放行,04=担保放行,05=补证放行,06=补税放行,07=补证补税放行,08=隔离检疫后放行,09=技术整改后放行,10=检疫处理后放")
    private String checkProcResult;

    /** 是否布控命中（0未命中 1命中） */
    @Excel(name = "是否布控命中", readConverterExp = "0=未命中,1=命中")
    private String isHit;

    /** 商品项查验结果（0异常 1无异常 2无法确认） */
    @Excel(name = "商品项查验结果", readConverterExp = "0=异常,1=无异常,2=无法确认")
    private String checkResult;

    /** 总署整体查验结果（0未见异常 1发现异常 2无该项操作） */
    @Excel(name = "总署整体查验结果", readConverterExp = "0=未见异常,1=发现异常,2=无该项操作")
    private String checkRequestTotalResult;

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getIqSpecialMark() {
        return iqSpecialMark;
    }

    public void setIqSpecialMark(String iqSpecialMark) {
        this.iqSpecialMark = iqSpecialMark;
    }

    public String getiEFlag() {
        return iEFlag;
    }

    public void setiEFlag(String iEFlag) {
        this.iEFlag = iEFlag;
    }

    public String getDeclPort() {
        return declPort;
    }

    public void setDeclPort(String declPort) {
        this.declPort = declPort;
    }

    public String getiEPort() {
        return iEPort;
    }

    public void setiEPort(String iEPort) {
        this.iEPort = iEPort;
    }

    public Date getdDate() {
        return dDate;
    }

    public void setdDate(Date dDate) {
        this.dDate = dDate;
    }

    public String getTradeMode() {
        return tradeMode;
    }

    public void setTradeMode(String tradeMode) {
        this.tradeMode = tradeMode;
    }

    public String getConsignCode() {
        return consignCode;
    }

    public void setConsignCode(String consignCode) {
        this.consignCode = consignCode;
    }

    public String getConsignName() {
        return consignName;
    }

    public void setConsignName(String consignName) {
        this.consignName = consignName;
    }

    public String getFrnConsignNameFn() {
        return frnConsignNameFn;
    }

    public void setFrnConsignNameFn(String frnConsignNameFn) {
        this.frnConsignNameFn = frnConsignNameFn;
    }

    public String getFrnConsignAddress() {
        return frnConsignAddress;
    }

    public void setFrnConsignAddress(String frnConsignAddress) {
        this.frnConsignAddress = frnConsignAddress;
    }

    public String getOwnerCodeScc() {
        return ownerCodeScc;
    }

    public void setOwnerCodeScc(String ownerCodeScc) {
        this.ownerCodeScc = ownerCodeScc;
    }

    public String getOwnerCode() {
        return ownerCode;
    }

    public void setOwnerCode(String ownerCode) {
        this.ownerCode = ownerCode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getTradeCountry() {
        return tradeCountry;
    }

    public void setTradeCountry(String tradeCountry) {
        this.tradeCountry = tradeCountry;
    }

    public String getTrafMode() {
        return trafMode;
    }

    public void setTrafMode(String trafMode) {
        this.trafMode = trafMode;
    }

    public String getFrnIECountry() {
        return frnIECountry;
    }

    public void setFrnIECountry(String frnIECountry) {
        this.frnIECountry = frnIECountry;
    }

    public String getIqImportPort() {
        return iqImportPort;
    }

    public void setIqImportPort(String iqImportPort) {
        this.iqImportPort = iqImportPort;
    }

    public String getEciqType() {
        return eciqType;
    }

    public void setEciqType(String eciqType) {
        this.eciqType = eciqType;
    }

    public Date getRecCreateTime() {
        return recCreateTime;
    }

    public void setRecCreateTime(Date recCreateTime) {
        this.recCreateTime = recCreateTime;
    }

    public Date getRecLastUpdateTime() {
        return recLastUpdateTime;
    }

    public void setRecLastUpdateTime(Date recLastUpdateTime) {
        this.recLastUpdateTime = recLastUpdateTime;
    }

    public Long getgNo() {
        return gNo;
    }

    public void setgNo(Long gNo) {
        this.gNo = gNo;
    }

    public String getCodeTs() {
        return codeTs;
    }

    public void setCodeTs(String codeTs) {
        this.codeTs = codeTs;
    }

    public String getIqCode() {
        return iqCode;
    }

    public void setIqCode(String iqCode) {
        this.iqCode = iqCode;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getIqGName() {
        return iqGName;
    }

    public void setIqGName(String iqGName) {
        this.iqGName = iqGName;
    }

    public String getgModel() {
        return gModel;
    }

    public void setgModel(String gModel) {
        this.gModel = gModel;
    }

    public Double getQty1() {
        return qty1;
    }

    public void setQty1(Double qty1) {
        this.qty1 = qty1;
    }

    public String getUnit1() {
        return unit1;
    }

    public void setUnit1(String unit1) {
        this.unit1 = unit1;
    }

    public Double getQty2() {
        return qty2;
    }

    public void setQty2(Double qty2) {
        this.qty2 = qty2;
    }

    public String getUnit2() {
        return unit2;
    }

    public void setUnit2(String unit2) {
        this.unit2 = unit2;
    }

    public Double getRmbPrice() {
        return rmbPrice;
    }

    public void setRmbPrice(Double rmbPrice) {
        this.rmbPrice = rmbPrice;
    }

    public Double getUsdPrice() {
        return usdPrice;
    }

    public void setUsdPrice(Double usdPrice) {
        this.usdPrice = usdPrice;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getFrnProducerName() {
        return frnProducerName;
    }

    public void setFrnProducerName(String frnProducerName) {
        this.frnProducerName = frnProducerName;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getUngid() {
        return ungid;
    }

    public void setUngid(String ungid) {
        this.ungid = ungid;
    }

    public String getUngFlag() {
        return ungFlag;
    }

    public void setUngFlag(String ungFlag) {
        this.ungFlag = ungFlag;
    }

    public String getUngModel() {
        return ungModel;
    }

    public void setUngModel(String ungModel) {
        this.ungModel = ungModel;
    }

    public String getUngClassify() {
        return ungClassify;
    }

    public void setUngClassify(String ungClassify) {
        this.ungClassify = ungClassify;
    }

    public String getUngGName() {
        return ungGName;
    }

    public void setUngGName(String ungGName) {
        this.ungGName = ungGName;
    }

    public String getProductCharCode() {
        return productCharCode;
    }

    public void setProductCharCode(String productCharCode) {
        this.productCharCode = productCharCode;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public Date getCheckStartTime() {
        return checkStartTime;
    }

    public void setCheckStartTime(Date checkStartTime) {
        this.checkStartTime = checkStartTime;
    }

    public Date getCheckEndTime() {
        return checkEndTime;
    }

    public void setCheckEndTime(Date checkEndTime) {
        this.checkEndTime = checkEndTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }

    public String getCheckRequestTotal() {
        return checkRequestTotal;
    }

    public void setCheckRequestTotal(String checkRequestTotal) {
        this.checkRequestTotal = checkRequestTotal;
    }

    public String getCheckResultTotal() {
        return checkResultTotal;
    }

    public void setCheckResultTotal(String checkResultTotal) {
        this.checkResultTotal = checkResultTotal;
    }

    public String getCheckProcIdea() {
        return checkProcIdea;
    }

    public void setCheckProcIdea(String checkProcIdea) {
        this.checkProcIdea = checkProcIdea;
    }

    public String getCheckProcResult() {
        return checkProcResult;
    }

    public void setCheckProcResult(String checkProcResult) {
        this.checkProcResult = checkProcResult;
    }

    public String getIsHit() {
        return isHit;
    }

    public void setIsHit(String isHit) {
        this.isHit = isHit;
    }

    public String getCheckRequestTotalResult() {
        return checkRequestTotalResult;
    }

    public void setCheckRequestTotalResult(String checkRequestTotalResult) {
        this.checkRequestTotalResult = checkRequestTotalResult;
    }
}
