package com.qianjing.project.entry.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.service.IEnRskCheckFormGoodsShangjianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 查验结果反馈Controller
 * 
 * @author zhangzy
 * @date 2022-09-16
 */
@RestController
@RequestMapping("/entry/masterCheckForm")
public class EnRskCheckFormGoodsShangjianController extends BaseController {

    @Autowired
    private IEnRskCheckFormGoodsShangjianService rskCheckFormGoodsShangjianService;

    /**
     * 查询查验结果反馈列表
     */
    @PreAuthorize("@ss.hasPermi('entry:masterCheckForm:list')")
    @GetMapping("/list")
    public TableDataInfo list(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian)
    {
        startPage();
        List<EnRskCheckFormGoodsShangjian> list = rskCheckFormGoodsShangjianService.selectRskCheckFormGoodsShangjianList(enRskCheckFormGoodsShangjian);
        return getDataTable(list);
    }

    /**
     * 导出查验结果反馈列表
     */
    @PreAuthorize("@ss.hasPermi('entry:masterCheckForm:export')")
    @Log(title = "查验结果反馈", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian)
    {
        List<EnRskCheckFormGoodsShangjian> list = rskCheckFormGoodsShangjianService.selectRskCheckFormGoodsShangjianList(enRskCheckFormGoodsShangjian);
        ExcelUtil<EnRskCheckFormGoodsShangjian> util = new ExcelUtil<EnRskCheckFormGoodsShangjian>(EnRskCheckFormGoodsShangjian.class);
        return util.exportExcel(list, "查验结果反馈数据");
    }

    /**
     * 导入查验结果反馈列表
     */
    @Log(title = "查验结果反馈", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('entry:masterCheckForm:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<EnRskCheckFormGoodsShangjian> util = new ExcelUtil<EnRskCheckFormGoodsShangjian>(EnRskCheckFormGoodsShangjian.class);
        List<EnRskCheckFormGoodsShangjian> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = rskCheckFormGoodsShangjianService.importRskCheckFormGoodsShangjian(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载查验结果反馈导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<EnRskCheckFormGoodsShangjian> util = new ExcelUtil<EnRskCheckFormGoodsShangjian>(EnRskCheckFormGoodsShangjian.class);
        util.importTemplateExcel(response, "查验结果反馈数据");
    }

    /**
     * 获取查验结果反馈详细信息
     */
    @PreAuthorize("@ss.hasPermi('entry:masterCheckForm:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(rskCheckFormGoodsShangjianService.selectRskCheckFormGoodsShangjianById(id));
    }

    /**
     * 新增查验结果反馈
     */
    @PreAuthorize("@ss.hasPermi('entry:masterCheckForm:add')")
    @Log(title = "查验结果反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian)
    {
        return toAjax(rskCheckFormGoodsShangjianService.insertRskCheckFormGoodsShangjian(enRskCheckFormGoodsShangjian));
    }

    /**
     * 修改查验结果反馈
     */
    @PreAuthorize("@ss.hasPermi('entry:masterCheckForm:edit')")
    @Log(title = "查验结果反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian)
    {
        return toAjax(rskCheckFormGoodsShangjianService.updateRskCheckFormGoodsShangjian(enRskCheckFormGoodsShangjian));
    }

    /**
     * 删除查验结果反馈
     */
    @PreAuthorize("@ss.hasPermi('entry:masterCheckForm:remove')")
    @Log(title = "查验结果反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(rskCheckFormGoodsShangjianService.deleteRskCheckFormGoodsShangjianByIds(ids));
    }
}
