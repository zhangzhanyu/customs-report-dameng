package com.qianjing.project.entry.service.impl;

import java.util.List;

import com.qianjing.common.constant.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.entry.mapper.EnEntryHeadListShangjianMapper;
import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;
import com.qianjing.project.entry.service.IEnEntryHeadListShangjianService;

/**
 * 报关表头表体Service业务层处理
 *
 * @author qianjing
 * @date 2023-03-09
 */
@Service
public class EnEntryHeadListShangjianServiceImpl implements IEnEntryHeadListShangjianService
{
    private static final Logger log = LoggerFactory.getLogger(EnEntryHeadListShangjianServiceImpl.class);

    @Autowired
    private EnEntryHeadListShangjianMapper enEntryHeadListShangjianMapper;

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    @Override
    public EnEntryHeadListShangjian checkEntryIdGnoUnique(String entryId, Long gNo) {
        return enEntryHeadListShangjianMapper.checkEntryIdGnoUnique(entryId, gNo);
    }

    /**
     * 查询报关表头表体
     *
     * @param id 报关表头表体主键
     * @return 报关表头表体
     */
    @Override
    public EnEntryHeadListShangjian selectEnEntryHeadListShangjianById(String id)
    {
        return enEntryHeadListShangjianMapper.selectEnEntryHeadListShangjianById(id);
    }

    /**
     * 查询报关表头表体列表
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 报关表头表体
     */
    @Override
    public List<EnEntryHeadListShangjian> selectEnEntryHeadListShangjianList(EnEntryHeadListShangjian enEntryHeadListShangjian)
    {
        return enEntryHeadListShangjianMapper.selectEnEntryHeadListShangjianList(enEntryHeadListShangjian);
    }

    /**
     * 新增报关表头表体
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    @Override
    public int insertEnEntryHeadListShangjian(EnEntryHeadListShangjian enEntryHeadListShangjian)
    {
        return enEntryHeadListShangjianMapper.insertEnEntryHeadListShangjian(enEntryHeadListShangjian);
    }

    /**
     * 修改报关表头表体
     *
     * @param enEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    @Override
    public int updateEnEntryHeadListShangjian(EnEntryHeadListShangjian enEntryHeadListShangjian)
    {
        return enEntryHeadListShangjianMapper.updateEnEntryHeadListShangjian(enEntryHeadListShangjian);
    }

    /**
     * 批量删除报关表头表体
     *
     * @param ids 需要删除的报关表头表体主键
     * @return 结果
     */
    @Override
    public int deleteEnEntryHeadListShangjianByIds(String[] ids)
    {
        return enEntryHeadListShangjianMapper.deleteEnEntryHeadListShangjianByIds(ids);
    }

    /**
     * 删除报关表头表体信息
     *
     * @param id 报关表头表体主键
     * @return 结果
     */
    @Override
    public int deleteEnEntryHeadListShangjianById(String id)
    {
        return enEntryHeadListShangjianMapper.deleteEnEntryHeadListShangjianById(id);
    }

    /**
     * 导入报关表头表体数据
     *
     * @param enEntryHeadListShangjianList enEntryHeadListShangjianList 报关表头表体信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importEnEntryHeadListShangjian(List<EnEntryHeadListShangjian> enEntryHeadListShangjianList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(enEntryHeadListShangjianList) || enEntryHeadListShangjianList.size() == 0) {
            throw new ServiceException("导入报关表头表体数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (EnEntryHeadListShangjian enEntryHeadListShangjian : enEntryHeadListShangjianList){
            try {
                // 验证是否存在
                if (true) {
                    enEntryHeadListShangjian.setCreateBy(operName);
                    this.insertEnEntryHeadListShangjian(enEntryHeadListShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    enEntryHeadListShangjian.setUpdateBy(operName);
                    this.updateEnEntryHeadListShangjian(enEntryHeadListShangjian);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
