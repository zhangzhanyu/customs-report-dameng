package com.qianjing.project.entry.service;

import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.domain.FieldPoolDO;

import java.util.List;

/**
 * 报关表头表体Service接口
 * 
 * @author qianjing
 * @date 2022-09-16
 */
public interface IDbSlaveService {

    /**
     * 查询报关表头表体列表
     *
     * @param enEntryHeadListShangjian 表头表体
     * @return 报关表头表体集合
     */
    public List<EnEntryHeadListShangjian> selectEntryHeadListShangjianList(EnEntryHeadListShangjian enEntryHeadListShangjian);

    /**
     * 查询查验结果反馈列表
     *
     * @param enRskCheckFormGoodsShangjian 查验结果
     * @return 查验结果反馈集合
     */
    public List<EnRskCheckFormGoodsShangjian> selectRskCheckFormGoodsShangjianList(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    public List<FieldPoolDO> selectFieldPoolDtoList(List<DbUnqied> dbUnqiedList);

    public List<FieldPoolDO> selectFieldPoolDtoList2(List<String> hsCodeList, List<String> ciqCodeList, List<String> productCharCodeList);



}
