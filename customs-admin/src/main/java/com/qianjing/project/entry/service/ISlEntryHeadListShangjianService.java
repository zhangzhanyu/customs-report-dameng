package com.qianjing.project.entry.service;

import java.util.List;
import com.qianjing.project.entry.domain.SlEntryHeadListShangjian;

/**
 * 报关表头表体Service接口
 * 
 * @author qianjing
 * @date 2023-03-21
 */
public interface ISlEntryHeadListShangjianService 
{
    /**
     * 查询报关表头表体
     * 
     * @param entryId 报关表头表体主键
     * @return 报关表头表体
     */
    public SlEntryHeadListShangjian selectSlEntryHeadListShangjianByEntryId(String entryId);

    /**
     * 查询报关表头表体列表
     * 
     * @param slEntryHeadListShangjian 报关表头表体
     * @return 报关表头表体集合
     */
    public List<SlEntryHeadListShangjian> selectSlEntryHeadListShangjianList(SlEntryHeadListShangjian slEntryHeadListShangjian);

    /**
     * 新增报关表头表体
     * 
     * @param slEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    public int insertSlEntryHeadListShangjian(SlEntryHeadListShangjian slEntryHeadListShangjian);

    /**
     * 修改报关表头表体
     * 
     * @param slEntryHeadListShangjian 报关表头表体
     * @return 结果
     */
    public int updateSlEntryHeadListShangjian(SlEntryHeadListShangjian slEntryHeadListShangjian);

    /**
     * 批量删除报关表头表体
     * 
     * @param entryIds 需要删除的报关表头表体主键集合
     * @return 结果
     */
    public int deleteSlEntryHeadListShangjianByEntryIds(String[] entryIds);

    /**
     * 删除报关表头表体信息
     * 
     * @param entryId 报关表头表体主键
     * @return 结果
     */
    public int deleteSlEntryHeadListShangjianByEntryId(String entryId);

    /**
     * 导入报关表头表体信息
     *
     * @param slEntryHeadListShangjianList 报关表头表体信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importSlEntryHeadListShangjian(List<SlEntryHeadListShangjian> slEntryHeadListShangjianList, Boolean isUpdateSupport, String operName);
}
