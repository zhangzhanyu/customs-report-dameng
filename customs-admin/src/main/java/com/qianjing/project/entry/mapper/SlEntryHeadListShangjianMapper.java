package com.qianjing.project.entry.mapper;

import java.util.List;
import com.qianjing.project.entry.domain.SlEntryHeadListShangjian;

public interface SlEntryHeadListShangjianMapper {

    SlEntryHeadListShangjian selectSlEntryHeadListShangjianByEntryId(String entryId);

    List<SlEntryHeadListShangjian> selectSlEntryHeadListShangjianList(SlEntryHeadListShangjian slEntryHeadListShangjian);

    int insertSlEntryHeadListShangjian(SlEntryHeadListShangjian slEntryHeadListShangjian);

    int updateSlEntryHeadListShangjian(SlEntryHeadListShangjian slEntryHeadListShangjian);

    int deleteSlEntryHeadListShangjianByEntryId(String entryId);

    int deleteSlEntryHeadListShangjianByEntryIds(String[] entryIds);
}
