package com.qianjing.project.entry.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 报关表头表体对象 entry_head_list_shangjian
 * 
 * @author qianjing
 * @date 2022-09-16
 */
public class EnEntryHeadListShangjian extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private String id;

    /** 海关编号 */
    @Excel(name = "海关编号")
    private String entryId;

    /** 检验检疫特殊业务标识 */
    @Excel(name = "检验检疫特殊业务标识")
    private String iqSpecialMark;

    /** 进出口标识（I进口 E出口） */
    @Excel(name = "进出口标识", readConverterExp = "I=进口,E=出口")
    private String iEFlag;

    /** 申报地海关 */
    @Excel(name = "申报地海关")
    private String declPort;

    /** 进出口关别 */
    @Excel(name = "进出口关别")
    private String iEPort;

    /** 申报日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申报日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dDate;

    /** 监管方式 */
    @Excel(name = "监管方式")
    private String tradeMode;

    /** 境内收发货人代码 */
    @Excel(name = "境内收发货人代码")
    private String consignCode;

    /** 境内收发货人名称 */
    @Excel(name = "境内收发货人名称")
    private String consignName;

    /** 境外收发货人名称(外文) */
    @Excel(name = "境外收发货人名称(外文)")
    private String frnConsignNameFn;

    /** 境外收发货人地址 */
    @Excel(name = "境外收发货人地址")
    private String frnConsignAddress;

    /** 消费使用单位代码\生产销售单位代码(统一社会信用代码) */
    @Excel(name = "消费使用单位代码\\生产销售单位代码(统一社会信用代码)")
    private String ownerCodeScc;

    /** 消费使用单位代码\生产销售单位代码 */
    @Excel(name = "消费使用单位代码\\生产销售单位代码")
    private String ownerCode;

    /** 消费使用单位名称\生产销售单位名称 */
    @Excel(name = "消费使用单位名称\\生产销售单位名称")
    private String ownerName;

    /** 申报单位代码 */
    @Excel(name = "申报单位代码")
    private String agentCode;

    /** 申报单位名称 */
    @Excel(name = "申报单位名称")
    private String agentName;

    /** 贸易国别(地区) */
    @Excel(name = "贸易国别(地区)")
    private String tradeCountry;

    /** 运输方式代码 */
    @Excel(name = "运输方式代码")
    private String trafMode;

    /** 启运国(地区)/运抵国(地区) */
    @Excel(name = "启运国(地区)/运抵国(地区)")
    private String frnIECountry;

    /** 入境口岸 */
    @Excel(name = "入境口岸")
    private String iqImportPort;

    /** 涉检标识（1涉检 0不涉检） */
    @Excel(name = "涉检标识", readConverterExp = "1=涉检,0=不涉检")
    private String eciqType;

    /** 记录创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recCreateTime;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recLastUpdateTime;

    /** 项号 */
    @Excel(name = "项号")
    private Long gNo;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String codeTs;

    /** 检验检疫编码 */
    @Excel(name = "检验检疫编码")
    private String iqCode;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String gName;

    /** 报检名称 */
    @Excel(name = "报检名称")
    private String iqGName;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String gModel;

    /** 法定第一数量 */
    @Excel(name = "法定第一数量")
    private Long qty1;

    /** 法定第一计量单位 */
    @Excel(name = "法定第一计量单位")
    private String unit1;

    /** 第二数量 */
    @Excel(name = "第二数量")
    private Long qty2;

    /** 第二计量单位 */
    @Excel(name = "第二计量单位")
    private String unit2;

    /** 统计人民币价 */
    @Excel(name = "统计人民币价")
    private Long rmbPrice;

    /** 统计美元价 */
    @Excel(name = "统计美元价")
    private Long usdPrice;

    /** 原产国(地区) */
    @Excel(name = "原产国(地区)")
    private String originCountry;

    /** 境外生产企业名称 */
    @Excel(name = "境外生产企业名称")
    private String frnProducerName;

    /** 货物型号 */
    @Excel(name = "货物型号")
    private String productModel;

    /** 货物品牌 */
    @Excel(name = "货物品牌")
    private String productBrand;

    /** UN编码 */
    @Excel(name = "UN编码")
    private String ungid;

    /** 非危险化学品 */
    @Excel(name = "非危险化学品")
    private String ungFlag;

    /** 危包规格 */
    @Excel(name = "危包规格")
    private String ungModel;

    /** 危包类别 */
    @Excel(name = "危包类别")
    private String ungClassify;

    /** 危险货物名称 */
    @Excel(name = "危险货物名称")
    private String ungGName;

    /** 货物属性代码 */
    @Excel(name = "货物属性代码")
    private String productCharCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getIqSpecialMark() {
        return iqSpecialMark;
    }

    public void setIqSpecialMark(String iqSpecialMark) {
        this.iqSpecialMark = iqSpecialMark;
    }

    public String getiEFlag() {
        return iEFlag;
    }

    public void setiEFlag(String iEFlag) {
        this.iEFlag = iEFlag;
    }

    public String getDeclPort() {
        return declPort;
    }

    public void setDeclPort(String declPort) {
        this.declPort = declPort;
    }

    public String getiEPort() {
        return iEPort;
    }

    public void setiEPort(String iEPort) {
        this.iEPort = iEPort;
    }

    public Date getdDate() {
        return dDate;
    }

    public void setdDate(Date dDate) {
        this.dDate = dDate;
    }

    public String getTradeMode() {
        return tradeMode;
    }

    public void setTradeMode(String tradeMode) {
        this.tradeMode = tradeMode;
    }

    public String getConsignCode() {
        return consignCode;
    }

    public void setConsignCode(String consignCode) {
        this.consignCode = consignCode;
    }

    public String getConsignName() {
        return consignName;
    }

    public void setConsignName(String consignName) {
        this.consignName = consignName;
    }

    public String getFrnConsignNameFn() {
        return frnConsignNameFn;
    }

    public void setFrnConsignNameFn(String frnConsignNameFn) {
        this.frnConsignNameFn = frnConsignNameFn;
    }

    public String getFrnConsignAddress() {
        return frnConsignAddress;
    }

    public void setFrnConsignAddress(String frnConsignAddress) {
        this.frnConsignAddress = frnConsignAddress;
    }

    public String getOwnerCodeScc() {
        return ownerCodeScc;
    }

    public void setOwnerCodeScc(String ownerCodeScc) {
        this.ownerCodeScc = ownerCodeScc;
    }

    public String getOwnerCode() {
        return ownerCode;
    }

    public void setOwnerCode(String ownerCode) {
        this.ownerCode = ownerCode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getTradeCountry() {
        return tradeCountry;
    }

    public void setTradeCountry(String tradeCountry) {
        this.tradeCountry = tradeCountry;
    }

    public String getTrafMode() {
        return trafMode;
    }

    public void setTrafMode(String trafMode) {
        this.trafMode = trafMode;
    }

    public String getFrnIECountry() {
        return frnIECountry;
    }

    public void setFrnIECountry(String frnIECountry) {
        this.frnIECountry = frnIECountry;
    }

    public String getIqImportPort() {
        return iqImportPort;
    }

    public void setIqImportPort(String iqImportPort) {
        this.iqImportPort = iqImportPort;
    }

    public String getEciqType() {
        return eciqType;
    }

    public void setEciqType(String eciqType) {
        this.eciqType = eciqType;
    }

    public Date getRecCreateTime() {
        return recCreateTime;
    }

    public void setRecCreateTime(Date recCreateTime) {
        this.recCreateTime = recCreateTime;
    }

    public Date getRecLastUpdateTime() {
        return recLastUpdateTime;
    }

    public void setRecLastUpdateTime(Date recLastUpdateTime) {
        this.recLastUpdateTime = recLastUpdateTime;
    }

    public Long getgNo() {
        return gNo;
    }

    public void setgNo(Long gNo) {
        this.gNo = gNo;
    }

    public String getCodeTs() {
        return codeTs;
    }

    public void setCodeTs(String codeTs) {
        this.codeTs = codeTs;
    }

    public String getIqCode() {
        return iqCode;
    }

    public void setIqCode(String iqCode) {
        this.iqCode = iqCode;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getIqGName() {
        return iqGName;
    }

    public void setIqGName(String iqGName) {
        this.iqGName = iqGName;
    }

    public String getgModel() {
        return gModel;
    }

    public void setgModel(String gModel) {
        this.gModel = gModel;
    }

    public Long getQty1() {
        return qty1;
    }

    public void setQty1(Long qty1) {
        this.qty1 = qty1;
    }

    public String getUnit1() {
        return unit1;
    }

    public void setUnit1(String unit1) {
        this.unit1 = unit1;
    }

    public Long getQty2() {
        return qty2;
    }

    public void setQty2(Long qty2) {
        this.qty2 = qty2;
    }

    public String getUnit2() {
        return unit2;
    }

    public void setUnit2(String unit2) {
        this.unit2 = unit2;
    }

    public Long getRmbPrice() {
        return rmbPrice;
    }

    public void setRmbPrice(Long rmbPrice) {
        this.rmbPrice = rmbPrice;
    }

    public Long getUsdPrice() {
        return usdPrice;
    }

    public void setUsdPrice(Long usdPrice) {
        this.usdPrice = usdPrice;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getFrnProducerName() {
        return frnProducerName;
    }

    public void setFrnProducerName(String frnProducerName) {
        this.frnProducerName = frnProducerName;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getUngid() {
        return ungid;
    }

    public void setUngid(String ungid) {
        this.ungid = ungid;
    }

    public String getUngFlag() {
        return ungFlag;
    }

    public void setUngFlag(String ungFlag) {
        this.ungFlag = ungFlag;
    }

    public String getUngModel() {
        return ungModel;
    }

    public void setUngModel(String ungModel) {
        this.ungModel = ungModel;
    }

    public String getUngClassify() {
        return ungClassify;
    }

    public void setUngClassify(String ungClassify) {
        this.ungClassify = ungClassify;
    }

    public String getUngGName() {
        return ungGName;
    }

    public void setUngGName(String ungGName) {
        this.ungGName = ungGName;
    }

    public String getProductCharCode() {
        return productCharCode;
    }

    public void setProductCharCode(String productCharCode) {
        this.productCharCode = productCharCode;
    }
}
