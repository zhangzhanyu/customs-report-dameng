package com.qianjing.project.entry.mapper;

import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.domain.FieldPoolDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DbSlaveMapper {

    public List<EnEntryHeadListShangjian> selectEntryHeadListShangjianList(EnEntryHeadListShangjian enEntryHeadListShangjian);

    public List<EnRskCheckFormGoodsShangjian> selectRskCheckFormGoodsShangjianList(EnRskCheckFormGoodsShangjian enRskCheckFormGoodsShangjian);

    public List<FieldPoolDO> selectFieldPoolDtoList(@Param("paramDTOList") List<DbUnqied> paramDTOList);

    public List<FieldPoolDO> selectFieldPoolDtoList2(@Param("hsCodeList") List<String> hsCodeList,
                                                    @Param("ciqCodeList") List<String> ciqCodeList,
                                                    @Param("productCharCodeList") List<String> productCharCodeList);

}
