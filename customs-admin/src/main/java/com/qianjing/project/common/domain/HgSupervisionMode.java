package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 监管方式代码对象 hg_supervision_mode
 *
 * @author qianjing
 * @date 2022-04-25
 */
public class HgSupervisionMode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 监管方式编号 */
    private Long supervisionModeId;

    /** 监管方式代码 */
    @Excel(name = "监管方式代码")
    private Long supervisionModeCode;

    /** 监管方式代码简称 */
    @Excel(name = "监管方式代码简称")
    private String supervisionModeSimpleName;

    /** 监管方式代码全称 */
    @Excel(name = "监管方式代码全称")
    private String supervisionModeFullName;

    /** 标签1 */
    @Excel(name = "标签1")
    private String tag1;

    /** 标签2 */
    @Excel(name = "标签2")
    private String tag2;

    /** 标签3 */
    @Excel(name = "标签3")
    private String tag3;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setSupervisionModeId(Long supervisionModeId)
    {
        this.supervisionModeId = supervisionModeId;
    }

    public Long getSupervisionModeId()
    {
        return supervisionModeId;
    }
    public void setSupervisionModeCode(Long supervisionModeCode)
    {
        this.supervisionModeCode = supervisionModeCode;
    }

    public Long getSupervisionModeCode()
    {
        return supervisionModeCode;
    }
    public void setSupervisionModeSimpleName(String supervisionModeSimpleName)
    {
        this.supervisionModeSimpleName = supervisionModeSimpleName;
    }

    public String getSupervisionModeSimpleName()
    {
        return supervisionModeSimpleName;
    }
    public void setSupervisionModeFullName(String supervisionModeFullName)
    {
        this.supervisionModeFullName = supervisionModeFullName;
    }

    public String getSupervisionModeFullName()
    {
        return supervisionModeFullName;
    }
    public void setTag1(String tag1)
    {
        this.tag1 = tag1;
    }

    public String getTag1()
    {
        return tag1;
    }
    public void setTag2(String tag2)
    {
        this.tag2 = tag2;
    }

    public String getTag2()
    {
        return tag2;
    }
    public void setTag3(String tag3)
    {
        this.tag3 = tag3;
    }

    public String getTag3()
    {
        return tag3;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("supervisionModeId", getSupervisionModeId())
                .append("supervisionModeCode", getSupervisionModeCode())
                .append("supervisionModeSimpleName", getSupervisionModeSimpleName())
                .append("supervisionModeFullName", getSupervisionModeFullName())
                .append("remark", getRemark())
                .append("tag1", getTag1())
                .append("tag2", getTag2())
                .append("tag3", getTag3())
                .append("delFlag", getDelFlag())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .toString();
    }
}
