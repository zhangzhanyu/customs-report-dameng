package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 商品目录对象 hg_goods_catalog
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgGoodsCatalog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 目录编号 */
    private Long goodsCatalogId;

    /** 商品编码 */
    @Excel(name = "商品编码")
    private Long goodsCode;

    /** 第一计量单位 */
    @Excel(name = "第一计量单位")
    private String unitOne;

    /** 第一计量单位名称 */
    @Excel(name = "第一计量单位名称")
    private String unitOneName;

    /** 第一计量单位英文名称 */
    @Excel(name = "第一计量单位英文名称")
    private String unitOneNameEn;

    /** 第二计量单位 */
    @Excel(name = "第二计量单位")
    private String unitTwo;

    /** 第二计量单位名称 */
    @Excel(name = "第二计量单位名称")
    private String unitTwoName;

    /** 第二计量单位英文名称 */
    @Excel(name = "第二计量单位英文名称")
    private String unitTwoNameEn;

    /** 商品中文名称 */
    @Excel(name = "商品中文名称")
    private String goodsName;

    /** 商品英文名称 */
    @Excel(name = "商品英文名称")
    private String goodsNameEn;

    /** 修改说明 */
    @Excel(name = "修改说明")
    private String updateDescription;

    /** 标签1 */
    @Excel(name = "标签1")
    private String tag1;

    /** 标签2 */
    @Excel(name = "标签2")
    private String tag2;

    /** 标签3 */
    @Excel(name = "标签3")
    private String tag3;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setGoodsCatalogId(Long goodsCatalogId) 
    {
        this.goodsCatalogId = goodsCatalogId;
    }

    public Long getGoodsCatalogId() 
    {
        return goodsCatalogId;
    }
    public void setGoodsCode(Long goodsCode) 
    {
        this.goodsCode = goodsCode;
    }

    public Long getGoodsCode() 
    {
        return goodsCode;
    }
    public void setUnitOne(String unitOne) 
    {
        this.unitOne = unitOne;
    }

    public String getUnitOne() 
    {
        return unitOne;
    }
    public void setUnitOneName(String unitOneName) 
    {
        this.unitOneName = unitOneName;
    }

    public String getUnitOneName() 
    {
        return unitOneName;
    }
    public void setUnitOneNameEn(String unitOneNameEn) 
    {
        this.unitOneNameEn = unitOneNameEn;
    }

    public String getUnitOneNameEn() 
    {
        return unitOneNameEn;
    }
    public void setUnitTwo(String unitTwo) 
    {
        this.unitTwo = unitTwo;
    }

    public String getUnitTwo() 
    {
        return unitTwo;
    }
    public void setUnitTwoName(String unitTwoName) 
    {
        this.unitTwoName = unitTwoName;
    }

    public String getUnitTwoName() 
    {
        return unitTwoName;
    }
    public void setUnitTwoNameEn(String unitTwoNameEn) 
    {
        this.unitTwoNameEn = unitTwoNameEn;
    }

    public String getUnitTwoNameEn() 
    {
        return unitTwoNameEn;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsNameEn(String goodsNameEn) 
    {
        this.goodsNameEn = goodsNameEn;
    }

    public String getGoodsNameEn() 
    {
        return goodsNameEn;
    }
    public void setUpdateDescription(String updateDescription) 
    {
        this.updateDescription = updateDescription;
    }

    public String getUpdateDescription() 
    {
        return updateDescription;
    }
    public void setTag1(String tag1) 
    {
        this.tag1 = tag1;
    }

    public String getTag1() 
    {
        return tag1;
    }
    public void setTag2(String tag2) 
    {
        this.tag2 = tag2;
    }

    public String getTag2() 
    {
        return tag2;
    }
    public void setTag3(String tag3) 
    {
        this.tag3 = tag3;
    }

    public String getTag3() 
    {
        return tag3;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("goodsCatalogId", getGoodsCatalogId())
            .append("goodsCode", getGoodsCode())
            .append("unitOne", getUnitOne())
            .append("unitOneName", getUnitOneName())
            .append("unitOneNameEn", getUnitOneNameEn())
            .append("unitTwo", getUnitTwo())
            .append("unitTwoName", getUnitTwoName())
            .append("unitTwoNameEn", getUnitTwoNameEn())
            .append("goodsName", getGoodsName())
            .append("goodsNameEn", getGoodsNameEn())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateDescription", getUpdateDescription())
            .append("tag1", getTag1())
            .append("tag2", getTag2())
            .append("tag3", getTag3())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
