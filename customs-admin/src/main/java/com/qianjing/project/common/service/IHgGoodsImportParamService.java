package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsImportParam;

/**
 * 商品进口参数Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgGoodsImportParamService 
{
    /**
     * 查询商品进口参数
     * 
     * @param goodsImportParamId 商品进口参数主键
     * @return 商品进口参数
     */
    public HgGoodsImportParam selectHgGoodsImportParamByGoodsImportParamId(Long goodsImportParamId);

    /**
     * 查询商品进口参数列表
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 商品进口参数集合
     */
    public List<HgGoodsImportParam> selectHgGoodsImportParamList(HgGoodsImportParam hgGoodsImportParam);

    /**
     * 新增商品进口参数
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 结果
     */
    public int insertHgGoodsImportParam(HgGoodsImportParam hgGoodsImportParam);

    /**
     * 修改商品进口参数
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 结果
     */
    public int updateHgGoodsImportParam(HgGoodsImportParam hgGoodsImportParam);

    /**
     * 批量删除商品进口参数
     * 
     * @param goodsImportParamIds 需要删除的商品进口参数主键集合
     * @return 结果
     */
    public int deleteHgGoodsImportParamByGoodsImportParamIds(Long[] goodsImportParamIds);

    /**
     * 删除商品进口参数信息
     * 
     * @param goodsImportParamId 商品进口参数主键
     * @return 结果
     */
    public int deleteHgGoodsImportParamByGoodsImportParamId(Long goodsImportParamId);

    /**
     * 导入商品进口参数信息
     *
     * @param hgGoodsImportParamList 商品进口参数信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgGoodsImportParam(List<HgGoodsImportParam> hgGoodsImportParamList, Boolean isUpdateSupport, String operName);
}
