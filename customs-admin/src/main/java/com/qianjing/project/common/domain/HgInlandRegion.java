package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 国内地区代码对象 hg_inland_region
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgInlandRegion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 国内地区编号 */
    private Long inlandRegionId;

    /** 国内地区编码 */
    @Excel(name = "国内地区编码")
    private String inlandRegionCode;

    /** 国内地区名称 */
    @Excel(name = "国内地区名称")
    private String inlandRegionName;

    /** 国内地区简称 */
    @Excel(name = "国内地区简称")
    private String inlandRegionSimpleName;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setInlandRegionId(Long inlandRegionId) 
    {
        this.inlandRegionId = inlandRegionId;
    }

    public Long getInlandRegionId() 
    {
        return inlandRegionId;
    }
    public void setInlandRegionCode(String inlandRegionCode) 
    {
        this.inlandRegionCode = inlandRegionCode;
    }

    public String getInlandRegionCode() 
    {
        return inlandRegionCode;
    }
    public void setInlandRegionName(String inlandRegionName) 
    {
        this.inlandRegionName = inlandRegionName;
    }

    public String getInlandRegionName() 
    {
        return inlandRegionName;
    }
    public void setInlandRegionSimpleName(String inlandRegionSimpleName) 
    {
        this.inlandRegionSimpleName = inlandRegionSimpleName;
    }

    public String getInlandRegionSimpleName() 
    {
        return inlandRegionSimpleName;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("inlandRegionId", getInlandRegionId())
            .append("inlandRegionCode", getInlandRegionCode())
            .append("inlandRegionName", getInlandRegionName())
            .append("inlandRegionSimpleName", getInlandRegionSimpleName())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
