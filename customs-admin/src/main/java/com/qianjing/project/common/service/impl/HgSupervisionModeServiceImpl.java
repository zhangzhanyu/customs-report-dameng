package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgSupervisionModeMapper;
import com.qianjing.project.common.domain.HgSupervisionMode;
import com.qianjing.project.common.service.IHgSupervisionModeService;

/**
 * 监管方式代码Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgSupervisionModeServiceImpl implements IHgSupervisionModeService 
{
    private static final Logger log = LoggerFactory.getLogger(HgSupervisionModeServiceImpl.class);

    @Autowired
    private HgSupervisionModeMapper hgSupervisionModeMapper;

    /**
     * 查询监管方式代码
     * 
     * @param supervisionModeId 监管方式代码主键
     * @return 监管方式代码
     */
    @Override
    public HgSupervisionMode selectHgSupervisionModeBySupervisionModeId(Long supervisionModeId)
    {
        return hgSupervisionModeMapper.selectHgSupervisionModeBySupervisionModeId(supervisionModeId);
    }

    /**
     * 查询监管方式代码列表
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 监管方式代码
     */
    @Override
    public List<HgSupervisionMode> selectHgSupervisionModeList(HgSupervisionMode hgSupervisionMode)
    {
        return hgSupervisionModeMapper.selectHgSupervisionModeList(hgSupervisionMode);
    }

    /**
     * 新增监管方式代码
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 结果
     */
    @Override
    public int insertHgSupervisionMode(HgSupervisionMode hgSupervisionMode)
    {
        hgSupervisionMode.setCreateTime(DateUtils.getNowDate());
        return hgSupervisionModeMapper.insertHgSupervisionMode(hgSupervisionMode);
    }

    /**
     * 修改监管方式代码
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 结果
     */
    @Override
    public int updateHgSupervisionMode(HgSupervisionMode hgSupervisionMode)
    {
        hgSupervisionMode.setUpdateTime(DateUtils.getNowDate());
        return hgSupervisionModeMapper.updateHgSupervisionMode(hgSupervisionMode);
    }

    /**
     * 批量删除监管方式代码
     * 
     * @param supervisionModeIds 需要删除的监管方式代码主键
     * @return 结果
     */
    @Override
    public int deleteHgSupervisionModeBySupervisionModeIds(Long[] supervisionModeIds)
    {
        return hgSupervisionModeMapper.deleteHgSupervisionModeBySupervisionModeIds(supervisionModeIds);
    }

    /**
     * 删除监管方式代码信息
     * 
     * @param supervisionModeId 监管方式代码主键
     * @return 结果
     */
    @Override
    public int deleteHgSupervisionModeBySupervisionModeId(Long supervisionModeId)
    {
        return hgSupervisionModeMapper.deleteHgSupervisionModeBySupervisionModeId(supervisionModeId);
    }

    /**
     * 导入监管方式代码数据
     *
     * @param hgSupervisionModeList hgSupervisionModeList 监管方式代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgSupervisionMode(List<HgSupervisionMode> hgSupervisionModeList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgSupervisionModeList) || hgSupervisionModeList.size() == 0) {
            throw new ServiceException("导入监管方式代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgSupervisionMode hgSupervisionMode : hgSupervisionModeList){
            try {
                // 验证是否存在
                if (true) {
                    hgSupervisionMode.setCreateBy(operName);
                    this.insertHgSupervisionMode(hgSupervisionMode);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgSupervisionMode.setUpdateBy(operName);
                    this.updateHgSupervisionMode(hgSupervisionMode);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
