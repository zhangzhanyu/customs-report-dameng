package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 特殊贸易区代码对象 hg_special_region
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgSpecialRegion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 特殊贸易区编号 */
    private Long specialRegionId;

    /** 特殊贸易区编码 */
    @Excel(name = "特殊贸易区编码")
    private String specialRegionCode;

    /** 特殊贸易区名称 */
    @Excel(name = "特殊贸易区名称")
    private String specialRegionName;

    /** 特殊贸易区编码说明 */
    @Excel(name = "特殊贸易区编码说明")
    private String specialRegionDescription;

    /** 对应直属海关代码 */
    @Excel(name = "对应直属海关代码")
    private String customsCode;

    /** 对应直属海关国内代码 */
    @Excel(name = "对应直属海关国内代码")
    private String customsHomeCode;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    /** 是否启用（0启用，1未启） */
    private String status;

    public void setSpecialRegionId(Long specialRegionId) 
    {
        this.specialRegionId = specialRegionId;
    }

    public Long getSpecialRegionId() 
    {
        return specialRegionId;
    }
    public void setSpecialRegionCode(String specialRegionCode) 
    {
        this.specialRegionCode = specialRegionCode;
    }

    public String getSpecialRegionCode() 
    {
        return specialRegionCode;
    }
    public void setSpecialRegionName(String specialRegionName) 
    {
        this.specialRegionName = specialRegionName;
    }

    public String getSpecialRegionName() 
    {
        return specialRegionName;
    }
    public void setSpecialRegionDescription(String specialRegionDescription) 
    {
        this.specialRegionDescription = specialRegionDescription;
    }

    public String getSpecialRegionDescription() 
    {
        return specialRegionDescription;
    }
    public void setCustomsCode(String customsCode) 
    {
        this.customsCode = customsCode;
    }

    public String getCustomsCode() 
    {
        return customsCode;
    }
    public void setCustomsHomeCode(String customsHomeCode) 
    {
        this.customsHomeCode = customsHomeCode;
    }

    public String getCustomsHomeCode() 
    {
        return customsHomeCode;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("specialRegionId", getSpecialRegionId())
            .append("specialRegionCode", getSpecialRegionCode())
            .append("specialRegionName", getSpecialRegionName())
            .append("specialRegionDescription", getSpecialRegionDescription())
            .append("customsCode", getCustomsCode())
            .append("customsHomeCode", getCustomsHomeCode())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
