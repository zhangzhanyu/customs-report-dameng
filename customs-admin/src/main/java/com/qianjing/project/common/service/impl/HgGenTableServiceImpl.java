package com.qianjing.project.common.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qianjing.common.constant.Constants;
import com.qianjing.common.constant.GenConstants;
import com.qianjing.common.constant.UserConstants;
import com.qianjing.common.core.text.CharsetKit;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.StringUtils;
import com.qianjing.project.common.domain.HgGenTable;
import com.qianjing.project.common.domain.HgGenTableColumn;
import com.qianjing.project.common.mapper.HgGenTableColumnMapper;
import com.qianjing.project.common.mapper.HgGenTableMapper;
import com.qianjing.project.common.service.IHgGenTableService;
import com.qianjing.project.common.util.GenUtils;
import com.qianjing.project.common.util.VelocityInitializer;
import com.qianjing.project.common.util.VelocityUtils;
import com.qianjing.project.system.domain.SysMenu;
import com.qianjing.project.system.service.ISysMenuService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.qianjing.common.utils.SecurityUtils.getUsername;

/**
 * 业务 服务层实现
 *
 * @author qianjing
 */
@Service
public class HgGenTableServiceImpl implements IHgGenTableService
{
    private static final Logger log = LoggerFactory.getLogger(HgGenTableServiceImpl.class);

    @Autowired
    private HgGenTableMapper hgGenTableMapper;

    @Autowired
    private HgGenTableColumnMapper hgGenTableColumnMapper;

    @Autowired
    private ISysMenuService menuService;

    /**
     * 查询业务信息
     *
     * @param id 业务ID
     * @return 业务信息
     */
    @Override
    public HgGenTable selectGenTableById(Long id)
    {
        HgGenTable hgGenTable = hgGenTableMapper.selectGenTableById(id);
        setTableFromOptions(hgGenTable);
        return hgGenTable;
    }

    /**
     * 查询业务列表
     *
     * @param hgGenTable 业务信息
     * @return 业务集合
     */
    @Override
    public List<HgGenTable> selectGenTableList(HgGenTable hgGenTable)
    {
        return hgGenTableMapper.selectGenTableList(hgGenTable);
    }

    /**
     * 查询据库列表
     *
     * @param hgGenTable 业务信息
     * @return 数据库表集合
     */
    @Override
    public List<HgGenTable> selectDbTableList(HgGenTable hgGenTable)
    {
        return hgGenTableMapper.selectDbTableList(hgGenTable);
    }

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    @Override
    public List<HgGenTable> selectDbTableListByNames(String[] tableNames)
    {
        return hgGenTableMapper.selectDbTableListByNames(tableNames);
    }

    /**
     * 查询所有表信息
     *
     * @return 表信息集合
     */
    @Override
    public List<HgGenTable> selectGenTableAll()
    {
        return hgGenTableMapper.selectGenTableAll();
    }

    /**
     * 修改业务
     *
     * @param hgGenTable 业务信息
     * @return 结果
     */
    @Override
    @Transactional
    public void updateGenTable(HgGenTable hgGenTable)
    {
        String options = JSON.toJSONString(hgGenTable.getParams());
        hgGenTable.setOptions(options);
        int row = hgGenTableMapper.updateGenTable(hgGenTable);
        if (row > 0)
        {
            for (HgGenTableColumn cenTableColumn : hgGenTable.getColumns())
            {
                hgGenTableColumnMapper.updateGenTableColumn(cenTableColumn);
            }
        }
    }

    /**
     * 删除业务对象
     *
     * @param tableIds 需要删除的数据ID
     * @return 结果
     */
    @Override
    @Transactional
    public void deleteGenTableByIds(Long[] tableIds)
    {
        hgGenTableMapper.deleteGenTableByIds(tableIds);
        hgGenTableColumnMapper.deleteGenTableColumnByIds(tableIds);
    }

    /**
     * 导入表结构
     *
     * @param tableList 导入表列表
     */
    @Override
    @Transactional
    public void importGenTable(List<HgGenTable> tableList)
    {
        String operName = getUsername();
        try
        {
            for (HgGenTable table : tableList)
            {
                String tableName = table.getTableName();
                GenUtils.initTable(table, operName);
                int row = hgGenTableMapper.insertGenTable(table);
                if (row > 0)
                {
                    // 保存列信息
                    List<HgGenTableColumn> hgGenTableColumns = hgGenTableColumnMapper.selectDbTableColumnsByName(tableName);
                    for (HgGenTableColumn column : hgGenTableColumns)
                    {
                        GenUtils.initColumnField(column, table);
                        hgGenTableColumnMapper.insertGenTableColumn(column);
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new ServiceException("导入失败：" + e.getMessage());
        }
    }

    /**
     * 导入表结构
     *
     * @param hgGenTable 导入表列表
     */
    @Override
    @Transactional
    public void importReportTable(HgGenTable hgGenTable) {
        int row = hgGenTableMapper.insertGenTable(hgGenTable);

        /** 创建菜单 **/
        SysMenu menu = new SysMenu();
        menu.setParentId(2045L);
        menu.setMenuName(hgGenTable.getTableComment());
        menu.setPerms("common:report:list");
        menu.setIcon("chart");
        menu.setMenuType("C");
        menu.setOrderNum(Constants.DB_IS_STATUS_YES);
        menu.setIsFrame(Constants.DB_IS_STATUS_YES);
        menu.setIsCache(Constants.DB_IS_STATUS_YES);
        menu.setVisible(Constants.DB_IS_STATUS_NO);
        menu.setStatus(Constants.DB_IS_STATUS_NO);
        menu.setQuery("{\"tableId\": "+ hgGenTable.getTableId()+ ", \"menuName\":\""+hgGenTable.getTableComment()+"\"}");
        menu.setComponent("common/report/index");
        menu.setPath("report");

        /** 校验菜单 **/
        if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))) {
            throw new ServiceException("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        } else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !StringUtils.ishttp(menu.getPath())) {
            throw new ServiceException("新增菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }
        menu.setCreateBy(getUsername());
        menuService.insertMenu(menu);

        if (row > 0) {
            for (HgGenTableColumn hgGenTableColumn : hgGenTable.getColumns()) {
                hgGenTableColumn.setTableId(hgGenTable.getTableId());
                hgGenTableColumnMapper.insertGenTableColumn(hgGenTableColumn);
            }
        }
    }

    /**
     * 预览代码
     *
     * @param tableId 表编号
     * @return 预览数据列表
     */
    @Override
    public Map<String, String> previewCode(Long tableId)
    {
        Map<String, String> dataMap = new LinkedHashMap<>();
        // 查询表信息
        HgGenTable table = hgGenTableMapper.selectGenTableById(tableId);
        // 设置主子表信息
        setSubTable(table);
        // 设置主键列信息
        setPkColumn(table);
        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(table.getTplCategory());
        for (String template : templates)
        {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
            tpl.merge(context, sw);
            dataMap.put(template, sw.toString());
        }
        return dataMap;
    }

    /**
     * 生成代码（下载方式）
     *
     * @param tableName 表名称
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String tableName)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        generatorCode(tableName, zip);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 生成代码（自定义路径）
     *
     * @param tableName 表名称
     */
    @Override
    public void generatorCode(String tableName)
    {
        // 查询表信息
        HgGenTable table = hgGenTableMapper.selectGenTableByName(tableName);
        // 获取菜单id序列，用于生成菜单sql语句
        long menuId = hgGenTableMapper.selectMenuId();
        table.setMenuId(menuId);
        // 设置主子表信息
        setSubTable(table);
        // 设置主键列信息
        setPkColumn(table);

        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(table.getTplCategory());
        for (String template : templates)
        {
            if (!StringUtils.containsAny(template, "sql.vm", "api.js.vm", "index.vue.vm", "index-tree.vue.vm"))
            {
                // 渲染模板
                StringWriter sw = new StringWriter();
                Template tpl = Velocity.getTemplate(template, Constants.UTF8);
                tpl.merge(context, sw);
                try
                {
                    String path = getGenPath(table, template);
                    FileUtils.writeStringToFile(FileUtils.getFile(path), sw.toString(), CharsetKit.UTF_8);
                }
                catch (IOException e)
                {
                    throw new ServiceException("渲染模板失败，表名：" + table.getTableName());
                }
            }
        }
    }

    /**
     * 同步数据库
     *
     * @param tableName 表名称
     */
    @Override
    @Transactional
    public void synchDb(String tableName)
    {
        HgGenTable table = hgGenTableMapper.selectGenTableByName(tableName);
        List<HgGenTableColumn> tableColumns = table.getColumns();
        List<String> tableColumnNames = tableColumns.stream().map(HgGenTableColumn::getColumnName).collect(Collectors.toList());

        List<HgGenTableColumn> dbTableColumns = hgGenTableColumnMapper.selectDbTableColumnsByName(tableName);
        if (StringUtils.isEmpty(dbTableColumns))
        {
            throw new ServiceException("同步数据失败，原表结构不存在");
        }
        List<String> dbTableColumnNames = dbTableColumns.stream().map(HgGenTableColumn::getColumnName).collect(Collectors.toList());

        dbTableColumns.forEach(column -> {
            if (!tableColumnNames.contains(column.getColumnName()))
            {
                GenUtils.initColumnField(column, table);
                hgGenTableColumnMapper.insertGenTableColumn(column);
            }
        });

        List<HgGenTableColumn> delColumns = tableColumns.stream().filter(column -> !dbTableColumnNames.contains(column.getColumnName())).collect(Collectors.toList());
        if (StringUtils.isNotEmpty(delColumns))
        {
            hgGenTableColumnMapper.deleteGenTableColumns(delColumns);
        }
    }

    /**
     * 批量生成代码（下载方式）
     *
     * @param tableNames 表数组
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String[] tableNames)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        for (String tableName : tableNames)
        {
            generatorCode(tableName, zip);
        }
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 查询表信息并生成代码
     */
    private void generatorCode(String tableName, ZipOutputStream zip)
    {
        // 查询表信息
        HgGenTable table = hgGenTableMapper.selectGenTableByName(tableName);
        // 获取菜单id序列，用于生成菜单sql语句
        long menuId = hgGenTableMapper.selectMenuId();
        table.setMenuId(menuId);
        // 设置主子表信息
        setSubTable(table);
        // 设置主键列信息
        setPkColumn(table);

        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(table);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(table.getTplCategory());
        for (String template : templates)
        {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
            tpl.merge(context, sw);
            try
            {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(VelocityUtils.getFileName(template, table)));
                IOUtils.write(sw.toString(), zip, Constants.UTF8);
                IOUtils.closeQuietly(sw);
                zip.flush();
                zip.closeEntry();
            }
            catch (IOException e)
            {
                log.error("渲染模板失败，表名：" + table.getTableName(), e);
            }
        }
    }

    /**
     * 修改保存参数校验
     *
     * @param hgGenTable 业务信息
     */
    @Override
    public void validateEdit(HgGenTable hgGenTable)
    {
        if (GenConstants.TPL_TREE.equals(hgGenTable.getTplCategory()))
        {
            String options = JSON.toJSONString(hgGenTable.getParams());
            JSONObject paramsObj = JSONObject.parseObject(options);
            if (StringUtils.isEmpty(paramsObj.getString(GenConstants.TREE_CODE)))
            {
                throw new ServiceException("树编码字段不能为空");
            }
            else if (StringUtils.isEmpty(paramsObj.getString(GenConstants.TREE_PARENT_CODE)))
            {
                throw new ServiceException("树父编码字段不能为空");
            }
            else if (StringUtils.isEmpty(paramsObj.getString(GenConstants.TREE_NAME)))
            {
                throw new ServiceException("树名称字段不能为空");
            }
        }
        else if (GenConstants.TPL_SUB.equals(hgGenTable.getTplCategory()))
        {
            if (StringUtils.isEmpty(hgGenTable.getSubTableName()))
            {
                throw new ServiceException("关联子表的表名不能为空");
            }
            else if (StringUtils.isEmpty(hgGenTable.getSubTableFkName()))
            {
                throw new ServiceException("子表关联的外键名不能为空");
            }
        }
    }

    /**
     * 设置主键列信息
     *
     * @param table 业务表信息
     */
    public void setPkColumn(HgGenTable table)
    {
        for (HgGenTableColumn column : table.getColumns())
        {
            if (column.isPk())
            {
                table.setPkColumn(column);
                break;
            }
        }
        if (StringUtils.isNull(table.getPkColumn()))
        {
            table.setPkColumn(table.getColumns().get(0));
        }
        if (GenConstants.TPL_SUB.equals(table.getTplCategory()))
        {
            for (HgGenTableColumn column : table.getSubTable().getColumns())
            {
                if (column.isPk())
                {
                    table.getSubTable().setPkColumn(column);
                    break;
                }
            }
            if (StringUtils.isNull(table.getSubTable().getPkColumn()))
            {
                table.getSubTable().setPkColumn(table.getSubTable().getColumns().get(0));
            }
        }
    }

    /**
     * 设置主子表信息
     *
     * @param table 业务表信息
     */
    public void setSubTable(HgGenTable table)
    {
        String subTableName = table.getSubTableName();
        if (StringUtils.isNotEmpty(subTableName))
        {
            table.setSubTable(hgGenTableMapper.selectGenTableByName(subTableName));
        }
    }

    /**
     * 设置代码生成其他选项值
     *
     * @param hgGenTable 设置后的生成对象
     */
    public void setTableFromOptions(HgGenTable hgGenTable)
    {
        JSONObject paramsObj = JSONObject.parseObject(hgGenTable.getOptions());
        if (StringUtils.isNotNull(paramsObj))
        {
            String treeCode = paramsObj.getString(GenConstants.TREE_CODE);
            String treeParentCode = paramsObj.getString(GenConstants.TREE_PARENT_CODE);
            String treeName = paramsObj.getString(GenConstants.TREE_NAME);
            String parentMenuId = paramsObj.getString(GenConstants.PARENT_MENU_ID);
            String parentMenuName = paramsObj.getString(GenConstants.PARENT_MENU_NAME);

            hgGenTable.setTreeCode(treeCode);
            hgGenTable.setTreeParentCode(treeParentCode);
            hgGenTable.setTreeName(treeName);
            hgGenTable.setParentMenuId(parentMenuId);
            hgGenTable.setParentMenuName(parentMenuName);
        }
    }

    /**
     * 获取代码生成地址
     *
     * @param table 业务表信息
     * @param template 模板文件路径
     * @return 生成地址
     */
    public static String getGenPath(HgGenTable table, String template)
    {
        String genPath = table.getGenPath();
        if (StringUtils.equals(genPath, "/"))
        {
            return System.getProperty("user.dir") + File.separator + "src" + File.separator + VelocityUtils.getFileName(template, table);
        }
        return genPath + File.separator + VelocityUtils.getFileName(template, table);
    }
}