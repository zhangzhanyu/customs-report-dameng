package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 商品出口参数对象 hg_goods_export_param
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgGoodsExportParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 出口商品参数编号 */
    private Long goodsExportParamId;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String goodsCategory;

    /** 商品编码范围 */
    @Excel(name = "商品编码范围")
    private String goodsCodeRange;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String meteringUnit;

    /** 标签1 */
    @Excel(name = "标签1")
    private String tag1;

    /** 标签2 */
    @Excel(name = "标签2")
    private String tag2;

    /** 标签3 */
    @Excel(name = "标签3")
    private String tag3;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setGoodsExportParamId(Long goodsExportParamId) 
    {
        this.goodsExportParamId = goodsExportParamId;
    }

    public Long getGoodsExportParamId() 
    {
        return goodsExportParamId;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setGoodsCodeRange(String goodsCodeRange) 
    {
        this.goodsCodeRange = goodsCodeRange;
    }

    public String getGoodsCodeRange() 
    {
        return goodsCodeRange;
    }
    public void setMeteringUnit(String meteringUnit) 
    {
        this.meteringUnit = meteringUnit;
    }

    public String getMeteringUnit() 
    {
        return meteringUnit;
    }
    public void setTag1(String tag1) 
    {
        this.tag1 = tag1;
    }

    public String getTag1() 
    {
        return tag1;
    }
    public void setTag2(String tag2) 
    {
        this.tag2 = tag2;
    }

    public String getTag2() 
    {
        return tag2;
    }
    public void setTag3(String tag3) 
    {
        this.tag3 = tag3;
    }

    public String getTag3() 
    {
        return tag3;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("goodsExportParamId", getGoodsExportParamId())
            .append("goodsCategory", getGoodsCategory())
            .append("goodsCodeRange", getGoodsCodeRange())
            .append("meteringUnit", getMeteringUnit())
            .append("tag1", getTag1())
            .append("tag2", getTag2())
            .append("tag3", getTag3())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
