package com.qianjing.project.common.mapper;

import java.util.List;
import java.util.Map;

import com.qianjing.project.common.domain.HgHsCodeBook;
import org.apache.ibatis.annotations.Param;

/**
 * HS编码册Mapper接口
 * 
 * @author qianjing
 * @date 2022-08-17
 */
public interface HgHsCodeBookMapper 
{
    /**
     * 查询HS编码册
     * 
     * @param hsCodeBookId HS编码册主键
     * @return HS编码册
     */
    public HgHsCodeBook selectHgHsCodeBookByHsCodeBookId(Long hsCodeBookId);

    /**
     * 查询HS编码册列表
     * 
     * @param hgHsCodeBook HS编码册
     * @return HS编码册集合
     */
    public List<HgHsCodeBook> selectHgHsCodeBookList(HgHsCodeBook hgHsCodeBook);

    /**
     * 新增HS编码册
     * 
     * @param hgHsCodeBook HS编码册
     * @return 结果
     */
    public int insertHgHsCodeBook(HgHsCodeBook hgHsCodeBook);

    /**
     * 修改HS编码册
     * 
     * @param hgHsCodeBook HS编码册
     * @return 结果
     */
    public int updateHgHsCodeBook(HgHsCodeBook hgHsCodeBook);

    /**
     * 删除HS编码册
     * 
     * @param hsCodeBookId HS编码册主键
     * @return 结果
     */
    public int deleteHgHsCodeBookByHsCodeBookId(Long hsCodeBookId);

    /**
     * 批量删除HS编码册
     * 
     * @param hsCodeBookIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgHsCodeBookByHsCodeBookIds(Long[] hsCodeBookIds);

    /**
     * 批量新增HS编码册
     *
     * @param list HS编码册
     * @return 结果
     */
    public int batchInsertHgHsCodeBook(List<HgHsCodeBook> list);

    List<Map<String, Object>> findAllHsCode();

}
