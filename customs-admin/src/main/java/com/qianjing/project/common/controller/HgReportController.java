package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgGenTableColumn;
import com.qianjing.project.common.domain.HgReport;
import com.qianjing.project.common.service.IHgGenTableColumnService;
import com.qianjing.project.common.service.IHgReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 动态报表Controller
 * 
 * @author qianjing
 * @date 2022-09-06
 */
@RestController
@RequestMapping("/common/report")
public class HgReportController extends BaseController
{
    @Autowired
    private IHgReportService hgReportService;

    @Autowired
    private IHgGenTableColumnService hgGenTableColumnService;

    /**
     * 查询动态报表列表
     */
    @PreAuthorize("@ss.hasPermi('common:report:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgReport hgReport)
    {
        List<HgGenTableColumn> hgGenTableColumnList = hgGenTableColumnService.selectGenTableColumnListByTableId(hgReport.getTableId());
        startPage();
        List<HgReport> list = hgReportService.selectHgReportList(hgReport);
        return getDataTable(list,hgGenTableColumnList);
    }

    /**
     * 导出动态报表列表
     */
    @PreAuthorize("@ss.hasPermi('common:report:export')")
    @Log(title = "动态报表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgReport hgReport)
    {
        List<HgReport> list = hgReportService.selectHgReportList(hgReport);
        ExcelUtil<HgReport> util = new ExcelUtil<HgReport>(HgReport.class);
        return util.exportExcel(list, "动态报表数据");
    }

    /**
     * 导入动态报表列表
     */
    @Log(title = "动态报表", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:report:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgReport> util = new ExcelUtil<HgReport>(HgReport.class);
        List<HgReport> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgReportService.importHgReport(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载动态报表导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgReport> util = new ExcelUtil<HgReport>(HgReport.class);
        util.importTemplateExcel(response, "动态报表数据");
    }

    /**
     * 获取动态报表详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:report:query')")
    @GetMapping(value = "/{reportId}")
    public AjaxResult getInfo(@PathVariable("reportId") Long reportId)
    {
        return AjaxResult.success(hgReportService.selectHgReportByReportId(reportId));
    }

    /**
     * 新增动态报表
     */
    @PreAuthorize("@ss.hasPermi('common:report:add')")
    @Log(title = "动态报表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgReport hgReport)
    {
        return toAjax(hgReportService.insertHgReport(hgReport));
    }

    /**
     * 修改动态报表
     */
    @PreAuthorize("@ss.hasPermi('common:report:edit')")
    @Log(title = "动态报表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgReport hgReport)
    {
        return toAjax(hgReportService.updateHgReport(hgReport));
    }

    /**
     * 删除动态报表
     */
    @PreAuthorize("@ss.hasPermi('common:report:remove')")
    @Log(title = "动态报表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{reportIds}")
    public AjaxResult remove(@PathVariable Long[] reportIds)
    {
        return toAjax(hgReportService.deleteHgReportByReportIds(reportIds));
    }
}
