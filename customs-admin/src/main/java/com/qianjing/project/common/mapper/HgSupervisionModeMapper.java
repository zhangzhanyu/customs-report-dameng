package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgSupervisionMode;

/**
 * 监管方式代码Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgSupervisionModeMapper 
{
    /**
     * 查询监管方式代码
     * 
     * @param supervisionModeId 监管方式代码主键
     * @return 监管方式代码
     */
    public HgSupervisionMode selectHgSupervisionModeBySupervisionModeId(Long supervisionModeId);

    /**
     * 查询监管方式代码列表
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 监管方式代码集合
     */
    public List<HgSupervisionMode> selectHgSupervisionModeList(HgSupervisionMode hgSupervisionMode);

    /**
     * 新增监管方式代码
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 结果
     */
    public int insertHgSupervisionMode(HgSupervisionMode hgSupervisionMode);

    /**
     * 修改监管方式代码
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 结果
     */
    public int updateHgSupervisionMode(HgSupervisionMode hgSupervisionMode);

    /**
     * 删除监管方式代码
     * 
     * @param supervisionModeId 监管方式代码主键
     * @return 结果
     */
    public int deleteHgSupervisionModeBySupervisionModeId(Long supervisionModeId);

    /**
     * 批量删除监管方式代码
     * 
     * @param supervisionModeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgSupervisionModeBySupervisionModeIds(Long[] supervisionModeIds);
}
