package com.qianjing.project.common.mapper;

import com.qianjing.project.common.domain.HgGenTable;

import java.util.List;

/**
 * 业务 数据层
 * 
 * @author qianjing
 */
public interface HgGenTableMapper
{
    /**
     * 查询业务列表
     * 
     * @param hgGenTable 业务信息
     * @return 业务集合
     */
    public List<HgGenTable> selectGenTableList(HgGenTable hgGenTable);

    /**
     * 查询据库列表
     * 
     * @param hgGenTable 业务信息
     * @return 数据库表集合
     */
    public List<HgGenTable> selectDbTableList(HgGenTable hgGenTable);

    /**
     * 查询据库列表
     * 
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    public List<HgGenTable> selectDbTableListByNames(String[] tableNames);

    /**
     * 查询所有表信息
     * 
     * @return 表信息集合
     */
    public List<HgGenTable> selectGenTableAll();

    /**
     * 查询表ID业务信息
     * 
     * @param id 业务ID
     * @return 业务信息
     */
    public HgGenTable selectGenTableById(Long id);

    /**
     * 查询表名称业务信息
     * 
     * @param tableName 表名称
     * @return 业务信息
     */
    public HgGenTable selectGenTableByName(String tableName);

    /**
     * 获取菜单id
     * 
     * @return seq_sys_menu.nextval
     */
    public long selectMenuId();

    /**
     * 新增业务
     * 
     * @param hgGenTable 业务信息
     * @return 结果
     */
    public int insertGenTable(HgGenTable hgGenTable);

    /**
     * 修改业务
     * 
     * @param hgGenTable 业务信息
     * @return 结果
     */
    public int updateGenTable(HgGenTable hgGenTable);

    /**
     * 批量删除业务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGenTableByIds(Long[] ids);
}