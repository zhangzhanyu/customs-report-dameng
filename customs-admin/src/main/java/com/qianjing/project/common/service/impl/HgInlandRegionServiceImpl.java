package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgInlandRegionMapper;
import com.qianjing.project.common.domain.HgInlandRegion;
import com.qianjing.project.common.service.IHgInlandRegionService;

/**
 * 国内地区代码Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgInlandRegionServiceImpl implements IHgInlandRegionService 
{
    private static final Logger log = LoggerFactory.getLogger(HgInlandRegionServiceImpl.class);

    @Autowired
    private HgInlandRegionMapper hgInlandRegionMapper;

    /**
     * 查询国内地区代码
     * 
     * @param inlandRegionId 国内地区代码主键
     * @return 国内地区代码
     */
    @Override
    public HgInlandRegion selectHgInlandRegionByInlandRegionId(Long inlandRegionId)
    {
        return hgInlandRegionMapper.selectHgInlandRegionByInlandRegionId(inlandRegionId);
    }

    /**
     * 查询国内地区代码列表
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 国内地区代码
     */
    @Override
    public List<HgInlandRegion> selectHgInlandRegionList(HgInlandRegion hgInlandRegion)
    {
        return hgInlandRegionMapper.selectHgInlandRegionList(hgInlandRegion);
    }

    /**
     * 新增国内地区代码
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 结果
     */
    @Override
    public int insertHgInlandRegion(HgInlandRegion hgInlandRegion)
    {
        hgInlandRegion.setCreateTime(DateUtils.getNowDate());
        return hgInlandRegionMapper.insertHgInlandRegion(hgInlandRegion);
    }

    /**
     * 修改国内地区代码
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 结果
     */
    @Override
    public int updateHgInlandRegion(HgInlandRegion hgInlandRegion)
    {
        hgInlandRegion.setUpdateTime(DateUtils.getNowDate());
        return hgInlandRegionMapper.updateHgInlandRegion(hgInlandRegion);
    }

    /**
     * 批量删除国内地区代码
     * 
     * @param inlandRegionIds 需要删除的国内地区代码主键
     * @return 结果
     */
    @Override
    public int deleteHgInlandRegionByInlandRegionIds(Long[] inlandRegionIds)
    {
        return hgInlandRegionMapper.deleteHgInlandRegionByInlandRegionIds(inlandRegionIds);
    }

    /**
     * 删除国内地区代码信息
     * 
     * @param inlandRegionId 国内地区代码主键
     * @return 结果
     */
    @Override
    public int deleteHgInlandRegionByInlandRegionId(Long inlandRegionId)
    {
        return hgInlandRegionMapper.deleteHgInlandRegionByInlandRegionId(inlandRegionId);
    }

    /**
     * 导入国内地区代码数据
     *
     * @param hgInlandRegionList hgInlandRegionList 国内地区代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgInlandRegion(List<HgInlandRegion> hgInlandRegionList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgInlandRegionList) || hgInlandRegionList.size() == 0) {
            throw new ServiceException("导入国内地区代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgInlandRegion hgInlandRegion : hgInlandRegionList){
            try {
                // 验证是否存在
                if (true) {
                    hgInlandRegion.setCreateBy(operName);
                    this.insertHgInlandRegion(hgInlandRegion);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgInlandRegion.setUpdateBy(operName);
                    this.updateHgInlandRegion(hgInlandRegion);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
