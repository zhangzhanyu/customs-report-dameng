package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgLevelEvaluateMapper;
import com.qianjing.project.common.domain.HgLevelEvaluate;
import com.qianjing.project.common.service.IHgLevelEvaluateService;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 风险分级评估Service业务层处理
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-12-08 下午3:00    张占宇          V1.0          initialize
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HgLevelEvaluateServiceImpl implements IHgLevelEvaluateService {

    private static final Logger log = LoggerFactory.getLogger(HgLevelEvaluateServiceImpl.class);

    private final HgLevelEvaluateMapper hgLevelEvaluateMapper;

    /**
     * 查询风险分级评估
     * 
     * @param id 风险分级评估主键
     * @return 风险分级评估
     */
    @Override
    public HgLevelEvaluate selectHgLevelEvaluateById(Long id) {
        return hgLevelEvaluateMapper.selectHgLevelEvaluateById(id);
    }

    /**
     * 查询风险分级评估列表
     * 
     * @param hgLevelEvaluate 风险分级评估
     * @return 风险分级评估
     */
    @Override
    public List<HgLevelEvaluate> selectHgLevelEvaluateList(HgLevelEvaluate hgLevelEvaluate) {
        return hgLevelEvaluateMapper.selectHgLevelEvaluateList(hgLevelEvaluate);
    }

    /**
     * 新增风险分级评估
     * 
     * @param hgLevelEvaluate 风险分级评估
     * @return 结果
     */
    @Override
    public int insertHgLevelEvaluate(HgLevelEvaluate hgLevelEvaluate) {
        hgLevelEvaluate.setCreateTime(DateUtils.getNowDate());
        return hgLevelEvaluateMapper.insertHgLevelEvaluate(hgLevelEvaluate);
    }

    /**
     * 修改风险分级评估
     * 
     * @param hgLevelEvaluate 风险分级评估
     * @return 结果
     */
    @Override
    public int updateHgLevelEvaluate(HgLevelEvaluate hgLevelEvaluate) {
        hgLevelEvaluate.setUpdateTime(DateUtils.getNowDate());
        return hgLevelEvaluateMapper.updateHgLevelEvaluate(hgLevelEvaluate);
    }

    /**
     * 批量删除风险分级评估
     * 
     * @param ids 需要删除的风险分级评估主键
     * @return 结果
     */
    @Override
    public int deleteHgLevelEvaluateByIds(Long[] ids) {
        return hgLevelEvaluateMapper.deleteHgLevelEvaluateByIds(ids);
    }

    /**
     * 删除风险分级评估信息
     * 
     * @param id 风险分级评估主键
     * @return 结果
     */
    @Override
    public int deleteHgLevelEvaluateById(Long id) {
        return hgLevelEvaluateMapper.deleteHgLevelEvaluateById(id);
    }

    /**
     * 导入风险分级评估数据
     *
     * @param hgLevelEvaluateList hgLevelEvaluateList 风险分级评估信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgLevelEvaluate(List<HgLevelEvaluate> hgLevelEvaluateList, Boolean isUpdateSupport, String operName){
        if (StringUtils.isNull(hgLevelEvaluateList) || hgLevelEvaluateList.size() == 0) {
            throw new ServiceException("导入风险分级评估数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgLevelEvaluate hgLevelEvaluate : hgLevelEvaluateList){
            try {
                // 验证是否存在
                if (true) {
                    hgLevelEvaluate.setCreateBy(operName);
                    this.insertHgLevelEvaluate(hgLevelEvaluate);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgLevelEvaluate.setUpdateBy(operName);
                    this.updateHgLevelEvaluate(hgLevelEvaluate);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
