package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgLevelEvaluate;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 风险分级评估Service接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-12-08 下午3:00    张占宇          V1.0          initialize
 */
public interface IHgLevelEvaluateService {
    /**
     * 查询风险分级评估
     * 
     * @param id 风险分级评估主键
     * @return 风险分级评估
     */
    public HgLevelEvaluate selectHgLevelEvaluateById(Long id);

    /**
     * 查询风险分级评估列表
     * 
     * @param hgLevelEvaluate 风险分级评估
     * @return 风险分级评估集合
     */
    public List<HgLevelEvaluate> selectHgLevelEvaluateList(HgLevelEvaluate hgLevelEvaluate);

    /**
     * 新增风险分级评估
     * 
     * @param hgLevelEvaluate 风险分级评估
     * @return 结果
     */
    public int insertHgLevelEvaluate(HgLevelEvaluate hgLevelEvaluate);

    /**
     * 修改风险分级评估
     * 
     * @param hgLevelEvaluate 风险分级评估
     * @return 结果
     */
    public int updateHgLevelEvaluate(HgLevelEvaluate hgLevelEvaluate);

    /**
     * 批量删除风险分级评估
     * 
     * @param ids 需要删除的风险分级评估主键集合
     * @return 结果
     */
    public int deleteHgLevelEvaluateByIds(Long[] ids);

    /**
     * 删除风险分级评估信息
     * 
     * @param id 风险分级评估主键
     * @return 结果
     */
    public int deleteHgLevelEvaluateById(Long id);

    /**
     * 导入风险分级评估信息
     *
     * @param hgLevelEvaluateList 风险分级评估信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgLevelEvaluate(List<HgLevelEvaluate> hgLevelEvaluateList, Boolean isUpdateSupport, String operName);
}
