package com.qianjing.project.common.util;

import lombok.Data;

@Data
public class SysFileResponse {
    private String fileName;
    private String filePath;
}
