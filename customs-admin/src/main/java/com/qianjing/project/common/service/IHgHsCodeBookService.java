package com.qianjing.project.common.service;

import java.util.List;
import java.util.Map;

import com.qianjing.project.common.domain.HgHsCodeBook;

/**
 * HS编码册Service接口
 * 
 * @author qianjing
 * @date 2022-08-17
 */
public interface IHgHsCodeBookService 
{
    /**
     * 查询HS编码册
     * 
     * @param hsCodeBookId HS编码册主键
     * @return HS编码册
     */
    public HgHsCodeBook selectHgHsCodeBookByHsCodeBookId(Long hsCodeBookId);

    /**
     * 查询HS编码册列表
     * 
     * @param hgHsCodeBook HS编码册
     * @return HS编码册集合
     */
    public List<HgHsCodeBook> selectHgHsCodeBookList(HgHsCodeBook hgHsCodeBook);

    /**
     * 新增HS编码册
     * 
     * @param hgHsCodeBook HS编码册
     * @return 结果
     */
    public int insertHgHsCodeBook(HgHsCodeBook hgHsCodeBook);

    /**
     * 修改HS编码册
     * 
     * @param hgHsCodeBook HS编码册
     * @return 结果
     */
    public int updateHgHsCodeBook(HgHsCodeBook hgHsCodeBook);

    /**
     * 批量删除HS编码册
     * 
     * @param hsCodeBookIds 需要删除的HS编码册主键集合
     * @return 结果
     */
    public int deleteHgHsCodeBookByHsCodeBookIds(Long[] hsCodeBookIds);

    /**
     * 删除HS编码册信息
     * 
     * @param hsCodeBookId HS编码册主键
     * @return 结果
     */
    public int deleteHgHsCodeBookByHsCodeBookId(Long hsCodeBookId);

    /**
     * 导入HS编码册信息
     *
     * @param hgHsCodeBookList HS编码册信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgHsCodeBook(List<HgHsCodeBook> hgHsCodeBookList, Boolean isUpdateSupport, String operName);

    public List<Map<String, Object>> findAllHsCode();
}
