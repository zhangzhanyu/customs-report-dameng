package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgSpecialRegion;

/**
 * 特殊贸易区代码Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgSpecialRegionService 
{
    /**
     * 查询特殊贸易区代码
     * 
     * @param specialRegionId 特殊贸易区代码主键
     * @return 特殊贸易区代码
     */
    public HgSpecialRegion selectHgSpecialRegionBySpecialRegionId(Long specialRegionId);

    /**
     * 查询特殊贸易区代码列表
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 特殊贸易区代码集合
     */
    public List<HgSpecialRegion> selectHgSpecialRegionList(HgSpecialRegion hgSpecialRegion);

    /**
     * 新增特殊贸易区代码
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 结果
     */
    public int insertHgSpecialRegion(HgSpecialRegion hgSpecialRegion);

    /**
     * 修改特殊贸易区代码
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 结果
     */
    public int updateHgSpecialRegion(HgSpecialRegion hgSpecialRegion);

    /**
     * 批量删除特殊贸易区代码
     * 
     * @param specialRegionIds 需要删除的特殊贸易区代码主键集合
     * @return 结果
     */
    public int deleteHgSpecialRegionBySpecialRegionIds(Long[] specialRegionIds);

    /**
     * 删除特殊贸易区代码信息
     * 
     * @param specialRegionId 特殊贸易区代码主键
     * @return 结果
     */
    public int deleteHgSpecialRegionBySpecialRegionId(Long specialRegionId);

    /**
     * 导入特殊贸易区代码信息
     *
     * @param hgSpecialRegionList 特殊贸易区代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgSpecialRegion(List<HgSpecialRegion> hgSpecialRegionList, Boolean isUpdateSupport, String operName);
}
