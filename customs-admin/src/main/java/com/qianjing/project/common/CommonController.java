package com.qianjing.project.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.common.base.Joiner;
import com.qianjing.common.utils.IdUtils;
import com.qianjing.project.common.util.CustomersFtpProperties;
import com.qianjing.project.common.util.FTPClientFactory;
import com.qianjing.project.common.util.FTPClientPool;
import com.qianjing.project.common.util.FTPClientUtils;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.qianjing.common.constant.Constants;
import com.qianjing.common.utils.StringUtils;
import com.qianjing.common.utils.file.FileUtils;
import com.qianjing.framework.config.QianJingConfig;
import com.qianjing.framework.web.domain.AjaxResult;

/**
 * 通用请求处理
 * 
 * @author qianjing
 */
@RestController
public class CommonController {

    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Resource
    private CustomersFtpProperties customersFtpProperties;

    /**
     * 通用下载请求
     * 
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("common/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request) {
        try {
            if (!FileUtils.checkAllowDownload(fileName)) {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = QianJingConfig.getDownloadPath() + fileName;

            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, realFileName);
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete) {
                FileUtils.deleteFile(filePath);
            }
        } catch (Exception e) {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    public AjaxResult uploadFile(MultipartFile file) throws Exception {
        try {
            // 获取原始文件
            String originalFilename = file.getOriginalFilename();
            // 获取文件后缀
            String suffix = originalFilename.substring(originalFilename.lastIndexOf('.'));
            // 生成新文件名
            String fileName = IdUtils.simpleUUID() + suffix;
            // 构造存储路径
            String filePath = customersFtpProperties.getRemotePath();
            // 客户端工厂
            FTPClientFactory factory = new FTPClientFactory(customersFtpProperties);
            // 连接池对象
            FTPClientPool pool = new FTPClientPool(factory);
            // 客户端工具
            FTPClientUtils ftpClientUtils = new FTPClientUtils(pool);
            ftpClientUtils.store(file.getInputStream(), filePath, fileName);
            String url = Joiner.on("/").join(filePath,fileName);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/common/download/resource")
    public void resourceDownload(String resource, HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        try
        {
            if (!FileUtils.checkAllowDownload(resource))
            {
                throw new Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", resource));
            }
            // 本地资源路径
            String localPath = QianJingConfig.getProfile();
            // 数据库资源地址
            String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
            // 下载名称
            String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, downloadName);
            FileUtils.writeBytes(downloadPath, response.getOutputStream());
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 文件下载
     * @param response
     * @throws IOException
     */
    @ApiOperation(value = "文件下载")
    @RequestMapping("/common/getImage")
    public void getImageBinary(@RequestParam(value = "url") String url, HttpServletResponse response) throws Exception {
        String fileName= url.substring(url.lastIndexOf("/")+1);
        // 客户端工厂
        FTPClientFactory factory = new FTPClientFactory(customersFtpProperties);
        // 连接池对象
        FTPClientPool pool = new FTPClientPool(factory);
        // 客户端工具
        FTPClientUtils ftpClientUtils = new FTPClientUtils(pool);
        ftpClientUtils.setAttachmentResponseHeader(response,fileName);
        ftpClientUtils.retrieve(url, response.getOutputStream());
    }
}
