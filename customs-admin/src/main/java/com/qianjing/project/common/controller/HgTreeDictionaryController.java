package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgTreeDictionary;
import com.qianjing.project.common.service.IHgTreeDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 字典（树形）Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/dictionary")
public class HgTreeDictionaryController extends BaseController
{
    @Autowired
    private IHgTreeDictionaryService hgTreeDictionaryService;

    /**
     * 查询字典（树形）列表
     */
    @PreAuthorize("@ss.hasPermi('common:dictionary:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgTreeDictionary hgTreeDictionary)
    {
        startPage();
        List<HgTreeDictionary> list = hgTreeDictionaryService.selectHgTreeDictionaryList(hgTreeDictionary);
        return getDataTable(list);
    }

    /**
     * 导出字典（树形）列表
     */
    @PreAuthorize("@ss.hasPermi('common:dictionary:export')")
    @Log(title = "字典（树形）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgTreeDictionary hgTreeDictionary)
    {
        List<HgTreeDictionary> list = hgTreeDictionaryService.selectHgTreeDictionaryList(hgTreeDictionary);
        ExcelUtil<HgTreeDictionary> util = new ExcelUtil<HgTreeDictionary>(HgTreeDictionary.class);
        return util.exportExcel(list, "字典（树形）数据");
    }

    /**
     * 导入字典（树形）列表
     */
    @Log(title = "字典（树形）", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:dictionary:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgTreeDictionary> util = new ExcelUtil<HgTreeDictionary>(HgTreeDictionary.class);
        List<HgTreeDictionary> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgTreeDictionaryService.importHgTreeDictionary(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载字典（树形）导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgTreeDictionary> util = new ExcelUtil<HgTreeDictionary>(HgTreeDictionary.class);
        util.importTemplateExcel(response, "字典（树形）数据");
    }

    /**
     * 获取字典（树形）详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:dictionary:query')")
    @GetMapping(value = "/{treeDicId}")
    public AjaxResult getInfo(@PathVariable("treeDicId") Long treeDicId)
    {
        return AjaxResult.success(hgTreeDictionaryService.selectHgTreeDictionaryByTreeDicId(treeDicId));
    }

    /**
     * 新增字典（树形）
     */
    @PreAuthorize("@ss.hasPermi('common:dictionary:add')")
    @Log(title = "字典（树形）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgTreeDictionary hgTreeDictionary)
    {
        return toAjax(hgTreeDictionaryService.insertHgTreeDictionary(hgTreeDictionary));
    }

    /**
     * 修改字典（树形）
     */
    @PreAuthorize("@ss.hasPermi('common:dictionary:edit')")
    @Log(title = "字典（树形）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgTreeDictionary hgTreeDictionary)
    {
        return toAjax(hgTreeDictionaryService.updateHgTreeDictionary(hgTreeDictionary));
    }

    /**
     * 删除字典（树形）
     */
    @PreAuthorize("@ss.hasPermi('common:dictionary:remove')")
    @Log(title = "字典（树形）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{treeDicIds}")
    public AjaxResult remove(@PathVariable Long[] treeDicIds)
    {
        return toAjax(hgTreeDictionaryService.deleteHgTreeDictionaryByTreeDicIds(treeDicIds));
    }
}
