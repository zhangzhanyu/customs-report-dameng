package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgGoodsCatalogMapper;
import com.qianjing.project.common.domain.HgGoodsCatalog;
import com.qianjing.project.common.service.IHgGoodsCatalogService;

/**
 * 商品目录Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgGoodsCatalogServiceImpl implements IHgGoodsCatalogService 
{
    private static final Logger log = LoggerFactory.getLogger(HgGoodsCatalogServiceImpl.class);

    @Autowired
    private HgGoodsCatalogMapper hgGoodsCatalogMapper;

    /**
     * 查询商品目录
     * 
     * @param goodsCatalogId 商品目录主键
     * @return 商品目录
     */
    @Override
    public HgGoodsCatalog selectHgGoodsCatalogByGoodsCatalogId(Long goodsCatalogId)
    {
        return hgGoodsCatalogMapper.selectHgGoodsCatalogByGoodsCatalogId(goodsCatalogId);
    }

    /**
     * 查询商品目录列表
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 商品目录
     */
    @Override
    public List<HgGoodsCatalog> selectHgGoodsCatalogList(HgGoodsCatalog hgGoodsCatalog)
    {
        return hgGoodsCatalogMapper.selectHgGoodsCatalogList(hgGoodsCatalog);
    }

    /**
     * 新增商品目录
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 结果
     */
    @Override
    public int insertHgGoodsCatalog(HgGoodsCatalog hgGoodsCatalog)
    {
        hgGoodsCatalog.setCreateTime(DateUtils.getNowDate());
        return hgGoodsCatalogMapper.insertHgGoodsCatalog(hgGoodsCatalog);
    }

    /**
     * 修改商品目录
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 结果
     */
    @Override
    public int updateHgGoodsCatalog(HgGoodsCatalog hgGoodsCatalog)
    {
        hgGoodsCatalog.setUpdateTime(DateUtils.getNowDate());
        return hgGoodsCatalogMapper.updateHgGoodsCatalog(hgGoodsCatalog);
    }

    /**
     * 批量删除商品目录
     * 
     * @param goodsCatalogIds 需要删除的商品目录主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsCatalogByGoodsCatalogIds(Long[] goodsCatalogIds)
    {
        return hgGoodsCatalogMapper.deleteHgGoodsCatalogByGoodsCatalogIds(goodsCatalogIds);
    }

    /**
     * 删除商品目录信息
     * 
     * @param goodsCatalogId 商品目录主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsCatalogByGoodsCatalogId(Long goodsCatalogId)
    {
        return hgGoodsCatalogMapper.deleteHgGoodsCatalogByGoodsCatalogId(goodsCatalogId);
    }

    /**
     * 导入商品目录数据
     *
     * @param hgGoodsCatalogList hgGoodsCatalogList 商品目录信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgGoodsCatalog(List<HgGoodsCatalog> hgGoodsCatalogList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgGoodsCatalogList) || hgGoodsCatalogList.size() == 0) {
            throw new ServiceException("导入商品目录数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgGoodsCatalog hgGoodsCatalog : hgGoodsCatalogList){
            try {
                // 验证是否存在
                if (true) {
                    hgGoodsCatalog.setCreateBy(operName);
                    this.insertHgGoodsCatalog(hgGoodsCatalog);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgGoodsCatalog.setUpdateBy(operName);
                    this.updateHgGoodsCatalog(hgGoodsCatalog);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
