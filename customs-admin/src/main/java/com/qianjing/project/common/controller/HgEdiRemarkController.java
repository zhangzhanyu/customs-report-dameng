package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgEdiRemark;
import com.qianjing.project.common.service.IHgEdiRemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * EDI申报备注Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/remark")
public class HgEdiRemarkController extends BaseController
{
    @Autowired
    private IHgEdiRemarkService hgEdiRemarkService;

    /**
     * 查询EDI申报备注列表
     */
    @PreAuthorize("@ss.hasPermi('common:remark:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgEdiRemark hgEdiRemark)
    {
        startPage();
        List<HgEdiRemark> list = hgEdiRemarkService.selectHgEdiRemarkList(hgEdiRemark);
        return getDataTable(list);
    }

    /**
     * 导出EDI申报备注列表
     */
    @PreAuthorize("@ss.hasPermi('common:remark:export')")
    @Log(title = "EDI申报备注", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgEdiRemark hgEdiRemark)
    {
        List<HgEdiRemark> list = hgEdiRemarkService.selectHgEdiRemarkList(hgEdiRemark);
        ExcelUtil<HgEdiRemark> util = new ExcelUtil<HgEdiRemark>(HgEdiRemark.class);
        return util.exportExcel(list, "EDI申报备注数据");
    }

    /**
     * 导入EDI申报备注列表
     */
    @Log(title = "EDI申报备注", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:remark:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgEdiRemark> util = new ExcelUtil<HgEdiRemark>(HgEdiRemark.class);
        List<HgEdiRemark> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgEdiRemarkService.importHgEdiRemark(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载EDI申报备注导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgEdiRemark> util = new ExcelUtil<HgEdiRemark>(HgEdiRemark.class);
        util.importTemplateExcel(response, "EDI申报备注数据");
    }

    /**
     * 获取EDI申报备注详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:remark:query')")
    @GetMapping(value = "/{ediId}")
    public AjaxResult getInfo(@PathVariable("ediId") Long ediId)
    {
        return AjaxResult.success(hgEdiRemarkService.selectHgEdiRemarkByEdiId(ediId));
    }

    /**
     * 新增EDI申报备注
     */
    @PreAuthorize("@ss.hasPermi('common:remark:add')")
    @Log(title = "EDI申报备注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgEdiRemark hgEdiRemark)
    {
        return toAjax(hgEdiRemarkService.insertHgEdiRemark(hgEdiRemark));
    }

    /**
     * 修改EDI申报备注
     */
    @PreAuthorize("@ss.hasPermi('common:remark:edit')")
    @Log(title = "EDI申报备注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgEdiRemark hgEdiRemark)
    {
        return toAjax(hgEdiRemarkService.updateHgEdiRemark(hgEdiRemark));
    }

    /**
     * 删除EDI申报备注
     */
    @PreAuthorize("@ss.hasPermi('common:remark:remove')")
    @Log(title = "EDI申报备注", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ediIds}")
    public AjaxResult remove(@PathVariable Long[] ediIds)
    {
        return toAjax(hgEdiRemarkService.deleteHgEdiRemarkByEdiIds(ediIds));
    }
}
