package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgHsCatalog;
import com.qianjing.project.common.service.IHgHsCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 体系类章对照Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/hsCatalog")
public class HgHsCatalogController extends BaseController
{
    @Autowired
    private IHgHsCatalogService hgHsCatalogService;

    /**
     * 查询体系类章对照列表
     */
    @PreAuthorize("@ss.hasPermi('common:hsCatalog:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgHsCatalog hgHsCatalog)
    {
        startPage();
        List<HgHsCatalog> list = hgHsCatalogService.selectHgHsCatalogList(hgHsCatalog);
        return getDataTable(list);
    }

    /**
     * 导出体系类章对照列表
     */
    @PreAuthorize("@ss.hasPermi('common:hsCatalog:export')")
    @Log(title = "体系类章对照", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgHsCatalog hgHsCatalog)
    {
        List<HgHsCatalog> list = hgHsCatalogService.selectHgHsCatalogList(hgHsCatalog);
        ExcelUtil<HgHsCatalog> util = new ExcelUtil<HgHsCatalog>(HgHsCatalog.class);
        return util.exportExcel(list, "体系类章对照数据");
    }

    /**
     * 导入体系类章对照列表
     */
    @Log(title = "体系类章对照", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:hsCatalog:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgHsCatalog> util = new ExcelUtil<HgHsCatalog>(HgHsCatalog.class);
        List<HgHsCatalog> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgHsCatalogService.importHgHsCatalog(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载体系类章对照导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgHsCatalog> util = new ExcelUtil<HgHsCatalog>(HgHsCatalog.class);
        util.importTemplateExcel(response, "体系类章对照数据");
    }

    /**
     * 获取体系类章对照详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:hsCatalog:query')")
    @GetMapping(value = "/{hsCatalogId}")
    public AjaxResult getInfo(@PathVariable("hsCatalogId") Long hsCatalogId)
    {
        return AjaxResult.success(hgHsCatalogService.selectHgHsCatalogByHsCatalogId(hsCatalogId));
    }

    /**
     * 新增体系类章对照
     */
    @PreAuthorize("@ss.hasPermi('common:hsCatalog:add')")
    @Log(title = "体系类章对照", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgHsCatalog hgHsCatalog)
    {
        return toAjax(hgHsCatalogService.insertHgHsCatalog(hgHsCatalog));
    }

    /**
     * 修改体系类章对照
     */
    @PreAuthorize("@ss.hasPermi('common:hsCatalog:edit')")
    @Log(title = "体系类章对照", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgHsCatalog hgHsCatalog)
    {
        return toAjax(hgHsCatalogService.updateHgHsCatalog(hgHsCatalog));
    }

    /**
     * 删除体系类章对照
     */
    @PreAuthorize("@ss.hasPermi('common:hsCatalog:remove')")
    @Log(title = "体系类章对照", businessType = BusinessType.DELETE)
	@DeleteMapping("/{hsCatalogIds}")
    public AjaxResult remove(@PathVariable Long[] hsCatalogIds)
    {
        return toAjax(hgHsCatalogService.deleteHgHsCatalogByHsCatalogIds(hsCatalogIds));
    }
}
