package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgCountry;

/**
 * 国别地区代码Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgCountryMapper 
{
    /**
     * 查询国别地区代码
     * 
     * @param countryId 国别地区代码主键
     * @return 国别地区代码
     */
    public HgCountry selectHgCountryByCountryId(Long countryId);

    /**
     * 查询国别地区代码列表
     * 
     * @param hgCountry 国别地区代码
     * @return 国别地区代码集合
     */
    public List<HgCountry> selectHgCountryList(HgCountry hgCountry);

    /**
     * 新增国别地区代码
     * 
     * @param hgCountry 国别地区代码
     * @return 结果
     */
    public int insertHgCountry(HgCountry hgCountry);

    /**
     * 修改国别地区代码
     * 
     * @param hgCountry 国别地区代码
     * @return 结果
     */
    public int updateHgCountry(HgCountry hgCountry);

    /**
     * 删除国别地区代码
     * 
     * @param countryId 国别地区代码主键
     * @return 结果
     */
    public int deleteHgCountryByCountryId(Long countryId);

    /**
     * 批量删除国别地区代码
     * 
     * @param countryIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgCountryByCountryIds(Long[] countryIds);
}
