package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 代码对象 hg_sys_code
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgSysCode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 代码id */
    private Long codeId;

    /** 系统a */
    @Excel(name = "系统a")
    private String systemA;

    /** 系统b */
    @Excel(name = "系统b")
    private String systemB;

    /** 系统C */
    @Excel(name = "系统C")
    private String systemC;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String nameEn;

    /** 代码类型（1：币制代码、2：港口代码，3：集装箱规格代码） */
    @Excel(name = "代码类型", readConverterExp = "1=：币制代码、2：港口代码，3：集装箱规格代码")
    private String codeType;

    /** 标签1 */
    @Excel(name = "标签1")
    private String tag1;

    /** 标签2 */
    @Excel(name = "标签2")
    private String tag2;

    /** 标签3 */
    @Excel(name = "标签3")
    private String tag3;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setCodeId(Long codeId) 
    {
        this.codeId = codeId;
    }

    public Long getCodeId() 
    {
        return codeId;
    }
    public void setSystemA(String systemA) 
    {
        this.systemA = systemA;
    }

    public String getSystemA() 
    {
        return systemA;
    }
    public void setSystemB(String systemB) 
    {
        this.systemB = systemB;
    }

    public String getSystemB() 
    {
        return systemB;
    }
    public void setSystemC(String systemC) 
    {
        this.systemC = systemC;
    }

    public String getSystemC() 
    {
        return systemC;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNameEn(String nameEn) 
    {
        this.nameEn = nameEn;
    }

    public String getNameEn() 
    {
        return nameEn;
    }
    public void setCodeType(String codeType) 
    {
        this.codeType = codeType;
    }

    public String getCodeType() 
    {
        return codeType;
    }
    public void setTag1(String tag1) 
    {
        this.tag1 = tag1;
    }

    public String getTag1() 
    {
        return tag1;
    }
    public void setTag2(String tag2) 
    {
        this.tag2 = tag2;
    }

    public String getTag2() 
    {
        return tag2;
    }
    public void setTag3(String tag3) 
    {
        this.tag3 = tag3;
    }

    public String getTag3() 
    {
        return tag3;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("codeId", getCodeId())
            .append("systemA", getSystemA())
            .append("systemB", getSystemB())
            .append("systemC", getSystemC())
            .append("name", getName())
            .append("nameEn", getNameEn())
            .append("codeType", getCodeType())
            .append("tag1", getTag1())
            .append("tag2", getTag2())
            .append("tag3", getTag3())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
