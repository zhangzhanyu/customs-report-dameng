package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgInlandRegion;

/**
 * 国内地区代码Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgInlandRegionMapper 
{
    /**
     * 查询国内地区代码
     * 
     * @param inlandRegionId 国内地区代码主键
     * @return 国内地区代码
     */
    public HgInlandRegion selectHgInlandRegionByInlandRegionId(Long inlandRegionId);

    /**
     * 查询国内地区代码列表
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 国内地区代码集合
     */
    public List<HgInlandRegion> selectHgInlandRegionList(HgInlandRegion hgInlandRegion);

    /**
     * 新增国内地区代码
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 结果
     */
    public int insertHgInlandRegion(HgInlandRegion hgInlandRegion);

    /**
     * 修改国内地区代码
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 结果
     */
    public int updateHgInlandRegion(HgInlandRegion hgInlandRegion);

    /**
     * 删除国内地区代码
     * 
     * @param inlandRegionId 国内地区代码主键
     * @return 结果
     */
    public int deleteHgInlandRegionByInlandRegionId(Long inlandRegionId);

    /**
     * 批量删除国内地区代码
     * 
     * @param inlandRegionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgInlandRegionByInlandRegionIds(Long[] inlandRegionIds);
}
