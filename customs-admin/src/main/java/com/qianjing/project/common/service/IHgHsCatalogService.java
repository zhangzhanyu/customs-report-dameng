package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgHsCatalog;

/**
 * 体系类章对照Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgHsCatalogService 
{
    /**
     * 查询体系类章对照
     * 
     * @param hsCatalogId 体系类章对照主键
     * @return 体系类章对照
     */
    public HgHsCatalog selectHgHsCatalogByHsCatalogId(Long hsCatalogId);

    /**
     * 查询体系类章对照列表
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 体系类章对照集合
     */
    public List<HgHsCatalog> selectHgHsCatalogList(HgHsCatalog hgHsCatalog);

    /**
     * 新增体系类章对照
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 结果
     */
    public int insertHgHsCatalog(HgHsCatalog hgHsCatalog);

    /**
     * 修改体系类章对照
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 结果
     */
    public int updateHgHsCatalog(HgHsCatalog hgHsCatalog);

    /**
     * 批量删除体系类章对照
     * 
     * @param hsCatalogIds 需要删除的体系类章对照主键集合
     * @return 结果
     */
    public int deleteHgHsCatalogByHsCatalogIds(Long[] hsCatalogIds);

    /**
     * 删除体系类章对照信息
     * 
     * @param hsCatalogId 体系类章对照主键
     * @return 结果
     */
    public int deleteHgHsCatalogByHsCatalogId(Long hsCatalogId);

    /**
     * 导入体系类章对照信息
     *
     * @param hgHsCatalogList 体系类章对照信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgHsCatalog(List<HgHsCatalog> hgHsCatalogList, Boolean isUpdateSupport, String operName);
}
