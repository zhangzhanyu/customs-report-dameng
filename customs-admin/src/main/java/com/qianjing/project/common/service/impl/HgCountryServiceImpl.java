package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgCountryMapper;
import com.qianjing.project.common.domain.HgCountry;
import com.qianjing.project.common.service.IHgCountryService;

/**
 * 国别地区代码Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgCountryServiceImpl implements IHgCountryService 
{
    private static final Logger log = LoggerFactory.getLogger(HgCountryServiceImpl.class);

    @Autowired
    private HgCountryMapper hgCountryMapper;

    /**
     * 查询国别地区代码
     * 
     * @param countryId 国别地区代码主键
     * @return 国别地区代码
     */
    @Override
    public HgCountry selectHgCountryByCountryId(Long countryId)
    {
        return hgCountryMapper.selectHgCountryByCountryId(countryId);
    }

    /**
     * 查询国别地区代码列表
     * 
     * @param hgCountry 国别地区代码
     * @return 国别地区代码
     */
    @Override
    public List<HgCountry> selectHgCountryList(HgCountry hgCountry)
    {
        return hgCountryMapper.selectHgCountryList(hgCountry);
    }

    /**
     * 新增国别地区代码
     * 
     * @param hgCountry 国别地区代码
     * @return 结果
     */
    @Override
    public int insertHgCountry(HgCountry hgCountry)
    {
        hgCountry.setCreateTime(DateUtils.getNowDate());
        return hgCountryMapper.insertHgCountry(hgCountry);
    }

    /**
     * 修改国别地区代码
     * 
     * @param hgCountry 国别地区代码
     * @return 结果
     */
    @Override
    public int updateHgCountry(HgCountry hgCountry)
    {
        hgCountry.setUpdateTime(DateUtils.getNowDate());
        return hgCountryMapper.updateHgCountry(hgCountry);
    }

    /**
     * 批量删除国别地区代码
     * 
     * @param countryIds 需要删除的国别地区代码主键
     * @return 结果
     */
    @Override
    public int deleteHgCountryByCountryIds(Long[] countryIds)
    {
        return hgCountryMapper.deleteHgCountryByCountryIds(countryIds);
    }

    /**
     * 删除国别地区代码信息
     * 
     * @param countryId 国别地区代码主键
     * @return 结果
     */
    @Override
    public int deleteHgCountryByCountryId(Long countryId)
    {
        return hgCountryMapper.deleteHgCountryByCountryId(countryId);
    }

    /**
     * 导入国别地区代码数据
     *
     * @param hgCountryList hgCountryList 国别地区代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgCountry(List<HgCountry> hgCountryList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgCountryList) || hgCountryList.size() == 0) {
            throw new ServiceException("导入国别地区代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgCountry hgCountry : hgCountryList){
            try {
                // 验证是否存在
                if (true) {
                    hgCountry.setCreateBy(operName);
                    this.insertHgCountry(hgCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgCountry.setUpdateBy(operName);
                    this.updateHgCountry(hgCountry);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
