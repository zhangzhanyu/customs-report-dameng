package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 征免性质代码对象 hg_exemption_nature
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgExemptionNature extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 征免性质编号 */
    private Long exemptionNatureId;

    /** CUT_MODE */
    @Excel(name = "CUT_MODE")
    private String cutMode;

    /** ABBR_CUT */
    @Excel(name = "ABBR_CUT")
    private String abbrCut;

    /** FULL_CUT */
    @Excel(name = "FULL_CUT")
    private String fullCut;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setExemptionNatureId(Long exemptionNatureId) 
    {
        this.exemptionNatureId = exemptionNatureId;
    }

    public Long getExemptionNatureId() 
    {
        return exemptionNatureId;
    }
    public void setCutMode(String cutMode) 
    {
        this.cutMode = cutMode;
    }

    public String getCutMode() 
    {
        return cutMode;
    }
    public void setAbbrCut(String abbrCut) 
    {
        this.abbrCut = abbrCut;
    }

    public String getAbbrCut() 
    {
        return abbrCut;
    }
    public void setFullCut(String fullCut) 
    {
        this.fullCut = fullCut;
    }

    public String getFullCut() 
    {
        return fullCut;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("exemptionNatureId", getExemptionNatureId())
            .append("cutMode", getCutMode())
            .append("abbrCut", getAbbrCut())
            .append("fullCut", getFullCut())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("remarks", getRemarks())
            .toString();
    }
}
