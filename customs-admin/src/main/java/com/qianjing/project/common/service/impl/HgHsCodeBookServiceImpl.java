package com.qianjing.project.common.service.impl;

import java.util.List;
import java.util.Map;

import com.qianjing.common.utils.DateUtils;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgHsCodeBookMapper;
import com.qianjing.project.common.domain.HgHsCodeBook;
import com.qianjing.project.common.service.IHgHsCodeBookService;

/**
 * HS编码册Service业务层处理
 *
 * @author qianjing
 * @date 2022-08-17
 */
@Service
public class HgHsCodeBookServiceImpl implements IHgHsCodeBookService
{
    private static final Logger log = LoggerFactory.getLogger(HgHsCodeBookServiceImpl.class);

    @Autowired
    private HgHsCodeBookMapper hgHsCodeBookMapper;

    /**
     * 查询HS编码册
     *
     * @param hsCodeBookId HS编码册主键
     * @return HS编码册
     */
    @Override
    public HgHsCodeBook selectHgHsCodeBookByHsCodeBookId(Long hsCodeBookId)
    {
        return hgHsCodeBookMapper.selectHgHsCodeBookByHsCodeBookId(hsCodeBookId);
    }

    /**
     * 查询HS编码册列表
     *
     * @param hgHsCodeBook HS编码册
     * @return HS编码册
     */
    @Override
    public List<HgHsCodeBook> selectHgHsCodeBookList(HgHsCodeBook hgHsCodeBook)
    {
        return hgHsCodeBookMapper.selectHgHsCodeBookList(hgHsCodeBook);
    }

    /**
     * 新增HS编码册
     *
     * @param hgHsCodeBook HS编码册
     * @return 结果
     */
    @Override
    public int insertHgHsCodeBook(HgHsCodeBook hgHsCodeBook)
    {
        hgHsCodeBook.setCreateTime(DateUtils.getNowDate());
        return hgHsCodeBookMapper.insertHgHsCodeBook(hgHsCodeBook);
    }

    /**
     * 修改HS编码册
     *
     * @param hgHsCodeBook HS编码册
     * @return 结果
     */
    @Override
    public int updateHgHsCodeBook(HgHsCodeBook hgHsCodeBook)
    {
        hgHsCodeBook.setUpdateTime(DateUtils.getNowDate());
        return hgHsCodeBookMapper.updateHgHsCodeBook(hgHsCodeBook);
    }

    /**
     * 批量删除HS编码册
     *
     * @param hsCodeBookIds 需要删除的HS编码册主键
     * @return 结果
     */
    @Override
    public int deleteHgHsCodeBookByHsCodeBookIds(Long[] hsCodeBookIds)
    {
        return hgHsCodeBookMapper.deleteHgHsCodeBookByHsCodeBookIds(hsCodeBookIds);
    }

    /**
     * 删除HS编码册信息
     *
     * @param hsCodeBookId HS编码册主键
     * @return 结果
     */
    @Override
    public int deleteHgHsCodeBookByHsCodeBookId(Long hsCodeBookId)
    {
        return hgHsCodeBookMapper.deleteHgHsCodeBookByHsCodeBookId(hsCodeBookId);
    }

    /**
     * 导入HS编码册数据
     *
     * @param hgHsCodeBookList hgHsCodeBookList HS编码册信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgHsCodeBook(List<HgHsCodeBook> hgHsCodeBookList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgHsCodeBookList) || hgHsCodeBookList.size() == 0) {
            throw new ServiceException("导入HS编码册数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgHsCodeBook hgHsCodeBook : hgHsCodeBookList){
            try {
                // 验证是否存在
                if (true) {
                    hgHsCodeBook.setCreateBy(operName);
                    this.insertHgHsCodeBook(hgHsCodeBook);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgHsCodeBook.setUpdateBy(operName);
                    this.updateHgHsCodeBook(hgHsCodeBook);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    @Override
    public List<Map<String, Object>> findAllHsCode() {
        return hgHsCodeBookMapper.findAllHsCode();
    }

}
