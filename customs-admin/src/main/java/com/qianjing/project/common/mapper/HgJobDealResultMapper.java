package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgJobDealResult;

/**
 * 检查作业处理结果清单Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgJobDealResultMapper 
{
    /**
     * 查询检查作业处理结果清单
     * 
     * @param jobDealResultId 检查作业处理结果清单主键
     * @return 检查作业处理结果清单
     */
    public HgJobDealResult selectHgJobDealResultByJobDealResultId(Long jobDealResultId);

    /**
     * 查询检查作业处理结果清单列表
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 检查作业处理结果清单集合
     */
    public List<HgJobDealResult> selectHgJobDealResultList(HgJobDealResult hgJobDealResult);

    /**
     * 新增检查作业处理结果清单
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 结果
     */
    public int insertHgJobDealResult(HgJobDealResult hgJobDealResult);

    /**
     * 修改检查作业处理结果清单
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 结果
     */
    public int updateHgJobDealResult(HgJobDealResult hgJobDealResult);

    /**
     * 删除检查作业处理结果清单
     * 
     * @param jobDealResultId 检查作业处理结果清单主键
     * @return 结果
     */
    public int deleteHgJobDealResultByJobDealResultId(Long jobDealResultId);

    /**
     * 批量删除检查作业处理结果清单
     * 
     * @param jobDealResultIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgJobDealResultByJobDealResultIds(Long[] jobDealResultIds);
}
