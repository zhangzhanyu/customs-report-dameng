package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.project.common.domain.HgGoodsClassificationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgGoodsClassificationMapper;
import com.qianjing.project.common.domain.HgGoodsClassification;
import com.qianjing.project.common.service.IHgGoodsClassificationService;

/**
 * 商品分类代码Service业务层处理
 *
 * @author zhangzy
 * @date 2022-08-02
 */
@Service
public class HgGoodsClassificationServiceImpl implements IHgGoodsClassificationService
{
    private static final Logger log = LoggerFactory.getLogger(HgGoodsClassificationServiceImpl.class);

    @Autowired
    private HgGoodsClassificationMapper hgGoodsClassificationMapper;

    /**
     * 查询商品分类代码
     *
     * @param goodsClassificationId 商品分类代码主键
     * @return 商品分类代码
     */
    @Override
    public HgGoodsClassification selectHgGoodsClassificationByGoodsClassificationId(Long goodsClassificationId)
    {
        return hgGoodsClassificationMapper.selectHgGoodsClassificationByGoodsClassificationId(goodsClassificationId);
    }

    /**
     * 查询商品分类代码列表
     *
     * @param hgGoodsClassification 商品分类代码
     * @return 商品分类代码
     */
    @Override
    public List<HgGoodsClassification> selectHgGoodsClassificationList(HgGoodsClassification hgGoodsClassification)
    {
        return hgGoodsClassificationMapper.selectHgGoodsClassificationList(hgGoodsClassification);
    }

    /**
     * 新增商品分类代码
     *
     * @param hgGoodsClassification 商品分类代码
     * @return 结果
     */
    @Override
    public int insertHgGoodsClassification(HgGoodsClassification hgGoodsClassification)
    {
        hgGoodsClassification.setCreateTime(DateUtils.getNowDate());
        return hgGoodsClassificationMapper.insertHgGoodsClassification(hgGoodsClassification);
    }

    /**
     * 修改商品分类代码
     *
     * @param hgGoodsClassification 商品分类代码
     * @return 结果
     */
    @Override
    public int updateHgGoodsClassification(HgGoodsClassification hgGoodsClassification)
    {
        hgGoodsClassification.setUpdateTime(DateUtils.getNowDate());
        return hgGoodsClassificationMapper.updateHgGoodsClassification(hgGoodsClassification);
    }

    /**
     * 批量删除商品分类代码
     *
     * @param goodsClassificationIds 需要删除的商品分类代码主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsClassificationByGoodsClassificationIds(Long[] goodsClassificationIds)
    {
        return hgGoodsClassificationMapper.deleteHgGoodsClassificationByGoodsClassificationIds(goodsClassificationIds);
    }

    /**
     * 删除商品分类代码信息
     *
     * @param goodsClassificationId 商品分类代码主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsClassificationByGoodsClassificationId(Long goodsClassificationId)
    {
        return hgGoodsClassificationMapper.deleteHgGoodsClassificationByGoodsClassificationId(goodsClassificationId);
    }

    /**
     * 导入商品分类代码数据
     *
     * @param hgGoodsClassificationList hgGoodsClassificationList 商品分类代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgGoodsClassification(List<HgGoodsClassification> hgGoodsClassificationList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgGoodsClassificationList) || hgGoodsClassificationList.size() == 0) {
            throw new ServiceException("导入商品分类代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgGoodsClassification hgGoodsClassification : hgGoodsClassificationList){
            try {
                // 验证是否存在
                if (true) {
                    hgGoodsClassification.setCreateBy(operName);
                    this.insertHgGoodsClassification(hgGoodsClassification);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgGoodsClassification.setUpdateBy(operName);
                    this.updateHgGoodsClassification(hgGoodsClassification);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    @Override
    public HgGoodsClassificationVo selectHgGoodsClassificationVo(String classificationName) {
        return hgGoodsClassificationMapper.selectHgGoodsClassificationVo(classificationName);
    }

}
