package com.qianjing.project.common.service;

import com.qianjing.project.common.domain.HgGenTableColumn;

import java.util.List;

/**
 * 业务字段 服务层
 * 
 * @author qianjing
 */
public interface IHgGenTableColumnService
{
    /**
     * 查询业务字段列表
     * 
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
    public List<HgGenTableColumn> selectGenTableColumnListByTableId(Long tableId);

    /**
     * 新增业务字段
     * 
     * @param hgGenTableColumn 业务字段信息
     * @return 结果
     */
    public int insertGenTableColumn(HgGenTableColumn hgGenTableColumn);

    /**
     * 修改业务字段
     * 
     * @param hgGenTableColumn 业务字段信息
     * @return 结果
     */
    public int updateGenTableColumn(HgGenTableColumn hgGenTableColumn);

    /**
     * 删除业务字段信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGenTableColumnByIds(String ids);
}
