package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 国内口岸代码对象 hg_domestic_port
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgDomesticPort extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 口岸编号 */
    private Long domesticPortId;

    /** 口岸编码 */
    @Excel(name = "口岸编码")
    private Long code;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String name;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String nameEn;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setDomesticPortId(Long domesticPortId) 
    {
        this.domesticPortId = domesticPortId;
    }

    public Long getDomesticPortId() 
    {
        return domesticPortId;
    }
    public void setCode(Long code) 
    {
        this.code = code;
    }

    public Long getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNameEn(String nameEn) 
    {
        this.nameEn = nameEn;
    }

    public String getNameEn() 
    {
        return nameEn;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("domesticPortId", getDomesticPortId())
            .append("code", getCode())
            .append("name", getName())
            .append("nameEn", getNameEn())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
