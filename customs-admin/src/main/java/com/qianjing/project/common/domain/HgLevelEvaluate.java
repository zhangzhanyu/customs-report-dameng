package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 风险分级评估对象 hg_level_evaluate
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-12-11 下午3:00    张占宇          V1.0          initialize
 */
public class HgLevelEvaluate extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long id;

    /** 商品类别 */
    @Excel(name = "商品类别")
    private String cpzl;

    /** HS编码范围 */
    @Excel(name = "HS编码范围")
    private String hsbmfw;

    /** 电气危险 */
    @Excel(name = "电气危险")
    private String dqwx;

    /** 货物属性范围 */
    @Excel(name = "货物属性范围")
    private String hwsxfw;

    /** 机械危险 */
    @Excel(name = "机械危险")
    private String jxwx;

    /** 燃烧/爆炸危险 */
    @Excel(name = "燃烧/爆炸危险")
    private String rsbzwx;

    /** 电磁危害 */
    @Excel(name = "电磁危害")
    private String dcwh;

    /** 放射性危害 */
    @Excel(name = "放射性危害")
    private String fsxwh;

    /** 听力视觉危害 */
    @Excel(name = "听力视觉危害")
    private String tlsjwh;

    /** 物理环境危害 */
    @Excel(name = "物理环境危害")
    private String wlhjwh;

    /** 有毒有害物质 */
    @Excel(name = "有毒有害物质")
    private String ydyhwz;

    /** 皮肤伤害 */
    @Excel(name = "皮肤伤害")
    private String pfsh;

    /** 化学环境危害 */
    @Excel(name = "化学环境危害")
    private String hxhjwh;

    /** 生物致癌危害 */
    @Excel(name = "生物致癌危害")
    private String swzawh;

    /** 生物环境危害 */
    @Excel(name = "生物环境危害")
    private String swhjwh;

    /** 消费者权益损害 */
    @Excel(name = "消费者权益损害")
    private String xfzqysh;

    /** 严重项目数 */
    @Excel(name = "严重项目数")
    private String yzxms;

    /** 一般项目数 */
    @Excel(name = "一般项目数")
    private String ybxms;

    /** 冗余字段3 */
    @Excel(name = "冗余字段3")
    private String filed3;

    /** 基础风险评级 */
    @Excel(name = "基础风险评级")
    private String jcfxpj;

    /** 年不合格检出率大于等于10% */
    @Excel(name = "年不合格检出率大于等于10%")
    private String nbhgjcldy;

    /** 年不合格检出率小于0.1% */
    @Excel(name = "年不合格检出率小于0.1%")
    private String nbhgjclxy;

    /** 冗余字段4 */
    @Excel(name = "冗余字段4")
    private String filed4;

    /** 冗余字段5 */
    @Excel(name = "冗余字段5")
    private String filed5;

    /** 品牌 */
    @Excel(name = "品牌")
    private String pp;

    /** 统计因素修正风险评级 */
    @Excel(name = "统计因素修正风险评级")
    private String tjysxzfxpj;

    /** 产品状态（1原材料 2制成品） */
    @Excel(name = "产品状态", readConverterExp = "1=原材料,2=制成品")
    private String cpzt;

    /** 产品用途（1工业用(包括服务业) 2农业用 3普通消费者用 4儿童用 5婴幼儿用 6老年人用 7医用） */
    @Excel(name = "产品用途", readConverterExp = "1=工业用(包括服务业),2=农业用,3=普通消费者用,4=儿童用,5=婴幼儿用,6=老年人用,7=医用")
    private String cpyt;

    /** 产品接触（1直接或预期与食品接触+3 2直接或预期与除四肢以外的人体皮肤接触+1） */
    @Excel(name = "产品接触", readConverterExp = "1=直接或预期与食品接触+3,2=直接或预期与除四肢以外的人体皮肤接触+1")
    private String cpjc;

    /** 使用环境变化 */
    @Excel(name = "使用环境变化")
    private String syhjbh;

    /** 社会关注程度 */
    @Excel(name = "社会关注程度")
    private String shgzcd;

    /** 敏感因子修正风险评级 */
    @Excel(name = "敏感因子修正风险评级")
    private String mgyzxzfxpj;

    /** 综合风险评级 */
    @Excel(name = "综合风险评级")
    private String zhfxpj;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setCpzl(String cpzl){
        this.cpzl = cpzl;
    }

    public String getCpzl(){
        return cpzl;
    }
    public void setHsbmfw(String hsbmfw){
        this.hsbmfw = hsbmfw;
    }

    public String getHsbmfw(){
        return hsbmfw;
    }
    public void setDqwx(String dqwx){
        this.dqwx = dqwx;
    }

    public String getDqwx(){
        return dqwx;
    }
    public void setHwsxfw(String hwsxfw){
        this.hwsxfw = hwsxfw;
    }

    public String getHwsxfw(){
        return hwsxfw;
    }
    public void setJxwx(String jxwx){
        this.jxwx = jxwx;
    }

    public String getJxwx(){
        return jxwx;
    }
    public void setRsbzwx(String rsbzwx){
        this.rsbzwx = rsbzwx;
    }

    public String getRsbzwx(){
        return rsbzwx;
    }
    public void setDcwh(String dcwh){
        this.dcwh = dcwh;
    }

    public String getDcwh(){
        return dcwh;
    }
    public void setFsxwh(String fsxwh){
        this.fsxwh = fsxwh;
    }

    public String getFsxwh(){
        return fsxwh;
    }
    public void setTlsjwh(String tlsjwh){
        this.tlsjwh = tlsjwh;
    }

    public String getTlsjwh(){
        return tlsjwh;
    }
    public void setWlhjwh(String wlhjwh){
        this.wlhjwh = wlhjwh;
    }

    public String getWlhjwh(){
        return wlhjwh;
    }
    public void setYdyhwz(String ydyhwz){
        this.ydyhwz = ydyhwz;
    }

    public String getYdyhwz(){
        return ydyhwz;
    }
    public void setPfsh(String pfsh){
        this.pfsh = pfsh;
    }

    public String getPfsh(){
        return pfsh;
    }
    public void setHxhjwh(String hxhjwh){
        this.hxhjwh = hxhjwh;
    }

    public String getHxhjwh(){
        return hxhjwh;
    }
    public void setSwzawh(String swzawh){
        this.swzawh = swzawh;
    }

    public String getSwzawh(){
        return swzawh;
    }
    public void setSwhjwh(String swhjwh){
        this.swhjwh = swhjwh;
    }

    public String getSwhjwh(){
        return swhjwh;
    }
    public void setXfzqysh(String xfzqysh){
        this.xfzqysh = xfzqysh;
    }

    public String getXfzqysh(){
        return xfzqysh;
    }
    public void setYzxms(String yzxms){
        this.yzxms = yzxms;
    }

    public String getYzxms(){
        return yzxms;
    }
    public void setYbxms(String ybxms){
        this.ybxms = ybxms;
    }

    public String getYbxms(){
        return ybxms;
    }
    public void setFiled3(String filed3){
        this.filed3 = filed3;
    }

    public String getFiled3(){
        return filed3;
    }
    public void setJcfxpj(String jcfxpj){
        this.jcfxpj = jcfxpj;
    }

    public String getJcfxpj(){
        return jcfxpj;
    }
    public void setNbhgjcldy(String nbhgjcldy){
        this.nbhgjcldy = nbhgjcldy;
    }

    public String getNbhgjcldy(){
        return nbhgjcldy;
    }
    public void setNbhgjclxy(String nbhgjclxy){
        this.nbhgjclxy = nbhgjclxy;
    }

    public String getNbhgjclxy(){
        return nbhgjclxy;
    }
    public void setFiled4(String filed4){
        this.filed4 = filed4;
    }

    public String getFiled4(){
        return filed4;
    }
    public void setFiled5(String filed5){
        this.filed5 = filed5;
    }

    public String getFiled5(){
        return filed5;
    }
    public void setPp(String pp){
        this.pp = pp;
    }

    public String getPp(){
        return pp;
    }
    public void setTjysxzfxpj(String tjysxzfxpj){
        this.tjysxzfxpj = tjysxzfxpj;
    }

    public String getTjysxzfxpj(){
        return tjysxzfxpj;
    }
    public void setCpzt(String cpzt){
        this.cpzt = cpzt;
    }

    public String getCpzt(){
        return cpzt;
    }
    public void setCpyt(String cpyt){
        this.cpyt = cpyt;
    }

    public String getCpyt(){
        return cpyt;
    }
    public void setCpjc(String cpjc){
        this.cpjc = cpjc;
    }

    public String getCpjc(){
        return cpjc;
    }
    public void setSyhjbh(String syhjbh){
        this.syhjbh = syhjbh;
    }

    public String getSyhjbh(){
        return syhjbh;
    }
    public void setShgzcd(String shgzcd){
        this.shgzcd = shgzcd;
    }

    public String getShgzcd(){
        return shgzcd;
    }
    public void setMgyzxzfxpj(String mgyzxzfxpj){
        this.mgyzxzfxpj = mgyzxzfxpj;
    }

    public String getMgyzxzfxpj(){
        return mgyzxzfxpj;
    }
    public void setZhfxpj(String zhfxpj){
        this.zhfxpj = zhfxpj;
    }

    public String getZhfxpj(){
        return zhfxpj;
    }
    public void setDelFlag(String delFlag){
        this.delFlag = delFlag;
    }

    public String getDelFlag(){
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("cpzl", getCpzl())
                .append("hsbmfw", getHsbmfw())
                .append("dqwx", getDqwx())
                .append("hwsxfw", getHwsxfw())
                .append("jxwx", getJxwx())
                .append("rsbzwx", getRsbzwx())
                .append("dcwh", getDcwh())
                .append("fsxwh", getFsxwh())
                .append("tlsjwh", getTlsjwh())
                .append("wlhjwh", getWlhjwh())
                .append("ydyhwz", getYdyhwz())
                .append("pfsh", getPfsh())
                .append("hxhjwh", getHxhjwh())
                .append("swzawh", getSwzawh())
                .append("swhjwh", getSwhjwh())
                .append("xfzqysh", getXfzqysh())
                .append("yzxms", getYzxms())
                .append("ybxms", getYbxms())
                .append("filed3", getFiled3())
                .append("jcfxpj", getJcfxpj())
                .append("nbhgjcldy", getNbhgjcldy())
                .append("nbhgjclxy", getNbhgjclxy())
                .append("filed4", getFiled4())
                .append("filed5", getFiled5())
                .append("pp", getPp())
                .append("tjysxzfxpj", getTjysxzfxpj())
                .append("cpzt", getCpzt())
                .append("cpyt", getCpyt())
                .append("cpjc", getCpjc())
                .append("syhjbh", getSyhjbh())
                .append("shgzcd", getShgzcd())
                .append("mgyzxzfxpj", getMgyzxzfxpj())
                .append("zhfxpj", getZhfxpj())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
