package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgSpecialRegion;
import com.qianjing.project.common.service.IHgSpecialRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 特殊贸易区代码Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/specialRegion")
public class HgSpecialRegionController extends BaseController
{
    @Autowired
    private IHgSpecialRegionService hgSpecialRegionService;

    /**
     * 查询特殊贸易区代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:specialRegion:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgSpecialRegion hgSpecialRegion)
    {
        startPage();
        List<HgSpecialRegion> list = hgSpecialRegionService.selectHgSpecialRegionList(hgSpecialRegion);
        return getDataTable(list);
    }

    /**
     * 导出特殊贸易区代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:specialRegion:export')")
    @Log(title = "特殊贸易区代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgSpecialRegion hgSpecialRegion)
    {
        List<HgSpecialRegion> list = hgSpecialRegionService.selectHgSpecialRegionList(hgSpecialRegion);
        ExcelUtil<HgSpecialRegion> util = new ExcelUtil<HgSpecialRegion>(HgSpecialRegion.class);
        return util.exportExcel(list, "特殊贸易区代码数据");
    }

    /**
     * 导入特殊贸易区代码列表
     */
    @Log(title = "特殊贸易区代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:specialRegion:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgSpecialRegion> util = new ExcelUtil<HgSpecialRegion>(HgSpecialRegion.class);
        List<HgSpecialRegion> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgSpecialRegionService.importHgSpecialRegion(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载特殊贸易区代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgSpecialRegion> util = new ExcelUtil<HgSpecialRegion>(HgSpecialRegion.class);
        util.importTemplateExcel(response, "特殊贸易区代码数据");
    }

    /**
     * 获取特殊贸易区代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:specialRegion:query')")
    @GetMapping(value = "/{specialRegionId}")
    public AjaxResult getInfo(@PathVariable("specialRegionId") Long specialRegionId)
    {
        return AjaxResult.success(hgSpecialRegionService.selectHgSpecialRegionBySpecialRegionId(specialRegionId));
    }

    /**
     * 新增特殊贸易区代码
     */
    @PreAuthorize("@ss.hasPermi('common:specialRegion:add')")
    @Log(title = "特殊贸易区代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgSpecialRegion hgSpecialRegion)
    {
        return toAjax(hgSpecialRegionService.insertHgSpecialRegion(hgSpecialRegion));
    }

    /**
     * 修改特殊贸易区代码
     */
    @PreAuthorize("@ss.hasPermi('common:specialRegion:edit')")
    @Log(title = "特殊贸易区代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgSpecialRegion hgSpecialRegion)
    {
        return toAjax(hgSpecialRegionService.updateHgSpecialRegion(hgSpecialRegion));
    }

    /**
     * 删除特殊贸易区代码
     */
    @PreAuthorize("@ss.hasPermi('common:specialRegion:remove')")
    @Log(title = "特殊贸易区代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{specialRegionIds}")
    public AjaxResult remove(@PathVariable Long[] specialRegionIds)
    {
        return toAjax(hgSpecialRegionService.deleteHgSpecialRegionBySpecialRegionIds(specialRegionIds));
    }
}
