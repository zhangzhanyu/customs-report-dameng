package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsClassification;
import com.qianjing.project.common.domain.HgGoodsClassificationVo;

/**
 * 商品分类代码Mapper接口
 * 
 * @author zhangzy
 * @date 2022-08-02
 */
public interface HgGoodsClassificationMapper 
{
    /**
     * 查询商品分类代码
     * 
     * @param goodsClassificationId 商品分类代码主键
     * @return 商品分类代码
     */
    public HgGoodsClassification selectHgGoodsClassificationByGoodsClassificationId(Long goodsClassificationId);

    /**
     * 查询商品分类代码列表
     * 
     * @param hgGoodsClassification 商品分类代码
     * @return 商品分类代码集合
     */
    public List<HgGoodsClassification> selectHgGoodsClassificationList(HgGoodsClassification hgGoodsClassification);

    /**
     * 新增商品分类代码
     * 
     * @param hgGoodsClassification 商品分类代码
     * @return 结果
     */
    public int insertHgGoodsClassification(HgGoodsClassification hgGoodsClassification);

    /**
     * 修改商品分类代码
     * 
     * @param hgGoodsClassification 商品分类代码
     * @return 结果
     */
    public int updateHgGoodsClassification(HgGoodsClassification hgGoodsClassification);

    /**
     * 删除商品分类代码
     * 
     * @param goodsClassificationId 商品分类代码主键
     * @return 结果
     */
    public int deleteHgGoodsClassificationByGoodsClassificationId(Long goodsClassificationId);

    /**
     * 批量删除商品分类代码
     * 
     * @param goodsClassificationIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgGoodsClassificationByGoodsClassificationIds(Long[] goodsClassificationIds);

    public HgGoodsClassificationVo selectHgGoodsClassificationVo(String classificationName);
}
