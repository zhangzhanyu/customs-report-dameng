package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgSysCode;
import com.qianjing.project.common.service.IHgSysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 代码Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/code")
public class HgSysCodeController extends BaseController
{
    @Autowired
    private IHgSysCodeService hgSysCodeService;

    /**
     * 查询代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:code:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgSysCode hgSysCode)
    {
        startPage();
        List<HgSysCode> list = hgSysCodeService.selectHgSysCodeList(hgSysCode);
        return getDataTable(list);
    }

    /**
     * 导出代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:code:export')")
    @Log(title = "代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgSysCode hgSysCode)
    {
        List<HgSysCode> list = hgSysCodeService.selectHgSysCodeList(hgSysCode);
        ExcelUtil<HgSysCode> util = new ExcelUtil<HgSysCode>(HgSysCode.class);
        return util.exportExcel(list, "代码数据");
    }

    /**
     * 导入代码列表
     */
    @Log(title = "代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:code:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgSysCode> util = new ExcelUtil<HgSysCode>(HgSysCode.class);
        List<HgSysCode> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgSysCodeService.importHgSysCode(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgSysCode> util = new ExcelUtil<HgSysCode>(HgSysCode.class);
        util.importTemplateExcel(response, "代码数据");
    }

    /**
     * 获取代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:code:query')")
    @GetMapping(value = "/{codeId}")
    public AjaxResult getInfo(@PathVariable("codeId") Long codeId)
    {
        return AjaxResult.success(hgSysCodeService.selectHgSysCodeByCodeId(codeId));
    }

    /**
     * 新增代码
     */
    @PreAuthorize("@ss.hasPermi('common:code:add')")
    @Log(title = "代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgSysCode hgSysCode)
    {
        return toAjax(hgSysCodeService.insertHgSysCode(hgSysCode));
    }

    /**
     * 修改代码
     */
    @PreAuthorize("@ss.hasPermi('common:code:edit')")
    @Log(title = "代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgSysCode hgSysCode)
    {
        return toAjax(hgSysCodeService.updateHgSysCode(hgSysCode));
    }

    /**
     * 删除代码
     */
    @PreAuthorize("@ss.hasPermi('common:code:remove')")
    @Log(title = "代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{codeIds}")
    public AjaxResult remove(@PathVariable Long[] codeIds)
    {
        return toAjax(hgSysCodeService.deleteHgSysCodeByCodeIds(codeIds));
    }
}
