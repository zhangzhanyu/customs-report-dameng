package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgGoodsImportParam;
import com.qianjing.project.common.service.IHgGoodsImportParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 商品进口参数Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/goodsImportParam")
public class HgGoodsImportParamController extends BaseController
{
    @Autowired
    private IHgGoodsImportParamService hgGoodsImportParamService;

    /**
     * 查询商品进口参数列表
     */
    @PreAuthorize("@ss.hasPermi('common:goodsImportParam:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgGoodsImportParam hgGoodsImportParam)
    {
        startPage();
        List<HgGoodsImportParam> list = hgGoodsImportParamService.selectHgGoodsImportParamList(hgGoodsImportParam);
        return getDataTable(list);
    }

    /**
     * 导出商品进口参数列表
     */
    @PreAuthorize("@ss.hasPermi('common:goodsImportParam:export')")
    @Log(title = "商品进口参数", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgGoodsImportParam hgGoodsImportParam)
    {
        List<HgGoodsImportParam> list = hgGoodsImportParamService.selectHgGoodsImportParamList(hgGoodsImportParam);
        ExcelUtil<HgGoodsImportParam> util = new ExcelUtil<HgGoodsImportParam>(HgGoodsImportParam.class);
        return util.exportExcel(list, "商品进口参数数据");
    }

    /**
     * 导入商品进口参数列表
     */
    @Log(title = "商品进口参数", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:goodsImportParam:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgGoodsImportParam> util = new ExcelUtil<HgGoodsImportParam>(HgGoodsImportParam.class);
        List<HgGoodsImportParam> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgGoodsImportParamService.importHgGoodsImportParam(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载商品进口参数导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgGoodsImportParam> util = new ExcelUtil<HgGoodsImportParam>(HgGoodsImportParam.class);
        util.importTemplateExcel(response, "商品进口参数数据");
    }

    /**
     * 获取商品进口参数详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:goodsImportParam:query')")
    @GetMapping(value = "/{goodsImportParamId}")
    public AjaxResult getInfo(@PathVariable("goodsImportParamId") Long goodsImportParamId)
    {
        return AjaxResult.success(hgGoodsImportParamService.selectHgGoodsImportParamByGoodsImportParamId(goodsImportParamId));
    }

    /**
     * 新增商品进口参数
     */
    @PreAuthorize("@ss.hasPermi('common:goodsImportParam:add')")
    @Log(title = "商品进口参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgGoodsImportParam hgGoodsImportParam)
    {
        return toAjax(hgGoodsImportParamService.insertHgGoodsImportParam(hgGoodsImportParam));
    }

    /**
     * 修改商品进口参数
     */
    @PreAuthorize("@ss.hasPermi('common:goodsImportParam:edit')")
    @Log(title = "商品进口参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgGoodsImportParam hgGoodsImportParam)
    {
        return toAjax(hgGoodsImportParamService.updateHgGoodsImportParam(hgGoodsImportParam));
    }

    /**
     * 删除商品进口参数
     */
    @PreAuthorize("@ss.hasPermi('common:goodsImportParam:remove')")
    @Log(title = "商品进口参数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{goodsImportParamIds}")
    public AjaxResult remove(@PathVariable Long[] goodsImportParamIds)
    {
        return toAjax(hgGoodsImportParamService.deleteHgGoodsImportParamByGoodsImportParamIds(goodsImportParamIds));
    }
}
