package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgReport;

/**
 * 动态报表Mapper接口
 * 
 * @author qianjing
 * @date 2022-09-06
 */
public interface HgReportMapper 
{
    /**
     * 查询动态报表
     * 
     * @param reportId 动态报表主键
     * @return 动态报表
     */
    public HgReport selectHgReportByReportId(Long reportId);

    /**
     * 查询动态报表列表
     * 
     * @param hgReport 动态报表
     * @return 动态报表集合
     */
    public List<HgReport> selectHgReportList(HgReport hgReport);

    /**
     * 新增动态报表
     * 
     * @param hgReport 动态报表
     * @return 结果
     */
    public int insertHgReport(HgReport hgReport);

    /**
     * 修改动态报表
     * 
     * @param hgReport 动态报表
     * @return 结果
     */
    public int updateHgReport(HgReport hgReport);

    /**
     * 删除动态报表
     * 
     * @param reportId 动态报表主键
     * @return 结果
     */
    public int deleteHgReportByReportId(Long reportId);

    /**
     * 批量删除动态报表
     * 
     * @param reportIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgReportByReportIds(Long[] reportIds);
}
