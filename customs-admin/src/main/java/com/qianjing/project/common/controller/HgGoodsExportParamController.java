package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgGoodsExportParam;
import com.qianjing.project.common.service.IHgGoodsExportParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 商品出口参数Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/goodsExportParam")
public class HgGoodsExportParamController extends BaseController
{
    @Autowired
    private IHgGoodsExportParamService hgGoodsExportParamService;

    /**
     * 查询商品出口参数列表
     */
    @PreAuthorize("@ss.hasPermi('common:goodsExportParam:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgGoodsExportParam hgGoodsExportParam)
    {
        startPage();
        List<HgGoodsExportParam> list = hgGoodsExportParamService.selectHgGoodsExportParamList(hgGoodsExportParam);
        return getDataTable(list);
    }

    /**
     * 导出商品出口参数列表
     */
    @PreAuthorize("@ss.hasPermi('common:goodsExportParam:export')")
    @Log(title = "商品出口参数", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgGoodsExportParam hgGoodsExportParam)
    {
        List<HgGoodsExportParam> list = hgGoodsExportParamService.selectHgGoodsExportParamList(hgGoodsExportParam);
        ExcelUtil<HgGoodsExportParam> util = new ExcelUtil<HgGoodsExportParam>(HgGoodsExportParam.class);
        return util.exportExcel(list, "商品出口参数数据");
    }

    /**
     * 导入商品出口参数列表
     */
    @Log(title = "商品出口参数", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:goodsExportParam:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgGoodsExportParam> util = new ExcelUtil<HgGoodsExportParam>(HgGoodsExportParam.class);
        List<HgGoodsExportParam> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgGoodsExportParamService.importHgGoodsExportParam(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载商品出口参数导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgGoodsExportParam> util = new ExcelUtil<HgGoodsExportParam>(HgGoodsExportParam.class);
        util.importTemplateExcel(response, "商品出口参数数据");
    }

    /**
     * 获取商品出口参数详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:goodsExportParam:query')")
    @GetMapping(value = "/{goodsExportParamId}")
    public AjaxResult getInfo(@PathVariable("goodsExportParamId") Long goodsExportParamId)
    {
        return AjaxResult.success(hgGoodsExportParamService.selectHgGoodsExportParamByGoodsExportParamId(goodsExportParamId));
    }

    /**
     * 新增商品出口参数
     */
    @PreAuthorize("@ss.hasPermi('common:goodsExportParam:add')")
    @Log(title = "商品出口参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgGoodsExportParam hgGoodsExportParam)
    {
        return toAjax(hgGoodsExportParamService.insertHgGoodsExportParam(hgGoodsExportParam));
    }

    /**
     * 修改商品出口参数
     */
    @PreAuthorize("@ss.hasPermi('common:goodsExportParam:edit')")
    @Log(title = "商品出口参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgGoodsExportParam hgGoodsExportParam)
    {
        return toAjax(hgGoodsExportParamService.updateHgGoodsExportParam(hgGoodsExportParam));
    }

    /**
     * 删除商品出口参数
     */
    @PreAuthorize("@ss.hasPermi('common:goodsExportParam:remove')")
    @Log(title = "商品出口参数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{goodsExportParamIds}")
    public AjaxResult remove(@PathVariable Long[] goodsExportParamIds)
    {
        return toAjax(hgGoodsExportParamService.deleteHgGoodsExportParamByGoodsExportParamIds(goodsExportParamIds));
    }
}
