package com.qianjing.project.common.controller;

import com.qianjing.common.utils.StringUtils;
import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgGoodsClassification;
import com.qianjing.project.common.service.IHgGoodsClassificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品分类代码Controller
 * 
 * @author zhangzy
 * @date 2022-08-02
 */
@Validated
@RestController
@RequestMapping("/common/classification")
public class HgGoodsClassificationController extends BaseController
{
    @Autowired
    private IHgGoodsClassificationService hgGoodsClassificationService;

    /**
     * 查询商品分类代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:classification:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgGoodsClassification hgGoodsClassification)
    {
        startPage();
        List<HgGoodsClassification> list = hgGoodsClassificationService.selectHgGoodsClassificationList(hgGoodsClassification);
        return getDataTable(list);
    }

    /**
     * 导出商品分类代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:classification:export')")
    @Log(title = "商品分类代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgGoodsClassification hgGoodsClassification)
    {
        List<HgGoodsClassification> list = hgGoodsClassificationService.selectHgGoodsClassificationList(hgGoodsClassification);
        ExcelUtil<HgGoodsClassification> util = new ExcelUtil<HgGoodsClassification>(HgGoodsClassification.class);
        return util.exportExcel(list, "商品分类代码数据");
    }

    /**
     * 导入商品分类代码列表
     */
    @Log(title = "商品分类代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:classification:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgGoodsClassification> util = new ExcelUtil<HgGoodsClassification>(HgGoodsClassification.class);
        List<HgGoodsClassification> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgGoodsClassificationService.importHgGoodsClassification(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载商品分类代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgGoodsClassification> util = new ExcelUtil<HgGoodsClassification>(HgGoodsClassification.class);
        util.importTemplateExcel(response, "商品分类代码数据");
    }

    /**
     * 获取商品分类代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:classification:query')")
    @GetMapping(value = "/{goodsClassificationId}")
    public AjaxResult getInfo(@PathVariable("goodsClassificationId") Long goodsClassificationId)
    {
        return AjaxResult.success(hgGoodsClassificationService.selectHgGoodsClassificationByGoodsClassificationId(goodsClassificationId));
    }

    /**
     * 新增商品分类代码
     */
    @PreAuthorize("@ss.hasPermi('common:classification:add')")
    @Log(title = "商品分类代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgGoodsClassification hgGoodsClassification) throws ParseException {
        if (hgGoodsClassification.getEffectiveFlag() == null || hgGoodsClassification.getEffectiveFlag()) {
            SimpleDateFormat sdf=new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
            hgGoodsClassification.setEffectiveDate(sdf.parse("2099-12-31 00:00:00"));
        }
        if (StringUtils.isNotBlank(hgGoodsClassification.getHsCode()) && StringUtils.isNotBlank(hgGoodsClassification.getCiqCode()) &&StringUtils.isNotBlank(hgGoodsClassification.getProductCharCode())){
            return AjaxResult.error("HS编码、CIQ编码、货物属性不可同时为空！");
        }
        if (StringUtils.isNotBlank(hgGoodsClassification.getProductCharCode())){
            return toAjax(hgGoodsClassificationService.insertHgGoodsClassification(hgGoodsClassification));
        }
        String[] split;
        List<HgGoodsClassification> list = new ArrayList<>();
        if (StringUtils.isNotBlank(hgGoodsClassification.getHsCode())) {
            split = StringUtils.replaceAllBlank(hgGoodsClassification.getHsCode()).split(",");
            for (int i = 0; i < split.length; i++) {
                HgGoodsClassification temp = new HgGoodsClassification();
                temp.setClassificationName(hgGoodsClassification.getClassificationName());
                temp.setHsCode(split[i]);
                temp.setEffectiveDate(hgGoodsClassification.getEffectiveDate());
                list.add(temp);
            }
        }
        if (StringUtils.isNotBlank(hgGoodsClassification.getCiqCode())) {
            split = StringUtils.replaceAllBlank(hgGoodsClassification.getCiqCode()).split(",");
            for (int i = 0; i < split.length; i++) {
                HgGoodsClassification temp = new HgGoodsClassification();
                temp.setClassificationName(hgGoodsClassification.getClassificationName());
                temp.setCiqCode(split[i]);
                temp.setEffectiveDate(hgGoodsClassification.getEffectiveDate());
                list.add(temp);
            }
        }
        String operName = getUsername();
        String message = hgGoodsClassificationService.importHgGoodsClassification(list, false, operName);
        return AjaxResult.success(message);
    }

    /**
     * 修改商品分类代码
     */
    @PreAuthorize("@ss.hasPermi('common:classification:edit')")
    @Log(title = "商品分类代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgGoodsClassification hgGoodsClassification)
    {
        return toAjax(hgGoodsClassificationService.updateHgGoodsClassification(hgGoodsClassification));
    }

    /**
     * 删除商品分类代码
     */
    @PreAuthorize("@ss.hasPermi('common:classification:remove')")
    @Log(title = "商品分类代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{goodsClassificationIds}")
    public AjaxResult remove(@PathVariable Long[] goodsClassificationIds)
    {
        return toAjax(hgGoodsClassificationService.deleteHgGoodsClassificationByGoodsClassificationIds(goodsClassificationIds));
    }

    @GetMapping("/selectHgGoodsClassificationVo")
    public AjaxResult selectHgGoodsClassificationVo(@NotBlank(message = "商品类别【classificationName】不能为空！") String classificationName) {
        return AjaxResult.success(hgGoodsClassificationService.selectHgGoodsClassificationVo(classificationName));
    }
}
