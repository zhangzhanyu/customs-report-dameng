package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgSysCodeMapper;
import com.qianjing.project.common.domain.HgSysCode;
import com.qianjing.project.common.service.IHgSysCodeService;

/**
 * 代码Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgSysCodeServiceImpl implements IHgSysCodeService 
{
    private static final Logger log = LoggerFactory.getLogger(HgSysCodeServiceImpl.class);

    @Autowired
    private HgSysCodeMapper hgSysCodeMapper;

    /**
     * 查询代码
     * 
     * @param codeId 代码主键
     * @return 代码
     */
    @Override
    public HgSysCode selectHgSysCodeByCodeId(Long codeId)
    {
        return hgSysCodeMapper.selectHgSysCodeByCodeId(codeId);
    }

    /**
     * 查询代码列表
     * 
     * @param hgSysCode 代码
     * @return 代码
     */
    @Override
    public List<HgSysCode> selectHgSysCodeList(HgSysCode hgSysCode)
    {
        return hgSysCodeMapper.selectHgSysCodeList(hgSysCode);
    }

    /**
     * 新增代码
     * 
     * @param hgSysCode 代码
     * @return 结果
     */
    @Override
    public int insertHgSysCode(HgSysCode hgSysCode)
    {
        hgSysCode.setCreateTime(DateUtils.getNowDate());
        return hgSysCodeMapper.insertHgSysCode(hgSysCode);
    }

    /**
     * 修改代码
     * 
     * @param hgSysCode 代码
     * @return 结果
     */
    @Override
    public int updateHgSysCode(HgSysCode hgSysCode)
    {
        hgSysCode.setUpdateTime(DateUtils.getNowDate());
        return hgSysCodeMapper.updateHgSysCode(hgSysCode);
    }

    /**
     * 批量删除代码
     * 
     * @param codeIds 需要删除的代码主键
     * @return 结果
     */
    @Override
    public int deleteHgSysCodeByCodeIds(Long[] codeIds)
    {
        return hgSysCodeMapper.deleteHgSysCodeByCodeIds(codeIds);
    }

    /**
     * 删除代码信息
     * 
     * @param codeId 代码主键
     * @return 结果
     */
    @Override
    public int deleteHgSysCodeByCodeId(Long codeId)
    {
        return hgSysCodeMapper.deleteHgSysCodeByCodeId(codeId);
    }

    /**
     * 导入代码数据
     *
     * @param hgSysCodeList hgSysCodeList 代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgSysCode(List<HgSysCode> hgSysCodeList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgSysCodeList) || hgSysCodeList.size() == 0) {
            throw new ServiceException("导入代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgSysCode hgSysCode : hgSysCodeList){
            try {
                // 验证是否存在
                if (true) {
                    hgSysCode.setCreateBy(operName);
                    this.insertHgSysCode(hgSysCode);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgSysCode.setUpdateBy(operName);
                    this.updateHgSysCode(hgSysCode);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
