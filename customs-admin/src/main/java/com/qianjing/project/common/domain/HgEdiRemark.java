package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * EDI申报备注对象 hg_edi_remark
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgEdiRemark extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 备注编号 */
    private Long ediId;

    /** 位数 */
    @Excel(name = "位数")
    private String digit;

    /** 位数含义 */
    @Excel(name = "位数含义")
    private String digitDescription;

    /** 位数值 */
    @Excel(name = "位数值")
    private String digitValue;

    /** 值含义 */
    @Excel(name = "值含义")
    private String digitValueDescription;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setEdiId(Long ediId) 
    {
        this.ediId = ediId;
    }

    public Long getEdiId() 
    {
        return ediId;
    }
    public void setDigit(String digit) 
    {
        this.digit = digit;
    }

    public String getDigit() 
    {
        return digit;
    }
    public void setDigitDescription(String digitDescription) 
    {
        this.digitDescription = digitDescription;
    }

    public String getDigitDescription() 
    {
        return digitDescription;
    }
    public void setDigitValue(String digitValue) 
    {
        this.digitValue = digitValue;
    }

    public String getDigitValue() 
    {
        return digitValue;
    }
    public void setDigitValueDescription(String digitValueDescription) 
    {
        this.digitValueDescription = digitValueDescription;
    }

    public String getDigitValueDescription() 
    {
        return digitValueDescription;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ediId", getEdiId())
            .append("digit", getDigit())
            .append("digitDescription", getDigitDescription())
            .append("digitValue", getDigitValue())
            .append("digitValueDescription", getDigitValueDescription())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
