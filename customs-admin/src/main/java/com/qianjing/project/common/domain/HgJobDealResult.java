package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 检查作业处理结果清单对象 hg_job_deal_result
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgJobDealResult extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 结果编号 */
    private Long jobDealResultId;

    /** 处理结果 */
    @Excel(name = "处理结果")
    private String result;

    /** 检查类型 */
    @Excel(name = "检查类型")
    private String resultCheckType;

    /** 计入查获 */
    @Excel(name = "计入查获")
    private String includeSeized;

    /** 适用情形 */
    @Excel(name = "适用情形")
    private String applicableSituation;

    /** 标签1 */
    @Excel(name = "标签1")
    private String tag1;

    /** 标签2 */
    @Excel(name = "标签2")
    private String tag2;

    /** 标签3 */
    @Excel(name = "标签3")
    private String tag3;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setJobDealResultId(Long jobDealResultId) 
    {
        this.jobDealResultId = jobDealResultId;
    }

    public Long getJobDealResultId() 
    {
        return jobDealResultId;
    }
    public void setResult(String result) 
    {
        this.result = result;
    }

    public String getResult() 
    {
        return result;
    }
    public void setResultCheckType(String resultCheckType) 
    {
        this.resultCheckType = resultCheckType;
    }

    public String getResultCheckType() 
    {
        return resultCheckType;
    }
    public void setIncludeSeized(String includeSeized) 
    {
        this.includeSeized = includeSeized;
    }

    public String getIncludeSeized() 
    {
        return includeSeized;
    }
    public void setApplicableSituation(String applicableSituation) 
    {
        this.applicableSituation = applicableSituation;
    }

    public String getApplicableSituation() 
    {
        return applicableSituation;
    }
    public void setTag1(String tag1) 
    {
        this.tag1 = tag1;
    }

    public String getTag1() 
    {
        return tag1;
    }
    public void setTag2(String tag2) 
    {
        this.tag2 = tag2;
    }

    public String getTag2() 
    {
        return tag2;
    }
    public void setTag3(String tag3) 
    {
        this.tag3 = tag3;
    }

    public String getTag3() 
    {
        return tag3;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jobDealResultId", getJobDealResultId())
            .append("result", getResult())
            .append("resultCheckType", getResultCheckType())
            .append("includeSeized", getIncludeSeized())
            .append("applicableSituation", getApplicableSituation())
            .append("tag1", getTag1())
            .append("tag2", getTag2())
            .append("tag3", getTag3())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
