package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgSpecialRegionMapper;
import com.qianjing.project.common.domain.HgSpecialRegion;
import com.qianjing.project.common.service.IHgSpecialRegionService;

/**
 * 特殊贸易区代码Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgSpecialRegionServiceImpl implements IHgSpecialRegionService 
{
    private static final Logger log = LoggerFactory.getLogger(HgSpecialRegionServiceImpl.class);

    @Autowired
    private HgSpecialRegionMapper hgSpecialRegionMapper;

    /**
     * 查询特殊贸易区代码
     * 
     * @param specialRegionId 特殊贸易区代码主键
     * @return 特殊贸易区代码
     */
    @Override
    public HgSpecialRegion selectHgSpecialRegionBySpecialRegionId(Long specialRegionId)
    {
        return hgSpecialRegionMapper.selectHgSpecialRegionBySpecialRegionId(specialRegionId);
    }

    /**
     * 查询特殊贸易区代码列表
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 特殊贸易区代码
     */
    @Override
    public List<HgSpecialRegion> selectHgSpecialRegionList(HgSpecialRegion hgSpecialRegion)
    {
        return hgSpecialRegionMapper.selectHgSpecialRegionList(hgSpecialRegion);
    }

    /**
     * 新增特殊贸易区代码
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 结果
     */
    @Override
    public int insertHgSpecialRegion(HgSpecialRegion hgSpecialRegion)
    {
        hgSpecialRegion.setCreateTime(DateUtils.getNowDate());
        return hgSpecialRegionMapper.insertHgSpecialRegion(hgSpecialRegion);
    }

    /**
     * 修改特殊贸易区代码
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 结果
     */
    @Override
    public int updateHgSpecialRegion(HgSpecialRegion hgSpecialRegion)
    {
        hgSpecialRegion.setUpdateTime(DateUtils.getNowDate());
        return hgSpecialRegionMapper.updateHgSpecialRegion(hgSpecialRegion);
    }

    /**
     * 批量删除特殊贸易区代码
     * 
     * @param specialRegionIds 需要删除的特殊贸易区代码主键
     * @return 结果
     */
    @Override
    public int deleteHgSpecialRegionBySpecialRegionIds(Long[] specialRegionIds)
    {
        return hgSpecialRegionMapper.deleteHgSpecialRegionBySpecialRegionIds(specialRegionIds);
    }

    /**
     * 删除特殊贸易区代码信息
     * 
     * @param specialRegionId 特殊贸易区代码主键
     * @return 结果
     */
    @Override
    public int deleteHgSpecialRegionBySpecialRegionId(Long specialRegionId)
    {
        return hgSpecialRegionMapper.deleteHgSpecialRegionBySpecialRegionId(specialRegionId);
    }

    /**
     * 导入特殊贸易区代码数据
     *
     * @param hgSpecialRegionList hgSpecialRegionList 特殊贸易区代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgSpecialRegion(List<HgSpecialRegion> hgSpecialRegionList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgSpecialRegionList) || hgSpecialRegionList.size() == 0) {
            throw new ServiceException("导入特殊贸易区代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgSpecialRegion hgSpecialRegion : hgSpecialRegionList){
            try {
                // 验证是否存在
                if (true) {
                    hgSpecialRegion.setCreateBy(operName);
                    this.insertHgSpecialRegion(hgSpecialRegion);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgSpecialRegion.setUpdateBy(operName);
                    this.updateHgSpecialRegion(hgSpecialRegion);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
