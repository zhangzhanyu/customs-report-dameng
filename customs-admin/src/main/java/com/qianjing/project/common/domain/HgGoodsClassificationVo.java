package com.qianjing.project.common.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 用于自动加载HS编码范围和货物属性范围
 */
@Getter
@Setter
public class HgGoodsClassificationVo {

    /** 商品分类 */
    private String classificationName;

    /** HS编码 */
    private String hsCodes;

    /** 货物属性 */
    private String productCharCodes;

}
