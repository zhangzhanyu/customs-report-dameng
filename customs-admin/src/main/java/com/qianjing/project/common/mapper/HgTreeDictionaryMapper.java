package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgTreeDictionary;

/**
 * 字典（树形）Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgTreeDictionaryMapper 
{
    /**
     * 查询字典（树形）
     * 
     * @param treeDicId 字典（树形）主键
     * @return 字典（树形）
     */
    public HgTreeDictionary selectHgTreeDictionaryByTreeDicId(Long treeDicId);

    /**
     * 查询字典（树形）列表
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 字典（树形）集合
     */
    public List<HgTreeDictionary> selectHgTreeDictionaryList(HgTreeDictionary hgTreeDictionary);

    /**
     * 新增字典（树形）
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 结果
     */
    public int insertHgTreeDictionary(HgTreeDictionary hgTreeDictionary);

    /**
     * 修改字典（树形）
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 结果
     */
    public int updateHgTreeDictionary(HgTreeDictionary hgTreeDictionary);

    /**
     * 删除字典（树形）
     * 
     * @param treeDicId 字典（树形）主键
     * @return 结果
     */
    public int deleteHgTreeDictionaryByTreeDicId(Long treeDicId);

    /**
     * 批量删除字典（树形）
     * 
     * @param treeDicIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgTreeDictionaryByTreeDicIds(Long[] treeDicIds);
}
