package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgSupervisionMode;
import com.qianjing.project.common.service.IHgSupervisionModeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 监管方式代码Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/mode")
public class HgSupervisionModeController extends BaseController
{
    @Autowired
    private IHgSupervisionModeService hgSupervisionModeService;

    /**
     * 查询监管方式代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:mode:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgSupervisionMode hgSupervisionMode)
    {
        startPage();
        List<HgSupervisionMode> list = hgSupervisionModeService.selectHgSupervisionModeList(hgSupervisionMode);
        return getDataTable(list);
    }

    /**
     * 导出监管方式代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:mode:export')")
    @Log(title = "监管方式代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgSupervisionMode hgSupervisionMode)
    {
        List<HgSupervisionMode> list = hgSupervisionModeService.selectHgSupervisionModeList(hgSupervisionMode);
        ExcelUtil<HgSupervisionMode> util = new ExcelUtil<HgSupervisionMode>(HgSupervisionMode.class);
        return util.exportExcel(list, "监管方式代码数据");
    }

    /**
     * 导入监管方式代码列表
     */
    @Log(title = "监管方式代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:mode:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgSupervisionMode> util = new ExcelUtil<HgSupervisionMode>(HgSupervisionMode.class);
        List<HgSupervisionMode> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgSupervisionModeService.importHgSupervisionMode(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载监管方式代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgSupervisionMode> util = new ExcelUtil<HgSupervisionMode>(HgSupervisionMode.class);
        util.importTemplateExcel(response, "监管方式代码数据");
    }

    /**
     * 获取监管方式代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:mode:query')")
    @GetMapping(value = "/{supervisionModeId}")
    public AjaxResult getInfo(@PathVariable("supervisionModeId") Long supervisionModeId)
    {
        return AjaxResult.success(hgSupervisionModeService.selectHgSupervisionModeBySupervisionModeId(supervisionModeId));
    }

    /**
     * 新增监管方式代码
     */
    @PreAuthorize("@ss.hasPermi('common:mode:add')")
    @Log(title = "监管方式代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgSupervisionMode hgSupervisionMode)
    {
        return toAjax(hgSupervisionModeService.insertHgSupervisionMode(hgSupervisionMode));
    }

    /**
     * 修改监管方式代码
     */
    @PreAuthorize("@ss.hasPermi('common:mode:edit')")
    @Log(title = "监管方式代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgSupervisionMode hgSupervisionMode)
    {
        return toAjax(hgSupervisionModeService.updateHgSupervisionMode(hgSupervisionMode));
    }

    /**
     * 删除监管方式代码
     */
    @PreAuthorize("@ss.hasPermi('common:mode:remove')")
    @Log(title = "监管方式代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{supervisionModeIds}")
    public AjaxResult remove(@PathVariable Long[] supervisionModeIds)
    {
        return toAjax(hgSupervisionModeService.deleteHgSupervisionModeBySupervisionModeIds(supervisionModeIds));
    }
}
