package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsCatalog;

/**
 * 商品目录Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgGoodsCatalogMapper 
{
    /**
     * 查询商品目录
     * 
     * @param goodsCatalogId 商品目录主键
     * @return 商品目录
     */
    public HgGoodsCatalog selectHgGoodsCatalogByGoodsCatalogId(Long goodsCatalogId);

    /**
     * 查询商品目录列表
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 商品目录集合
     */
    public List<HgGoodsCatalog> selectHgGoodsCatalogList(HgGoodsCatalog hgGoodsCatalog);

    /**
     * 新增商品目录
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 结果
     */
    public int insertHgGoodsCatalog(HgGoodsCatalog hgGoodsCatalog);

    /**
     * 修改商品目录
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 结果
     */
    public int updateHgGoodsCatalog(HgGoodsCatalog hgGoodsCatalog);

    /**
     * 删除商品目录
     * 
     * @param goodsCatalogId 商品目录主键
     * @return 结果
     */
    public int deleteHgGoodsCatalogByGoodsCatalogId(Long goodsCatalogId);

    /**
     * 批量删除商品目录
     * 
     * @param goodsCatalogIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgGoodsCatalogByGoodsCatalogIds(Long[] goodsCatalogIds);
}
