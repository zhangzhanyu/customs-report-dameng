package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgCountry;

/**
 * 国别地区代码Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgCountryService 
{
    /**
     * 查询国别地区代码
     * 
     * @param countryId 国别地区代码主键
     * @return 国别地区代码
     */
    public HgCountry selectHgCountryByCountryId(Long countryId);

    /**
     * 查询国别地区代码列表
     * 
     * @param hgCountry 国别地区代码
     * @return 国别地区代码集合
     */
    public List<HgCountry> selectHgCountryList(HgCountry hgCountry);

    /**
     * 新增国别地区代码
     * 
     * @param hgCountry 国别地区代码
     * @return 结果
     */
    public int insertHgCountry(HgCountry hgCountry);

    /**
     * 修改国别地区代码
     * 
     * @param hgCountry 国别地区代码
     * @return 结果
     */
    public int updateHgCountry(HgCountry hgCountry);

    /**
     * 批量删除国别地区代码
     * 
     * @param countryIds 需要删除的国别地区代码主键集合
     * @return 结果
     */
    public int deleteHgCountryByCountryIds(Long[] countryIds);

    /**
     * 删除国别地区代码信息
     * 
     * @param countryId 国别地区代码主键
     * @return 结果
     */
    public int deleteHgCountryByCountryId(Long countryId);

    /**
     * 导入国别地区代码信息
     *
     * @param hgCountryList 国别地区代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgCountry(List<HgCountry> hgCountryList, Boolean isUpdateSupport, String operName);
}
