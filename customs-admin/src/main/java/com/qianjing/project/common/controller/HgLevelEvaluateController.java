package com.qianjing.project.common.controller;

import java.util.List;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.project.common.domain.HgLevelEvaluate;
import com.qianjing.project.common.service.IHgLevelEvaluateService;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.common.utils.poi.ExcelUtil;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.qianjing.framework.web.page.TableDataInfo;

/**
 * 风险分级评估Controller
 *
 * @Created by 张占宇 2023-12-08 下午3:55:54
 * @Version: V1.0.0
 */
@Validated
@RestController
@RequestMapping("/common/evaluate")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HgLevelEvaluateController extends BaseController {

    private final IHgLevelEvaluateService hgLevelEvaluateService;

    /**
     * 查询风险分级评估列表
     */
    @PreAuthorize("@ss.hasPermi('common:evaluate:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgLevelEvaluate hgLevelEvaluate) {
        startPage();
        List<HgLevelEvaluate> list = hgLevelEvaluateService.selectHgLevelEvaluateList(hgLevelEvaluate);
        return getDataTable(list);
    }

    /**
     * 导出风险分级评估列表
     */
    @PreAuthorize("@ss.hasPermi('common:evaluate:export')")
    @Log(title = "风险分级评估", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgLevelEvaluate hgLevelEvaluate) {
        List<HgLevelEvaluate> list = hgLevelEvaluateService.selectHgLevelEvaluateList(hgLevelEvaluate);
        ExcelUtil<HgLevelEvaluate> util = new ExcelUtil<HgLevelEvaluate>(HgLevelEvaluate.class);
        return util.exportExcel(list, "风险分级评估数据");
    }

    /**
     * 导入风险分级评估列表
     */
    @Log(title = "风险分级评估", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:evaluate:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<HgLevelEvaluate> util = new ExcelUtil<HgLevelEvaluate>(HgLevelEvaluate.class);
        List<HgLevelEvaluate> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgLevelEvaluateService.importHgLevelEvaluate(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载风险分级评估导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgLevelEvaluate> util = new ExcelUtil<HgLevelEvaluate>(HgLevelEvaluate.class);
        util.importTemplateExcel(response, "风险分级评估数据");
    }

    /**
     * 获取风险分级评估详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:evaluate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(hgLevelEvaluateService.selectHgLevelEvaluateById(id));
    }

    /**
     * 新增风险分级评估
     */
    @PreAuthorize("@ss.hasPermi('common:evaluate:add')")
    @Log(title = "风险分级评估", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgLevelEvaluate hgLevelEvaluate) {
        return toAjax(hgLevelEvaluateService.insertHgLevelEvaluate(hgLevelEvaluate));
    }

    /**
     * 修改风险分级评估
     */
    @PreAuthorize("@ss.hasPermi('common:evaluate:edit')")
    @Log(title = "风险分级评估", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgLevelEvaluate hgLevelEvaluate) {
        return toAjax(hgLevelEvaluateService.updateHgLevelEvaluate(hgLevelEvaluate));
    }

    /**
     * 删除风险分级评估
     */
    @PreAuthorize("@ss.hasPermi('common:evaluate:remove')")
    @Log(title = "风险分级评估", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(hgLevelEvaluateService.deleteHgLevelEvaluateByIds(ids));
    }
}
