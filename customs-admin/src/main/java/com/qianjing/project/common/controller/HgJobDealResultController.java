package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgJobDealResult;
import com.qianjing.project.common.service.IHgJobDealResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 检查作业处理结果清单Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/result")
public class HgJobDealResultController extends BaseController
{
    @Autowired
    private IHgJobDealResultService hgJobDealResultService;

    /**
     * 查询检查作业处理结果清单列表
     */
    @PreAuthorize("@ss.hasPermi('common:result:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgJobDealResult hgJobDealResult)
    {
        startPage();
        List<HgJobDealResult> list = hgJobDealResultService.selectHgJobDealResultList(hgJobDealResult);
        return getDataTable(list);
    }

    /**
     * 导出检查作业处理结果清单列表
     */
    @PreAuthorize("@ss.hasPermi('common:result:export')")
    @Log(title = "检查作业处理结果清单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgJobDealResult hgJobDealResult)
    {
        List<HgJobDealResult> list = hgJobDealResultService.selectHgJobDealResultList(hgJobDealResult);
        ExcelUtil<HgJobDealResult> util = new ExcelUtil<HgJobDealResult>(HgJobDealResult.class);
        return util.exportExcel(list, "检查作业处理结果清单数据");
    }

    /**
     * 导入检查作业处理结果清单列表
     */
    @Log(title = "检查作业处理结果清单", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:result:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgJobDealResult> util = new ExcelUtil<HgJobDealResult>(HgJobDealResult.class);
        List<HgJobDealResult> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgJobDealResultService.importHgJobDealResult(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载检查作业处理结果清单导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgJobDealResult> util = new ExcelUtil<HgJobDealResult>(HgJobDealResult.class);
        util.importTemplateExcel(response, "检查作业处理结果清单数据");
    }

    /**
     * 获取检查作业处理结果清单详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:result:query')")
    @GetMapping(value = "/{jobDealResultId}")
    public AjaxResult getInfo(@PathVariable("jobDealResultId") Long jobDealResultId)
    {
        return AjaxResult.success(hgJobDealResultService.selectHgJobDealResultByJobDealResultId(jobDealResultId));
    }

    /**
     * 新增检查作业处理结果清单
     */
    @PreAuthorize("@ss.hasPermi('common:result:add')")
    @Log(title = "检查作业处理结果清单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgJobDealResult hgJobDealResult)
    {
        return toAjax(hgJobDealResultService.insertHgJobDealResult(hgJobDealResult));
    }

    /**
     * 修改检查作业处理结果清单
     */
    @PreAuthorize("@ss.hasPermi('common:result:edit')")
    @Log(title = "检查作业处理结果清单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgJobDealResult hgJobDealResult)
    {
        return toAjax(hgJobDealResultService.updateHgJobDealResult(hgJobDealResult));
    }

    /**
     * 删除检查作业处理结果清单
     */
    @PreAuthorize("@ss.hasPermi('common:result:remove')")
    @Log(title = "检查作业处理结果清单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jobDealResultIds}")
    public AjaxResult remove(@PathVariable Long[] jobDealResultIds)
    {
        return toAjax(hgJobDealResultService.deleteHgJobDealResultByJobDealResultIds(jobDealResultIds));
    }
}
