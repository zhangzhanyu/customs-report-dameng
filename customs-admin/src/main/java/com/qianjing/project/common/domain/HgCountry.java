package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 国别地区代码对象 hg_country
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgCountry extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 地区编号 */
    private Long countryId;

    /** 地区编码 */
    @Excel(name = "地区编码")
    private String countryCode;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String countryChinese;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String countryEnglish;

    /** 报关代码 */
    @Excel(name = "报关代码")
    private Long declareCode;

    /** 标签1 */
    @Excel(name = "标签1")
    private String tag1;

    /** 标签2 */
    @Excel(name = "标签2")
    private String tag2;

    /** 标签3 */
    @Excel(name = "标签3")
    private String tag3;

    /** 删除标志(0未删 1已删) */
    private String delFlag;

    public void setCountryId(Long countryId) 
    {
        this.countryId = countryId;
    }

    public Long getCountryId() 
    {
        return countryId;
    }
    public void setCountryCode(String countryCode) 
    {
        this.countryCode = countryCode;
    }

    public String getCountryCode() 
    {
        return countryCode;
    }
    public void setCountryChinese(String countryChinese) 
    {
        this.countryChinese = countryChinese;
    }

    public String getCountryChinese() 
    {
        return countryChinese;
    }
    public void setCountryEnglish(String countryEnglish) 
    {
        this.countryEnglish = countryEnglish;
    }

    public String getCountryEnglish() 
    {
        return countryEnglish;
    }
    public void setDeclareCode(Long declareCode) 
    {
        this.declareCode = declareCode;
    }

    public Long getDeclareCode() 
    {
        return declareCode;
    }
    public void setTag1(String tag1) 
    {
        this.tag1 = tag1;
    }

    public String getTag1() 
    {
        return tag1;
    }
    public void setTag2(String tag2) 
    {
        this.tag2 = tag2;
    }

    public String getTag2() 
    {
        return tag2;
    }
    public void setTag3(String tag3) 
    {
        this.tag3 = tag3;
    }

    public String getTag3() 
    {
        return tag3;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("countryId", getCountryId())
            .append("countryCode", getCountryCode())
            .append("countryChinese", getCountryChinese())
            .append("countryEnglish", getCountryEnglish())
            .append("declareCode", getDeclareCode())
            .append("tag1", getTag1())
            .append("tag2", getTag2())
            .append("tag3", getTag3())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
