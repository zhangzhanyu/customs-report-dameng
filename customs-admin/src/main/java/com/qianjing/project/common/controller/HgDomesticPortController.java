package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgDomesticPort;
import com.qianjing.project.common.service.IHgDomesticPortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 国内口岸代码Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/port")
public class HgDomesticPortController extends BaseController
{
    @Autowired
    private IHgDomesticPortService hgDomesticPortService;

    /**
     * 查询国内口岸代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:port:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgDomesticPort hgDomesticPort)
    {
        startPage();
        List<HgDomesticPort> list = hgDomesticPortService.selectHgDomesticPortList(hgDomesticPort);
        return getDataTable(list);
    }

    /**
     * 导出国内口岸代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:port:export')")
    @Log(title = "国内口岸代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgDomesticPort hgDomesticPort)
    {
        List<HgDomesticPort> list = hgDomesticPortService.selectHgDomesticPortList(hgDomesticPort);
        ExcelUtil<HgDomesticPort> util = new ExcelUtil<HgDomesticPort>(HgDomesticPort.class);
        return util.exportExcel(list, "国内口岸代码数据");
    }

    /**
     * 导入国内口岸代码列表
     */
    @Log(title = "国内口岸代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:port:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgDomesticPort> util = new ExcelUtil<HgDomesticPort>(HgDomesticPort.class);
        List<HgDomesticPort> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgDomesticPortService.importHgDomesticPort(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载国内口岸代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgDomesticPort> util = new ExcelUtil<HgDomesticPort>(HgDomesticPort.class);
        util.importTemplateExcel(response, "国内口岸代码数据");
    }

    /**
     * 获取国内口岸代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:port:query')")
    @GetMapping(value = "/{domesticPortId}")
    public AjaxResult getInfo(@PathVariable("domesticPortId") Long domesticPortId)
    {
        return AjaxResult.success(hgDomesticPortService.selectHgDomesticPortByDomesticPortId(domesticPortId));
    }

    /**
     * 新增国内口岸代码
     */
    @PreAuthorize("@ss.hasPermi('common:port:add')")
    @Log(title = "国内口岸代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgDomesticPort hgDomesticPort)
    {
        return toAjax(hgDomesticPortService.insertHgDomesticPort(hgDomesticPort));
    }

    /**
     * 修改国内口岸代码
     */
    @PreAuthorize("@ss.hasPermi('common:port:edit')")
    @Log(title = "国内口岸代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgDomesticPort hgDomesticPort)
    {
        return toAjax(hgDomesticPortService.updateHgDomesticPort(hgDomesticPort));
    }

    /**
     * 删除国内口岸代码
     */
    @PreAuthorize("@ss.hasPermi('common:port:remove')")
    @Log(title = "国内口岸代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{domesticPortIds}")
    public AjaxResult remove(@PathVariable Long[] domesticPortIds)
    {
        return toAjax(hgDomesticPortService.deleteHgDomesticPortByDomesticPortIds(domesticPortIds));
    }
}
