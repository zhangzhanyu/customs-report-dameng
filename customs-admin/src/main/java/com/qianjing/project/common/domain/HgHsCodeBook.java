package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * HS编码册对象 hg_hs_code_book
 * 
 * @author qianjing
 * @date 2022-08-17
 */
public class HgHsCodeBook extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long hsCodeBookId;

    /** hs编码名称 */
    @Excel(name = "hs编码名称")
    private String hsName;

    /** hs编码 */
    @Excel(name = "hs编码")
    private String hsCode;

    /** 扩展码 */
    @Excel(name = "扩展码")
    private String hsCodeSpreading;

    /** ciq商品名称 */
    @Excel(name = "ciq商品名称")
    private String ciqName;

    /** ciq分类代码 */
    @Excel(name = "ciq分类代码")
    private String ciqCode;

    /** ciq分类代名称 */
    @Excel(name = "ciq分类代名称")
    private String ciqAbbreviation;

    /** 父ciq分类代码 */
    @Excel(name = "父ciq分类代码")
    private String ciqCodeParent;

    /** 作废标识（0未作废 1已作废） */
    @Excel(name = "作废标识", readConverterExp = "0=未作废,1=已作废")
    private String discardFlag;

    public void setHsCodeBookId(Long hsCodeBookId) 
    {
        this.hsCodeBookId = hsCodeBookId;
    }

    public Long getHsCodeBookId() 
    {
        return hsCodeBookId;
    }
    public void setHsName(String hsName) 
    {
        this.hsName = hsName;
    }

    public String getHsName() 
    {
        return hsName;
    }
    public void setHsCode(String hsCode) 
    {
        this.hsCode = hsCode;
    }

    public String getHsCode() 
    {
        return hsCode;
    }
    public void setHsCodeSpreading(String hsCodeSpreading) 
    {
        this.hsCodeSpreading = hsCodeSpreading;
    }

    public String getHsCodeSpreading() 
    {
        return hsCodeSpreading;
    }
    public void setCiqName(String ciqName) 
    {
        this.ciqName = ciqName;
    }

    public String getCiqName() 
    {
        return ciqName;
    }
    public void setCiqCode(String ciqCode) 
    {
        this.ciqCode = ciqCode;
    }

    public String getCiqCode() 
    {
        return ciqCode;
    }
    public void setCiqAbbreviation(String ciqAbbreviation) 
    {
        this.ciqAbbreviation = ciqAbbreviation;
    }

    public String getCiqAbbreviation() 
    {
        return ciqAbbreviation;
    }
    public void setCiqCodeParent(String ciqCodeParent) 
    {
        this.ciqCodeParent = ciqCodeParent;
    }

    public String getCiqCodeParent() 
    {
        return ciqCodeParent;
    }
    public void setDiscardFlag(String discardFlag) 
    {
        this.discardFlag = discardFlag;
    }

    public String getDiscardFlag() 
    {
        return discardFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("hsCodeBookId", getHsCodeBookId())
            .append("hsName", getHsName())
            .append("hsCode", getHsCode())
            .append("hsCodeSpreading", getHsCodeSpreading())
            .append("ciqName", getCiqName())
            .append("ciqCode", getCiqCode())
            .append("ciqAbbreviation", getCiqAbbreviation())
            .append("ciqCodeParent", getCiqCodeParent())
            .append("discardFlag", getDiscardFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
