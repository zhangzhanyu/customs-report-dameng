package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgLevelEvaluate;

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * 风险分级评估Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * 2023-12-08 下午3:00    张占宇          V1.0          initialize
 */
public interface HgLevelEvaluateMapper {

    HgLevelEvaluate selectHgLevelEvaluateById(Long id);

    List<HgLevelEvaluate> selectHgLevelEvaluateList(HgLevelEvaluate hgLevelEvaluate);

    int insertHgLevelEvaluate(HgLevelEvaluate hgLevelEvaluate);

    int updateHgLevelEvaluate(HgLevelEvaluate hgLevelEvaluate);

    int deleteHgLevelEvaluateById(Long id);

    int deleteHgLevelEvaluateByIds(Long[] ids);
}
