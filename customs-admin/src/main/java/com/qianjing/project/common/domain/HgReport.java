package com.qianjing.project.common.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 动态报表对象 hg_report
 * 
 * @author qianjing
 * @date 2022-09-06
 */
public class HgReport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long reportId;

    private Long tableId;

    /** 进出口 */
    @Excel(name = "进出口")
    private String iEFlag;

    /** 报关单号 */
    @Excel(name = "报关单号")
    private String entryId;

    /** 项号 */
    @Excel(name = "项号")
    private String gNo;

    /** 货物属性代码 */
    @Excel(name = "货物属性代码")
    private String productCharCode;

    /** HS编码 */
    @Excel(name = "HS编码")
    private String codeTs;

    /** CIQ编码 */
    @Excel(name = "CIQ编码")
    private String iqCode;

    /** 运输方式 */
    @Excel(name = "运输方式")
    private String trafMode;

    /** 启用国别/地区 */
    @Excel(name = "启用国别/地区")
    private String originCountry;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String gName;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String gModel;

    /** 对应《危险化学品目录》名称 */
    @Excel(name = "对应《危险化学品目录》名称")
    private String correspondingName;

    /** UN编号 */
    @Excel(name = "UN编号")
    private String ungid;

    /** 第一(法定)数量 */
    @Excel(name = "第一(法定)数量")
    private String qty1;

    /** 第一(法定)计量单位 */
    @Excel(name = "第一(法定)计量单位")
    private String unit1;

    /** 货值(美元) */
    @Excel(name = "货值(美元)")
    private String usdPrice;

    /** 货值(人民币) */
    @Excel(name = "货值(人民币)")
    private String rmbPrice;

    /** 商品项查验结果（0异常 1无异常 2无法确认） */
    @Excel(name = "商品项查验结果", readConverterExp = "0=异常,1=无异常,2=无法确认")
    private String checkResult;

    /** 备注（1非危险品 2不合格） */
    @Excel(name = "备注", readConverterExp = "1=非危险品,2=不合格")
    private String remarks;

    /** 不合格类别（1标签 2安全技术说明书 3包装 4品质 5数量 6重量 7内容物特性与申报信息不符 8其他） */
    @Excel(name = "不合格类别", readConverterExp = "1=标签,2=安全技术说明书,3=包装,4=品质,5=数量,6=重量,7=内容物特性与申报信息不符,8=其他")
    private String nonconformityCategory;

    /** 处置（1技术整改 2退运 3使用救助包装 4降级 5无害化处理 6其他） */
    @Excel(name = "处置", readConverterExp = "1=技术整改,2=退运,3=使用救助包装,4=降级,5=无害化处理,6=其他")
    private String disposal;

    /** 放行时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "放行时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date releaseTime;

    public void setReportId(Long reportId) 
    {
        this.reportId = reportId;
    }

    public Long getReportId() 
    {
        return reportId;
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }

    public void setiEFlag(String iEFlag)
    {
        this.iEFlag = iEFlag;
    }

    public String getiEFlag() 
    {
        return iEFlag;
    }
    public void setEntryId(String entryId) 
    {
        this.entryId = entryId;
    }

    public String getEntryId() 
    {
        return entryId;
    }
    public void setgNo(String gNo) 
    {
        this.gNo = gNo;
    }

    public String getgNo() 
    {
        return gNo;
    }
    public void setProductCharCode(String productCharCode) 
    {
        this.productCharCode = productCharCode;
    }

    public String getProductCharCode() 
    {
        return productCharCode;
    }
    public void setCodeTs(String codeTs) 
    {
        this.codeTs = codeTs;
    }

    public String getCodeTs() 
    {
        return codeTs;
    }
    public void setIqCode(String iqCode) 
    {
        this.iqCode = iqCode;
    }

    public String getIqCode() 
    {
        return iqCode;
    }
    public void setTrafMode(String trafMode) 
    {
        this.trafMode = trafMode;
    }

    public String getTrafMode() 
    {
        return trafMode;
    }
    public void setOriginCountry(String originCountry) 
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry() 
    {
        return originCountry;
    }
    public void setgName(String gName) 
    {
        this.gName = gName;
    }

    public String getgName() 
    {
        return gName;
    }
    public void setgModel(String gModel) 
    {
        this.gModel = gModel;
    }

    public String getgModel() 
    {
        return gModel;
    }
    public void setCorrespondingName(String correspondingName) 
    {
        this.correspondingName = correspondingName;
    }

    public String getCorrespondingName() 
    {
        return correspondingName;
    }
    public void setUngid(String ungid) 
    {
        this.ungid = ungid;
    }

    public String getUngid() 
    {
        return ungid;
    }
    public void setQty1(String qty1) 
    {
        this.qty1 = qty1;
    }

    public String getQty1() 
    {
        return qty1;
    }
    public void setUnit1(String unit1) 
    {
        this.unit1 = unit1;
    }

    public String getUnit1() 
    {
        return unit1;
    }
    public void setUsdPrice(String usdPrice) 
    {
        this.usdPrice = usdPrice;
    }

    public String getUsdPrice() 
    {
        return usdPrice;
    }
    public void setRmbPrice(String rmbPrice) 
    {
        this.rmbPrice = rmbPrice;
    }

    public String getRmbPrice() 
    {
        return rmbPrice;
    }
    public void setCheckResult(String checkResult) 
    {
        this.checkResult = checkResult;
    }

    public String getCheckResult() 
    {
        return checkResult;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setNonconformityCategory(String nonconformityCategory) 
    {
        this.nonconformityCategory = nonconformityCategory;
    }

    public String getNonconformityCategory() 
    {
        return nonconformityCategory;
    }
    public void setDisposal(String disposal) 
    {
        this.disposal = disposal;
    }

    public String getDisposal() 
    {
        return disposal;
    }
    public void setReleaseTime(Date releaseTime) 
    {
        this.releaseTime = releaseTime;
    }

    public Date getReleaseTime() 
    {
        return releaseTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("reportId", getReportId())
            .append("iEFlag", getiEFlag())
            .append("entryId", getEntryId())
            .append("gNo", getgNo())
            .append("productCharCode", getProductCharCode())
            .append("codeTs", getCodeTs())
            .append("iqCode", getIqCode())
            .append("trafMode", getTrafMode())
            .append("originCountry", getOriginCountry())
            .append("gName", getgName())
            .append("gModel", getgModel())
            .append("correspondingName", getCorrespondingName())
            .append("ungid", getUngid())
            .append("qty1", getQty1())
            .append("unit1", getUnit1())
            .append("usdPrice", getUsdPrice())
            .append("rmbPrice", getRmbPrice())
            .append("checkResult", getCheckResult())
            .append("remarks", getRemarks())
            .append("nonconformityCategory", getNonconformityCategory())
            .append("disposal", getDisposal())
            .append("releaseTime", getReleaseTime())
            .toString();
    }
}
