package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 体系类章对照对象 hg_hs_catalog
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgHsCatalog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 体系类章对照编号 */
    private Long hsCatalogId;

    /** 类章代码 */
    @Excel(name = "类章代码")
    private String code;

    /** 单位1 */
    @Excel(name = "单位1")
    private String unit1;

    /** 单位2 */
    @Excel(name = "单位2")
    private String unit2;

    /** 单位1名称 */
    @Excel(name = "单位1名称")
    private String unit1Name;

    /** 单位2名称 */
    @Excel(name = "单位2名称")
    private String unit2Name;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 层级 */
    @Excel(name = "层级")
    private Long layerid;

    /** 排序 */
    @Excel(name = "排序")
    private Long sortid;

    /** 标签1 */
    @Excel(name = "标签1")
    private String tag1;

    /** 标签2 */
    @Excel(name = "标签2")
    private String tag2;

    /** 标签3 */
    @Excel(name = "标签3")
    private String tag3;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    public void setHsCatalogId(Long hsCatalogId) 
    {
        this.hsCatalogId = hsCatalogId;
    }

    public Long getHsCatalogId() 
    {
        return hsCatalogId;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setUnit1(String unit1) 
    {
        this.unit1 = unit1;
    }

    public String getUnit1() 
    {
        return unit1;
    }
    public void setUnit2(String unit2) 
    {
        this.unit2 = unit2;
    }

    public String getUnit2() 
    {
        return unit2;
    }
    public void setUnit1Name(String unit1Name) 
    {
        this.unit1Name = unit1Name;
    }

    public String getUnit1Name() 
    {
        return unit1Name;
    }
    public void setUnit2Name(String unit2Name) 
    {
        this.unit2Name = unit2Name;
    }

    public String getUnit2Name() 
    {
        return unit2Name;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setLayerid(Long layerid) 
    {
        this.layerid = layerid;
    }

    public Long getLayerid() 
    {
        return layerid;
    }
    public void setSortid(Long sortid) 
    {
        this.sortid = sortid;
    }

    public Long getSortid() 
    {
        return sortid;
    }
    public void setTag1(String tag1) 
    {
        this.tag1 = tag1;
    }

    public String getTag1() 
    {
        return tag1;
    }
    public void setTag2(String tag2) 
    {
        this.tag2 = tag2;
    }

    public String getTag2() 
    {
        return tag2;
    }
    public void setTag3(String tag3) 
    {
        this.tag3 = tag3;
    }

    public String getTag3() 
    {
        return tag3;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("hsCatalogId", getHsCatalogId())
            .append("code", getCode())
            .append("unit1", getUnit1())
            .append("unit2", getUnit2())
            .append("unit1Name", getUnit1Name())
            .append("unit2Name", getUnit2Name())
            .append("name", getName())
            .append("layerid", getLayerid())
            .append("sortid", getSortid())
            .append("tag1", getTag1())
            .append("tag2", getTag2())
            .append("tag3", getTag3())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
