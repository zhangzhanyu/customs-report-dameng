package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgInlandRegion;
import com.qianjing.project.common.service.IHgInlandRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 国内地区代码Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/inlandRegion")
public class HgInlandRegionController extends BaseController
{
    @Autowired
    private IHgInlandRegionService hgInlandRegionService;

    /**
     * 查询国内地区代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:inlandRegion:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgInlandRegion hgInlandRegion)
    {
        startPage();
        List<HgInlandRegion> list = hgInlandRegionService.selectHgInlandRegionList(hgInlandRegion);
        return getDataTable(list);
    }

    /**
     * 导出国内地区代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:inlandRegion:export')")
    @Log(title = "国内地区代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgInlandRegion hgInlandRegion)
    {
        List<HgInlandRegion> list = hgInlandRegionService.selectHgInlandRegionList(hgInlandRegion);
        ExcelUtil<HgInlandRegion> util = new ExcelUtil<HgInlandRegion>(HgInlandRegion.class);
        return util.exportExcel(list, "国内地区代码数据");
    }

    /**
     * 导入国内地区代码列表
     */
    @Log(title = "国内地区代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:inlandRegion:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgInlandRegion> util = new ExcelUtil<HgInlandRegion>(HgInlandRegion.class);
        List<HgInlandRegion> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgInlandRegionService.importHgInlandRegion(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载国内地区代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgInlandRegion> util = new ExcelUtil<HgInlandRegion>(HgInlandRegion.class);
        util.importTemplateExcel(response, "国内地区代码数据");
    }

    /**
     * 获取国内地区代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:inlandRegion:query')")
    @GetMapping(value = "/{inlandRegionId}")
    public AjaxResult getInfo(@PathVariable("inlandRegionId") Long inlandRegionId)
    {
        return AjaxResult.success(hgInlandRegionService.selectHgInlandRegionByInlandRegionId(inlandRegionId));
    }

    /**
     * 新增国内地区代码
     */
    @PreAuthorize("@ss.hasPermi('common:inlandRegion:add')")
    @Log(title = "国内地区代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgInlandRegion hgInlandRegion)
    {
        return toAjax(hgInlandRegionService.insertHgInlandRegion(hgInlandRegion));
    }

    /**
     * 修改国内地区代码
     */
    @PreAuthorize("@ss.hasPermi('common:inlandRegion:edit')")
    @Log(title = "国内地区代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgInlandRegion hgInlandRegion)
    {
        return toAjax(hgInlandRegionService.updateHgInlandRegion(hgInlandRegion));
    }

    /**
     * 删除国内地区代码
     */
    @PreAuthorize("@ss.hasPermi('common:inlandRegion:remove')")
    @Log(title = "国内地区代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{inlandRegionIds}")
    public AjaxResult remove(@PathVariable Long[] inlandRegionIds)
    {
        return toAjax(hgInlandRegionService.deleteHgInlandRegionByInlandRegionIds(inlandRegionIds));
    }
}
