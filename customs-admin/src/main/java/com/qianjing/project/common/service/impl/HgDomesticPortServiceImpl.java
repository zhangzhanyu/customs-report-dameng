package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgDomesticPortMapper;
import com.qianjing.project.common.domain.HgDomesticPort;
import com.qianjing.project.common.service.IHgDomesticPortService;

/**
 * 国内口岸代码Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgDomesticPortServiceImpl implements IHgDomesticPortService 
{
    private static final Logger log = LoggerFactory.getLogger(HgDomesticPortServiceImpl.class);

    @Autowired
    private HgDomesticPortMapper hgDomesticPortMapper;

    /**
     * 查询国内口岸代码
     * 
     * @param domesticPortId 国内口岸代码主键
     * @return 国内口岸代码
     */
    @Override
    public HgDomesticPort selectHgDomesticPortByDomesticPortId(Long domesticPortId)
    {
        return hgDomesticPortMapper.selectHgDomesticPortByDomesticPortId(domesticPortId);
    }

    /**
     * 查询国内口岸代码列表
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 国内口岸代码
     */
    @Override
    public List<HgDomesticPort> selectHgDomesticPortList(HgDomesticPort hgDomesticPort)
    {
        return hgDomesticPortMapper.selectHgDomesticPortList(hgDomesticPort);
    }

    /**
     * 新增国内口岸代码
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 结果
     */
    @Override
    public int insertHgDomesticPort(HgDomesticPort hgDomesticPort)
    {
        hgDomesticPort.setCreateTime(DateUtils.getNowDate());
        return hgDomesticPortMapper.insertHgDomesticPort(hgDomesticPort);
    }

    /**
     * 修改国内口岸代码
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 结果
     */
    @Override
    public int updateHgDomesticPort(HgDomesticPort hgDomesticPort)
    {
        hgDomesticPort.setUpdateTime(DateUtils.getNowDate());
        return hgDomesticPortMapper.updateHgDomesticPort(hgDomesticPort);
    }

    /**
     * 批量删除国内口岸代码
     * 
     * @param domesticPortIds 需要删除的国内口岸代码主键
     * @return 结果
     */
    @Override
    public int deleteHgDomesticPortByDomesticPortIds(Long[] domesticPortIds)
    {
        return hgDomesticPortMapper.deleteHgDomesticPortByDomesticPortIds(domesticPortIds);
    }

    /**
     * 删除国内口岸代码信息
     * 
     * @param domesticPortId 国内口岸代码主键
     * @return 结果
     */
    @Override
    public int deleteHgDomesticPortByDomesticPortId(Long domesticPortId)
    {
        return hgDomesticPortMapper.deleteHgDomesticPortByDomesticPortId(domesticPortId);
    }

    /**
     * 导入国内口岸代码数据
     *
     * @param hgDomesticPortList hgDomesticPortList 国内口岸代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgDomesticPort(List<HgDomesticPort> hgDomesticPortList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgDomesticPortList) || hgDomesticPortList.size() == 0) {
            throw new ServiceException("导入国内口岸代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgDomesticPort hgDomesticPort : hgDomesticPortList){
            try {
                // 验证是否存在
                if (true) {
                    hgDomesticPort.setCreateBy(operName);
                    this.insertHgDomesticPort(hgDomesticPort);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgDomesticPort.setUpdateBy(operName);
                    this.updateHgDomesticPort(hgDomesticPort);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
