package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgTreeDictionaryMapper;
import com.qianjing.project.common.domain.HgTreeDictionary;
import com.qianjing.project.common.service.IHgTreeDictionaryService;

/**
 * 字典（树形）Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgTreeDictionaryServiceImpl implements IHgTreeDictionaryService 
{
    private static final Logger log = LoggerFactory.getLogger(HgTreeDictionaryServiceImpl.class);

    @Autowired
    private HgTreeDictionaryMapper hgTreeDictionaryMapper;

    /**
     * 查询字典（树形）
     * 
     * @param treeDicId 字典（树形）主键
     * @return 字典（树形）
     */
    @Override
    public HgTreeDictionary selectHgTreeDictionaryByTreeDicId(Long treeDicId)
    {
        return hgTreeDictionaryMapper.selectHgTreeDictionaryByTreeDicId(treeDicId);
    }

    /**
     * 查询字典（树形）列表
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 字典（树形）
     */
    @Override
    public List<HgTreeDictionary> selectHgTreeDictionaryList(HgTreeDictionary hgTreeDictionary)
    {
        return hgTreeDictionaryMapper.selectHgTreeDictionaryList(hgTreeDictionary);
    }

    /**
     * 新增字典（树形）
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 结果
     */
    @Override
    public int insertHgTreeDictionary(HgTreeDictionary hgTreeDictionary)
    {
        hgTreeDictionary.setCreateTime(DateUtils.getNowDate());
        return hgTreeDictionaryMapper.insertHgTreeDictionary(hgTreeDictionary);
    }

    /**
     * 修改字典（树形）
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 结果
     */
    @Override
    public int updateHgTreeDictionary(HgTreeDictionary hgTreeDictionary)
    {
        hgTreeDictionary.setUpdateTime(DateUtils.getNowDate());
        return hgTreeDictionaryMapper.updateHgTreeDictionary(hgTreeDictionary);
    }

    /**
     * 批量删除字典（树形）
     * 
     * @param treeDicIds 需要删除的字典（树形）主键
     * @return 结果
     */
    @Override
    public int deleteHgTreeDictionaryByTreeDicIds(Long[] treeDicIds)
    {
        return hgTreeDictionaryMapper.deleteHgTreeDictionaryByTreeDicIds(treeDicIds);
    }

    /**
     * 删除字典（树形）信息
     * 
     * @param treeDicId 字典（树形）主键
     * @return 结果
     */
    @Override
    public int deleteHgTreeDictionaryByTreeDicId(Long treeDicId)
    {
        return hgTreeDictionaryMapper.deleteHgTreeDictionaryByTreeDicId(treeDicId);
    }

    /**
     * 导入字典（树形）数据
     *
     * @param hgTreeDictionaryList hgTreeDictionaryList 字典（树形）信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgTreeDictionary(List<HgTreeDictionary> hgTreeDictionaryList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgTreeDictionaryList) || hgTreeDictionaryList.size() == 0) {
            throw new ServiceException("导入字典（树形）数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgTreeDictionary hgTreeDictionary : hgTreeDictionaryList){
            try {
                // 验证是否存在
                if (true) {
                    hgTreeDictionary.setCreateBy(operName);
                    this.insertHgTreeDictionary(hgTreeDictionary);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgTreeDictionary.setUpdateBy(operName);
                    this.updateHgTreeDictionary(hgTreeDictionary);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
