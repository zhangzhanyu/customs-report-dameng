package com.qianjing.project.common.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 商品分类代码对象 hg_goods_classification
 *
 * @author zhangzy
 * @date 2022-08-04
 */
public class HgGoodsClassification extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long goodsClassificationId;

    /** 商品分类 */
    @Excel(name = "商品分类")
    private String classificationName;

    /** HS编码 */
    @Excel(name = "HS编码")
    private String hsCode;

    /** HS编码名称 */
    @Excel(name = "HS编码名称")
    private String hsName;

    /** CIQ编码 */
    @Excel(name = "CIQ编码")
    private String ciqCode;

    /** CIQ商品名称 */
    @Excel(name = "CIQ商品名称")
    private String ciqName;

    /** 货物属性 */
    @Excel(name = "货物属性")
    private String productCharCode;

    /** 有效期至 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期至", width = 30, dateFormat = "yyyy-MM-dd")
    private Date effectiveDate;

    /** 删除标志（0未删，1删除） */
    private String delFlag;

    /** 是否长期有效 true是 false否 */
    private Boolean effectiveFlag;

    public void setGoodsClassificationId(Long goodsClassificationId)
    {
        this.goodsClassificationId = goodsClassificationId;
    }

    public Long getGoodsClassificationId()
    {
        return goodsClassificationId;
    }
    public void setClassificationName(String classificationName)
    {
        this.classificationName = classificationName;
    }

    public String getClassificationName()
    {
        return classificationName;
    }
    public void setHsCode(String hsCode)
    {
        this.hsCode = hsCode;
    }

    public String getHsCode()
    {
        return hsCode;
    }
    public void setHsName(String hsName)
    {
        this.hsName = hsName;
    }

    public String getHsName()
    {
        return hsName;
    }
    public void setCiqCode(String ciqCode)
    {
        this.ciqCode = ciqCode;
    }

    public String getCiqCode()
    {
        return ciqCode;
    }
    public void setCiqName(String ciqName)
    {
        this.ciqName = ciqName;
    }

    public String getCiqName()
    {
        return ciqName;
    }
    public void setProductCharCode(String productCharCode)
    {
        this.productCharCode = productCharCode;
    }

    public String getProductCharCode()
    {
        return productCharCode;
    }
    public void setEffectiveDate(Date effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    public Date getEffectiveDate()
    {
        return effectiveDate;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public Boolean getEffectiveFlag() {
        return effectiveFlag;
    }

    public void setEffectiveFlag(Boolean effectiveFlag) {
        this.effectiveFlag = effectiveFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("goodsClassificationId", getGoodsClassificationId())
                .append("classificationName", getClassificationName())
                .append("hsCode", getHsCode())
                .append("hsName", getHsName())
                .append("ciqCode", getCiqCode())
                .append("ciqName", getCiqName())
                .append("productCharCode", getProductCharCode())
                .append("effectiveDate", getEffectiveDate())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("delFlag", getDelFlag())
                .toString();
    }
}
