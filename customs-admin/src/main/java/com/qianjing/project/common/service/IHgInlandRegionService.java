package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgInlandRegion;

/**
 * 国内地区代码Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgInlandRegionService 
{
    /**
     * 查询国内地区代码
     * 
     * @param inlandRegionId 国内地区代码主键
     * @return 国内地区代码
     */
    public HgInlandRegion selectHgInlandRegionByInlandRegionId(Long inlandRegionId);

    /**
     * 查询国内地区代码列表
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 国内地区代码集合
     */
    public List<HgInlandRegion> selectHgInlandRegionList(HgInlandRegion hgInlandRegion);

    /**
     * 新增国内地区代码
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 结果
     */
    public int insertHgInlandRegion(HgInlandRegion hgInlandRegion);

    /**
     * 修改国内地区代码
     * 
     * @param hgInlandRegion 国内地区代码
     * @return 结果
     */
    public int updateHgInlandRegion(HgInlandRegion hgInlandRegion);

    /**
     * 批量删除国内地区代码
     * 
     * @param inlandRegionIds 需要删除的国内地区代码主键集合
     * @return 结果
     */
    public int deleteHgInlandRegionByInlandRegionIds(Long[] inlandRegionIds);

    /**
     * 删除国内地区代码信息
     * 
     * @param inlandRegionId 国内地区代码主键
     * @return 结果
     */
    public int deleteHgInlandRegionByInlandRegionId(Long inlandRegionId);

    /**
     * 导入国内地区代码信息
     *
     * @param hgInlandRegionList 国内地区代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgInlandRegion(List<HgInlandRegion> hgInlandRegionList, Boolean isUpdateSupport, String operName);
}
