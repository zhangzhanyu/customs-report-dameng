package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgJobDealResultMapper;
import com.qianjing.project.common.domain.HgJobDealResult;
import com.qianjing.project.common.service.IHgJobDealResultService;

/**
 * 检查作业处理结果清单Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgJobDealResultServiceImpl implements IHgJobDealResultService 
{
    private static final Logger log = LoggerFactory.getLogger(HgJobDealResultServiceImpl.class);

    @Autowired
    private HgJobDealResultMapper hgJobDealResultMapper;

    /**
     * 查询检查作业处理结果清单
     * 
     * @param jobDealResultId 检查作业处理结果清单主键
     * @return 检查作业处理结果清单
     */
    @Override
    public HgJobDealResult selectHgJobDealResultByJobDealResultId(Long jobDealResultId)
    {
        return hgJobDealResultMapper.selectHgJobDealResultByJobDealResultId(jobDealResultId);
    }

    /**
     * 查询检查作业处理结果清单列表
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 检查作业处理结果清单
     */
    @Override
    public List<HgJobDealResult> selectHgJobDealResultList(HgJobDealResult hgJobDealResult)
    {
        return hgJobDealResultMapper.selectHgJobDealResultList(hgJobDealResult);
    }

    /**
     * 新增检查作业处理结果清单
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 结果
     */
    @Override
    public int insertHgJobDealResult(HgJobDealResult hgJobDealResult)
    {
        hgJobDealResult.setCreateTime(DateUtils.getNowDate());
        return hgJobDealResultMapper.insertHgJobDealResult(hgJobDealResult);
    }

    /**
     * 修改检查作业处理结果清单
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 结果
     */
    @Override
    public int updateHgJobDealResult(HgJobDealResult hgJobDealResult)
    {
        hgJobDealResult.setUpdateTime(DateUtils.getNowDate());
        return hgJobDealResultMapper.updateHgJobDealResult(hgJobDealResult);
    }

    /**
     * 批量删除检查作业处理结果清单
     * 
     * @param jobDealResultIds 需要删除的检查作业处理结果清单主键
     * @return 结果
     */
    @Override
    public int deleteHgJobDealResultByJobDealResultIds(Long[] jobDealResultIds)
    {
        return hgJobDealResultMapper.deleteHgJobDealResultByJobDealResultIds(jobDealResultIds);
    }

    /**
     * 删除检查作业处理结果清单信息
     * 
     * @param jobDealResultId 检查作业处理结果清单主键
     * @return 结果
     */
    @Override
    public int deleteHgJobDealResultByJobDealResultId(Long jobDealResultId)
    {
        return hgJobDealResultMapper.deleteHgJobDealResultByJobDealResultId(jobDealResultId);
    }

    /**
     * 导入检查作业处理结果清单数据
     *
     * @param hgJobDealResultList hgJobDealResultList 检查作业处理结果清单信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgJobDealResult(List<HgJobDealResult> hgJobDealResultList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgJobDealResultList) || hgJobDealResultList.size() == 0) {
            throw new ServiceException("导入检查作业处理结果清单数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgJobDealResult hgJobDealResult : hgJobDealResultList){
            try {
                // 验证是否存在
                if (true) {
                    hgJobDealResult.setCreateBy(operName);
                    this.insertHgJobDealResult(hgJobDealResult);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgJobDealResult.setUpdateBy(operName);
                    this.updateHgJobDealResult(hgJobDealResult);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
