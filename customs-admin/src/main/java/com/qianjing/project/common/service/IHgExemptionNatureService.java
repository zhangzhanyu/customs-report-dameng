package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgExemptionNature;

/**
 * 征免性质代码Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgExemptionNatureService 
{
    /**
     * 查询征免性质代码
     * 
     * @param exemptionNatureId 征免性质代码主键
     * @return 征免性质代码
     */
    public HgExemptionNature selectHgExemptionNatureByExemptionNatureId(Long exemptionNatureId);

    /**
     * 查询征免性质代码列表
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 征免性质代码集合
     */
    public List<HgExemptionNature> selectHgExemptionNatureList(HgExemptionNature hgExemptionNature);

    /**
     * 新增征免性质代码
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 结果
     */
    public int insertHgExemptionNature(HgExemptionNature hgExemptionNature);

    /**
     * 修改征免性质代码
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 结果
     */
    public int updateHgExemptionNature(HgExemptionNature hgExemptionNature);

    /**
     * 批量删除征免性质代码
     * 
     * @param exemptionNatureIds 需要删除的征免性质代码主键集合
     * @return 结果
     */
    public int deleteHgExemptionNatureByExemptionNatureIds(Long[] exemptionNatureIds);

    /**
     * 删除征免性质代码信息
     * 
     * @param exemptionNatureId 征免性质代码主键
     * @return 结果
     */
    public int deleteHgExemptionNatureByExemptionNatureId(Long exemptionNatureId);

    /**
     * 导入征免性质代码信息
     *
     * @param hgExemptionNatureList 征免性质代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgExemptionNature(List<HgExemptionNature> hgExemptionNatureList, Boolean isUpdateSupport, String operName);
}
