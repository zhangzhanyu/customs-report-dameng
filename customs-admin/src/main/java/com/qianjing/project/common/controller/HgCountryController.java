package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgCountry;
import com.qianjing.project.common.service.IHgCountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 国别地区代码Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/country")
public class HgCountryController extends BaseController
{
    @Autowired
    private IHgCountryService hgCountryService;

    /**
     * 查询国别地区代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:country:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgCountry hgCountry)
    {
        startPage();
        List<HgCountry> list = hgCountryService.selectHgCountryList(hgCountry);
        return getDataTable(list);
    }

    /**
     * 导出国别地区代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:country:export')")
    @Log(title = "国别地区代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgCountry hgCountry)
    {
        List<HgCountry> list = hgCountryService.selectHgCountryList(hgCountry);
        ExcelUtil<HgCountry> util = new ExcelUtil<HgCountry>(HgCountry.class);
        return util.exportExcel(list, "国别地区代码数据");
    }

    /**
     * 导入国别地区代码列表
     */
    @Log(title = "国别地区代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:country:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgCountry> util = new ExcelUtil<HgCountry>(HgCountry.class);
        List<HgCountry> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgCountryService.importHgCountry(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载国别地区代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgCountry> util = new ExcelUtil<HgCountry>(HgCountry.class);
        util.importTemplateExcel(response, "国别地区代码数据");
    }

    /**
     * 获取国别地区代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:country:query')")
    @GetMapping(value = "/{countryId}")
    public AjaxResult getInfo(@PathVariable("countryId") Long countryId)
    {
        return AjaxResult.success(hgCountryService.selectHgCountryByCountryId(countryId));
    }

    /**
     * 新增国别地区代码
     */
    @PreAuthorize("@ss.hasPermi('common:country:add')")
    @Log(title = "国别地区代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgCountry hgCountry)
    {
        return toAjax(hgCountryService.insertHgCountry(hgCountry));
    }

    /**
     * 修改国别地区代码
     */
    @PreAuthorize("@ss.hasPermi('common:country:edit')")
    @Log(title = "国别地区代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgCountry hgCountry)
    {
        return toAjax(hgCountryService.updateHgCountry(hgCountry));
    }

    /**
     * 删除国别地区代码
     */
    @PreAuthorize("@ss.hasPermi('common:country:remove')")
    @Log(title = "国别地区代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{countryIds}")
    public AjaxResult remove(@PathVariable Long[] countryIds)
    {
        return toAjax(hgCountryService.deleteHgCountryByCountryIds(countryIds));
    }
}
