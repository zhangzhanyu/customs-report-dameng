package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgGoodsExportParamMapper;
import com.qianjing.project.common.domain.HgGoodsExportParam;
import com.qianjing.project.common.service.IHgGoodsExportParamService;

/**
 * 商品出口参数Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgGoodsExportParamServiceImpl implements IHgGoodsExportParamService 
{
    private static final Logger log = LoggerFactory.getLogger(HgGoodsExportParamServiceImpl.class);

    @Autowired
    private HgGoodsExportParamMapper hgGoodsExportParamMapper;

    /**
     * 查询商品出口参数
     * 
     * @param goodsExportParamId 商品出口参数主键
     * @return 商品出口参数
     */
    @Override
    public HgGoodsExportParam selectHgGoodsExportParamByGoodsExportParamId(Long goodsExportParamId)
    {
        return hgGoodsExportParamMapper.selectHgGoodsExportParamByGoodsExportParamId(goodsExportParamId);
    }

    /**
     * 查询商品出口参数列表
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 商品出口参数
     */
    @Override
    public List<HgGoodsExportParam> selectHgGoodsExportParamList(HgGoodsExportParam hgGoodsExportParam)
    {
        return hgGoodsExportParamMapper.selectHgGoodsExportParamList(hgGoodsExportParam);
    }

    /**
     * 新增商品出口参数
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 结果
     */
    @Override
    public int insertHgGoodsExportParam(HgGoodsExportParam hgGoodsExportParam)
    {
        hgGoodsExportParam.setCreateTime(DateUtils.getNowDate());
        return hgGoodsExportParamMapper.insertHgGoodsExportParam(hgGoodsExportParam);
    }

    /**
     * 修改商品出口参数
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 结果
     */
    @Override
    public int updateHgGoodsExportParam(HgGoodsExportParam hgGoodsExportParam)
    {
        hgGoodsExportParam.setUpdateTime(DateUtils.getNowDate());
        return hgGoodsExportParamMapper.updateHgGoodsExportParam(hgGoodsExportParam);
    }

    /**
     * 批量删除商品出口参数
     * 
     * @param goodsExportParamIds 需要删除的商品出口参数主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsExportParamByGoodsExportParamIds(Long[] goodsExportParamIds)
    {
        return hgGoodsExportParamMapper.deleteHgGoodsExportParamByGoodsExportParamIds(goodsExportParamIds);
    }

    /**
     * 删除商品出口参数信息
     * 
     * @param goodsExportParamId 商品出口参数主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsExportParamByGoodsExportParamId(Long goodsExportParamId)
    {
        return hgGoodsExportParamMapper.deleteHgGoodsExportParamByGoodsExportParamId(goodsExportParamId);
    }

    /**
     * 导入商品出口参数数据
     *
     * @param hgGoodsExportParamList hgGoodsExportParamList 商品出口参数信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgGoodsExportParam(List<HgGoodsExportParam> hgGoodsExportParamList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgGoodsExportParamList) || hgGoodsExportParamList.size() == 0) {
            throw new ServiceException("导入商品出口参数数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgGoodsExportParam hgGoodsExportParam : hgGoodsExportParamList){
            try {
                // 验证是否存在
                if (true) {
                    hgGoodsExportParam.setCreateBy(operName);
                    this.insertHgGoodsExportParam(hgGoodsExportParam);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgGoodsExportParam.setUpdateBy(operName);
                    this.updateHgGoodsExportParam(hgGoodsExportParam);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
