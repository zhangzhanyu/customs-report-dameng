package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgEdiRemark;

/**
 * EDI申报备注Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgEdiRemarkService 
{
    /**
     * 查询EDI申报备注
     * 
     * @param ediId EDI申报备注主键
     * @return EDI申报备注
     */
    public HgEdiRemark selectHgEdiRemarkByEdiId(Long ediId);

    /**
     * 查询EDI申报备注列表
     * 
     * @param hgEdiRemark EDI申报备注
     * @return EDI申报备注集合
     */
    public List<HgEdiRemark> selectHgEdiRemarkList(HgEdiRemark hgEdiRemark);

    /**
     * 新增EDI申报备注
     * 
     * @param hgEdiRemark EDI申报备注
     * @return 结果
     */
    public int insertHgEdiRemark(HgEdiRemark hgEdiRemark);

    /**
     * 修改EDI申报备注
     * 
     * @param hgEdiRemark EDI申报备注
     * @return 结果
     */
    public int updateHgEdiRemark(HgEdiRemark hgEdiRemark);

    /**
     * 批量删除EDI申报备注
     * 
     * @param ediIds 需要删除的EDI申报备注主键集合
     * @return 结果
     */
    public int deleteHgEdiRemarkByEdiIds(Long[] ediIds);

    /**
     * 删除EDI申报备注信息
     * 
     * @param ediId EDI申报备注主键
     * @return 结果
     */
    public int deleteHgEdiRemarkByEdiId(Long ediId);

    /**
     * 导入EDI申报备注信息
     *
     * @param hgEdiRemarkList EDI申报备注信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgEdiRemark(List<HgEdiRemark> hgEdiRemarkList, Boolean isUpdateSupport, String operName);
}
