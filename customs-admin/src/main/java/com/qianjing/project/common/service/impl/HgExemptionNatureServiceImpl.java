package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgExemptionNatureMapper;
import com.qianjing.project.common.domain.HgExemptionNature;
import com.qianjing.project.common.service.IHgExemptionNatureService;

/**
 * 征免性质代码Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgExemptionNatureServiceImpl implements IHgExemptionNatureService 
{
    private static final Logger log = LoggerFactory.getLogger(HgExemptionNatureServiceImpl.class);

    @Autowired
    private HgExemptionNatureMapper hgExemptionNatureMapper;

    /**
     * 查询征免性质代码
     * 
     * @param exemptionNatureId 征免性质代码主键
     * @return 征免性质代码
     */
    @Override
    public HgExemptionNature selectHgExemptionNatureByExemptionNatureId(Long exemptionNatureId)
    {
        return hgExemptionNatureMapper.selectHgExemptionNatureByExemptionNatureId(exemptionNatureId);
    }

    /**
     * 查询征免性质代码列表
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 征免性质代码
     */
    @Override
    public List<HgExemptionNature> selectHgExemptionNatureList(HgExemptionNature hgExemptionNature)
    {
        return hgExemptionNatureMapper.selectHgExemptionNatureList(hgExemptionNature);
    }

    /**
     * 新增征免性质代码
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 结果
     */
    @Override
    public int insertHgExemptionNature(HgExemptionNature hgExemptionNature)
    {
        hgExemptionNature.setCreateTime(DateUtils.getNowDate());
        return hgExemptionNatureMapper.insertHgExemptionNature(hgExemptionNature);
    }

    /**
     * 修改征免性质代码
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 结果
     */
    @Override
    public int updateHgExemptionNature(HgExemptionNature hgExemptionNature)
    {
        hgExemptionNature.setUpdateTime(DateUtils.getNowDate());
        return hgExemptionNatureMapper.updateHgExemptionNature(hgExemptionNature);
    }

    /**
     * 批量删除征免性质代码
     * 
     * @param exemptionNatureIds 需要删除的征免性质代码主键
     * @return 结果
     */
    @Override
    public int deleteHgExemptionNatureByExemptionNatureIds(Long[] exemptionNatureIds)
    {
        return hgExemptionNatureMapper.deleteHgExemptionNatureByExemptionNatureIds(exemptionNatureIds);
    }

    /**
     * 删除征免性质代码信息
     * 
     * @param exemptionNatureId 征免性质代码主键
     * @return 结果
     */
    @Override
    public int deleteHgExemptionNatureByExemptionNatureId(Long exemptionNatureId)
    {
        return hgExemptionNatureMapper.deleteHgExemptionNatureByExemptionNatureId(exemptionNatureId);
    }

    /**
     * 导入征免性质代码数据
     *
     * @param hgExemptionNatureList hgExemptionNatureList 征免性质代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgExemptionNature(List<HgExemptionNature> hgExemptionNatureList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgExemptionNatureList) || hgExemptionNatureList.size() == 0) {
            throw new ServiceException("导入征免性质代码数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgExemptionNature hgExemptionNature : hgExemptionNatureList){
            try {
                // 验证是否存在
                if (true) {
                    hgExemptionNature.setCreateBy(operName);
                    this.insertHgExemptionNature(hgExemptionNature);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgExemptionNature.setUpdateBy(operName);
                    this.updateHgExemptionNature(hgExemptionNature);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
