package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsImportParam;

/**
 * 商品进口参数Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgGoodsImportParamMapper 
{
    /**
     * 查询商品进口参数
     * 
     * @param goodsImportParamId 商品进口参数主键
     * @return 商品进口参数
     */
    public HgGoodsImportParam selectHgGoodsImportParamByGoodsImportParamId(Long goodsImportParamId);

    /**
     * 查询商品进口参数列表
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 商品进口参数集合
     */
    public List<HgGoodsImportParam> selectHgGoodsImportParamList(HgGoodsImportParam hgGoodsImportParam);

    /**
     * 新增商品进口参数
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 结果
     */
    public int insertHgGoodsImportParam(HgGoodsImportParam hgGoodsImportParam);

    /**
     * 修改商品进口参数
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 结果
     */
    public int updateHgGoodsImportParam(HgGoodsImportParam hgGoodsImportParam);

    /**
     * 删除商品进口参数
     * 
     * @param goodsImportParamId 商品进口参数主键
     * @return 结果
     */
    public int deleteHgGoodsImportParamByGoodsImportParamId(Long goodsImportParamId);

    /**
     * 批量删除商品进口参数
     * 
     * @param goodsImportParamIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgGoodsImportParamByGoodsImportParamIds(Long[] goodsImportParamIds);
}
