package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgDomesticPort;

/**
 * 国内口岸代码Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgDomesticPortService 
{
    /**
     * 查询国内口岸代码
     * 
     * @param domesticPortId 国内口岸代码主键
     * @return 国内口岸代码
     */
    public HgDomesticPort selectHgDomesticPortByDomesticPortId(Long domesticPortId);

    /**
     * 查询国内口岸代码列表
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 国内口岸代码集合
     */
    public List<HgDomesticPort> selectHgDomesticPortList(HgDomesticPort hgDomesticPort);

    /**
     * 新增国内口岸代码
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 结果
     */
    public int insertHgDomesticPort(HgDomesticPort hgDomesticPort);

    /**
     * 修改国内口岸代码
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 结果
     */
    public int updateHgDomesticPort(HgDomesticPort hgDomesticPort);

    /**
     * 批量删除国内口岸代码
     * 
     * @param domesticPortIds 需要删除的国内口岸代码主键集合
     * @return 结果
     */
    public int deleteHgDomesticPortByDomesticPortIds(Long[] domesticPortIds);

    /**
     * 删除国内口岸代码信息
     * 
     * @param domesticPortId 国内口岸代码主键
     * @return 结果
     */
    public int deleteHgDomesticPortByDomesticPortId(Long domesticPortId);

    /**
     * 导入国内口岸代码信息
     *
     * @param hgDomesticPortList 国内口岸代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgDomesticPort(List<HgDomesticPort> hgDomesticPortList, Boolean isUpdateSupport, String operName);
}
