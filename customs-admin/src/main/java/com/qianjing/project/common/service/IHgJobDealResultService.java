package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgJobDealResult;

/**
 * 检查作业处理结果清单Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgJobDealResultService 
{
    /**
     * 查询检查作业处理结果清单
     * 
     * @param jobDealResultId 检查作业处理结果清单主键
     * @return 检查作业处理结果清单
     */
    public HgJobDealResult selectHgJobDealResultByJobDealResultId(Long jobDealResultId);

    /**
     * 查询检查作业处理结果清单列表
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 检查作业处理结果清单集合
     */
    public List<HgJobDealResult> selectHgJobDealResultList(HgJobDealResult hgJobDealResult);

    /**
     * 新增检查作业处理结果清单
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 结果
     */
    public int insertHgJobDealResult(HgJobDealResult hgJobDealResult);

    /**
     * 修改检查作业处理结果清单
     * 
     * @param hgJobDealResult 检查作业处理结果清单
     * @return 结果
     */
    public int updateHgJobDealResult(HgJobDealResult hgJobDealResult);

    /**
     * 批量删除检查作业处理结果清单
     * 
     * @param jobDealResultIds 需要删除的检查作业处理结果清单主键集合
     * @return 结果
     */
    public int deleteHgJobDealResultByJobDealResultIds(Long[] jobDealResultIds);

    /**
     * 删除检查作业处理结果清单信息
     * 
     * @param jobDealResultId 检查作业处理结果清单主键
     * @return 结果
     */
    public int deleteHgJobDealResultByJobDealResultId(Long jobDealResultId);

    /**
     * 导入检查作业处理结果清单信息
     *
     * @param hgJobDealResultList 检查作业处理结果清单信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgJobDealResult(List<HgJobDealResult> hgJobDealResultList, Boolean isUpdateSupport, String operName);
}
