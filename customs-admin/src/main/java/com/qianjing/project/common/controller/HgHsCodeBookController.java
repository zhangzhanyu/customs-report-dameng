package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgHsCodeBook;
import com.qianjing.project.common.service.IHgHsCodeBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * HS编码册Controller
 * 
 * @author qianjing
 * @date 2022-08-17
 */
@RestController
@RequestMapping("/common/hsCodeBook")
public class HgHsCodeBookController extends BaseController
{
    @Autowired
    private IHgHsCodeBookService hgHsCodeBookService;

    /**
     * 查询HS编码册列表
     */
    @PreAuthorize("@ss.hasPermi('common:hsCodeBook:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgHsCodeBook hgHsCodeBook)
    {
        startPage();
        List<HgHsCodeBook> list = hgHsCodeBookService.selectHgHsCodeBookList(hgHsCodeBook);
        return getDataTable(list);
    }

    /**
     * 导出HS编码册列表
     */
    @PreAuthorize("@ss.hasPermi('common:hsCodeBook:export')")
    @Log(title = "HS编码册", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgHsCodeBook hgHsCodeBook)
    {
        List<HgHsCodeBook> list = hgHsCodeBookService.selectHgHsCodeBookList(hgHsCodeBook);
        ExcelUtil<HgHsCodeBook> util = new ExcelUtil<HgHsCodeBook>(HgHsCodeBook.class);
        return util.exportExcel(list, "HS编码册数据");
    }

    /**
     * 导入HS编码册列表
     */
    @Log(title = "HS编码册", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:hsCodeBook:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgHsCodeBook> util = new ExcelUtil<HgHsCodeBook>(HgHsCodeBook.class);
        List<HgHsCodeBook> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgHsCodeBookService.importHgHsCodeBook(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载HS编码册导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgHsCodeBook> util = new ExcelUtil<HgHsCodeBook>(HgHsCodeBook.class);
        util.importTemplateExcel(response, "HS编码册数据");
    }

    /**
     * 获取HS编码册详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:hsCodeBook:query')")
    @GetMapping(value = "/{hsCodeBookId}")
    public AjaxResult getInfo(@PathVariable("hsCodeBookId") Long hsCodeBookId)
    {
        return AjaxResult.success(hgHsCodeBookService.selectHgHsCodeBookByHsCodeBookId(hsCodeBookId));
    }

    /**
     * 新增HS编码册
     */
    @PreAuthorize("@ss.hasPermi('common:hsCodeBook:add')")
    @Log(title = "HS编码册", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgHsCodeBook hgHsCodeBook)
    {
        return toAjax(hgHsCodeBookService.insertHgHsCodeBook(hgHsCodeBook));
    }

    /**
     * 修改HS编码册
     */
    @PreAuthorize("@ss.hasPermi('common:hsCodeBook:edit')")
    @Log(title = "HS编码册", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgHsCodeBook hgHsCodeBook)
    {
        return toAjax(hgHsCodeBookService.updateHgHsCodeBook(hgHsCodeBook));
    }

    /**
     * 删除HS编码册
     */
    @PreAuthorize("@ss.hasPermi('common:hsCodeBook:remove')")
    @Log(title = "HS编码册", businessType = BusinessType.DELETE)
	@DeleteMapping("/{hsCodeBookIds}")
    public AjaxResult remove(@PathVariable Long[] hsCodeBookIds)
    {
        return toAjax(hgHsCodeBookService.deleteHgHsCodeBookByHsCodeBookIds(hsCodeBookIds));
    }
}
