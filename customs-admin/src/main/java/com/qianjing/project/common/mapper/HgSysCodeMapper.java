package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgSysCode;

/**
 * 代码Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgSysCodeMapper 
{
    /**
     * 查询代码
     * 
     * @param codeId 代码主键
     * @return 代码
     */
    public HgSysCode selectHgSysCodeByCodeId(Long codeId);

    /**
     * 查询代码列表
     * 
     * @param hgSysCode 代码
     * @return 代码集合
     */
    public List<HgSysCode> selectHgSysCodeList(HgSysCode hgSysCode);

    /**
     * 新增代码
     * 
     * @param hgSysCode 代码
     * @return 结果
     */
    public int insertHgSysCode(HgSysCode hgSysCode);

    /**
     * 修改代码
     * 
     * @param hgSysCode 代码
     * @return 结果
     */
    public int updateHgSysCode(HgSysCode hgSysCode);

    /**
     * 删除代码
     * 
     * @param codeId 代码主键
     * @return 结果
     */
    public int deleteHgSysCodeByCodeId(Long codeId);

    /**
     * 批量删除代码
     * 
     * @param codeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgSysCodeByCodeIds(Long[] codeIds);
}
