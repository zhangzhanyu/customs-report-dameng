package com.qianjing.project.common.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgReportMapper;
import com.qianjing.project.common.domain.HgReport;
import com.qianjing.project.common.service.IHgReportService;

/**
 * 动态报表Service业务层处理
 * 
 * @author qianjing
 * @date 2022-09-06
 */
@Service
public class HgReportServiceImpl implements IHgReportService 
{
    private static final Logger log = LoggerFactory.getLogger(HgReportServiceImpl.class);

    @Autowired
    private HgReportMapper hgReportMapper;

    /**
     * 查询动态报表
     * 
     * @param reportId 动态报表主键
     * @return 动态报表
     */
    @Override
    public HgReport selectHgReportByReportId(Long reportId)
    {
        return hgReportMapper.selectHgReportByReportId(reportId);
    }

    /**
     * 查询动态报表列表
     * 
     * @param hgReport 动态报表
     * @return 动态报表
     */
    @Override
    public List<HgReport> selectHgReportList(HgReport hgReport)
    {
        return hgReportMapper.selectHgReportList(hgReport);
    }

    /**
     * 新增动态报表
     * 
     * @param hgReport 动态报表
     * @return 结果
     */
    @Override
    public int insertHgReport(HgReport hgReport)
    {
        return hgReportMapper.insertHgReport(hgReport);
    }

    /**
     * 修改动态报表
     * 
     * @param hgReport 动态报表
     * @return 结果
     */
    @Override
    public int updateHgReport(HgReport hgReport)
    {
        return hgReportMapper.updateHgReport(hgReport);
    }

    /**
     * 批量删除动态报表
     * 
     * @param reportIds 需要删除的动态报表主键
     * @return 结果
     */
    @Override
    public int deleteHgReportByReportIds(Long[] reportIds)
    {
        return hgReportMapper.deleteHgReportByReportIds(reportIds);
    }

    /**
     * 删除动态报表信息
     * 
     * @param reportId 动态报表主键
     * @return 结果
     */
    @Override
    public int deleteHgReportByReportId(Long reportId)
    {
        return hgReportMapper.deleteHgReportByReportId(reportId);
    }

    /**
     * 导入动态报表数据
     *
     * @param hgReportList hgReportList 动态报表信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgReport(List<HgReport> hgReportList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgReportList) || hgReportList.size() == 0) {
            throw new ServiceException("导入动态报表数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgReport hgReport : hgReportList){
            try {
                // 验证是否存在
                if (true) {
                    hgReport.setCreateBy(operName);
                    this.insertHgReport(hgReport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgReport.setUpdateBy(operName);
                    this.updateHgReport(hgReport);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
