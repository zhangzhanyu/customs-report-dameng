package com.qianjing.project.common.service.impl;

import com.qianjing.common.core.text.Convert;
import com.qianjing.project.common.domain.HgGenTableColumn;
import com.qianjing.project.common.mapper.HgGenTableColumnMapper;
import com.qianjing.project.common.service.IHgGenTableColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务字段 服务层实现
 * 
 * @author qianjing
 */
@Service
public class HgGenTableColumnServiceImpl implements IHgGenTableColumnService
{
	@Autowired
	private HgGenTableColumnMapper hgGenTableColumnMapper;

	/**
     * 查询业务字段列表
     * 
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
	@Override
	public List<HgGenTableColumn> selectGenTableColumnListByTableId(Long tableId)
	{
	    return hgGenTableColumnMapper.selectGenTableColumnListByTableId(tableId);
	}
	
    /**
     * 新增业务字段
     * 
     * @param hgGenTableColumn 业务字段信息
     * @return 结果
     */
	@Override
	public int insertGenTableColumn(HgGenTableColumn hgGenTableColumn)
	{
	    return hgGenTableColumnMapper.insertGenTableColumn(hgGenTableColumn);
	}
	
	/**
     * 修改业务字段
     * 
     * @param hgGenTableColumn 业务字段信息
     * @return 结果
     */
	@Override
	public int updateGenTableColumn(HgGenTableColumn hgGenTableColumn)
	{
	    return hgGenTableColumnMapper.updateGenTableColumn(hgGenTableColumn);
	}

	/**
     * 删除业务字段对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteGenTableColumnByIds(String ids)
	{
		return hgGenTableColumnMapper.deleteGenTableColumnByIds(Convert.toLongArray(ids));
	}
}