package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgSpecialRegion;

/**
 * 特殊贸易区代码Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgSpecialRegionMapper 
{
    /**
     * 查询特殊贸易区代码
     * 
     * @param specialRegionId 特殊贸易区代码主键
     * @return 特殊贸易区代码
     */
    public HgSpecialRegion selectHgSpecialRegionBySpecialRegionId(Long specialRegionId);

    /**
     * 查询特殊贸易区代码列表
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 特殊贸易区代码集合
     */
    public List<HgSpecialRegion> selectHgSpecialRegionList(HgSpecialRegion hgSpecialRegion);

    /**
     * 新增特殊贸易区代码
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 结果
     */
    public int insertHgSpecialRegion(HgSpecialRegion hgSpecialRegion);

    /**
     * 修改特殊贸易区代码
     * 
     * @param hgSpecialRegion 特殊贸易区代码
     * @return 结果
     */
    public int updateHgSpecialRegion(HgSpecialRegion hgSpecialRegion);

    /**
     * 删除特殊贸易区代码
     * 
     * @param specialRegionId 特殊贸易区代码主键
     * @return 结果
     */
    public int deleteHgSpecialRegionBySpecialRegionId(Long specialRegionId);

    /**
     * 批量删除特殊贸易区代码
     * 
     * @param specialRegionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgSpecialRegionBySpecialRegionIds(Long[] specialRegionIds);
}
