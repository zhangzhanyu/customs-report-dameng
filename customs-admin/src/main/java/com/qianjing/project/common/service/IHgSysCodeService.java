package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgSysCode;

/**
 * 代码Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgSysCodeService 
{
    /**
     * 查询代码
     * 
     * @param codeId 代码主键
     * @return 代码
     */
    public HgSysCode selectHgSysCodeByCodeId(Long codeId);

    /**
     * 查询代码列表
     * 
     * @param hgSysCode 代码
     * @return 代码集合
     */
    public List<HgSysCode> selectHgSysCodeList(HgSysCode hgSysCode);

    /**
     * 新增代码
     * 
     * @param hgSysCode 代码
     * @return 结果
     */
    public int insertHgSysCode(HgSysCode hgSysCode);

    /**
     * 修改代码
     * 
     * @param hgSysCode 代码
     * @return 结果
     */
    public int updateHgSysCode(HgSysCode hgSysCode);

    /**
     * 批量删除代码
     * 
     * @param codeIds 需要删除的代码主键集合
     * @return 结果
     */
    public int deleteHgSysCodeByCodeIds(Long[] codeIds);

    /**
     * 删除代码信息
     * 
     * @param codeId 代码主键
     * @return 结果
     */
    public int deleteHgSysCodeByCodeId(Long codeId);

    /**
     * 导入代码信息
     *
     * @param hgSysCodeList 代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgSysCode(List<HgSysCode> hgSysCodeList, Boolean isUpdateSupport, String operName);
}
