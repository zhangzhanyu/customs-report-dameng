package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgGoodsCatalog;
import com.qianjing.project.common.service.IHgGoodsCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 商品目录Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/goodsCatalog")
public class HgGoodsCatalogController extends BaseController
{
    @Autowired
    private IHgGoodsCatalogService hgGoodsCatalogService;

    /**
     * 查询商品目录列表
     */
    @PreAuthorize("@ss.hasPermi('common:goodsCatalog:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgGoodsCatalog hgGoodsCatalog)
    {
        startPage();
        List<HgGoodsCatalog> list = hgGoodsCatalogService.selectHgGoodsCatalogList(hgGoodsCatalog);
        return getDataTable(list);
    }

    /**
     * 导出商品目录列表
     */
    @PreAuthorize("@ss.hasPermi('common:goodsCatalog:export')")
    @Log(title = "商品目录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgGoodsCatalog hgGoodsCatalog)
    {
        List<HgGoodsCatalog> list = hgGoodsCatalogService.selectHgGoodsCatalogList(hgGoodsCatalog);
        ExcelUtil<HgGoodsCatalog> util = new ExcelUtil<HgGoodsCatalog>(HgGoodsCatalog.class);
        return util.exportExcel(list, "商品目录数据");
    }

    /**
     * 导入商品目录列表
     */
    @Log(title = "商品目录", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:goodsCatalog:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgGoodsCatalog> util = new ExcelUtil<HgGoodsCatalog>(HgGoodsCatalog.class);
        List<HgGoodsCatalog> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgGoodsCatalogService.importHgGoodsCatalog(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载商品目录导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgGoodsCatalog> util = new ExcelUtil<HgGoodsCatalog>(HgGoodsCatalog.class);
        util.importTemplateExcel(response, "商品目录数据");
    }

    /**
     * 获取商品目录详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:goodsCatalog:query')")
    @GetMapping(value = "/{goodsCatalogId}")
    public AjaxResult getInfo(@PathVariable("goodsCatalogId") Long goodsCatalogId)
    {
        return AjaxResult.success(hgGoodsCatalogService.selectHgGoodsCatalogByGoodsCatalogId(goodsCatalogId));
    }

    /**
     * 新增商品目录
     */
    @PreAuthorize("@ss.hasPermi('common:goodsCatalog:add')")
    @Log(title = "商品目录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgGoodsCatalog hgGoodsCatalog)
    {
        return toAjax(hgGoodsCatalogService.insertHgGoodsCatalog(hgGoodsCatalog));
    }

    /**
     * 修改商品目录
     */
    @PreAuthorize("@ss.hasPermi('common:goodsCatalog:edit')")
    @Log(title = "商品目录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgGoodsCatalog hgGoodsCatalog)
    {
        return toAjax(hgGoodsCatalogService.updateHgGoodsCatalog(hgGoodsCatalog));
    }

    /**
     * 删除商品目录
     */
    @PreAuthorize("@ss.hasPermi('common:goodsCatalog:remove')")
    @Log(title = "商品目录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{goodsCatalogIds}")
    public AjaxResult remove(@PathVariable Long[] goodsCatalogIds)
    {
        return toAjax(hgGoodsCatalogService.deleteHgGoodsCatalogByGoodsCatalogIds(goodsCatalogIds));
    }
}
