package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgHsCatalog;

/**
 * 体系类章对照Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgHsCatalogMapper 
{
    /**
     * 查询体系类章对照
     * 
     * @param hsCatalogId 体系类章对照主键
     * @return 体系类章对照
     */
    public HgHsCatalog selectHgHsCatalogByHsCatalogId(Long hsCatalogId);

    /**
     * 查询体系类章对照列表
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 体系类章对照集合
     */
    public List<HgHsCatalog> selectHgHsCatalogList(HgHsCatalog hgHsCatalog);

    /**
     * 新增体系类章对照
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 结果
     */
    public int insertHgHsCatalog(HgHsCatalog hgHsCatalog);

    /**
     * 修改体系类章对照
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 结果
     */
    public int updateHgHsCatalog(HgHsCatalog hgHsCatalog);

    /**
     * 删除体系类章对照
     * 
     * @param hsCatalogId 体系类章对照主键
     * @return 结果
     */
    public int deleteHgHsCatalogByHsCatalogId(Long hsCatalogId);

    /**
     * 批量删除体系类章对照
     * 
     * @param hsCatalogIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgHsCatalogByHsCatalogIds(Long[] hsCatalogIds);
}
