package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgTreeDictionary;

/**
 * 字典（树形）Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgTreeDictionaryService 
{
    /**
     * 查询字典（树形）
     * 
     * @param treeDicId 字典（树形）主键
     * @return 字典（树形）
     */
    public HgTreeDictionary selectHgTreeDictionaryByTreeDicId(Long treeDicId);

    /**
     * 查询字典（树形）列表
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 字典（树形）集合
     */
    public List<HgTreeDictionary> selectHgTreeDictionaryList(HgTreeDictionary hgTreeDictionary);

    /**
     * 新增字典（树形）
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 结果
     */
    public int insertHgTreeDictionary(HgTreeDictionary hgTreeDictionary);

    /**
     * 修改字典（树形）
     * 
     * @param hgTreeDictionary 字典（树形）
     * @return 结果
     */
    public int updateHgTreeDictionary(HgTreeDictionary hgTreeDictionary);

    /**
     * 批量删除字典（树形）
     * 
     * @param treeDicIds 需要删除的字典（树形）主键集合
     * @return 结果
     */
    public int deleteHgTreeDictionaryByTreeDicIds(Long[] treeDicIds);

    /**
     * 删除字典（树形）信息
     * 
     * @param treeDicId 字典（树形）主键
     * @return 结果
     */
    public int deleteHgTreeDictionaryByTreeDicId(Long treeDicId);

    /**
     * 导入字典（树形）信息
     *
     * @param hgTreeDictionaryList 字典（树形）信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgTreeDictionary(List<HgTreeDictionary> hgTreeDictionaryList, Boolean isUpdateSupport, String operName);
}
