package com.qianjing.project.common.util;

import lombok.Data;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 文件服务器连接配置类
 * @ClassName: CustomersFtpProperties
 * @CreateBy: 张占宇
 * @CreateTime: 2023年3月15日 下午17:44
 */
@Data
@Component
@ConfigurationProperties(prefix = "ftp")
public class CustomersFtpProperties extends GenericObjectPoolConfig<FTPClient> {

    /** 主机 */
    public static String hostname;
    /** 端口 */
    public static String port;
    /** 用户 */
    public static String username;
    /** 密码 */
    public static String password;
    /** 本地临时存放文件的路径 */
    public static String downPath;
    /** 上传到文件服务器的目录 */
    public static String remotePath;


    /** 是否启用被动模式 */
    public static boolean passiveMode;
    /** 连接超时时间 毫秒 */
    private int connectTimeOut = 5000;
    /** 编码格式 */
    private String controlEncoding = "utf-8";
    /** 缓冲区大小 */
    private int bufferSize = 1024;
    /** 传输数据格式 2表示binary二进制数据 */
    private int fileType = 2;
    /** 数据超时时间 */
    private int dataTimeout = 120000;
    private boolean useEPSVwithIPv4 = false;
    /** 连接池空闲检测周期 */
    private long poolEvictInterval = 30000;

    public void setHostname(String hostname) {
        CustomersFtpProperties.hostname = hostname;
    }

    public void setPort(String port) {
        CustomersFtpProperties.port = port;
    }

    public void setUsername(String username) {
        CustomersFtpProperties.username = username;
    }

    public void setPassword(String password) {
        CustomersFtpProperties.password = password;
    }

    public void setRemotePath(String remotePath) {
        CustomersFtpProperties.remotePath = remotePath;
    }

    public void setDownPath(String downPath) {
        CustomersFtpProperties.downPath = downPath;
    }

    public void setPassiveMode(boolean passiveMode) {
        CustomersFtpProperties.passiveMode = passiveMode;
    }

    public String getHostname() {
        return hostname;
    }

    public String getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public String getDownPath() {
        return downPath;
    }

    public boolean getPassiveMode() {
        return passiveMode;
    }
}


