package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgEdiRemark;

/**
 * EDI申报备注Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgEdiRemarkMapper 
{
    /**
     * 查询EDI申报备注
     * 
     * @param ediId EDI申报备注主键
     * @return EDI申报备注
     */
    public HgEdiRemark selectHgEdiRemarkByEdiId(Long ediId);

    /**
     * 查询EDI申报备注列表
     * 
     * @param hgEdiRemark EDI申报备注
     * @return EDI申报备注集合
     */
    public List<HgEdiRemark> selectHgEdiRemarkList(HgEdiRemark hgEdiRemark);

    /**
     * 新增EDI申报备注
     * 
     * @param hgEdiRemark EDI申报备注
     * @return 结果
     */
    public int insertHgEdiRemark(HgEdiRemark hgEdiRemark);

    /**
     * 修改EDI申报备注
     * 
     * @param hgEdiRemark EDI申报备注
     * @return 结果
     */
    public int updateHgEdiRemark(HgEdiRemark hgEdiRemark);

    /**
     * 删除EDI申报备注
     * 
     * @param ediId EDI申报备注主键
     * @return 结果
     */
    public int deleteHgEdiRemarkByEdiId(Long ediId);

    /**
     * 批量删除EDI申报备注
     * 
     * @param ediIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgEdiRemarkByEdiIds(Long[] ediIds);
}
