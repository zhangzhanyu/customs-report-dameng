package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsExportParam;

/**
 * 商品出口参数Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgGoodsExportParamMapper 
{
    /**
     * 查询商品出口参数
     * 
     * @param goodsExportParamId 商品出口参数主键
     * @return 商品出口参数
     */
    public HgGoodsExportParam selectHgGoodsExportParamByGoodsExportParamId(Long goodsExportParamId);

    /**
     * 查询商品出口参数列表
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 商品出口参数集合
     */
    public List<HgGoodsExportParam> selectHgGoodsExportParamList(HgGoodsExportParam hgGoodsExportParam);

    /**
     * 新增商品出口参数
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 结果
     */
    public int insertHgGoodsExportParam(HgGoodsExportParam hgGoodsExportParam);

    /**
     * 修改商品出口参数
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 结果
     */
    public int updateHgGoodsExportParam(HgGoodsExportParam hgGoodsExportParam);

    /**
     * 删除商品出口参数
     * 
     * @param goodsExportParamId 商品出口参数主键
     * @return 结果
     */
    public int deleteHgGoodsExportParamByGoodsExportParamId(Long goodsExportParamId);

    /**
     * 批量删除商品出口参数
     * 
     * @param goodsExportParamIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgGoodsExportParamByGoodsExportParamIds(Long[] goodsExportParamIds);
}
