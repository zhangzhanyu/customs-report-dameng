package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsCatalog;

/**
 * 商品目录Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgGoodsCatalogService 
{
    /**
     * 查询商品目录
     * 
     * @param goodsCatalogId 商品目录主键
     * @return 商品目录
     */
    public HgGoodsCatalog selectHgGoodsCatalogByGoodsCatalogId(Long goodsCatalogId);

    /**
     * 查询商品目录列表
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 商品目录集合
     */
    public List<HgGoodsCatalog> selectHgGoodsCatalogList(HgGoodsCatalog hgGoodsCatalog);

    /**
     * 新增商品目录
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 结果
     */
    public int insertHgGoodsCatalog(HgGoodsCatalog hgGoodsCatalog);

    /**
     * 修改商品目录
     * 
     * @param hgGoodsCatalog 商品目录
     * @return 结果
     */
    public int updateHgGoodsCatalog(HgGoodsCatalog hgGoodsCatalog);

    /**
     * 批量删除商品目录
     * 
     * @param goodsCatalogIds 需要删除的商品目录主键集合
     * @return 结果
     */
    public int deleteHgGoodsCatalogByGoodsCatalogIds(Long[] goodsCatalogIds);

    /**
     * 删除商品目录信息
     * 
     * @param goodsCatalogId 商品目录主键
     * @return 结果
     */
    public int deleteHgGoodsCatalogByGoodsCatalogId(Long goodsCatalogId);

    /**
     * 导入商品目录信息
     *
     * @param hgGoodsCatalogList 商品目录信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgGoodsCatalog(List<HgGoodsCatalog> hgGoodsCatalogList, Boolean isUpdateSupport, String operName);
}
