package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsExportParam;

/**
 * 商品出口参数Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgGoodsExportParamService 
{
    /**
     * 查询商品出口参数
     * 
     * @param goodsExportParamId 商品出口参数主键
     * @return 商品出口参数
     */
    public HgGoodsExportParam selectHgGoodsExportParamByGoodsExportParamId(Long goodsExportParamId);

    /**
     * 查询商品出口参数列表
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 商品出口参数集合
     */
    public List<HgGoodsExportParam> selectHgGoodsExportParamList(HgGoodsExportParam hgGoodsExportParam);

    /**
     * 新增商品出口参数
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 结果
     */
    public int insertHgGoodsExportParam(HgGoodsExportParam hgGoodsExportParam);

    /**
     * 修改商品出口参数
     * 
     * @param hgGoodsExportParam 商品出口参数
     * @return 结果
     */
    public int updateHgGoodsExportParam(HgGoodsExportParam hgGoodsExportParam);

    /**
     * 批量删除商品出口参数
     * 
     * @param goodsExportParamIds 需要删除的商品出口参数主键集合
     * @return 结果
     */
    public int deleteHgGoodsExportParamByGoodsExportParamIds(Long[] goodsExportParamIds);

    /**
     * 删除商品出口参数信息
     * 
     * @param goodsExportParamId 商品出口参数主键
     * @return 结果
     */
    public int deleteHgGoodsExportParamByGoodsExportParamId(Long goodsExportParamId);

    /**
     * 导入商品出口参数信息
     *
     * @param hgGoodsExportParamList 商品出口参数信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgGoodsExportParam(List<HgGoodsExportParam> hgGoodsExportParamList, Boolean isUpdateSupport, String operName);
}
