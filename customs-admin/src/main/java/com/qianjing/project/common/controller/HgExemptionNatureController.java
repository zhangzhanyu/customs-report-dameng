package com.qianjing.project.common.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.common.domain.HgExemptionNature;
import com.qianjing.project.common.service.IHgExemptionNatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 征免性质代码Controller
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@RestController
@RequestMapping("/common/nature")
public class HgExemptionNatureController extends BaseController
{
    @Autowired
    private IHgExemptionNatureService hgExemptionNatureService;

    /**
     * 查询征免性质代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:nature:list')")
    @GetMapping("/list")
    public TableDataInfo list(HgExemptionNature hgExemptionNature)
    {
        startPage();
        List<HgExemptionNature> list = hgExemptionNatureService.selectHgExemptionNatureList(hgExemptionNature);
        return getDataTable(list);
    }

    /**
     * 导出征免性质代码列表
     */
    @PreAuthorize("@ss.hasPermi('common:nature:export')")
    @Log(title = "征免性质代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HgExemptionNature hgExemptionNature)
    {
        List<HgExemptionNature> list = hgExemptionNatureService.selectHgExemptionNatureList(hgExemptionNature);
        ExcelUtil<HgExemptionNature> util = new ExcelUtil<HgExemptionNature>(HgExemptionNature.class);
        return util.exportExcel(list, "征免性质代码数据");
    }

    /**
     * 导入征免性质代码列表
     */
    @Log(title = "征免性质代码", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('common:nature:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<HgExemptionNature> util = new ExcelUtil<HgExemptionNature>(HgExemptionNature.class);
        List<HgExemptionNature> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = hgExemptionNatureService.importHgExemptionNature(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载征免性质代码导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<HgExemptionNature> util = new ExcelUtil<HgExemptionNature>(HgExemptionNature.class);
        util.importTemplateExcel(response, "征免性质代码数据");
    }

    /**
     * 获取征免性质代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:nature:query')")
    @GetMapping(value = "/{exemptionNatureId}")
    public AjaxResult getInfo(@PathVariable("exemptionNatureId") Long exemptionNatureId)
    {
        return AjaxResult.success(hgExemptionNatureService.selectHgExemptionNatureByExemptionNatureId(exemptionNatureId));
    }

    /**
     * 新增征免性质代码
     */
    @PreAuthorize("@ss.hasPermi('common:nature:add')")
    @Log(title = "征免性质代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HgExemptionNature hgExemptionNature)
    {
        return toAjax(hgExemptionNatureService.insertHgExemptionNature(hgExemptionNature));
    }

    /**
     * 修改征免性质代码
     */
    @PreAuthorize("@ss.hasPermi('common:nature:edit')")
    @Log(title = "征免性质代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HgExemptionNature hgExemptionNature)
    {
        return toAjax(hgExemptionNatureService.updateHgExemptionNature(hgExemptionNature));
    }

    /**
     * 删除征免性质代码
     */
    @PreAuthorize("@ss.hasPermi('common:nature:remove')")
    @Log(title = "征免性质代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{exemptionNatureIds}")
    public AjaxResult remove(@PathVariable Long[] exemptionNatureIds)
    {
        return toAjax(hgExemptionNatureService.deleteHgExemptionNatureByExemptionNatureIds(exemptionNatureIds));
    }
}
