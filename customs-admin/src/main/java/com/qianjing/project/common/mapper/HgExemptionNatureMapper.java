package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgExemptionNature;

/**
 * 征免性质代码Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgExemptionNatureMapper 
{
    /**
     * 查询征免性质代码
     * 
     * @param exemptionNatureId 征免性质代码主键
     * @return 征免性质代码
     */
    public HgExemptionNature selectHgExemptionNatureByExemptionNatureId(Long exemptionNatureId);

    /**
     * 查询征免性质代码列表
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 征免性质代码集合
     */
    public List<HgExemptionNature> selectHgExemptionNatureList(HgExemptionNature hgExemptionNature);

    /**
     * 新增征免性质代码
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 结果
     */
    public int insertHgExemptionNature(HgExemptionNature hgExemptionNature);

    /**
     * 修改征免性质代码
     * 
     * @param hgExemptionNature 征免性质代码
     * @return 结果
     */
    public int updateHgExemptionNature(HgExemptionNature hgExemptionNature);

    /**
     * 删除征免性质代码
     * 
     * @param exemptionNatureId 征免性质代码主键
     * @return 结果
     */
    public int deleteHgExemptionNatureByExemptionNatureId(Long exemptionNatureId);

    /**
     * 批量删除征免性质代码
     * 
     * @param exemptionNatureIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgExemptionNatureByExemptionNatureIds(Long[] exemptionNatureIds);
}
