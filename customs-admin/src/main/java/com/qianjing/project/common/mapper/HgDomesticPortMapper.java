package com.qianjing.project.common.mapper;

import java.util.List;
import com.qianjing.project.common.domain.HgDomesticPort;

/**
 * 国内口岸代码Mapper接口
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public interface HgDomesticPortMapper 
{
    /**
     * 查询国内口岸代码
     * 
     * @param domesticPortId 国内口岸代码主键
     * @return 国内口岸代码
     */
    public HgDomesticPort selectHgDomesticPortByDomesticPortId(Long domesticPortId);

    /**
     * 查询国内口岸代码列表
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 国内口岸代码集合
     */
    public List<HgDomesticPort> selectHgDomesticPortList(HgDomesticPort hgDomesticPort);

    /**
     * 新增国内口岸代码
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 结果
     */
    public int insertHgDomesticPort(HgDomesticPort hgDomesticPort);

    /**
     * 修改国内口岸代码
     * 
     * @param hgDomesticPort 国内口岸代码
     * @return 结果
     */
    public int updateHgDomesticPort(HgDomesticPort hgDomesticPort);

    /**
     * 删除国内口岸代码
     * 
     * @param domesticPortId 国内口岸代码主键
     * @return 结果
     */
    public int deleteHgDomesticPortByDomesticPortId(Long domesticPortId);

    /**
     * 批量删除国内口岸代码
     * 
     * @param domesticPortIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHgDomesticPortByDomesticPortIds(Long[] domesticPortIds);
}
