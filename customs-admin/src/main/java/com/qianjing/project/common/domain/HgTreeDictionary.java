package com.qianjing.project.common.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 字典（树形）对象 hg_tree_dictionary
 * 
 * @author qianjing
 * @date 2022-04-23
 */
public class HgTreeDictionary extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 字典编号 */
    private Long treeDicId;

    /** 字典名称 */
    @Excel(name = "字典名称")
    private String treeDicName;

    /** 字典代码 */
    @Excel(name = "字典代码")
    private String treeDicCode;

    /** 字典层级 */
    @Excel(name = "字典层级")
    private Long treeDicLayer;

    /** 字典排序 */
    @Excel(name = "字典排序")
    private Long treeDicSort;

    /** 连接 */
    @Excel(name = "连接")
    private String treeDicLink;

    /** 年份 */
    @Excel(name = "年份")
    private String treeDicYear;

    /** 删除标志(0未删1已删) */
    private String delFlag;

    public void setTreeDicId(Long treeDicId) 
    {
        this.treeDicId = treeDicId;
    }

    public Long getTreeDicId() 
    {
        return treeDicId;
    }
    public void setTreeDicName(String treeDicName) 
    {
        this.treeDicName = treeDicName;
    }

    public String getTreeDicName() 
    {
        return treeDicName;
    }
    public void setTreeDicCode(String treeDicCode) 
    {
        this.treeDicCode = treeDicCode;
    }

    public String getTreeDicCode() 
    {
        return treeDicCode;
    }
    public void setTreeDicLayer(Long treeDicLayer) 
    {
        this.treeDicLayer = treeDicLayer;
    }

    public Long getTreeDicLayer() 
    {
        return treeDicLayer;
    }
    public void setTreeDicSort(Long treeDicSort) 
    {
        this.treeDicSort = treeDicSort;
    }

    public Long getTreeDicSort() 
    {
        return treeDicSort;
    }
    public void setTreeDicLink(String treeDicLink) 
    {
        this.treeDicLink = treeDicLink;
    }

    public String getTreeDicLink() 
    {
        return treeDicLink;
    }
    public void setTreeDicYear(String treeDicYear) 
    {
        this.treeDicYear = treeDicYear;
    }

    public String getTreeDicYear() 
    {
        return treeDicYear;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("treeDicId", getTreeDicId())
            .append("treeDicName", getTreeDicName())
            .append("treeDicCode", getTreeDicCode())
            .append("treeDicLayer", getTreeDicLayer())
            .append("treeDicSort", getTreeDicSort())
            .append("treeDicLink", getTreeDicLink())
            .append("treeDicYear", getTreeDicYear())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
