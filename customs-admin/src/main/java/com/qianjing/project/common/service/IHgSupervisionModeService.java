package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgSupervisionMode;

/**
 * 监管方式代码Service接口
 * 
 * @author qianjing
 * @date 2022-04-24
 */
public interface IHgSupervisionModeService 
{
    /**
     * 查询监管方式代码
     * 
     * @param supervisionModeId 监管方式代码主键
     * @return 监管方式代码
     */
    public HgSupervisionMode selectHgSupervisionModeBySupervisionModeId(Long supervisionModeId);

    /**
     * 查询监管方式代码列表
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 监管方式代码集合
     */
    public List<HgSupervisionMode> selectHgSupervisionModeList(HgSupervisionMode hgSupervisionMode);

    /**
     * 新增监管方式代码
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 结果
     */
    public int insertHgSupervisionMode(HgSupervisionMode hgSupervisionMode);

    /**
     * 修改监管方式代码
     * 
     * @param hgSupervisionMode 监管方式代码
     * @return 结果
     */
    public int updateHgSupervisionMode(HgSupervisionMode hgSupervisionMode);

    /**
     * 批量删除监管方式代码
     * 
     * @param supervisionModeIds 需要删除的监管方式代码主键集合
     * @return 结果
     */
    public int deleteHgSupervisionModeBySupervisionModeIds(Long[] supervisionModeIds);

    /**
     * 删除监管方式代码信息
     * 
     * @param supervisionModeId 监管方式代码主键
     * @return 结果
     */
    public int deleteHgSupervisionModeBySupervisionModeId(Long supervisionModeId);

    /**
     * 导入监管方式代码信息
     *
     * @param hgSupervisionModeList 监管方式代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgSupervisionMode(List<HgSupervisionMode> hgSupervisionModeList, Boolean isUpdateSupport, String operName);
}
