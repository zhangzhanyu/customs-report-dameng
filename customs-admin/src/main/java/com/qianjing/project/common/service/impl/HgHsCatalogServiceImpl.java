package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgHsCatalogMapper;
import com.qianjing.project.common.domain.HgHsCatalog;
import com.qianjing.project.common.service.IHgHsCatalogService;

/**
 * 体系类章对照Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgHsCatalogServiceImpl implements IHgHsCatalogService 
{
    private static final Logger log = LoggerFactory.getLogger(HgHsCatalogServiceImpl.class);

    @Autowired
    private HgHsCatalogMapper hgHsCatalogMapper;

    /**
     * 查询体系类章对照
     * 
     * @param hsCatalogId 体系类章对照主键
     * @return 体系类章对照
     */
    @Override
    public HgHsCatalog selectHgHsCatalogByHsCatalogId(Long hsCatalogId)
    {
        return hgHsCatalogMapper.selectHgHsCatalogByHsCatalogId(hsCatalogId);
    }

    /**
     * 查询体系类章对照列表
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 体系类章对照
     */
    @Override
    public List<HgHsCatalog> selectHgHsCatalogList(HgHsCatalog hgHsCatalog)
    {
        return hgHsCatalogMapper.selectHgHsCatalogList(hgHsCatalog);
    }

    /**
     * 新增体系类章对照
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 结果
     */
    @Override
    public int insertHgHsCatalog(HgHsCatalog hgHsCatalog)
    {
        hgHsCatalog.setCreateTime(DateUtils.getNowDate());
        return hgHsCatalogMapper.insertHgHsCatalog(hgHsCatalog);
    }

    /**
     * 修改体系类章对照
     * 
     * @param hgHsCatalog 体系类章对照
     * @return 结果
     */
    @Override
    public int updateHgHsCatalog(HgHsCatalog hgHsCatalog)
    {
        hgHsCatalog.setUpdateTime(DateUtils.getNowDate());
        return hgHsCatalogMapper.updateHgHsCatalog(hgHsCatalog);
    }

    /**
     * 批量删除体系类章对照
     * 
     * @param hsCatalogIds 需要删除的体系类章对照主键
     * @return 结果
     */
    @Override
    public int deleteHgHsCatalogByHsCatalogIds(Long[] hsCatalogIds)
    {
        return hgHsCatalogMapper.deleteHgHsCatalogByHsCatalogIds(hsCatalogIds);
    }

    /**
     * 删除体系类章对照信息
     * 
     * @param hsCatalogId 体系类章对照主键
     * @return 结果
     */
    @Override
    public int deleteHgHsCatalogByHsCatalogId(Long hsCatalogId)
    {
        return hgHsCatalogMapper.deleteHgHsCatalogByHsCatalogId(hsCatalogId);
    }

    /**
     * 导入体系类章对照数据
     *
     * @param hgHsCatalogList hgHsCatalogList 体系类章对照信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgHsCatalog(List<HgHsCatalog> hgHsCatalogList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgHsCatalogList) || hgHsCatalogList.size() == 0) {
            throw new ServiceException("导入体系类章对照数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgHsCatalog hgHsCatalog : hgHsCatalogList){
            try {
                // 验证是否存在
                if (true) {
                    hgHsCatalog.setCreateBy(operName);
                    this.insertHgHsCatalog(hgHsCatalog);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgHsCatalog.setUpdateBy(operName);
                    this.updateHgHsCatalog(hgHsCatalog);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
