package com.qianjing.project.common.service;

import java.util.List;
import com.qianjing.project.common.domain.HgGoodsClassification;
import com.qianjing.project.common.domain.HgGoodsClassificationVo;

/**
 * 商品分类代码Service接口
 * 
 * @author zhangzy
 * @date 2022-08-02
 */
public interface IHgGoodsClassificationService 
{
    /**
     * 查询商品分类代码
     * 
     * @param goodsClassificationId 商品分类代码主键
     * @return 商品分类代码
     */
    public HgGoodsClassification selectHgGoodsClassificationByGoodsClassificationId(Long goodsClassificationId);

    /**
     * 查询商品分类代码列表
     * 
     * @param hgGoodsClassification 商品分类代码
     * @return 商品分类代码集合
     */
    public List<HgGoodsClassification> selectHgGoodsClassificationList(HgGoodsClassification hgGoodsClassification);

    /**
     * 新增商品分类代码
     * 
     * @param hgGoodsClassification 商品分类代码
     * @return 结果
     */
    public int insertHgGoodsClassification(HgGoodsClassification hgGoodsClassification);

    /**
     * 修改商品分类代码
     * 
     * @param hgGoodsClassification 商品分类代码
     * @return 结果
     */
    public int updateHgGoodsClassification(HgGoodsClassification hgGoodsClassification);

    /**
     * 批量删除商品分类代码
     * 
     * @param goodsClassificationIds 需要删除的商品分类代码主键集合
     * @return 结果
     */
    public int deleteHgGoodsClassificationByGoodsClassificationIds(Long[] goodsClassificationIds);

    /**
     * 删除商品分类代码信息
     * 
     * @param goodsClassificationId 商品分类代码主键
     * @return 结果
     */
    public int deleteHgGoodsClassificationByGoodsClassificationId(Long goodsClassificationId);

    /**
     * 导入商品分类代码信息
     *
     * @param hgGoodsClassificationList 商品分类代码信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importHgGoodsClassification(List<HgGoodsClassification> hgGoodsClassificationList, Boolean isUpdateSupport, String operName);

    public HgGoodsClassificationVo selectHgGoodsClassificationVo(String classificationName);
}
