package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgGoodsImportParamMapper;
import com.qianjing.project.common.domain.HgGoodsImportParam;
import com.qianjing.project.common.service.IHgGoodsImportParamService;

/**
 * 商品进口参数Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgGoodsImportParamServiceImpl implements IHgGoodsImportParamService 
{
    private static final Logger log = LoggerFactory.getLogger(HgGoodsImportParamServiceImpl.class);

    @Autowired
    private HgGoodsImportParamMapper hgGoodsImportParamMapper;

    /**
     * 查询商品进口参数
     * 
     * @param goodsImportParamId 商品进口参数主键
     * @return 商品进口参数
     */
    @Override
    public HgGoodsImportParam selectHgGoodsImportParamByGoodsImportParamId(Long goodsImportParamId)
    {
        return hgGoodsImportParamMapper.selectHgGoodsImportParamByGoodsImportParamId(goodsImportParamId);
    }

    /**
     * 查询商品进口参数列表
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 商品进口参数
     */
    @Override
    public List<HgGoodsImportParam> selectHgGoodsImportParamList(HgGoodsImportParam hgGoodsImportParam)
    {
        return hgGoodsImportParamMapper.selectHgGoodsImportParamList(hgGoodsImportParam);
    }

    /**
     * 新增商品进口参数
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 结果
     */
    @Override
    public int insertHgGoodsImportParam(HgGoodsImportParam hgGoodsImportParam)
    {
        hgGoodsImportParam.setCreateTime(DateUtils.getNowDate());
        return hgGoodsImportParamMapper.insertHgGoodsImportParam(hgGoodsImportParam);
    }

    /**
     * 修改商品进口参数
     * 
     * @param hgGoodsImportParam 商品进口参数
     * @return 结果
     */
    @Override
    public int updateHgGoodsImportParam(HgGoodsImportParam hgGoodsImportParam)
    {
        hgGoodsImportParam.setUpdateTime(DateUtils.getNowDate());
        return hgGoodsImportParamMapper.updateHgGoodsImportParam(hgGoodsImportParam);
    }

    /**
     * 批量删除商品进口参数
     * 
     * @param goodsImportParamIds 需要删除的商品进口参数主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsImportParamByGoodsImportParamIds(Long[] goodsImportParamIds)
    {
        return hgGoodsImportParamMapper.deleteHgGoodsImportParamByGoodsImportParamIds(goodsImportParamIds);
    }

    /**
     * 删除商品进口参数信息
     * 
     * @param goodsImportParamId 商品进口参数主键
     * @return 结果
     */
    @Override
    public int deleteHgGoodsImportParamByGoodsImportParamId(Long goodsImportParamId)
    {
        return hgGoodsImportParamMapper.deleteHgGoodsImportParamByGoodsImportParamId(goodsImportParamId);
    }

    /**
     * 导入商品进口参数数据
     *
     * @param hgGoodsImportParamList hgGoodsImportParamList 商品进口参数信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgGoodsImportParam(List<HgGoodsImportParam> hgGoodsImportParamList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgGoodsImportParamList) || hgGoodsImportParamList.size() == 0) {
            throw new ServiceException("导入商品进口参数数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgGoodsImportParam hgGoodsImportParam : hgGoodsImportParamList){
            try {
                // 验证是否存在
                if (true) {
                    hgGoodsImportParam.setCreateBy(operName);
                    this.insertHgGoodsImportParam(hgGoodsImportParam);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgGoodsImportParam.setUpdateBy(operName);
                    this.updateHgGoodsImportParam(hgGoodsImportParam);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
