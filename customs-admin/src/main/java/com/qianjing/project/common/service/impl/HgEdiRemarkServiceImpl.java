package com.qianjing.project.common.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.common.mapper.HgEdiRemarkMapper;
import com.qianjing.project.common.domain.HgEdiRemark;
import com.qianjing.project.common.service.IHgEdiRemarkService;

/**
 * EDI申报备注Service业务层处理
 * 
 * @author qianjing
 * @date 2022-04-24
 */
@Service
public class HgEdiRemarkServiceImpl implements IHgEdiRemarkService 
{
    private static final Logger log = LoggerFactory.getLogger(HgEdiRemarkServiceImpl.class);

    @Autowired
    private HgEdiRemarkMapper hgEdiRemarkMapper;

    /**
     * 查询EDI申报备注
     * 
     * @param ediId EDI申报备注主键
     * @return EDI申报备注
     */
    @Override
    public HgEdiRemark selectHgEdiRemarkByEdiId(Long ediId)
    {
        return hgEdiRemarkMapper.selectHgEdiRemarkByEdiId(ediId);
    }

    /**
     * 查询EDI申报备注列表
     * 
     * @param hgEdiRemark EDI申报备注
     * @return EDI申报备注
     */
    @Override
    public List<HgEdiRemark> selectHgEdiRemarkList(HgEdiRemark hgEdiRemark)
    {
        return hgEdiRemarkMapper.selectHgEdiRemarkList(hgEdiRemark);
    }

    /**
     * 新增EDI申报备注
     * 
     * @param hgEdiRemark EDI申报备注
     * @return 结果
     */
    @Override
    public int insertHgEdiRemark(HgEdiRemark hgEdiRemark)
    {
        hgEdiRemark.setCreateTime(DateUtils.getNowDate());
        return hgEdiRemarkMapper.insertHgEdiRemark(hgEdiRemark);
    }

    /**
     * 修改EDI申报备注
     * 
     * @param hgEdiRemark EDI申报备注
     * @return 结果
     */
    @Override
    public int updateHgEdiRemark(HgEdiRemark hgEdiRemark)
    {
        hgEdiRemark.setUpdateTime(DateUtils.getNowDate());
        return hgEdiRemarkMapper.updateHgEdiRemark(hgEdiRemark);
    }

    /**
     * 批量删除EDI申报备注
     * 
     * @param ediIds 需要删除的EDI申报备注主键
     * @return 结果
     */
    @Override
    public int deleteHgEdiRemarkByEdiIds(Long[] ediIds)
    {
        return hgEdiRemarkMapper.deleteHgEdiRemarkByEdiIds(ediIds);
    }

    /**
     * 删除EDI申报备注信息
     * 
     * @param ediId EDI申报备注主键
     * @return 结果
     */
    @Override
    public int deleteHgEdiRemarkByEdiId(Long ediId)
    {
        return hgEdiRemarkMapper.deleteHgEdiRemarkByEdiId(ediId);
    }

    /**
     * 导入EDI申报备注数据
     *
     * @param hgEdiRemarkList hgEdiRemarkList EDI申报备注信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importHgEdiRemark(List<HgEdiRemark> hgEdiRemarkList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(hgEdiRemarkList) || hgEdiRemarkList.size() == 0) {
            throw new ServiceException("导入EDI申报备注数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (HgEdiRemark hgEdiRemark : hgEdiRemarkList){
            try {
                // 验证是否存在
                if (true) {
                    hgEdiRemark.setCreateBy(operName);
                    this.insertHgEdiRemark(hgEdiRemark);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    hgEdiRemark.setUpdateBy(operName);
                    this.updateHgEdiRemark(hgEdiRemark);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
