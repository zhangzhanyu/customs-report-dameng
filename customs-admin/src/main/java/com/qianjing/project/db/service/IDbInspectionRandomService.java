package com.qianjing.project.db.service;

import java.util.List;
import com.qianjing.project.db.domain.DbInspectionRandom;

/**
 * 2021年法检目录外商品抽查检验结果汇总Service接口
 * 
 * @author zhangzy
 * @date 2022-08-03
 */
public interface IDbInspectionRandomService 
{
    /**
     * 查询2021年法检目录外商品抽查检验结果汇总
     * 
     * @param inspectionRandomId 2021年法检目录外商品抽查检验结果汇总主键
     * @return 2021年法检目录外商品抽查检验结果汇总
     */
    public DbInspectionRandom selectDbInspectionRandomByInspectionRandomId(Long inspectionRandomId);

    /**
     * 查询2021年法检目录外商品抽查检验结果汇总列表
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 2021年法检目录外商品抽查检验结果汇总集合
     */
    public List<DbInspectionRandom> selectDbInspectionRandomList(DbInspectionRandom dbInspectionRandom);

    /**
     * 新增2021年法检目录外商品抽查检验结果汇总
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 结果
     */
    public int insertDbInspectionRandom(DbInspectionRandom dbInspectionRandom);

    /**
     * 修改2021年法检目录外商品抽查检验结果汇总
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 结果
     */
    public int updateDbInspectionRandom(DbInspectionRandom dbInspectionRandom);

    /**
     * 批量删除2021年法检目录外商品抽查检验结果汇总
     * 
     * @param inspectionRandomIds 需要删除的2021年法检目录外商品抽查检验结果汇总主键集合
     * @return 结果
     */
    public int deleteDbInspectionRandomByInspectionRandomIds(Long[] inspectionRandomIds);

    /**
     * 删除2021年法检目录外商品抽查检验结果汇总信息
     * 
     * @param inspectionRandomId 2021年法检目录外商品抽查检验结果汇总主键
     * @return 结果
     */
    public int deleteDbInspectionRandomByInspectionRandomId(Long inspectionRandomId);

    /**
     * 导入2021年法检目录外商品抽查检验结果汇总信息
     *
     * @param dbInspectionRandomList 2021年法检目录外商品抽查检验结果汇总信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importDbInspectionRandom(List<DbInspectionRandom> dbInspectionRandomList, Boolean isUpdateSupport, String operName);
}
