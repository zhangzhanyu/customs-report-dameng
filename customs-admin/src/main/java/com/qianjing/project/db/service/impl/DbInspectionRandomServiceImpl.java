package com.qianjing.project.db.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.db.mapper.DbInspectionRandomMapper;
import com.qianjing.project.db.domain.DbInspectionRandom;
import com.qianjing.project.db.service.IDbInspectionRandomService;

/**
 * 2021年法检目录外商品抽查检验结果汇总Service业务层处理
 * 
 * @author zhangzy
 * @date 2022-08-03
 */
@Service
public class DbInspectionRandomServiceImpl implements IDbInspectionRandomService 
{
    private static final Logger log = LoggerFactory.getLogger(DbInspectionRandomServiceImpl.class);

    @Autowired
    private DbInspectionRandomMapper dbInspectionRandomMapper;

    /**
     * 查询2021年法检目录外商品抽查检验结果汇总
     * 
     * @param inspectionRandomId 2021年法检目录外商品抽查检验结果汇总主键
     * @return 2021年法检目录外商品抽查检验结果汇总
     */
    @Override
    public DbInspectionRandom selectDbInspectionRandomByInspectionRandomId(Long inspectionRandomId)
    {
        return dbInspectionRandomMapper.selectDbInspectionRandomByInspectionRandomId(inspectionRandomId);
    }

    /**
     * 查询2021年法检目录外商品抽查检验结果汇总列表
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 2021年法检目录外商品抽查检验结果汇总
     */
    @Override
    public List<DbInspectionRandom> selectDbInspectionRandomList(DbInspectionRandom dbInspectionRandom)
    {
        return dbInspectionRandomMapper.selectDbInspectionRandomList(dbInspectionRandom);
    }

    /**
     * 新增2021年法检目录外商品抽查检验结果汇总
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 结果
     */
    @Override
    public int insertDbInspectionRandom(DbInspectionRandom dbInspectionRandom)
    {
        return dbInspectionRandomMapper.insertDbInspectionRandom(dbInspectionRandom);
    }

    /**
     * 修改2021年法检目录外商品抽查检验结果汇总
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 结果
     */
    @Override
    public int updateDbInspectionRandom(DbInspectionRandom dbInspectionRandom)
    {
        return dbInspectionRandomMapper.updateDbInspectionRandom(dbInspectionRandom);
    }

    /**
     * 批量删除2021年法检目录外商品抽查检验结果汇总
     * 
     * @param inspectionRandomIds 需要删除的2021年法检目录外商品抽查检验结果汇总主键
     * @return 结果
     */
    @Override
    public int deleteDbInspectionRandomByInspectionRandomIds(Long[] inspectionRandomIds)
    {
        return dbInspectionRandomMapper.deleteDbInspectionRandomByInspectionRandomIds(inspectionRandomIds);
    }

    /**
     * 删除2021年法检目录外商品抽查检验结果汇总信息
     * 
     * @param inspectionRandomId 2021年法检目录外商品抽查检验结果汇总主键
     * @return 结果
     */
    @Override
    public int deleteDbInspectionRandomByInspectionRandomId(Long inspectionRandomId)
    {
        return dbInspectionRandomMapper.deleteDbInspectionRandomByInspectionRandomId(inspectionRandomId);
    }

    /**
     * 导入2021年法检目录外商品抽查检验结果汇总数据
     *
     * @param dbInspectionRandomList dbInspectionRandomList 2021年法检目录外商品抽查检验结果汇总信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importDbInspectionRandom(List<DbInspectionRandom> dbInspectionRandomList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(dbInspectionRandomList) || dbInspectionRandomList.size() == 0) {
            throw new ServiceException("导入2021年法检目录外商品抽查检验结果汇总数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (DbInspectionRandom dbInspectionRandom : dbInspectionRandomList){
            try {
                // 验证是否存在
                if (true) {
                    dbInspectionRandom.setCreateBy(operName);
                    this.insertDbInspectionRandom(dbInspectionRandom);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    dbInspectionRandom.setUpdateBy(operName);
                    this.updateDbInspectionRandom(dbInspectionRandom);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
