package com.qianjing.project.db.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 2021年法检目录外商品抽查检验结果汇总对象 db_inspection_random
 * 
 * @author zhangzy
 * @date 2022-08-03
 */
public class DbInspectionRandom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long inspectionRandomId;

    /** 进出口 */
    @Excel(name = "进出口")
    private String iEFlag;

    /** 指令命中报关单号 */
    @Excel(name = "指令命中报关单号")
    private String entryId;

    /** 货物项号 */
    @Excel(name = "货物项号")
    private String gNo;

    /** 所涉品牌/型号 */
    @Excel(name = "所涉品牌/型号")
    private String brand;

    /** 申报品名 */
    @Excel(name = "申报品名")
    private String declareName;

    /** HS编码 */
    @Excel(name = "HS编码")
    private String codeTs;

    /** 原产国/地区 */
    @Excel(name = "原产国/地区")
    private String originCountry;

    /** 所属商品类别(抽查计划中的商品类别) */
    @Excel(name = "所属商品类别(抽查计划中的商品类别)")
    private String gCategory;

    /** 检验结果（合格/不合格） */
    @Excel(name = "检验结果", readConverterExp = "合=格/不合格")
    private String checkResult;

    /** 不合格项目 */
    @Excel(name = "不合格项目")
    private String uItem;

    /** 不合格数量 */
    @Excel(name = "不合格数量")
    private String uQuantity;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 统计人民币价 */
    @Excel(name = "统计人民币价")
    private String rmbPrice;

    /** 统计美元价 */
    @Excel(name = "统计美元价")
    private String usdPrice;

    /** 处理 */
    @Excel(name = "处理")
    private String disposal;

    /** 检验依据 */
    @Excel(name = "检验依据")
    private String standard;

    /** 已通过系统上报（是/否） */
    @Excel(name = "已通过系统上报", readConverterExp = "是=/否")
    private String passFlag;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setInspectionRandomId(Long inspectionRandomId) 
    {
        this.inspectionRandomId = inspectionRandomId;
    }

    public Long getInspectionRandomId() 
    {
        return inspectionRandomId;
    }
    public void setiEFlag(String iEFlag) 
    {
        this.iEFlag = iEFlag;
    }

    public String getiEFlag() 
    {
        return iEFlag;
    }
    public void setEntryId(String entryId) 
    {
        this.entryId = entryId;
    }

    public String getEntryId() 
    {
        return entryId;
    }
    public void setgNo(String gNo) 
    {
        this.gNo = gNo;
    }

    public String getgNo() 
    {
        return gNo;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setDeclareName(String declareName) 
    {
        this.declareName = declareName;
    }

    public String getDeclareName() 
    {
        return declareName;
    }
    public void setCodeTs(String codeTs) 
    {
        this.codeTs = codeTs;
    }

    public String getCodeTs() 
    {
        return codeTs;
    }
    public void setOriginCountry(String originCountry) 
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry() 
    {
        return originCountry;
    }
    public void setgCategory(String gCategory) 
    {
        this.gCategory = gCategory;
    }

    public String getgCategory() 
    {
        return gCategory;
    }
    public void setCheckResult(String checkResult) 
    {
        this.checkResult = checkResult;
    }

    public String getCheckResult() 
    {
        return checkResult;
    }
    public void setuItem(String uItem) 
    {
        this.uItem = uItem;
    }

    public String getuItem() 
    {
        return uItem;
    }
    public void setuQuantity(String uQuantity) 
    {
        this.uQuantity = uQuantity;
    }

    public String getuQuantity() 
    {
        return uQuantity;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setRmbPrice(String rmbPrice) 
    {
        this.rmbPrice = rmbPrice;
    }

    public String getRmbPrice() 
    {
        return rmbPrice;
    }
    public void setUsdPrice(String usdPrice) 
    {
        this.usdPrice = usdPrice;
    }

    public String getUsdPrice() 
    {
        return usdPrice;
    }
    public void setDisposal(String disposal) 
    {
        this.disposal = disposal;
    }

    public String getDisposal() 
    {
        return disposal;
    }
    public void setStandard(String standard) 
    {
        this.standard = standard;
    }

    public String getStandard() 
    {
        return standard;
    }
    public void setPassFlag(String passFlag) 
    {
        this.passFlag = passFlag;
    }

    public String getPassFlag() 
    {
        return passFlag;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("inspectionRandomId", getInspectionRandomId())
            .append("iEFlag", getiEFlag())
            .append("entryId", getEntryId())
            .append("gNo", getgNo())
            .append("brand", getBrand())
            .append("declareName", getDeclareName())
            .append("codeTs", getCodeTs())
            .append("originCountry", getOriginCountry())
            .append("gCategory", getgCategory())
            .append("checkResult", getCheckResult())
            .append("uItem", getuItem())
            .append("uQuantity", getuQuantity())
            .append("unit", getUnit())
            .append("rmbPrice", getRmbPrice())
            .append("usdPrice", getUsdPrice())
            .append("disposal", getDisposal())
            .append("standard", getStandard())
            .append("passFlag", getPassFlag())
            .append("remarks", getRemarks())
            .toString();
    }
}
