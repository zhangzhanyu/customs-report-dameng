package com.qianjing.project.db.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.common.constant.Constants;
import com.qianjing.common.constant.UserConstants;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.project.db.domain.DbUnqiedImport;
import com.qianjing.project.system.domain.SysDept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.db.mapper.DbUnqiedMapper;
import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.db.service.IDbUnqiedService;

/**
 * 检验不合格数据Service业务层处理
 *
 * @author qianjing
 * @date 2022-06-09
 */
@Service
public class DbUnqiedServiceImpl implements IDbUnqiedService
{
    private static final Logger log = LoggerFactory.getLogger(DbUnqiedServiceImpl.class);

    @Autowired
    private DbUnqiedMapper dbUnqiedMapper;

    /**
     * 查询检验不合格数据
     *
     * @param unqiedId 检验不合格数据主键
     * @return 检验不合格数据
     */
    @Override
    public DbUnqied selectDbUnqiedByUnqiedId(Long unqiedId)
    {
        return dbUnqiedMapper.selectDbUnqiedByUnqiedId(unqiedId);
    }

    /**
     * 查询检验不合格数据列表
     *
     * @param dbUnqied 检验不合格数据
     * @return 检验不合格数据
     */
    @Override
    public List<DbUnqied> selectDbUnqiedList(DbUnqied dbUnqied)
    {
        return dbUnqiedMapper.selectDbUnqiedList(dbUnqied);
    }

    /**
     * 新增检验不合格数据
     *
     * @param dbUnqied 检验不合格数据
     * @return 结果
     */
    @Override
    public int insertDbUnqied(DbUnqied dbUnqied)
    {
        return dbUnqiedMapper.insertDbUnqied(dbUnqied);
    }

    /**
     * 修改检验不合格数据
     *
     * @param dbUnqied 检验不合格数据
     * @return 结果
     */
    @Override
    public int updateDbUnqied(DbUnqied dbUnqied)
    {
        return dbUnqiedMapper.updateDbUnqied(dbUnqied);
    }

    /**
     * 批量删除检验不合格数据
     *
     * @param unqiedIds 需要删除的检验不合格数据主键
     * @return 结果
     */
    @Override
    public int deleteDbUnqiedByUnqiedIds(Long[] unqiedIds)
    {
        return dbUnqiedMapper.deleteDbUnqiedByUnqiedIds(unqiedIds);
    }

    /**
     * 删除检验不合格数据信息
     *
     * @param unqiedId 检验不合格数据主键
     * @return 结果
     */
    @Override
    public int deleteDbUnqiedByUnqiedId(Long unqiedId)
    {
        return dbUnqiedMapper.deleteDbUnqiedByUnqiedId(unqiedId);
    }

    /**
     * 导入检验不合格数据数据
     *
     * @param dbUnqiedList dbUnqiedList 检验不合格数据信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importDbUnqied(List<DbUnqied> dbUnqiedList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(dbUnqiedList) || dbUnqiedList.size() == 0) {
            throw new ServiceException("导入检验不合格数据数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (DbUnqied dbUnqied : dbUnqiedList){
            try {
                // 验证是否存在
                DbUnqied oldData = dbUnqiedMapper.checkEntryIdGnoUnique(dbUnqied.getEntryId(), dbUnqied.getgNo());
                if (StringUtils.isNull(oldData)) {
                    dbUnqied.setCreateBy(operName);
                    dbUnqied.setConfirmStatus(Constants.DB_IS_STATUS_NO);
                    this.insertDbUnqied(dbUnqied);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    dbUnqied.setUnqiedId(oldData.getUnqiedId());
                    dbUnqied.setUpdateBy(operName);
                    dbUnqied.setConfirmStatus(Constants.DB_IS_STATUS_NO);
                    this.updateDbUnqied(dbUnqied);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、报关单号项号【 " + dbUnqied.getEntryIdAndGNo() + " 】已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、报关单号项号【 " + dbUnqied.getEntryIdAndGNo() + " 】导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    @Override
    public DbUnqied checkEntryIdGnoUnique(String entryId, Long gNo) {
        return dbUnqiedMapper.checkEntryIdGnoUnique(entryId, gNo);
    }

    @Override
    public String checkDbUnqiedUnique(DbUnqied dbUnqied) {
        Long unqiedId = StringUtils.isNull(dbUnqied.getUnqiedId()) ? -1L : dbUnqied.getUnqiedId();
        DbUnqied info = dbUnqiedMapper.checkEntryIdGnoUnique(dbUnqied.getEntryId(), dbUnqied.getgNo());
        if (StringUtils.isNotNull(info) && info.getUnqiedId().longValue() != unqiedId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 导入危险化学品及其包装检验监管情况统计信息
     *
     * @param dbUnqiedList 批量数据
     * @return 结果
     */
    @Override
    public int batchInsertDbUnqiedList(List<DbUnqied> dbUnqiedList) {
        return dbUnqiedMapper.batchInsertDbUnqiedList(dbUnqiedList);
    }

}
