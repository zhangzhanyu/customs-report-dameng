package com.qianjing.project.db.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 其他检验不合格数据对象 db_unqied_other
 * 
 * @author qianjing
 * @date 2022-08-03
 */
public class DbUnqiedOther extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long unqiedOtherId;

    /** 原产国/目的国 */
    @Excel(name = "原产国/目的国")
    private String originCountry;

    /** 送检单位 */
    @Excel(name = "送检单位")
    private String inspectionUnit;

    /** 实验室名称 */
    @Excel(name = "实验室名称")
    private String laboratoryName;

    /** 申报号 */
    @Excel(name = "申报号")
    private String declareCode;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String gName;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 货号/款号/型号/规格 */
    @Excel(name = "货号/款号/型号/规格")
    private String gModel;

    /** 检毕日期 */
    @Excel(name = "检毕日期")
    private String completeDate;

    /** 检测标准或依据 */
    @Excel(name = "检测标准或依据")
    private String standard;

    /** 检测项目和结果 */
    @Excel(name = "检测项目和结果")
    private String itemResult;

    /** 不合格情况描述 */
    @Excel(name = "不合格情况描述")
    private String describe;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 删除标志（0：删除，1：正常） */
    private String delFlag;

    public void setUnqiedOtherId(Long unqiedOtherId) 
    {
        this.unqiedOtherId = unqiedOtherId;
    }

    public Long getUnqiedOtherId() 
    {
        return unqiedOtherId;
    }
    public void setOriginCountry(String originCountry) 
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry() 
    {
        return originCountry;
    }
    public void setInspectionUnit(String inspectionUnit) 
    {
        this.inspectionUnit = inspectionUnit;
    }

    public String getInspectionUnit() 
    {
        return inspectionUnit;
    }
    public void setLaboratoryName(String laboratoryName) 
    {
        this.laboratoryName = laboratoryName;
    }

    public String getLaboratoryName() 
    {
        return laboratoryName;
    }
    public void setDeclareCode(String declareCode) 
    {
        this.declareCode = declareCode;
    }

    public String getDeclareCode() 
    {
        return declareCode;
    }
    public void setgName(String gName) 
    {
        this.gName = gName;
    }

    public String getgName() 
    {
        return gName;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setgModel(String gModel) 
    {
        this.gModel = gModel;
    }

    public String getgModel() 
    {
        return gModel;
    }
    public void setCompleteDate(String completeDate) 
    {
        this.completeDate = completeDate;
    }

    public String getCompleteDate() 
    {
        return completeDate;
    }
    public void setStandard(String standard) 
    {
        this.standard = standard;
    }

    public String getStandard() 
    {
        return standard;
    }
    public void setItemResult(String itemResult) 
    {
        this.itemResult = itemResult;
    }

    public String getItemResult() 
    {
        return itemResult;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("unqiedOtherId", getUnqiedOtherId())
            .append("originCountry", getOriginCountry())
            .append("inspectionUnit", getInspectionUnit())
            .append("laboratoryName", getLaboratoryName())
            .append("declareCode", getDeclareCode())
            .append("gName", getgName())
            .append("brand", getBrand())
            .append("gModel", getgModel())
            .append("completeDate", getCompleteDate())
            .append("standard", getStandard())
            .append("itemResult", getItemResult())
            .append("describe", getDescribe())
            .append("remarks", getRemarks())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
