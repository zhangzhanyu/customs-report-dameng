package com.qianjing.project.db.service;

import java.util.List;
import com.qianjing.project.db.domain.DbUnqiedOther;

/**
 * 其他检验不合格数据Service接口
 * 
 * @author qianjing
 * @date 2022-08-03
 */
public interface IDbUnqiedOtherService 
{
    /**
     * 查询其他检验不合格数据
     * 
     * @param unqiedOtherId 其他检验不合格数据主键
     * @return 其他检验不合格数据
     */
    public DbUnqiedOther selectDbUnqiedOtherByUnqiedOtherId(Long unqiedOtherId);

    /**
     * 查询其他检验不合格数据列表
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 其他检验不合格数据集合
     */
    public List<DbUnqiedOther> selectDbUnqiedOtherList(DbUnqiedOther dbUnqiedOther);

    /**
     * 新增其他检验不合格数据
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 结果
     */
    public int insertDbUnqiedOther(DbUnqiedOther dbUnqiedOther);

    /**
     * 修改其他检验不合格数据
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 结果
     */
    public int updateDbUnqiedOther(DbUnqiedOther dbUnqiedOther);

    /**
     * 批量删除其他检验不合格数据
     * 
     * @param unqiedOtherIds 需要删除的其他检验不合格数据主键集合
     * @return 结果
     */
    public int deleteDbUnqiedOtherByUnqiedOtherIds(Long[] unqiedOtherIds);

    /**
     * 删除其他检验不合格数据信息
     * 
     * @param unqiedOtherId 其他检验不合格数据主键
     * @return 结果
     */
    public int deleteDbUnqiedOtherByUnqiedOtherId(Long unqiedOtherId);

    /**
     * 导入其他检验不合格数据信息
     *
     * @param dbUnqiedOtherList 其他检验不合格数据信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importDbUnqiedOther(List<DbUnqiedOther> dbUnqiedOtherList, Boolean isUpdateSupport, String operName);
}
