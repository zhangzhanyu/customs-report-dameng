package com.qianjing.project.db.service.impl;

import java.util.List;
import com.qianjing.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.db.mapper.DbUnqiedOtherMapper;
import com.qianjing.project.db.domain.DbUnqiedOther;
import com.qianjing.project.db.service.IDbUnqiedOtherService;

/**
 * 其他检验不合格数据Service业务层处理
 * 
 * @author qianjing
 * @date 2022-08-03
 */
@Service
public class DbUnqiedOtherServiceImpl implements IDbUnqiedOtherService 
{
    private static final Logger log = LoggerFactory.getLogger(DbUnqiedOtherServiceImpl.class);

    @Autowired
    private DbUnqiedOtherMapper dbUnqiedOtherMapper;

    /**
     * 查询其他检验不合格数据
     * 
     * @param unqiedOtherId 其他检验不合格数据主键
     * @return 其他检验不合格数据
     */
    @Override
    public DbUnqiedOther selectDbUnqiedOtherByUnqiedOtherId(Long unqiedOtherId)
    {
        return dbUnqiedOtherMapper.selectDbUnqiedOtherByUnqiedOtherId(unqiedOtherId);
    }

    /**
     * 查询其他检验不合格数据列表
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 其他检验不合格数据
     */
    @Override
    public List<DbUnqiedOther> selectDbUnqiedOtherList(DbUnqiedOther dbUnqiedOther)
    {
        return dbUnqiedOtherMapper.selectDbUnqiedOtherList(dbUnqiedOther);
    }

    /**
     * 新增其他检验不合格数据
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 结果
     */
    @Override
    public int insertDbUnqiedOther(DbUnqiedOther dbUnqiedOther)
    {
        dbUnqiedOther.setCreateTime(DateUtils.getNowDate());
        return dbUnqiedOtherMapper.insertDbUnqiedOther(dbUnqiedOther);
    }

    /**
     * 修改其他检验不合格数据
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 结果
     */
    @Override
    public int updateDbUnqiedOther(DbUnqiedOther dbUnqiedOther)
    {
        dbUnqiedOther.setUpdateTime(DateUtils.getNowDate());
        return dbUnqiedOtherMapper.updateDbUnqiedOther(dbUnqiedOther);
    }

    /**
     * 批量删除其他检验不合格数据
     * 
     * @param unqiedOtherIds 需要删除的其他检验不合格数据主键
     * @return 结果
     */
    @Override
    public int deleteDbUnqiedOtherByUnqiedOtherIds(Long[] unqiedOtherIds)
    {
        return dbUnqiedOtherMapper.deleteDbUnqiedOtherByUnqiedOtherIds(unqiedOtherIds);
    }

    /**
     * 删除其他检验不合格数据信息
     * 
     * @param unqiedOtherId 其他检验不合格数据主键
     * @return 结果
     */
    @Override
    public int deleteDbUnqiedOtherByUnqiedOtherId(Long unqiedOtherId)
    {
        return dbUnqiedOtherMapper.deleteDbUnqiedOtherByUnqiedOtherId(unqiedOtherId);
    }

    /**
     * 导入其他检验不合格数据数据
     *
     * @param dbUnqiedOtherList dbUnqiedOtherList 其他检验不合格数据信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importDbUnqiedOther(List<DbUnqiedOther> dbUnqiedOtherList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(dbUnqiedOtherList) || dbUnqiedOtherList.size() == 0) {
            throw new ServiceException("导入其他检验不合格数据数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (DbUnqiedOther dbUnqiedOther : dbUnqiedOtherList){
            try {
                // 验证是否存在
                if (true) {
                    dbUnqiedOther.setCreateBy(operName);
                    this.insertDbUnqiedOther(dbUnqiedOther);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    dbUnqiedOther.setUpdateBy(operName);
                    this.updateDbUnqiedOther(dbUnqiedOther);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
