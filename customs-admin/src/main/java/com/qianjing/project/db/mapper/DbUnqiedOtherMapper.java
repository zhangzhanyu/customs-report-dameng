package com.qianjing.project.db.mapper;

import java.util.List;
import com.qianjing.project.db.domain.DbUnqiedOther;

/**
 * 其他检验不合格数据Mapper接口
 * 
 * @author qianjing
 * @date 2022-08-03
 */
public interface DbUnqiedOtherMapper 
{
    /**
     * 查询其他检验不合格数据
     * 
     * @param unqiedOtherId 其他检验不合格数据主键
     * @return 其他检验不合格数据
     */
    public DbUnqiedOther selectDbUnqiedOtherByUnqiedOtherId(Long unqiedOtherId);

    /**
     * 查询其他检验不合格数据列表
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 其他检验不合格数据集合
     */
    public List<DbUnqiedOther> selectDbUnqiedOtherList(DbUnqiedOther dbUnqiedOther);

    /**
     * 新增其他检验不合格数据
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 结果
     */
    public int insertDbUnqiedOther(DbUnqiedOther dbUnqiedOther);

    /**
     * 修改其他检验不合格数据
     * 
     * @param dbUnqiedOther 其他检验不合格数据
     * @return 结果
     */
    public int updateDbUnqiedOther(DbUnqiedOther dbUnqiedOther);

    /**
     * 删除其他检验不合格数据
     * 
     * @param unqiedOtherId 其他检验不合格数据主键
     * @return 结果
     */
    public int deleteDbUnqiedOtherByUnqiedOtherId(Long unqiedOtherId);

    /**
     * 批量删除其他检验不合格数据
     * 
     * @param unqiedOtherIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbUnqiedOtherByUnqiedOtherIds(Long[] unqiedOtherIds);
}
