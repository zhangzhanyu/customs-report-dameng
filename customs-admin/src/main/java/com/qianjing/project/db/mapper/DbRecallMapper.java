package com.qianjing.project.db.mapper;

import java.util.List;
import com.qianjing.project.db.domain.DbRecall;

/**
 * 官方召回信息Mapper接口
 * 
 * @author qianjing
 * @date 2023-05-11
 */
public interface DbRecallMapper 
{
    /**
     * 查询官方召回信息
     * 
     * @param recallId 官方召回信息主键
     * @return 官方召回信息
     */
    public DbRecall selectDbRecallByRecallId(Long recallId);

    /**
     * 查询官方召回信息列表
     * 
     * @param dbRecall 官方召回信息
     * @return 官方召回信息集合
     */
    public List<DbRecall> selectDbRecallList(DbRecall dbRecall);

    /**
     * 新增官方召回信息
     * 
     * @param dbRecall 官方召回信息
     * @return 结果
     */
    public int insertDbRecall(DbRecall dbRecall);

    /**
     * 修改官方召回信息
     * 
     * @param dbRecall 官方召回信息
     * @return 结果
     */
    public int updateDbRecall(DbRecall dbRecall);

    /**
     * 删除官方召回信息
     * 
     * @param recallId 官方召回信息主键
     * @return 结果
     */
    public int deleteDbRecallByRecallId(Long recallId);

    /**
     * 批量删除官方召回信息
     * 
     * @param recallIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbRecallByRecallIds(Long[] recallIds);
}
