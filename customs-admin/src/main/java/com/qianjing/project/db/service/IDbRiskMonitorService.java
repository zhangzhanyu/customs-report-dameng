package com.qianjing.project.db.service;

import java.util.List;
import com.qianjing.project.db.domain.DbRiskMonitor;

/**
 * 风险监测信息Service接口
 * 
 * @author qianjing
 * @date 2022-08-03
 */
public interface IDbRiskMonitorService 
{
    /**
     * 查询风险监测信息
     * 
     * @param riskMonitorId 风险监测信息主键
     * @return 风险监测信息
     */
    public DbRiskMonitor selectDbRiskMonitorByRiskMonitorId(Long riskMonitorId);

    /**
     * 查询风险监测信息列表
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 风险监测信息集合
     */
    public List<DbRiskMonitor> selectDbRiskMonitorList(DbRiskMonitor dbRiskMonitor);

    /**
     * 新增风险监测信息
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 结果
     */
    public int insertDbRiskMonitor(DbRiskMonitor dbRiskMonitor);

    /**
     * 修改风险监测信息
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 结果
     */
    public int updateDbRiskMonitor(DbRiskMonitor dbRiskMonitor);

    /**
     * 批量删除风险监测信息
     * 
     * @param riskMonitorIds 需要删除的风险监测信息主键集合
     * @return 结果
     */
    public int deleteDbRiskMonitorByRiskMonitorIds(Long[] riskMonitorIds);

    /**
     * 删除风险监测信息信息
     * 
     * @param riskMonitorId 风险监测信息主键
     * @return 结果
     */
    public int deleteDbRiskMonitorByRiskMonitorId(Long riskMonitorId);

    /**
     * 导入风险监测信息信息
     *
     * @param dbRiskMonitorList 风险监测信息信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importDbRiskMonitor(List<DbRiskMonitor> dbRiskMonitorList, Boolean isUpdateSupport, String operName);
}
