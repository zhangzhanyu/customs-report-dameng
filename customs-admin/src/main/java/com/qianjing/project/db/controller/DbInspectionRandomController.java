package com.qianjing.project.db.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.db.domain.DbInspectionRandom;
import com.qianjing.project.db.service.IDbInspectionRandomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 2021年法检目录外商品抽查检验结果汇总Controller
 * 
 * @author zhangzy
 * @date 2022-08-03
 */
@RestController
@RequestMapping("/db/random")
public class DbInspectionRandomController extends BaseController
{
    @Autowired
    private IDbInspectionRandomService dbInspectionRandomService;

    /**
     * 查询2021年法检目录外商品抽查检验结果汇总列表
     */
    @PreAuthorize("@ss.hasPermi('db:random:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbInspectionRandom dbInspectionRandom)
    {
        startPage();
        List<DbInspectionRandom> list = dbInspectionRandomService.selectDbInspectionRandomList(dbInspectionRandom);
        return getDataTable(list);
    }

    /**
     * 导出2021年法检目录外商品抽查检验结果汇总列表
     */
    @PreAuthorize("@ss.hasPermi('db:random:export')")
    @Log(title = "2021年法检目录外商品抽查检验结果汇总", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DbInspectionRandom dbInspectionRandom)
    {
        List<DbInspectionRandom> list = dbInspectionRandomService.selectDbInspectionRandomList(dbInspectionRandom);
        ExcelUtil<DbInspectionRandom> util = new ExcelUtil<DbInspectionRandom>(DbInspectionRandom.class);
        return util.exportExcel(list, "2021年法检目录外商品抽查检验结果汇总数据");
    }

    /**
     * 导入2021年法检目录外商品抽查检验结果汇总列表
     */
    @Log(title = "2021年法检目录外商品抽查检验结果汇总", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('db:random:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<DbInspectionRandom> util = new ExcelUtil<DbInspectionRandom>(DbInspectionRandom.class);
        List<DbInspectionRandom> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = dbInspectionRandomService.importDbInspectionRandom(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载2021年法检目录外商品抽查检验结果汇总导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<DbInspectionRandom> util = new ExcelUtil<DbInspectionRandom>(DbInspectionRandom.class);
        util.importTemplateExcel(response, "2021年法检目录外商品抽查检验结果汇总数据");
    }

    /**
     * 获取2021年法检目录外商品抽查检验结果汇总详细信息
     */
    @PreAuthorize("@ss.hasPermi('db:random:query')")
    @GetMapping(value = "/{inspectionRandomId}")
    public AjaxResult getInfo(@PathVariable("inspectionRandomId") Long inspectionRandomId)
    {
        return AjaxResult.success(dbInspectionRandomService.selectDbInspectionRandomByInspectionRandomId(inspectionRandomId));
    }

    /**
     * 新增2021年法检目录外商品抽查检验结果汇总
     */
    @PreAuthorize("@ss.hasPermi('db:random:add')")
    @Log(title = "2021年法检目录外商品抽查检验结果汇总", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbInspectionRandom dbInspectionRandom)
    {
        return toAjax(dbInspectionRandomService.insertDbInspectionRandom(dbInspectionRandom));
    }

    /**
     * 修改2021年法检目录外商品抽查检验结果汇总
     */
    @PreAuthorize("@ss.hasPermi('db:random:edit')")
    @Log(title = "2021年法检目录外商品抽查检验结果汇总", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbInspectionRandom dbInspectionRandom)
    {
        return toAjax(dbInspectionRandomService.updateDbInspectionRandom(dbInspectionRandom));
    }

    /**
     * 删除2021年法检目录外商品抽查检验结果汇总
     */
    @PreAuthorize("@ss.hasPermi('db:random:remove')")
    @Log(title = "2021年法检目录外商品抽查检验结果汇总", businessType = BusinessType.DELETE)
	@DeleteMapping("/{inspectionRandomIds}")
    public AjaxResult remove(@PathVariable Long[] inspectionRandomIds)
    {
        return toAjax(dbInspectionRandomService.deleteDbInspectionRandomByInspectionRandomIds(inspectionRandomIds));
    }
}
