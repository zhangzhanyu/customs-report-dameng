package com.qianjing.project.db.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

import com.qianjing.common.constant.Constants;
import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.project.db.domain.DbUnqiedImport;
import com.qianjing.project.db.service.impl.AsyncUpdateTaskService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.db.service.IDbUnqiedService;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.common.utils.poi.ExcelUtil;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.qianjing.framework.web.page.TableDataInfo;

/**
 * 检验不合格数据Controller
 *
 * @author qianjing
 * @date 2022-06-09
 */
@RestController
@RequestMapping("/db/unqied")
public class DbUnqiedController extends BaseController
{
    @Autowired
    private IDbUnqiedService dbUnqiedService;

    @Autowired
    private AsyncUpdateTaskService asyncUpdateTaskService;

    /**
     * 查询检验不合格数据列表
     */
    @PreAuthorize("@ss.hasPermi('db:unqied:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbUnqied dbUnqied)
    {
        startPage();
        List<DbUnqied> list = dbUnqiedService.selectDbUnqiedList(dbUnqied);
        return getDataTable(list);
    }

    /**
     * 导出检验不合格数据列表
     */
    @PreAuthorize("@ss.hasPermi('db:unqied:export')")
    @Log(title = "检验不合格数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DbUnqied dbUnqied)
    {
        List<DbUnqied> list = dbUnqiedService.selectDbUnqiedList(dbUnqied);
        ExcelUtil<DbUnqied> util = new ExcelUtil<DbUnqied>(DbUnqied.class);
        return util.exportExcel(list, "检验不合格数据数据");
    }

    /**
     * 导入检验不合格数据列表
     */
    @Log(title = "检验不合格数据", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('db:unqied:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<DbUnqiedImport> util = new ExcelUtil<>(DbUnqiedImport.class);
        List<DbUnqiedImport> dbUnqiedImportList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        List<DbUnqied> dbUnqiedList = new ArrayList<>();
        for (DbUnqiedImport dbUnqiedImport : dbUnqiedImportList) {
            DbUnqied dbUnqied = new DbUnqied();
            dbUnqied.setEntryId(dbUnqiedImport.getEntryId());
            dbUnqied.setgNo(dbUnqiedImport.getGoodNo());
            dbUnqied.setEntryIdAndGNo(dbUnqiedImport.getEntryId().concat(String.valueOf(dbUnqiedImport.getGoodNo())));
            dbUnqied.setItem(dbUnqiedImport.getItem());
            dbUnqied.setJudgmentBasis(dbUnqiedImport.getJudgmentBasis());
            dbUnqied.setDescription(dbUnqiedImport.getDescription());
            dbUnqied.setDisposal(dbUnqiedImport.getDisposal());
            dbUnqied.setUnqualifiedSiteFlag(dbUnqiedImport.getUnqualifiedSiteFlag());
            dbUnqied.setUnqualifiedLaboratoryFlag(dbUnqiedImport.getUnqualifiedLaboratoryFlag());
            dbUnqied.setEnterCustoms(dbUnqiedImport.getEnterCustoms());
            dbUnqied.setReportTime(dbUnqiedImport.getReportTime());
            dbUnqied.setCreateBy(operName);
            dbUnqied.setConfirmStatus(Constants.DB_IS_STATUS_NO);
            dbUnqiedList.add(dbUnqied);
        }
        String message = dbUnqiedService.importDbUnqied(dbUnqiedList, updateSupport, operName);
        // 添加异步任务
        asyncUpdateTaskService.dataScheduled(dbUnqiedList);
        return AjaxResult.success(message);
    }

    /**
     * 下载检验不合格数据导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<DbUnqiedImport> util = new ExcelUtil<>(DbUnqiedImport.class);
        util.importTemplateExcel(response, "检验不合格数据数据");
    }

    /**
     * 获取检验不合格数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('db:unqied:query')")
    @GetMapping(value = "/{unqiedId}")
    public AjaxResult getInfo(@PathVariable("unqiedId") Long unqiedId)
    {
        return AjaxResult.success(dbUnqiedService.selectDbUnqiedByUnqiedId(unqiedId));
    }

    /**
     * 新增检验不合格数据
     */
    @PreAuthorize("@ss.hasPermi('db:unqied:add')")
    @Log(title = "检验不合格数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbUnqied dbUnqied) {
        dbUnqied.setEntryIdAndGNo(dbUnqied.getEntryId().concat(String.valueOf(dbUnqied.getgNo())));
        if (Constants.NOT_UNIQUE.equals(dbUnqiedService.checkDbUnqiedUnique(dbUnqied))) {
            return AjaxResult.error("新增失败，报关单号项号【'" + dbUnqied.getEntryIdAndGNo() + "'】已存在");
        }
        List<DbUnqied> dbUnqiedList = new ArrayList<>();
        dbUnqiedList.add(dbUnqied);
        // 添加异步任务
        asyncUpdateTaskService.dataScheduled(dbUnqiedList);
        return toAjax(dbUnqiedService.insertDbUnqied(dbUnqied));
    }

    /**
     * 修改检验不合格数据
     */
    @PreAuthorize("@ss.hasPermi('db:unqied:edit')")
    @Log(title = "检验不合格数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbUnqied dbUnqied) {
        dbUnqied.setEntryIdAndGNo(dbUnqied.getEntryId().concat(String.valueOf(dbUnqied.getgNo())));
        if (Constants.NOT_UNIQUE.equals(dbUnqiedService.checkDbUnqiedUnique(dbUnqied))) {
            return AjaxResult.error("修改失败，报关单号项号【'" + dbUnqied.getEntryIdAndGNo() + "'】已存在");
        }
        List<DbUnqied> dbUnqiedList = new ArrayList<>();
        dbUnqiedList.add(dbUnqied);
        // 添加异步任务
        asyncUpdateTaskService.dataScheduled(dbUnqiedList);
        return toAjax(dbUnqiedService.updateDbUnqied(dbUnqied));
    }

    /**
     * 删除检验不合格数据
     */
    @PreAuthorize("@ss.hasPermi('db:unqied:remove')")
    @Log(title = "检验不合格数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{unqiedIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedIds)
    {
        return toAjax(dbUnqiedService.deleteDbUnqiedByUnqiedIds(unqiedIds));
    }
}
