package com.qianjing.project.db.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.db.domain.DbRiskMonitor;
import com.qianjing.project.db.service.IDbRiskMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 风险监测信息Controller
 * 
 * @author qianjing
 * @date 2022-08-03
 */
@RestController
@RequestMapping("/db/monitor")
public class DbRiskMonitorController extends BaseController
{
    @Autowired
    private IDbRiskMonitorService dbRiskMonitorService;

    /**
     * 查询风险监测信息列表
     */
    @PreAuthorize("@ss.hasPermi('db:monitor:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbRiskMonitor dbRiskMonitor)
    {
        startPage();
        List<DbRiskMonitor> list = dbRiskMonitorService.selectDbRiskMonitorList(dbRiskMonitor);
        return getDataTable(list);
    }

    /**
     * 导出风险监测信息列表
     */
    @PreAuthorize("@ss.hasPermi('db:monitor:export')")
    @Log(title = "风险监测信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DbRiskMonitor dbRiskMonitor)
    {
        List<DbRiskMonitor> list = dbRiskMonitorService.selectDbRiskMonitorList(dbRiskMonitor);
        ExcelUtil<DbRiskMonitor> util = new ExcelUtil<DbRiskMonitor>(DbRiskMonitor.class);
        return util.exportExcel(list, "风险监测信息数据");
    }

    /**
     * 导入风险监测信息列表
     */
    @Log(title = "风险监测信息", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('db:monitor:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<DbRiskMonitor> util = new ExcelUtil<DbRiskMonitor>(DbRiskMonitor.class);
        List<DbRiskMonitor> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = dbRiskMonitorService.importDbRiskMonitor(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载风险监测信息导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<DbRiskMonitor> util = new ExcelUtil<DbRiskMonitor>(DbRiskMonitor.class);
        util.importTemplateExcel(response, "风险监测信息数据");
    }

    /**
     * 获取风险监测信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('db:monitor:query')")
    @GetMapping(value = "/{riskMonitorId}")
    public AjaxResult getInfo(@PathVariable("riskMonitorId") Long riskMonitorId)
    {
        return AjaxResult.success(dbRiskMonitorService.selectDbRiskMonitorByRiskMonitorId(riskMonitorId));
    }

    /**
     * 新增风险监测信息
     */
    @PreAuthorize("@ss.hasPermi('db:monitor:add')")
    @Log(title = "风险监测信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbRiskMonitor dbRiskMonitor)
    {
        return toAjax(dbRiskMonitorService.insertDbRiskMonitor(dbRiskMonitor));
    }

    /**
     * 修改风险监测信息
     */
    @PreAuthorize("@ss.hasPermi('db:monitor:edit')")
    @Log(title = "风险监测信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbRiskMonitor dbRiskMonitor)
    {
        return toAjax(dbRiskMonitorService.updateDbRiskMonitor(dbRiskMonitor));
    }

    /**
     * 删除风险监测信息
     */
    @PreAuthorize("@ss.hasPermi('db:monitor:remove')")
    @Log(title = "风险监测信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{riskMonitorIds}")
    public AjaxResult remove(@PathVariable Long[] riskMonitorIds)
    {
        return toAjax(dbRiskMonitorService.deleteDbRiskMonitorByRiskMonitorIds(riskMonitorIds));
    }
}
