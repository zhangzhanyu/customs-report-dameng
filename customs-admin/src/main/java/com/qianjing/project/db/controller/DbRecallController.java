package com.qianjing.project.db.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.db.domain.DbRecall;
import com.qianjing.project.db.service.IDbRecallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 官方召回信息Controller
 * 
 * @author qianjing
 * @date 2023-05-11
 */
@RestController
@RequestMapping("/db/recall")
public class DbRecallController extends BaseController
{
    @Autowired
    private IDbRecallService dbRecallService;

    /**
     * 查询官方召回信息列表
     */
//    @PreAuthorize("@ss.hasPermi('db:recall:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbRecall dbRecall)
    {
        startPage();
        List<DbRecall> list = dbRecallService.selectDbRecallList(dbRecall);
        return getDataTable(list);
    }

    /**
     * 导出官方召回信息列表
     */
    @PreAuthorize("@ss.hasPermi('db:recall:export')")
    @Log(title = "官方召回信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DbRecall dbRecall)
    {
        List<DbRecall> list = dbRecallService.selectDbRecallList(dbRecall);
        ExcelUtil<DbRecall> util = new ExcelUtil<DbRecall>(DbRecall.class);
        return util.exportExcel(list, "官方召回信息数据");
    }

    /**
     * 导入官方召回信息列表
     */
    @Log(title = "官方召回信息", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('db:recall:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<DbRecall> util = new ExcelUtil<DbRecall>(DbRecall.class);
        List<DbRecall> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = dbRecallService.importDbRecall(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载官方召回信息导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<DbRecall> util = new ExcelUtil<DbRecall>(DbRecall.class);
        util.importTemplateExcel(response, "官方召回信息数据");
    }

    /**
     * 获取官方召回信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('db:recall:query')")
    @GetMapping(value = "/{recallId}")
    public AjaxResult getInfo(@PathVariable("recallId") Long recallId)
    {
        return AjaxResult.success(dbRecallService.selectDbRecallByRecallId(recallId));
    }

    /**
     * 新增官方召回信息
     */
    @PreAuthorize("@ss.hasPermi('db:recall:add')")
    @Log(title = "官方召回信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbRecall dbRecall)
    {
        return toAjax(dbRecallService.insertDbRecall(dbRecall));
    }

    /**
     * 修改官方召回信息
     */
    @PreAuthorize("@ss.hasPermi('db:recall:edit')")
    @Log(title = "官方召回信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbRecall dbRecall)
    {
        return toAjax(dbRecallService.updateDbRecall(dbRecall));
    }

    /**
     * 删除官方召回信息
     */
    @PreAuthorize("@ss.hasPermi('db:recall:remove')")
    @Log(title = "官方召回信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recallIds}")
    public AjaxResult remove(@PathVariable Long[] recallIds)
    {
        return toAjax(dbRecallService.deleteDbRecallByRecallIds(recallIds));
    }
}
