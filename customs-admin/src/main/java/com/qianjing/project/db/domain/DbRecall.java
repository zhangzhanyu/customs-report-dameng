package com.qianjing.project.db.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 官方召回信息对象 hg_recall
 *
 * @author qianjing
 * @date 2022-11-10
 */
public class DbRecall extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long recallId;

    /** 业务主键 */
    @JSONField(name = "id")
    private Long id;

    /** 来源名称英文 */
    @JSONField(name = "sourceName_en")
    @Excel(name = "来源名称英文")
    private String sourceNameEn;

    /** 来源名称中文 */
    @JSONField(name = "sourceName_cn")
    @Excel(name = "来源名称中文")
    private String sourceNameCn;

    /** 来源域名 */
    @JSONField(name = "sourceDomainName")
    @Excel(name = "来源域名")
    private String sourceDomainName;

    /** 采集日期 */
    @JSONField(name = "collectDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采集日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date collectDate;

    /** 来源类别 */
    @JSONField(name = "sourceType")
    @Excel(name = "来源类别")
    private String sourceType;

    /** 来源链接 */
    @JSONField(name = "source_url")
    @Excel(name = "来源链接")
    private String sourceUrl;

    /** 发布日期 */
    @JSONField(name = "publishDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishDate;

    /** 标题英文 */
    @JSONField(name = "title_en")
    @Excel(name = "标题英文")
    private String titleEn;

    /** 标题中文 */
    @JSONField(name = "title_cn")
    @Excel(name = "标题中文")
    private String titleCn;

    /** 产品信息英文 */
    @JSONField(name = "products_en")
    @Excel(name = "产品信息英文")
    private String productsEn;

    /** 产品信息中文 */
    @JSONField(name = "products_cn")
    @Excel(name = "产品信息中文")
    private String productsCn;

    /** 描述英文 */
    @JSONField(name = "description_en")
    @Excel(name = "描述英文")
    private String descriptionEn;

    /** 描述中文 */
    @JSONField(name = "description_cn")
    @Excel(name = "描述中文")
    private String descriptionCn;

    /** 公司英文 */
    @JSONField(name = "firm_en")
    @Excel(name = "公司英文")
    private String firmEn;

    /** 公司中文 */
    @JSONField(name = "firm_cn")
    @Excel(name = "公司中文")
    private String firmCn;

    /** 国家 */
    @JSONField(name = "country")
    @Excel(name = "国家")
    private String country;

    /** 措施 */
    @JSONField(name = "remedyOptions")
    @Excel(name = "措施")
    private String remedyOptions;

    /** 产品分类tag */
    @JSONField(name = "tag_products")
    @Excel(name = "产品分类tag")
    private String tagProduct;

    /** 产品编号tag */
    @JSONField(name = "tag_product_id")
    @Excel(name = "产品编号tag")
    private Long tagProductId;

    /** 公司品牌tag */
    @JSONField(name = "tag_firm")
    @Excel(name = "公司品牌tag")
    private String tagFirm;

    /** 公司品牌tag */
    @JSONField(name = "tag_hazards")
    @Excel(name = "公司品牌tag")
    private String tagHazards;

    /** 信息来源tag */
    @JSONField(name = "tag_source")
    @Excel(name = "信息来源tag")
    private String tagSource;

    /** 和中国有关的tag */
    @JSONField(name = "tag_china")
    @Excel(name = "和中国有关的tag")
    private String tagChina;

    /** 特殊人群 */
    @JSONField(name = "tag_people")
    @Excel(name = "特殊人群")
    private String tagPeople;

    /** 情感tag */
    @JSONField(name = "tag_emotion")
    @Excel(name = "情感tag")
    private Long tagEmotion;

    /** 风险等级tag */
    @JSONField(name = "tag_level")
    @Excel(name = "风险等级tag")
    private String tagLevel;

    /** 召回ID */
    @JSONField(name = "recallNumber")
    @Excel(name = "召回ID")
    private String recallNumber;

    /** 召回日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JSONField(name = "recallDate")
    @Excel(name = "召回日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recallDate;

    /** 联系方式 */
    @JSONField(name = "consumerContact")
    @Excel(name = "联系方式")
    private String consumerContact;

    /** UPC */
    @JSONField(name = "productUpcs")
    @Excel(name = "UPC")
    private String productUpcs;

    /** 损害 */
    @JSONField(name = "injuries")
    @Excel(name = "损害")
    private String injuries;

    /** 法律引文 */
    @JSONField(name = "citation")
    @Excel(name = "法律引文")
    private String citation;

    /** 风险英文 */
    @JSONField(name = "hazards_en")
    @Excel(name = "风险英文")
    private String hazardsEn;

    /** 风险中文 */
    @JSONField(name = "hazards_cn")
    @Excel(name = "风险中文")
    private String hazardsCn;

    /** p1 */
    @JSONField(name = "p1")
    @Excel(name = "p1")
    private String p1;

    /** p2 */
    @JSONField(name = "p2")
    @Excel(name = "p2")
    private String p2;

    /** p3 */
    @JSONField(name = "p3")
    @Excel(name = "p3")
    private String p3;

    /** p4 */
    @JSONField(name = "p4")
    @Excel(name = "p4")
    private String p4;

    /** p5 */
    @JSONField(name = "p5")
    @Excel(name = "p5")
    private String p5;

    /** p6 */
    @JSONField(name = "p6")
    @Excel(name = "p6")
    private String p6;

    /** p7 */
    @JSONField(name = "p7")
    @Excel(name = "p7")
    private String p7;

    /** p8 */
    @JSONField(name = "p8")
    @Excel(name = "p8")
    private String p8;

    /** p9 */
    @JSONField(name = "p9")
    @Excel(name = "p9")
    private String p9;

    /** 相似召回信息 */
    @JSONField(name = "relation")
    @Excel(name = "相似召回信息")
    private Long relation;

    /** 相似度 */
    @JSONField(name = "similarity")
    @Excel(name = "相似度")
    private Double similarity;

    /** 关联 */
    @JSONField(name = "referred_number")
    @Excel(name = "关联")
    private String referredNumber;

    /** 召收国 */
    @JSONField(name = "country_recall")
    @Excel(name = "召收国")
    private String countryRecall;

    /** 召收国 */
    @JSONField(name = "tag_source_recall")
    @Excel(name = "召收国")
    private String tagSourceRecall;

    /** 产品tag */
    @JSONField(name = "tag_products")
    @Excel(name = "产品tag")
    private String tagProducts;

    /** 分数tag */
    @JSONField(name = "tag_scores")
    @Excel(name = "分数tag")
    private String tagScores;

    /** 周次 */
    @JSONField(name = "weeks")
    @Excel(name = "周次")
    private String weeks;

    /** 产品创建ID */
    @JSONField(name = "tag_products")
    @Excel(name = "产品创建ID")
    private Long prodCateId;

    /** 删除标志（0未删 1已删） */
    private String delFlag;

    public void setRecallId(Long recallId)
    {
        this.recallId = recallId;
    }

    public Long getRecallId()
    {
        return recallId;
    }
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSourceNameEn(String sourceNameEn)
    {
        this.sourceNameEn = sourceNameEn;
    }

    public String getSourceNameEn()
    {
        return sourceNameEn;
    }
    public void setSourceNameCn(String sourceNameCn)
    {
        this.sourceNameCn = sourceNameCn;
    }

    public String getSourceNameCn()
    {
        return sourceNameCn;
    }
    public void setSourceDomainName(String sourceDomainName)
    {
        this.sourceDomainName = sourceDomainName;
    }

    public String getSourceDomainName()
    {
        return sourceDomainName;
    }
    public void setCollectDate(Date collectDate)
    {
        this.collectDate = collectDate;
    }

    public Date getCollectDate()
    {
        return collectDate;
    }
    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    public String getSourceType()
    {
        return sourceType;
    }
    public void setSourceUrl(String sourceUrl)
    {
        this.sourceUrl = sourceUrl;
    }

    public String getSourceUrl()
    {
        return sourceUrl;
    }
    public void setPublishDate(Date publishDate)
    {
        this.publishDate = publishDate;
    }

    public Date getPublishDate()
    {
        return publishDate;
    }
    public void setTitleEn(String titleEn)
    {
        this.titleEn = titleEn;
    }

    public String getTitleEn()
    {
        return titleEn;
    }
    public void setTitleCn(String titleCn)
    {
        this.titleCn = titleCn;
    }

    public String getTitleCn()
    {
        return titleCn;
    }
    public void setProductsEn(String productsEn)
    {
        this.productsEn = productsEn;
    }

    public String getProductsEn()
    {
        return productsEn;
    }
    public void setProductsCn(String productsCn)
    {
        this.productsCn = productsCn;
    }

    public String getProductsCn()
    {
        return productsCn;
    }
    public void setDescriptionEn(String descriptionEn)
    {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionEn()
    {
        return descriptionEn;
    }
    public void setDescriptionCn(String descriptionCn)
    {
        this.descriptionCn = descriptionCn;
    }

    public String getDescriptionCn()
    {
        return descriptionCn;
    }
    public void setFirmEn(String firmEn)
    {
        this.firmEn = firmEn;
    }

    public String getFirmEn()
    {
        return firmEn;
    }
    public void setFirmCn(String firmCn)
    {
        this.firmCn = firmCn;
    }

    public String getFirmCn()
    {
        return firmCn;
    }
    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCountry()
    {
        return country;
    }
    public void setRemedyOptions(String remedyOptions)
    {
        this.remedyOptions = remedyOptions;
    }

    public String getRemedyOptions()
    {
        return remedyOptions;
    }
    public void setTagProduct(String tagProduct)
    {
        this.tagProduct = tagProduct;
    }

    public String getTagProduct()
    {
        return tagProduct;
    }
    public void setTagProductId(Long tagProductId)
    {
        this.tagProductId = tagProductId;
    }

    public Long getTagProductId()
    {
        return tagProductId;
    }
    public void setTagFirm(String tagFirm)
    {
        this.tagFirm = tagFirm;
    }

    public String getTagFirm()
    {
        return tagFirm;
    }
    public void setTagHazards(String tagHazards)
    {
        this.tagHazards = tagHazards;
    }

    public String getTagHazards()
    {
        return tagHazards;
    }
    public void setTagSource(String tagSource)
    {
        this.tagSource = tagSource;
    }

    public String getTagSource()
    {
        return tagSource;
    }
    public void setTagChina(String tagChina)
    {
        this.tagChina = tagChina;
    }

    public String getTagChina()
    {
        return tagChina;
    }
    public void setTagPeople(String tagPeople)
    {
        this.tagPeople = tagPeople;
    }

    public String getTagPeople()
    {
        return tagPeople;
    }
    public void setTagEmotion(Long tagEmotion)
    {
        this.tagEmotion = tagEmotion;
    }

    public Long getTagEmotion()
    {
        return tagEmotion;
    }
    public void setTagLevel(String tagLevel)
    {
        this.tagLevel = tagLevel;
    }

    public String getTagLevel()
    {
        return tagLevel;
    }
    public void setRecallNumber(String recallNumber)
    {
        this.recallNumber = recallNumber;
    }

    public String getRecallNumber()
    {
        return recallNumber;
    }
    public void setRecallDate(Date recallDate)
    {
        this.recallDate = recallDate;
    }

    public Date getRecallDate()
    {
        return recallDate;
    }
    public void setConsumerContact(String consumerContact)
    {
        this.consumerContact = consumerContact;
    }

    public String getConsumerContact()
    {
        return consumerContact;
    }
    public void setProductUpcs(String productUpcs)
    {
        this.productUpcs = productUpcs;
    }

    public String getProductUpcs()
    {
        return productUpcs;
    }
    public void setInjuries(String injuries)
    {
        this.injuries = injuries;
    }

    public String getInjuries()
    {
        return injuries;
    }
    public void setCitation(String citation)
    {
        this.citation = citation;
    }

    public String getCitation()
    {
        return citation;
    }
    public void setHazardsEn(String hazardsEn)
    {
        this.hazardsEn = hazardsEn;
    }

    public String getHazardsEn()
    {
        return hazardsEn;
    }
    public void setHazardsCn(String hazardsCn)
    {
        this.hazardsCn = hazardsCn;
    }

    public String getHazardsCn()
    {
        return hazardsCn;
    }
    public void setP1(String p1)
    {
        this.p1 = p1;
    }

    public String getP1()
    {
        return p1;
    }
    public void setP2(String p2)
    {
        this.p2 = p2;
    }

    public String getP2()
    {
        return p2;
    }
    public void setP3(String p3)
    {
        this.p3 = p3;
    }

    public String getP3()
    {
        return p3;
    }
    public void setP4(String p4)
    {
        this.p4 = p4;
    }

    public String getP4()
    {
        return p4;
    }
    public void setP5(String p5)
    {
        this.p5 = p5;
    }

    public String getP5()
    {
        return p5;
    }
    public void setP6(String p6)
    {
        this.p6 = p6;
    }

    public String getP6()
    {
        return p6;
    }
    public void setP7(String p7)
    {
        this.p7 = p7;
    }

    public String getP7()
    {
        return p7;
    }
    public void setP8(String p8)
    {
        this.p8 = p8;
    }

    public String getP8()
    {
        return p8;
    }
    public void setP9(String p9)
    {
        this.p9 = p9;
    }

    public String getP9()
    {
        return p9;
    }
    public void setRelation(Long relation)
    {
        this.relation = relation;
    }

    public Long getRelation()
    {
        return relation;
    }
    public void setSimilarity(Double similarity)
    {
        this.similarity = similarity;
    }

    public Double getSimilarity()
    {
        return similarity;
    }
    public void setReferredNumber(String referredNumber)
    {
        this.referredNumber = referredNumber;
    }

    public String getReferredNumber()
    {
        return referredNumber;
    }
    public void setCountryRecall(String countryRecall)
    {
        this.countryRecall = countryRecall;
    }

    public String getCountryRecall()
    {
        return countryRecall;
    }
    public void setTagSourceRecall(String tagSourceRecall)
    {
        this.tagSourceRecall = tagSourceRecall;
    }

    public String getTagSourceRecall()
    {
        return tagSourceRecall;
    }
    public void setTagProducts(String tagProducts)
    {
        this.tagProducts = tagProducts;
    }

    public String getTagProducts()
    {
        return tagProducts;
    }
    public void setTagScores(String tagScores)
    {
        this.tagScores = tagScores;
    }

    public String getTagScores()
    {
        return tagScores;
    }
    public void setWeeks(String weeks)
    {
        this.weeks = weeks;
    }

    public String getWeeks()
    {
        return weeks;
    }
    public void setProdCateId(Long prodCateId)
    {
        this.prodCateId = prodCateId;
    }

    public Long getProdCateId()
    {
        return prodCateId;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("recallId", getRecallId())
                .append("id", getId())
                .append("sourceNameEn", getSourceNameEn())
                .append("sourceNameCn", getSourceNameCn())
                .append("sourceDomainName", getSourceDomainName())
                .append("collectDate", getCollectDate())
                .append("sourceType", getSourceType())
                .append("sourceUrl", getSourceUrl())
                .append("publishDate", getPublishDate())
                .append("titleEn", getTitleEn())
                .append("titleCn", getTitleCn())
                .append("productsEn", getProductsEn())
                .append("productsCn", getProductsCn())
                .append("descriptionEn", getDescriptionEn())
                .append("descriptionCn", getDescriptionCn())
                .append("firmEn", getFirmEn())
                .append("firmCn", getFirmCn())
                .append("country", getCountry())
                .append("remedyOptions", getRemedyOptions())
                .append("tagProduct", getTagProduct())
                .append("tagProductId", getTagProductId())
                .append("tagFirm", getTagFirm())
                .append("tagHazards", getTagHazards())
                .append("tagSource", getTagSource())
                .append("tagChina", getTagChina())
                .append("tagPeople", getTagPeople())
                .append("tagEmotion", getTagEmotion())
                .append("tagLevel", getTagLevel())
                .append("recallNumber", getRecallNumber())
                .append("recallDate", getRecallDate())
                .append("consumerContact", getConsumerContact())
                .append("productUpcs", getProductUpcs())
                .append("injuries", getInjuries())
                .append("citation", getCitation())
                .append("hazardsEn", getHazardsEn())
                .append("hazardsCn", getHazardsCn())
                .append("p1", getP1())
                .append("p2", getP2())
                .append("p3", getP3())
                .append("p4", getP4())
                .append("p5", getP5())
                .append("p6", getP6())
                .append("p7", getP7())
                .append("p8", getP8())
                .append("p9", getP9())
                .append("relation", getRelation())
                .append("similarity", getSimilarity())
                .append("referredNumber", getReferredNumber())
                .append("countryRecall", getCountryRecall())
                .append("tagSourceRecall", getTagSourceRecall())
                .append("tagProducts", getTagProducts())
                .append("tagScores", getTagScores())
                .append("weeks", getWeeks())
                .append("prodCateId", getProdCateId())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("delFlag", getDelFlag())
                .toString();
    }
}
