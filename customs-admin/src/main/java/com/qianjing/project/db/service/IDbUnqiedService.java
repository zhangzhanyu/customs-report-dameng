package com.qianjing.project.db.service;

import java.util.List;
import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;

/**
 * 检验不合格数据Service接口
 * 
 * @author qianjing
 * @date 2022-06-09
 */
public interface IDbUnqiedService 
{
    /**
     * 查询检验不合格数据
     * 
     * @param unqiedId 检验不合格数据主键
     * @return 检验不合格数据
     */
    public DbUnqied selectDbUnqiedByUnqiedId(Long unqiedId);

    /**
     * 查询检验不合格数据列表
     * 
     * @param dbUnqied 检验不合格数据
     * @return 检验不合格数据集合
     */
    public List<DbUnqied> selectDbUnqiedList(DbUnqied dbUnqied);

    /**
     * 新增检验不合格数据
     * 
     * @param dbUnqied 检验不合格数据
     * @return 结果
     */
    public int insertDbUnqied(DbUnqied dbUnqied);

    /**
     * 修改检验不合格数据
     * 
     * @param dbUnqied 检验不合格数据
     * @return 结果
     */
    public int updateDbUnqied(DbUnqied dbUnqied);

    /**
     * 批量删除检验不合格数据
     * 
     * @param unqiedIds 需要删除的检验不合格数据主键集合
     * @return 结果
     */
    public int deleteDbUnqiedByUnqiedIds(Long[] unqiedIds);

    /**
     * 删除检验不合格数据信息
     * 
     * @param unqiedId 检验不合格数据主键
     * @return 结果
     */
    public int deleteDbUnqiedByUnqiedId(Long unqiedId);

    /**
     * 导入检验不合格数据信息
     *
     * @param dbUnqiedList 检验不合格数据信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importDbUnqied(List<DbUnqied> dbUnqiedList, Boolean isUpdateSupport, String operName);

    /**
     * 校验报关单号+项号唯一性
     *
     * @param entryId 报关单号
     * @param gNo     商品项号
     * @return 结果
     */
    public DbUnqied checkEntryIdGnoUnique(String entryId, Long gNo);

    public String checkDbUnqiedUnique(DbUnqied dbUnqied);

    /**
     * 导入危险化学品及其包装检验监管情况统计信息
     *
     * @param dbUnqiedList 批量数据
     * @return 结果
     */
    public int batchInsertDbUnqiedList(List<DbUnqied> dbUnqiedList);
}
