package com.qianjing.project.db.domain;

import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 风险监测信息对象 db_risk_monitor
 * 
 * @author qianjing
 * @date 2022-08-03
 */
public class DbRiskMonitor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    @Excel(name = "业务主键")
    private Long riskMonitorId;

    /** 信息标题 */
    @Excel(name = "信息标题")
    private String messageTitle;

    /** 信息类别 */
    @Excel(name = "信息类别")
    private String messageCategory;

    /** 信息分类 */
    @Excel(name = "信息分类")
    private String messageClassification;

    /** 涉及产品名称 */
    @Excel(name = "涉及产品名称")
    private String involvedProductName;

    /** 涉及产品大类 */
    @Excel(name = "涉及产品大类")
    private String involvedProductCategory;

    /** 法规或标准生效日期 */
    @Excel(name = "法规或标准生效日期")
    private String effectiveDate;

    /** 货号/款号/型号/规格 */
    @Excel(name = "货号/款号/型号/规格")
    private String specification;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 型号 */
    @Excel(name = "型号")
    private String model;

    /** 产品大类 */
    @Excel(name = "产品大类")
    private String productCategory;

    /** 产品描述 */
    @Excel(name = "产品描述")
    private String productDescription;

    /** 生产商国别或地区 */
    @Excel(name = "生产商国别或地区")
    private String manufacturerRegion;

    /** 不合格风险项目 */
    @Excel(name = "不合格风险项目")
    private String unqiedProject;

    /** 不合格风险内容 */
    @Excel(name = "不合格风险内容")
    private String unqiedContent;

    /** 事故报告数量 */
    @Excel(name = "事故报告数量")
    private String accidentNumber;

    /** 伤害报告数量 */
    @Excel(name = "伤害报告数量")
    private String injuryNumber;

    /** 召回或下架发布日期 */
    @Excel(name = "召回或下架发布日期")
    private String recallDate;

    /** 安全隐患或事故详细描述 */
    @Excel(name = "安全隐患或事故详细描述")
    private String accidentDetailDescription;

    /** 是否发生事故 */
    @Excel(name = "是否发生事故")
    private String accidentFlag;

    /** 事故发生时间 */
    @Excel(name = "事故发生时间")
    private String accidentTime;

    /** 事故发生地点 */
    @Excel(name = "事故发生地点")
    private String accidentAddress;

    /** 事故发生具体地址 */
    @Excel(name = "事故发生具体地址")
    private String accidentAddressDetaili;

    /** 伤害部位 */
    @Excel(name = "伤害部位")
    private String injurySite;

    /** 伤害类型 */
    @Excel(name = "伤害类型")
    private String injuryType;

    /** 伤害描述 */
    @Excel(name = "伤害描述")
    private String injuryDescription;

    /** 投诉人身份类别 */
    @Excel(name = "投诉人身份类别")
    private String complaintCategory;

    /** 投诉人姓名 */
    @Excel(name = "投诉人姓名")
    private String complaintName;

    /** 受害人与投诉人的关系 */
    @Excel(name = "受害人与投诉人的关系")
    private String relationship;

    /** 受害人性别 */
    @Excel(name = "受害人性别")
    private String victimGender;

    /** 受害人年龄 */
    @Excel(name = "受害人年龄")
    private String victimAge;

    /** 投诉人联系电话 */
    @Excel(name = "投诉人联系电话")
    private String complaintTelephone;

    /** 投诉人联系地址 */
    @Excel(name = "投诉人联系地址")
    private String complaintAddress;

    /** 舆情话题 */
    @Excel(name = "舆情话题")
    private String topic;

    /** 舆情类别 */
    @Excel(name = "舆情类别")
    private String category;

    /** 信息发布国家 */
    @Excel(name = "信息发布国家")
    private String releaseCountry;

    /** 信息发布机构 */
    @Excel(name = "信息发布机构")
    private String releaseOrganization;

    /** 风险信息来源 */
    @Excel(name = "风险信息来源")
    private String riskSource;

    /** 原文链接 */
    @Excel(name = "原文链接")
    private String originalLink;

    /** 原文译文 */
    @Excel(name = "原文译文")
    private String originalTranslation;

    /** 报送稿 */
    @Excel(name = "报送稿")
    private String draft;

    /** 处置建议 */
    @Excel(name = "处置建议")
    private String disposalSuggestions;

    /** 是否需要调查 */
    @Excel(name = "是否需要调查")
    private String investigateFlag;

    /** 目的调查直属局 */
    @Excel(name = "目的调查直属局")
    private String bureau;

    /** 是否推送专家组评估 */
    @Excel(name = "是否推送专家组评估")
    private String pushFlag;

    /** 选择专家组 */
    @Excel(name = "选择专家组")
    private String expertGroup;

    /** 生产商名称 */
    @Excel(name = "生产商名称")
    private String manufacturerName;

    public void setRiskMonitorId(Long riskMonitorId) 
    {
        this.riskMonitorId = riskMonitorId;
    }

    public Long getRiskMonitorId() 
    {
        return riskMonitorId;
    }
    public void setMessageTitle(String messageTitle) 
    {
        this.messageTitle = messageTitle;
    }

    public String getMessageTitle() 
    {
        return messageTitle;
    }
    public void setMessageCategory(String messageCategory) 
    {
        this.messageCategory = messageCategory;
    }

    public String getMessageCategory() 
    {
        return messageCategory;
    }
    public void setMessageClassification(String messageClassification) 
    {
        this.messageClassification = messageClassification;
    }

    public String getMessageClassification() 
    {
        return messageClassification;
    }
    public void setInvolvedProductName(String involvedProductName) 
    {
        this.involvedProductName = involvedProductName;
    }

    public String getInvolvedProductName() 
    {
        return involvedProductName;
    }
    public void setInvolvedProductCategory(String involvedProductCategory) 
    {
        this.involvedProductCategory = involvedProductCategory;
    }

    public String getInvolvedProductCategory() 
    {
        return involvedProductCategory;
    }
    public void setEffectiveDate(String effectiveDate) 
    {
        this.effectiveDate = effectiveDate;
    }

    public String getEffectiveDate() 
    {
        return effectiveDate;
    }
    public void setSpecification(String specification) 
    {
        this.specification = specification;
    }

    public String getSpecification() 
    {
        return specification;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setModel(String model) 
    {
        this.model = model;
    }

    public String getModel() 
    {
        return model;
    }
    public void setProductCategory(String productCategory) 
    {
        this.productCategory = productCategory;
    }

    public String getProductCategory() 
    {
        return productCategory;
    }
    public void setProductDescription(String productDescription) 
    {
        this.productDescription = productDescription;
    }

    public String getProductDescription() 
    {
        return productDescription;
    }
    public void setManufacturerRegion(String manufacturerRegion) 
    {
        this.manufacturerRegion = manufacturerRegion;
    }

    public String getManufacturerRegion() 
    {
        return manufacturerRegion;
    }
    public void setUnqiedProject(String unqiedProject) 
    {
        this.unqiedProject = unqiedProject;
    }

    public String getUnqiedProject() 
    {
        return unqiedProject;
    }
    public void setUnqiedContent(String unqiedContent) 
    {
        this.unqiedContent = unqiedContent;
    }

    public String getUnqiedContent() 
    {
        return unqiedContent;
    }
    public void setAccidentNumber(String accidentNumber) 
    {
        this.accidentNumber = accidentNumber;
    }

    public String getAccidentNumber() 
    {
        return accidentNumber;
    }
    public void setInjuryNumber(String injuryNumber) 
    {
        this.injuryNumber = injuryNumber;
    }

    public String getInjuryNumber() 
    {
        return injuryNumber;
    }
    public void setRecallDate(String recallDate) 
    {
        this.recallDate = recallDate;
    }

    public String getRecallDate() 
    {
        return recallDate;
    }
    public void setAccidentDetailDescription(String accidentDetailDescription) 
    {
        this.accidentDetailDescription = accidentDetailDescription;
    }

    public String getAccidentDetailDescription() 
    {
        return accidentDetailDescription;
    }
    public void setAccidentFlag(String accidentFlag) 
    {
        this.accidentFlag = accidentFlag;
    }

    public String getAccidentFlag() 
    {
        return accidentFlag;
    }
    public void setAccidentTime(String accidentTime) 
    {
        this.accidentTime = accidentTime;
    }

    public String getAccidentTime() 
    {
        return accidentTime;
    }
    public void setAccidentAddress(String accidentAddress) 
    {
        this.accidentAddress = accidentAddress;
    }

    public String getAccidentAddress() 
    {
        return accidentAddress;
    }
    public void setAccidentAddressDetaili(String accidentAddressDetaili) 
    {
        this.accidentAddressDetaili = accidentAddressDetaili;
    }

    public String getAccidentAddressDetaili() 
    {
        return accidentAddressDetaili;
    }
    public void setInjurySite(String injurySite) 
    {
        this.injurySite = injurySite;
    }

    public String getInjurySite() 
    {
        return injurySite;
    }
    public void setInjuryType(String injuryType) 
    {
        this.injuryType = injuryType;
    }

    public String getInjuryType() 
    {
        return injuryType;
    }
    public void setInjuryDescription(String injuryDescription) 
    {
        this.injuryDescription = injuryDescription;
    }

    public String getInjuryDescription() 
    {
        return injuryDescription;
    }
    public void setComplaintCategory(String complaintCategory) 
    {
        this.complaintCategory = complaintCategory;
    }

    public String getComplaintCategory() 
    {
        return complaintCategory;
    }
    public void setComplaintName(String complaintName) 
    {
        this.complaintName = complaintName;
    }

    public String getComplaintName() 
    {
        return complaintName;
    }
    public void setRelationship(String relationship) 
    {
        this.relationship = relationship;
    }

    public String getRelationship() 
    {
        return relationship;
    }
    public void setVictimGender(String victimGender) 
    {
        this.victimGender = victimGender;
    }

    public String getVictimGender() 
    {
        return victimGender;
    }
    public void setVictimAge(String victimAge) 
    {
        this.victimAge = victimAge;
    }

    public String getVictimAge() 
    {
        return victimAge;
    }
    public void setComplaintTelephone(String complaintTelephone) 
    {
        this.complaintTelephone = complaintTelephone;
    }

    public String getComplaintTelephone() 
    {
        return complaintTelephone;
    }
    public void setComplaintAddress(String complaintAddress) 
    {
        this.complaintAddress = complaintAddress;
    }

    public String getComplaintAddress() 
    {
        return complaintAddress;
    }
    public void setTopic(String topic) 
    {
        this.topic = topic;
    }

    public String getTopic() 
    {
        return topic;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setReleaseCountry(String releaseCountry) 
    {
        this.releaseCountry = releaseCountry;
    }

    public String getReleaseCountry() 
    {
        return releaseCountry;
    }
    public void setReleaseOrganization(String releaseOrganization) 
    {
        this.releaseOrganization = releaseOrganization;
    }

    public String getReleaseOrganization() 
    {
        return releaseOrganization;
    }
    public void setRiskSource(String riskSource) 
    {
        this.riskSource = riskSource;
    }

    public String getRiskSource() 
    {
        return riskSource;
    }
    public void setOriginalLink(String originalLink) 
    {
        this.originalLink = originalLink;
    }

    public String getOriginalLink() 
    {
        return originalLink;
    }
    public void setOriginalTranslation(String originalTranslation) 
    {
        this.originalTranslation = originalTranslation;
    }

    public String getOriginalTranslation() 
    {
        return originalTranslation;
    }
    public void setDraft(String draft) 
    {
        this.draft = draft;
    }

    public String getDraft() 
    {
        return draft;
    }
    public void setDisposalSuggestions(String disposalSuggestions) 
    {
        this.disposalSuggestions = disposalSuggestions;
    }

    public String getDisposalSuggestions() 
    {
        return disposalSuggestions;
    }
    public void setInvestigateFlag(String investigateFlag) 
    {
        this.investigateFlag = investigateFlag;
    }

    public String getInvestigateFlag() 
    {
        return investigateFlag;
    }
    public void setBureau(String bureau) 
    {
        this.bureau = bureau;
    }

    public String getBureau() 
    {
        return bureau;
    }
    public void setPushFlag(String pushFlag) 
    {
        this.pushFlag = pushFlag;
    }

    public String getPushFlag() 
    {
        return pushFlag;
    }
    public void setExpertGroup(String expertGroup) 
    {
        this.expertGroup = expertGroup;
    }

    public String getExpertGroup() 
    {
        return expertGroup;
    }
    public void setManufacturerName(String manufacturerName) 
    {
        this.manufacturerName = manufacturerName;
    }

    public String getManufacturerName() 
    {
        return manufacturerName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("riskMonitorId", getRiskMonitorId())
            .append("messageTitle", getMessageTitle())
            .append("messageCategory", getMessageCategory())
            .append("messageClassification", getMessageClassification())
            .append("involvedProductName", getInvolvedProductName())
            .append("involvedProductCategory", getInvolvedProductCategory())
            .append("effectiveDate", getEffectiveDate())
            .append("specification", getSpecification())
            .append("productName", getProductName())
            .append("brand", getBrand())
            .append("model", getModel())
            .append("productCategory", getProductCategory())
            .append("productDescription", getProductDescription())
            .append("manufacturerRegion", getManufacturerRegion())
            .append("unqiedProject", getUnqiedProject())
            .append("unqiedContent", getUnqiedContent())
            .append("accidentNumber", getAccidentNumber())
            .append("injuryNumber", getInjuryNumber())
            .append("recallDate", getRecallDate())
            .append("accidentDetailDescription", getAccidentDetailDescription())
            .append("accidentFlag", getAccidentFlag())
            .append("accidentTime", getAccidentTime())
            .append("accidentAddress", getAccidentAddress())
            .append("accidentAddressDetaili", getAccidentAddressDetaili())
            .append("injurySite", getInjurySite())
            .append("injuryType", getInjuryType())
            .append("injuryDescription", getInjuryDescription())
            .append("complaintCategory", getComplaintCategory())
            .append("complaintName", getComplaintName())
            .append("relationship", getRelationship())
            .append("victimGender", getVictimGender())
            .append("victimAge", getVictimAge())
            .append("complaintTelephone", getComplaintTelephone())
            .append("complaintAddress", getComplaintAddress())
            .append("topic", getTopic())
            .append("category", getCategory())
            .append("releaseCountry", getReleaseCountry())
            .append("releaseOrganization", getReleaseOrganization())
            .append("riskSource", getRiskSource())
            .append("originalLink", getOriginalLink())
            .append("originalTranslation", getOriginalTranslation())
            .append("draft", getDraft())
            .append("disposalSuggestions", getDisposalSuggestions())
            .append("investigateFlag", getInvestigateFlag())
            .append("bureau", getBureau())
            .append("pushFlag", getPushFlag())
            .append("expertGroup", getExpertGroup())
            .append("manufacturerName", getManufacturerName())
            .toString();
    }
}
