package com.qianjing.project.db.mapper;

import java.util.List;
import com.qianjing.project.db.domain.DbUnqied;
import org.apache.ibatis.annotations.Param;

/**
 * 检验不合格数据Mapper接口
 * 
 * @author qianjing
 * @date 2022-06-09
 */
public interface DbUnqiedMapper 
{
    /**
     * 查询检验不合格数据
     * 
     * @param unqiedId 检验不合格数据主键
     * @return 检验不合格数据
     */
    public DbUnqied selectDbUnqiedByUnqiedId(Long unqiedId);

    /**
     * 查询检验不合格数据列表
     * 
     * @param dbUnqied 检验不合格数据
     * @return 检验不合格数据集合
     */
    public List<DbUnqied> selectDbUnqiedList(DbUnqied dbUnqied);

    /**
     * 新增检验不合格数据
     * 
     * @param dbUnqied 检验不合格数据
     * @return 结果
     */
    public int insertDbUnqied(DbUnqied dbUnqied);

    /**
     * 修改检验不合格数据
     * 
     * @param dbUnqied 检验不合格数据
     * @return 结果
     */
    public int updateDbUnqied(DbUnqied dbUnqied);

    /**
     * 删除检验不合格数据
     * 
     * @param unqiedId 检验不合格数据主键
     * @return 结果
     */
    public int deleteDbUnqiedByUnqiedId(Long unqiedId);

    /**
     * 批量删除检验不合格数据
     * 
     * @param unqiedIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbUnqiedByUnqiedIds(Long[] unqiedIds);

    public DbUnqied checkEntryIdGnoUnique(@Param("entryId") String entryId, @Param("gNo") Long gNo);

    public int batchInsertDbUnqiedList(List<DbUnqied> dbUnqiedList);
}
