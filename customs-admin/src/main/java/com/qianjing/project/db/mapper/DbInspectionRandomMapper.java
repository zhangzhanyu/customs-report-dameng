package com.qianjing.project.db.mapper;

import java.util.List;
import com.qianjing.project.db.domain.DbInspectionRandom;

/**
 * 2021年法检目录外商品抽查检验结果汇总Mapper接口
 * 
 * @author zhangzy
 * @date 2022-08-03
 */
public interface DbInspectionRandomMapper 
{
    /**
     * 查询2021年法检目录外商品抽查检验结果汇总
     * 
     * @param inspectionRandomId 2021年法检目录外商品抽查检验结果汇总主键
     * @return 2021年法检目录外商品抽查检验结果汇总
     */
    public DbInspectionRandom selectDbInspectionRandomByInspectionRandomId(Long inspectionRandomId);

    /**
     * 查询2021年法检目录外商品抽查检验结果汇总列表
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 2021年法检目录外商品抽查检验结果汇总集合
     */
    public List<DbInspectionRandom> selectDbInspectionRandomList(DbInspectionRandom dbInspectionRandom);

    /**
     * 新增2021年法检目录外商品抽查检验结果汇总
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 结果
     */
    public int insertDbInspectionRandom(DbInspectionRandom dbInspectionRandom);

    /**
     * 修改2021年法检目录外商品抽查检验结果汇总
     * 
     * @param dbInspectionRandom 2021年法检目录外商品抽查检验结果汇总
     * @return 结果
     */
    public int updateDbInspectionRandom(DbInspectionRandom dbInspectionRandom);

    /**
     * 删除2021年法检目录外商品抽查检验结果汇总
     * 
     * @param inspectionRandomId 2021年法检目录外商品抽查检验结果汇总主键
     * @return 结果
     */
    public int deleteDbInspectionRandomByInspectionRandomId(Long inspectionRandomId);

    /**
     * 批量删除2021年法检目录外商品抽查检验结果汇总
     * 
     * @param inspectionRandomIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbInspectionRandomByInspectionRandomIds(Long[] inspectionRandomIds);
}
