package com.qianjing.project.db.mapper;

import java.util.List;
import com.qianjing.project.db.domain.DbRiskMonitor;

/**
 * 风险监测信息Mapper接口
 * 
 * @author qianjing
 * @date 2022-08-03
 */
public interface DbRiskMonitorMapper 
{
    /**
     * 查询风险监测信息
     * 
     * @param riskMonitorId 风险监测信息主键
     * @return 风险监测信息
     */
    public DbRiskMonitor selectDbRiskMonitorByRiskMonitorId(Long riskMonitorId);

    /**
     * 查询风险监测信息列表
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 风险监测信息集合
     */
    public List<DbRiskMonitor> selectDbRiskMonitorList(DbRiskMonitor dbRiskMonitor);

    /**
     * 新增风险监测信息
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 结果
     */
    public int insertDbRiskMonitor(DbRiskMonitor dbRiskMonitor);

    /**
     * 修改风险监测信息
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 结果
     */
    public int updateDbRiskMonitor(DbRiskMonitor dbRiskMonitor);

    /**
     * 删除风险监测信息
     * 
     * @param riskMonitorId 风险监测信息主键
     * @return 结果
     */
    public int deleteDbRiskMonitorByRiskMonitorId(Long riskMonitorId);

    /**
     * 批量删除风险监测信息
     * 
     * @param riskMonitorIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbRiskMonitorByRiskMonitorIds(Long[] riskMonitorIds);
}
