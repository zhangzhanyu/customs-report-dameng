package com.qianjing.project.db.service.impl;

import com.qianjing.framework.task.convert.DbUnqiedConvert;
import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.db.service.IDbUnqiedService;
import com.qianjing.project.entry.domain.FieldPoolDO;
import com.qianjing.project.entry.service.IDbSlaveService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 异步任务服务
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AsyncUpdateTaskService {

    private static final Logger log = LoggerFactory.getLogger(AsyncUpdateTaskService.class);

    private final IDbSlaveService dbSlaveService;

    private final IDbUnqiedService dbUnqiedService;

    @Async
    public void dataScheduled(List<DbUnqied> list) {
        List<FieldPoolDO> fieldPoolDOList = dbSlaveService.selectFieldPoolDtoList(list);
        List<DbUnqied> dataList = DbUnqiedConvert.convertDOListToVOList(fieldPoolDOList);
        for (DbUnqied data : dataList) {
            DbUnqied oldData = dbUnqiedService.checkEntryIdGnoUnique(data.getEntryId(), data.getgNo());
            if (oldData == null) {
                dbUnqiedService.insertDbUnqied(data);
            }else {
                data.setUnqiedId(oldData.getUnqiedId());
                dbUnqiedService.updateDbUnqied(data);
            }
        }
    }

}
