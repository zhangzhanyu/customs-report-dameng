package com.qianjing.project.db.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;

import java.util.Date;

public class DbUnqiedImport {

    /** 报关单号 */
    @Excel(name = "报关单号")
    private String entryId;

    /** 项号 */
    @Excel(name = "项号")
    private Long goodNo;

    /** 不合格项目 */
    @Excel(name = "不合格项目")
    private String item;

    /** 判定依据名称 */
    @Excel(name = "判定依据名称")
    private String judgmentBasis;

    /** 不合格描述 */
    @Excel(name = "不合格描述")
    private String description;

    /** 不合格处置 */
    @Excel(name = "不合格处置")
    private String disposal;

    /** 现场检验不合格（Y是 N否） */
    @Excel(name = "现场检验不合格", readConverterExp = "Y=是,N=否")
    private String unqualifiedSiteFlag;

    /** 实验室不合格（Y是 N否） */
    @Excel(name = "实验室不合格", readConverterExp = "Y=是,N=否")
    private String unqualifiedLaboratoryFlag;

    /** 录入海关 */
    @Excel(name = "录入海关")
    private String enterCustoms;

    /** 上报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上报时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reportTime;

    /** 确认状态（0待确认 1已确认） */
    @Excel(name = "确认状态", readConverterExp = "0=待确认,1=已确认")
    private String confirmStatus;

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public Long getGoodNo() {
        return goodNo;
    }

    public void setGoodNo(Long goodNo) {
        this.goodNo = goodNo;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getJudgmentBasis() {
        return judgmentBasis;
    }

    public void setJudgmentBasis(String judgmentBasis) {
        this.judgmentBasis = judgmentBasis;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisposal() {
        return disposal;
    }

    public void setDisposal(String disposal) {
        this.disposal = disposal;
    }

    public String getUnqualifiedSiteFlag() {
        return unqualifiedSiteFlag;
    }

    public void setUnqualifiedSiteFlag(String unqualifiedSiteFlag) {
        this.unqualifiedSiteFlag = unqualifiedSiteFlag;
    }

    public String getUnqualifiedLaboratoryFlag() {
        return unqualifiedLaboratoryFlag;
    }

    public void setUnqualifiedLaboratoryFlag(String unqualifiedLaboratoryFlag) {
        this.unqualifiedLaboratoryFlag = unqualifiedLaboratoryFlag;
    }

    public String getEnterCustoms() {
        return enterCustoms;
    }

    public void setEnterCustoms(String enterCustoms) {
        this.enterCustoms = enterCustoms;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }

    public String getConfirmStatus() {
        return confirmStatus;
    }

    public void setConfirmStatus(String confirmStatus) {
        this.confirmStatus = confirmStatus;
    }
}
