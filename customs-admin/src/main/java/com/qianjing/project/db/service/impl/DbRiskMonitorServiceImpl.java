package com.qianjing.project.db.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qianjing.project.db.mapper.DbRiskMonitorMapper;
import com.qianjing.project.db.domain.DbRiskMonitor;
import com.qianjing.project.db.service.IDbRiskMonitorService;

/**
 * 风险监测信息Service业务层处理
 * 
 * @author qianjing
 * @date 2022-08-03
 */
@Service
public class DbRiskMonitorServiceImpl implements IDbRiskMonitorService 
{
    private static final Logger log = LoggerFactory.getLogger(DbRiskMonitorServiceImpl.class);

    @Autowired
    private DbRiskMonitorMapper dbRiskMonitorMapper;

    /**
     * 查询风险监测信息
     * 
     * @param riskMonitorId 风险监测信息主键
     * @return 风险监测信息
     */
    @Override
    public DbRiskMonitor selectDbRiskMonitorByRiskMonitorId(Long riskMonitorId)
    {
        return dbRiskMonitorMapper.selectDbRiskMonitorByRiskMonitorId(riskMonitorId);
    }

    /**
     * 查询风险监测信息列表
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 风险监测信息
     */
    @Override
    public List<DbRiskMonitor> selectDbRiskMonitorList(DbRiskMonitor dbRiskMonitor)
    {
        return dbRiskMonitorMapper.selectDbRiskMonitorList(dbRiskMonitor);
    }

    /**
     * 新增风险监测信息
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 结果
     */
    @Override
    public int insertDbRiskMonitor(DbRiskMonitor dbRiskMonitor)
    {
        return dbRiskMonitorMapper.insertDbRiskMonitor(dbRiskMonitor);
    }

    /**
     * 修改风险监测信息
     * 
     * @param dbRiskMonitor 风险监测信息
     * @return 结果
     */
    @Override
    public int updateDbRiskMonitor(DbRiskMonitor dbRiskMonitor)
    {
        return dbRiskMonitorMapper.updateDbRiskMonitor(dbRiskMonitor);
    }

    /**
     * 批量删除风险监测信息
     * 
     * @param riskMonitorIds 需要删除的风险监测信息主键
     * @return 结果
     */
    @Override
    public int deleteDbRiskMonitorByRiskMonitorIds(Long[] riskMonitorIds)
    {
        return dbRiskMonitorMapper.deleteDbRiskMonitorByRiskMonitorIds(riskMonitorIds);
    }

    /**
     * 删除风险监测信息信息
     * 
     * @param riskMonitorId 风险监测信息主键
     * @return 结果
     */
    @Override
    public int deleteDbRiskMonitorByRiskMonitorId(Long riskMonitorId)
    {
        return dbRiskMonitorMapper.deleteDbRiskMonitorByRiskMonitorId(riskMonitorId);
    }

    /**
     * 导入风险监测信息数据
     *
     * @param dbRiskMonitorList dbRiskMonitorList 风险监测信息信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importDbRiskMonitor(List<DbRiskMonitor> dbRiskMonitorList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(dbRiskMonitorList) || dbRiskMonitorList.size() == 0) {
            throw new ServiceException("导入风险监测信息数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (DbRiskMonitor dbRiskMonitor : dbRiskMonitorList){
            try {
                // 验证是否存在
                if (true) {
                    dbRiskMonitor.setCreateBy(operName);
                    this.insertDbRiskMonitor(dbRiskMonitor);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    dbRiskMonitor.setUpdateBy(operName);
                    this.updateDbRiskMonitor(dbRiskMonitor);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
