package com.qianjing.project.db.controller;

import com.qianjing.common.utils.file.FileUploadUtils;
import com.qianjing.common.utils.file.MimeTypeUtils;
import com.qianjing.common.utils.poi.ExcelUtil;
import com.qianjing.framework.aspectj.lang.annotation.Log;
import com.qianjing.framework.aspectj.lang.enums.BusinessType;
import com.qianjing.framework.web.controller.BaseController;
import com.qianjing.framework.web.domain.AjaxResult;
import com.qianjing.framework.web.page.TableDataInfo;
import com.qianjing.project.db.domain.DbUnqiedOther;
import com.qianjing.project.db.service.IDbUnqiedOtherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 其他检验不合格数据Controller
 * 
 * @author qianjing
 * @date 2022-08-03
 */
@RestController
@RequestMapping("/db/other")
public class DbUnqiedOtherController extends BaseController
{
    @Autowired
    private IDbUnqiedOtherService dbUnqiedOtherService;

    /**
     * 查询其他检验不合格数据列表
     */
    @PreAuthorize("@ss.hasPermi('db:other:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbUnqiedOther dbUnqiedOther)
    {
        startPage();
        List<DbUnqiedOther> list = dbUnqiedOtherService.selectDbUnqiedOtherList(dbUnqiedOther);
        return getDataTable(list);
    }

    /**
     * 导出其他检验不合格数据列表
     */
    @PreAuthorize("@ss.hasPermi('db:other:export')")
    @Log(title = "其他检验不合格数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DbUnqiedOther dbUnqiedOther)
    {
        List<DbUnqiedOther> list = dbUnqiedOtherService.selectDbUnqiedOtherList(dbUnqiedOther);
        ExcelUtil<DbUnqiedOther> util = new ExcelUtil<DbUnqiedOther>(DbUnqiedOther.class);
        return util.exportExcel(list, "其他检验不合格数据数据");
    }

    /**
     * 导入其他检验不合格数据列表
     */
    @Log(title = "其他检验不合格数据", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('db:other:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        FileUploadUtils.assertAllowed(file, MimeTypeUtils.EXCEL_EXTENSION);
        ExcelUtil<DbUnqiedOther> util = new ExcelUtil<DbUnqiedOther>(DbUnqiedOther.class);
        List<DbUnqiedOther> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = dbUnqiedOtherService.importDbUnqiedOther(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 下载其他检验不合格数据导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {
        ExcelUtil<DbUnqiedOther> util = new ExcelUtil<DbUnqiedOther>(DbUnqiedOther.class);
        util.importTemplateExcel(response, "其他检验不合格数据数据");
    }

    /**
     * 获取其他检验不合格数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('db:other:query')")
    @GetMapping(value = "/{unqiedOtherId}")
    public AjaxResult getInfo(@PathVariable("unqiedOtherId") Long unqiedOtherId)
    {
        return AjaxResult.success(dbUnqiedOtherService.selectDbUnqiedOtherByUnqiedOtherId(unqiedOtherId));
    }

    /**
     * 新增其他检验不合格数据
     */
    @PreAuthorize("@ss.hasPermi('db:other:add')")
    @Log(title = "其他检验不合格数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbUnqiedOther dbUnqiedOther)
    {
        return toAjax(dbUnqiedOtherService.insertDbUnqiedOther(dbUnqiedOther));
    }

    /**
     * 修改其他检验不合格数据
     */
    @PreAuthorize("@ss.hasPermi('db:other:edit')")
    @Log(title = "其他检验不合格数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbUnqiedOther dbUnqiedOther)
    {
        return toAjax(dbUnqiedOtherService.updateDbUnqiedOther(dbUnqiedOther));
    }

    /**
     * 删除其他检验不合格数据
     */
    @PreAuthorize("@ss.hasPermi('db:other:remove')")
    @Log(title = "其他检验不合格数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unqiedOtherIds}")
    public AjaxResult remove(@PathVariable Long[] unqiedOtherIds)
    {
        return toAjax(dbUnqiedOtherService.deleteDbUnqiedOtherByUnqiedOtherIds(unqiedOtherIds));
    }
}
