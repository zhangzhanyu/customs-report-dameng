package com.qianjing.project.db.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qianjing.framework.aspectj.lang.annotation.Excel;
import com.qianjing.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 检验不合格数据对象 db_unqied
 * 
 * @author 上海前景网络科技有限公司
 * @date 2023-05-16
 */
public class DbUnqied extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务主键 */
    private Long unqiedId;

    /** 报关单号 */
    @Excel(name = "报关单号")
    private String entryId;

    /** 项号 */
    @Excel(name = "项号")
    private Long gNo;

    /** 报关单号项号 */
    @Excel(name = "报关单号项号")
    private String entryIdAndGNo;

    /** HS编码 */
    @Excel(name = "HS编码")
    private String codeTs;

    /** 类别码 */
    @Excel(name = "类别码",type = Excel.Type.EXPORT)
    private String iqCode;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String gName;

    /** 监管方式 */
    @Excel(name = "监管方式",type = Excel.Type.EXPORT)
    private String tradeMode;

    /** 进出口标识（I进口 E出口）  */
    @Excel(name = "进出口标识", readConverterExp = "I=进口,E=出口")
    private String iEFlag;

    /** 进出境口岸（1中国海关） */
    @Excel(name = "进出境口岸", readConverterExp = "1=中国海关")
    private String iEPort;

    /** 二级口岸 */
    @Excel(name = "二级口岸",type = Excel.Type.EXPORT)
    private String iqImportPort;

    /** 产地国别区域 */
    @Excel(name = "产地国别区域",type = Excel.Type.EXPORT)
    private String originCountry;

    /** 贸易国别区域 */
    @Excel(name = "贸易国别区域",type = Excel.Type.EXPORT)
    private String tradeCountry;

    /** 申报数量 */
    @Excel(name = "申报数量",type = Excel.Type.EXPORT)
    private String gQty;

    /** 申报计量单位 */
    @Excel(name = "申报计量单位",type = Excel.Type.EXPORT)
    private String gUnit;

    /** 法定第一数量 */
    @Excel(name = "法定第一数量",type = Excel.Type.EXPORT)
    private Double qty1;

    /** 法定第一计量单位 */
    @Excel(name = "法定第一计量单位",type = Excel.Type.EXPORT)
    private String unit1;

    /** 第二数量 */
    @Excel(name = "第二数量",type = Excel.Type.EXPORT)
    private Double qty2;

    /** 第二计量单位 */
    @Excel(name = "第二计量单位",type = Excel.Type.EXPORT)
    private String unit2;

    /** 统计人民币价 */
    @Excel(name = "统计人民币价",type = Excel.Type.EXPORT)
    private Double rmbPrice;

    /** 统计美元价 */
    @Excel(name = "统计美元价",type = Excel.Type.EXPORT)
    private Double usdPrice;

    /** 品牌 */
    @Excel(name = "品牌",type = Excel.Type.EXPORT)
    private String productBrand;

    /** 型号 */
    @Excel(name = "型号",type = Excel.Type.EXPORT)
    private String productModel;

    /** 批次号 */
    @Excel(name = "批次号",type = Excel.Type.EXPORT)
    private String productBatch;

    /** 货物属性 */
    @Excel(name = "货物属性",type = Excel.Type.EXPORT)
    private String productCharCode;

    /** UN编号 */
    @Excel(name = "UN编号",type = Excel.Type.EXPORT)
    private String ungid;

    /** 发货人 */
    @Excel(name = "发货人",type = Excel.Type.EXPORT)
    private String frnConsignNameFn;

    /** 收货人 */
    @Excel(name = "收货人",type = Excel.Type.EXPORT)
    private String consignName;

    /** 消费使用单位 */
    @Excel(name = "消费使用单位",type = Excel.Type.EXPORT)
    private String ownerName;

    /** 申报单位名称 */
    @Excel(name = "申报单位名称",type = Excel.Type.EXPORT)
    private String agentName;

    /** 申报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申报时间", width = 30, dateFormat = "yyyy-MM-dd",type = Excel.Type.EXPORT)
    private Date dDate;

    /** 查验时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "查验时间", width = 30, dateFormat = "yyyy-MM-dd",type = Excel.Type.EXPORT)
    private Date checkStartTime;

    /** 查验环节 */
    @Excel(name = "查验环节",type = Excel.Type.EXPORT)
    private String checkState;

    /** 查验海关 */
    @Excel(name = "查验海关",type = Excel.Type.EXPORT)
    private String inspectionDeptName;

    /** 放行时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "放行时间", width = 30, dateFormat = "yyyy-MM-dd",type = Excel.Type.EXPORT)
    private Date createDate;

    /** 不合格项目 */
    @Excel(name = "不合格项目")
    private String item;

    /** 判定依据名称 */
    @Excel(name = "判定依据名称")
    private String judgmentBasis;

    /** 不合格描述 */
    @Excel(name = "不合格描述")
    private String description;

    /** 不合格处置 */
    @Excel(name = "不合格处置")
    private String disposal;

    /** 现场检验不合格（Y是 N否） */
    @Excel(name = "现场检验不合格", readConverterExp = "Y=是,N=否")
    private String unqualifiedSiteFlag;

    /** 实验室不合格（Y是 N否） */
    @Excel(name = "实验室不合格", readConverterExp = "Y=是,N=否")
    private String unqualifiedLaboratoryFlag;

    /** 录入海关 */
    @Excel(name = "录入海关")
    private String enterCustoms;

    /** 上报时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上报时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reportTime;

    /** 确认状态（0待确认 1已确认） */
    @Excel(name = "确认状态", readConverterExp = "0=待确认,1=已确认")
    private String confirmStatus;

    public void setUnqiedId(Long unqiedId) 
    {
        this.unqiedId = unqiedId;
    }

    public Long getUnqiedId() 
    {
        return unqiedId;
    }
    public void setEntryId(String entryId) 
    {
        this.entryId = entryId;
    }

    public String getEntryId() 
    {
        return entryId;
    }

    public Long getgNo() {
        return gNo;
    }

    public void setgNo(Long gNo) {
        this.gNo = gNo;
    }

    public void setEntryIdAndGNo(String entryIdAndGNo)
    {
        this.entryIdAndGNo = entryIdAndGNo;
    }

    public String getEntryIdAndGNo() 
    {
        return entryIdAndGNo;
    }
    public void setCodeTs(String codeTs) 
    {
        this.codeTs = codeTs;
    }

    public String getCodeTs() 
    {
        return codeTs;
    }
    public void setIqCode(String iqCode) 
    {
        this.iqCode = iqCode;
    }

    public String getIqCode() 
    {
        return iqCode;
    }
    public void setgName(String gName) 
    {
        this.gName = gName;
    }

    public String getgName() 
    {
        return gName;
    }
    public void setTradeMode(String tradeMode) 
    {
        this.tradeMode = tradeMode;
    }

    public String getTradeMode() 
    {
        return tradeMode;
    }
    public void setiEFlag(String iEFlag) 
    {
        this.iEFlag = iEFlag;
    }

    public String getiEFlag() 
    {
        return iEFlag;
    }
    public void setiEPort(String iEPort) 
    {
        this.iEPort = iEPort;
    }

    public String getiEPort() 
    {
        return iEPort;
    }
    public void setIqImportPort(String iqImportPort) 
    {
        this.iqImportPort = iqImportPort;
    }

    public String getIqImportPort() 
    {
        return iqImportPort;
    }
    public void setOriginCountry(String originCountry) 
    {
        this.originCountry = originCountry;
    }

    public String getOriginCountry() 
    {
        return originCountry;
    }
    public void setTradeCountry(String tradeCountry) 
    {
        this.tradeCountry = tradeCountry;
    }

    public String getTradeCountry() 
    {
        return tradeCountry;
    }
    public void setgQty(String gQty) 
    {
        this.gQty = gQty;
    }

    public String getgQty() 
    {
        return gQty;
    }
    public void setgUnit(String gUnit) 
    {
        this.gUnit = gUnit;
    }

    public String getgUnit() 
    {
        return gUnit;
    }
    public void setQty1(Double qty1)
    {
        this.qty1 = qty1;
    }

    public Double getQty1()
    {
        return qty1;
    }
    public void setUnit1(String unit1) 
    {
        this.unit1 = unit1;
    }

    public String getUnit1() 
    {
        return unit1;
    }
    public void setQty2(Double qty2)
    {
        this.qty2 = qty2;
    }

    public Double getQty2()
    {
        return qty2;
    }
    public void setUnit2(String unit2) 
    {
        this.unit2 = unit2;
    }

    public String getUnit2() 
    {
        return unit2;
    }
    public void setRmbPrice(Double rmbPrice)
    {
        this.rmbPrice = rmbPrice;
    }

    public Double getRmbPrice()
    {
        return rmbPrice;
    }
    public void setUsdPrice(Double usdPrice)
    {
        this.usdPrice = usdPrice;
    }

    public Double getUsdPrice()
    {
        return usdPrice;
    }
    public void setProductBrand(String productBrand) 
    {
        this.productBrand = productBrand;
    }

    public String getProductBrand() 
    {
        return productBrand;
    }
    public void setProductModel(String productModel) 
    {
        this.productModel = productModel;
    }

    public String getProductModel() 
    {
        return productModel;
    }
    public void setProductBatch(String productBatch) 
    {
        this.productBatch = productBatch;
    }

    public String getProductBatch() 
    {
        return productBatch;
    }
    public void setProductCharCode(String productCharCode) 
    {
        this.productCharCode = productCharCode;
    }

    public String getProductCharCode() 
    {
        return productCharCode;
    }
    public void setUngid(String ungid) 
    {
        this.ungid = ungid;
    }

    public String getUngid() 
    {
        return ungid;
    }
    public void setFrnConsignNameFn(String frnConsignNameFn) 
    {
        this.frnConsignNameFn = frnConsignNameFn;
    }

    public String getFrnConsignNameFn() 
    {
        return frnConsignNameFn;
    }
    public void setConsignName(String consignName) 
    {
        this.consignName = consignName;
    }

    public String getConsignName() 
    {
        return consignName;
    }
    public void setOwnerName(String ownerName) 
    {
        this.ownerName = ownerName;
    }

    public String getOwnerName() 
    {
        return ownerName;
    }
    public void setAgentName(String agentName) 
    {
        this.agentName = agentName;
    }

    public String getAgentName() 
    {
        return agentName;
    }
    public void setdDate(Date dDate) 
    {
        this.dDate = dDate;
    }

    public Date getdDate() 
    {
        return dDate;
    }
    public void setCheckStartTime(Date checkStartTime) 
    {
        this.checkStartTime = checkStartTime;
    }

    public Date getCheckStartTime() 
    {
        return checkStartTime;
    }
    public void setCheckState(String checkState) 
    {
        this.checkState = checkState;
    }

    public String getCheckState() 
    {
        return checkState;
    }
    public void setInspectionDeptName(String inspectionDeptName) 
    {
        this.inspectionDeptName = inspectionDeptName;
    }

    public String getInspectionDeptName() 
    {
        return inspectionDeptName;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setItem(String item) 
    {
        this.item = item;
    }

    public String getItem() 
    {
        return item;
    }
    public void setJudgmentBasis(String judgmentBasis) 
    {
        this.judgmentBasis = judgmentBasis;
    }

    public String getJudgmentBasis() 
    {
        return judgmentBasis;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setDisposal(String disposal) 
    {
        this.disposal = disposal;
    }

    public String getDisposal() 
    {
        return disposal;
    }
    public void setUnqualifiedSiteFlag(String unqualifiedSiteFlag) 
    {
        this.unqualifiedSiteFlag = unqualifiedSiteFlag;
    }

    public String getUnqualifiedSiteFlag() 
    {
        return unqualifiedSiteFlag;
    }
    public void setUnqualifiedLaboratoryFlag(String unqualifiedLaboratoryFlag) 
    {
        this.unqualifiedLaboratoryFlag = unqualifiedLaboratoryFlag;
    }

    public String getUnqualifiedLaboratoryFlag() 
    {
        return unqualifiedLaboratoryFlag;
    }
    public void setEnterCustoms(String enterCustoms) 
    {
        this.enterCustoms = enterCustoms;
    }

    public String getEnterCustoms() 
    {
        return enterCustoms;
    }
    public void setReportTime(Date reportTime) 
    {
        this.reportTime = reportTime;
    }

    public Date getReportTime() 
    {
        return reportTime;
    }
    public void setConfirmStatus(String confirmStatus) 
    {
        this.confirmStatus = confirmStatus;
    }

    public String getConfirmStatus() 
    {
        return confirmStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("unqiedId", getUnqiedId())
            .append("entryId", getEntryId())
            .append("gNo", getgNo())
            .append("entryIdAndGNo", getEntryIdAndGNo())
            .append("codeTs", getCodeTs())
            .append("iqCode", getIqCode())
            .append("gName", getgName())
            .append("tradeMode", getTradeMode())
            .append("iEFlag", getiEFlag())
            .append("iEPort", getiEPort())
            .append("iqImportPort", getIqImportPort())
            .append("originCountry", getOriginCountry())
            .append("tradeCountry", getTradeCountry())
            .append("gQty", getgQty())
            .append("gUnit", getgUnit())
            .append("qty1", getQty1())
            .append("unit1", getUnit1())
            .append("qty2", getQty2())
            .append("unit2", getUnit2())
            .append("rmbPrice", getRmbPrice())
            .append("usdPrice", getUsdPrice())
            .append("productBrand", getProductBrand())
            .append("productModel", getProductModel())
            .append("productBatch", getProductBatch())
            .append("productCharCode", getProductCharCode())
            .append("ungid", getUngid())
            .append("frnConsignNameFn", getFrnConsignNameFn())
            .append("consignName", getConsignName())
            .append("ownerName", getOwnerName())
            .append("agentName", getAgentName())
            .append("dDate", getdDate())
            .append("checkStartTime", getCheckStartTime())
            .append("checkState", getCheckState())
            .append("inspectionDeptName", getInspectionDeptName())
            .append("createDate", getCreateDate())
            .append("item", getItem())
            .append("judgmentBasis", getJudgmentBasis())
            .append("description", getDescription())
            .append("disposal", getDisposal())
            .append("unqualifiedSiteFlag", getUnqualifiedSiteFlag())
            .append("unqualifiedLaboratoryFlag", getUnqualifiedLaboratoryFlag())
            .append("enterCustoms", getEnterCustoms())
            .append("remark", getRemark())
            .append("reportTime", getReportTime())
            .append("confirmStatus", getConfirmStatus())
            .toString();
    }
}
