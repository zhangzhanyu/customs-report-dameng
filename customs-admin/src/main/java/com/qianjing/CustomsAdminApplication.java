package com.qianjing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.WebApplicationInitializer;

/**
 * @ClassName CustomsAdminApplication
 * @Description 启动程序
 * @Created by zzymaster@163.com 2020/7/17 15:06
 * @Version V1.0
 */
@EnableAsync
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class CustomsAdminApplication extends SpringBootServletInitializer implements WebApplicationInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CustomsAdminApplication.class);
    }

    public static void main(String[] args) {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(CustomsAdminApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  风险评估采集启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  o__ __o                o           o__ __o       \n" +
                " <|     v\\              <|>         /v     v\\    \n" +
                " / \\     <\\             / \\        />       <\\ \n" +
                " \\o/     o/           o/   \\o    o/              \n" +
                "  |__  _<|           <|__ __|>  <|                 \n" +
                "  |       \\          /       \\   \\\\            \n" +
                " <o>       \\o      o/         \\o   \\         /  \n" +
                "  |         v\\    /v           v\\   o       o    \n" +
                " / \\         <\\  />             <\\  <\\__ __/>    ");
    }
}
