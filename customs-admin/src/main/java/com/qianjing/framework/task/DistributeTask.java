package com.qianjing.framework.task;

import com.qianjing.common.utils.StringUtils;
import com.qianjing.framework.task.convert.SusionDangerousChemicalConvert;
import com.qianjing.project.common.domain.HgGoodsClassification;
import com.qianjing.project.common.service.IHgGoodsClassificationService;
import com.qianjing.project.entry.domain.FieldPoolDO;
import com.qianjing.project.entry.service.IDbSlaveService;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;
import com.qianjing.project.twoDept.service.ISusionDangerousChemicalService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 分发同步数据
 * @ClassName: DistributeTask
 * @Version: V1.0
 * @CreateTime: 2022年9月16日
 * @Author 上海前景网络科技有限公司
 */
@Component("distributeTask")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DistributeTask {

    private final IDbSlaveService dbSlaveService;

    private final IHgGoodsClassificationService hgGoodsClassificationService;

    private final ISusionDangerousChemicalService dangerousChemicalService;


    /**
     * 数据分发
     *
     * @param goodClassificationName
     */
    public void dataScheduled(String goodClassificationName) {
        //存放编码集合
        List<String> list1 = new ArrayList<>();
        //存放编码集合
        List<String> list2 = new ArrayList<>();
        //存放货物属性
        List<String> list3 = new ArrayList<>();
        if ("危险化学品".equals(goodClassificationName)) {
            //构造入参
            structureParam(goodClassificationName,list1,list2,list3);
            //查询列表
            List<FieldPoolDO> fieldPoolDOList = dbSlaveService.selectFieldPoolDtoList2(list1, list2, list3);
            List<SusionDangerousChemical> dataList = SusionDangerousChemicalConvert.convertDOListToVOList(fieldPoolDOList);
            for (SusionDangerousChemical data : dataList) {
                SusionDangerousChemical oldData = dangerousChemicalService.checkEntryIdGnoUnique(data.getEntryId(), data.getgNo());
                if (oldData == null) {
                    dangerousChemicalService.insertSusionDangerousChemical(data);
                }else {
                    data.setSusionDangerousChemicalId(oldData.getSusionDangerousChemicalId());
                    dangerousChemicalService.updateSusionDangerousChemical(data);
                }
            }
        }
    }

    // 构造查询入参
    private void structureParam(String name, List<String> hsCodeList, List<String> ciqCodeList, List<String> productCharCodeList){
        HgGoodsClassification hgGoodsClassification = new HgGoodsClassification();
        hgGoodsClassification.setClassificationName(name);
        List<HgGoodsClassification> hgGoodsClassificationList = hgGoodsClassificationService.selectHgGoodsClassificationList(hgGoodsClassification);
        for (HgGoodsClassification goodsClassification : hgGoodsClassificationList) {
            if (StringUtils.isNotBlank(goodsClassification.getHsCode())) {
                hsCodeList.add(goodsClassification.getHsCode());
            }
            if (StringUtils.isNotBlank(goodsClassification.getCiqCode())) {
                ciqCodeList.add(goodsClassification.getCiqCode());
            }
            if (StringUtils.isNotBlank(goodsClassification.getProductCharCode())) {
                productCharCodeList.add(goodsClassification.getProductCharCode());
            }
        }
    }

}
