package com.qianjing.framework.task;

import com.qianjing.project.entry.domain.EnEntryHeadListShangjian;
import com.qianjing.project.entry.domain.EnRskCheckFormGoodsShangjian;
import com.qianjing.project.entry.service.IDbSlaveService;
import com.qianjing.project.entry.service.IEnEntryHeadListShangjianService;
import com.qianjing.project.entry.service.IEnRskCheckFormGoodsShangjianService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @Description: 报关表头表体数据+查验结果反馈数据
 * @ClassName: RemoteDbTask
 * @Version: V1.0
 * @CreateTime: 2022年9月16日
 * @Author 上海前景网络科技有限公司
 */
@Component("remoteDbTask")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RemoteDbTask {

    private final IDbSlaveService dbSlaveService;

    private final IEnEntryHeadListShangjianService entryHeadListShangjianService;

    private final IEnRskCheckFormGoodsShangjianService rskCheckFormGoodsShangjianService;

    public void hgHeadListScheduled() {
        EnEntryHeadListShangjian build = new EnEntryHeadListShangjian();
        List<EnEntryHeadListShangjian> itemData = dbSlaveService.selectEntryHeadListShangjianList(build);
        for (EnEntryHeadListShangjian newData: itemData) {
            // 根据(报关单号+项号)确定唯一性
            EnEntryHeadListShangjian oldData = entryHeadListShangjianService.checkEntryIdGnoUnique(newData.getEntryId(), newData.getgNo());
            if (oldData == null) {
                entryHeadListShangjianService.insertEnEntryHeadListShangjian(newData);
            }else {
                newData.setId(oldData.getId());
                entryHeadListShangjianService.updateEnEntryHeadListShangjian(newData);
            }
        }
    }

    public void hgCheckFormScheduled() {
        EnRskCheckFormGoodsShangjian build = new EnRskCheckFormGoodsShangjian();
        List<EnRskCheckFormGoodsShangjian> itemData = dbSlaveService.selectRskCheckFormGoodsShangjianList(build);
        for (EnRskCheckFormGoodsShangjian newData: itemData) {
            // 根据(报关单号+项号)确定唯一性
            EnRskCheckFormGoodsShangjian oldData = rskCheckFormGoodsShangjianService.checkEntryIdGnoUnique(newData.getEntryId(), newData.getgNo());
            if (oldData == null) {
                rskCheckFormGoodsShangjianService.insertRskCheckFormGoodsShangjian(newData);
            }else {
                newData.setId(oldData.getId());
                rskCheckFormGoodsShangjianService.updateRskCheckFormGoodsShangjian(newData);
            }
        }
    }
}
