package com.qianjing.framework.task;

import com.qianjing.common.constant.Constants;
import com.qianjing.framework.task.convert.DbUnqiedConvert;
import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.db.service.IDbUnqiedService;
import com.qianjing.project.entry.domain.FieldPoolDO;
import com.qianjing.project.entry.service.IDbSlaveService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @Description: 不合格任务宽表数据同步
 * @ClassName: unqualifiedTask
 * @Version: V1.0
 * @CreateTime: 2023年5月16日
 * @Author 上海前景网络科技有限公司
 */
@Component("unqualifiedTask")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UnqualifiedTask {

    private static final Logger log = LoggerFactory.getLogger(UnqualifiedTask.class);

    private final IDbSlaveService dbSlaveService;

    private final IDbUnqiedService dbUnqiedService;

    public void dataScheduled() {
        DbUnqied dbUnqied = new DbUnqied();
        dbUnqied.setConfirmStatus(Constants.DB_IS_STATUS_NO);
        List<DbUnqied> selectList = dbUnqiedService.selectDbUnqiedList(dbUnqied);
        List<FieldPoolDO> fieldPoolDOList = dbSlaveService.selectFieldPoolDtoList(selectList);
        List<DbUnqied> dataList = DbUnqiedConvert.convertDOListToVOList(fieldPoolDOList);
        for (DbUnqied data : dataList) {
            DbUnqied oldData = dbUnqiedService.checkEntryIdGnoUnique(data.getEntryId(), data.getgNo());
            if (oldData == null) {
                dbUnqiedService.insertDbUnqied(data);
            }else {
                data.setUnqiedId(oldData.getUnqiedId());
                dbUnqiedService.updateDbUnqied(data);
            }
        }

    }

}
