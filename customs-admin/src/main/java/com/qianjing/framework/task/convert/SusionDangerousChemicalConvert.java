package com.qianjing.framework.task.convert;


import com.qianjing.project.entry.domain.FieldPoolDO;
import com.qianjing.project.twoDept.domain.SusionDangerousChemical;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 项目勘察DO\VO转换类
 * @author loujinglong
 * @version $Id: ProjectSurveyConvert.java, v 0.1 2018年7月3日 下午5:51:21 loujinglong Exp $
 */
public class SusionDangerousChemicalConvert {

    private SusionDangerousChemicalConvert() {
    }

    public static SusionDangerousChemical convertDOToVO(FieldPoolDO fieldPoolDO) {
        SusionDangerousChemical susionDangerousChemical = new SusionDangerousChemical();
        if (fieldPoolDO == null) {
            return susionDangerousChemical;
        }
        susionDangerousChemical.setiEFlag(fieldPoolDO.getiEFlag());
        susionDangerousChemical.setEntryId(fieldPoolDO.getEntryId());
        susionDangerousChemical.setgNo(fieldPoolDO.getgNo());
        susionDangerousChemical.setProductCharCode(fieldPoolDO.getProductCharCode());
        susionDangerousChemical.setCodeTs(fieldPoolDO.getCodeTs());
        susionDangerousChemical.setIqCode(fieldPoolDO.getIqCode());
        susionDangerousChemical.setTrafMode(fieldPoolDO.getTrafMode());
        susionDangerousChemical.setOriginCountry(fieldPoolDO.getOriginCountry());
        susionDangerousChemical.setgName(fieldPoolDO.getgName());
        susionDangerousChemical.setgModel(fieldPoolDO.getgModel());
        susionDangerousChemical.setUngid(fieldPoolDO.getUngid());
        susionDangerousChemical.setQty1(fieldPoolDO.getQty1());
        susionDangerousChemical.setUnit1(fieldPoolDO.getUnit1());
        susionDangerousChemical.setUsdPrice(fieldPoolDO.getUsdPrice());
        susionDangerousChemical.setRmbPrice(fieldPoolDO.getRmbPrice());
        susionDangerousChemical.setCheckResult(fieldPoolDO.getCheckResult());
        return susionDangerousChemical;
    }

    public static List<SusionDangerousChemical> convertDOListToVOList(List<FieldPoolDO> fieldPoolDOList) {
        List<SusionDangerousChemical> susionDangerousChemicalList = new ArrayList<SusionDangerousChemical>();
        if (CollectionUtils.isEmpty(fieldPoolDOList)) {
            return susionDangerousChemicalList;
        }
        return fieldPoolDOList.parallelStream().map(SusionDangerousChemicalConvert::convertDOToVO)
                .collect(Collectors.toList());
    }
}
