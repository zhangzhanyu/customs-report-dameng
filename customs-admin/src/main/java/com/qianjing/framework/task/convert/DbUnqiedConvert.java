package com.qianjing.framework.task.convert;


import com.qianjing.project.db.domain.DbUnqied;
import com.qianjing.project.entry.domain.FieldPoolDO;
import org.apache.commons.collections.CollectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 项目勘察DO\VO转换类
 * @author loujinglong
 * @version $Id: ProjectSurveyConvert.java, v 0.1 2018年7月3日 下午5:51:21 loujinglong Exp $
 */
public class DbUnqiedConvert {

    private DbUnqiedConvert() {
    }

    public static DbUnqied convertDOToVO(FieldPoolDO fieldPoolDO) {
        DbUnqied dbUnqied = new DbUnqied();
        if (fieldPoolDO == null) {
            return dbUnqied;
        }
        dbUnqied.setEntryId(fieldPoolDO.getEntryId());
        // 项号
        dbUnqied.setgNo(fieldPoolDO.getgNo());
        // HS编码
        dbUnqied.setCodeTs(fieldPoolDO.getCodeTs());
        // 类别码
        dbUnqied.setIqCode(fieldPoolDO.getIqCode());
        // 商品名称
        dbUnqied.setgName(fieldPoolDO.getgName());
        // 监管方式
        dbUnqied.setTradeMode(fieldPoolDO.getTradeMode());
        // 进出标识
        dbUnqied.setiEFlag(fieldPoolDO.getiEFlag());
        // 进出境口岸
        dbUnqied.setiEPort(fieldPoolDO.getiEPort());
        // 二级口岸
        dbUnqied.setIqImportPort(fieldPoolDO.getIqImportPort());
        // 产地国别区域
        dbUnqied.setOriginCountry(fieldPoolDO.getOriginCountry());
        // 贸易国别区域
        dbUnqied.setTradeCountry(fieldPoolDO.getTradeCountry());
        // 申报数量(TODO 字段缺失)
        dbUnqied.setgQty(null);
        // 申报计量单位(TODO 字段缺失)
        dbUnqied.setgUnit(null);
        // 法定第一数量
        dbUnqied.setQty1(fieldPoolDO.getQty1());
        // 法定第一计量单位
        dbUnqied.setUnit1(fieldPoolDO.getUnit1());
        // 第二数量
        dbUnqied.setQty2(fieldPoolDO.getQty2());
        // 第二计量单位
        dbUnqied.setUnit2(fieldPoolDO.getUnit2());
        // 统计人民币价
        dbUnqied.setRmbPrice(fieldPoolDO.getRmbPrice());
        // 统计美元价
        dbUnqied.setUsdPrice(fieldPoolDO.getUsdPrice());
        // 品牌
        dbUnqied.setProductBrand(fieldPoolDO.getProductBrand());
        // 型号
        dbUnqied.setProductModel(fieldPoolDO.getProductModel());
        // 批次号(TODO 字段缺失)
        dbUnqied.setProductBatch(null);
        // 货物属性
        dbUnqied.setProductCharCode(fieldPoolDO.getProductCharCode());
        // UN编号
        dbUnqied.setUngid(fieldPoolDO.getUngid());
        // 发货人
        dbUnqied.setFrnConsignNameFn(fieldPoolDO.getFrnConsignNameFn());
        // 收货人
        dbUnqied.setConsignName(fieldPoolDO.getConsignName());
        // 消费使用单位名称
        dbUnqied.setOwnerName(fieldPoolDO.getOwnerName());
        // 申报单位名称
        dbUnqied.setAgentName(fieldPoolDO.getAgentName());
        // 申报时间
        dbUnqied.setdDate(fieldPoolDO.getdDate());
        // 查验时间
        dbUnqied.setCheckStartTime(fieldPoolDO.getCheckStartTime());
        // 查验环节
        dbUnqied.setCheckState(fieldPoolDO.getCheckState());
        // 查验海关(TODO 字段缺失)
        dbUnqied.setInspectionDeptName(null);
        // 放行时间
        dbUnqied.setCreateDate(fieldPoolDO.getCreateTime());
        return dbUnqied;
    }

    public static List<DbUnqied> convertDOListToVOList(List<FieldPoolDO> fieldPoolDOList) {
        List<DbUnqied> dbUnqiedList = new ArrayList<DbUnqied>();
        if (CollectionUtils.isEmpty(fieldPoolDOList)) {
            return dbUnqiedList;
        }
        return fieldPoolDOList.parallelStream().map(DbUnqiedConvert::convertDOToVO)
                .collect(Collectors.toList());
    }
}
