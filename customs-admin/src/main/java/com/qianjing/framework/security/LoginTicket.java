package com.qianjing.framework.security;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginTicket implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 单点编号 */
    private String userGuid;
    /** 用户昵称 */
    private String nickName;
    /** 用户名称 */
    private String userName;
    /** 登录地址 */
    private String ip;
    /** 拥有角色 */
    private Long[] roleIds;

}