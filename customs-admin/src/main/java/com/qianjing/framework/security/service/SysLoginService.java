package com.qianjing.framework.security.service;

import javax.annotation.Resource;
import com.qianjing.framework.security.LoginTicket;
import com.qianjing.framework.security.auth.OAuthAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import com.qianjing.common.constant.Constants;
import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.exception.user.CaptchaException;
import com.qianjing.common.exception.user.CaptchaExpireException;
import com.qianjing.common.exception.user.UserPasswordNotMatchException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.MessageUtils;
import com.qianjing.common.utils.ServletUtils;
import com.qianjing.common.utils.ip.IpUtils;
import com.qianjing.framework.manager.AsyncManager;
import com.qianjing.framework.manager.factory.AsyncFactory;
import com.qianjing.framework.redis.RedisCache;
import com.qianjing.framework.security.LoginUser;
import com.qianjing.project.system.domain.SysUser;
import com.qianjing.project.system.service.ISysConfigService;
import com.qianjing.project.system.service.ISysUserService;

/**
 * 登录校验方法
 * 
 * @author qianjing
 */
@Component
public class SysLoginService
{
    @Autowired
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysUserService userService;
    
    @Autowired
    private ISysConfigService configService;

    /**
     * 登录验证
     * 
     * @param username 用户名
     * @param password 密码
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public String login(String username, String password, String code, String uuid, String paramToken)
    {
        boolean captchaOnOff = configService.selectCaptchaOnOff();
        // 验证码开关
        if (captchaOnOff)
        {
            validateCaptcha(username, code, uuid);
        }
        // 用户验证
        Authentication authentication = null;
        try
        {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        }
        catch (Exception e)
        {
            if (e instanceof BadCredentialsException)
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new ServiceException(e.getMessage());
            }
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        recordLoginInfo(loginUser.getUserId());
        // 生成token
        loginUser.setAccount(username);
        return tokenService.createToken(loginUser,paramToken);
    }


    /**
     * @Description:             单点登录验证
     *             1、校验本应用是否存在此用户
     *             2、本应用不存在则--创建用户+同时为用户添加角色
     *             3、本应用已存在则--更新用户
     * @param loginTicket  统一认证的返回用户信息
     * @return: String
     * @Author: zzy
     * @Date: 2020年7月20日 下午3:55:54
     */
    public String ssoLogin(LoginTicket loginTicket, String paramToken) {
        Authentication authentication;
        try {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager.authenticate(new OAuthAuthenticationToken(loginTicket, null));
        } catch (Exception e) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(loginTicket.getUserName(), Constants.LOGIN_FAIL, e.getMessage()));
            throw new ServiceException(e.getMessage());
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(loginTicket.getUserName(), Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        // 生成token
        loginUser.setAccount(loginTicket.getUserName());
        return tokenService.createToken(loginUser,paramToken);
    }

    /**
     * 校验验证码
     * 
     * @param username 用户名
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid)
    {
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
    }

    /**
     * 记录登录信息
     *
     * @param userId 用户ID
     */
    public void recordLoginInfo(Long userId)
    {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(userId);
        sysUser.setLoginIp(IpUtils.getIpAddr(ServletUtils.getRequest()));
        sysUser.setLoginDate(DateUtils.getNowDate());
        userService.updateUserProfile(sysUser);
    }
}
