package ${packageName}.mapper;

import java.util.List;
import ${packageName}.domain.${ClassName};
#if($table.sub)
import ${packageName}.domain.${subClassName};
#end

/**
 * Copyright: Copyright (c) 2023 上海前景网络科技有限公司
 *
 * ${functionName}Mapper接口
 * @Version: V1.0.0
 * @Author: JAVA研发组成员
 *
 * Modification History:
 * Date         	Author          Version       Description
 *---------------------------------------------------------------*
 * ${datetime} 下午3:00    张占宇          V1.0          initialize
 */
public interface ${ClassName}Mapper {

    ${ClassName} select${ClassName}By${pkColumn.capJavaField}(${pkColumn.javaType} ${pkColumn.javaField});

    List<${ClassName}> select${ClassName}List(${ClassName} ${className});

    int insert${ClassName}(${ClassName} ${className});

    int update${ClassName}(${ClassName} ${className});

    int delete${ClassName}By${pkColumn.capJavaField}(${pkColumn.javaType} ${pkColumn.javaField});

    int delete${ClassName}By${pkColumn.capJavaField}s(${pkColumn.javaType}[] ${pkColumn.javaField}s);
#if($table.sub)

    int delete${subClassName}By${subTableFkClassName}s(${pkColumn.javaType}[] ${pkColumn.javaField}s);

    int batch${subClassName}(List<${subClassName}> ${subclassName}List);

    int delete${subClassName}By${subTableFkClassName}(${pkColumn.javaType} ${pkColumn.javaField});
#end
}
