package com.qianjing.framework.aspectj.lang.enums;

/**
 * 数据源
 * 
 * @author qianjing
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
