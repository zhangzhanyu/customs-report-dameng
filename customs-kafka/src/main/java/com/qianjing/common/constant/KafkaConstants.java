package com.qianjing.common.constant;

public class KafkaConstants {

    public static final String TOPIC_TEST = "topic_gdchk_cbrisk";

    public static final String TOPIC_GROUP1 = "topic_group1";

    public static final String TOPIC_GROUP2 = "topic_group2";
}
