package com.qianjing.common.utils.kafka;

public class KafkaServers {
    private String[] bootStrapServers;

    public String[] getBootStrapServers() {
        return bootStrapServers;
    }

    public void setBootStrapServers(String[] bootStrapServers) {
        this.bootStrapServers = bootStrapServers;
    }
}
