package com.qianjing.project.common.service.impl;

import com.qianjing.common.exception.ServiceException;
import com.qianjing.common.utils.DateUtils;
import com.qianjing.common.utils.StringUtils;
import com.qianjing.project.common.domain.DbRecall;
import com.qianjing.project.common.mapper.DbRecallMapper;
import com.qianjing.project.common.service.IDbRecallService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 官方召回信息Service业务层处理
 * 
 * @author qianjing
 * @date 2023-05-11
 */
@Service
public class DbRecallServiceImpl implements IDbRecallService 
{
    private static final Logger log = LoggerFactory.getLogger(DbRecallServiceImpl.class);

    @Autowired
    private DbRecallMapper dbRecallMapper;

    /**
     * 查询官方召回信息
     * 
     * @param recallId 官方召回信息主键
     * @return 官方召回信息
     */
    @Override
    public DbRecall selectDbRecallByRecallId(Long recallId)
    {
        return dbRecallMapper.selectDbRecallByRecallId(recallId);
    }

    @Override
    public DbRecall selectDbRecallById(Long id) {
        return dbRecallMapper.selectDbRecallById(id);
    }

    /**
     * 查询官方召回信息列表
     * 
     * @param dbRecall 官方召回信息
     * @return 官方召回信息
     */
    @Override
    public List<DbRecall> selectDbRecallList(DbRecall dbRecall)
    {
        return dbRecallMapper.selectDbRecallList(dbRecall);
    }

    /**
     * 新增官方召回信息
     * 
     * @param dbRecall 官方召回信息
     * @return 结果
     */
    @Override
    public int insertDbRecall(DbRecall dbRecall)
    {
        dbRecall.setCreateTime(DateUtils.getNowDate());
        return dbRecallMapper.insertDbRecall(dbRecall);
    }

    /**
     * 修改官方召回信息
     * 
     * @param dbRecall 官方召回信息
     * @return 结果
     */
    @Override
    public int updateDbRecall(DbRecall dbRecall)
    {
        dbRecall.setUpdateTime(DateUtils.getNowDate());
        return dbRecallMapper.updateDbRecall(dbRecall);
    }

    /**
     * 批量删除官方召回信息
     * 
     * @param recallIds 需要删除的官方召回信息主键
     * @return 结果
     */
    @Override
    public int deleteDbRecallByRecallIds(Long[] recallIds)
    {
        return dbRecallMapper.deleteDbRecallByRecallIds(recallIds);
    }

    /**
     * 删除官方召回信息信息
     * 
     * @param recallId 官方召回信息主键
     * @return 结果
     */
    @Override
    public int deleteDbRecallByRecallId(Long recallId)
    {
        return dbRecallMapper.deleteDbRecallByRecallId(recallId);
    }

    /**
     * 导入官方召回信息数据
     *
     * @param dbRecallList dbRecallList 官方召回信息信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importDbRecall(List<DbRecall> dbRecallList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(dbRecallList) || dbRecallList.size() == 0) {
            throw new ServiceException("导入官方召回信息数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (DbRecall dbRecall : dbRecallList){
            try {
                // 验证是否存在
                if (true) {
                    dbRecall.setCreateBy(operName);
                    this.insertDbRecall(dbRecall);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else if (isUpdateSupport) {
                    dbRecall.setUpdateBy(operName);
                    this.updateDbRecall(dbRecall);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、条数据导入成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、条数据导入成功");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、条数据导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
