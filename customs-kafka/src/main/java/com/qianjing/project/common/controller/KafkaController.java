package com.qianjing.project.common.controller;

import com.qianjing.common.constant.KafkaConstants;
import com.qianjing.common.core.lang.UUID;
import com.qianjing.common.utils.StringUtils;
import com.qianjing.common.utils.kafka.KafkaProducerManager;
import com.qianjing.framework.web.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/common/kafka")
public class KafkaController {

    @Autowired
    private KafkaProducerManager kafkaProducerManager = new KafkaProducerManager();

    @GetMapping(value = "/sendMessage")
    public AjaxResult sendMessage(String value, String key, Integer partition){
        if(StringUtils.isBlank(value)){
            return AjaxResult.error("请填写传入的值");
        }
        //Message message = new Message.Builder().id(UuidUtil.getUuid32()).msg(value).sendTime(DateUtils.nowDate()).build();
        //String str = JSONObject.toJSONString(message);
        String str = "";
        if(StringUtils.isNotBlank(key)){
            if(partition != null){
                kafkaProducerManager.sendMessage(KafkaConstants.TOPIC_TEST, partition, key, str);
            }else{
                kafkaProducerManager.sendMessage(KafkaConstants.TOPIC_TEST, key, str);
            }
        }else {
            kafkaProducerManager.sendMessage(KafkaConstants.TOPIC_TEST, str);
        }
        return null;
    }

    /**
     * 单条发送消息
     * @param value     发送的value
     * @param key       发送的key
     * @param partition 绑定的分区
     */
    @GetMapping(value = "/sendOneMessage")
    public void sendOneMessage(String value, String key, Integer partition){
        String msg = "{\n" +
                "  \"collectDate\": \"2019-01-22\",\n" +
                "  \"tag_product\": \"其他\",\n" +
                "  \"citation\": \"\",\n" +
                "  \"similarity\": 0.0,\n" +
                "  \"description_en\": \"Guidecraft Inc. Recalls Children's Puppet Theaters Due to Violation of Lead Paint Standard NEWS from CPSC U.S. Consumer Product Safety Commission Office of Information and Public Affairs Washington, DC 20207 FOR IMMEDIATE RELEASE October 17, 2007 Release #08-031 Firm’s Recall Hotline: (877) 808-8154 CPSC Recall Hotline: (800) 638-2772 CPSC Media Contact: (301) 504-7908 Guidecraft Inc. Recalls Children’s Puppet Theaters Due to Violation of Lead Paint Standard WASHINGTON, D.C. - The U.S. Consumer Product Safety Commission, in cooperation with the firm named below, today announced a voluntary recall of the following consumer product. Consumers should stop using recalled products immediately unless otherwise instructed. It is illegal to resell or attempt to resell a recalled consumer product. Name of Product: Tabletop Puppet Theaters Units: About 5,400 Manufacturer: Guidecraft Inc., of Englewood, N.J. Hazard: Surface paints on the puppet theater’s wooden panels contain excessive levels of lead, violating the federal lead paint standard. Incidents/Injuries: None reported. Description: The recalled puppet theater has red panels on the front and sides and a chalkboard signboard on top. The puppet theater measures about 24-inches in length, 6-inches in width and about 28-inches in height. Sold at: Specialty toy stores, gift shops, catalogs and Web sites nationwide from June 2006 through August 2007 for about $35. Manufactured in: China Remedy: Consumers should immediately take the recalled puppet theaters away from children and contact Guidecraft to receive a replacement theater or another product of equal value. Consumer Contact: For additional information, contact Guidecraft toll-free at (888) 824-1308 between 9 a.m. and 4:30 p.m. CT Monday through Friday, or visit the firm’s Web site at www.guidecraft.com\",\n" +
                "  \"tag_level\": null,\n" +
                "  \"p6\": null,\n" +
                "  \"sourceType\": \"Recall\",\n" +
                "  \"tag_product_id\": 0,\n" +
                "  \"hazards_en\": \"Lead\",\n" +
                "  \"referred_number\": null,\n" +
                "  \"description_cn\": \"Guidecraft Inc.召回因违反CPSC美国消费者产品安全委员会华盛顿特区信息和公共事务办公室（邮编：20207）铅涂料标准新闻而导致的儿童木偶剧院，立即发布于2007年10月17日发布08-031公司召回热线：（877）808-8154 CPSC召回热线：（800）638-2772 CPSC媒体联系方式：（301）504-7908 Guidecraft Inc.召回儿童木偶剧院，原因是违反了华盛顿特区的含铅油漆标准。美国消费品安全委员会与以下公司合作，今天宣布自愿召回以下消费品。消费者应立即停止使用召回产品，除非另有指示。转售或试图转售召回的消费品是违法的。产品名称：桌面木偶剧院单位：约5400制造商：新泽西州恩格尔伍德的Guidecraft公司。危害：木偶剧院木板上的表面涂料含铅量过高，违反了联邦铅涂料标准。事件/伤害：未报告。描述：召回的木偶剧院正面和侧面有红色的面板，顶部有一个黑板标志。木偶剧院长约24英寸，宽6英寸，高约28英寸。2006年6月至2007年8月在全国范围内的专业玩具店、礼品店、目录和网站以35美元左右的价格出售。制造商：中国补救措施：消费者应立即将召回的木偶剧院从儿童手中带走，并联系导游，以获得替代剧院或其他同等价值的产品。消费者联系：欲了解更多信息，请于周一至周五上午9点至下午4:30联系Guidecraft免费电话（888）824-1308，或访问公司网站www.guidecraft.com。\",\n" +
                "  \"recallNumber\": \"08031\",\n" +
                "  \"p7\": null,\n" +
                "  \"source_url\": \"https://www.cpsc.gov/Recalls/2008/Guidecraft-Inc-Recalls-Childrens-Puppet-Theaters-Due-to-Violation-of-Lead-Paint-Standard\",\n" +
                "  \"tag_firm\": null,\n" +
                "  \"hazards_cn\": \"铅\",\n" +
                "  \"p5\": null,\n" +
                "  \"country_recall\": null,\n" +
                "  \"id\": 1189,\n" +
                "  \"firm_en\": \"Guidecraft\",\n" +
                "  \"recallDate\": \"2007-10-17\",\n" +
                "  \"p8\": null,\n" +
                "  \"publishDate\": \"2007-10-17\",\n" +
                "  \"tag_source\": \"US,CN\",\n" +
                "  \"p1\": \"Puppet Theaters\",\n" +
                "  \"tag_source_recall\": null,\n" +
                "  \"sourceName_en\": \"CPSC\",\n" +
                "  \"firm_cn\": \"导航仪\",\n" +
                "  \"consumerContact\": null,\n" +
                "  \"p9\": null,\n" +
                "  \"title_en\": \"Guidecraft Inc. Recalls Children's Puppet Theaters Due to Violation of Lead Paint Standard\",\n" +
                "  \"tag_china\": \"t\",\n" +
                "  \"p2\": \"2082\",\n" +
                "  \"tag_products\": null,\n" +
                "  \"sourceName_cn\": \"美国消费品安全委员会\",\n" +
                "  \"country\": \"China\",\n" +
                "  \"productUPCs\": \"\",\n" +
                "  \"tag_aged\": \"f\",\n" +
                "  \"title_cn\": \"Guidecraft Inc.因违反含铅油漆标准召回儿童木偶剧院。\",\n" +
                "  \"tag_children\": \"f\",\n" +
                "  \"p3\": null,\n" +
                "  \"tag_scores\": null,\n" +
                "  \"sourceDomainName\": \"www.cpsc.gov\",\n" +
                "  \"remedyOptions\": \"\",\n" +
                "  \"injuries\": \"\",\n" +
                "  \"relation\": 0,\n" +
                "  \"products_en\": \"Guidecraft Tabletop Puppet Theaters\",\n" +
                "  \"tag_hazards\": \"Choking\",\n" +
                "  \"p4\": null,\n" +
                "  \"weeks\": \"42\",\n" +
                "  \"tag_emotion\": null,\n" +
                "  \"products_cn\": \"导游台上木偶剧院\"\n" +
                "}";
        kafkaProducerManager.sendMessage(KafkaConstants.TOPIC_TEST, 0, UUID.fastUUID().toString(), msg);
    }

    /**
     * 批量发送消息
     * @param value     发送的value
     * @param key       发送的key
     * @param partition 绑定的分区
     */
    @GetMapping(value = "/batch")
    public void batch(String value, String key, Integer partition){
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            list.add("第"+i+"条数据");
        }
        System.out.println(list.size());
        kafkaProducerManager.sendMessage(KafkaConstants.TOPIC_TEST, list);
    }

}

