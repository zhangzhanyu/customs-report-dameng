package com.qianjing.project.common.service;

import com.qianjing.project.common.domain.DbRecall;

import java.util.List;

/**
 * 官方召回信息Service接口
 * 
 * @author qianjing
 * @date 2023-05-11
 */
public interface IDbRecallService 
{
    /**
     * 查询官方召回信息
     * 
     * @param recallId 官方召回信息主键
     * @return 官方召回信息
     */
    public DbRecall selectDbRecallByRecallId(Long recallId);

    public DbRecall selectDbRecallById(Long id);

    /**
     * 查询官方召回信息列表
     * 
     * @param dbRecall 官方召回信息
     * @return 官方召回信息集合
     */
    public List<DbRecall> selectDbRecallList(DbRecall dbRecall);

    /**
     * 新增官方召回信息
     * 
     * @param dbRecall 官方召回信息
     * @return 结果
     */
    public int insertDbRecall(DbRecall dbRecall);

    /**
     * 修改官方召回信息
     * 
     * @param dbRecall 官方召回信息
     * @return 结果
     */
    public int updateDbRecall(DbRecall dbRecall);

    /**
     * 批量删除官方召回信息
     * 
     * @param recallIds 需要删除的官方召回信息主键集合
     * @return 结果
     */
    public int deleteDbRecallByRecallIds(Long[] recallIds);

    /**
     * 删除官方召回信息信息
     * 
     * @param recallId 官方召回信息主键
     * @return 结果
     */
    public int deleteDbRecallByRecallId(Long recallId);

    /**
     * 导入官方召回信息信息
     *
     * @param dbRecallList 官方召回信息信息列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importDbRecall(List<DbRecall> dbRecallList, Boolean isUpdateSupport, String operName);
}
