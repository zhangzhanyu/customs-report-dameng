package com.qianjing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

/**
 * @ClassName CustomsKafkaApplication
 * @Description 启动程序
 * @Created by zzymaster@163.com 2020/7/17 15:06
 * @Version V1.0
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class , SecurityAutoConfiguration.class})
public class CustomsKafkaApplication extends SpringBootServletInitializer implements WebApplicationInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CustomsKafkaApplication.class);
    }

    public static void main(String[] args) {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(CustomsKafkaApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Kafka启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  o__ __o                o           o__ __o       \n" +
                " <|     v\\              <|>         /v     v\\    \n" +
                " / \\     <\\             / \\        />       <\\ \n" +
                " \\o/     o/           o/   \\o    o/              \n" +
                "  |__  _<|           <|__ __|>  <|                 \n" +
                "  |       \\          /       \\   \\\\            \n" +
                " <o>       \\o      o/         \\o   \\         /  \n" +
                "  |         v\\    /v           v\\   o       o    \n" +
                " / \\         <\\  />             <\\  <\\__ __/>    ");
    }
}
