# 海关-统计报表-后端

#### kafka启动说明

1、必须先启动zookeeper
进入到D:\developer\env\kafka_2.12-3.2.1\bin\windows当前目录下进入命令行窗口输入
zookeeper-server-start.bat D:\developer\env\kafka_2.12-3.2.1\config\zookeeper.properties
注意:千万不要关闭这个窗口，最小化就好

2、然后再启动kafka
进入到D:\developer\env\kafka_2.12-3.2.1\bin\windows当前目录下进入命令行窗口输入
kafka-server-start.bat D:\developer\env\kafka_2.12-3.2.1\config\server.properties
注意:千万不要关闭这个窗口，最小化就好